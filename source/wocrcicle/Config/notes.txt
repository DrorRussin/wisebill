//open configuration file in ...Config\\wocrec.properties
// get the property value from configuration file
//locate window  in center
//set color yellow
// check if HotFolder.exe running in task manager by [hotfolderapp]
//isn't runnig -> start HotFolder by[startHFscript]
// check if WiserunOCR.exe running in task manager by [wiserunapp]
//if running ->stop run by [killWiserunOCRscript]
// check if WisecopyOCR.exe running in task manager by [wisecopyapp]
//if running ->stop run by [killWisecopyOCRscript]
//start WiserunOCR.exe with script by [startWiserunFlat2Inscript]
//start WiserunOCR.exe with script by [startWiserunOCRscript]
//start monitor for find words(failed, crash ... etc by [word2check]) in log by [path2log][formatlog], with delay by [delayreadlog] in mileseconds

//search crash words
//open log file by[path2log], if not exist -> waiting
//read log file by[path2log] each row and search crash words by [word2check]
//if found crash word-> show main window, print: Found error, time
//start search error file
//define search pattern by[regexfilepath]
//scan fro row with crash word to up
//print: path to error file
//stop run HotFolder.exe by [killHFscript]
//if the first find of same file-->ignored move and copy, only restart for hotfolder
//move error file(if exist) to error folder by[errorfolder] and to ocr_out folder by[ocroutfolder] (but the optional by [continuewithoutOCR])
//move to error folder by: [wisecopypath]+er_file+[errorfolder]+[move2errorparam]
//copy to ocr_out folder by: [wisecopypath]+[errorfolder]+file+.pdf+[ocroutfolder]+[copy2ocroutparam]
//if moved print: filename Moved to error
//replace hotfolder job and restart HotFolder by [restartHFscript]
//if restarted, print: HF restarted
//print: Done time

//hide window to tray by clicking on X

//Tray menu
//Show-show main window
//Close-close run processes by[wiserunapp],[wisecopyapp],stop monitoring, exit from programm
//path to image for tray icon ...source/images/1.gif


//folders_list:
Wisepage\\AC\\ocr_flat   -- flating filenames
Wisepage\\AC\\backexc  -- backup and for exlude existing items, only for cicle
Wisepage\\AC\\ocr_pre   -- prepare for items, only for cicle
Wisepage\\AC\\ocr_in     -- input folder for ABBYY
Wisepage\\AC\\ocr_out   -- output folder for ABBYY
Wisepage\\AC\\b           -- backup for .hft file, job HotFolder
Wisepage\\AC\\t            -- recycle
Wisepage\\AC\\befMult   -- for files "as is" after OCR with /rep
Wisepage\\AC\\befexc    -- for create multipages if needed, by name file
Wisepage\\AC\\exc        -- for history, with files 0 kb
Wisepage\\AC\\error      -- for error files from HotFolder
Wisepage\\AC\\error2    -- for invalid files or 0 kb