/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralMatch;

import Constructors.ObData;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author zvika
 */
public class InvoicesByGroups {

    final static int PO_NUMBER_ID = 7;
    final static int supplier_ID = 13;

    private static final InvoicesByGroups instance = null;
    public static HashMap<String, Supplier> Suppliers = new HashMap<String, Supplier>();

    private InvoicesByGroups(Vector<String[]> orderedItemsCSV_orignal) {
        Vector<String[]> copyOrderedItemsCSV = copyVector(orderedItemsCSV_orignal);

        //create Suppliers:
        for (String[] line : copyOrderedItemsCSV) {
            String supplier = line[supplier_ID];
            if (!Suppliers.containsKey(supplier)) {
                Suppliers.put(supplier, new Supplier(supplier));
            }
            //Suppliers.get(supplierID).Invoices.add(e);
        }

        for (int k = 0; k < copyOrderedItemsCSV.size(); k++) {

            String supplier = copyOrderedItemsCSV.get(k)[supplier_ID];
            {
                String PO_NUMBER = copyOrderedItemsCSV.get(k)[PO_NUMBER_ID];

                if (PO_NUMBER != null && !PO_NUMBER.isEmpty()) {
                    Invoice Invoice = new Invoice();
                    for (int i = 0; i < copyOrderedItemsCSV.size(); i++) {
                        if (supplier.equals(copyOrderedItemsCSV.get(i)[supplier_ID])
                                && PO_NUMBER.equals(copyOrderedItemsCSV.get(i)[PO_NUMBER_ID])) {
                            Invoice.addItem(copyOrderedItemsCSV.get(i));
                            copyOrderedItemsCSV.remove(i--);
                        }
                    }
                    Suppliers.get(supplier).Invoices.add(Invoice);
                    k=-1;
                }

            }

            //Suppliers.get(supplierID).Invoices.add(e);
        }
        //System.out.println("GeneralMatch.InvoicesByGroups.<init>()");
    }

    public static InvoicesByGroups getInstance(Vector<String[]> orderedItemsCSV) {
        if (instance == null) {
            new InvoicesByGroups(orderedItemsCSV);
        }
        return instance;
    }

    public static Vector copyVector(Vector v) {
        Vector temp = new Vector();
        for (Object object : v) {
            temp.add(object);
        }
        return temp;
    }
}
