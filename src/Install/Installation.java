/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SConsole.java
 *
 * Created on May 20, 2013, 1:00:02 PM
 */
package Install;

import CSV.ConfigFun;
import Management.SConsole;
import Management.SConsoleAdmin;
import WiseConfig.Config;
import CSV.LFun;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import CSV.Log;

public class Installation extends javax.swing.JFrame {

    String abbyyVersion;
    Process process;
    Config config;
    Log log;

    public Installation() {
        config = new Config(new String[0]);
        WindowsDesign();

        initComponents();
        initialize();

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);// <- prevent closing
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);// <- prevent closing
                String s = ConfigFun.wiseBillFolderPath + "\\source\\images\\1.png";
        if(!new File(s).exists()){
            s = "C:\\HyperOCR\\WiseBill\\source\\images\\1.png";
        }
        ImageIcon main_icon = new ImageIcon(s);
        setIconImage(main_icon.getImage());
        setTitle("WiseBill");
        setSize(new Dimension(620, 370));

    }

    public void installAllProgram() {
        if (checkInstallation()) {

            final JPanel panel = new JPanel();

            JOptionPane.showMessageDialog(panel, "Remember to install ABBYY! \n(This program will install wocrcicle for you).\nYou need to create the task for ABBYY.", "INFO", JOptionPane.OK_OPTION);
            config.createConfigFolder();
            config.setWisePageINIFile("C:\\HyperOCR\\ini\\Wrun_W_Bill_Server.ini");

            abbyyVersion = (String) JOptionPane.showInputDialog(null, "Please choose the ABBYY version", "OCR",
                    JOptionPane.QUESTION_MESSAGE, null, new Object[]{"11.00",
                        "12.00"}, "12.00");
            if (abbyyVersion == null) {
                abbyyVersion = "12.00";
            }

            /*String pathToCsv = ConfigFun.wiseBillFolderPath + "\\files\\csv";
            File csv[] = new File(pathToCsv).listFiles();
            config.setWisePageCSVFiles(csv);*/
            Vector<String> fp = new Vector<String>();
            fp.add("לתשלום");
            fp.add("אשראי");
            fp.add("שולם,סה\"כ");
            fp.add("מגנטי");
            fp.add("סהכ");
            fp.add("מזומן");
            fp.add("מזו מן");
            fp.add("מג נטי");
            fp.add("ביניים");
            config.setForPayWords(fp);
            createFolders();
            moveFiles();//remember to add backup to abbyy
            changeINI();

            installOCR();

            setVisible(false);
            new SConsole().setVisible(true);

        }

    }

    private void installOCR() {
        editINIOCR();
    }

    private void editINIOCR() {
        File Wrun_RestorHFT = new File("C:\\HyperOCR\\wocrcicle\\Config\\Wrun_RestorHFT.ini");
        File wocrec = new File("C:\\HyperOCR\\wocrcicle\\Config\\wocrec.properties");

        String Wrun_RestorHFT_string = read(Wrun_RestorHFT);
        String wocrec_string = read(wocrec);
        Wrun_RestorHFT_string = Wrun_RestorHFT_string.replace("----Username----", System.getProperty("user.name"));
        wocrec_string = wocrec_string.replace("----Username----", System.getProperty("user.name"));
        Wrun_RestorHFT_string = Wrun_RestorHFT_string.replace("----AbbyyVersion----", abbyyVersion);
        wocrec_string = wocrec_string.replace("----AbbyyVersion----", abbyyVersion);
        Wrun_RestorHFT_string = Wrun_RestorHFT_string.replace("----AbbyyVersionPF----", abbyyVersion.substring(0, 2));
        wocrec_string = wocrec_string.replace("----AbbyyVersionPF----", abbyyVersion.substring(0, 2));
        write(Wrun_RestorHFT, Wrun_RestorHFT_string);
        write(wocrec, wocrec_string);

    }

    private void changeINI() {
        File ini = new File("C:\\HyperOCR\\ini\\Wrun_W_Bill_Server.ini");
        String string = read(ini);
        string = string.replace("--chagemeoutput--", output_field.getText());
        string = string.replace("--chagemeinput--", input_field.getText());
        config.setWisePageInput(input_field.getText());
        config.setWisePageOutput(output_field.getText());

        write(ini, string);
    }

    private void moveFiles() {
        createFolder("C:\\HyperOCR\\WiseLogger");
        Vector<String> input = new Vector<String>();
        Vector<String> output = new Vector<String>();
        input.add(ConfigFun.wiseBillFolderPath + "\\files\\csv");
        input.add(ConfigFun.wiseBillFolderPath + "\\files\\wocrcicle");
        input.add(ConfigFun.wiseBillFolderPath + "\\files\\ini");
        input.add(ConfigFun.wiseBillFolderPath + "\\files\\WiseLogger");
        input.add(ConfigFun.wiseBillFolderPath + "\\files\\zones");
        //input.add(ConfigFun.wiseBillFolderPath + "\\files\\htf");
        output.add("C:\\WiseBill\\WB\\csv");
        output.add("C:\\HyperOCR\\wocrcicle");
        output.add("C:\\HyperOCR\\ini");
        output.add("C:\\HyperOCR\\WiseLogger");
        output.add("C:\\WiseBill\\WB\\wise_in");
        //output.add("C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Local\\ABBYY\\HotFolder\\" + abbyyVersion);

        for (int i = 0; i < input.size(); i++) {
            String command = "";
            command = "\""+WiseConfig.Config.getWiseCopyExePath()+"\" \"" + input.get(i) + "\\*.*\" \"" + output.get(i) + "\" /h /l /copy /over";

            try {
                process = Runtime.getRuntime().exec(command);System.out.println(command);
                while (process.isAlive()) {

                }
            } catch (Exception r) {
                String a = "Error in wisecopy";
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        csv_choiser = new javax.swing.JFileChooser();
        tech_m = new javax.swing.JMenuItem();
        log_m = new javax.swing.JMenuItem();
        era_logo = new javax.swing.JLabel();
        software_logo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        input_button = new javax.swing.JButton();
        input_field = new javax.swing.JTextField();
        output_button = new javax.swing.JButton();
        output_field = new javax.swing.JTextField();
        csv_button = new javax.swing.JButton();
        ini_button1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        wtmenu = new javax.swing.JMenuBar();

        csv_choiser.setCurrentDirectory(new java.io.File("E:\\workspace\\freetext"));

        tech_m.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.SHIFT_MASK));
        tech_m.setBackground(new java.awt.Color(255, 255, 204));
        tech_m.setText("tech");
        tech_m.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tech_mActionPerformed(evt);
            }
        });

        log_m.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        log_m.setBackground(new java.awt.Color(255, 255, 204));
        log_m.setText("tech");
        log_m.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                log_mActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 0));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowDeactivated(java.awt.event.WindowEvent evt) {
                formWindowDeactivated(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        jLabel1.setText("@  ERA (Enchanced Regognition Algorithms) Ltd.");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });

        input_button.setText("...");
        input_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                input_buttonActionPerformed(evt);
            }
        });

        input_field.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                input_fieldActionPerformed(evt);
            }
        });

        output_button.setText("...");
        output_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                output_buttonActionPerformed(evt);
            }
        });

        output_field.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                output_fieldActionPerformed(evt);
            }
        });

        csv_button.setText("Set CSV Files");
        csv_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                csv_buttonActionPerformed(evt);
            }
        });

        ini_button1.setText("Install the program");
        ini_button1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ini_button1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Input Folder");

        jLabel3.setText("Output Folder");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        wtmenu.setBackground(new java.awt.Color(0, 51, 153));
        wtmenu.setBorder(new javax.swing.border.MatteBorder(null));
        wtmenu.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        setJMenuBar(wtmenu);
        wtmenu.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 77, Short.MAX_VALUE)
                .addComponent(software_logo, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(189, 189, 189))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(era_logo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(input_field, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
                                    .addComponent(output_field))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(output_button)
                                    .addComponent(input_button))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(csv_button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ini_button1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(era_logo, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(software_logo, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(csv_button, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(input_button)
                    .addComponent(input_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(output_button)
                    .addComponent(output_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ini_button1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        setVisible(false);
        CSV.LFun.killWiseBill(); //System.exit(0);

    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosed

    private void formWindowDeactivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeactivated
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowDeactivated

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        System.out.println("Browse clicked");
        //JOptionPane.showMessageDialog(null,"EDIT clicked");
        Desktop desktop = null;
        if (Desktop.isDesktopSupported()) {
            desktop = Desktop.getDesktop();
            // Now enable buttons for actions that are supported.
        }

        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            System.out.println("browse supported");
            URI uri = null;
            try {
                uri = new URI("www.wisepage.co.il");
                desktop.browse(uri);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } catch (URISyntaxException use) {
                use.printStackTrace();

            }
        }
    }//GEN-LAST:event_jLabel1MouseClicked

    public boolean checkIfNeedCSVChecking() {
        return true;
    }


    private void tech_mActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tech_mActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tech_mActionPerformed

    private void log_mActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_log_mActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_log_mActionPerformed

    private void output_fieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_output_fieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_output_fieldActionPerformed

    private void csv_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_csv_buttonActionPerformed
        // TODO add your handling code here:

        String path = ConfigFun.wiseBillFolderPath + "\\files\\csv";
        try {
            Desktop.getDesktop().open(new File(path));
        } catch (IOException ex) {
            Logger.getLogger(SConsoleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_csv_buttonActionPerformed

    private boolean checkInstallation() {
        String error = "";
        boolean errorAns = true;
        if (input_field.getText().equals("")) {

            error = error + "Please set an input folder.\n";

            errorAns = false;
        }
        if (output_field.getText().equals("")) {

            error = error + "Please set an output folder.\n";

            errorAns = false;
        }
        File file;
        file = new File(ConfigFun.wiseBillFolderPath + "\\files\\csv\\CUSTOMERS.csv");
        if (!file.exists()) {

            error = error + "Please set CUSTOMERS.csv file.\n";

            errorAns = false;

        }
        file = new File(ConfigFun.wiseBillFolderPath + "\\files\\csv\\ITEMS.csv");
        if (!file.exists()) {
            error = error + "Please set ITEMS.csv file\n";
            errorAns = false;
        }
        file = new File(ConfigFun.wiseBillFolderPath + "\\files\\csv\\STORES.csv");
        if (!file.exists()) {
            error = error + "Please set STORES.csv file.\n";

            errorAns = false;
        }
        file = new File(ConfigFun.wiseBillFolderPath + "\\files\\csv\\SUPPLIERS.csv");
        if (!file.exists()) {
            error = error + "Please set SUPPLIERS.csv file.\n";

            errorAns = false;
        }
        if (!errorAns) {

            final JPanel panel = new JPanel();

            JOptionPane.showMessageDialog(panel, error, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return errorAns;
    }


    private void ini_button1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ini_button1ActionPerformed
        // TODO add your handling code here:
        setSize(new Dimension(620, 500));
        installAllProgram();


    }//GEN-LAST:event_ini_button1ActionPerformed

    private void input_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_buttonActionPerformed
        // TODO add your handling code here:

        JFileChooser f = new JFileChooser();
        f.setCurrentDirectory(new File("C:\\"));
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        f.showSaveDialog(null);

        //  input_field.setText(f.getCurrentDirectory().getPath());
        input_field.setText(f.getSelectedFile().getPath());


    }//GEN-LAST:event_input_buttonActionPerformed

    private void output_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_output_buttonActionPerformed
        // TODO add your handling code here:
        JFileChooser f = new JFileChooser();
        f.setCurrentDirectory(new File("C:\\"));

        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        f.showSaveDialog(null);

        output_field.setText(f.getSelectedFile().getPath());
    }//GEN-LAST:event_output_buttonActionPerformed

    private void input_fieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_input_fieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_input_fieldActionPerformed

    public void initialize() {
        /*  
         Locate Window
         */
        Point p = new Point(LFun.calib_center(new Dimension(this.getWidth(), this.getHeight())));
        setBounds(p.x, p.y, this.getWidth(), this.getHeight());
        getContentPane().setBackground(new Color(255, 255, 204));

        /*  
         Process
         */
        final JFrame JFrameSconsole = this;
        /*  
         ERA Logo
         */
        ImageIcon icon = new ImageIcon(ConfigFun.wiseBillFolderPath + "\\source\\images\\ERA-LOGO_110x55.png");
        era_logo.setIcon(icon);
        icon = new ImageIcon(ConfigFun.wiseBillFolderPath + "\\source\\images\\WF-Logo.png");
        software_logo.setIcon(icon);

        /*  
         Menu
         */
        wtmenu.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        wtmenu.setBackground(new java.awt.Color(0, 51, 153));

        /*  
         HotKeys
         */
    }

    public void WindowsDesign() {
        /*   Windows Design   */
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            System.out.println("UIManager Exception : " + e);
        } catch (InstantiationException e) {
            System.out.println("UIManager Exception : " + e);
        } catch (IllegalAccessException e) {
            System.out.println("UIManager Exception : " + e);
        } catch (UnsupportedLookAndFeelException e) {
            System.out.println("UIManager Exception : " + e);
        }
    }

    public static boolean isRunning(Process process) {
        try {
            return process.exitValue() < 0;
        } catch (Exception e) {
            return true;
        }
    }

    private void createFolders() {
        String WiseBillFolders[] = {
            "C:\\WiseBill",
            "C:\\WiseBill\\WB",
            "C:\\WiseBill\\WB\\bill_in",
            "C:\\WiseBill\\WB\\bill_in\\a",
            "C:\\WiseBill\\WB\\bill_out",
            "C:\\WiseBill\\WB\\csv",
            "C:\\WiseBill\\WB\\csv_backup",
            "C:\\WiseBill\\WB\\csv_csvm",
            "C:\\WiseBill\\WB\\csv_csvnc",
            "C:\\WiseBill\\WB\\csv_final",
            "C:\\WiseBill\\WB\\csv_trim",
            "C:\\WiseBill\\WB\\wise_in",
            "C:\\WiseBill\\WB\\wise_out",
            "C:\\WiseBill\\WB\\wise_out\\A",
            "C:\\WiseBill\\WB\\wise_out\\error",
            "C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Local\\ABBYY",
            "C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Local\\ABBYY\\HotFolder",
            "C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Local\\ABBYY\\HotFolder\\" + abbyyVersion};

        for (String name : WiseBillFolders) {
            createFolder(name);
        }
        String ABBYYFolders[] = {
            "C:\\WiseBill\\AC",
            "C:\\WiseBill\\AC\\backupHTF",
            "C:\\WiseBill\\AC\\t",
            "C:\\WiseBill\\AC\\error",
            "C:\\WiseBill\\AC\\ocr_in",
            "C:\\WiseBill\\AC\\ocr_out",
            "C:\\WiseBill\\AC\\ocr_pre"};
        for (String name : ABBYYFolders) {
            createFolder(name);
        }
    }

    private void createFolder(String Path) {
        try {
            File file = new File(Path);
            if (!file.exists()) {
                if (file.mkdir()) {
                    System.out.println("created " + Path);
                } else {
                    System.out.println("Failed to create directory " + Path);
                }
            }
        } catch (Exception r) {

        }
    }

    private void write(File ini, String string) {
        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ini)));
            writer.write(string);

        } catch (IOException ex) {
            // report
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/

            }
        }
    }

    private String read(File ini) {
        String content = null;
        FileReader reader = null;
        try {
            reader = new FileReader(ini);
            char[] chars = new char[(int) ini.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    Logger.getLogger(Installation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return content;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton csv_button;
    private javax.swing.JFileChooser csv_choiser;
    private javax.swing.JLabel era_logo;
    private javax.swing.JButton ini_button1;
    private javax.swing.JButton input_button;
    private javax.swing.JTextField input_field;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JMenuItem log_m;
    private javax.swing.JButton output_button;
    private javax.swing.JTextField output_field;
    private javax.swing.JLabel software_logo;
    private javax.swing.JMenuItem tech_m;
    private javax.swing.JMenuBar wtmenu;
    // End of variables declaration//GEN-END:variables

}
