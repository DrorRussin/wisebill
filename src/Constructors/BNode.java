/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Constructors;

import CSV.CSV2Hash;
import Constructors.ObData;
import java.util.Vector;

/**
 *
 * @author ERA-mobile
 */
public class BNode {

    public ObData mam; // סך הכל מעמ
    public ObData without_mam; // מחיר הפריט ללא מעמ
    public ObData with_mam; // מחיר הפריט עם מעמ
    public ObData date; // תאריך
    public ObData rate;//שער המרה
    public float Percent_difference_rate_block_vs_matches = 0;
    public ObData pn;//מקט
    public ObData price; // מחיר הפריט הרלבנטי לפני הנחה,  כולל מעמ.   
    public ObData price_z;
    public ObData fullprice; // מחיר לאחר הנחה
    public ObData percent; // אחוז מעמ
    public ObData qty;//כמות
    public ObData one_price;//מחיר ליחידה
    public ObData orderNum; // מספר הזמנה של הפריט
    public ObData shipNum; // מספר תעודת משלוח של הפריט
    public ObData supNum;
    public ObData quantityInPack; // מספר פריטים באריזה
    public ObData quantityOfPacks; // מספר אריזות של הפריט
    public ObData itemNumInOrder; // מספר פריט הזמנה
    public String anaha = "0"; // הנחה על הפריט הרלבנטי, לא כולל מע"מ.
    public String anaha_by_percent = "0"; // אחוז ההנחה על הפריט
    public Vector<ObData> un_num; // וקטור מספרים בתוך הבלוק
    public Vector<ObData> listPn; // רשימת מקטים
    public Vector<ObData> descr; // תיאור הפריט
    public String desCsu; // תיאור הפריט כפי שנמצא בCSU
    public String pnCsu; // מספר המקט כפי שנמצא ב-CSU
    public String Curreny_for_one_item; //קוד מטבע שלפיו נרשם המחיר ליחידת מידה של הפריט	
    public String Curreny_for_all_qty; //קוד מטבע שלפיו נרשמה העלות הכוללת של הפריט

    public BNode() {
        mam = null;
        without_mam = null;
        with_mam = null;
        date = null;
        rate = null;
        pn = null;//מקט
        price = null;
        price_z = null;
        fullprice = null;
        percent = null;
        qty = null;//כמות
        one_price = null;//מחיר ליחידה
        orderNum = null;//מספר הזמנה
        shipNum = null; // מספר תעודת משלוח
        supNum = null;
        itemNumInOrder = null;
        anaha = "0";
        anaha_by_percent = "0";
        listPn = new Vector<ObData>();
        un_num = new Vector<ObData>();
        descr = new Vector<ObData>();
        desCsu = "";
        pnCsu = "";
        Curreny_for_one_item = null;
        Curreny_for_all_qty = null;
    }

    public BNode(BNode bn) {
        mam = new ObData(bn.mam);
        without_mam = new ObData(bn.without_mam);
        with_mam = new ObData(bn.with_mam);
        date = new ObData(bn.date);
        rate = new ObData(bn.rate);
        pn = new ObData(bn.pn);//מקט
        price = new ObData(bn.price);
        price_z = new ObData(bn.price_z);
        fullprice = new ObData(bn.fullprice);
        percent = new ObData(bn.percent);
        qty = new ObData(bn.qty);//כמות
        one_price = new ObData(bn.one_price);//מחיר ליחידה
        orderNum = new ObData(bn.orderNum);
        shipNum = new ObData(bn.shipNum);
        supNum = new ObData(bn.supNum);
        itemNumInOrder = new ObData(bn.itemNumInOrder);
        anaha = bn.anaha;
        anaha_by_percent = bn.anaha_by_percent;
        listPn = bn.listPn;
        un_num = bn.un_num;
        descr = bn.descr;
        desCsu = bn.desCsu;
        pnCsu = bn.pnCsu;
        Curreny_for_one_item = bn.Curreny_for_one_item;
        Curreny_for_all_qty = bn.Curreny_for_all_qty;
    }

    public int get_XLeftPn() {
        int indexLeft = -1;
        if (pn != null) {
            indexLeft = pn.get_left();
        } else if (listPn != null) {
            int mostLeft = Integer.MAX_VALUE;
            for (ObData pn : listPn) {
                if (pn.get_left() < mostLeft) {
                    mostLeft = pn.get_left();
                }
            }
            indexLeft = mostLeft;
        }

        return indexLeft;
    }

    public int get_XRightPn() {
        int indexRight = -1;
        if (pn != null) {
            indexRight = pn.get_right();
        } else if (listPn != null) {
            int mostRight = -1;
            for (ObData pn : listPn) {
                if (pn.get_right() > mostRight) {
                    mostRight = pn.get_right();
                }
            }
            indexRight = mostRight;
        }

        return indexRight;
    }

    public int get_XTopPn() {
        int indexTop = -1;
        if (pn != null) {
            indexTop = pn.get_top();
        } else if (listPn != null) {
            int mostTop = Integer.MAX_VALUE;
            for (ObData pn : listPn) {
                if (pn.get_top() < mostTop) {
                    mostTop = pn.get_top();
                }
            }
            indexTop = mostTop;
        }

        return indexTop;
    }

    public int get_XBottomPn() {
        int indexBottom = -1;
        if (pn != null) {
            indexBottom = pn.get_bottom();
        } else if (listPn != null) {
            int mostBottom = - 1;
            for (ObData pn : listPn) {
                if (pn.get_bottom() > mostBottom) {
                    mostBottom = pn.get_bottom();
                }
            }
            indexBottom = mostBottom;
        }

        return indexBottom;
    }

    public int get_XLeftDescription() {
        int indexLeft = -1;
        if (descr != null && descr.size() > 0) {
            int mostLeft = Integer.MAX_VALUE;
            for (ObData word : descr) {
                if (word.get_left() < mostLeft) {
                    mostLeft = word.get_left();
                }
            }

            indexLeft = mostLeft;
        }

        return indexLeft;
    }

    public int get_XRightDescription() {
        int indexRight = -1;
        if (descr != null && descr.size() > 0) {
            int mostRight = -1;
            for (ObData word : descr) {
                if (word.get_right() > mostRight) {
                    mostRight = word.get_right();
                }
            }
            indexRight = mostRight;
        }

        return indexRight;
    }

    public int get_XTopDescription() {
        int indexTop = -1;
        if (descr != null && descr.size() > 0) {
            int mostTop = Integer.MAX_VALUE;
            for (ObData word : descr) {
                if (word.get_top() < mostTop) {
                    mostTop = word.get_top();
                }
            }
            indexTop = mostTop;
        }

        return indexTop;
    }

    public int get_XBottomDescription() {
        int indexBottom = -1;
        if (descr != null && descr.size() > 0) {
            int mostBottom = - 1;
            for (ObData word : descr) {
                if (word.get_bottom() > mostBottom) {
                    mostBottom = word.get_bottom();
                }
            }
            indexBottom = mostBottom;
        }

        return indexBottom;
    }

    public String get_XLeftUnitPrice() {
        if (one_price.get_isFictitious()) {
            return "@";
        }

        int indexLeft = -1;
        if (one_price != null) {
            indexLeft = one_price.get_left();
        }

        return indexLeft + "";
    }

    public String get_XRightUnitPrice() {
        if (one_price.get_isFictitious()) {
            return "@";
        }

        int indexRight = -1;
        if (one_price != null) {
            indexRight = one_price.get_right();
        }

        return indexRight+ "";
    }

    public String get_XTopUnitPrice() {
        if (one_price.get_isFictitious()) {
            return "@";
        }

        int indexTop = -1;
        if (one_price != null) {
            indexTop = one_price.get_top();
        }

        return indexTop+ "";
    }

    public String get_XBottomUnitPrice() {
        if (one_price.get_isFictitious()) {
            return "@";
        }

        int indexBottom = -1;
        if (one_price != null) {
            indexBottom = one_price.get_bottom();
        }

        return indexBottom+ "";
    }

    public int get_XLeftPrice() {
        int indexLeft = -1;
        if (price != null) {
            indexLeft = price.get_left();
        }

        return indexLeft;
    }

    public int get_XRightPrice() {
        int indexRight = -1;
        if (price != null) {
            indexRight = price.get_right();
        }

        return indexRight;
    }

    public int get_XTopPrice() {
        int indexTop = -1;
        if (price != null) {
            indexTop = price.get_top();
        }

        return indexTop;
    }

    public int get_XBottomPrice() {
        int indexBottom = -1;
        if (price != null) {
            indexBottom = price.get_bottom();
        }

        return indexBottom;
    }

    public String get_description() {
        String s = "";
        if (descr==null || descr.size() < 1) {
            return s;
        }
        if (!descr.get(0).get_isVerifived()) {
            s += "~";
        }

        if (CSV2Hash.isHebFile) {
            for (int i = (descr.size() - 1); i >= 0; i--) {
                s = s + descr.elementAt(i).get_result_str().replaceAll("~", "") + " ";
            }
        } else {
            for (int i = 0; i < descr.size(); i++) {
                s = s + descr.elementAt(i).get_result_str().replaceAll("~", "") + " ";
            }
        }

        return s;
    }

    public String get_description_for_CSV() {
        String s = get_description();
        if (s.contains("״") || s.contains("\"")) {
            s = "";
            if (CSV2Hash.isHebFile) {
                for (int i = (descr.size() - 1); i >= 0; i--) {
                    String ele = descr.elementAt(i).get_result_str().replaceAll("~", "");
                    ele = ele.replaceAll("״", "\"").replaceAll("\"", "\"\"");
                    s += ele + " ";
                }
            } else {
                for (int i = 0; i < descr.size(); i++) {
                    String ele = descr.elementAt(i).get_result_str().replaceAll("~", "");
                    ele = ele.replaceAll("״", "\"").replaceAll("\"", "\"\"");
                    s += ele + " ";
                }
            }
            return "\"" + s + "\"";
        } else {
            return s;
        }
    }

    public String get_listPn() {
        String s = "";
        if (listPn.size() < 1) {
            return s;
        }
        for (int i = 0; i < listPn.size(); i++) {
            s += listPn.elementAt(i).get_result_str();
            if (i != listPn.size() - 1) {
                s += " ";
            }
        }

        return s;
    }

    public void set_o_mam(ObData o) {
        mam = new ObData(o);
    }

    public void set_o_without_mam(ObData o) {
        without_mam = new ObData(o);
    }

    public void set_o_with_mam(ObData o) {
        with_mam = new ObData(o);
    }

    public void set_o_date(ObData o) {
        date = new ObData(o);
    }

    public void set_o_rate(ObData o) {
        rate = new ObData(o);
    }

    public void set_o_pn(ObData o) {
        pn = new ObData(o);
    }

    public void set_o_price(ObData o) {
        price = new ObData(o);
    }

    public void set_o_price_z(ObData o) {
        price_z = new ObData(o);
    }

    public void set_o_fullprice(ObData o) {
        fullprice = new ObData(o);
    }

    public void set_o_percent(ObData o) {
        percent = new ObData(o);
    }

    public void set_o_qty(ObData o) {
        qty = new ObData(o);
    }

    public void set_o_one_price(ObData o) {
        one_price = new ObData(o);
    }

    public void set_o_anaha(String s) {
        anaha = s;
    }

    public void set_o_anaha_by_percent(String s) {
        anaha_by_percent = s;
    }

    public void set_o_order_num(ObData o) {
        orderNum = o;
    }

    public void set_o_ship_num(ObData o) {
        shipNum = o;
    }

    public void set_o_quantity_in_pack(ObData o) {
        quantityInPack = o;
    }

    public void set_o_quantity_of_packs(ObData o) {
        quantityOfPacks = o;
    }

    public void add_o_listPn(ObData o) {
        listPn.add(o);
    }

    public void add_o_un_num(ObData o) {
        un_num.add(o);
    }

    public void add_o_descr(ObData o) {
        descr.add(o);
    }

    public String ToString() {
        String s = "";
        if (pn != null) {
            String t = pn.get_result_str();

            for (int i = t.length(); i < 16; i++) {
                //t= t.concat("_");
                t = t.concat(" ");
            }

            s = t + "\t|";
        } else {
            String t = "";
            for (int i = 0; i < 16; i++) {
                //t= t.concat("_");
                t = t.concat(" ");
            }

            s = t + "\t|";
        }

        if (price != null) {
            s = s + price.get_result_str() + "\t|";
        } else {
            s = s + "\t|";
        }

        if (!anaha.equalsIgnoreCase("0")) {
            s = s + anaha + "\t|";
        } else {
            s = s + "\t|";
        }

        if (!anaha_by_percent.equalsIgnoreCase("0")) {
            s = s + anaha_by_percent + "\t|";
        } else {
            s = s + "\t|";
        }

        if (percent != null) {
            s = s + percent.get_result_str() + "\t|";
        } else {
            s = s + "\t|";
        }

        if (qty != null) {
            s = s + qty.get_result_str() + "\t|";
        } else {
            s = s + "\t|";
        }

        if (one_price != null) {
            s = s + one_price.get_result_str() + "\t|";
        } else {
            s = s + "\t|";
        }
        if (fullprice != null) {
            s = s + fullprice.get_result_str() + "\t|";
        } else {
            s = s + "\t|";
        }
        if (un_num != null) {
            s = s + v2string(un_num) + "\t|";
        } else {
            s = s + "\t|";
        }

        if (descr != null) {
            s = s + v2string(descr) + "\t|";
        } else {
            s = s + "\t|";
        }

        return s;
    }

    public String v2string(Vector<ObData> vo) {
        if (vo == null) {
            return "\t";
        }
        if (vo.size() == 0) {
            return "\t";
        }
        String s = "";
        for (int i = 0; i < vo.size(); i++) {
            s = s + " " + vo.elementAt(i).get_result_str();

        }
        return s;
    }
}
