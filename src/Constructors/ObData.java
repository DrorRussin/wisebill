package Constructors;

import java.io.File;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Class for parsing row from CSV
 *
 */
public class ObData implements Comparable<ObData> {

    private String colnames[] = {"result_str", "name_in", "pfile_num", "inx_col", "line_num", "left", "top", "right", "bottom", "inx_renum", "fntsizasc", "fntnameasc", "CR"};
    private String result_str, name_in, fntnameasc, cr, format, pos, name_by_inv/*סוג הערך*/, external_source, STR_MEANING;
    private int pfile_num, inx_col, line_num, left, top, right, bottom, inx_renum, fntsizasc, num_block_by_inv;
    private boolean inMakatCsu = true;
    private boolean isVerifived = true;
    private boolean isFictitious = false;

    public ObData() {

        result_str = "";
        name_in = "";
        fntnameasc = "";
        cr = "";
        pfile_num = 0;
        inx_col = 0;
        line_num = 0;
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        inx_renum = 0;
        fntsizasc = 0;
        pos = "u";
        name_by_inv = "";
        num_block_by_inv = -1;
        format = "GENERAL";
        external_source = "no";
        inMakatCsu = false;
        STR_MEANING = "";
    }

    public ObData(ObData o) {
        result_str = o.result_str;
        name_in = o.name_in;
        fntnameasc = o.fntnameasc;
        cr = o.cr;
        pfile_num = o.pfile_num;
        inx_col = o.inx_col;
        line_num = o.line_num;
        left = o.left;
        top = o.top;
        right = o.right;
        bottom = o.bottom;
        inx_renum = o.inx_renum;
        fntsizasc = o.fntsizasc;
        pos = o.pos;
        name_by_inv = o.name_by_inv;
        num_block_by_inv = o.num_block_by_inv;
        format = o.format;
        if (format.equalsIgnoreCase("INVP")) {
            System.out.println("Constructors.ObData.set_format()");
        }
        external_source = o.external_source;
        STR_MEANING = o.STR_MEANING;

    }

    public String toString() {
        //  System.out.println(result_str);//+" "+format);
        //System.out.println(format);//+" "+);
        return "\n" + colnames[0].toUpperCase() + ": " + result_str
                + "\nFormat: " + format + "\nposition: " + pos;
        // "\n"+colnames[1].toUpperCase()+": "+name_in+
        // "\n"+colnames[2].toUpperCase()+": "+pfile_num+
        // "\n"+colnames[3].toUpperCase()+": "+inx_col+
        // "\n"+colnames[4].toUpperCase()+": "+line_num+
        // "\n"+colnames[5].toUpperCase()+": "+left+
        // "\n"+colnames[6].toUpperCase()+": "+top+
        // "\n"+colnames[7].toUpperCase()+": "+right+
        // "\n"+colnames[8].toUpperCase()+": "+bottom+
        // "\n"+colnames[9].toUpperCase()+": "+inx_renum+
        // "\n"+colnames[10].toUpperCase()+": "+fntsizasc+
        // "\n"+colnames[11].toUpperCase()+": "+fntnameasc+
        // "\n"+colnames[12].toUpperCase()+": "+cr;

    }

    /**
     * set name_by_inv
     *
     * @param s string for name_by_inv
     */
    public void set_name_by_inv(String s) {
        /*if (name_by_inv.equals("price")) {
            //System.out.println("Constructors.ObData.set_name_by_inv()");
        }*/
        name_by_inv = s;
    }

    /**
     * set position
     *
     * @param s string for set position
     */
    public void set_position(String s) {
        pos = s;
    }

    /**
     * set format
     *
     * @param s string for set format
     */
    public void set_format(String s) {
        if (s.equalsIgnoreCase("INVP")) {
            System.out.println("Constructors.ObData.set_format()");
        }
        format = s;
    }

    /**
     * set result string
     *
     * @param s string for set result string
     */
    public void set_result_str(String s) {
        result_str = s;
        if(result_str.equalsIgnoreCase("NaN")){
            System.out.println("Constructors.ObData.set_result_str()");
        }
    }

    /**
     * set name file
     *
     * @param s string for set name file
     */
    public void set_name_in(String s) {
        name_in = s;
    }

    /**
     * set font name asc
     *
     * @param s string for set font name asc
     */
    public void set_fntnameasc(String s) {
        fntnameasc = s;
    }

    /**
     * set cr
     *
     * @param s string for set cr
     */
    public void set_cr(String s) {
        cr = s;
    }

    /**
     * set p+file_num
     *
     * @param n for set p+file_num
     */
    public void set_pfile_num(int n) {
        pfile_num = n;
    }

    /**
     * set index of column
     *
     * @param n for index of column
     */
    public void set_inx_col(int n) {
        inx_col = n;
    }

    /**
     * set line number
     *
     * @param n for line number
     */
    public void set_line_num(int n) {
        line_num = n;
    }

    /**
     * set left coordinate
     *
     * @param n left coordinate
     */
    public void set_left(int n) {
        left = n;
    }

    /**
     * set top coordinate
     *
     * @param n for set top coordinate
     */
    public void set_top(int n) {
        top = n;
    }

    /**
     * set right coordinate
     *
     * @param n for set right coordinate
     */
    public void set_right(int n) {
        right = n;
    }

    /**
     * set bottom coordinate
     *
     * @param n for set bottom coordinate
     */
    public void set_bottom(int n) {
        bottom = n;
    }

    /**
     * set inx_renum
     *
     * @param n for set inx_renum
     */
    public void set_inx_renum(int n) {
        inx_renum = n;
    }

    /**
     * set font size
     *
     * @param n for set font size
     */
    public void set_fntsizasc(int n) {
        fntsizasc = n;
    }

    /**
     * set num_block_by_inv
     *
     * @param n for num_block_by_inv
     */
    public void set_num_block_by_inv(int n) {
        num_block_by_inv = n;
    }

    /**
     * set external_source
     *
     * @param s string for external_source
     */
    public void set_external_source(String s) {
        external_source = s;
    }

    /**
     *
     * @param exists boolean for inMakatCsu
     */
    public void set_in_Makat_Csu(boolean exists) {
        inMakatCsu = exists;
    }

    public void set_STR_MEANING(String s) {
        STR_MEANING = s;
    }

    /**
     *
     * @return external_source
     */
    public String get_external_source() {
        return external_source;
    }

    /**
     *
     * @return name_by_inv
     */
    public String get_name_by_inv() {
        return name_by_inv;
    }

    /**
     *
     * @return position
     */
    public String get_position() {
        return pos;
    }

    /**
     *
     * @return format
     */
    public String get_format() {
        return format;
    }

    /**
     *
     * @return result string
     */
    public String get_result_str() {
        /*if (get_isFictitious()) {
            return "@" + result_str;
        }
        if (!get_isVerifived()) {
            return "~" + result_str;
        }*/
        return result_str;
    }

    /*public String get_result_str_clear() {
        return result_str;
    }*/
    public String get_result_str_print() {
        if (get_isFictitious() & !get_isVerifived()) {
            return "@" + result_str;
        } else if (get_isFictitious() & get_isVerifived()) {
            return "~" + result_str;
        }
        if (!get_isVerifived()) {
            return "~" + result_str;
        }
        return result_str;
    }

    /**
     *
     * @return p+file_num
     */
    public int get_pfile_num() {
        return pfile_num;
    }

    /**
     *
     * @return index of column
     */
    public int get_inx_col() {
        return inx_col;
    }

    /**
     *
     * @return line number
     */
    public int get_line_num() {
        return line_num;
    }

    /**
     *
     * @return left coordinate
     */
    public int get_left() {
        return left;
    }

    /**
     *
     * @return top coordinate
     */
    public int get_top() {
        return top;
    }

    /**
     *
     * @return right coordinate
     */
    public int get_right() {
        return right;
    }

    /**
     *
     * @return bottom coordinate
     */
    public int get_bottom() {
        return bottom;
    }

    /**
     *
     * @return inx_renum
     */
    public int get_inx_renum() {
        return inx_renum;
    }

    /**
     *
     * @return num_block_by_inv
     */
    public int get_num_block_by_inv() {
        return num_block_by_inv;
    }

    /**
     *
     * @return font size
     */
    public int get_fntsizasc() {
        return fntsizasc;
    }

    /**
     *
     * @return name_in
     */
    public String get_name_in() {
        return name_in;
    }

    /**
     *
     * @return font name asc
     */
    public String get_fntnameasc() {
        return fntnameasc;
    }

    /**
     *
     * @return cr
     */
    public String get_cr() {
        return cr;
    }

    /**
     *
     * @return inMakatCsu
     */
    public boolean get_in_Makat_Csu() {
        return inMakatCsu;
    }

    public String get_STR_MEANING() {
        return STR_MEANING;
    }

    public boolean get_isFictitious() {
        if (CSV.CSV2Hash.isDigitalFile) {
            return true;
        }
        return isFictitious;
    }

    public boolean get_isVerifived() {
        if (CSV.CSV2Hash.isDigitalFile) {
            return true;
        }
        return isVerifived;
    }

    public void set_isVerifived(boolean i_IsVerifived) {
        isVerifived = i_IsVerifived;
    }

    public void set_isFictitious(boolean i_isFictitious) {
        isFictitious = i_isFictitious;
    }

    @Override
    public int compareTo(ObData t) {
        int Line = Integer.compare(this.get_line_num(), t.get_line_num());
        if (Line != 0) {
            return Line;
        }

        int left = Integer.compare(this.get_left(), t.get_left());

        return left;
    }
}
