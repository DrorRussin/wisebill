/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constructors;

/**
 *
 * @author ERA-mobile
 */
public class Item {

    double price = 0;
    String pn = "";
    double qty = 0,
            hanaha = 0;

    public Item() {
    }

    public double getPrice() {
        return price;
    }

    public String getPn() {
        return pn;
    }

    public double getQty() {
        return qty;
    }

    public double getHanaha() {
        return hanaha;
    }

    public void setQty(double _qty) {
        qty = _qty;
    }

    public void setPrice(double _price) {
        price = _price;

    }

    public void setPn(String _pn) {
        pn = _pn;

    }

    public void setHanaha(double _hanaha) {
        hanaha = _hanaha;

    }

}
