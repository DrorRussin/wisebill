/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Constructors;

import Constructors.BNode;
import CSV.LFun;
import CSV.LFun;
import Check.CheckSingleDNum;
import Check.CheckSingleMam;
import Check.CheckVM;
import Constructors.VerVar;
import Constructors.ObData;
import Constructors.VerVarWM;
import java.text.DecimalFormat;
import java.util.Vector;

/**
 *
 * @author ERA-mobile
 */
public class CheckMam {

    public Vector<CheckSingleMam> csm; //=new Vector<CheckSingleMam>();
    public Vector<Double> existmamopt = new Vector<Double>();
    public Vector<Double> doublenum;
    public Vector<Double> mamVerify = new Vector<Double>();
    public Vector<Vector<CheckSingleDNum>> perVerify = new Vector<Vector<CheckSingleDNum>>();
    public Vector<String> mam_percents;
    public Vector<BNode> bnv;
    public VerVar sumallmamv;
    public VerVarWM globalanahav;
    public VerVarWM shipmentv;
    public boolean pricewithmam = true;
    public boolean needrefresh = false;
    double tolMam = 0;

    public CheckMam(Vector<BNode> bnv, Vector<CheckSingleMam> csm, Vector<Double> doublenum, Vector<String> mam_percents, VerVar sumallmamv, VerVarWM globalanahav, VerVarWM shipmentv, boolean pricewithmam, double tolMam) {
        this.pricewithmam = pricewithmam;
        this.bnv = bnv;
        this.csm = csm;
        this.doublenum = doublenum;
        doublenum.remove(0.00);
        this.mam_percents = mam_percents;
        this.sumallmamv = sumallmamv;
        this.globalanahav = globalanahav;
        this.shipmentv = shipmentv;
        this.tolMam = tolMam;

        if (sumallmamv.getSum() > 0 && !doublenum.contains(sumallmamv.getSum())) {
            doublenum.add(sumallmamv.getSum());
        }

        if (!doublenum.contains(shipmentv.getSum())) {
            doublenum.add(shipmentv.getSum());
        }

        for (CheckSingleMam checkSingleMam : csm) {
            if (checkSingleMam.getPercent() != -1 && !existmamopt.contains(checkSingleMam.getPercent())) {
                existmamopt.add(checkSingleMam.getPercent());
            }
        }

        for (String mstring : mam_percents) {
            double m = Double.parseDouble(mstring.replaceAll("%", ""));
            if (!existmamopt.contains(m)) {
                existmamopt.add(m);
            }
        }

    }

    public void upd_bnv(Vector<CheckSingleDNum> v) {
        double ts = 0;
        double without_mam = 0;

        int k = 0;
        for (int i = 0; i < csm.size(); i++) {
            CheckSingleMam checkSingleMam = csm.elementAt(i);
            double perc = 0;
            boolean f = false;
            if (checkSingleMam.getPercent() == -1) {
                for (; k < v.size() && !f; k++) {
                    if (v.elementAt(k).getPosInRootVector() == i) {
                        perc = v.elementAt(k).checkNum;
                        f = true;
                    }

                }
            }

            BNode bNode = bnv.elementAt(i);
            if (checkSingleMam.getPercent() == -1) {
                if (pricewithmam) {
                    ts = (checkSingleMam.getPrice() / (100 + perc)) * perc;
                    ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                    without_mam = checkSingleMam.getPrice() - ts;
                    without_mam = Double.parseDouble(new DecimalFormat("##.##").format(without_mam));
                } else {
                    ts = (checkSingleMam.getPrice() / 100) * perc;
                    ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                    without_mam = checkSingleMam.getPrice();
                    without_mam = Double.parseDouble(new DecimalFormat("##.##").format(without_mam));
                }
                bNode.mam = new ObData();
                bNode.mam.set_name_by_inv("mam");
                bNode.mam.set_result_str("" + ts);

                bNode.without_mam = new ObData();
                bNode.without_mam.set_name_by_inv("without_mam");
                bNode.without_mam.set_result_str("" + without_mam);

                bNode.with_mam = new ObData();
                bNode.with_mam.set_name_by_inv("with_mam");
                bNode.with_mam.set_result_str("" + Double.parseDouble(new DecimalFormat("##.##").format(without_mam + ts)));

                bNode.percent = new ObData();
                bNode.percent.set_name_by_inv("percent");
                bNode.percent.set_result_str("" + perc + "%");
            } else {

                perc = checkSingleMam.getPercent();

                if (pricewithmam) {

                    ts = (checkSingleMam.getPrice() / (100 + perc)) * perc;
                    ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                    without_mam = checkSingleMam.getPrice() - ts;
                    without_mam = Double.parseDouble(new DecimalFormat("##.##").format(without_mam));
                } else {
                    ts = (checkSingleMam.getPrice() / 100) * perc;
                    ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                    without_mam = checkSingleMam.getPrice();
                    without_mam = Double.parseDouble(new DecimalFormat("##.##").format(without_mam));
                }

                bNode.mam = new ObData();
                bNode.mam.set_name_by_inv("mam");
                bNode.mam.set_result_str("" + ts);

                bNode.without_mam = new ObData();
                bNode.without_mam.set_name_by_inv("without_mam");
                bNode.without_mam.set_result_str("" + without_mam);

                bNode.with_mam = new ObData();
                bNode.with_mam.set_name_by_inv("with_mam");
                bNode.with_mam.set_result_str("" + Double.parseDouble(new DecimalFormat("##.##").format(without_mam + ts)));

                bNode.percent.set_name_by_inv("percent");
                bNode.percent.set_result_str("" + perc + "%");
            }

        }

    }

    public boolean secondstep() {
        if (secondstep(true)) {
            //if found one type % prices withmam
            return true;
        } else {
            mamVerify.removeAllElements();
            perVerify.removeAllElements();
            if (needrefresh) {
                return false;
            }
            pricewithmam = false;
            if (secondstep(true)) {
                //if found one type % prices withoutmam
                return true;
            } else {
                mamVerify.removeAllElements();
                perVerify.removeAllElements();
                if (secondstep(false)) {
                    //if found shufle types (0+any from mam percents) % prices withoutmam
                    return true;
                } else {
                    mamVerify.removeAllElements();
                    perVerify.removeAllElements();
                    //if found shufle types (0+any from mam percents) % prices withmam
                    pricewithmam = true;
                    if (secondstep(false)) {
                        return true;
                    }
                }

            }
        }

        return false;
    }

    boolean secondstep(boolean first) {
        //if(firststep()) return true;
        Vector<Double> check2Num = new Vector<Double>();
        if (!first) {
            check2Num.add(0.0);
        }

        for (int z = 0; z < existmamopt.size(); z++) {
            if (existmamopt.elementAt(z) != 0.0) {
                check2Num.add(existmamopt.elementAt(z));
            }

            Vector<CheckVM> vecCheckVM = new Vector<CheckVM>();
            for (int i = 0; i < csm.size(); i++) {
                if (csm.elementAt(i).percent == -1) {
                    CheckVM cvm = new CheckVM(check2Num, i);
                    vecCheckVM.add(cvm);
                }

            }

            Vector<Vector<CheckSingleDNum>> test = generate(vecCheckVM);

            System.out.println("perestanovki count=" + test.size());
            /*   for (Vector<CheckSingleDNum> vector : test) {
             System.out.println();
             for (CheckSingleDNum checkSingleDNum : vector) {
             System.out.print(checkSingleDNum.checkNum + " [r "+checkSingleDNum.posInRootVector + " s "+checkSingleDNum.posInSubVector + "] ");
             }
             }
             */
            System.out.println();

            for (int i = 0; i < test.size(); i++) {
                double summam = 0;
                double ts = 0;
                int k = 0;
                for (int j = 0; j < csm.size(); j++) {
                    CheckSingleMam checkSingleMam = csm.elementAt(j);
                    double emo = 0;
                    boolean f = false;

                    if (checkSingleMam.getPercent() == -1) {
                        for (; k < test.elementAt(i).size() && !f; k++) {
                            if (test.elementAt(i).elementAt(k).getPosInRootVector() == j) {
                                emo = test.elementAt(i).elementAt(k).checkNum;
                                f = true;
                            }

                        }
                    }

                    if (pricewithmam) {
                        if (checkSingleMam.getPercent() == -1) {
                            ts = (checkSingleMam.getPrice() / (100 + emo)) * emo;
                            ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                        } else {
                            ts = (checkSingleMam.getPrice() / (100 + checkSingleMam.getPercent())) * checkSingleMam.getPercent();
                            ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                        }
                    } else {
                        //if price without mam
                        if (checkSingleMam.getPercent() == -1) {
                            ts = (checkSingleMam.getPrice() / (100)) * emo;
                            ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                        } else {
                            ts = (checkSingleMam.getPrice() / 100) * checkSingleMam.getPercent();
                            ts = Double.parseDouble(new DecimalFormat("##.##").format(ts));
                        }
                    }
                    summam = summam + ts;
                }

                for (Double double1 : doublenum) {
                    //System.out.println(summam+" vs "+double1);
                    //summam!=0 && double1>20 &&
                    if (summam != 0 && LFun.checktoler(summam, double1, tolMam)) {

                        if (LFun.checktoler(csm.lastElement().price, double1, tolMam)) {
                            needrefresh = true;
                            return false;
                        }
                        verifyNums(double1, test.elementAt(i));

                        /*  if (verifyNums(double1, test.elementAt(i)) == 2) {

                         System.out.println("summam calculated is not verified");
                         return false;

                         }
                         */
                    }
                }
            }
            if (existmamopt.elementAt(z) != 0.0 && check2Num.size() - 1 >= 0) {
                // check2Num.removeElementAt(1);
                check2Num.removeElementAt(check2Num.size() - 1);
            }
        }

        for (int i = 0; i < mamVerify.size() - 1; i++) {
            if (mamVerify.elementAt(i).equals(mamVerify.elementAt(i + 1))) {
                mamVerify.removeElementAt(i + 1);
                perVerify.removeElementAt(i + 1);
                i--;
            }
        }

        if (mamVerify.size() == 1) {
            sumallmamv.setSum(mamVerify.lastElement());
            sumallmamv.setVerifived(true);

            System.out.println("summam calculated is verified per rows");
            upd_bnv(perVerify.lastElement());
            return true;
        }

        return false;
    }

    public int verifyNums(double mam, Vector<CheckSingleDNum> per) {
        mamVerify.add(mam);
        perVerify.add(per);

        return mamVerify.size();
    }

    public Vector<Vector<CheckSingleDNum>> generate(Vector<CheckVM> sets) {
        int solutions = 1;
        Vector<Vector<CheckSingleDNum>> vec_csqty = new Vector<Vector<CheckSingleDNum>>();
        CheckSingleDNum csqty = null;
        for (int i = 0; i < sets.size(); solutions *= sets.elementAt(i).checkDNum.size(), i++);

        if (sets != null && sets.size() > 0 && sets.get(0).checkDNum != null && sets.get(0).checkDNum.size() == 2 && solutions >= 100000) {
            for (int stam = 0; stam < sets.size(); stam++) {
                Vector<CheckSingleDNum> rowvec = new Vector<CheckSingleDNum>();
                //vectot without mam and after this vector with mam:
                for (int i = 0; i < stam; i++) {
                    CheckVM set = sets.get(i);
                    try {
                        csqty = new CheckSingleDNum(set.checkDNum.elementAt(0),
                                set.posInRootVector,
                                0);
                        rowvec.add(csqty);
                    } catch (NumberFormatException e) {

                    }
                }
                for (int i = stam; i < sets.size(); i++) {
                    CheckVM set = sets.get(i);
                    try {
                        csqty = new CheckSingleDNum(set.checkDNum.elementAt(1),
                                set.posInRootVector,
                                1);
                        rowvec.add(csqty);
                    } catch (NumberFormatException e) {

                    }
                }
                vec_csqty.add(rowvec);
            }
            for (int stam = 0; stam < sets.size(); stam++) {
                Vector<CheckSingleDNum> rowvec = new Vector<CheckSingleDNum>();
                //vectot with mam and after this vector without mam:
                for (int i = 0; i < stam; i++) {
                    CheckVM set = sets.get(i);
                    try {
                        csqty = new CheckSingleDNum(set.checkDNum.elementAt(1),
                                set.posInRootVector,
                                1);
                        rowvec.add(csqty);
                    } catch (NumberFormatException e) {

                    }
                }
                for (int i = stam; i < sets.size(); i++) {
                    CheckVM set = sets.get(i);
                    try {
                        csqty = new CheckSingleDNum(set.checkDNum.elementAt(0),
                                set.posInRootVector,
                                0);
                        rowvec.add(csqty);
                    } catch (NumberFormatException e) {

                    }
                }
                vec_csqty.add(rowvec);
            }
        } else {
            for (int i = 0; i < solutions && i < 100000; i++) {
                int j = 1;
                Vector<CheckSingleDNum> rowvec = new Vector<CheckSingleDNum>();
                for (CheckVM set : sets) {
                    //  System.out.print(set.checkDNum.elementAt((i/j)%set.checkDNum.size()).get_result_str() + " ");
                    try {
                        csqty = new CheckSingleDNum(set.checkDNum.elementAt((i / j) % set.checkDNum.size()),
                                set.posInRootVector,
                                (i / j) % set.checkDNum.size());
                        rowvec.add(csqty);
                        j *= set.checkDNum.size();
                    } catch (NumberFormatException e) {

                    }

                }
                vec_csqty.add(rowvec);
                // System.out.println();
            }
        }

        return vec_csqty;
    }

}
