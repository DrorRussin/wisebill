/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Constructors;

import java.util.Date;

/**
 *
 * @author ERA-mobile
 */
public class VerVar {

    double sum = 0;
    boolean verifived = false;
    boolean cancel = false;

    public VerVar(double n) {
        sum = n;
    }

    @Override
    public String toString() {
        String s = "";
        if (cancel) {
            return "@";
        }
        if (!verifived) {
            s = "~";
        }
        return s + sum;
    }

    public void setCancel(boolean t) {
        cancel = t;
    }

    public double getSum() {
        return sum;
    }
    
    public boolean isCancel(){
        return cancel;
    }

    public boolean getVerifived() {
        return verifived;
    }

    public void setSum(double _sum) {
        sum = _sum;
        cancel = false;
    }

    public void setVerifived(boolean _verifived) {
        verifived = _verifived;
    }

}
