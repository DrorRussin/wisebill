package Constructors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import CSV.CSV2Hash;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ERA-mobile
 */
public class InfoReader extends CSV.Log {

    public String filename1;
    public String clientnumber2, datePictureWesTaken3, TimePictureWasTaken4, deviceType6, Year7, totalPriceNoTaxes8;
    public String totalTaxes10;
    public String totalTaxes_Hayav_Bemaam9, storeID11, ClubNumber21, CreditCard22,
            InvoiceTime15, CashID16, ClubMemberID17, CreditCardLastDigits18, InvoiceDate14, dateAndTimeOfPic, numberClient, firstIdOfDigitalSignature, secondIdOfDigitalSignature, documentClassification, SupplierName, SupplierID, SupplierDealerID = null;

    
    public String Invoice_ID12;
    public double totalInvoiceSum13 = 0;
    public Location GPS5;
    public String Path;
    public Vector<String> InfoVec;

    public InfoReader(String p, String TypeOfDocument) {
        InfoVec = new Vector<String>();
        Path = p;
        File file = new File(Path);
        if (file.exists()) {
            String string = readFileReader(file);
            int i = string.indexOf(",");
            string = string.substring(i + 1, string.length());

            StringTokenizer strToke = new StringTokenizer(string, ",");
            while (strToke.hasMoreTokens()) {
                InfoVec.add(strToke.nextToken().replaceAll("\r", "").replaceAll("\n", ""));
            }
            if (TypeOfDocument.equals("Bill")) {
                filename1 = InfoVec.elementAt(0);
                clientnumber2 = InfoVec.elementAt(1);
                datePictureWesTaken3 = InfoVec.elementAt(2);
                TimePictureWasTaken4 = InfoVec.elementAt(3);
                GPS5 = new Location(InfoVec.elementAt(4));
                deviceType6 = InfoVec.elementAt(5);
                Year7 = InfoVec.elementAt(6);
                totalPriceNoTaxes8 = InfoVec.elementAt(7);
                totalTaxes_Hayav_Bemaam9 = InfoVec.elementAt(8);
                totalTaxes10 = InfoVec.elementAt(9);
                storeID11 = InfoVec.elementAt(10);

                Invoice_ID12 = InfoVec.elementAt(11);

                if (!InfoVec.elementAt(12).equalsIgnoreCase("@")) {
                    try {
                        totalInvoiceSum13 = Double.valueOf(InfoVec.elementAt(12));
                    } catch (Exception e) {
                        write(filename1, this.getClass().toString(), e.getMessage(), "Cant read totalInvoiceSum13", true);

                    }
                }

                InvoiceDate14 = InfoVec.elementAt(13);

                InvoiceTime15 = InfoVec.elementAt(14);

                CashID16 = InfoVec.elementAt(15);

                ClubMemberID17 = InfoVec.elementAt(16);

                CreditCardLastDigits18 = InfoVec.elementAt(17);
            } else if (TypeOfDocument.equals("Invp")) {//A4!
                filename1 = InfoVec.elementAt(0);
                dateAndTimeOfPic = InfoVec.elementAt(1);
                numberClient = InfoVec.elementAt(2);
                firstIdOfDigitalSignature = InfoVec.elementAt(3);
                secondIdOfDigitalSignature = InfoVec.elementAt(4);
                documentClassification = InfoVec.elementAt(5);
                Year7 = InfoVec.elementAt(6);
                totalPriceNoTaxes8 = InfoVec.elementAt(7);
                totalTaxes_Hayav_Bemaam9 = InfoVec.elementAt(8);
                totalTaxes10 = InfoVec.elementAt(9);
                storeID11 = InfoVec.elementAt(10).replaceAll("\"", "-").replaceAll("/", "-");

                Invoice_ID12 = InfoVec.elementAt(11);
                if (!InfoVec.elementAt(12).equalsIgnoreCase("@")) {
                    try {
                        totalInvoiceSum13 = Double.valueOf(InfoVec.elementAt(12));
                    } catch (Exception e) {
                        write(filename1, this.getClass().toString(), e.getMessage(), "Cant read totalInvoiceSum13", true);

                    }
                }

                InvoiceDate14 = InfoVec.elementAt(13);

                SupplierName = InfoVec.elementAt(14);
                try {
                    SupplierID = CSV2Hash.SuppliersId2ErpId(InfoVec.elementAt(18));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (InfoVec.elementAt(19) != null && InfoVec.elementAt(19).length() > 2) {
                        SupplierDealerID = InfoVec.elementAt(19);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                if((SupplierDealerID!=null && SupplierDealerID.equals("מספר עוסק מורשה"))
                        ||(SupplierID!=null &&SupplierID.equals("מספר ספק ב-ERP"))
                        ||(SupplierName!=null &&SupplierName.equals("ישוב"))){//error in WiseRoute
                    SupplierDealerID = "@";
                    SupplierID = "@";
                    SupplierName = "@";
                    storeID11 = "@";
                }

            }
        } else {
            System.err.println("can't read" + file.getAbsolutePath());
        }
    }

    public static String readFileReader(File file) {
        String ret = "";
        try {
            FileInputStream fr = new FileInputStream(file);
            BufferedReader br = null;
            try {
                if (!"Windows-1255".equals("ANSI")) {
                    br = new BufferedReader(new InputStreamReader(fr, "Windows-1255"));
                } else {
                    br = new BufferedReader(new InputStreamReader(fr));
                }
            } catch (Exception e) {
            }
            String s = "";
            while ((s = br.readLine()) != null) {
                // System.out.println(s);
                ret = ret + s + "\n";
            }
        } catch (Exception r) {
        }
        return ret;
    }

    public static String readFile(File file) {
        String content = null;
        try {
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (Exception e) {
        }
        return content;
    }

    private class Location {

        private String Longitude, latitude;

        public Location(String str) {
            if (str.length() > 1) {
                int Separator = str.indexOf("-");
                latitude = str.substring(0, Separator);
                Longitude = str.substring(Separator + 1, str.length());
            } else {
                latitude = "";
                Longitude = "";
            }
        }

        public String getlatitude() {
            return latitude;
        }

        public String getLongitude() {
            return Longitude;
        }
    }
}
