/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constructors;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

/**
 *
 * @author user
 */
public class MoveFile {

        public static boolean Move(Vector<File> afile, String path) {
            boolean finish = false;
            for(File f : afile){
                finish = Move(f,path);
                
            }
            return finish;
        }
    
    
    public static boolean Move(File afile, String path) {
        InputStream inStream = null;
        OutputStream outStream = null;

        try {
            File bfile = new File(path,afile.getName());
            inStream = new FileInputStream(afile);
            outStream = new FileOutputStream(bfile);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            inStream.close();
            outStream.close();
            afile.delete();
            return true;
        } catch (IOException e) {}
        return false;
    }
}
