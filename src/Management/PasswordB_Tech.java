/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

import CSV.ConfigFun;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PasswordB_Tech extends JPanel implements ActionListener {

    static JFrame JFrameSconsole;
    private static String OK = "ok";
    private static String HELP = "help";
    private boolean fex = true;
    private JFrame controllingFrame; //needed for dialogs
    private JPasswordField passwordField;
    JFrame lastf = null;

    public PasswordB_Tech(JFrame lfr, JFrame f) {
        //Use the default FlowLayout.
        Color c = new Color(255, 255, 204);

        controllingFrame = f;
        lastf = lfr;
        lastf.setEnabled(false);
        //Create everything.
        passwordField = new JPasswordField(10);
        passwordField.setActionCommand(OK);
        passwordField.addActionListener(this);

        //JLabel label = new JLabel("Enter the password: ");
        JLabel label = new JLabel();

        label.setLabelFor(passwordField);
        label.setBackground(c);
        JComponent buttonPane = createButtonPanel();

        //Lay out everything.
        JPanel textPane = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        textPane.add(label);
        textPane.add(passwordField);
        textPane.setBackground(c);
        add(textPane);
        add(buttonPane);
        controllingFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }

        });
    }

    protected JComponent createButtonPanel() {
        JPanel p = new JPanel(new GridLayout(0, 1));
        JButton okButton = new JButton("OK");
        //  JButton helpButton = new JButton("Help");

        okButton.setActionCommand(OK);
        //  helpButton.setActionCommand(HELP);
        okButton.addActionListener(this);
        //   helpButton.addActionListener(this);

        p.add(okButton);
        //   p.add(helpButton);

        return p;
    }

    public void close() {
        this.controllingFrame.dispose();

    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {
        // TODO add your handling code here:
        if (fex) {
            lastf.setEnabled(true);
            lastf.toFront();
        }
    }

    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();

        if (OK.equals(cmd)) { //Process the password.
            char[] input = passwordField.getPassword();
            if (isPasswordCorrect(input)) {
                // JOptionPane.showMessageDialog(controllingFrame,
                //     "Success! You typed the right password.");
                JFrameSconsole.setState(Frame.ICONIFIED);
                SConsoleAdmin sca = new SConsoleAdmin(JFrameSconsole);
                sca.setVisible(true);

                fex = false;
                close();
            } else {
                JOptionPane.showMessageDialog(controllingFrame,
                        "Wrong password.",
                        "Error Message",
                        JOptionPane.ERROR_MESSAGE);
                fex = true;
                // close();
            }

            //Zero out the possible password, for security.
            for (int i = 0; i < input.length; i++) {
                input[i] = 0;
            }

            passwordField.selectAll();
            resetFocus();
        } else { //The user has asked for help.
            // JOptionPane.showMessageDialog(controllingFrame,
            //     "You can get the password by searching this example's\n"
            //   + "source code for the string \"correctPassword\".\n"
            //   + "Or look at the section How to Use Password Fields in\n"
            //   + "the components section of The Java Tutorial.");
        }
    }

    /**
     * Checks the passed-in array against the correct password. After this
     * method returns, you should invoke eraseArray on the passed-in array.
     */
    private static boolean isPasswordCorrect(char[] input) {
        boolean isCorrect = true;
        char[] correctPassword = {'e', 'r', 'a', 't', 'e', 'c', 'h'};
        char[] correctPassword_heb = {'ק', 'ר', 'ש', 'א', 'ק', 'ב', 'י'};
        int lanTry = 0;
        char[] current = correctPassword;
        while (lanTry < 2) {
            if (lanTry == 1) {
                current = correctPassword_heb;
                isCorrect = true;
            }
            if (input.length != current.length) {
                isCorrect = false;
            } else {
                for (int i = 0; i < input.length; i++) {
                    if (input[i] != current[i]) {
                        isCorrect = false;
                    }
                }
            }
            //Zero out the password.
            for (int i = 0; i < current.length; i++) {
                current[i] = 0;
            }

            if (isCorrect == true) {
                break;
            }
            lanTry++;

        }

        return isCorrect;
    }

    //Must be called from the event-dispatching thread.
    protected void resetFocus() {
        passwordField.requestFocusInWindow();
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI(JFrame lfr) {

        ImageIcon icon = new ImageIcon(ConfigFun.wiseBillFolderPath + "\\source\\images\\ERA-LOGO_110x55.png");

        //Create and set up the window.
        JFrame frame = new JFrame("PasswordBox");
        frame.setIconImage(icon.getImage());

        Point p = new Point(ConfigFun.calib_center(new Dimension(350, 80)));
        frame.setBounds(p.x, p.y, 350, 80);
        frame.setSize(100, 100);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        //Create and set up the content pane.
        final PasswordB_Tech newContentPane = new PasswordB_Tech(lfr, frame);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Make sure the focus goes to the right component
        //whenever the frame is initially given the focus.
        frame.addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent e) {
                newContentPane.resetFocus();
            }
        });

        //Display the window.
        frame.pack();
        frame.setVisible(true);

        //color
        Color c = new Color(255, 255, 204);

        Container con = frame.getContentPane();
        con.setBackground(c);
    }

    static public void run(JFrame _JFrameSconsole) {
        JFrameSconsole = _JFrameSconsole;
        createAndShowGUI(new JFrame());
        
    }

}
