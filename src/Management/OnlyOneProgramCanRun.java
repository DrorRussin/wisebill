/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Arrays;

import javax.swing.JOptionPane;
import java.lang.management.ManagementFactory;
import java.util.List;

import javax.swing.DefaultListModel;

/**
 *
 * @author zvika
 */
public class OnlyOneProgramCanRun {

    private static File f;
    private static FileChannel channel;
    private static FileLock lock;
    private static boolean wait = true;
    public static javax.swing.JFrame ActiveGui = null;
    private static String this_line = "";

    public static void Only1ProgrammCanRun() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    f = new File("RingOnRequest.lock");
                    // Check if the lock exist
                    if (f.exists()) {
                        // if exist try to delete it
                        f.delete();
                    }
                    // Try to get the lock
                    channel = new RandomAccessFile(f, "rw").getChannel();
                    lock = channel.tryLock();
                    if (lock == null) {

                        // File is lock by other application
                        channel.close();
                        //WISESIGN_GUI.AutoHideMSG.new_AutoHide("Only 1 instance of program can run.", true, false);
                        //JOptionPane.showMessageDialog(null, "Only 1 instance of program can run.");
                        System.err.println("Only 1 instance of program can run.");
                        System.exit(0);

                        throw new RuntimeException("Only 1 instance of program can run.");

                    } else {
                        wait = false;
                    }
                    // Add shutdown hook to release lock when application shutdown
                    ShutdownHook shutdownHook = new ShutdownHook();
                    Runtime.getRuntime().addShutdownHook(shutdownHook);

                    //Your application tasks here..
                    //System.out.println("Running");
                    Thread.sleep(1000); //10000
                } catch (Exception e) {
                    throw new RuntimeException("Could not start process.", e);
                }
            }
        });
        try {
            while (wait) {
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
        }
    }

    public static void unlockFile() {
// release and delete file lock
        try {
            if (lock != null) {
                lock.release();
            }
            channel.close();
            f.delete();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    static class ShutdownHook extends Thread {

        public void run() {
            unlockFile();
        }
    }
}
