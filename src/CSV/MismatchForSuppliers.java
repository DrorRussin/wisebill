package CSV;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author zvika
 */
public class MismatchForSuppliers extends CsvHandlerReader {

    public static MismatchForSuppliers single;
    private static final int orderedItemsCSV_Suppliers_ID = 13;
    private static final int SuppliersCSV_Suppliers_ID = 0;
    private static final int COLUMNS_SIZE = 1 + 5;
    private static final int SUPPLIER_ID = 0;

    private static final int ORDER_MISMATCH = 1;
    private static final int B2B_MISMATCH = 2;
    private static final int ENTRY_MISMATCH = 3;
    private static final int SHIPPING_MISMATCH = 4;
    private static final int TRANSACTION_MISMATCH = 5;

    public static final int ORDER_MISMATCH_COLUMNS = 7;
    public static final int B2B_MISMATCH_COLUMNS_1 = 76;
    public static final int B2B_MISMATCH_COLUMNS_2 = 77;
    public static final int ENTRY_MISMATCH_COLUMNS = 0;
    public static final int SHIPPING_MISMATCH_COLUMNS = 52;
    public static final int TRANSACTION_MISMATCH_COLUMNS = 79;

    private static final String Global_settings = "<Global Settings>";

    private HashMap<String, String[]> suppliersHashMap = new HashMap<String, String[]>();

    public MismatchForSuppliers(String csv_file_path) {
        super(csv_file_path, "UTF-8", "mismatch");
        initialization();
    }

    private void initialization() {
        //Create the header:
        {
            String[] header_arr = new String[COLUMNS_SIZE];
            String header = "";
            header_arr[SUPPLIER_ID] = "Suppliers ID";
            header_arr[B2B_MISMATCH] = "Show B2B_MISMATCH";
            header_arr[ORDER_MISMATCH] = "Show ORDER_MISMATCH";
            header_arr[ENTRY_MISMATCH] = "Show ENTRY_MISMATCH";
            header_arr[SHIPPING_MISMATCH] = "Show SHIPPING_MISMATCH";
            header_arr[TRANSACTION_MISMATCH] = "Show TRANSACTION_MISMATCH";

            for (int j = 0; j < header_arr.length - 1; j++) {
                header += header_arr[j] + ",";
            }
            header += header_arr[header_arr.length - 1];
            if (!vector.isEmpty()) {
                vector.remove(0);
            }
            vector.add(0, header);
        }
        CSV2Hash.readOrderedItemsCSV();
        CSV2Hash.readSuppliersCSV();
        vectorArrayToHashMap();
        findNewAndInitializationSuppliersHashMap(CSV2Hash.SuppliersCSV);
        this.saveChanges();
    }

    public void findNewAndInitializationSuppliersHashMap(Vector<String[]> SuppliersCSV) {
        for (int i = 1; i < SuppliersCSV.size(); i++) {
            String id = SuppliersCSV.get(i)[SuppliersCSV_Suppliers_ID];
            if (id != null && id.length() > 2 && !id.equalsIgnoreCase("999999999")) {
                String[] line;
                if (suppliersHashMap.containsKey(id)) {
                    line = suppliersHashMap.get(id);
                } else {
                    line = new String[COLUMNS_SIZE];
                    line[SUPPLIER_ID] = id;
                    initializationMismatchColumns(line);
                    suppliersHashMap.put(id, line);
                    vectorArray.add(line);
                }
                fillMismatchColumns(line);
            }
        }

        if (!suppliersHashMap.containsKey(Global_settings)) {
            String[] line = new String[COLUMNS_SIZE];
            for (int i = 0; i < line.length; i++) {
                line[i] = !CSV2Hash.orderedItemsCSV.isEmpty()+" ";
            }
            line[SUPPLIER_ID] = Global_settings;
            suppliersHashMap.put(line[SUPPLIER_ID], line);
            vectorArray.add(0, line);
        }
    }

    public void vectorArrayToHashMap() {
        for (String[] line : vectorArray) {
            suppliersHashMap.put(line[SUPPLIER_ID], line);
        }
    }

    private void initializationMismatchColumns(String[] sup) {
        sup[ORDER_MISMATCH] = "false";
        sup[B2B_MISMATCH] = "false";
        sup[ENTRY_MISMATCH] = "false";
        sup[SHIPPING_MISMATCH] = "false";
        sup[TRANSACTION_MISMATCH] = "false";
    }

    private void fillMismatchColumns(String[] sup) {
        for (int i = 0; i < CSV2Hash.orderedItemsCSV.size(); i++) {
            //is same ID:
            if (CSV2Hash.orderedItemsCSV.get(i)[orderedItemsCSV_Suppliers_ID].equalsIgnoreCase(sup[SUPPLIER_ID])) {
                if (CSV2Hash.orderedItemsCSV.get(i)[ORDER_MISMATCH_COLUMNS].length() > 1) {
                    sup[ORDER_MISMATCH] = "true";
                }
                if (CSV2Hash.orderedItemsCSV.get(i)[B2B_MISMATCH_COLUMNS_1].length() > 1
                        || CSV2Hash.orderedItemsCSV.get(i)[B2B_MISMATCH_COLUMNS_2].length() > 1) {
                    sup[B2B_MISMATCH] = "true";
                }
                if (CSV2Hash.orderedItemsCSV.get(i)[ENTRY_MISMATCH_COLUMNS].length() > 1) {
                    sup[ENTRY_MISMATCH] = "true";
                }
                if (CSV2Hash.orderedItemsCSV.get(i)[SHIPPING_MISMATCH_COLUMNS].length() > 1) {
                    sup[SHIPPING_MISMATCH] = "true";
                }
                if (CSV2Hash.orderedItemsCSV.get(i)[TRANSACTION_MISMATCH_COLUMNS].length() > 1) {
                    sup[TRANSACTION_MISMATCH] = "true";
                }
            }
        }
    }

    public boolean isMismatch(String mismatch_name, String supplier_id) {
        String[] line_supplier_id = null;
        String[] line_Global_settings = suppliersHashMap.get(Global_settings);
        if (suppliersHashMap.containsKey(supplier_id)) {
            line_supplier_id = suppliersHashMap.get(supplier_id);
        }

        switch (mismatch_name.toUpperCase()) {
            case "ORDER_MISMATCH":
                return line_Global_settings[ORDER_MISMATCH].equalsIgnoreCase("true")
                        && (line_supplier_id == null || (line_supplier_id != null &&line_supplier_id[ORDER_MISMATCH].equalsIgnoreCase("true")));
            case "B2B_MISMATCH":
                return line_Global_settings[B2B_MISMATCH].equalsIgnoreCase("true")
                        && (line_supplier_id == null || (line_supplier_id != null &&line_supplier_id[B2B_MISMATCH].equalsIgnoreCase("true")));
            case "ENTRY_MISMATCH":
                return line_Global_settings[ENTRY_MISMATCH].equalsIgnoreCase("true")
                        && (line_supplier_id == null || (line_supplier_id != null &&line_supplier_id[ENTRY_MISMATCH].equalsIgnoreCase("true")));
            case "SHIPPING_MISMATCH":
                return line_Global_settings[SHIPPING_MISMATCH].equalsIgnoreCase("true")
                        && (line_supplier_id == null || (line_supplier_id != null &&line_supplier_id[SHIPPING_MISMATCH].equalsIgnoreCase("true")));
            case "TRANSACTION_MISMATCH":
                return line_Global_settings[TRANSACTION_MISMATCH].equalsIgnoreCase("true")
                        && (line_supplier_id == null || (line_supplier_id != null &&line_supplier_id[TRANSACTION_MISMATCH].equalsIgnoreCase("true")));
        }

        throw new RuntimeException("Error in isMismatch()");

    }
}
