/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import static CSV.Log.write;
import Constructors.Char2Dig;
import Check.CheckSingleQTY;
import Check.CheckQTY;
import Constructors.InfoReader;
import Constructors.ObData;
import Management.SConsoleAdmin;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import static CSV.Log.writeWiserunLog;

/**
 *
 *
 */
public class LFun {

    /**
     *
     * @param a character
     * @return number by asci range
     */
    static Vector<ObData> prq = null;
    static String rightside = "";
    static double firstNum = 0;
    static double secNum = 0;
    static boolean isReverse = false;

    public static boolean ifmostthanNcharsperrows(Vector<ObData> od, int n) {
        int line = od.elementAt(0).get_line_num();
        int stind = 0;
        int endind = 0;
        for (int i = 0; i < od.size() - 1; i++) {//counr < result.size()
            if (od.elementAt(i).get_line_num() > line) {

                endind = i;
                System.out.println();
                int alldist = Math.abs(od.elementAt(stind).get_right() - od.elementAt(endind - 1).get_left());
                int sumdist = 0;
                int sumchar = 0;

                for (int j = stind; j < endind; j++) {
                    sumdist = sumdist + Math.abs(od.elementAt(j).get_right() - od.elementAt(j).get_left());
                    sumchar = sumchar + od.elementAt(j).get_result_str().length();
                    System.out.println(od.elementAt(j).get_result_str());
                }
                if (sumdist == 0) {
                    sumdist = 1;
                }
                if (sumchar == 0) {
                    sumchar = 1;
                }

                int ans = (int) alldist / (sumdist / sumchar);
                System.out.println("Row: " + line + " chars= " + ans);
                // if(ans>n) return true;

                line = od.elementAt(i).get_line_num();
                stind = i;

            }
        }

        return false;
    }

    public static Vector copyVec(Vector v, int from, int to) {
        Vector newVector = new Vector();
        for (; from < to; from++) {
            newVector.add(v.get(from));
        }
        return newVector;
    }

    public static boolean ifmostthanNcharsperpage(Vector<ObData> od, int n, int alldist) {
        int line = 0;

        int sumdist = 0;
        int sumchar = 0;
        for (int i = 0; i < od.size() - 1; i++) {//counr < result.size()
            sumdist = sumdist + Math.abs(od.elementAt(i).get_right() - od.elementAt(i).get_left());
            sumchar = sumchar + od.elementAt(i).get_result_str().length();
            line = od.elementAt(i).get_line_num();
        }
        int ans = 0;
        try {
            ans = (int) ((alldist) / (sumdist / sumchar));
        } catch (Exception e) {

        }

        System.out.println("Row: " + line + " chars= " + ans);
        if (ans > n) {
            return true;
        }

        return false;
    }

    public static Vector<Vector<CheckSingleQTY>> generate(Vector<CheckQTY> sets) {
        int solutions = 1;
        Vector<Vector<CheckSingleQTY>> vec_csqty = new Vector<Vector<CheckSingleQTY>>();
        CheckSingleQTY csqty = null;
        for (int i = 0; i < sets.size(); solutions *= sets.elementAt(i).checkIntNum.size(), i++);
        for (int i = 0; i < solutions; i++) {
            int j = 1;
            Vector<CheckSingleQTY> rowvec = new Vector<CheckSingleQTY>();
            for (CheckQTY set : sets) {
                //  System.out.print(set.checkIntNum.elementAt((i/j)%set.checkIntNum.size()).get_result_str() + " ");
                try {
                    csqty = new CheckSingleQTY(Integer.parseInt(set.checkIntNum.elementAt((i / j) % set.checkIntNum.size()).get_result_str()),
                            set.posInRootVector,
                            (i / j) % set.checkIntNum.size());
                    rowvec.add(csqty);
                    j *= set.checkIntNum.size();
                } catch (NumberFormatException e) {

                }

            }
            vec_csqty.add(rowvec);
            // System.out.println();
        }
        return vec_csqty;
    }

    static Vector<ObData> fcandscbybottom(Vector<ObData> fv, Vector<ObData> sv) {
        Vector<ObData> t = new Vector<ObData>();
        if (sv.size() == 0) {
            return fv;
        }

        //int size=Math.max(fv.size(),sv.size());
        int size = fv.size() + sv.size();
        for (int i = 0, j = 0; (i + j) < size;) {

            if (i == fv.size()) {
                t.add(sv.elementAt(j++));
            } else if (j == sv.size()) {
                t.add(fv.elementAt(i++));
            } else {
                if (fv.elementAt(i).get_bottom() < sv.elementAt(j).get_bottom()) {
                    t.add(fv.elementAt(i++));
                } else if (sv.elementAt(j).get_bottom() < fv.elementAt(i).get_bottom()) {
                    t.add(sv.elementAt(j++));
                } else if (sv.elementAt(j).get_bottom() == fv.elementAt(i).get_bottom()) { //&& sv.elementAt(j).get_left() != fv.elementAt(j).get_left()
                    t.add(sv.elementAt(j++));
                    i++;

                }
            }
        }
        return t;

    }

    /**
     * remove "=" from price and change format
     *
     * @param o ObData
     * @return
     */
    static ObData removeequalfromprice(ObData o, Vector<String> vtypes) {
        String t = o.get_result_str();
        t = t.replaceAll("=", "");
        String nf = LFun.checkFormat(t);
        if (LFun.contain_type(vtypes, nf)) {
            o.set_result_str(t);
            o.set_format(nf);
        }
        return o;
    }

    /*
     * replace character to digit by table
     */
    public static String repChar2Dig(String str) {
        ArrayList<Char2Dig> repchars = new ArrayList<Char2Dig>();
        repchars.add(new Char2Dig("B", "8"));
        repchars.add(new Char2Dig("s", "5"));
        repchars.add(new Char2Dig("S", "5"));
        for (Char2Dig char2Dig : repchars) {

            str = str.replaceAll(char2Dig.charval, char2Dig.digval);

        }
        return str;
    }

    /*
     * return string by regex
     */
    public static String getStrbyRegex(String str, String reg) {

        Pattern pattern = Pattern.compile(reg);
        rightside = str;

//System.out.println("to match: "+str);
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            //System.out.println("match1: "+matcher.group(1));

            rightside = rightside.replaceFirst(matcher.group(1), "");
            str = matcher.group(1);

        }

        return str;
    }

    /**
     * cut price regex from general and change format
     *
     * @param o ObData
     * @return
     */
    static ObData cutpriceregexfromgeneral(ObData o, Vector<String> vtypes, String reg) {
        ObData obd = null;
        String t = o.get_result_str();
        rightside = "";

        int told = t.length();

        t = getStrbyRegex(t, reg);

        int tnew = t.length();
        int dist = o.get_right() - o.get_left();
        int curright = o.get_right();

        if (o.get_position().equalsIgnoreCase("LP")) {
            o.set_right((int) (o.get_left() + (dist / told) * tnew));
        } else if (o.get_position().equalsIgnoreCase("RP")) {
            o.set_left((int) (o.get_right() - (dist / told) * tnew));
        }

        if ((tnew - told) == 0) {

            return null;
        }

        if (t.equalsIgnoreCase("")) {
            o.set_result_str("null");
            return o;
        }

        String nf = LFun.checkFormat(t);
        //if(LFun.contain_type(vtypes,nf)){

        o.set_result_str(t);
        o.set_format(nf);

        if (!rightside.equalsIgnoreCase("")) {
            System.out.println("for next object : " + rightside);

            obd = new ObData(o);
            obd.set_result_str(rightside);
            obd.set_left(o.get_right() + 1);
            obd.set_right(curright);
            nf = LFun.checkFormat(rightside);
            obd.set_format(nf);
            obd.set_position("u");

        }
        //}

        return obd;
    }

    /**
     * remove by regex from price and change format
     *
     * @param o ObData
     * @return
     */
    static ObData removenoisebyregexfromprice(ObData o, Vector<String> vtypes, String reg) {
        //[A-z~`'\"•;=!?:״׳י]+

        String t = o.get_result_str();
        int told = t.length();
        // System.out.println("to remove noise: "+t);
        t = t.replaceAll(reg, "");
        // System.out.println("after remove noise: "+t);        
        int tnew = t.length();
        int dist = o.get_right() - o.get_left();

        if (o.get_position().equalsIgnoreCase("LP")) {
            o.set_right((int) (o.get_left() + (dist / told) * tnew));
        } else if (o.get_position().equalsIgnoreCase("RP")) {
            o.set_left((int) (o.get_right() - (dist / told) * tnew));
        }

        if (t.equalsIgnoreCase("")) {
            o.set_result_str("null");
            return o;
        }

        o.set_format(checkFormat(t));
        o.set_result_str(t);

        //System.out.println("after remove noise: "+o.get_result_str());           
        return o;
    }

    /**
     * remove "'" from price and change format
     *
     * @param o ObData
     * @return
     */
    static ObData removegereshfromprice(ObData o, Vector<String> vtypes) {
        String t = o.get_result_str();
        int told = t.length();
        t = t.replaceAll("׳", "");
        int tnew = t.length();
        int dist = o.get_right() - o.get_left();

        if (o.get_position().equalsIgnoreCase("LP")) {
            o.set_right((int) (o.get_left() + (dist / told) * tnew));
        } else if (o.get_position().equalsIgnoreCase("RP")) {
            o.set_left((int) (o.get_right() - (dist / told) * tnew));
        }
        String nf = LFun.checkFormat(t);
        if (LFun.contain_type(vtypes, nf)) {
            o.set_result_str(t);
            o.set_format(nf);
        }
        return o;
    }

    /**
     * remove """ from price and change format
     *
     * @param o ObData
     * @return
     */
    static ObData removegershaimfromprice(ObData o, Vector<String> vtypes) {
        String t = o.get_result_str();

        t = t.replaceAll("\"", "");

        String nf = LFun.checkFormat(t);
        if (LFun.contain_type(vtypes, nf)) {
            o.set_result_str(t);
            o.set_format(nf);
        }
        return o;
    }

    static public int asci_range(char a) {
        if ((int) a <= 57 && (int) a >= 48) {
            return 1;  //number range
        } else if (a == '.') {
            return 2; // 
        } else if (a == ',') {
            return 2;
        }
        return 0;
    }

    /**
     *
     * @param a character
     * @return asci code
     */
    public static int getasci(char a) {
        return (int) a;
    }

    /**
     *
     * @param i asci code
     * @return character
     */
    static char getchar(int i) {
        return (char) i;
    }

    /**
     * check if found contain x,*,
     *
     * @
     * @param str for search
     * @return true if type exist in vector
     */
    static boolean contain_mul(String str) {
        Vector<String> v = new Vector<String>();
        v.add("x");
        v.add("*");
        v.add("@");

        for (int i = 0; i < v.size(); i++) {
            if (v.elementAt(i).equalsIgnoreCase(str)) {
                return true;
            }

        }
        return false;
    }

    static boolean iscontain_s(Vector<ObData> line_n, String s) {
        for (int i = 0; i < line_n.size(); i++) {
            if (line_n.elementAt(i).get_name_by_inv().contains(s)) {
                return true;
            }

        }

        return false;
    }

    /**
     * check if contain minimum 2 numbers in one row
     *
     * @param line_n
     * @return
     */
    static boolean iscontain2num(Vector<ObData> line_n) {
        String s = "num";
        int count = 0;
        for (int i = 0; i < line_n.size(); i++) {
            if (line_n.elementAt(i).get_name_by_inv().contains(s)) {
                count++;

                if (count == 2) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * check if contain : price or pn or percent or qty or one price - in line
     *
     * @param line_n
     * @return
     */
    static boolean iscontaindata(Vector<ObData> line_n) {

        for (int i = 0; i < line_n.size(); i++) {
            if (line_n.elementAt(i).get_name_by_inv().contains("price")
                    || line_n.elementAt(i).get_name_by_inv().contains("pn")
                    || line_n.elementAt(i).get_name_by_inv().contains("percent")
                    || line_n.elementAt(i).get_name_by_inv().contains("qty")
                    || line_n.elementAt(i).get_name_by_inv().contains("one")) {
                return true;
            }
        }

        return false;
    }

    static int checkline(double price, Vector<ObData> line_n, double rate) {
        /*  double tol_pr=0.5;
         Vector<String> vnum=new Vector<String>();
         vnum.add("USADD");
         vnum.add("EURDD"); 
       
         vnum.add("USADDN");
         vnum.add("EURDDN"); 
         vnum.add("INTP");
         */
        // vnum.add("INTPSEP");

        isReverse = false;
        Vector<ObData> line = new Vector<ObData>(line_n);

        if (CSV2Hash.isA4) {
            for (int i = 0; i < line.size(); i++) {
                line.get(i).set_result_str(line.get(i).get_result_str().replaceAll(",", ""));
            }
        } else {
            for (int i = 0; i < line.size(); i++) {
                line.get(i).set_result_str(line.get(i).get_result_str().replaceAll(",", "."));
            }
        }

        for (int i = 0; i < line.size(); i++) {
            //System.out.println(i+": "+line.elementAt(i).get_result_str()+"["+line.elementAt(i).get_name_by_inv()+"]");
            if (!line.elementAt(i).get_name_by_inv().equalsIgnoreCase("num") && !line.elementAt(i).get_name_by_inv().equalsIgnoreCase("pn") && !line.elementAt(i).get_name_by_inv().equalsIgnoreCase("qty")) {
                line.removeElementAt(i);
                i--;
            } else if (line.elementAt(i).get_format().equalsIgnoreCase("INTPSEP")) {
                line.removeElementAt(i);
                i--;
            }

        }

        prq = new Vector<ObData>();

        if (line.size() == 2) {
            try {
                double m = Double.valueOf(fixdot(line.elementAt(0).get_result_str())) * Double.valueOf(fixdot(line.elementAt(1).get_result_str()));
                if (checktoler(m, price) || checktoler(m / 1000, price)) {

                    prq.add(line.elementAt(0));
                    prq.add(line.elementAt(1));
                    return 1;
                }
            } catch (Exception e) {

            }
        } else if (line.size() > 2) {

            for (int i = 0; i < line.size() - 1; i++) {

                for (int j = i + 1; j < line.size(); j++) {
                    double a = 0,
                            b = 0,
                            m = 0;
                    try {
                        a = Double.valueOf(fixdot(line.elementAt(i).get_result_str()));
                    } catch (Exception e) {

                    }
                    try {
                        b = Double.valueOf(fixdot(line.elementAt(j).get_result_str()));
                    } catch (Exception e) {

                    }
                    m = a * b;
                    if (checktoler(m, price)) {
                        prq.add(line.elementAt(i));
                        prq.add(line.elementAt(j));
                        return 1;
                    } else if (checktoler(m * rate, price)) {
                        prq.add(line.elementAt(i));
                        prq.add(line.elementAt(j));
                        return 1;
                    }

                }

            }

            //second find for price*1000
            for (int i = 0; i < line.size() - 1; i++) {

                for (int j = i + 1; j < line.size(); j++) {

                    try {
                        double a = Double.valueOf(fixdot(line.elementAt(i).get_result_str())),
                                b = Double.valueOf(fixdot(line.elementAt(j).get_result_str())),
                                m = a * b;

                        if (checktoler(m / 1000, price)) {

                            prq.add(line.elementAt(i));
                            prq.add(line.elementAt(j));
                            return 1;
                        }
                    } catch (Exception r) {
                    }
                }

            }
        }

        firstNum = 0;
        secNum = 0;
        double res = 0;
        for (ObData ob : line) {
            if (ob.get_result_str().length() <= 6) {
                if (ob.get_result_str().length() == 6) {
                    try {
                        firstNum = Double.valueOf(ob.get_result_str().substring(0, 2));
                        secNum = Double.valueOf(ob.get_result_str().substring(2));
                        res = firstNum * secNum;
                        if (checktoler(price, res)) {
                            prq.add(ob);
                            return 1;
                        }
                        firstNum = Double.valueOf(ob.get_result_str().substring(0, 1));
                        secNum = Double.valueOf(ob.get_result_str().substring(1));
                        res = firstNum * secNum;
                        if (checktoler(price, res)) {
                            prq.add(ob);
                            return 1;
                        }

                    } catch (Exception e) {

                    }
                } else if (ob.get_result_str().length() == 5) {
                    try {
                        firstNum = Double.valueOf(ob.get_result_str().substring(0, 1));
                        secNum = Double.valueOf(ob.get_result_str().substring(1));
                        res = firstNum * secNum;
                        if (checktoler(price, res)) {
                            prq.add(ob);
                            return 1;
                        }
                    } catch (Exception e) {

                    }
                }
            }
        }

        if (line.size() > 1) {
            double a = 0;
            double b = 0;
            double m = 0;

            Vector<String> afterReverse = new Vector<String>();
            for (int i = 0; i < line.size(); i++) {
                if (line.elementAt(i).get_result_str().length() > 2) {
                    afterReverse.add(reverseString(line.elementAt(i).get_result_str()));
                }
            }
            for (int i = 0; i < line.size(); i++) {
                for (int j = i + 1; j < afterReverse.size(); j++) {
                    try {
                        a = Double.valueOf(line.get(i).get_result_str());
                        b = Double.valueOf(afterReverse.get(j));
                        m = a * b;
                        if (checktoler(price, m)) {
                            isReverse = true;
                            prq.add(line.get(i));
                            prq.add(line.get(j));
                            return 1;
                        }
                    } catch (Exception e) {

                    }

                }

            }
        }

        /* for (int i = 0; i < line.size(); i++) {
         System.out.print(" \t"+line.elementAt(i).get_result_str());
         
         }
         */
        //need add check by file list
        return 0;
    }

    public static String reverseString(String source) {
        int i, len = source.length();
        StringBuilder dest = new StringBuilder(len);

        for (i = (len - 1); i >= 0; i--) {
            dest.append(source.charAt(i));
        }

        return dest.toString();
    }

    static public boolean checktoler(double p1, double p2, double tol) {
        double t = Math.abs(Math.abs(Math.max(p1, p2) / Math.min(p1, p2) - 1));
        if (t <= tol && ((p1 < 0 && p2 < 0) || (p1 > 0 && p2 > 0))) {
            return true;
        }
        return false;
        /*double t = Math.abs(Math.abs(Math.max(p1, p2) / Math.min(p1, p2)) - 1);
         if (t <= tol) {
         return true;
         }
         return false;*/
    }

    static public boolean checktoler(double p1, double p2) {
        double t = Math.abs(Math.abs(Math.max(p1, p2) / Math.min(p1, p2)) - 1);
        if (t <= 0.01) {
            return true;
        }
        return false;
    }

    static public double getpossub(double p1, double p2) {
        double t = Math.abs(Math.abs(p1) - Math.abs(p2));

        return t;
    }

    /**
     * change dot to comma in string if N.NNN.NNN.NN
     *
     * @param s
     * @return
     */
    static String fix2dot(String s) {
        String regex;
        Pattern p;
        Matcher m;
        int end = s.length() - 1;

        regex = "[0-9]{1,3}([\\.\\,]{1}[0-9]{3})+\\.[0-9]{2}";      //NNN.NNN.NNN.NN
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            String t = s.substring(0, s.length() - 4);
            t = t.replace('.', ',');

            t = t + s.substring(s.length() - 4);

            return t;
        }

        regex = "[0-9]{1,3}([\\.\\,]{1}[0-9]{3})+\\,[0-9]{2}";      //NNN,NNN,NNN,NN
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            String t = s.substring(0, s.length() - 4);
            t = t.replace(',', '.');

            t = t + s.substring(s.length() - 4);

            return t;
        }

        return s;
    }

    /**
     * 0.001 -> 0.0 2.00002 ->2.00 75 -> 75.00
     *
     * @param s
     * @return
     */
    static String doubleValueToPrint(String s) {
        try {
            double d = Double.valueOf(s);
            s = ((int) (d * 100)) + "";
            if (s.equals("0")) {
                return "0.00";
            } else {
                return s.substring(0, s.length() - 2) + "." + s.substring(s.length() - 2);
            }
        } catch (Exception e) {
        }
        return s;
    }

    /**
     * change comma to dot in string
     *
     * @param s
     * @return
     */
    static String fixdot(String s) {

        try {
            try {
                s = s.replace("^", "").replace("^", "");//replaceAll doesn't work here
            } catch (Exception e) {
            }
            Double.valueOf(s);
            if (s.contains(".")) {
                if (s.charAt(s.length() - 1) == '.') {
                    s = s + "00";
                } else if (s.charAt(0) == '.') {
                    s = "0" + s;
                }
            }
            if (s.contains(",") && CSV2Hash.isA4) {
                s = s.replace(",", "");
            }

            if (s.contains("00") && !s.contains(".")) {
                s = s.substring(0, s.indexOf("00")) + ".00";
            }
        } catch (Exception r) {
        }

        if (s.contains(",")) {
            if (s.contains(".")) {
                s = s.replace(",", "");
            } else {
                s = s.replace(",", ".");
            }
        }

        if (!s.contains(".")) {
            s = s + ".00";
        }

        if (s.startsWith(".") || s.startsWith(",")) {
            if (s.substring(1).contains(".") || s.substring(1).contains(",")) {
                s = s.substring(1);
            }
        }

        String regex;
        Pattern p;
        Matcher m;

        regex = "(([-]?[0-9]*)(\\.)([0-9]+))"; // one .
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return s;
        }

        regex = "(([-]?[0-9]*)(\\,)([0-9]+))";//one ,
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            s = s.replace(',', '.');
            return s;
        }

        regex = "[0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{1,}";      //NNN,NNN,NNN.N
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            s = s.replaceAll(",", "");

            return s;
        }

        regex = "[0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{1,}";      //NNN.NNN.NNN,N
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            s = s.replaceAll(".", "");
            s = s.replace(',', '.');
            return s;
        }

        return s;
    }

    /**
     * if in block found price
     *
     * @param start block
     * @param end block
     * @param hash
     * @return boolean
     */
    static boolean iscontain_price(int start, int end, CSV2Hash hash) {
        for (int i = start; i <= end && i < hash.lines.size(); i++) {
            for (int j = 0; j < hash.lines.elementAt(i).size(); j++) {
                if (hash.lines.elementAt(i).elementAt(j).get_name_by_inv().contains("price")) {
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * if in block found price
     *
     * @param start block
     * @param end block
     * @param hash
     * @return boolean
     */
    static void parse_block(int start, int end, CSV2Hash hash) {

        for (int i = start; i <= end && i < hash.lines.size(); i++) {
            Vector<String> t = new Vector<String>();
            Vector<Integer> it = new Vector<Integer>();
            for (int j = 0; j < hash.lines.elementAt(i).size(); j++) {
                if (!hash.lines.elementAt(i).elementAt(j).get_format().equalsIgnoreCase("INTPSEP") && (hash.lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("num") || hash.lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn"))) {
                    t.add(hash.lines.elementAt(i).elementAt(j).get_result_str());
                    it.add(j);
                }
            }

            if (t.size() > 1) {
                if (t.size() == 2) {
                    double tsum = Double.valueOf(fixdot(hash.lines.elementAt(i).elementAt(it.elementAt(0)).get_result_str())) * Double.valueOf(fixdot(hash.lines.elementAt(i).elementAt(it.elementAt(1)).get_result_str()));
                    if (LFun.update_price_block_by_qty_oneprice(start, end, hash, tsum)) {
                        hash.lines.elementAt(i).elementAt(it.elementAt(0)).set_name_by_inv(LFun.qtyorpart(hash.lines.elementAt(i).elementAt(it.elementAt(0)).get_result_str()));
                        hash.lines.elementAt(i).elementAt(it.elementAt(1)).set_name_by_inv(LFun.qtyorpart(hash.lines.elementAt(i).elementAt(it.elementAt(1)).get_result_str()));
                    }

                } else if (t.size() > 2) {
                    for (int j = 0; j < t.size() - 1; j++) {
                        double tsum = Double.valueOf(fixdot(hash.lines.elementAt(i).elementAt(it.elementAt(j)).get_result_str())) * Double.valueOf(fixdot(hash.lines.elementAt(i).elementAt(it.elementAt(j + 1)).get_result_str()));
                        if (LFun.update_price_block_by_qty_oneprice(start, end, hash, tsum)) {
                            hash.lines.elementAt(i).elementAt(it.elementAt(j)).set_name_by_inv(LFun.qtyorpart(hash.lines.elementAt(i).elementAt(it.elementAt(j)).get_result_str()));
                            hash.lines.elementAt(i).elementAt(it.elementAt(j + 1)).set_name_by_inv(LFun.qtyorpart(hash.lines.elementAt(i).elementAt(it.elementAt(j + 1)).get_result_str()));
                        }

                    }
                }
            }
        }

    }

    /**
     * update found price in block
     *
     * @param start
     * @param end
     * @param hash
     * @param sum
     * @param f
     * @param s return boolean is found
     */
    static boolean update_price_block_by_qty_oneprice(int start, int end, CSV2Hash hash, double sum) {
        for (int i = start; i <= end && i < hash.lines.size(); i++) {
            for (int j = 0; j < hash.lines.elementAt(i).size(); j++) {
                if ((hash.lines.elementAt(i).elementAt(j).get_name_by_inv().contains("num")
                        || hash.lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn")) && !hash.lines.elementAt(i).elementAt(j).get_format().equalsIgnoreCase("INTPSEP")) {
                    double m = Double.valueOf(fixdot(hash.lines.elementAt(i).elementAt(j).get_result_str()));
                    if ((Math.abs(m) > sum - 0.1 && Math.abs(m) < sum + 0.1) || (Math.abs(m) * 1000 > sum - 0.1 && Math.abs(m) * 1000 < sum + 0.1)) {
                        if (m > 0) {
                            hash.lines.elementAt(i).elementAt(j).set_name_by_inv("pos_price");
                        } else {
                            hash.lines.elementAt(i).elementAt(j).set_name_by_inv("neg_price");
                        }
                        return true;
                    }

                }
            }

        }
        return false;
    }

    /**
     * return true if string contain digit
     *
     * @param s
     * @return
     */
    static boolean iscontain_num(String s) {

        for (int i = 0; i < s.length(); i++) {

            if (LFun.asci_range(s.charAt(i)) == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * check if vector of types contain current type
     *
     * @param v vector of types
     * @param type for search
     * @return true if type exist in vector
     */
    static boolean contain_type(Vector<String> v, String type) {
        for (int i = 0; i < v.size(); i++) {
            if (v.elementAt(i).equalsIgnoreCase(type)) {
                return true;
            }

        }
        return false;
    }

    static String removeNoiseFromString(String toClean) {
        String res = toClean;
        res = res.replaceAll("[~`׳'\\\"•;!=?:״׳י\\s׳]", "");
        return res;
    }

    static String removeCharCantBeSaved(String toClean) {
        String res = toClean;
        res = res.replaceAll("[\\/:*?\"<>|]", "");
        return res;
    }

    /**
     * check if number price of part or qty
     *
     * @param s
     * @return
     */
    static String qtyorpart(String s) {
        String regex;
        //if(!CSV2Hash.isA4){
        regex = "([-]?[0-9]+[.,]{1}[0-9]{2})"; //00.00 price
        //}
        //else{
        // regex = "([-]?[0-9]+[.,]{1}[0-9]{2,3})"; //00.00 or 00.000 price
        //}

        regex = "^" + regex + "$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(s);

        if (m.matches()) {
            return "one_pr";//NEGATIVE INTEGER";
        }
        return "qty";
    }

    /**
     * check if number price of part or qty
     *
     * @param s
     * @return
     */
    static boolean twoafterdot(String s) {
        String regex = "([-]?[0-9.,]+[.,]{1}[0-9]{2})";  //00.00 price
        regex = "^" + regex + "$";

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(s);

        if (m.matches()) {
            return true;
        }

        return false;
    }

    /**
     * check if number partnumber integer min 3 digits
     *
     * @param s
     * @return
     */
    static boolean ispn(ObData o) {
        String lenpn = CSV2Hash.minlpn;
        String s = o.get_result_str();
        if ((!o.get_position().equalsIgnoreCase("LP") && !o.get_position().equalsIgnoreCase("RP")) && !o.get_position().equalsIgnoreCase("MP")) {
            String regex = "([0-9]{" + lenpn + ",})";  //
            regex = "^" + regex + "$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(s);

            if (m.matches()) {
                return true;
            }

            return false;

            //return false;
        } else {
            String regex = "([0-9]{" + lenpn + ",})";  //
            regex = "^" + regex + "$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(s);

            if (m.matches()) {
                return true;
            }

            return false;
        }
    }

    static boolean isDouble(String num) {
        boolean isDouble = false;
        try {
            double a = Double.valueOf(num);
            isDouble = true;
        } catch (Exception e) {
            return isDouble;
        }
        return isDouble;
    }

    static Vector reverseVector(Vector V) {
        Vector newVector = new Vector();
        if (V != null && !V.isEmpty()) {
            for (int i = V.size() - 1; i >= 0; i--) {
                newVector.add(V.get(i));
            }
        }

        return newVector;
    }

    static boolean isContainMorethanTwoDigits(String word) {
        if (word == null) {
            return false;
        }
        char[] c = word.toCharArray();
        int moneDigit = 0;
        for (int i = 0; i < c.length; i++) {
            if (Character.isDigit(c[i])) {
                moneDigit++;
            }
        }
        return moneDigit >= 2;
    }

    static boolean isContainDigit(String word) {
        if (word == null) {
            return false;
        }
        char[] c = word.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (Character.isDigit(c[i])) {
                return true;
            }
        }
        return false;
    }

    static boolean isCanBePnInA4(ObData o) {
        String s = o.get_result_str();
        if ((!o.get_position().equalsIgnoreCase("LP") && !o.get_position().equalsIgnoreCase("RP")) && !o.get_position().equalsIgnoreCase("MP")) {
            String regex = "^[A-Z0-9]{1,}[-]?[A-Z0-9.]{1,}$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(s);

            if (m.matches() && iscontain_num(o.get_result_str())) {
                return true;
            }

            return false;

            //return false;
        } else {
            String regex = "^[A-Z0-9]{1,}[-]?[A-Z0-9.]{1,}$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(s);

            if (m.matches() && iscontain_num(o.get_result_str())) {
                return true;
            }

            return false;
        }
    }

    /**
     * check string format
     *
     * @param s string
     * @return format name
     */
    static public String checkFormat(String s) {

        String regex = "[a-z_A-Z]\\w+";      //first characte A-Z or a-z
        regex = "^" + regex + "$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(s);

        if (!m.matches()) { //not include any characters

            regex = "([-][^.,](0*([1-9][0-9]*|0)))";  //0-9 ONLY WITH - EXAMPLES: -100 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "INTN";//NEGATIVE INTEGER";
            }
            regex = "([(][0-9]{1,3}[)])";  //0-9 ONLY WITH  () EXAMPLES:   (100)
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "ACCINTN";//NEGATIVE INTEGER()";
            }
            regex = "([(]([1]+[5-9]+|[2][0-1]+)+[0-9]{2}[)])";      //0-9 ONLY
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "YEART";
            }

            regex = "([1-9][0-9]*|0)";      //0-9 ONLY [^%.,\\(\\)](0*([1-9][0-9]*|0))
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "INTP";//POSITIVE INTEGER";
            }
            //regex ="(([A-z]*)[0-9]{1,}([-_]{1}[0-9]{1,})([-_]{1}[0-9]{1,})*)";      //[A-z]*123-582_58-9 //unknown character [־]?
            regex = "(([A-z]*)[0-9]{1,}([A-z]*)(([-_]{1}[0-9]{1,})([-_]{1}[0-9]{1,})*)*)[-_]*";      //([A-z]*)123([A-z]*)-582_58-9 //unknown character [־]?
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "INTPSEP";//POSITIVE INTEGER with separator "-" "_";
            }
            /*
             regex ="(([-][0-9]+)(\\.)([0-9]+)|([-]([0-9]+)\\,)([0-9]+)|([-]\\,)([0-9]+)|([-]\\.)([0-9]+)|([(][0-9]+)(\\,)([0-9]+[)])|([(][0-9]+)(\\.)([0-9]+[)])|([(]\\,)([0-9]+)[)]|([(]\\.)([0-9]+)[)])";//0-9 WITH ONE . OR , - EXAMPLES {-.1} {-,1} {(100,100)} {(100.100)} {-1234.1234} {-1234,1234}
             p = Pattern.compile(regex);
             m = p.matcher(s);
        
             if(m.matches()) return "NEGATIVE FLOAT";
             * 
             */

            regex = "(([0-9]+)(\\.)([0-9]{4}))";//0-9 WITH ONE x.xxxx  current rate
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "UCURRATE";//NEGATIVE FLOAT";
            }
            regex = "(([0-9]+)(\\,)([0-9]{4}))";//0-9 WITH ONE x,xxxx  current rate
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "ECURRATE";//NEGATIVE FLOAT";
            }

            // float with 1 number after dot,comma//   
            regex = "(([-][0-9]+)(\\.)([0-9]{1}))";//0-9 WITH ONE .  - EXAMPLES    {-1234.1}  not include {-.1 |([-]\\.)([0-9]+)} 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADN";//NEGATIVE FLOAT";
            }
            regex = "(([-][0-9]+)(\\,)([0-9]{1}))";//0-9 WITH ONE ,  - EXAMPLES     {-1234,1}  not include  {-.1  |([-]\\,)([0-9]+)}
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDN";//NEGATIVE FLOAT";
            }

            regex = "(([0-9]+)(\\,)([0-9]{1}))"; //0-9 WITH ONE  , EXAMPLES   {1234,1}  not include {,1  |(\\,)([0-9]+)}
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURD";//POSITIVE FLOAT";
            }
            regex = "(([0-9]+)(\\.)([0-9]{1}))"; //0-9 WITH ONE .  EXAMPLES  {1234.1}  not include {.1 |(\\.)([0-9]+)} 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USAD";//POSITIVE FLOAT";
            }
            // end float with 1 number after dot,comma    

            // float with 2 numbers after dot,comma   
            regex = "(([-][0-9]+)(\\.)([0-9]{2}))";//0-9 WITH ONE .  - EXAMPLES    {-1234.12}  not include {-.1 |([-]\\.)([0-9]+)} 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDN";//NEGATIVE FLOAT";
            }
            regex = "(([-][0-9]+)(\\,)([0-9]{2}))";//0-9 WITH ONE ,  - EXAMPLES     {-1234.12}  not include  {-.1  |([-]\\,)([0-9]+)}
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDN";//NEGATIVE FLOAT";
            }

            regex = "(([0-9]+)(\\,)([0-9]{2}))"; //0-9 WITH ONE  , EXAMPLE:  {1234,12}  not include {,1  |(\\,)([0-9]+)}
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDD";//POSITIVE FLOAT";
            }
            regex = "(([0-9]+)(\\.)([0-9]{2}))"; //0-9 WITH ONE .  EXAMPLE: {1234.12}  not include {.1 |(\\.)([0-9]+)} 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADD";//POSITIVE FLOAT";
            }
            // end float with 2 numbers after dot,comma    

            // float with 3 numbers after dot,comma   
            regex = "(([-][0-9]+)(\\.)([0-9]{3}))";//0-9 WITH ONE .  - EXAMPLE:  {-1234.123}  not include {-.1 |([-]\\.)([0-9]+)} 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDDN";//NEGATIVE FLOAT";
            }
            regex = "(([-][0-9]+)(\\,)([0-9]{3}))";//0-9 WITH ONE .  - EXAMPLES     {-1234.1234}  not include  {-.1  |([-]\\,)([0-9]+)}
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDDN";//NEGATIVE FLOAT";
            }

            regex = "(([0-9]+)(\\,)([0-9]{3}))"; //0-9 WITH ONE  , EXAMPLES   {1234,1234}  not include {,1  |(\\,)([0-9]+)}
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDD";//POSITIVE FLOAT";
            }
            regex = "(([0-9]+)(\\.)([0-9]{3}))"; //0-9 WITH ONE .  EXAMPLES  {1234.1234}  not include {.1 |(\\.)([0-9]+)} 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDD";//POSITIVE FLOAT";
            }
            // end float with 3 numbers after dot,comma    

            //FLOAT NEGATIVE NNN.NNN.NNN,N OR NNN,NNN,NNN.N OR(NNN.NNN.NNN,N)
            regex = "([-][0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{1})";      //-NNN,NNN,NNN.N 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADN";//NEGATIVE -NNN,NNN,NNN.N";
            }
            regex = "([-][0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{1})";      //-NNN.NNN.NNN,N 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDN";//NEGATIVE -NNN.NNN.NNN,N";
            }
            regex = "([-][0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{2})";       //-NNN,NNN,NNN.NN 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDN";//"NEGATIVE -NNN,NNN,NNN.NN";
            }
            regex = "([-][0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{2})";      //-NNN.NNN.NNN,NN 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDN";//"NEGATIVE -NNN.NNN.NNN,NN";
            }

            regex = "([-][0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{3})";       //-NNN,NNN,NNN.NNN 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDDN";//"NEGATIVE -NNN,NNN,NNN.NNN";
            }
            regex = "([-][0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{3})";      //-NNN.NNN.NNN,NNN 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDDN";//"NEGATIVE -NNN.NNN.NNN,NNN";  
            }

            regex = "([-][0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{4,})";       //-NNN,NNN,NNN.NNNN 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDDDN";//"NEGATIVE -NNN,NNN,NNN.NNNN";
            }
            regex = "([-][0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{4,})";      //-NNN.NNN.NNN,NNNN 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDDDN";//"NEGATIVE -NNN.NNN.NNN,NNNN";    
            }

            //end
            //FLOAT POSITIVE NNN.NNN.NNN,N OR NNN,NNN,NNN.N 
            regex = "[0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{1}";      //NNN,NNN,NNN.N
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USAD";//"POSITIVE NNN,NNN,NNN.N";
            }
            regex = "[0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{1}";      //NNN.NNN.NNN,N
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURD";//"POSITIVE NNN.NNN.NNN,N";
            }
            regex = "[0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{2}";      //NNN,NNN,NNN.NN
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADD";//"POSITIVE NNN,NNN,NNN.NN";
            }
            regex = "[0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{2}";      //NNN.NNN.NNN,NN
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDD";//"POSITIVE NNN.NNN.NNN,NN";
            }

            regex = "[0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{3}";      //NNN,NNN,NNN.NNN
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDD";//"POSITIVE NNN,NNN,NNN.NNN";
            }
            regex = "[0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{3}";      //NNN.NNN.NNN,NNN
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDD";//"POSITIVE NNN.NNN.NNN,NNN";  
            }

            regex = "[0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{4,}";      //NNN,NNN,NNN.NNNN
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USADDDD";//"POSITIVE NNN,NNN,NNN.NNNN";
            }
            regex = "[0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{4,}";      //NNN.NNN.NNN,NNNN
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURDDDD";//"POSITIVE NNN.NNN.NNN,NNNN";    
            }        //END

            // POSITIVE AND NEGATIVE INTEGERS NNN.NNN.NNN OR NNN,NNN,NNN
            regex = "([-][0-9]{1,3}(\\,[0-9]{3})+)";      //-NNN,NNN,NNN  
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);
            if (m.matches()) {
                return "USAINTN";//NEGATIVE INTEGER -NNN,NNN,NNN";
            }

            regex = "([(][0-9]{1,3}(\\,[0-9]{3})+[)])";      //  (NNN,NNN,NNN)
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "ACCINTN";//NEGATIVE INTEGER (NNN,NNN,NNN)";
            }
            regex = "([-][0-9]{1,3}([.][0-9]{3})+)";      //-NNN.NNN.NNN  
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURINTN";//"NEGATIVE INTEGER -NNN.NNN.NNN";
            }
            regex = "([(][0-9]{1,3}(\\.[0-9]{3})+[)])";      //  (NNN.NNN.NNN)
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "ACCINTN";//NEGATIVE INTEGER (NNN.NNN.NNN)";
            }
            regex = "([0-9]{1,3}(\\,[0-9]{3})+)";      //NNN,NNN,NNN  
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "USAINTP";//"POSITIVE INTEGER NNN,NNN,NNN";
            }
            regex = "([0-9]{1,3}([.][0-9]{3})+)";      //NNN.NNN.NNN  
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "EURINTP";//POSITIVE INTEGER NNN.NNN.NNN";
            }
            //END

            //DATE
            //YYYY  ([1]+[5-9]+|[2][0-1]+)+[0-9]{2}  all years ([0-9]{2})+[0-9]{2}
            regex = "(3[01]|[12][0-9]|0?[1-9])\\/(1[0-2]|0?[1-9])\\/([1]+[5-9]+|[2][0-1]+)+[0-9]{2}";      // DD/MM/yyyy
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE4";//"DATE DD/MM";
            }
            regex = "(1[0-2]|0?[1-9])\\/(3[01]|[12][0-9]|0?[1-9])\\/([1]+[5-9]+|[2][0-1]+)+[0-9]{2}";      // mm/dd/yyyy
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE4";//"DATE MM/DD";
            }

            regex = "(3[01]|[12][0-9]|0?[1-9])\\-(1[0-2]|0?[1-9])\\-([1]+[5-9]+|[2][0-1]+)+[0-9]{2}";      // DD-MM-yyyy
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE4";//"DATE DD-MM"; 
            }
            regex = "(1[0-2]|0?[1-9])\\-(3[01]|[12][0-9]|0?[1-9])\\-([1]+[5-9]+|[2][0-1]+)+[0-9]{2}";      // mm-dd-yyyy
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE4";//"DATE MM-DD";
            }
            regex = "(3[01]|[12][0-9]|0?[1-9])\\.(1[0-2]|0?[1-9])\\.([1]+[5-9]+|[2][0-1]+)+[0-9]{2}";      // DD.MM.yyyy
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE4";//"DATE DD.MM";
            }

            regex = "(1[0-2]|0?[1-9])\\.(3[01]|[12][0-9]|0?[1-9])\\.([1]+[5-9]+|[2][0-1]+)+[0-9]{2}";      // mm.dd.yyyy
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE4";//"DATE MM.DD";
            }
            //END YYYY
            //YY
            regex = "(3[01]|[12][0-9]|0?[1-9])\\/(1[0-2]|0?[1-9])\\/([0-9]{2})";      //D/M/yy 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE2";//DATE DD/MM";
            }
            regex = "(1[0-2]|0?[1-9])\\/(3[01]|[12][0-9]|0?[1-9])\\/([0-9]{2})";      //m/d/yy 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE2";//"DATE MM/DD";
            }

            regex = "(3[01]|[12][0-9]|0?[1-9])\\-(1[0-2]|0?[1-9])\\-([0-9]{2})";      //D-M-yy 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE2";//"DATE DD-MM"; 
            }
            regex = "(1[0-2]|0?[1-9])\\-(3[01]|[12][0-9]|0?[1-9])\\-([0-9]{2})";      //m-d-yy 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE2";//"DATE MM-DD";
            }
            regex = "(3[01]|[12][0-9]|0?[1-9])\\.(1[0-2]|0?[1-9])\\.([0-9]{2})";      //D.M.yy 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE2";//"DATE DD.MM";
            }

            regex = "(1[0-2]|0?[1-9])\\.(3[01]|[12][0-9]|0?[1-9])\\.([0-9]{2})";      //m.d.yy 
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "DATE2";//"DATE MM.DD";
            }       //END

            //TIME 
            regex = "(2[0-3]|[01]?[0-9])\\:([0-5]+[0-9]+)\\:([0-5]+[0-9]+)";      //HH:MM:SS
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "HHMMSS";//TIME HH:MM:SS";
            }
            regex = "(2[0-3]|[01]?[0-9])\\:([0-5]+[0-9]+)";      //HH:MM
            regex = "^" + regex + "$";
            p = Pattern.compile(regex);
            m = p.matcher(s);

            if (m.matches()) {
                return "HHMM";//TIME HH:MM";
            }        //END

        }

        regex = "([a-z_A-Z]+)([-_:/]{1})[0-9]+";      //  a-45788   AD:565 Sds_343 Rt/565

        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "ENGSDIG";//[characters separate digits]
        }        //a45788
        regex = "[a-z_A-Z]+\\d+";      //a45788

        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "ENGDIG";//[characters NO_separate digits]
        }

        //tel,fax 7 digits after area
        regex = "((\\(\\+?\\+*([0-9]{3})\\)|\\+?\\+*[0-9]{3}|0))([-_●]?)[0-9]{1}([-_●])+([0-9]{7})";      //fax 7 digits after area (123)-3-4567890 (123)4_4567890 (123)3●4567890 123-3-4567890 01-4567890 04_4567890
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "TEL";//tel, fax
        }
        //tel,fax -3-4 digits after area
        regex = "((\\(\\+?\\+*([0-9]{3})\\)|\\+?\\+*[0-9]{3}|0))([-_●]?)[0-9]{1}([-_●])+([0-9]{3})([-_●])+([0-9]{4})";      //03-123-4567
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "TEL";//tel, fax
        }
        //tel,fax 3-2-2-3 digits after area
        regex = "((\\(\\+?\\+*([0-9]{3})\\)|\\+?\\+*[0-9]{3}|0))([-_●]?)[0-9]{2}([-_●]?)[0-9]{2}([-_●])+([0-9]{2})([-_●])+([0-9]{3})";      //077-78-78-322
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "TEL";//tel, fax
        }

        //percent
        regex = "([-]?)(([0-9]+)([.]{1})([0-9]+)|([0-9]+))([%]{1})|([%]{1})([-]?)(([0-9]+)([.]{1})([0-9]+)|([0-9]+))";      //32% -1.25% 0.125% or %32  %-1.25 %0.125 
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PERCENT";
        }

        //MONTH
        regex = "(1[0-2]|0?[1-9])\\/(([1]{1}[5-9]{1}|[2][0-1]{1}){1}[0-9]{2}|([0-9]{2}))";      //05/73 6/12 04/2012 5/2013
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "MONTH";
        }

        //PERIOD
        regex = "(((1[0-2]|0?[1-9])\\/)?)(([1]{1}[5-9]{1}|[2][0-1]{1}){1}[0-9]{2}|([0-9]{2}))\\-(((1[0-2]|0?[1-9])\\/)?)(([1]{1}[5-9]{1}|[2][0-1]{1}){1}[0-9]{2}|([0-9]{2}))";      //11/2007-04/2009 09/73-09/87 2011-2013 05/2002-2013
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PERIOD";
        }

        //paragraf
        //latin
        String lat_chars = "[ếẾềỀễỄểỂẽ ẼḝḜḗḖḕḔẻẺẹẸệỆḙḘḛḚ ḟḞ ḡḠ ḧḦḣḢḩḨḥḤḫḪ ḯḮỉỈịỊ ḭḬ ḱḰḳḲḵḴ ḷḶḹḸḽḼḻḺ ḿḾṁṀṃṂ ṅṄṇṆṋṊṉṈ ốỐồỒỗ ỖổỔṍṌṏṎṓṒṑṐỏỎớỚờỜỡỠởỞ ợỢọỌộỘ ṕṔṗṖ ṙṘṛṚṝṜṟṞ ṥṤṧṦṡṠṣṢṩṨẛ ẞ ẗṫṪ ṭṬṱṰṯṮ ṹṸṻṺủỦứỨừỪữỮửỬ ựỰụỤṳṲṷṶṵṴ ṽṼṿṾ ẃẂ ẁẀẘẅẄẇẆẉẈ ẍẌẋẊ ỳỲẙỹỸẏ ẎỷỶỵỴ ẑẐẓẒẕẔ ÁàÀâÂ åÅäÄãÃ æÆ çÇ ðÐ éÉèÈê ÊëË íÍìÌîÎïÏ ñÑ ºóÓòÒ ôÔöÖõÕøØ ß úÚùÙûÛüÜ ý Ýÿ þÞ µ]";
        regex = "(([0-9]){1,2}|([A-Za-z]{1})|(" + lat_chars + "{1}))([.)\\]]{1})";      //latin paragraph one character or numbers with one . or ) or ]
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PARLAT1";//PARLAT1
        }
        regex = "((([0-9]){1,2}|([A-Za-z]{1})|(" + lat_chars + "{1}))(\\.{1})){2}";      //latin paragraph two character or numbers with two . 
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PARLAT2";//PARLAT2
        }
        regex = "((([0-9]){1,2}|([A-Za-z]{1})|(" + lat_chars + "{1}))(\\.{1})){3}";      //latin paragraph three character or numbers with two . 
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PARLAT3";//PARLAT3
        }
        //hebrew
        String heb_chars = "[֑-֯ ֽ ׄ ׅ ְ-ָ ׇ ֹ-ֻ ׂ ׁ ּ ֿ ־ ׀ ׆ ׳ ״ א-ו װ ױ ז-י ײ כך ל מם נן ס ע פף צץ ק-ת \u0590 \u05C8-\u05CF \u05EB-\u05EF \u05F5-\u05FF]";
        regex = "(([\\.(]{1})(([0-9]){1,2}|(" + heb_chars + "{1,2}))){1}";      //hebrew paragraph one character or numbers with one . or (
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PARHEB1";//PARHEB1
        }
        regex = "((\\.{1})(([0-9]){1,2}|(" + heb_chars + "{1,2}))){2}";      //hebrew paragraph two character or numbers with two . 
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PARHEB2";//PARHEB2
        }
        regex = "((\\.{1})(([0-9]){1,2}|(" + heb_chars + "{1,2}))){3}";      //hebrew paragraph three character or numbers with two . 
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "PARHEB3";//PARHEB3
        }
        //end paragraph

        //text
        //latin
        //[ aA bB cC dD eE fF gG hH iI jJ kK lL mM nN oO pP qQ rR sS tT uU vV wW xX yY zZ]
        //Latin Extended Additional — Latin general extensions
        //[ắẮằẰẵẴẳẲấẤầẦẫẪẩẨảẢạ ẠặẶậẬḁḀ ẚ ḃḂḅḄḇḆ ḉḈ ḋ ḊḑḐḍḌḓḒḏḎ 
        //ếẾềỀễỄểỂẽ ẼḝḜḗḖḕḔẻẺẹẸệỆḙḘḛḚ ḟḞ ḡḠ ḧḦḣḢḩḨḥḤḫḪ ḯḮỉỈịỊ ḭḬ ḱḰḳḲḵḴ ḷḶḹḸḽḼḻḺ 
        //ḿḾṁṀṃṂ ṅṄṇṆṋṊṉṈ ốỐồỒỗ ỖổỔṍṌṏṎṓṒṑṐỏỎớỚờỜỡỠởỞ ợỢọỌộỘ ṕṔṗṖ ṙṘṛṚṝṜṟṞ ṥṤṧṦṡṠṣṢṩṨẛ ẞ 
        //ẗṫṪ ṭṬṱṰṯṮ ṹṸṻṺủỦứỨừỪữỮửỬ ựỰụỤṳṲṷṶṵṴ ṽṼṿṾ ẃẂ ẁẀẘẅẄẇẆẉẈ ẍẌẋẊ ỳỲẙỹỸẏ ẎỷỶỵỴ ẑẐẓẒẕẔ
        //ÁàÀâÂ åÅäÄãÃ æÆ çÇ ðÐ éÉèÈê ÊëË íÍìÌîÎïÏ ñÑ ºóÓòÒ ôÔöÖõÕøØ ß úÚùÙûÛüÜ ý Ýÿ þÞ µ]
        regex = "([\"(\\[]*)(([a-z_A-Z]+)|([ếẾềỀễỄểỂẽ ẼḝḜḗḖḕḔẻẺẹẸệỆḙḘḛḚ ḟḞ ḡḠ ḧḦḣḢḩḨḥḤḫḪ ḯḮỉỈịỊ ḭḬ ḱḰḳḲḵḴ ḷḶḹḸḽḼḻḺ ḿḾṁṀṃṂ ṅṄṇṆṋṊṉṈ ốỐồỒỗ ỖổỔṍṌṏṎṓṒṑṐỏỎớỚờỜỡỠởỞ ợỢọỌộỘ ṕṔṗṖ ṙṘṛṚṝṜṟṞ ṥṤṧṦṡṠṣṢṩṨẛ ẞ ẗṫṪ ṭṬṱṰṯṮ ṹṸṻṺủỦứỨừỪữỮửỬ ựỰụỤṳṲṷṶṵṴ ṽṼṿṾ ẃẂ ẁẀẘẅẄẇẆẉẈ ẍẌẋẊ ỳỲẙỹỸẏ ẎỷỶỵỴ ẑẐẓẒẕẔ ÁàÀâÂ åÅäÄãÃ æÆ çÇ ðÐ éÉèÈê ÊëË íÍìÌîÎïÏ ñÑ ºóÓòÒ ôÔöÖõÕøØ ß úÚùÙûÛüÜ ý Ýÿ þÞ µ]+))([\"-_:/?!',;)\\]]*)([^0-9])";      //  

        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "LATTXT";//
        }
        //grek
        regex = "([\"(\\[]*)([϶ αΑάΆ βϐΒ γΓ δΔ εϵΕέΈ ϝϜ ϛϚ ζΖ ηΗήΉ θϑΘϴ ιͺ ΙίΊϊΪΐ ϳ κϰΚ ϗ λΛ μΜ νΝ ξΞ οΟόΌ πϖΠ ϻϺ ϟϞ ϙϘ ρϱΡ ϼ σϲΣϹς ͼϾ ͻϽ ͽϿ τΤ υΥϒύΎϓϋΫϔΰ φϕΦ χΧ ψΨ ωΩώΏ ϡϠ ϸϷ ϣ Ϣ ϥϤ ϧϦ ϩϨ ϫϪ ϭϬ ϯϮ ἀἈἄἌᾄᾌἂἊᾂᾊἆἎᾆᾎᾀᾈἁἉἅ ἍᾅᾍἃἋᾃᾋἇἏᾇᾏᾁᾉάΆᾴὰᾺᾲᾰᾸ ᾶᾷᾱᾹᾳᾼ ἐἘἔἜἒἚἑἙἕἝἓἛέΈ ὲῈ ἠἨἤἬᾔᾜἢἪᾒᾚἦἮᾖᾞᾐᾘἡἩ ἥἭᾕᾝἣἫᾓᾛἧἯᾗᾟᾑᾙήΉῄὴῊῂῆ ῇῃῌ ιἰἸἴἼἲἺἶἾἱἹἵἽἳἻἷἿ ίΊὶῚῐῘῖΐῒῗῑῙ ὀὈὄὌὂὊὁὉ ὅὍὃὋόΌὸῸ ῤ ῥῬ ὐὔὒὖὑὙὕ ὝὓὛὗὟύΎὺῪῠῨῦΰῢῧῡῩ ὠὨὤ ὬᾤᾬὢὪᾢᾪὦὮᾦᾮᾠᾨὡὩὥὭᾥᾭὣὫ ᾣᾫὧὯᾧᾯᾡᾩώΏῴὼῺῲῶῷῳῼ]+)([\"-_:/?!',;)\\]]*)([^0-9])";      //  аАяЯ

        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "GRKTXT";//
        }

        //russian
        regex = "([\"(\\[]*)([а-я_А-Я]+)([\"-_:/?!',;)\\]]*)";      //  аАяЯ

        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "RUSTXT";//аАяЯ
        }
        //hebrew
        regex = "([\"(\\[]*)(([׆ ־ ׳ ״ א-ו װ ױ ז-י ײ כך ל מם נן ס ע פף צץ ק-ת]+))([\"-_:/?!',;)\\]]*)([^0-9])";      //  
        //regex = "([\"(\\[]*)(([׆ ־ ׳ ״ א-ו װ ױ ז-י ײ כך ל מם נן ס ע פף צץ ק-ת]+))([\"-_:/?!',;)\\]]*)([^0-9])";  

        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "HEBTXT";//זאבה אבא,
        }

        regex = "([\"(\\[]*)(([֑-֯ ֽ ׄ ׅ ְ-ָ ׇ ֹ-ֻ ׂ ׁ ּ ֿ ־ ׀ ׃ ׆ ׳ ״ א-ו װ ױ ז-י ײ כך ל מם נן ס ע פף צץ ק-ת \u0590 \u05C8-\u05CF \u05EB-\u05EF \u05F5-\u05FF]+))([\"-_:/?!',;)\\]]*)([^0-9])";      //  
        regex = "^" + regex + "$";
        p = Pattern.compile(regex);
        m = p.matcher(s);

        if (m.matches()) {
            return "NKDTXT";//זְאֵבָה!", (כָּזֹאת?) וְגָמַרנוּ]
        }

        return "GENERAL";
    }

    static public String escapeUnicode(String input) {
        StringBuilder b = new StringBuilder(input.length());
        Formatter f = new Formatter(b);

        for (char c : input.toCharArray()) {
            if (c < 128) {
                if (c != ' ') {
                    b.append(c);
                }
            } else {
                f.format("\\u%04x", (int) c);//.format("\\u%04x", )
            }
        }
        return b.toString();
    }

    public static boolean checkChanges(String str1, String str2) {
        boolean isClose = false;
        int numberOfChanges = -1;
        int lengthDiffernce = Math.abs(str1.length() - str2.length());
        if (lengthDiffernce < 3) {
            if (lengthDiffernce == 0) {
                if (str1.length() <= 3) {
                    isClose = str1.equals(str2);
                } else if (3 < str1.length() && str1.length() <= 6) {
                    numberOfChanges = numberOfChanges(str1, str2, lengthDiffernce);
                    isClose = numberOfChanges <= 1;
                } else if (str1.length() > 6) {
                    numberOfChanges = numberOfChanges(str1, str2, lengthDiffernce);
                    isClose = numberOfChanges <= 2;
                }
                if (str2.contains(str1.substring(1, str1.length())) || str1.contains(str2.substring(1, str2.length()))) {
                    isClose = true;
                }
            } else {
                String shortString = str1;
                String longString = str2;
                if (str2.length() < str1.length()) {
                    shortString = str2;
                    longString = str1;
                }
                if (shortString.length() <= 3) {
                    isClose = longString.contains(shortString);
                } else if (4 <= shortString.length() && shortString.length() <= 6) {
                    numberOfChanges = numberOfChanges(shortString, longString, lengthDiffernce);
                    isClose = numberOfChanges <= 1;
                } else if (shortString.length() > 6) {
                    numberOfChanges = numberOfChanges(shortString, longString, lengthDiffernce);
                    isClose = numberOfChanges <= 2;
                }
            }
        }

        return isClose;
    }

    public static int numberOfChanges(String shortString, String longString, int lengthDiffernce) {
        int numberOfChanges = shortString.length();
        int temp;
        for (int j = 0; j < lengthDiffernce + 1; j++) {
            temp = shortString.length();
            for (int i = 0; i < shortString.length(); i++) {
                if (longString.charAt(j + i) == shortString.charAt(i)) {
                    temp--;
                }
            }
            numberOfChanges = Math.min(temp, numberOfChanges);
        }

        return numberOfChanges;
    }

    public static int numberOfChanges2(String shortString, String longString, int lengthDiffernce) {
        int numberOfChanges = shortString.length();
        int temp;
        int maxSkip = 0;
        for (int j = 0; j < lengthDiffernce + 1; j++) {
            temp = shortString.length();
            for (int i = 0; i < shortString.length(); i++) {
                if (longString.charAt(j + i) == shortString.charAt(i)) {
                    temp--;
                } else if (j + 1 < lengthDiffernce + 1 && maxSkip < 2) {
                    maxSkip++;
                    j++;
                    i--;
                } else if (maxSkip == 2 && longString.charAt(j + i) != shortString.charAt(i)) {
                    break;
                }
            }
            numberOfChanges = Math.min(temp, numberOfChanges);
        }

        return numberOfChanges;
    }

    /**
     * check string format
     *
     * @param s string
     * @return format name
     */
    static public String checkFormatold(String s) {
        boolean pos = true;
        boolean mix = false;
        int count_psik = 0;
        int count_dot = 0;
        int count_dig_after_psik = 0;
        if (s.charAt(0) == '-') {
            pos = false;
            s = s.substring(1);
        }
        if (asci_range(s.charAt(0)) == 1) {
            for (int i = 1; i < s.length() && !mix; i++) {
                if (asci_range(s.charAt(i)) != 1) {

                    if (s.charAt(i) == ',') {
                        count_psik++;
                        count_dig_after_psik = s.length() - (i + 1);
                    } else if (s.charAt(i) == '.') {
                        count_dot++;
                    } else {
                        mix = true;
                    }
                }

            }
            //   System.out.println(s);

            if (pos && !mix && count_psik == 0) {
                return "POSITIVE INTEGER";
            } else if (!pos && !mix && count_psik == 0) {
                return "NEGATIVE INTEGER";
            } else if (pos && !mix && count_psik == 1) {
                return "POSITIVE FLOAT " + count_dig_after_psik;
            } else if (!pos && !mix && count_psik == 1) {
                return "NEGATIVE FLOAT " + count_dig_after_psik;
            }

        }

        return "GENERAL";
    }

    /**
     * search last index of "
     *
     * @param s string for search
     * @return last index of "
     */
    static public int ind_last_q(String s) {

        int ind = -1;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '"') {
                ind = i;
            }

        }
        return ind;
    }

    /**
     * function define center of window
     *
     * @param dform this window
     * @return p - Point of center
     */
    static public Point calib_center(Dimension dform) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point p = new Point();
        p.x = (screenSize.width - dform.width) / 2;
        p.y = (screenSize.height - dform.height) / 2;
        return p;

    }

    /**
     * function space_fix(String k) - added "" to String k
     *
     * @param k received String
     * @return "k" - returned String
     */
    static public String space_fix(String k) {
        k = "\"" + k + "\" ";
        // k=k+" ";
        return k;
    }

    public static boolean isRunning(Process process) {
        if (process == null) {
            return false;
        }
        try {
            process.exitValue();
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean isHebWord(String str) {
        for (char c = 'א'; c <= 'ת'; c++) {
            if (str.contains(c + "")) {
                return true;
            }
        }
        return false;
    }

    /**
     * csv to csu
     *
     * @param fn
     */
    public static String convert2Unicode(String fn) {
        try {
            //clean old
            File oldcsu = new File(fn.substring(0, fn.length() - 4) + ".csu");
            if (oldcsu.exists()) {
                oldcsu.delete();
            }

            File convertedFile = new File(fn);

            //C:\HyperOCR\csv_convert.exe E:\temp\csv_convert\4.csv
            String command = space_fix(CSV2Hash.csv_convertpath) + " " + space_fix(convertedFile.getAbsolutePath());
            Process p = Runtime.getRuntime().exec(command);
            System.out.println(command);
            write("", "convert2Unicode()", "", command, true);
            int k = 0;
            while (isRunning(p)) {
                //System.out.println(k++);
            }

            return fn.substring(0, fn.length() - 4) + ".csu";
        } catch (IOException ex) {
            Logger.getLogger(LFun.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * static function for read from file to vector strings
     *
     * @param fn string path to file
     * @return vector strings
     */
    static public Vector<String> file2vector(String fn, String format) {
        Vector<String> v = new Vector<String>();
        try {
            File convertedFile = new File(fn);
            //csv to csu
            if ((fn.endsWith(".csv") || fn.endsWith(".hrz") || fn.endsWith(".vrt")) && convertedFile.exists()) {
                fn = convert2Unicode(fn);
                convertedFile = new File(fn);
            } else {

                convertedFile = new File(fn.substring(0, fn.length() - 4) + ".csu");
            }

            //new java.util.native2ascii.Main().convert(new String[]{"-reverse", new File("README.TXT"), convertedFile});
            FileInputStream fr = new FileInputStream(convertedFile);
            BufferedReader br = null;
            try {
                if (!format.equals("ANSI")) {
                    br = new BufferedReader(new InputStreamReader(fr, format));
                } else {
                    br = new BufferedReader(new InputStreamReader(fr));
                }
            } catch (Exception e) {
            }
            String s = "";
            while ((s = br.readLine()) != null) {
                // System.out.println(s);
                v.add(s);
            }
        } catch (Exception ex) {
        }
        return v;
    }

    public static Vector<String> readFile2vector(String fn, String format) {
        File f = new File(fn);
        String line;
        Vector<String> model = new Vector<String>();
        FileReader file;
        BufferedReader Reader;
        if (!f.exists()) {
            return model;
        }

        try {

            //file = new FileReader(f);
            //Reader = new BufferedReader(file);
            Reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), format));

            line = Reader.readLine();
            while (line != null) {
                if (line == null || line.length() < 1) {
                    line = " ";
                }
                model.addElement(line);

                line = Reader.readLine();
            }

            Reader.close();
            //file.close();

        } catch (IOException ex) {
            return model;
        }
        return model;
    }

    /**
     * static function for read from file to vector strings
     *
     * @param fn string path to file
     * @return vector strings
     */
    static public Vector<String> file2vector2(String fn) {

        Vector<String> v = new Vector<String>();
        BufferedReader in = null;
        String str = null;
        try {
            File m = new File(fn);
            //in = new BufferedReader(new InputStreamReader(new FileInputStream(m)));
            Charset inputCharset = Charset.forName("ISO-8859-8");
            in = new BufferedReader(new InputStreamReader(new FileInputStream(m), inputCharset));
            str = in.readLine();
            str = in.readLine();
            while (str != null) {
                v.add(str);
                str = in.readLine();
            }

        } catch (IOException ex) {
            Logger.getLogger(LFun.class.getName()).log(Level.SEVERE, null, ex);
        }

        return v;
    }

    public static void cleanFolder(String path) {
        File directory = new File(path);

        File[] fList = directory.listFiles();
        if (fList != null) {
            for (File file : fList) {
                if (file.isFile()) {
                    file.delete();

                } else if (file.isDirectory()) {
                    cleanFolder(file.getAbsolutePath());
                    if (file.list().length == 0 && file != new File(path)) {
                        file.delete();
                    }
                }
            }
        }
    }

    static public Vector<String> file2vector3(String fn) {

        Vector<String> v = new Vector<String>();
        BufferedReader in = null;
        String str = null;
        try {
            File m = new File(fn);
            if (!m.exists()) {
                return null;
            }
            //in = new BufferedReader(new InputStreamReader(new FileInputStream(m)));
            Charset inputCharset = Charset.forName("ISO-8859-8");
            in = new BufferedReader(new InputStreamReader(new FileInputStream(m), inputCharset));

            str = in.readLine();
            while (str != null) {
                v.add(str);
                str = in.readLine();
            }

        } catch (IOException ex) {
            Logger.getLogger(LFun.class.getName()).log(Level.SEVERE, null, ex);
        }

        return v;
    }

    public static Vector<String> getElements(String sep, String string) {
        Vector<String> ret = new Vector<String>();
        StringTokenizer tok = new StringTokenizer(string, sep);
        while (tok.hasMoreTokens()) {
            ret.add(tok.nextToken());
        }
        return ret;
    }

    public static void printVec(Vector<Vector<ObData>> Vectors) {
        try {
            System.out.println("**printVec:**");
            for (int i = 0; i < Vectors.size(); i++) {
                for (int j = 0; j < Vectors.get(i).size(); j++) {
                    System.out.print(Vectors.get(i).get(j).get_result_str() + " (" + Vectors.get(i).get(j).get_line_num() + "), ");
                }
                System.out.println(".");
            }
            System.out.println("*************");
        } catch (Exception e) {
        }
    }

    public static double sumVec(Vector<ObData> Vector) {
        double s = 0.0;
        try {
            for (int i = 0; i < Vector.size(); i++) {
                s += Double.valueOf(Vector.get(i).get_result_str().replaceAll(",", ""));
            }
        } catch (Exception e) {
        }
        return s;
    }

    public static boolean maxSumVec(Vector<ObData> Vector1, Vector<ObData> Vector2) {
        double s1 = sumVec(Vector1);
        double s2 = sumVec(Vector2);
        if (s1 >= s2) {
            return true;
        } else {
            return false;
        }
    }

    static Vector<ObData> lineByIndex(int num_line, Vector<ObData> od) {
        Vector<ObData> line_n = new Vector<ObData>();
        for (ObData obd : od) {
            if (obd.get_line_num() == num_line) {
                line_n.add(obd);
            }
        }

        return line_n;
    }

    static boolean checkIfIsLessThanPrecent(Double Percent, double Total, double toCheck) {
        boolean isLessThanPrecent = false;
        double currentPrecent = Math.abs(toCheck) * 100 / Math.abs(Total);
        if (currentPrecent < Percent) {
            isLessThanPrecent = true;
        }

        return isLessThanPrecent;
    }

    public static void copyFile(String input, String output, boolean Always) {
        try {
            File F_input = new File(input);
            File F_output = new File(output);
            if (F_input == null || F_output == null || !F_input.exists()) {
                //LogFile.wlog("ConfigFun.copyFile()", "Error - not copy (" + input + "," + output + ")");
                return;
            }
            try {
                F_output.getParentFile().mkdirs();
            } catch (Exception e) {
            }
            if ((F_input.lastModified() != F_output.lastModified()) || Always) {
                //System.out.println("copyFile(" + input + "," + output + ")");
                //LogFile.wlog("ConfigFun.copyFile()", "copy (" + input + "," + output + ")");
                try {
                    Path from = Paths.get(input);
                    Path to = Paths.get(output);
                    Files.copy(from, to, REPLACE_EXISTING);
                } catch (IOException ex) {
                    //ex.printStackTrace();
                    //LogFile.wlog("ConfigFun.copyFile()", "Error copy wisescan.exe", ex.toString());
                }

            } else {
                //System.out.println("copyFile(" + input + "," + output + "): Same File - not copy");
                //LogFile.wlog("ConfigFun.copyFile()", "Same File - not copy (" + input + "," + output + ")");
            }
        } catch (Exception e) {
            //LogFile.wlog("ConfigFun.copyFile()", "Error copy wisescan.exe", e.toString());
        }
    }

    static public File getFile(String FolderPath, String EndWith) {
        EndWith = EndWith.toLowerCase();
        File[] list = new File(FolderPath).listFiles();
        if (list != null) {
            for (File f : list) {
                if (f.getName().toLowerCase().endsWith(EndWith)) {
                    return f;
                }
            }
        }
        return null;
    }

    static public File getFile(String FolderPath, String StartWith, String EndWith) {
        StartWith = StartWith.toLowerCase();
        EndWith = EndWith.toLowerCase();
        File[] list = new File(FolderPath).listFiles();
        if (list != null) {
            for (File f : list) {
                if (f.getName().toLowerCase().startsWith(StartWith) && f.getName().toLowerCase().endsWith(EndWith)) {
                    return f;
                }
            }
        }
        return null;
    }

    public static void killWiseBill() {
        System.err.println("killWiseBill()");
        String[] process_name = {"WiseRunBill.exe", "WiseCopyBill.exe", "WiseCopyBillWB.exe", "WisePageBill.exe"};
        for (int i = 0; process_name != null && i < process_name.length; i++) {
            try {
                Runtime.getRuntime().exec("C:\\HyperOCR\\taskkill.exe /f /im " + process_name[i]);
                Runtime.getRuntime().exec("C:\\HyperOCR\\taskkill.exe /f /im " + process_name[i] + " *32");
                Runtime.getRuntime().exec("C:\\HyperOCR\\taskkill.exe /f /im " + process_name[i] + " *64");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        System.exit(0);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {

        }
    }

    public static boolean IsTheSameHight(int[] left, int[] right, int deviation) {
        return checkDeviation(left[1], right[1], deviation) && checkDeviation(left[3], right[3], deviation);
    }

    public static boolean checkDeviation(double number, double numberToCheck, double deviation) {

        if (number + deviation > numberToCheck && number - deviation < numberToCheck) {
            return true;
        }

        return false;
    }

    public static String ToDataFormat(String str) {
        if (str.length() < 5) {
            return str;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date date = formatter.parse(str);
            return new SimpleDateFormat("dd.MM.yyyy").format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    public static Vector<int[]> UnionHrz(Vector<String> v) {
        int minPixelToUnion = 3;
        int minPixelToRemove = 2;
        int minDiff_y = 300;

        Vector<int[]> Coordinate = new Vector<int[]>();
        for (int i = 0; i < v.size(); i++) {
            try {
                StringTokenizer st = new StringTokenizer(v.get(i), ",");
                int[] arr = {Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken())};

                Coordinate.add(arr);
            } catch (Exception e) {
                System.out.println("v.get(i)=" + v.get(i));
                e.printStackTrace();
            }
        }
        for (int j = 0; j < Coordinate.size(); j++) {

            for (int i = 0; i < Coordinate.size() - 1; i++) {
                int[] Union_arr = Coordinate.get(i);
                int last_y = Union_arr[3];
                Coordinate.remove(i);
                while (i < Coordinate.size()
                        && Union_arr[4] == Coordinate.get(i)[4]
                        && ObDataWithLines.twoLineMeet(Coordinate.get(i)[1], Coordinate.get(i)[3], Union_arr[1], Union_arr[3], minPixelToUnion)
                        && ObDataWithLines.twoLineMeet(Coordinate.get(i)[0], Coordinate.get(i)[2], Union_arr[0], Union_arr[2], minDiff_y)) {
                    last_y = Coordinate.get(i)[3];
                    Union_arr[0] = (int) Math.min(Union_arr[0], Coordinate.get(i)[0]);
                    Union_arr[1] = (int) Math.min(Union_arr[1], Coordinate.get(i)[1]);
                    Union_arr[2] = (int) Math.max(Union_arr[2], Coordinate.get(i)[2]);
                    Union_arr[3] = (int) Math.max(Union_arr[3], Coordinate.get(i)[3]);

                    Coordinate.remove(i);
                }
                Coordinate.add(i, Union_arr);
                //i--;

            }
        }

        for (int i = 0; i < Coordinate.size(); i++) {
            if (1 + Coordinate.get(i)[3] - Coordinate.get(i)[1] < minPixelToRemove) {
                Coordinate.remove(i--);
            }
        }

        //remove the first and the last
        try {
            Coordinate.remove(0);
            Coordinate.remove(Coordinate.size() - 1);
        } catch (Exception e) {
        }

        return Coordinate;
    }

    public static Vector<int[]> UnionVrt(Vector<String> v) {
        int DEVIATION = 50;
        int minPixsel = 3;
        int minDiff_x = 300;

        Vector<int[]> Coordinate = new Vector<int[]>();

        for (int i = 0; i < v.size(); i++) {
            try {
                StringTokenizer st = new StringTokenizer(v.get(i), ",");
                int[] arr = {Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken()),
                    Integer.valueOf(st.nextToken())};

                Coordinate.add(arr);
            } catch (Exception e) {
                System.out.println("v.get(i)=" + v.get(i));
                e.printStackTrace();
            }
        }

        for (int j = 0; j < Coordinate.size(); j++) {
            for (int i = 0; i < Coordinate.size() - 1; i++) {
                int[] Union_arr = Coordinate.get(i);
                //int last_x = Union_arr[2];
                Coordinate.remove(i);
                while (i < Coordinate.size()
                        && Union_arr[4] == Coordinate.get(i)[4]
                        //&& Coordinate.get(i)[0] - last_x < minDiff_x
                        && ObDataWithLines.twoLineMeet(Coordinate.get(i)[0], Coordinate.get(i)[2], Union_arr[0], Union_arr[2], minPixsel)
                        && ObDataWithLines.twoLineMeet(Coordinate.get(i)[1], Coordinate.get(i)[3], Union_arr[1], Union_arr[3], minDiff_x)) {
                    //last_x = Coordinate.get(i)[2];
                    Union_arr[0] = (int) Math.min(Union_arr[0], Coordinate.get(i)[0]);
                    Union_arr[1] = (int) Math.min(Union_arr[1], Coordinate.get(i)[1]);
                    Union_arr[2] = (int) Math.max(Union_arr[2], Coordinate.get(i)[2]);
                    Union_arr[3] = (int) Math.max(Union_arr[3], Coordinate.get(i)[3]);

                    Coordinate.remove(i);
                }
                Coordinate.add(i, Union_arr);
                //i--;

            }
        }

        for (int i = 1; i < Coordinate.size(); i++) {
            if (Coordinate.get(i)[4] == Coordinate.get(i - 1)[4] && ObDataWithLines.twoLineMeet(Coordinate.get(i)[0], Coordinate.get(i)[2], Coordinate.get(i - 1)[0], Coordinate.get(i - 1)[2], 16)) {
                Coordinate.remove(i--);
            }
        }

        for (int i = 0; i < Coordinate.size(); i++) {
            if (1 + Coordinate.get(i)[2] - Coordinate.get(i)[0] < minPixsel) {
                int mone_same_y = 0;
                for (int j = 0; j < Coordinate.size(); j++) {
                    if (i != j
                            && Coordinate.get(i)[4] == Coordinate.get(j)[4]
                            && Math.abs(Coordinate.get(i)[1] - Coordinate.get(j)[1]) < DEVIATION
                            && Math.abs(Coordinate.get(i)[3] - Coordinate.get(j)[3]) < DEVIATION) {
                        mone_same_y++;
                    }
                }
                if (mone_same_y < 3) {
                    Coordinate.remove(i--);
                }
            }
        }

        return Coordinate;
    }

    public static boolean writeToFile(File file, Vector<String> v) {
        FileWriter File_Writer;
        BufferedWriter Buffered_Writer;

        String line;
        int size = v.size();

        try {
            if (v == null) {
                return false;
            }
            if (file == null) {
                return false;
            }
            if (!file.exists()) {
                //System.out.println(file.getAbsolutePath());
                file.createNewFile();
            }
            if (!file.exists()) {
                return false;
            }

            //File_Writer = new FileWriter(file);
            //Buffered_Writer = new BufferedWriter(File_Writer);
            Buffered_Writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

            for (int i = 0; i < size; i++) {
                //dtl.wlog("for:"+ i + " to " + size);
                line = v.get(i).toString();
                if (line != null && !line.equals("")) {
                    Buffered_Writer.write(line.trim());
                    Buffered_Writer.newLine();
                }
            }

            Buffered_Writer.close();
            //File_Writer.close();

        } catch (IOException ex) {
            ex.printStackTrace();
            System.err.println("Error in writeToFile()");
            return false;
        }
        return true;
    }

    /*public static void createInfoDotCsvFile(String f) {
        //System.out.println(getFile(Pathes.Path.getbill_in(), ";info.csv"));
        if (getFile(Pathes.Path.getbill_in(), new File(f).getName(), ";info.csv") == null) {
            Pathes.Path.getbackup_pdf();
            String command = WiseConfig.Config.getWiseCopyExePath() + " " + Pathes.Path.getbackup_pdf() + "\\*.pdf " + Pathes.Path.getbackup_pdf() + " /h /s /repfile /copy /over /keywords:a /subs";
            Process process = null;
            try {
                System.out.println(command);
                process = Runtime.getRuntime().exec(command);
            } catch (Exception ex) {
            } finally {
                writeWiserunLog(command);
            }
            while (process != null && process.isAlive()) {

            }
            File info = getFile(Pathes.Path.getbackup_pdf(), ".kwd");
            //if (info != null) {
            //Vector<String> readFile2vector = LFun.readFile2vector(info.getAbsolutePath(), "ANSI");

            Vector<String> v;
            if (info.exists()) {
                Vector<String> s = String2Array(InfoReader.readFileReader(info));

                String info_line = "\"" + (info.getName().substring(0, info.getName().length() - ".kwd".length()) + ".pdf") + "\"";
                for (int i = 7; i <= 24; i++) {
                    info_line += "," + s.get(i);
                }

                Vector<String> info_vec = new Vector<String>();
                info_vec.add(info_line);

                System.out.println("CSV.LFun.createInfoDotCsvFile()");
                writeToFile(new File(Pathes.Path.getbill_in() + "\\" + info.getName().substring(0, info.getName().length() - ".kwd".length()) + ";info.csv"), info_vec);
            }
            //}
        }
    }*/
    public static Vector<String> String2Array(String str) {
        Vector<String> v = new Vector<String>();
        StringTokenizer st = new StringTokenizer(str, ",");
        try {
            while (st.hasMoreTokens()) {
                v.add(st.nextToken().replaceAll("\"", ""));
            }
        } catch (Exception e) {
        }

        return v;
    }

    public static void main(String[] args) {
        System.out.println("1");
        //createInfoDotCsvFile();
        /*Vector<int[]> Coordinate = LFun.UnionHrz(LFun.readFile2vector(getFile("C:\\WiseBill\\WB\\bill_in\\A", ".hrz").getAbsolutePath(), "UTF-8"));
        //Vector<int[]> Coordinate = LFun.UnionVrt(LFun.readFile2vector(getFile("C:\\WiseBill\\WB\\bill_in\\A", ".vrt").getAbsolutePath(), "UTF-8"));
        System.out.println(Coordinate.size());
        for (int j = 0; j < Coordinate.size(); j++) {
            System.out.println(Arrays.toString(Coordinate.get(j)));
        }*/
    }
}
