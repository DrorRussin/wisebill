/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 *
 */
public class ConfigFun {
    
    //public static final String wiseBillFolderPath = System.getProperty("user.dir");
    public static final String wiseBillFolderPath = "C:\\HyperOCR\\WiseBill";

    private static final int ACCSIZE = 600;
    /**
     * res_ind - Integer variable for resolution index(init default = 0)
     */
    static public int res_ind = 0;
    /**
     * fontsize - Integer variable for size of browser font(init default = 16)
     */
    static public int fontsize = 16;
    /**
     * wadpas - String variable for admin password(init default = "era")
     */
    static public String wadpas = "era";
    /**
     * wuspref - String variable for current user prefix(init default = "era")
     */
    static public String wuspref = "era";

    /**
     *
     */
    static public String wwait = "";// wwait="אנא המתן, ממיר קובץ ל-PDF";
    /**
     *
     */
    static public String wer = "";// wer="לא ניתן להמיר פורמט זה ל-PDF";
    /**
     * wise_spar - String variable for search parameter "/SHOWDIR"(init default
     * = "")
     */
    static public String wise_spar = "";
    /**
     * wise_browser - String variable for path to showing folder of browser(init
     * default = "C:\\")
     */
    static public String wise_browser = "C:\\";
    /**
     * wise_bat - String variable for path to folder of scripts(.bat files)(init
     * default = "Source\\bat_files")
     */
    static public String wise_bat = "wrun_files";
    /**
     * wise_mailin - String variable for path to folder of incoming mails(.bat
     * files)(init default = "C:\\HyperOCR\\mailin")
     */
    static public String wise_mailin = "C:\\HyperOCR\\mailin";
    /**
     * wise_hotfolder_path - String variable for path to hotfolder programm(init
     * default = "unknown")
     */
    static public String wise_hotfolder_path = "unknown";
    /**
     * wise_mailout - String variable for path to folder of outgoing mails(.bat
     * files)(init default = "C:\\HyperOCR\\mailout")
     */
    static public String wise_mailout = "C:\\HyperOCR\\mailout";
    /**
     * wise_fil_dir - String variable for path to folder of WiseFiling
     * folders(init default = "c:\\")
     */
    static public String wise_fil_dir = "c:\\";
    /**
     * wise_show_dir - String variable for path to SHOW_DIR folder (init default
     * = "c:\\SHOW_DIR")
     */
    static public String wise_show_dir = "c:\\SHOW_DIR";
    /**
     * wise_scan_dir - String variable for path to scan folder(init default =
     * "C:\\Scan_Dir")
     */
    static public String wise_scan_dir = "C:\\Scan_Dir";
    /**
     * wise_monitor_path - String variable for path to WiseFind monitor
     * folder(init default = "c:\\Archive")
     */
    static public String wise_monitor_path = "c:\\Archive";

    /**
     * wisefind_path - String variable for path to WiseFind temporary (init
     * default = "c:\\Archive")
     */
    static public String wisefind_path = "c:\\Archive";
    /**
     * wise_folder_path - String variable for path to HyperOCR folder(init
     * default = "c:\\HyperOCR")
     */
    static public String wise_folder_path = "c:\\HyperOCR";
    /**
     * wise_doctemplte_path - String variable for path to doctemplate
     * folder(init default = wise_folder_path+"\\doctemplate")
     */
    static public String wise_doctemplte_path = wise_folder_path + "\\doctemplate";
    /**
     * wise_serverfol_path - String variable for path to server folder(init
     * default = wise_folder_path+"\\server")
     */
    static public String wise_serverfol_path = wise_folder_path + "\\server";
    /**
     * wise_trace_path - String variable for path to trace folder(init default =
     * wise_folder_path+"\\trace")
     */
    static public String wise_trace_path = wise_folder_path + "\\trace";
    /**
     * dir_trace - File variable for trace folder by wise_trace_path
     */
    static public File dir_trace = new File(wise_trace_path);
    /**
     * en=900(timeout),dn=15(delay),pn=0(select
     * driver),mn=1(multipage),un=1(user interface),an=0(degree),sf=add
     * stamp,rf=add reference,nf=add name;
     */

    static public int en = 900, dn = 15, pn = 0, mn = 1, un = 1, an = 0, sf = 0, rf = 0, nf = 0;

    static public String flag_scan = "false";
    static public String flag_optarchive = "false";
    static public String flag_optscan = "false";
    static public String flag_adv_scan = "false";

    static public String wise_stopscan = "/q";

    static public File numer = new File("Config\\defconf");
    /**
     * pcon - File variable for config_path file by "Config\\config_path"
     */

    static public File pcon = new File("Config\\config_path");
    /**
     * pconspecial - File variable for config_path file by
     * "Config\\config_special"
     */

    static public File pcon_sp = new File("Config\\config_special");
    /**
     * version
     */
    static public String version = "6.00";
    static String nprog = "Wisepage";
    /**
     * ver_info - File version in C:\hyperocr\versions
     */

    static public File ver_info;

    static public String kill1 = WiseConfig.Config.getWisePageExePath();
    static public String kill2 = WiseConfig.Config.getWiseCopyExePath();

    static public String ini1 = "NOINI.ini";
    static public String ini2 = "NOINI.ini";

// button name changing
    static public File bnames = new File("Config\\bnames");
    static Vector<String> butn = new Vector<String>();

    /**
     * string messages
     */
    static String w_mess1 = "Enter the password: ";
    static String w_mess2 = "Are you shure to delete choised files";
    static String w_mess3 = "Program will be restarted ";
    static String w_mess4 = "Please enter new password";
    static String w_mess5 = "Profile name";
    static String w_mess6 = "Not supported";
    static String w_mess7 = "Need fill all fields";
    static String w_mess8 = "first need define preffix";
    static String w_mess9 = "Input preffix";
    static String w_mess10 = "Input counter";
    static String w_mess11 = "Overwrite existing file ";
    static String w_mess12 = "remind folders= ";
    static String w_mess13 = "WiseFind no have license ";
    static String w_mess14 = "WiseSearch no have license ";
    static String w_mess15 = "Multiselection in developing for this action";
    static String w_mess16 = " in developing ";
    static String w_mess17 = "Error reading directory ";
    static String w_mess18 = "Invalid password. Try again.";
    static String w_mess19 = "temporary signed file will be removed: ";
    static String w_mess20 = "This action can be performed on image files only";
    static String w_mess21 = "Must fill name fields ";
    static String w_mess22 = "err:Folder isn't exist ";
    static String w_mess23 = "OverWrite user?";
    static String w_mess24 = "report for ";
    static String w_mess25 = "err:File isn't exist ";
    static String w_mess26 = "Please define OCR cycle commands ";
    static String w_mess27 = "Loading Files, Please wait.\nIf a lot of files are queue, this action may take time.";
    static String w_mess28 = "Restore Completed ";
    static String w_mess29 = "Back-up License does not Exist ";
    static String w_mess30 = "Script not Exist ";
    static String w_mess31 = "שינויים יוצגו לאחר לחיצה על \"עדכן\" במסך ERA_TECH הראשי.";

    /**
     *
     * @return list of running processes
     */
    /**
     * function define center of window
     *
     * @param dform this window
     * @return p - Point of center
     */
    static public Point calib_center(Dimension dform) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point p = new Point();
        p.x = (screenSize.width - dform.width) / 2;
        p.y = (screenSize.height - dform.height) / 2;
        // System.out.println(screenSize.height);
        if (screenSize.height > 720) {
            //  System.out.println(screenSize.height);
            p.y = (screenSize.height - dform.height) / 2 - 15;
        }
        return p;

    }

    static public void change_ini(String path_ini, String name_par, String val_par) throws FileNotFoundException, IOException {
        File fn = new File(path_ini);
        //File fn=new File(wise_fil_dir+"\\config\\Wise_ARCHIVE_VERIFY.ini");
        Vector<String> rfv = read_file2vector(fn);
        rfv = change_param(rfv, name_par, val_par);

        save_vector2file(rfv, fn);
    }

    /**
     * function read_file2vector(File fn) - read data from File fn to Vector rfv
     *
     * @param fn file
     * @return rfv String vector
     * @throws IOException
     */
    static public Vector<String> read_file2vector(File fn) throws IOException {
        Vector<String> rfv = new Vector<String>();

        FileInputStream fis = new FileInputStream(fn);

        int numberOfBytes = fis.available();
        byte vecByte[] = new byte[numberOfBytes];

        fis.read(vecByte, 0, numberOfBytes);
        String s = new String(vecByte);
        StringTokenizer st = new StringTokenizer(s, "\r\n", false);
        while (st.hasMoreTokens()) {
            rfv.add(st.nextToken());
        }

        fis.close();

        return rfv;
    }

    /**
     * function change_param(Vector<String> old,String s,String par) - change
     * String par in row s in Vector old
     *
     * @param old String vector
     * @param s row of file
     * @param par parameter
     * @return changed Vector old
     */
    static public Vector<String> change_param(Vector<String> old, String s, String par) {

        for (int i = 0; i < old.size(); i++) {
            if (old.elementAt(i).startsWith(s)) {
                old.set(i, s + par + "\r\n");
            }

        }
        return old;
    }

    /**
     * void function save_vector2file(Vector<String> rfv,File fn) - save Vector
     * rfv to File fn
     *
     * @param rfv String vector
     * @param fn file name
     * @throws FileNotFoundException
     * @throws IOException
     */
    static public void save_vector2file(Vector<String> rfv, File fn) throws FileNotFoundException, IOException {

        FileOutputStream fos = new FileOutputStream(fn, false);

        for (int i = 0; i < rfv.size(); i++) {
            String s = rfv.elementAt(i) + "\r\n";
            fos.write(s.getBytes());

        }
        fos.close();

    }

}
