/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import static CSV.CSV2Hash.Total_Lines;
import static CSV.CSV2Hash.config;
import Constructors.InfoReader;
import Constructors.ObData;
import java.util.LinkedList;
import java.util.Vector;

/**
 *
 * @author zvika
 */
public class FindItemPrices {

    private InfoReader _infoReader;
    private Vector<ObData> _od;
    private LinkedList<ObDataWithType> ObDataWithTypes;
    Vector<Vector<ObData>> _AllItemsOnTheInvoice;
    private String[] DiscountWords = {"discount", "discou", "הנחה", "הנחת", "הנחות", "זיכויים", "זיכוים", "זיכוי"};

    FindItemPrices(Vector<ObData> result, InfoReader infoReader, Vector<ObData> od, Vector<Vector<ObData>> AllItemsOnTheInvoice) {
        _infoReader = infoReader;
        _od = od;
        _AllItemsOnTheInvoice = AllItemsOnTheInvoice;

        //CSV2Hash.orderedItemsCSV
        ObDataWithTypes = new LinkedList<ObDataWithType>();
        if (result != null) {
            for (ObData r : result) {
                ObDataWithTypes.add(new ObDataWithType(r));
            }
        }
    }

    public Vector<ObData> getResult() {
        Total_Lines = new Vector<String>();

        //First Try - all ObData that Type is NULL:
        {
            Vector<ObData> Result = new Vector<ObData>();
            double sum = 0.0;
            for (int i = 0; i < ObDataWithTypes.size(); i++) {
                if (ObDataWithTypes.get(i).Type == null || (ObDataWithTypes.get(i).Type.equalsIgnoreCase("Discount") &&  Math.abs(Double.valueOf(ObDataWithTypes.get(i).O.get_result_str()))>0.51)) {
                    sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                    Result.add(ObDataWithTypes.get(i).O);
                } else if (ObDataWithTypes.get(i).Type.equalsIgnoreCase("Discount")) {
                    sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                } else {
                    Total_Lines.add(ObDataWithTypes.get(i).O.get_pfile_num() + "_" + ObDataWithTypes.get(i).O.get_line_num());
                }
            }

            Result = CompareAmountForGetResult(Result, sum);

            if (Result != null) {
                return Result;
            } else {
                Total_Lines.clear();
            }
        }

        //Secend Try - all ObData that Type is NULL until first 'total' or 'tax':
        {
            boolean first_total = false;
            Vector<ObData> Result = new Vector<ObData>();
            Vector<ObData> DiscountResult = new Vector<ObData>();
            double sum = 0.0;
            for (int i = 0; i < ObDataWithTypes.size(); i++) {
                if (ObDataWithTypes.get(i).Type == null || ObDataWithTypes.get(i).Type.equalsIgnoreCase("Discount")) {
                    if (!first_total) {
                        sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                        Result.add(ObDataWithTypes.get(i).O);
                    } else if (ObDataWithTypes.get(i).Type != null && ObDataWithTypes.get(i).Type.equalsIgnoreCase("Discount")) {
                        DiscountResult.add(ObDataWithTypes.get(i).O);
                    }
                } else if (ObDataWithTypes.get(i).Type.equalsIgnoreCase("Total") || ObDataWithTypes.get(i).Type.equalsIgnoreCase("Tax")) {
                    first_total = true;
                    Total_Lines.add(ObDataWithTypes.get(i).O.get_pfile_num() + "_" + ObDataWithTypes.get(i).O.get_line_num());
                } else {
                    Total_Lines.add(ObDataWithTypes.get(i).O.get_pfile_num() + "_" + ObDataWithTypes.get(i).O.get_line_num());
                }
            }
            for (int i = 0; i < DiscountResult.size() + 1; i++) {
                double discount_sum = 0.0;
                for (int j = 0; j < i; j++) {
                    discount_sum += Double.valueOf(DiscountResult.get(j).get_result_str());
                }
                Result = CompareAmountForGetResult(Result, sum + discount_sum);
                if (Result != null) {
                    return Result;
                } else {
                    Total_Lines.clear();
                }
            }

        }

        //Third Try - all ObData before Tax:
        {
            if (ObDataWithTypes.size() > 3) {
                Total_Lines.clear();
                boolean found_tax = false;
                Vector<ObData> Result = new Vector<ObData>();
                double sum = 0.0;
                for (int i = 0; i < ObDataWithTypes.size() && !found_tax; i++) {
                    if (ObDataWithTypes.get(i).Type != null && ObDataWithTypes.get(i).Type.equalsIgnoreCase("tax")) {
                        found_tax = true;
                        Total_Lines.add(ObDataWithTypes.get(i).O.get_pfile_num() + "_" + ObDataWithTypes.get(i).O.get_line_num());
                    } else if (Math.abs(Double.valueOf(ObDataWithTypes.get(i).O.get_result_str()) - sum) > 0.1) {
                        sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                        Result.add(ObDataWithTypes.get(i).O);
                    } else {
                        Total_Lines.add(ObDataWithTypes.get(i).O.get_pfile_num() + "_" + ObDataWithTypes.get(i).O.get_line_num());
                    }
                }
                Result = CompareAmountForGetResult(Result, sum);
                if (Result != null) {
                    return Result;
                } else {
                    Total_Lines.clear();
                }
            }
        }

        //Fourth Try - all ObData that Type is NULL until first 'sum wisepage':
        {
            Vector<ObData> Result = new Vector<ObData>();
            Vector<ObData> DiscountResult = new Vector<ObData>();
            double sum = 0.0;
            for (int i = 0; i < ObDataWithTypes.size(); i++) {
                if (ObDataWithTypes.get(i).Type == null) {
                    sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                    Result.add(ObDataWithTypes.get(i).O);
                } else {
                    if (CompareAmountForGetResult(Result, sum) != null) {
                        break;
                    } else {
                        sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                        Result.add(ObDataWithTypes.get(i).O);
                    }
                }
            }
            Result = CompareAmountForGetResult(Result, sum);
            if (Result != null) {
                return Result;
            } else {
                Total_Lines.clear();
            }
        }

        //Fifth Try - sum all:
        try {
            if (ObDataWithTypes.size() > 4) {
                Total_Lines.clear();
                boolean found_Result = false;
                Vector<ObData> Result = new Vector<ObData>();
                double sum = 0.0;
                for (int i = 0; i < ObDataWithTypes.size(); i++) {
                    if (!found_Result && ObDataWithTypes.get(i).Type == null || (ObDataWithTypes.get(i).Type != null && ObDataWithTypes.get(i).Type.equalsIgnoreCase("Discount"))) {
                        sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                        Result.add(ObDataWithTypes.get(i).O);
                    } else {
                        Total_Lines.add(ObDataWithTypes.get(i).O.get_pfile_num() + "_" + ObDataWithTypes.get(i).O.get_line_num());
                    }

                    if (i > 4 && CompareAmountForGetResult(Result, sum) != null) {
                        found_Result = true;
                        Result = CompareAmountForGetResult(Result, sum);
                    }
                }

                if (found_Result) {
                    return Result;
                } else {
                    Total_Lines.clear();
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        try {
//
            Vector<Object> need_to_replace = new Vector<Object>();
            for (int i = 0; i < ObDataWithTypes.size() - 1; i++) {
                if (ObDataWithTypes.get(i + 1).O.get_line_num() - ObDataWithTypes.get(i).O.get_line_num() > 1) {
                    boolean found = false;
                    int line_num = ObDataWithTypes.get(i).O.get_line_num() + 1;
                    for (int j = 0; j < _od.size() && !found; j++) {
                        ObData _obdata = _od.get(j);
                        if (_obdata.get_line_num() == line_num
                                && _obdata.get_result_str().length() >= 3
                                && !_obdata.get_result_str().contains(".")
                                && ObDataWithLines.twoLineMeet(_obdata.get_left(), _obdata.get_right(), ObDataWithTypes.get(i).O.get_left(), ObDataWithTypes.get(i).O.get_right())) {
                            found = true;

                            String old_s = _obdata.get_result_str();
                            String new_s = old_s.substring(0, old_s.length() - 2) + "." + old_s.substring(old_s.length() - 2, old_s.length());
                            _obdata.set_result_str(new_s);
                            ObDataWithTypes.add(i + 1, new ObDataWithType(_obdata));
                            Object[] need_to_replace_y = {_obdata, old_s};
                            need_to_replace.add(need_to_replace_y);

                            Vector<ObData> result = getResult();

                            if (result != null) {
                                System.out.println("@@@");
                                for (int t = 0; t < ObDataWithTypes.size(); t++) {

                                    System.out.println(ObDataWithTypes.get(t).O.get_result_str() + " is " + ObDataWithTypes.get(t).Type);
                                }
                                System.out.println("@@@");
                                return result;
                            }
                        }
                    }
                }
            }

            for (Object object : need_to_replace) {
                Object[] t = (Object[]) object;
                ((ObData) t[0]).set_result_str((String) t[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vector<ObData> CompareAmountForGetResult(Vector<ObData> Result, double sum) {
        if (Math.abs(sum) > 1) {
            try {
                if (Math.abs(sum - Double.valueOf(_infoReader.totalInvoiceSum13)) < 0.1
                        || Math.abs(sum - Double.valueOf(_infoReader.totalTaxes_Hayav_Bemaam9)) < 0.1
                        || (_infoReader.totalTaxes10 != null && !_infoReader.totalTaxes10.isEmpty() && Math.abs(sum - Double.valueOf(_infoReader.totalInvoiceSum13) + Double.valueOf(_infoReader.totalTaxes10)) < 0.3)) {
                    return Result;
                }
            } catch (Exception e) {
            }

            for (int i = 0; i < _AllItemsOnTheInvoice.size() - 1; i++) {//Without the last
                double sum_AllItemsOnTheInvoice_i = LFun.sumVec(_AllItemsOnTheInvoice.get(i));
                try {

                    if (sum_AllItemsOnTheInvoice_i > 0.0 && (Math.abs(sum + sum_AllItemsOnTheInvoice_i - Double.valueOf(_infoReader.totalInvoiceSum13)) < 0.1
                            || Math.abs(sum + sum_AllItemsOnTheInvoice_i - Double.valueOf(_infoReader.totalTaxes_Hayav_Bemaam9)) < 0.1)) {
                        {
                            int line_num = _AllItemsOnTheInvoice.get(i).get(_AllItemsOnTheInvoice.get(i).size() - 1).get_line_num() + 1;
                            int page_num = _AllItemsOnTheInvoice.get(i).get(_AllItemsOnTheInvoice.get(i).size() - 1).get_pfile_num();
                            for (; line_num <= 100; line_num++) {
                                Total_Lines.add(page_num + "_" + line_num);
                            }
                            page_num = Result.get(0).get_pfile_num();

                            for (int j = 0; j < Result.get(0).get_line_num(); j++) {
                                Total_Lines.add(page_num + "_" + j);
                            }
                        }
                        //Adding a vector from _AllItemsOnTheInvoice:
                        for (int j = _AllItemsOnTheInvoice.get(i).size() - 1; j >= 0; j--) {
                            Result.add(0, _AllItemsOnTheInvoice.get(i).get(j));
                        }

                        return Result;
                    }
                } catch (Exception e) {
                }
            }
            //use the orderedItemsSumPrices:
            for (double element : CSV2Hash.orderedItemsSumPrices) {
                if (Math.abs(sum - element) < 0.1) {
                    return Result;
                }
            }
        }
        return null;
    }

    private class ObDataWithType {

        String Type;
        ObData O;
        Vector<String> allLine = new Vector<String>();
        boolean containsInOrderedItemsPrices = false;

        ObDataWithType(ObData o) {
            this.O = o;
            try {
                containsInOrderedItemsPrices = CSV2Hash.orderedItemsPrices.contains(Double.valueOf(o.get_result_str()));
            } catch (Exception e) {
                containsInOrderedItemsPrices = CSV2Hash.orderedItemsPrices.contains(o.get_result_str());
            }

            for (int i = 0; i < _od.size(); i++) {
                if (O == _od.get(i)) {
                    for (int j = 0; j < _od.size(); j++) {
                        if (_od.get(i).get_line_num() == _od.get(j).get_line_num() && _od.get(i).get_pfile_num() == _od.get(j).get_pfile_num()) {
                            String word = _od.get(j).get_result_str().toLowerCase().replaceAll(":", "");
                            allLine.add(word);
                            //System.out.print(_od.get(j).get_result_str() + " | ");
                        }
                    }
                    break;
                }
            }

            int size = allLine.size();
            for (int i = 0; i < size; i++) {
                try {
                    allLine.add(allLine.get(i) + allLine.get(i + 1));
                    allLine.add(allLine.get(i + 1) + allLine.get(i));//for hebrew 
                } catch (Exception e) {
                }
            }
            //System.out.println();
            decisionType();
            System.out.println(this.O.get_result_str() + " is " + this.Type);
        }

        private void decisionType() {
            int line_num = 0;
            config.getTaxWords();

            //is Total:
            try {
                if ((Double.valueOf(_infoReader.totalInvoiceSum13) > 0 && Math.abs(Double.valueOf(O.get_result_str()) - Double.valueOf(_infoReader.totalInvoiceSum13)) < 0.1)
                        || Math.abs(Double.valueOf(O.get_result_str()) - Double.valueOf(_infoReader.totalPriceNoTaxes8)) < 0.1
                        || Math.abs(Double.valueOf(O.get_result_str()) - Double.valueOf(_infoReader.totalTaxes_Hayav_Bemaam9)) < 0.1) {
                    this.Type = "Total";
                    return;
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }

            //is Tax:
            try {
                if (Math.abs(Double.valueOf(O.get_result_str()) - Double.valueOf(_infoReader.totalTaxes10)) < 0.1) {
                    this.Type = "Tax";
                    return;
                }
                for (int i = 0; i < allLine.size(); i++) {
                    if (config.getTaxWords().contains(allLine.get(i).replaceAll("\"", "").replaceAll("-", ""))) {
                        this.Type = "Tax";
                        return;
                    }
                }
                if (allLine.contains("17.00%") || allLine.contains("%17.00")) {
                    this.Type = "Tax";
                    return;
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }

            //is discount:
            try {
                for (int i = 0; i < DiscountWords.length; i++) {
                    if (allLine.contains(DiscountWords[i]) && !allLine.contains("מחיר")) {
                        this.Type = "Discount";
                        if (!this.O.get_result_str().startsWith("-")) {
                            this.O.set_result_str("-" + this.O.get_result_str());
                        }
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //is discount:
            try {
                if (!containsInOrderedItemsPrices) {
                    double sum = 0;
                    double price = Double.valueOf(O.get_result_str());
                    try {
                        for (int i = ObDataWithTypes.size() - 1; i >= 0 && Math.abs(sum - price) > 0.05 && ObDataWithTypes.get(i).Type == null; i--) {
                            sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                        }
                        if (Math.abs(sum - price) < 0.05 && Math.abs(Double.valueOf(ObDataWithTypes.get(ObDataWithTypes.size() - 1).O.get_result_str()) - price) > 0.05) {
                            this.Type = "Temporary_Total";
                        } else {
                            this.Type = null;
                        }
                    } catch (Exception e) {
                    }
                    if (this.Type == null && ObDataWithTypes.size() > 1) {
                        try {
                            for (int i = 0; i < ObDataWithTypes.size() - 1; i++) {
                                sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                            }
                            if (Math.abs(sum - price) < 0.05 && Math.abs(Double.valueOf(ObDataWithTypes.get(ObDataWithTypes.size() - 1).O.get_result_str()) - price) > 0.05) {
                                this.Type = "Temporary_Total";
                            } else {
                                this.Type = null;
                            }
                        } catch (Exception e) {
                        }
                    }

                } else {
                    /*double sum = 0;
                    double price = Double.valueOf(O.get_result_str());
                    try {
                        for (int i = ObDataWithTypes.size() - 1; i >= 0 && Math.abs(sum - price) > 0.05 && ObDataWithTypes.get(i).Type == null; i--) {
                            sum += Double.valueOf(ObDataWithTypes.get(i).O.get_result_str());
                        }
                        if (Math.abs(sum - price) < 0.05 && Math.abs(Double.valueOf(ObDataWithTypes.get(ObDataWithTypes.size() - 1).O.get_result_str()) - price) > 0.05) {
                            this.Type = "Temporary_Total";
                        } else {
                            this.Type = null;
                        }
                    } catch (Exception e) {
                    }*/
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
