package CSV;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static CSV.Log.write;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.FileWriter;
import java.io.OutputStreamWriter;

/**
 *
 * @author ERA-SUP
 */
public class CsvHandlerReader {

    String csv_file_path;
    String csv_format;
    Vector<String[]> vectorArray;
    Vector<String> vector;
    String FILE_HEADER;
    boolean newFile = false;
    String type;
    public static String CSV_FORMAT = "ISO-8859-8";

    public CsvHandlerReader(String csv_file_path, String csv_format, String type) {
        if (csv_file_path == null) {
            return;
        }
        this.csv_file_path = csv_file_path;
        this.csv_format = csv_format;
        this.type = type;

        initialCsv();
        File f = new File(csv_file_path);
    }

    private void initialCsv() {
        File file = new File(csv_file_path);
        if (file.exists()) {
            //    convert2Unicode(csv_file_path);
            vector = fileToVector(csv_file_path, CSV_FORMAT);

            vectorArray = vectorToVectorArray();
        } else {

            vector = new Vector<String>();
            vectorArray = new Vector<String[]>();
            newFile = true;
        }
    }

    public void saveChanges() {
        vector = vectorArrayToVector();
        writeCsvFile();

    }

    public Vector<String[]> vectorToVectorArray() {
        Vector<String[]> list_of_string_array = new Vector<String[]>();

        for (int i = 1; i < vector.size(); i++) {
            String line = vector.elementAt(i);
            Pattern patt = Pattern.compile(",\"(.*)\",");
            Matcher m = patt.matcher(line);
            while (m.find()) {
                line = line.replace(m.group(1), m.group(1).replace(",", ";"));
            }
            String[] arr = line.split("\\,", -1);
            list_of_string_array.add(arr);
        }
        return list_of_string_array;
    }

    public Vector<String> vectorArrayToVector() {
        Vector<String> vec = new Vector<String>();
        vec.add(vector.get(0));
        for (int i = 0; i < vectorArray.size(); i++) {
            String vecLine = "";
            String[] vecArray = vectorArray.get(0);
            for (int j = 0; j < vecArray.length - 1; j++) {
                vecLine += vecArray[j] + ",";
            }
            if (vecLine.length() > 0) {
                vecLine += vecArray[vecArray.length - 1];
            }
            vec.add(vecLine);
        }
        return vec;
    }

    private Vector<String> fileToVector(String filePath, String Type) {
        File f = new File(filePath);
        String line;
        Vector<String> model = new Vector<String>();
        BufferedReader Reader;
        if (!f.exists()) {
            return model;
        }

        try {
            Reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), Type));

            line = Reader.readLine();
            while (line != null) {
                if (line == null || line.length() < 1) {
                    line = " ";
                }
                model.addElement(line);

                line = Reader.readLine();
            }

            Reader.close();
            //file.close();

        } catch (IOException ex) {
            System.err.println(ex);
            return model;
        }
        return model;
    }

    String csv_convertpath = "C:\\HyperOCR\\csv_convert.exe";

    private String convert2Unicode(String fn) {
        try {
            //clean old
            File oldcsu = new File(fn.substring(0, fn.length() - 4) + ".csu");
            if (oldcsu.exists()) {
                oldcsu.delete();
            }

            File convertedFile = new File(fn);

            //C:\HyperOCR\csv_convert.exe E:\temp\csv_convert\4.csv
            String command = space_fix(csv_convertpath) + " " + space_fix(convertedFile.getAbsolutePath());
            write("", "convert2Unicode", "", "command", true);
            Process p = Runtime.getRuntime().exec(command);
            //   System.out.println(command);
            int k = 0;
            try {
                p.waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(CsvHandlerReader.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fn.substring(0, fn.length() - 4) + ".csu";
        } catch (IOException ex) {
            ex.printStackTrace();

        }

        return null;
    }

    private String space_fix(String k) {
        k = "\"" + k + "\" ";
        // k=k+" ";
        return k;

    }

    public String getCsv_file_path() {
        return csv_file_path;
    }

    public String getCsv_format() {
        return csv_format;
    }

    public Vector<String[]> getVectorArray() {
        return vectorArray;
    }

    public Vector<String> getVector() {
        return vector;
    }

    public String getCsv_convertpath() {
        return csv_convertpath;
    }

    public void printVectorOfArrays() {
        if (vectorArray != null) {
            for (String[] vectorArray : vectorArray) {
                // System.out.println(Arrays.toString(vectorArray));
            }
        }
    }

    public void writeCsvFile1() {
        FileWriter writer;
        try {
            writer = new FileWriter(csv_file_path);
            //   writer.append(FILE_HEADER);

            for (String row : vector) {
                // System.out.println(row);

                writer.append(row + "\n");
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(CsvHandlerReader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void writeCsvFile() {
        String s = vector.get(0);
        //save csv:
        vector.clear();
        vector.add(s);
        for (String[] csv_line : vectorArray) {
            vector.addElement(StringArray2String(csv_line));
        }
        writeModelToFile(new File(csv_file_path), vector, CSV_FORMAT);
    }

    public static String StringArray2String(String[] arr) {
        String s = "";
        if (arr != null) {
            if (arr.length > 0) {
                s = arr[0];
            }
            for (int i = 1; i < arr.length; i++) {
                s += "," + arr[i];
            }
        }
        return s;
    }

    public static boolean writeModelToFile(File file, Vector<String> list, String Type) {
        FileWriter File_Writer;
        BufferedWriter Buffered_Writer;

        String line;
        int size = list.size();

        try {
            if (list == null) {
                return false;
            }
            if (file == null) {
                return false;
            }
            if (!file.exists()) {
                file.createNewFile();
            }
            if (!file.exists()) {
                return false;
            }

            Buffered_Writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), Type));

            for (int i = 0; i < size; i++) {
                line = list.get(i).toString();
                if (line != null && !line.equals("")) {
                    Buffered_Writer.write(line.trim());
                    Buffered_Writer.newLine();
                }
            }

            Buffered_Writer.close();

        } catch (IOException ex) {
            //ex.printStackTrace();
            System.err.println("Error in writeModelToFile("+file.getAbsolutePath()+")");
            return false;
        }
        return true;
    }

    public void createNewFileIfNotExist() {
        vector.add(FILE_HEADER);
        writeCsvFile();

    }
}
