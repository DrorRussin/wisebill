/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

/**
 *
 * @author YAEL-MOBILE
 */
public class IndexesOfCsv {

    private int indexOfFileNumber = -1; // מספר החשבונית בה סופק הפריט
    private int indexOfFileDate = -1; //תאריך החשבונית (בפורמט אחיד DD.MM.YYYY)
    private int indexOfItemLine = -1; //מספר השורה בחשבונית בה מופיע הפריט הרלבנטי
    private int indexOfPN = -1; // מספר קטלוגי של הפריט
    private int indexOfDescription = -1; //תיאור הפריט 
    private int indexOfInteranalCounter = -1; //מונה דפים/קילומטרים לגבי הפריט 
    private int indexOfCurrencyItem = -1; // //קוד מטבע של הפריט הרלבנטי(לדוגמא: USD אוILS)
    private int indexOfUnitPrice = -1; // //מחיר ליחידה (ללא מעמ) 
    private int indexOfItemQuantity = -1; // כמות הפריט
    private int indexOfItemTotalPriceExcludingVat = -1;  //מחיר הכמות שסופקה (ללא מעמ)
    private int indexOfItemVatSum = -1;  // המעמ בגין הפריט
    private int indexOfFileName = -1; // שם הקובץ
    private int indexOfVatPercentPerItem = -1; // אחוז המעמ לפריט
    private int indexOfBUDGET_CURRENCY = -1; //קוד המטבע שבו רשומים סכום החשבונית והתקציב
    public static int indexOfSupplierID = 13;//זיהוי הספק ב-ERP

    public IndexesOfCsv(MatchesToItems bnvWithMatchesToItems) {
        if (bnvWithMatchesToItems.isINVO || bnvWithMatchesToItems.isCreditInvoice) {
            indexOfFileNumber = 92; // מספר החשבונית בה סופק הפריט
            indexOfFileDate = 93; //תאריך החשבונית (בפורמט אחיד DD.MM.YYYY)
            indexOfItemLine = 94; //מספר השורה בחשבונית בה מופיע הפריט הרלבנטי
            indexOfPN = 95; // מספר קטלוגי של הפריט
            indexOfDescription = 96; //תיאור הפריט 
            indexOfInteranalCounter = 97; //מונה דפים/קילומטרים לגבי הפריט 
            indexOfCurrencyItem = 98; // //קוד מטבע של הפריט הרלבנטי(לדוגמא: USD אוILS)
            indexOfUnitPrice = 99; // //מחיר ליחידה (ללא מעמ) 
            indexOfItemQuantity = 100; // כמות הפריט
            indexOfItemTotalPriceExcludingVat = 101;  //מחיר הכמות שסופקה (ללא מעמ)
            indexOfItemVatSum = 102;  // המעמ בגין הפריט
            indexOfFileName = 104; // שם הקובץ
            indexOfBUDGET_CURRENCY = 114; //קוד המטבע שבו רשומים סכום החשבונית והתקציב
        } else if (bnvWithMatchesToItems.isProformalInvoice) {
            indexOfFileNumber = 79; // מספר החשבונית בה סופק הפריט
            indexOfFileDate = 80; //תאריך החשבונית (בפורמט אחיד DD.MM.YYYY)
            indexOfItemLine = 81; //מספר השורה בחשבונית בה מופיע הפריט הרלבנטי
            indexOfPN = 82; // מספר קטלוגי של הפריט
            indexOfDescription = 83; //תיאור הפריט 
            indexOfInteranalCounter = 84; //מונה דפים/קילומטרים לגבי הפריט 
            indexOfCurrencyItem = 85; // //קוד מטבע של הפריט הרלבנטי(לדוגמא: USD אוILS)
            indexOfBUDGET_CURRENCY = 114; //קוד המטבע שבו רשומים סכום החשבונית והתקציב
            indexOfUnitPrice = 86; // //מחיר ליחידה (ללא מעמ) 
            indexOfItemQuantity = 87; // כמות הפריט
            indexOfItemTotalPriceExcludingVat = 88;  //מחיר הכמות שסופקה (ללא מעמ)
            indexOfItemVatSum = 89;  // המעמ בגין הפריט
            indexOfFileName = 91; // שם הקובץ
        } else if (bnvWithMatchesToItems.isMISHFile || bnvWithMatchesToItems.isCreditMISHFile) {
            indexOfFileNumber = 52; // מספר תעודת המשלוח
            indexOfFileDate = 54; //תאריך תעודת המשלוח (בפורמט אחיד DD.MM.YYYY)
            indexOfItemLine = 53; //מספר השורה בה מופיע הפריט הרלבנטי
            indexOfItemQuantity = 55; // כמות הפריט
        } else if (bnvWithMatchesToItems.isPORDFile || bnvWithMatchesToItems.isCreditPORDFile) {
            indexOfFileNumber = 7; // מספר הזמנה
            indexOfFileDate = 9; //תאריך הזמנה (בפורמט אחיד DD.MM.YYYY)
            indexOfItemLine = 8; //מספר השורה בה מופיע הפריט הרלבנטי
            indexOfPN = -1; // מספר קטלוגי של הפריט
            indexOfDescription = 17; //תיאור הפריט 
            indexOfCurrencyItem = 22; // //קוד מטבע של הפריט הרלבנטי(לדוגמא: USD אוILS)
            indexOfUnitPrice = 23; // //מחיר ליחידה (ללא מעמ) 
            indexOfItemQuantity = 10; // כמות הפריט
            indexOfItemTotalPriceExcludingVat = 24;  //מחיר הכמות שסופקה (ללא מעמ)
            indexOfVatPercentPerItem = 25; // אחוז המעמ לפריט
        }
    }

    public int getIndexOfFileNumber() {
        return indexOfFileNumber;
    }

    public int getIndexOfSupplierID() {
        return indexOfSupplierID;
    }

    public int getIndexOfFileDate() {
        return indexOfFileDate;
    }

    public int getIndexOfItemLine() {
        return indexOfItemLine;
    }

    public int getIndexOfPN() {
        return indexOfPN;
    }

    public int getIndexOfDescription() {
        return indexOfDescription;
    }

    public int getIndexOfInteranalCounter() {
        return indexOfInteranalCounter;
    }

    public int getIndexOfCurrencyItem() {
        return indexOfCurrencyItem;
    }

    public int getindexOfBUDGET_CURRENCY() {
        return indexOfBUDGET_CURRENCY;
    }

    public int getIndexOfUnitPrice() {
        return indexOfUnitPrice;
    }

    public int getIndexOfItemQuantity() {
        return indexOfItemQuantity;
    }

    public int getIndexOfItemTotalPriceExcludingVat() {
        return indexOfItemTotalPriceExcludingVat;
    }

    public int getIndexOfItemVatSum() {
        return indexOfItemVatSum;
    }

    public int getIndexOfFileName() {
        return indexOfFileName;
    }

    public int getIndexOfVatPercentPerItem() {
        return indexOfVatPercentPerItem;
    }
}
