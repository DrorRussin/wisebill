/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import static CSV.CSV2Hash.isDigitalFile;
import static CSV.Log.write;
import Constructors.BNode;
import Constructors.InfoReader;
import Constructors.ObData;
import Constructors.VerVar;
import Constructors.VerVarWM;
import Pathes.Path;
import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author YAEL-MOBILE
 */
public class MatchesToItems {

    public Vector<MatchesToOneItem> MatchesToItems = new Vector<MatchesToOneItem>();
    public String[] matchInCsvToInvoiceA4 = null;
    private Vector<String[]> SuppliersCSV = null;
    private InfoReader infoReader = null;
    public boolean isINVO = false;
    public boolean isCreditInvoice = false;
    public boolean isProformalInvoice = false;
    public boolean isPORDFile = false;
    public boolean isMISHFile = false;
    public boolean isCreditMISHFile = false;
    public boolean isCreditPORDFile = false;
    public boolean isProformalError = false;
    public boolean isQUANTITY_MISMATCH = false;  // הכמות בחשבונית של פריט אחד לפחות גבוהה מהכמות שסופקה בפועל מהפריט.
    public boolean isPRICE_MISMATCH = false;
    public boolean isENTRY_MISMATCH = false;
    public boolean isSHIPPING_MISMATCH = false;
    public boolean isTRANSACTION_MISMATCH = false;
    //public boolean isPROFORMA_MISMATCH = false;
    public boolean isB2B_MISMATCH = false;
    public boolean isBUDGET_MISMATCH = false;
    public boolean isORDER_MISMATCH = false;
    public boolean isCURRENCY_CONVERSION_MISMATCH = false;
    public boolean isTOTAL_MISMATCH = false;
    public boolean isBeenArchive = false;
    public boolean SupplierFound = true;

    public MatchesToItems(Vector<BNode> bnv, String path, Vector<String[]> i_makatcsu, Vector<String[]> i_itemdesccsu,
            Vector<String[]> i_unitpricecsu, Vector<String[]> i_notcatnocsu, Vector<String[]> i_OrderedItemsCSV, Vector<String[]> i_SuppliersCSV, double i_Tolerance,
            Vector<Integer> i_INDEX_OF_PRICE, Vector<Integer> i_INDEX_OF_DESCRIPTION, Vector<Integer> i_INDEX_OF_PN, Vector<Integer> i_INDEX_OF_QTY, InfoReader i_InfoReader, boolean i_BeenArchived,
            VerVarWM i_Globalanahav, VerVar i_SumallNoMamv, VerVarWM i_Igulv, boolean i_isINVO, boolean i_IsCreditInvoice, boolean i_IsProformalInvoice,
            VerVar i_sumallWithMamv, boolean i_isMISHFile, boolean i_isPORDFile, boolean i_isCreditMISHFile, boolean i_isCreditPORDFile) {

        this.infoReader = i_InfoReader;
        this.isINVO = i_isINVO;
        this.isCreditInvoice = i_IsCreditInvoice;
        this.isProformalInvoice = i_IsProformalInvoice;
        this.isBeenArchive = i_BeenArchived;
        this.isPORDFile = i_isPORDFile;
        this.isMISHFile = i_isMISHFile;
        this.isCreditMISHFile = i_isCreditMISHFile;
        this.isCreditPORDFile = i_isCreditPORDFile;
        this.SuppliersCSV = i_SuppliersCSV;

        MatchesToOneItem.SupplierId = null;

        if (!isBeenArchive) {
            int mone_of_matches_is_fictitious = 0;
            for (BNode block : bnv) {
                MatchesToOneItem item = new MatchesToOneItem(block, i_makatcsu, i_itemdesccsu, i_unitpricecsu, i_notcatnocsu, i_OrderedItemsCSV, i_Tolerance, i_INDEX_OF_PRICE,
                        i_INDEX_OF_DESCRIPTION, i_INDEX_OF_PN, i_INDEX_OF_QTY, i_InfoReader, i_BeenArchived, i_Globalanahav, i_SumallNoMamv, bnv.size(), i_Igulv, i_isINVO, i_IsCreditInvoice, i_IsProformalInvoice, i_isMISHFile, i_isPORDFile, i_isCreditMISHFile, i_isCreditPORDFile);
                item.findMatch(MatchesToOneItem.MatchType.Regular);
                item.fillAllDetails();
                if (item.matches_is_fictitious) {
                    mone_of_matches_is_fictitious++;
                }
                MatchesToItems.add(item);
            }

            //Search for a single line for all items:
            {
                // int mone_of_matches_is_fictitious_option2 = 0;
                if (mone_of_matches_is_fictitious >= 1 && mone_of_matches_is_fictitious == bnv.size()) {
                    mone_of_matches_is_fictitious = 0;
                    Vector<MatchesToOneItem> MatchesToItems_option2 = new Vector<MatchesToOneItem>();
                    //MatchesToItems.clear();
                    for (BNode block : bnv) {
                        MatchesToOneItem item = new MatchesToOneItem(block, i_makatcsu, i_itemdesccsu, i_unitpricecsu, i_notcatnocsu, i_OrderedItemsCSV, i_Tolerance, i_INDEX_OF_PRICE,
                                i_INDEX_OF_DESCRIPTION, i_INDEX_OF_PN, i_INDEX_OF_QTY, i_InfoReader, i_BeenArchived, i_Globalanahav, i_SumallNoMamv, bnv.size(), i_Igulv, i_isINVO, i_IsCreditInvoice, i_IsProformalInvoice, i_isMISHFile, i_isPORDFile, i_isCreditMISHFile, i_isCreditPORDFile);
                        item.findMatch(MatchesToOneItem.MatchType.SingleLineForAllItems);
                        item.fillAllDetails();
                        MatchesToItems_option2.add(item);
                        if (item.matches_is_fictitious) {
                            //mone_of_matches_is_fictitious_option2++;
                            mone_of_matches_is_fictitious++;
                        }
                    }
                    if (mone_of_matches_is_fictitious == 0) {
                        MatchesToItems.clear();
                        MatchesToItems = MatchesToItems_option2;
                    }
                }
            }

            //Search for a many line for many items:
            {
                //int mone_of_matches_is_fictitious_option3 = 0;
                if (mone_of_matches_is_fictitious >= 1 && mone_of_matches_is_fictitious == bnv.size()) {
                    mone_of_matches_is_fictitious = 0;
                    Vector<MatchesToOneItem> MatchesToItems_option3 = new Vector<MatchesToOneItem>();

                    //MatchesToItems.clear();
                    for (BNode block : bnv) {
                        MatchesToOneItem item = new MatchesToOneItem(block, i_makatcsu, i_itemdesccsu, i_unitpricecsu, i_notcatnocsu, i_OrderedItemsCSV, i_Tolerance, i_INDEX_OF_PRICE,
                                i_INDEX_OF_DESCRIPTION, i_INDEX_OF_PN, i_INDEX_OF_QTY, i_InfoReader, i_BeenArchived, i_Globalanahav, i_SumallNoMamv, bnv.size(), i_Igulv, i_isINVO, i_IsCreditInvoice, i_IsProformalInvoice, i_isMISHFile, i_isPORDFile, i_isCreditMISHFile, i_isCreditPORDFile);
                        if (infoReader.SupplierID != null && infoReader.SupplierID.length() > 2) {
                            item.findMatch(MatchesToOneItem.MatchType.ManyLinesToManyLines);
                        } else {
                            item.findMatch(MatchesToOneItem.MatchType.ManyLinesToManyLinesNoSupp);
                        }
                        MatchesToItems_option3.add(item);
                        if (item.matches_is_fictitious) {
                            mone_of_matches_is_fictitious++;
                        }
                    }
                    if (mone_of_matches_is_fictitious == 0) {
                        try {
                            if (infoReader.SupplierID == null || infoReader.SupplierID.length() <= 2) {
                                infoReader.SupplierID = MatchesToItems_option3.get(0).matches.get(0).match[24];
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        MatchesToItems.clear();
                        MatchesToItems = MatchesToItems_option3;
                        matchToOrderedItemsCSVOneLine_ManyLines(bnv);

                        for (MatchesToOneItem _matches_to_one_item : MatchesToItems) {
                            _matches_to_one_item.fillAllDetails();
                        }
                    } else {
                        MatchesToItems_option3.clear();
                    }
                }
            }

            if (infoReader != null) {
                if (infoReader.storeID11.equals("@") || infoReader.storeID11 == null) {
                    fillSupplierDetails(tryToFindSupplier());
                }
            }

            //אם החשבונית לא זוהתה מראש כחשבונית זיכוי אז נבדוק:
            // אם לגבי כל הפריטים שבחשבונית קיימות רשומות זיכוי מתאימות  לחלוטין,
            // וגם  לא נמצאה התאמה לגבי כל הפריטים עם רשומות רגילות
            // רק אז נחליט שכנראה זו חשבונית זיכוי.
            if ((isINVO || isMISHFile || isPORDFile) && SupplierFound) {
                for (int i = 0; i < MatchesToItems.size(); i++) {
                    if (!MatchesToItems.elementAt(i).foundFullyCreditMatch) {
                        break;
                    } else if (MatchesToItems.elementAt(i).foundFullyCreditMatch && i == MatchesToItems.size() - 1) {
                        String orig_name = new File(path).getName().replace(".csu", "");
                        orig_name = orig_name.replace(".csv", "");
                        if (isINVO) {
                            isCreditInvoice = true;
                            isINVO = false;
                            renameTif(orig_name, "INVO", "INVC");
                            for (int j = 0; j < MatchesToItems.size(); j++) {
                                MatchesToItems.elementAt(j).isCreditInvoice = true;
                                MatchesToItems.elementAt(j).isINVO = false;
                            }
                        } else if (isMISHFile) {
                            isCreditMISHFile = true;
                            isMISHFile = false;
                            renameTif(orig_name, "SHPM", "SHPC");
                            for (int j = 0; j < MatchesToItems.size(); j++) {
                                MatchesToItems.elementAt(j).isCreditMISHFile = true;
                                MatchesToItems.elementAt(j).isMISHFile = false;
                            }
                        } else if (isPORDFile) {
                            isCreditPORDFile = true;
                            isPORDFile = false;
                            renameTif(orig_name, "PORD", "PORC");
                            for (int j = 0; j < MatchesToItems.size(); j++) {
                                MatchesToItems.elementAt(j).isCreditPORDFile = true;
                                MatchesToItems.elementAt(j).isPORDFile = false;
                            }
                        }
                    }
                }
            } else if (isProformalInvoice /*אם זו חשבונית עסקה*/) {
                for (int i = 0; i < MatchesToItems.size(); i++) {
                    if (!MatchesToItems.elementAt(i).allConnectToPrevInvoice) {
                        break;
                    } else if (MatchesToItems.elementAt(i).allConnectToPrevInvoice && MatchesToItems.size() - 1 == i) {
                        isProformalError = true;
                    }
                }
            }

            if (!isProformalError && SupplierFound) {
                for (int i = 0; i < MatchesToItems.size(); i++) {
                    MatchesToItems.elementAt(i).searchWhichMatchToItem();
                    MatchesToItems.elementAt(i).fillDetails();
                    set_MisMatchWithoutBudget(i_Tolerance, i_sumallWithMamv, isBeenArchive);
                }

                // התאמה כללית לחשבונית
                if (!checkIfThereIsMatches(MatchesToItems) && !i_BeenArchived) {
                    matchInCsvToInvoiceA4 = matchToNotCatCsu(i_notcatnocsu);
                }
            }
        }
    }

    private String[] tryToFindSupplier() {
        String[] supplier = null;
        String SupplierID = getSupplierIDFromMatches();
        if (infoReader != null) {
            if (infoReader.storeID11.equals("@") || infoReader.storeID11 == null) {
                if (allHaveMatches()) {
                    //String SupplierID = getSupplierIDFromMatches();
                    if (!SupplierID.equals("@")) {
                        supplier = findSupplierRowInCsv(SupplierID);
                    }
                }
            }
        }

        return supplier;
    }

    private void fillSupplierDetails(String[] SuppplierRow) {
        if (SuppplierRow != null && infoReader != null) {
            infoReader.storeID11 = SuppplierRow[0];
            infoReader.SupplierName = SuppplierRow[3];
            SupplierFound = true;
            for (MatchesToOneItem item : MatchesToItems) {
                item.infoReader.storeID11 = infoReader.storeID11;
                item.infoReader.SupplierName = infoReader.SupplierName;
            }
        } else {
            for (MatchesToOneItem item : MatchesToItems) {
                if (!item.matches_is_fictitious) {
                    item.removeMatches();
                }
            }
            SupplierFound = false;
        }
    }

    private String[] findSupplierRowInCsv(String SupplierID) {
        for (String[] sup : SuppliersCSV) {
            if (sup[0].equals(SupplierID)) {
                System.out.println("The supplier found");
                return sup;
            }
        }
        SupplierID = CSV2Hash.SuppliersId2ErpId(infoReader.SupplierDealerID);
        for (String[] sup : SuppliersCSV) {
            if (sup[0].equals(SupplierID)) {
                System.out.println("The supplier found");
                infoReader.SupplierDealerID = SupplierID;
                return sup;
            }
        }
        System.err.println("The supplier not found!");
        return null;
    }

    private String getSupplierIDFromMatches() {
        String supplierID = "@";
        boolean isSame = true;
        for (int i = 0; i < MatchesToItems.size() && isSame; i++) {
            for (int j = 0; j < MatchesToItems.elementAt(i).matches.size() && isSame; j++) {
                if (supplierID.equals("@")) {
                    supplierID = MatchesToItems.elementAt(i).matches.elementAt(j).match[24];
                } else if (!supplierID.equals(MatchesToItems.elementAt(i).matches.elementAt(j).match[24])) {
                    isSame = false;
                    supplierID = "@";
                }
            }
        }

        return supplierID;
    }

    private boolean allHaveMatches() {
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (MatchesToItems.elementAt(i).matches.isEmpty() || MatchesToItems.elementAt(i).matches == null
                    || MatchesToItems.elementAt(i).foundPartialMatchToDES || MatchesToItems.elementAt(i).foundPartialMatchToPN) {
                return false;
            }
        }
        return true;
    }

    private boolean checkIfThereIsMatches(Vector<MatchesToOneItem> MatchesToItems) {
        boolean haveMatches = false;
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (MatchesToItems.elementAt(i).matches.size() > 0) {
                haveMatches = true;
                break;
            }
        }

        return haveMatches;
    }

    private String[] matchToNotCatCsu(Vector<String[]> i_notcatnocsu) {
        String[] matchToNotCat = null;
        Vector<String[]> notcatnocsuWithoutDuplicate = deleteDuplicate(i_notcatnocsu);

        if (infoReader != null) {
            if (infoReader.Invoice_ID12 != null && !infoReader.Invoice_ID12.equals("@") && (isINVO || isCreditInvoice || isProformalInvoice)) {
                for (String[] v : notcatnocsuWithoutDuplicate) {
                    if (v[79].length() > 2 && isMatchInvoiceNumber(v[79])) {
                        matchToNotCat = v;
                    }
                }
            }
        }

        if (matchToNotCat == null) {
            Vector<String[]> matches = new Vector<String[]>();
            for (String[] v : notcatnocsuWithoutDuplicate) {
                if (v[103].equals("") && (isINVO || isCreditInvoice || isProformalInvoice)) {
                    matches.add(v);
                } //עבור תעודות משלוח- 
                //הרשומה המתאימה תהיה זו שבה לא מופיע מספר תעודת משלוח ובתנאי שהיא היחידה 
                else if (v[63].equals("") && (isMISHFile || isCreditMISHFile)) {
                    matches.add(v);
                    // עבור הזמנות- 
                    //הרשומה המתאימה תהיה זו שבה לא מופיע מספר הזמנה ובתנאי שהיא היחידה 
                } else if (v[18].equals("") && (isPORDFile || isCreditPORDFile)) {
                    matches.add(v);
                }
            }
            if (matches.size() == 1) {
                matchToNotCat = matches.get(0);
            }
        }
        if (matchToNotCat == null) {
            //צריך להוסיף השוואה עם המונה דפים וקילומטרים בעמודה לאחר שימומש בקוד 74
        }

        return matchToNotCat;
    }

    private Vector<String[]> deleteDuplicate(Vector<String[]> i_notcatnocsu) {
        //Vector<String[]> notcatnocsuWithoutDuplicate = notcatnocsu;
        Vector<String[]> notcatnocsuWithoutDuplicate = new Vector<String[]>(i_notcatnocsu);
        for (int i = 0; i < notcatnocsuWithoutDuplicate.size(); i++) {
            for (int j = 0; j < notcatnocsuWithoutDuplicate.size(); j++) {
                if (i != j) {
                    if (isSame(notcatnocsuWithoutDuplicate.get(i), notcatnocsuWithoutDuplicate.get(j))) {
                        notcatnocsuWithoutDuplicate.remove(j);
                        j--;
                    }
                }
            }
        }

        return notcatnocsuWithoutDuplicate;
    }

    private boolean isSame(String[] v1, String[] v2) {
        boolean isSame = true;
        if (v1.length == v2.length) {
            for (int i = 11; i < v1.length && isSame; i++) {
                if (!v1[i].equals(v2[i])) {
                    isSame = false;
                }
            }
        }

        return isSame;
    }

    /**
     * If the number in the csv is shorter then the number we found - we will do
     * match just with the right chars
     *
     * @param numberInCsv
     * @return true if match
     */
    private boolean isMatchInvoiceNumber(String numberInCsv) {
        boolean isMatch = false;
        if (infoReader.Invoice_ID12.length() == numberInCsv.length()) {
            isMatch = infoReader.Invoice_ID12.equals(numberInCsv);
        } else if (infoReader.Invoice_ID12.length() > numberInCsv.length()) {
            boolean toContinue = true;
            for (int i = numberInCsv.length() - 1; i > 0 && toContinue; i++) {
                if (numberInCsv.charAt(i) != infoReader.Invoice_ID12.charAt(i)) {
                    toContinue = false;
                }
            }
            if (toContinue) {
                isMatch = true;
            }
        }

        return isMatch;
    }

    public void set_BudgetMisMatch(boolean beenArchived, double DeviationForBudget, double PercentageOfBudget) {
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (matchInCsvToInvoiceA4 == null && !beenArchived && ((!isCreditInvoice && MatchesToItems.elementAt(i).BalanceOfBudget < (-1) * DeviationForBudget && (!LFun.checkIfIsLessThanPrecent(PercentageOfBudget, MatchesToItems.elementAt(i).budgetForAllorders, MatchesToItems.elementAt(i).BalanceOfBudget) && MatchesToItems.elementAt(i).BalanceOfBudget < 0))
                    || (isCreditInvoice && MatchesToItems.elementAt(i).BalanceOfBudget > DeviationForBudget && (!LFun.checkIfIsLessThanPrecent(PercentageOfBudget, MatchesToItems.elementAt(i).budgetForAllorders, MatchesToItems.elementAt(i).BalanceOfBudget))))) {
                isBUDGET_MISMATCH = true; //סכום החשבונית לא תוקצב או שהוא חורג מתקציב ההזמנה הרלבנטית.
            }
            if ((!MatchesToItems.elementAt(i).foundMatches && MatchesToItems.elementAt(i).matches.isEmpty()) || MatchesToItems.elementAt(i).matches_is_fictitious) {
                if (MismatchForSuppliers.single.isMismatch("ORDER_MISMATCH", infoReader.SupplierID)) {
                    isORDER_MISMATCH = true;
                }
            }
        }
        //cracking reamot 
        if (infoReader.SupplierID.equals("30202318")) {
            isBUDGET_MISMATCH = false;
        }
    }

    private void set_MisMatchWithoutBudget(double tolerance, VerVar VerVar, boolean beenArchived) {
        isQUANTITY_MISMATCH = false;  // הכמות בחשבונית של פריט אחד לפחות גבוהה מהכמות שסופקה בפועל מהפריט.
        isPRICE_MISMATCH = false; // המחיר בחשבונית, של פריט אחד לפחות – גבוה ממחיר הפריט בהזמנה או במחירון הרלבנטי.
        isB2B_MISMATCH = false;
        isBUDGET_MISMATCH = false;
        isORDER_MISMATCH = false;
        isCURRENCY_CONVERSION_MISMATCH = false;
        isTOTAL_MISMATCH = false;
        isENTRY_MISMATCH = false;
        isSHIPPING_MISMATCH = false;
        isTRANSACTION_MISMATCH = false;
        //isPROFORMA_MISMATCH = false;
        for (int i = 0; i < MatchesToItems.size(); i++) {
            try {
                double rate_block = Double.valueOf(MatchesToItems.elementAt(i).block.rate.get_result_str());
                double rate_matches = Double.valueOf(MatchesToItems.elementAt(i).matches.get(0).match[77]);
                if (Math.abs(rate_block - rate_matches) > 0.01) {
                    isCURRENCY_CONVERSION_MISMATCH = true;
                    MatchesToItems.elementAt(i).block.Percent_difference_rate_block_vs_matches = (float) ((Math.abs(rate_block - rate_matches) * 100) - 100);
                }
            } catch (Exception e) {
            }

            if (((MatchesToItems.elementAt(i).diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice > 0 && !isCreditInvoice)
                    || (MatchesToItems.elementAt(i).diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice < 0 && isCreditInvoice))
                    && !beenArchived) {
                isQUANTITY_MISMATCH = true; // הכמות בחשבונית של פריט אחד לפחות גבוהה מהכמות שסופקה בפועל מהפריט.
            }
            if (MatchesToItems.elementAt(i).diffrenceInPriceBetweenHazmanaAndInvoice > tolerance && !beenArchived) {
                isPRICE_MISMATCH = true; //המחיר בחשבונית של פריט אחד לפחות  גבוה ממחיר הפריט בהזמנה או במחירון הרלבנטי.
            }
            /*if(Math.abs(MatchesToItems.elementAt(i).diffrenceInPriceBetweenProformaInvoiceAndInvoice) > tolerance && Math.abs(MatchesToItems.elementAt(i).diffrenceInQtyBetweenProformaInvoiceAndInvoice) > tolerance && !beenArchived){
                isPROFORMA_MISMATCH = true; //קיימת אי זהות כלשהי בכמות או במחיר של אחד הפריטים בחשבונית מול הנתון המתאים בחשבונית העסקה אם אכן הוגשה קודם לכן.
            }*/
            if ((Math.abs(MatchesToItems.elementAt(i).diffrenceInPriceBetweenSupplierAndInvoice) > tolerance && MatchesToItems.elementAt(i).findSupplierPrice)
                    || (Math.abs(MatchesToItems.elementAt(i).diffrenceInQtyBetweenSupplierAndInvoice) > tolerance && MatchesToItems.elementAt(i).findSupplierQty) && !beenArchived) {
                if (MismatchForSuppliers.single.isMismatch("B2B_MISMATCH", infoReader.SupplierID)) {
                    isB2B_MISMATCH = true; //נתוני החשבונית אינם זהים לנתונים שהועברו עי הספק בצורה דיגיטאלית לגבי החשבונית.
                }
            }

            if (!VerVar.getVerifived() || VerVar.isCancel()) {
                isTOTAL_MISMATCH = true; // סהכ עלויות הפריטים בחשבונית אינם מסתכמים לסכום הכולל המצויין בחשבונית.
            }

            //cracking reamot 
            if (infoReader.SupplierID.equals("30202318") || infoReader.SupplierID.equals("512244302")) {
                isTOTAL_MISMATCH = false;
            }

            String[] match = null;

            try {
                match = MatchesToItems.elementAt(i).matches.get(0).match;
            } catch (Exception e) {
                e.printStackTrace();
                if (MismatchForSuppliers.single.isMismatch("ORDER_MISMATCH", infoReader.SupplierID)) {
                    isORDER_MISMATCH = true;
                }
                return;
            }

            try {
                if ((match[MismatchForSuppliers.ENTRY_MISMATCH_COLUMNS + 11] == null
                        || match[MismatchForSuppliers.ENTRY_MISMATCH_COLUMNS + 11].length() < 3)
                        && MismatchForSuppliers.single.isMismatch("ENTRY_MISMATCH", infoReader.SupplierID)) {
                    isENTRY_MISMATCH = true;
                }
            } catch (Exception e) {
            }
            try {
                if ((match[MismatchForSuppliers.SHIPPING_MISMATCH_COLUMNS + 11] == null
                        || match[MismatchForSuppliers.SHIPPING_MISMATCH_COLUMNS + 11].length() < 3)
                        && MismatchForSuppliers.single.isMismatch("SHIPPING_MISMATCH", infoReader.SupplierID)) {
                    isSHIPPING_MISMATCH = true;
                }
            } catch (Exception e) {
            }

            try {
                if ((match[MismatchForSuppliers.TRANSACTION_MISMATCH_COLUMNS + 11] == null
                        || match[MismatchForSuppliers.TRANSACTION_MISMATCH_COLUMNS + 11].length() < 3)
                        && MismatchForSuppliers.single.isMismatch("TRANSACTION_MISMATCH", infoReader.SupplierID)) {
                    isTRANSACTION_MISMATCH = true;
                }
            } catch (Exception e) {
            }
        }
    }

    public String getDocType() {
        String docType = "";
        if (isINVO) {
            docType = "INVO";
        } else if (isMISHFile) {
            docType = "SHPM";
        } else if (isPORDFile) {
            docType = "PORD";
        } else if (isProformalInvoice) {
            docType = "INVP";
        } else if (isCreditInvoice) {
            docType = "INVC";
        } else if (isCreditMISHFile) {
            docType = "SHPC";
        } else if (isCreditPORDFile) {
            docType = "PORC";
        }

        return docType;
    }

    public void setDocType(String docType) {
        isINVO = false;
        isMISHFile = false;
        isPORDFile = false;
        isProformalInvoice = false;
        isCreditInvoice = false;
        isCreditMISHFile = false;
        isCreditPORDFile = false;

        if (docType.equalsIgnoreCase("INVO")) {
            isINVO = true;
        } else if (docType.equalsIgnoreCase("SHPM")) {
            isMISHFile = true;
        } else if (docType.equalsIgnoreCase("PORD")) {
            isPORDFile = true;
        } else if (docType.equalsIgnoreCase("INVP")) {
            isProformalInvoice = true;
        } else if (docType.equalsIgnoreCase("INVC")) {
            isCreditInvoice = true;
        } else if (docType.equalsIgnoreCase("SHPC")) {
            isCreditMISHFile = true;
        } else if (docType.equalsIgnoreCase("PORC")) {
            isCreditPORDFile = true;
        } else {
            throw new RuntimeException("Error in setDocType");
        }
    }

    Vector<String> findUnuniqueWordsInDescr(Vector<BNode> bnv) {
        Vector<String> UnuniqueWordsInDescr = new Vector<String>();
        for (int i = 0; i < bnv.size(); i++) {
            for (int j = 0; j < bnv.size(); j++) {
                if (i != j) {
                    for (int i_desc = 0; bnv.get(i).descr != null && i_desc < bnv.get(i).descr.size(); i_desc++) {
                        for (int j_desc = 0; bnv.get(j).descr != null && j_desc < bnv.get(j).descr.size(); j_desc++) {
                            try {
                                if (bnv.get(i).descr.get(i_desc).get_result_str().equals(bnv.get(j).descr.get(j_desc).get_result_str())
                                        && !UnuniqueWordsInDescr.contains(bnv.get(i).descr.get(i_desc).get_result_str())) {
                                    UnuniqueWordsInDescr.add(bnv.get(i).descr.get(i_desc).get_result_str());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
            }
        }
        return UnuniqueWordsInDescr;
    }

    Vector<String> findUnuniqueWordsInOrderedItems(Vector<BNode> bnv, Vector<Match> orderedItemsLines) {
        Vector<String> UnuniqueWordsInOrderedItems = new Vector<String>();
        for (int i = 0; i < orderedItemsLines.size(); i++) {
            for (int j = 0; j < orderedItemsLines.size(); j++) {
                if (i != j) {
                    try {
                        StringTokenizer st_i = new StringTokenizer(orderedItemsLines.get(i).match[28], " ");
                        StringTokenizer st_j = new StringTokenizer(orderedItemsLines.get(j).match[28], " ");
                        while (st_i.hasMoreTokens()) {
                            String word_i = st_i.nextToken();

                            while (st_j.hasMoreTokens()) {
                                String word_j = st_j.nextToken();
                                if (word_i.equals(word_j) && !UnuniqueWordsInOrderedItems.contains(word_j)) {
                                    UnuniqueWordsInOrderedItems.add(word_i);
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        return UnuniqueWordsInOrderedItems;
    }

    public static void renameTif(String orig_name, String docTypeFrom, String docTypeToReplace) {
        File Invoice = new File(Path.getOutputFolder() + "\\" + orig_name + ".tif");
        if (Invoice.exists()) {
            File file2 = new File(Invoice.getAbsolutePath().replace(docTypeFrom, docTypeToReplace));
            Invoice.renameTo(file2);
        }
    }

    void matchToOrderedItemsCSVOneLine_ManyLines(Vector<BNode> bnv) {
        Vector<Match> orderedItemsLines = new Vector<Match>();
        for (int i = 0; i < MatchesToItems.get(0).matches.size(); i++) {
            orderedItemsLines.add(MatchesToItems.get(0).matches.get(i));
        }

        for (int i = 0; i < MatchesToItems.size(); i++) {
            MatchesToItems.get(i).matches.clear();
        }

        for (int t = 0; t < Math.pow(MatchesToItems.size(), 2) && !MatchesToItems.isEmpty(); t++) {
            //find ununique words in the descr:
            Vector<String> UnuniqueWordsInDescr = findUnuniqueWordsInDescr(bnv);

            //find Ununique words in the OrderedItems:
            Vector<String> UnuniqueWordsInOrderedItems = findUnuniqueWordsInOrderedItems(bnv, orderedItemsLines);
            for (int j = 0; j < orderedItemsLines.size(); j++) {
                ArrayList<Integer> foundMatcheIndexs = new ArrayList<Integer>();
                double orderedItems_price = -1;
                try {
                    orderedItems_price = Double.valueOf(orderedItemsLines.get(j).match[34]);
                } catch (Exception e) {
                }

                for (int i = 0; i < MatchesToItems.size(); i++) {
                    double block_price = -1;
                    if (MatchesToItems.get(i).block.price != null) {
                        try {
                            block_price = Double.valueOf(MatchesToItems.get(i).block.price.get_result_str().replaceAll(",", ""));
                        } catch (Exception e) {
                        }
                    }

                    double price_detected = 0;
                    for (Match matche : MatchesToItems.get(i).matches) {
                        price_detected += Double.valueOf(matche.match[35]);
                    }

                    if (block_price >= price_detected + orderedItems_price // If there is space in the item price
                            && orderedItems_price <= block_price && block_price % orderedItems_price == 0) {//If appropriate in amount and quantities
                        String orderedItems_desc = orderedItemsLines.get(j).match[28];
                        {
                            for (String ref : UnuniqueWordsInOrderedItems) {
                                orderedItems_desc = orderedItems_desc.replaceAll(ref, " ").replaceAll("  ", " ").trim();
                            }
                        }

                        String _matches_to_items_desc = MatchesToItems.get(i).block.get_description();
                        {
                            for (String ref : UnuniqueWordsInDescr) {
                                _matches_to_items_desc = _matches_to_items_desc.replaceAll(ref, " ").replaceAll("  ", " ").trim();
                            }
                        }

                        if (orderedItems_desc.contains(_matches_to_items_desc)
                                || _matches_to_items_desc.contains(orderedItems_desc)
                                || (_matches_to_items_desc.length() > 3
                                && orderedItems_desc.contains(_matches_to_items_desc.substring(_matches_to_items_desc.length() - 3, _matches_to_items_desc.length())))) {//אם מתאים לתיאור פריט
                            //System.out.println(orderedItems_desc + " Like " + _matches_to_items_desc);      
                            foundMatcheIndexs.add(i);
                        }
                    }
                }

                if (foundMatcheIndexs.size() == 1) {
                    MatchesToItems.get(foundMatcheIndexs.get(0)).matches.add(orderedItemsLines.get(j));
                    System.out.println(orderedItemsLines.get(j).match[28] + " add to " + foundMatcheIndexs.get(0));
                    orderedItemsLines.remove(j);
                    j--;
                } else {
                    //System.out.println(orderedItemsLines.get(j).match[28] + ": " + foundMatcheIndexs.size());
                }

            }
        }

        System.out.println(orderedItemsLines.size());

        //Second attempt - the first adjustment according to the amounts and quantities only
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (MatchesToItems.get(i).matches.size() == 0) {
                double block_price = -1;
                if (MatchesToItems.get(i).block.price != null) {
                    try {
                        block_price = Double.valueOf(MatchesToItems.get(i).block.price.get_result_str().replaceAll(",", ""));
                    } catch (Exception e) {
                    }
                }
                for (int j = 0; j < orderedItemsLines.size(); j++) {
                    double orderedItems_price = -1;
                    try {
                        orderedItems_price = Double.valueOf(orderedItemsLines.get(j).match[35]);
                    } catch (Exception e) {
                    }

                    double price_detected = 0;
                    for (Match matche : MatchesToItems.get(i).matches) {
                        price_detected += Double.valueOf(matche.match[34]);
                    }

                    if (block_price >= price_detected + orderedItems_price // If there is space in the item price
                            && orderedItems_price <= block_price && block_price % orderedItems_price == 0) {//If appropriate in amount and quantities
                        MatchesToItems.get(i).matches.add(orderedItemsLines.get(j));
                        System.out.println(orderedItemsLines.get(j).match[28] + " add to " + i);
                        orderedItemsLines.remove(j);
                        j = orderedItemsLines.size();
                    }
                }
            }
            if (MatchesToItems.get(i).matches.size() == 0) {
                System.err.println("not matches for " + i);
            }
        }

        System.out.println(orderedItemsLines.size());

        //Third attempt:
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (MatchesToItems.get(i).matches.size() == 0) {
                try {
                    MatchesToItems.get(i).matches.add(orderedItemsLines.get(0));
                    System.out.println(orderedItemsLines.get(0).match[28] + " add to " + i);
                    orderedItemsLines.remove(0);
                } catch (Exception e) {
                    //e.printStackTrace();

                    if (Double.valueOf(MatchesToItems.get(i).block.price.get_result_str()) < 0.51) {
                        write("", "matchToOrderedItemsCSVOneLine_ManyLines()", "", "Remove block number " + i + "; price is " + MatchesToItems.get(i).block.price, true);
                        MatchesToItems.remove(i--);
                    }
                }
            }
        }
        System.out.println(orderedItemsLines.size());
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (MatchesToItems.get(i).block.one_price == null || MatchesToItems.get(i).block.qty == null) {
                double price = 0;
                double Qty = 0;

                for (int j = 0; j < MatchesToItems.get(i).matches.size(); j++) {
                    price += Double.valueOf(MatchesToItems.get(i).matches.get(j).match[34]);
                    Qty += Double.valueOf(MatchesToItems.get(i).matches.get(j).match[21]);
                }

                if (Qty == 0) {
                    Qty = 1;
                }

                try {
                    if (MatchesToItems.get(i).block.qty == null) {
                        MatchesToItems.get(i).block.qty = new ObData();
                        MatchesToItems.get(i).block.qty.set_isFictitious(!isDigitalFile);
                        MatchesToItems.get(i).block.qty.set_isVerifived(true);
                        MatchesToItems.get(i).block.qty.set_result_str((Qty) + "");
                        MatchesToItems.get(i).block.qty.set_left(-1);
                        MatchesToItems.get(i).block.qty.set_right(-1);
                        MatchesToItems.get(i).block.qty.set_top(-1);
                        MatchesToItems.get(i).block.qty.set_bottom(-1);
                        System.out.println("MatchesToItems.get(" + i + ").block.qty=" + Qty + "(isFictitious)");
                    } else {
                        Qty = Double.valueOf(MatchesToItems.get(i).block.qty.get_result_str());
                        if (Qty <= 0) {
                            Qty = 1;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (MatchesToItems.get(i).block.one_price == null) {
                        MatchesToItems.get(i).block.one_price = new ObData();
                        MatchesToItems.get(i).block.one_price.set_isFictitious(!isDigitalFile);
                        MatchesToItems.get(i).block.one_price.set_isVerifived(true);
                        MatchesToItems.get(i).block.one_price.set_result_str((price / (Qty + 0.0)) + "");
                        MatchesToItems.get(i).block.one_price.set_left(-1);
                        MatchesToItems.get(i).block.one_price.set_right(-1);
                        MatchesToItems.get(i).block.one_price.set_top(-1);
                        MatchesToItems.get(i).block.one_price.set_bottom(-1);
                        System.out.println("MatchesToItems.get(" + i + ").block.one_price=" + (price / (Qty + 0.0)) + "(isFictitious)");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
