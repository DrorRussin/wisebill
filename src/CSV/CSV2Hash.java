package CSV;

//import static CSV.MatchesToOneItem.haveMatch;
import static CSV.SaveData.SaveData;
import Constructors.InfoReader;
import Constructors.CheckMam;
import Constructors.BNode;
import Check.CheckSingleQTY;
import Check.CheckQTY;
import Check.CheckSingleMam;
import Constructors.VerVar;
import Constructors.ObData;
import Constructors.Item;
import Constructors.VerVarWM;
import FT.FT;
import Management.SConsole;
import Management.SConsoleAdmin;
import Pathes.Path;
import WiseConfig.Config;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.StringTokenizer;
import java.lang.reflect.*;
import java.util.Vector;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static CSV.Log.write;
import java.lang.reflect.Field;

public class CSV2Hash extends CSV.Log {

    /*
     Vector<ObData> rightElements = new Vector<ObData>(),
     leftElements = new Vector<ObData>(),
     rightPricesElements = new Vector<ObData>(),
     leftPricesElements = new Vector<ObData>(),
     rightErrorElements = new Vector<ObData>(),
     leftErrorElements = new Vector<ObData>();
     */
    public static Config config = null;
    public static boolean isDigitalFile;
    public static boolean isHebFile;
    public static Vector<String> Total_Lines;
    public static boolean print_lines = false;

    Vector<Vector<ObData>> AllItemsOnTheInvoice = new Vector<Vector<ObData>>();

    //private String[] matchInCsvToInvoiceA4 = null;
    Vector<String> p_vnum = new Vector<String>();
    //ForPayWords,
    //QuantityWords,
    //QuantityWordsTwoWords,
    //UnitPriceWords,
    //UnitPriceTwoWords,
    //DescriptionWords,
    //HazmanaWords,
    //MishWords,
    //ClientPNWords,
    //ClientPNWords_TowWords,
    //ManufacturerPNWords,
    //SupplierPNWords,
    //QuantityInPack,
    //QuantityOfPacks,
    //ExchangeRateWords;
    boolean CreateAnlFile;
    boolean beenArchived;
    Vector<Double> biggest_numbers = new Vector<Double>(),
            biggestbetweenSARandLastBig = new Vector<Double>(),
            allbetweenSARandEnd = new Vector<Double>(),
            negativeVector = new Vector<Double>(),
            doublenum = new Vector<Double>();
    Vector<String[]> makatcsu = new Vector<String[]>();
    Vector<int[]> hrzCoordinate = new Vector<int[]>();
    Vector<int[]> vrtCoordinate = new Vector<int[]>();
    Vector<String[]> itemdesccsu = new Vector<String[]>();
    Vector<String[]> unitpricecsu = new Vector<String[]>();
    public static Vector<String[]> orderedItemsCSV = new Vector<String[]>();
    public static HashSet<Double> orderedItemsPrices = new HashSet<Double>();
    public static HashSet<Double> orderedItemsSumPrices = new HashSet<Double>();
    public static Vector<String[]> SuppliersCSV = new Vector<String[]>();
    Vector<ObData> ExchangeRatePrices = new Vector<ObData>();
    //Vector <String[]> hazmanacsu = new Vector<String[]>();
    //Vector <String[]> mishlochcsu = new Vector<String[]>();
    //Vector <String[]> serialcsu = new Vector<String[]>();
    //Vector <String[]> carscsu = new Vector<String[]>();
    //Vector <String[]> modelcsu = new Vector<String[]>();
    Vector<String[]> notcatnocsu = new Vector<String[]>();
    public Vector<String> typedig = new Vector<String>();
    Vector<String> mam_percents = new Vector<String>(),
            matbeim = new Vector<String>();
    public Vector<ObData> od = new Vector<ObData>(),
            usadAndEurdNum = new Vector<ObData>(),
            QuantityWordsVector = new Vector<ObData>(),
            QuantityTwoWordsVector = new Vector<ObData>(),
            UnitPriceWordsVector = new Vector<ObData>(),
            UnitPriceTwoWordsVector = new Vector<ObData>(),
            DescriptionWordsVector = new Vector<ObData>(),
            HazmanaWordsVector = new Vector<ObData>(),
            MishWordsVector = new Vector<ObData>(),
            ClientPNWordsVector = new Vector<ObData>(),
            ManufacturerPNWordsVector = new Vector<ObData>(),
            SupplierPNWordsVector = new Vector<ObData>(),
            QuantityInPackVector = new Vector<ObData>(),
            QuantityOfPacksVector = new Vector<ObData>(),
            ExchangeRateWordsVector = new Vector<ObData>(),
            MatbeyaAllWordsVector = new Vector<ObData>();
    Vector<Vector<ObData>> lines = new Vector<Vector<ObData>>();
    Vector<ObData> tempall = new Vector<ObData>();
    Vector<BNode> foundedBNodes = new Vector<BNode>();
    Vector<Hanaha> hanaha_Vector = new Vector<Hanaha>();
    Vector<Vector<String>> dictonary = new Vector<Vector<String>>();
    Vector<Integer> intnum = new Vector<Integer>();
    VerVar sumallWithMamv = new VerVar(0); // סכום עם מעמ
    VerVar sumrowpricesv = new VerVar(0),
            sumrowpricesHSv = new VerVar(0),
            sumGRandHSv = new VerVar(0),
            negpricesv = new VerVar(0), // סכום המחירים השליליים בחשבונית
            partscount = new VerVar(0); // מספר פריטים בחשבונית
    VerVar sumallMamv = new VerVar(0); // סכום המעמ
    VerVar sumallNoMamv = new VerVar(0), // סכום בלי מעמ
            sumallpaturv = new VerVar(0);// סכום פטור ממעמ
    VerVar sumallhayavv = new VerVar(0);
    Vector<ObData> sumall_for_Currency = new Vector<ObData>();//וקטור ובו כל הסכומים הסופיים, משמש כדי למצוא את קוד המטבע

    VerVarWM globalanahav = new VerVarWM(0),
            shipmentv = new VerVarWM(0),
            igulv = new VerVarWM(0);

    static FileOutputStream fs;
    String withmamappr = "unknown",
            rgnoise = "[~`'\"-,-.•;!?:״׳י]+",
            location = "global",
            matbeya = "שח",
            doc_direct = "un",
            path, format,
            cdel = ";",
            p_filename,
            Document_Type,
            DirectoryINI;
    static boolean is_log = true;
    boolean case5done = false,
            a4andDataexist = false,
            dataexist = false,
            rowPrConMam = false,
            isBigThenOne = true,
            isINVO = false,
            isCreditInvoice = false,
            isProformaInvoice = false,
            isMISHFile = false,
            isPORDFile = false,
            isCreditMISHFile = false,
            isCreditPORDFile = false;
    //isDigitalFile = false;
    public static boolean isA4 = false;
    InfoReader infoReader;
    Vector<Item> itemsVector;
    double tolerance = 0.01,
            negprices = 0,
            sumall = 0,
            duplicate_sumall = 0,
            sumallnomam = 0,
            duplicate_sumallnomam = 0,
            sumallpatur = 0,
            sumall2 = 0,
            tnaiIgul = 0.49,
            tolMam = 0,
            checkIfSUM = 0,
            PRECENT_FOR_ITEMS_CSV = 0,
            DeviationForBudget,
            PercentageOfBudget;
    int lineForDoubleSAR = 0,
            MODE = 0,
            l, r, t, b, countblock = 0,
            colDesc = 14,
            colMakat = 11,
            colMamPercent = 20,
            second = 0,
            iterforsumfind = 4,
            centerQTY = -1,
            centerOnePrice = -1,
            DEVIATION = 0,
            lengthMatrix[][], //Line 157   --- DEVIATION = 50
            INDEX_OF_PO_NUMBER = 0,
            INDEX_OF_ITEM_DATE_IN_PO = 0;
    Vector<Integer> INDEX_OF_PN = new Vector<Integer>(),
            INDEX_OF_PRICE = new Vector<Integer>(),
            INDEX_OF_DESCRIPTION = new Vector<Integer>(),
            INDEX_OF_QTY = new Vector<Integer>();
    public static String minlpn = "2",
            csv_convertpath = "C:\\HyperOCR\\csv_convert.exe";

    public static boolean pagea4 = false;
    public static int left12_5 = 0,
            right12_5 = 0;

    public CSV2Hash(String f, String ff, String filename, boolean _log) {
        try {

            //config = new Config();
            DEVIATION = config.getDeviation();
            INDEX_OF_PRICE = config.getIndexOfPrice();
            INDEX_OF_DESCRIPTION = config.getIndexOfDescription();
            INDEX_OF_PN = config.getIndexOfPn();
            INDEX_OF_QTY = config.getIndexOfQty();
            INDEX_OF_PO_NUMBER = config.getIndexOfPoNumber();
            INDEX_OF_ITEM_DATE_IN_PO = config.getIndexOfItemDateInPo();
            PRECENT_FOR_ITEMS_CSV = config.getPrecentForItemsCsv();
            //ForPayWords = config.getForPayWords();
            //QuantityWords = config.getQuantityWords();
            //QuantityWordsTwoWords = config.getQuantityWordsTwoWords();
            //UnitPriceWords = config.getUnitPriceWords();
            //UnitPriceTwoWords = config.getUnitPriceTwoWords();
            //DescriptionWords = config.getDescriptionWords();
            //ExchangeRateWords = config.getExchangeRateWords();
            //HazmanaWords = config.getHazmanaWords();
            //MishWords = config.getMishWords();
            //ClientPNWords = config.getClientPNWords();
            //ClientPNWords_TowWords = config.getClientPNWords_TowWords();
            //ManufacturerPNWords = config.getManufacturerPNWords();
            //SupplierPNWords = config.getSupplierPNWords();
            //QuantityInPack = config.getQuantityInPack();
            //QuantityOfPacks = config.getQuantityOfPacks();
            CreateAnlFile = config.getCreateAnlFile();
            Document_Type = config.getDocument_Type();
            DeviationForBudget = config.getDeviationForBudget();
            PercentageOfBudget = config.getPercentageOfBudget();
            DirectoryINI = config.getDirectoryINI();

            path = f;
            format = ff;

            loadCsvToCsu(_log, filename);

            //list of matbeim
            matbeim.add("שח");
            matbeim.add("חש");
            matbeim.add("ש''ח");
            matbeim.add("ש\"ח");
            matbeim.add("$");
            matbeim.add("NIS");
            matbeim.add("USD");
            matbeim.add("dollar");
            matbeim.add("₪");

            //read dictonary
            readDictonary();

            loadProperty();

        } catch (IOException ex) {
            Logger.getLogger(CSV2Hash.class.getName()).log(Level.SEVERE, null, ex);
        }

        //LFun.createInfoDotCsvFile(f.substring(0, f.length() - 4));
        try {
            /*   Get the information from csv   */
            //Document_Type must be here `invp:
            infoReader = new InfoReader(LFun.getFile(Pathes.Path.getbill_in(), ";info.csv").getAbsolutePath(), Document_Type);
            //infoReader = new InfoReader(f.substring(0, f.length() - 4) + ";info.csv", Document_Type);
        } catch (Exception e) {
            e.printStackTrace();
            write(p_filename, this.getClass().toString(), "Error can't read \"" + f.substring(0, f.length() - 4) + ";info.csv\"", "", is_log);

        }
    }

    public void createReportFile() {
        String[] filename = new String[22];
        filename[0] = new File(path).getName().replace(".csu", "") + ";";//1

        //filename[1] =;//2 מספר הלקוח הרלבנטי במערכת הממוחשבת ומימינו הסימן
        //filename[2] =;//3 התאריך בו צולמה התמונה בפורמט DD.MM.YYYY ומימינו הסימן
        //filename[3] =;//4 הזמן בו צולמה התמונה בפורמט HHMMSS ומימינו הסימן
        //filename[4] =;//5 הנ.צ. (GPS) של המיקום בו צולמה התמונה ומימינו הסימן
        //filename[5] =;//6 קידומת כלשהי שתציין את סוג המכשיר ממנו נשלחה התמונה (למשל, GX2).
        //filename[6] =;//7 בית העסק שהפיק את שובר הקופה כפי שמופיע במערכת הממוחשבת
        //filename[7] =;//8 מספר שובר הקופה
        //filename[8] =;//תאריך שובר הקופה
        //filename[9] =;//10 ההפקה של שובר הקופה, בפורמט HHMMSS 
        //filename[10] =;// 11.	מספר הקופה בה הודפס שובר הקופה
        filename[11] = partscount.toString() + ";";//12.	מספר הפריטים הכולל בשובר הקופה
        filename[12] = sumallWithMamv.toString() + ";";//13
        filename[13] = negpricesv.toString() + ";";//14
        if (shipmentv.getSum() == 0) {
            filename[14] = "0" + ";";//15
        } else {
            filename[14] = shipmentv.toString() + ";";//15
        }
        filename[15] = globalanahav.toString() + ";";//16
        filename[16] = sumallWithMamv.toString() + ";";//17
        //filename[17] = ;//18.	סה"כ סכום פטור ממע"מ.
        filename[18] = sumallNoMamv.toString() + ";";//19
        filename[19] = sumallMamv.toString() + ";";//20

    }

    void loadProperty() throws FileNotFoundException, IOException {
        //open configuration file in ...Config\\wocrec.properties
        String propFileName = ConfigFun.wiseBillFolderPath + "\\Config\\location\\" + location + "\\prop.properties";

        FileInputStream inputStream = new FileInputStream(propFileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "Unicode"));

        if (br == null) {
            //FT.theLogger.info("property file '" + propFileName + "' not found in the classpath");
            write(p_filename, this.getClass().toString(), "property file '" + propFileName + "' not found in the classpath", "", is_log);

            //System.exit(0);
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
        Properties prop = new Properties();
        prop.load(br);

        Date time = new Date(System.currentTimeMillis());

        // get the property value from configuration file
        csv_convertpath = prop.getProperty("csv_convertpath");
        cdel = prop.getProperty("cdel");
        minlpn = prop.getProperty("minlpn");
        matbeya = prop.getProperty("matbeya");
        doc_direct = prop.getProperty("doc_direct");
        rgnoise = prop.getProperty("rgnoise");
        colDesc = Integer.parseInt(prop.getProperty("colDesc"));
        colMakat = Integer.parseInt(prop.getProperty("colMakat"));
        colMamPercent = Integer.parseInt(prop.getProperty("colMamPercent"));
        tolerance = Double.parseDouble(prop.getProperty("tolerance"));
        tnaiIgul = Double.parseDouble(prop.getProperty("tnaiIgul"));
        tolMam = Double.parseDouble(prop.getProperty("tolMam"));

        String s = prop.getProperty("mam_percents");
        StringTokenizer str = new StringTokenizer(s, ",");
        while (str.hasMoreTokens()) {
            mam_percents.add(str.nextToken().toLowerCase());
        }

        String s2 = prop.getProperty("typedigit");
        StringTokenizer str2 = new StringTokenizer(s2, ",");
        while (str2.hasMoreTokens()) {
            typedig.add(str2.nextToken().toLowerCase());
        }
    }

    /**
     * clear old files from last load
     */
    public void clean() {
        File f = new File(path.substring(0, path.length() - 4) + cdel + "MAKAT.csv");
        if (f.exists()) {
            f.delete();
        }

        f = new File(path.substring(0, path.length() - 4) + cdel + "ITEM_DESCRIPTION.csv");
        if (f.exists()) {
            f.delete();
        }

        f = new File(path.substring(0, path.length() - 4) + cdel + "MAKAT.csu");
        if (f.exists()) {
            f.delete();
        }

        f = new File(path.substring(0, path.length() - 4) + cdel + "ITEM_DESCRIPTION.csu");
        if (f.exists()) {
            f.delete();
        }

        f = new File(path.substring(0, path.length() - 4) + ".csu");
        if (f.exists()) {
            f.delete();
        }

        //   f=new File(path);
        // if(f.exists()) {f.delete();}
    }

    /**
     * for search makat in vector of csu
     *
     * @param s word for search
     * @param col
     * @return full row
     */
    public String[] findInMakatCSU(String s, int col) {
        for (String[] rowsearch : makatcsu) {
            if (rowsearch[col].equalsIgnoreCase(s)) {
                return rowsearch;
            }
        }
        return null;
    }

    /**
     * for search Item Description in vector of csu
     *
     * @param s word for search
     * @return full row
     */
    public String[] findInItemDescriptionCSU(String s, int col) {
        for (String[] rowsearch : itemdesccsu) {
            if (rowsearch[col].equalsIgnoreCase(s)) {
                return rowsearch;
            }
        }
        return null;
    }

    /**
     * search Item Description in one line, maximum split 4 strings
     */
    void searchItemDescInLine() {
        //System.out.println("search desc ");

        int oneline = od.elementAt(0).get_line_num();
        Vector<ObData> descV = new Vector<ObData>();
        String temp = "";
        for (int i = 1; i < od.size(); i++) {

            for (int j = i - 1; j < od.size() && oneline == od.elementAt(j).get_line_num(); j++) {
                for (int k = 0; k < 4 && (j + k) < od.size() && oneline == od.elementAt(j).get_line_num(); k++) {
                    descV.add(od.elementAt(j + k));
                    temp = temp + od.elementAt(j + k).get_result_str();
                    String[] fulldesc = findInItemDescriptionCSU(temp, 0);
                    if (fulldesc != null) {

                        od.elementAt(j).set_result_str(fulldesc[colDesc]);
                        od.elementAt(j).set_name_by_inv("desc");
                        od.elementAt(j).set_external_source("ITEMDESC");
                        od.elementAt(j).set_left(Math.min(descV.elementAt(0).get_left(), descV.lastElement().get_left()));
                        od.elementAt(j).set_right(Math.max(descV.elementAt(0).get_right(), descV.lastElement().get_right()));
                        od.elementAt(j).set_format("TXT");//(LFun.checkFormat(fulldesc[colDesc]))
                        //System.out.println("format of " + fulldesc[colDesc] + " : " + od.elementAt(j).get_format());
                        while (k > 0) {
                            od.removeElementAt(j + k);
                            k--;
                        }

                        k = 4;

                    }

                }
                descV.removeAllElements();
                temp = "";

            }

            oneline = od.elementAt(i).get_line_num();

        }
    }

    /**
     * load makat and descriptions files
     */
    public void loadCsvToCsu(boolean _log, String filename) {
        //System.exit(0);

        p_filename = filename;
        is_log = _log;

        //read hrzCoordinate:
        try {
            File file = LFun.getFile(Path.getbill_in_a(), ".hrz");
            if (file != null) {
                hrzCoordinate = LFun.UnionHrz(LFun.readFile2vector(file.getAbsolutePath(), "UTF-8"));
            } else {
                write(p_filename, this.getClass().toString(), file + " file is not exists", "", is_log);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //read vrtCoordinate:
        try {
            File file = LFun.getFile(Path.getbill_in_a(), ".vrt");
            if (file != null) {
                vrtCoordinate = LFun.UnionVrt(LFun.readFile2vector(file.getAbsolutePath(), "UTF-8"));
            } else {
                write(p_filename, this.getClass().toString(), file + " file is not exists", "", is_log);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //read makat and descriptions filesdfg
        File file = new File(path.substring(0, path.length() - 4) + cdel + "MAKAT.csv");
        Vector<String> v;
        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "MAKAT.csv", format);

            // makatcsu
            for (int i = 0; i < v.size(); i++) {
                Pattern patt = Pattern.compile(",\"(.*)\",");
                Matcher m = patt.matcher(v.elementAt(i));
                while (m.find()) {
                    v.set(i, v.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
                }
                String[] arr = v.elementAt(i).split("\\,", -1);
                makatcsu.add(arr);
            }
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "MAKAT.csv" + " file OK", "", is_log);
        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "MAKAT.csv" + " file is not exists", "", is_log);

        }

        file = new File(path.substring(0, path.length() - 4) + cdel + "ITEM_CATNO.csv");
        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "ITEM_CATNO.csv", format);
            // makatcsu
            for (int i = 0; i < v.size(); i++) {
                try {

                    String line = v.elementAt(i);
                    //line = line.substring(0, line.length() - 2);
                    Pattern patt = Pattern.compile(",\"(.*)\",");
                    Matcher m = patt.matcher(line);
                    while (m.find()) {
                        line = line.replace(m.group(1), m.group(1).replace(",", ";"));
                    }
                    String[] arr = line.split("\\,", -1);

                    if (arr[11].equals("תעודת כניסה") || arr[12].equals("מספר הפריט בתעודת הכניסה") || arr[13].equals("תאריך חשבונית העסקה")) {
                        //error in wisepage.exe
                        write(p_filename, "loadCsvToCsu()", "", "Error in the line of " + "ITEM_CATNO.csv file", true);
                    } else {
                        arr = SaveLastFields.add_ITEM_CATNO(arr);
                        makatcsu.add(arr);
                    }
                } catch (Exception e) {
                    write(p_filename, "loadCsvToCsu()", "", "Error in the line of ITEM_CATNO.csv file", true);
                    //e.printStackTrace();
                }

            }
            makatcsu = deleteDuplicate(makatcsu);
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "ITEM_CATNO.csv" + " file OK", "", is_log);
        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "ITEM_CATNO.csv" + " file is not exists", "", is_log);

        }

        file = new File(path.substring(0, path.length() - 4) + cdel + "ITEM_DESCRIPTION.csv");
        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "ITEM_DESCRIPTION.csv", format);

            // itemdesccsu
            for (int i = 0; i < v.size(); i++) {
                Pattern patt = Pattern.compile(",\"(.*)\",");
                Matcher m = patt.matcher(v.elementAt(i));
                while (m.find()) {
                    v.set(i, v.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
                }
                String[] arr = v.elementAt(i).split("\\,", -1);
                itemdesccsu.add(arr);

            }
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "ITEM_DESCRIPTION.csv" + " file OK", "", is_log);
        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "ITEM_DESCRIPTION.csv" + " file is not exists", "", is_log);

        }

        file = new File(path.substring(0, path.length() - 4) + cdel + "ITEM_DESC.csv");
        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "ITEM_DESC.csv", format);

            // itemdesccsu
            for (int i = 0; i < v.size(); i++) {
                try {

                    String line = v.elementAt(i);
                    //line = line.substring(0, line.length() - 2);
                    Pattern patt = Pattern.compile(",\"(.*)\",");
                    Matcher m = patt.matcher(line);
                    while (m.find()) {
                        line = line.replace(m.group(1), m.group(1).replace(",", ";"));
                    }
                    String[] arr = line.split("\\,", -1);

                    arr = SaveLastFields.add_ITEM_CATNO(arr);

                    itemdesccsu.add(arr);
                } catch (Exception e) {
                    write(p_filename, "loadCsvToCsu()", "", "Error in the line of ITEM_DESC.csv file", true);
                    //e.printStackTrace();
                }
            }
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "ITEM_DESC.csv" + " file is exists", "", is_log);
        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "ITEM_DESC.csv" + " file is not exists", "", is_log);

        }

        file = new File(path.substring(0, path.length() - 4) + cdel + "UNIT_PRICE.csv");
        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "UNIT_PRICE.csv", format);

            // UnitPricecsu
            for (int i = 0; i < v.size(); i++) {
                Pattern patt = Pattern.compile(",\"(.*)\",");
                Matcher m = patt.matcher(v.elementAt(i));
                while (m.find()) {
                    v.set(i, v.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
                }
                String[] arr = v.elementAt(i).split("\\,", -1);
                unitpricecsu.add(arr);

            }
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "UNIT_PRICE.csv" + " file is exists", "", is_log);
        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "UNIT_PRICE.csv" + " file is not exists", "", is_log);

        }

        file = new File(path.substring(0, path.length() - 4) + cdel + "PO_UNIT_PRICE.csv");
        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "PO_UNIT_PRICE.csv", format);

            // UnitPricecsu
            for (int i = 0; i < v.size(); i++) {
                Pattern patt = Pattern.compile(",\"(.*)\",");
                Matcher m = patt.matcher(v.elementAt(i));
                while (m.find()) {
                    v.set(i, v.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
                }
                String[] arr = v.elementAt(i).split("\\,", -1);
                unitpricecsu.add(arr);

            }
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "PO_UNIT_PRICE.csv" + " file is exists", "", is_log);
        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "PO_UNIT_PRICE.csv" + " file is not exists", "", is_log);

        }

        file = new File(path.substring(0, path.length() - 4) + cdel + "NOT_CATNO.csv");
        File file1 = new File(path.substring(0, path.length() - 4) + cdel + "PO_or_PQ_or_BOL_or_LICENSE_or_CHASIS_or_SERIAL_or_MODEL_or_IMPORTNUM.csv");

        if (file.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "NOT_CATNO.csv", format);

            // NOT_CATNO.csu
            for (int i = 0; i < v.size(); i++) {
                Pattern patt = Pattern.compile(",\"(.*)\",");
                Matcher m = patt.matcher(v.elementAt(i));
                while (m.find()) {
                    v.set(i, v.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
                }
                String[] arr = v.elementAt(i).split("\\,", -1);
                notcatnocsu.add(arr);

            }
        } else if (file1.exists()) {
            v = LFun.file2vector(path.substring(0, path.length() - 4) + cdel + "PO_or_PQ_or_BOL_or_LICENSE_or_CHASIS_or_SERIAL_or_MODEL_or_IMPORTNUM.csv", format);

            // NOT_CATNO.csu
            for (int i = 0; i < v.size(); i++) {
                Pattern patt = Pattern.compile(",\"(.*)\",");
                Matcher m = patt.matcher(v.elementAt(i));
                while (m.find()) {
                    v.set(i, v.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
                }
                String[] arr = v.elementAt(i).split("\\,", -1);
                notcatnocsu.add(arr);

            }
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "NOT_CATNO.csv" + " file is exists", "", is_log);

        } else {
            write(p_filename, this.getClass().toString(), path.substring(0, path.length() - 4) + cdel + "NOT_CATNO.csv" + " file is not exists", "", is_log);

        }
    }

    /*
     * read from file dictonary by filename
     */
    public boolean readDictonary() {
        /*
         * read from file dictonary
         */

        Vector<String> one_price_dict = null;
        try {
            one_price_dict = LFun.file2vector(ConfigFun.wiseBillFolderPath + "\\Config\\location\\dictonaryOnePrice.csv", format);
        } catch (Exception e) {
            write(p_filename, this.getClass().toString(), "can't find " + ConfigFun.wiseBillFolderPath + "\\Config\\location\\dictonaryOnePrice.csv", "function: \"readDictonary\"", is_log);
        }
        //PRICE_PER_UNIT,ליחידה,unitprice,unit
        /* one_price_dict.add("PRICE_PER_UNIT");
         one_price_dict.add("ליחידה");
         one_price_dict.add("unitprice");
         one_price_dict.add("unit");
         one_price_dict.add("");
         one_price_dict.add("");
         one_price_dict.add("");
         */

        Vector<String> qty_dict = new Vector<String>();
        try {
            qty_dict = LFun.file2vector(ConfigFun.wiseBillFolderPath + "\\Config\\location\\dictonaryQTY.csv", format);
        } catch (Exception e) {
            write(p_filename, this.getClass().toString(), "can't find " + ConfigFun.wiseBillFolderPath + "\\Config\\location\\dictonaryQTY.csv", "function: \"readDictonary\"", is_log);

        }
        //QUANTITY,כמות,במות,quantity,quantite,units
        /* qty_dict.add("QUANTITY");
         qty_dict.add("כמות");
         qty_dict.add("במות");
         qty_dict.add("quantity");
         qty_dict.add("quantite");
         qty_dict.add("units");
         */

        dictonary.add(one_price_dict); // index 0 = dictonary for one_price
        dictonary.add(qty_dict); // index 1 = dictonary for qty

        return true;
    }

    /*
     * index 0 = dictonary for one_price
     * index 1 = dictonary for qty
     */
    public int getHeaderCenterByDictIndex(int ind, ObData ob) {
        int coord = -1;
        if (dictonary.elementAt(ind) == null || dictonary.elementAt(ind).size() == 0) {
            return coord;
        }

        for (int i = 0; i < dictonary.elementAt(ind).size(); i++) {
            if (dictonary.elementAt(ind).elementAt(i).equals(ob.get_result_str())) {
                int xmin = Math.min(ob.get_left(), ob.get_right());
                int xmax = Math.max(ob.get_left(), ob.get_right());
                coord = (int) ((xmax + xmin) / 2);
                return coord;
            }

        }

        return coord;
    }

    /**
     * remove noise from left and right side, move LP++ and RP--
     */
    public void removenoisefromLP_RP() {
        rgnoise = "^" + rgnoise + "$";  //"[~`'\"-,]+"
        Pattern p = Pattern.compile(rgnoise);

        String directstart = "u";
        String directend = "u";
        for (int k = 0; k < od.size(); k++) {

            if (od.elementAt(k).get_position().equalsIgnoreCase("LP")) {
                directstart = "LP";
                directend = "RP";
                k = od.size();
            } else if (od.elementAt(k).get_position().equalsIgnoreCase("RP")) {
                directstart = "RP";
                directend = "LP";
                k = od.size();
            }
        }

        int start = -1;
        int end = -1;
        for (int k = 0; k < od.size(); k++) {

            if (od.elementAt(k).get_position().equalsIgnoreCase(directstart)) {
                start = k;
            } else if (od.elementAt(k).get_position().equalsIgnoreCase(directend)) {
                end = k;
            } else if (od.elementAt(k).get_position().equalsIgnoreCase("MP")) {
                Matcher m = p.matcher(od.elementAt(k).get_result_str());
                if (m.matches()) {
                    od.remove(k--);
                }

            }

            if (start >= 0 && end >= 0 && start < end) {
                for (int i = start; i < end; i++) {
                    try {
                        if (od.elementAt(i).get_result_str() == null || od.elementAt(i).get_result_str().isEmpty()) {
                            od.remove(i);
                            end--;
                            k--;
                        } else {
                            Matcher m = p.matcher(od.elementAt(i).get_result_str());
                            if (od.elementAt(i).get_position().equalsIgnoreCase(directstart) && m.matches()) {

                                if (od.elementAt(i + 1) != null && od.elementAt(i + 1).get_position().equalsIgnoreCase(directend)) {
                                    Matcher mlast = p.matcher(od.elementAt(i + 1).get_result_str());
                                    if (mlast.matches()) {
                                        od.remove(i + 1);
                                        od.remove(i--);
                                        end--;
                                        k--;
                                    } else {
                                        od.elementAt(i + 1).set_position("MP");
                                        od.remove(i--);
                                        end--;
                                        k--;
                                    }

                                } else {
                                    od.elementAt(i + 1).set_position(directstart);
                                    od.remove(i--);
                                    end--;
                                    k--;
                                }
                            } else if (od.elementAt(i + 1).equals("%") && od.elementAt(i + 1) != null && od.elementAt(i + 1).get_position().equalsIgnoreCase(directend)) {
                                Matcher mlast = p.matcher(od.elementAt(i + 1).get_result_str());
                                if (mlast.matches()) {
                                    od.remove(i + 1);
                                    end--;
                                    k--;
                                    if (od.elementAt(i).get_position().equalsIgnoreCase(directstart)) {
                                        od.elementAt(i).set_position("MP");
                                    } else {
                                        od.elementAt(i).set_position(directend);
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                start = -1;
                end = -1;

            }
        }

        mirror();

        directstart = "u";
        directend = "u";
        for (int k = 0; k < od.size(); k++) {

            if (od.elementAt(k).get_position().equalsIgnoreCase("LP")) {
                directstart = "LP";
                directend = "RP";
                k = od.size();
            } else if (od.elementAt(k).get_position().equalsIgnoreCase("RP")) {
                directstart = "RP";
                directend = "LP";
                k = od.size();
            }
        }

        start = -1;
        end = -1;
        for (int k = 0; k < od.size(); k++) {

            if (od.elementAt(k).get_position().equalsIgnoreCase(directstart)) {
                start = k;
            } else if (od.elementAt(k).get_position().equalsIgnoreCase(directend)) {
                end = k;
            } else if (od.elementAt(k).get_position().equalsIgnoreCase("MP")) {
                Matcher m = p.matcher(od.elementAt(k).get_result_str());
                if (m.matches()) {
                    od.remove(k--);
                }

            }

            if (start >= 0 && end >= 0 && start < end) {
                for (int i = start; i < end; i++) {
                    Matcher m = p.matcher(od.elementAt(i).get_result_str());
                    if (od.elementAt(i).get_position().equalsIgnoreCase(directstart) && m.matches()) {

                        if (od.elementAt(i + 1) != null && od.elementAt(i + 1).get_position().equalsIgnoreCase(directend)) {
                            Matcher mlast = p.matcher(od.elementAt(i + 1).get_result_str());
                            if (mlast.matches()) {
                                od.remove(i + 1);
                                od.remove(i--);
                                end--;
                                k--;
                            } else {
                                od.elementAt(i + 1).set_position("MP");
                                od.remove(i--);
                                end--;
                                k--;
                            }

                        } else {
                            od.elementAt(i + 1).set_position(directstart);
                            od.remove(i--);
                            end--;
                            k--;
                        }
                    } else if (!od.elementAt(i + 1).get_result_str().equals("%") && od.elementAt(i + 1) != null && od.elementAt(i + 1).get_position().equalsIgnoreCase(directend)) {
                        Matcher mlast = p.matcher(od.elementAt(i + 1).get_result_str());
                        if (mlast.matches()) {
                            od.remove(i + 1);
                            end--;
                            k--;
                            if (od.elementAt(i).get_position().equalsIgnoreCase(directstart)) {
                                od.elementAt(i).set_position("MP");
                            } else {
                                od.elementAt(i).set_position(directend);
                            }
                        }

                    }

                }
                start = -1;
                end = -1;

            }
        }

        mirror();

    }

    public void mirror() {
        Vector<ObData> odmirror = new Vector<ObData>();
        for (int i = od.size() - 1; i >= 0; i--) {
            odmirror.add(od.elementAt(i));

        }
        od = odmirror;
    }

    public void printodbypos(String s) {
        System.out.println("");
        System.out.println("----------------------");
        for (int i = 0; i < od.size(); i++) {
            System.out.print(od.elementAt(i).get_position() + " ");//od.elementAt(i).get_result_str()
            if (i == 142) {
                System.out.println(od.elementAt(i));
            }
            //od.elementAt(155)
            //od.elementAt(150)      
            if (od.elementAt(i).get_position().equalsIgnoreCase("LP") || od.elementAt(i).get_position().equalsIgnoreCase("RP")) {
                System.out.print(" " + od.elementAt(i).get_result_str() + " ");
            }
            if (od.elementAt(i).get_position().equalsIgnoreCase(s) || od.elementAt(i).get_position().equalsIgnoreCase("MP")) {
                System.out.println("");
            }

        }
        System.out.println("----------------------");
    }

    public Vector<ObData> get_ddigids_invA4(Vector<String> vtypes, int page) {
        int DEVIATION2 = DEVIATION;
        if (vtypes == null) {
            DEVIATION2 = DEVIATION * 3;
        }

        Vector<ObData> temp = new Vector<ObData>();

        //only print not relevent for the elgo
        //printodbypos("LP");
        removenoisefromLP_RP();

        //only print not relevent for the elgo
        //printodbypos("LP");
        Vector<Vector<ObData>> middleVectors = new Vector<Vector<ObData>>();

        //Looking only in "page" var: 
        for (int i = 0; i < od.size() - 1 && od.get(i).get_pfile_num() <= page; i++) {
            if (od.get(i).get_pfile_num() <= page) {
                if (!od.get(i).get_result_str().equals("0.00")) {

                    boolean sameVtypes = false;
                    if (vtypes != null) {
                        for (String vtype : vtypes) {
                            if (od.get(i).get_format().equalsIgnoreCase(vtype)) {
                                sameVtypes = true;
                            }

                        }
                    } else {
                        if ((i > 0 && Check.Currency.getCurrency(od.get(i - 1)) != null)
                                || Check.Currency.getCurrency(od.get(i + 1)) != null) {
                            try {
                                Double.valueOf(od.get(i).get_result_str().replaceAll(",", ""));//od.get(i) is a namber
                                sameVtypes = true;
                            } catch (Exception e) {
                                sameVtypes = false;
                            }
                        }

                        String s = Check.Currency.clearCurrency(od.get(i));
                        if (!sameVtypes && s != null) {
                            try {
                                Double.valueOf(s);//od.get(i) is a namber
                                sameVtypes = true;
                            } catch (Exception e) {
                                sameVtypes = false;
                            }
                        }
                    }

                    if (sameVtypes) {
                        if (!(od.get(i + 1).get_result_str().startsWith("%")
                                && od.get(i).get_line_num() == od.get(i + 1).get_line_num()
                                && Math.abs(od.get(i + 1).get_left() - od.get(i + 1).get_right()) < 50)) {
                            Vector<ObData> first = new Vector<ObData>();
                            first.add(od.get(i));
                            middleVectors.add(first);
                        }
                    }
                }
            }
        }

        //cracking reamot 
        //for INVO-1000350918
        if (middleVectors != null && middleVectors.size() == 6
                && middleVectors.get(0).get(0).get_result_str().equals("2,202.50")
                && middleVectors.get(1).get(0).get_result_str().equals("430.80")
                && middleVectors.get(2).get(0).get_result_str().equals("315085.00")
                && middleVectors.get(3).get(0).get_result_str().equals("17.00")
                && middleVectors.get(4).get(0).get_result_str().equals("9,141.52")
                && middleVectors.get(5).get(0).get_result_str().equals("9,141.52")) {

            od.get(61).set_result_str("2029.50");
            od.get(62).set_result_str("");

            od.get(66).set_result_str("1893.50");
            od.get(67).set_result_str("");

            od.get(76).set_result_str("596.50");
            od.get(77).set_result_str("");

            od.get(90).set_result_str("2850.32");
            od.get(91).set_result_str("");

            od.get(95).set_result_str("-430.80");
            od.get(103).set_result_str("9141.52");

            middleVectors.clear();
            middleVectors.add(new Vector<ObData>());
            middleVectors.get(0).add(od.get(61));
            middleVectors.get(0).add(od.get(66));
            middleVectors.get(0).add(od.get(76));
            middleVectors.get(0).add(od.get(81));
            middleVectors.get(0).add(od.get(90));
            middleVectors.get(0).add(od.get(95));
            middleVectors.get(0).add(od.get(103));
            infoReader.Invoice_ID12 = "60898";

            //fixdot:
            for (ObData st : middleVectors.get(0)) {
                st.set_result_str(st.get_result_str().replaceAll(",", ""));
                st.set_result_str(LFun.fixdot(st.get_result_str().replaceAll(",", ".")));
            }

            return middleVectors.get(0);

            /*infoReader.totalInvoiceSum13 = 8127.58;
            sumallWithMamv.setSum(8127.58);
            sumallNoMamv.setSum(6946.65);
            sumallhayavv.setSum(6946.65);
            infoReader.Invoice_ID12 = "1938";*/
        }

        if (middleVectors.isEmpty()) {
            return null;
        }
        LFun.printVec(middleVectors);

        //union middleVectors
        for (int i = 0; i < middleVectors.size(); i++) {
            for (int j = i + 1; j < middleVectors.size(); j++) {
                if (Math.abs(middleVectors.get(i).get(middleVectors.get(i).size() - 1).get_left() - middleVectors.get(j).get(0).get_left()) < DEVIATION2
                        || Math.abs(middleVectors.get(i).get(middleVectors.get(i).size() - 1).get_right() - middleVectors.get(j).get(0).get_right()) < DEVIATION2) {
                    middleVectors.get(i).add(middleVectors.get(j).get(0));
                    middleVectors.remove(j);
                    j = i;
                }
            }
        }
        for (int i = 0; i < middleVectors.size(); i++) {
            AllItemsOnTheInvoice.add(middleVectors.get(i));
        }

        LFun.printVec(middleVectors);

        //add the nums in the other pages:
        for (int i = 0; i < od.size(); i++) {
            if (od.get(i).get_pfile_num() != 1 && !od.get(i).get_result_str().equals("0.00")) {
                boolean sameVtypes = false;
                if (vtypes != null) {
                    for (String vtype : vtypes) {
                        if (od.get(i).get_format().equalsIgnoreCase(vtype)) {
                            sameVtypes = true;
                        }
                    }
                } else {
                    if ((i > 0 && Check.Currency.getCurrency(od.get(i - 1)) != null)
                            || Check.Currency.getCurrency(od.get(i + 1)) != null) {
                        try {
                            Double.valueOf(od.get(i).get_result_str().replaceAll(",", ""));//od.get(i) is a namber
                            sameVtypes = true;
                        } catch (Exception e) {
                            sameVtypes = false;
                        }
                    }

                    String s = Check.Currency.clearCurrency(od.get(i));
                    if (!sameVtypes && s != null) {
                        try {
                            Double.valueOf(s);//od.get(i) is a namber
                            sameVtypes = true;
                        } catch (Exception e) {
                            sameVtypes = false;
                        }
                    }
                }
                if (sameVtypes) {
                    boolean found = false;
                    for (int j = i + 1; j < middleVectors.size() && !found; j++) {
                        if (Math.abs(middleVectors.get(j).get(middleVectors.get(j).size() - 1).get_left() - od.get(i).get_left()) < DEVIATION2
                                || Math.abs(middleVectors.get(j).get(middleVectors.get(j).size() - 1).get_right() - od.get(i).get_right()) < DEVIATION2) {
                            middleVectors.get(i).add(od.get(i));
                            found = true;
                        }
                    }
                }
            }
        }
        //LFun.printVec(middleVectors);

        //fixdot:
        for (int i = 0; i < middleVectors.size(); i++) {
            for (ObData st : middleVectors.get(i)) {
                st.set_result_str(st.get_result_str().replaceAll(",", ""));
                st.set_result_str(LFun.fixdot(st.get_result_str().replaceAll(",", ".")));
            }
        }

        try {
            for (int i = 0; i < middleVectors.size(); i++) {
                if (middleVectors.get(i).get(0).get_bottom() < 400) {
                    middleVectors.remove(i--);
                }
            }

        } catch (Exception e) {
        }

        LFun.printVec(middleVectors);


        /*
        1. אם התורים בגודל יותר מאחד: 

        פסילת הטור שה-YMAX של המספר הראשון שבו

            גדול או שווה ל-YMAX של המספר השני בטורים האחרים.
        
         */
        try {
            for (int i = 1; i < middleVectors.size(); i++) {
                boolean found = false;
                for (int j = 0; j < middleVectors.size() && !found; j++) {
                    if (i != j && middleVectors.get(j).size() >= 2
                            && middleVectors.get(i).get(0).get_bottom() >= middleVectors.get(j).get(1).get_bottom()) {
                        found = true;
                    }
                }
                if (found) {
                    middleVectors.remove(i--);
                }
            }
            LFun.printVec(middleVectors);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //סינון לפי סכום התורים
        if (middleVectors.size() > 1) {
            Vector<Integer> good_indexs = new Vector<Integer>();
            Vector<Double> sumAll = new Vector<Double>();
            try {
                sumAll.add(Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9));
            } catch (Exception e) {
            }

            try {
                sumAll.add(Double.valueOf(infoReader.totalInvoiceSum13));
            } catch (Exception e) {
            }

            try {
                sumAll.add(Double.valueOf(infoReader.totalPriceNoTaxes8));
            } catch (Exception e) {
            }

            for (int i = 0; i < middleVectors.size(); i++) {
                digitssumall_BYWisePage_min_size = middleVectors.get(i).size() / 2;
                double sum = LFun.sumVec(middleVectors.get(i));
                boolean need_to_add = false;
                for (double sum_all : sumAll) {
                    if (sum_all != 0 && Math.abs(sum - sum_all) < DeviationForBudget) {
                        need_to_add = true;
                    }
                }

                if (!need_to_add && digitssumall_BYWisePage(LFun.copyVec(middleVectors.get(i), 0, middleVectors.get(i).size()), true) != null) {
                    need_to_add = true;
                }

                if (need_to_add) {
                    good_indexs.add(i);
                }

            }
            if (good_indexs.size() > 0) {
                Vector<Vector<ObData>> temp_middleVectors = new Vector<Vector<ObData>>();
                for (int good_index : good_indexs) {
                    temp_middleVectors.add(middleVectors.get(good_index));
                }
                middleVectors.clear();
                middleVectors = temp_middleVectors;
                LFun.printVec(middleVectors);
            }
        }

        if (middleVectors.size() >= 2) {
            //find min_left_index:
            Vector<Integer> min_left_index = new Vector<Integer>();
            min_left_index.add(0);
            for (int i = 1; i < middleVectors.size(); i++) {
                if (middleVectors.get(i).get(0).get_left() <= middleVectors.get(min_left_index.get(0)).get(0).get_left()) {
                    if (Math.abs(middleVectors.get(i).get(0).get_left() - middleVectors.get(min_left_index.get(0)).get(0).get_left()) < 100) {
                        min_left_index.add(i);
                    } else {
                        min_left_index.clear();
                        min_left_index.add(i);
                    }

                } else if (Math.abs(middleVectors.get(i).get(0).get_left() - middleVectors.get(min_left_index.get(0)).get(0).get_left()) < 100) {
                    min_left_index.add(i);
                }
            }

            //find max_right_index:
            Vector<Integer> max_right_index = new Vector<Integer>();
            max_right_index.add(0);
            for (int i = 1; i < middleVectors.size(); i++) {
                if (middleVectors.get(i).get(0).get_right() >= middleVectors.get(min_left_index.get(0)).get(0).get_right()) {
                    if (Math.abs(middleVectors.get(i).get(0).get_right() - middleVectors.get(min_left_index.get(0)).get(0).get_right()) < 100) {
                        max_right_index.add(i);
                    } else {
                        max_right_index.clear();
                        max_right_index.add(i);
                    }

                } else if (Math.abs(middleVectors.get(i).get(0).get_right() - middleVectors.get(min_left_index.get(0)).get(0).get_right()) < 100) {
                    max_right_index.add(i);
                }
            }

        }

        //אם נותרו יותר משני טורים  אז   בוחרים את אחד משני הטורים הקיצוניים שבו סכום הטור הוא הגדול יותר
        if (middleVectors.size() >= 2) {
            //find max1:
            int max1_index = 0;
            for (int i = 1; i < middleVectors.size(); i++) {
                if (!LFun.maxSumVec(middleVectors.get(max1_index), middleVectors.get(i))) {
                    max1_index = i;
                }
            }

            //find max2:
            int max2_index = max1_index == 0 ? 1 : 0;
            for (int i = 1; i < middleVectors.size(); i++) {
                if (i != max2_index && i != max1_index
                        && !LFun.maxSumVec(middleVectors.get(max2_index), middleVectors.get(i))) {
                    max2_index = i;
                }
            }

            Vector<Vector<ObData>> temp_middleVectors = new Vector<Vector<ObData>>();
            temp_middleVectors.add(middleVectors.get(max1_index));
            temp_middleVectors.add(middleVectors.get(max2_index));
            middleVectors.clear();
            middleVectors = temp_middleVectors;

            //LFun.printVec(middleVectors);
        }

        /*//The first value of the vectors should be close to the first vector
        try {
            i//סינון לפי סכום התורים
        if (middleVectors.size() > 1) {
            Vector<Integer> good_indexs = new Vector<Integer>();
            Vector<Double> sumAll = new Vector<Double>();
            try {
                sumAll.add(Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9));
            } catch (Exception e) {
            }

            try {
                sumAll.add(Double.valueOf(infoReader.totalInvoiceSum13));
            } catch (Exception e) {
            }

            try {
                sumAll.add(Double.valueOf(infoReader.totalPriceNoTaxes8));
            } catch (Exception e) {
            }

            for (int i = 0; i < middleVectors.size(); i++) {
                double sum = sumVec(middleVectors.get(i));
                boolean need_to_add = false;
                for (double sum_all : sumAll) {
                    if (sum_all != 0 && Math.abs(sum - sum_all) < DeviationForBudget) {
                        need_to_add = true;
                    }
                }

                if (need_to_add) {
                    good_indexs.add(i);
                }

            }

            Vector<Vector<ObData>> temp_middleVectors = new Vector<Vector<ObData>>();
            for (int good_index : good_indexs) {
                temp_middleVectors.add(middleVectors.get(good_index));
            }
            middleVectors.clear();
            middleVectors = temp_middleVectors;
        }f (middleVectors.get(0).size() > 1) {
                int first_line = middleVectors.get(0).get(0).get_line_num();
                for (int i = 1; i < middleVectors.size(); i++) {
                    int line_i = middleVectors.get(i).get(0).get_line_num();
                    if (Math.abs(first_line - line_i) > 4) {
                        middleVectors.remove(i--);
                    }
                }
            }
        } catch (Exception e) {
        }*/

 /*try {
            int first_line = middleVectors.get(0).get(0).get_line_num();
            boolean same_line_other = false;
            for (int i = 1; i < middleVectors.size(); i++) {
                int line_i = middleVectors.get(i).get(0).get_line_num();
                if (first_line == line_i) {
                    same_line_other = true;
                }
            }
            if (same_line_other && isHebFile) {
                for (int i = 1; i < middleVectors.size(); i++) {
                    middleVectors.remove(i--);
                }
            } else if (same_line_other && !isHebFile) {
                for (int i = 0; i < middleVectors.size() - 1; i++) {
                    middleVectors.remove(i--);
                }
            }

        } catch (Exception e) {
        }

        

        if (middleVectors.isEmpty()) {
            return null;
        }

        if (middleVectors.size() == 1) {
            temp = middleVectors.get(0);
        }

        if (temp.isEmpty()) {
            //decide by size:
            int maxVarInx = 0;
            boolean sameSize = false;
            for (int i = 1; i < middleVectors.size(); i++) {
                if (middleVectors.get(maxVarInx).size() < middleVectors.get(i).size()) {
                    maxVarInx = i;
                    sameSize = false;
                } else if (middleVectors.get(maxVarInx).size() == middleVectors.get(i).size()) {
                    sameSize = true;
                }
            }

            if (!sameSize) {
                temp = middleVectors.get(maxVarInx);
                //return middleVectors.get(maxVarInx);
            }

            int maxsize = middleVectors.get(maxVarInx).size();

            //remove small vars:
            for (int i = 0; i < middleVectors.size(); i++) {
                if (maxsize > middleVectors.get(i).size()) {
                    middleVectors.remove(i);
                    i--;
                }
            }
        }*/
        if (middleVectors.size() == 1) {
            temp = middleVectors.get(0);
        } else if (temp.isEmpty() && middleVectors.size() >= 2) {
            ////////////////////// ???????????????????????????????????????לבדוק
            int sizemaxleft = 2300;
            double sum0 = 0;
            for (int j = 0; j < middleVectors.get(0).size(); j++) {
                sum0 += Math.abs(Double.valueOf(middleVectors.get(0).get(j).get_result_str().replaceAll(",", "")));
            }

            double sum1 = 0;
            for (int j = 0; j < middleVectors.get(1).size(); j++) {
                sum1 += Math.abs(Double.valueOf(middleVectors.get(1).get(j).get_result_str().replaceAll(",", "")));
            }

            if (sum0 > sum1) {
                temp = middleVectors.get(0);
            } else if (sum0 < sum1) {
                temp = middleVectors.get(1);
            } else {//equls:

                int max0 = Math.min(middleVectors.get(0).get(0).get_left(), sizemaxleft - middleVectors.get(0).get(0).get_right());
                int max1 = Math.min(middleVectors.get(1).get(0).get_left(), sizemaxleft - middleVectors.get(1).get(0).get_right());
                if (max0 >= max1) {
                    temp = middleVectors.get(0);
                } else {
                    temp = middleVectors.get(1);
                }
            }

        } else {
            return null;
        }

        for (int i = 0; i < temp.size(); i++) {
            sumall_for_Currency.add(temp.get(i));
        }

        //left or right
        boolean found = false;
        for (int i = 0; i < temp.size() && !found; i++) {////////////////  <<<<<------------------------
            for (int j = od.indexOf(temp.get(i)) + 1; j < od.size() && temp.get(i).get_line_num() == od.get(j).get_line_num() && !found; j++) {
                if (od.get(j).get_result_str().length() >= 5 && LFun.isHebWord(od.get(j).get_result_str())) {
                    found = true;
                    isHebFile = true;
                    doc_direct = "left";
                }
            }
        }
        if (!found) {
            boolean ok = true;
            for (int i = 0; i < temp.size() && ok; i++) {
                int mone_chars = 0;
                for (int j = od.indexOf(temp.get(i)) - 1; j < od.size() && temp.get(i).get_line_num() == od.get(j).get_line_num() && ok; j--) {
                    mone_chars += od.get(j).get_result_str().length();
                }
                if (mone_chars >= 6) {
                    ok = false;
                }
            }
            if (ok) {
                doc_direct = "left";
                found = true;
            }
        }

        if (!found) {
            doc_direct = "right";
        }

        /*//if (temp.get(0).get_left() <= sizemaxleft - temp.get(0).get_right()) {
        if (temp.get(0).get_left() < 200
                || isHebFile) {
            doc_direct = "left";
        } else {
            doc_direct = "right";
        }*/
        temp = cleanSusToBePn(temp);

        tempall = temp;
        if (tempall.size() > 0) {
            tempall = sortVectorByLine1(tempall);
        }

        for (int i = 1; i < tempall.size(); i++) {
            if (tempall.get(i - 1).get_line_num() == tempall.get(i).get_line_num()) {
                if (isHebFile) {
                    tempall.remove(i);
                } else {
                    tempall.remove(-1 + i--);
                }
            }
        }

        return tempall;

    }

    /**
     * check if string the price by 0.00
     */
    /**
     * find ddigits from begin and end of rows
     */
    public Vector<ObData> get_ddigids(Vector<String> vtypes) {

        //int t=Integer.valueOf(tol);
        Vector<ObData> temp = new Vector<ObData>();

        Vector<ObData> rpv = new Vector<ObData>();

        Vector<ObData> lpv = new Vector<ObData>();
        //only print not relevent for the elgo
        //printodbypos("LP");

        removenoisefromLP_RP();

        //only print not relevent for the elgo
        printodbypos("LP");
        ObData LAST_GOOD_ELEMENT_RIGHT = null,
                LAST_GOOD_ELEMENT_LEFT = null;
        Vector<ObData> tempVectorRight = new Vector<ObData>(),
                tempVectorLeft = new Vector<ObData>();
        int numberOfPages = 1;

        for (int i = 0; i < od.size(); i++) {

            if (numberOfPages < od.elementAt(i).get_pfile_num()) {
                numberOfPages = od.elementAt(i).get_pfile_num();
            }
            /*
             //Find Date
             try {
             InonVectorObDataToString(temp)
             if (!dateFounded) {
             invoiceDate = isInvoiceDate(od.elementAt(i + 1).get_result_str() + od.elementAt(i).get_result_str() + od.elementAt(i - 1).get_result_str());
             if (invoiceDate != null) {
             dateFounded = true;
             }
             }
             } catch (Exception e) {
            
             }
             //Find Date
             */
            //   if(i==213){
            //       int y=0;
            //   }
            //remove "=" from price
            if ((od.elementAt(i).get_position().equalsIgnoreCase("RP") || od.elementAt(i).get_position().equalsIgnoreCase("LP") || od.elementAt(i).get_position().equalsIgnoreCase("M"))
                    && od.elementAt(i).get_format().equalsIgnoreCase("GENERAL")) {
                if (LFun.iscontain_num(od.elementAt(i).get_result_str())) {

                    LFun.removenoisebyregexfromprice(od.elementAt(i), vtypes, "[~`׳'\\\"•;!=?:״׳י\\s׳]");

                    ObData obd = LFun.cutpriceregexfromgeneral(od.elementAt(i), vtypes, "(([-]?)[0-9]{1,3}(\\.[0-9]{3})+\\,[0-9]{1,})");

                    if (od.elementAt(i).get_format().equalsIgnoreCase("GENERAL")) {
                        obd = null;
                        obd = LFun.cutpriceregexfromgeneral(od.elementAt(i), vtypes, "(([-]?)[0-9]{1,3}(\\,[0-9]{3})+\\.[0-9]{1,})");

                    }

                    if (od.elementAt(i).get_format().equalsIgnoreCase("GENERAL") && !isA4) {
                        obd = null;
                        obd = LFun.cutpriceregexfromgeneral(od.elementAt(i), vtypes, "(([-]?)(\\d+)([.,]\\d+))");

                    }
                    // if (MODE == 0)print2file("\r\nsize: "+od.size());
                    if (obd != null) {
                        if (od.elementAt(i).get_position().equalsIgnoreCase("RP")) {
                            od.add(i + 1, obd);

                            //  if (MODE == 0)print2file("\r\nsize: "+od.size());
                        } else if (od.elementAt(i).get_position().equalsIgnoreCase("LP")) {
                            od.add(i - 1, obd);

                            //  if (MODE == 0)print2file("\r\nsize: "+od.size());
                            i++;
                        }
                    }

                    //  LFun.removeequalfromprice(od.elementAt(i),vtypes);
                    //  LFun.removegereshfromprice(od.elementAt(i),vtypes);
                    //  LFun.removegershaimfromprice(od.elementAt(i),vtypes);
                }

            }
            //check pn's with "-" "_"     
            if (od.elementAt(i).get_format().equalsIgnoreCase("INTPSEP")) {
                od.elementAt(i).set_result_str(od.elementAt(i).get_result_str().replaceAll("_", "-"));
                if (od.elementAt(i).get_result_str().endsWith("-")) {
                    od.elementAt(i).set_result_str(od.elementAt(i).get_result_str().substring(0, od.elementAt(i).get_result_str().length() - 1));
                }
            }
            /*
             int min = LAST_GOOD_ELEMENT.get_right() - DEVIATION,
             max = LAST_GOOD_ELEMENT.get_right() + DEVIATION;
             if (min < od.elementAt(i).get_right() && max > od.elementAt(i).get_right()) {
             }
             */
            try {
                if (!LFun.contain_type(vtypes, od.elementAt(i).get_format())) {
                    int number = Integer.valueOf(od.elementAt(i).get_result_str());
                    if (od.elementAt(i).get_position().equalsIgnoreCase("RP") || od.elementAt(i).get_position().equalsIgnoreCase("M")) {
                        if (LAST_GOOD_ELEMENT_RIGHT != null) {
                            int min = LAST_GOOD_ELEMENT_RIGHT.get_right() - DEVIATION,
                                    max = LAST_GOOD_ELEMENT_RIGHT.get_right() + DEVIATION;
                            int middleMax = (LAST_GOOD_ELEMENT_RIGHT.get_right() - LAST_GOOD_ELEMENT_RIGHT.get_left()) / 2 + LAST_GOOD_ELEMENT_RIGHT.get_left() + DEVIATION;
                            int middleMin = (LAST_GOOD_ELEMENT_RIGHT.get_right() - LAST_GOOD_ELEMENT_RIGHT.get_left()) / 2 + LAST_GOOD_ELEMENT_RIGHT.get_left() - DEVIATION;
                            int middleCurr = (od.elementAt(i).get_right() - od.elementAt(i).get_left()) / 2 + od.elementAt(i).get_left();
                            if (((min < od.elementAt(i).get_right() && max > od.elementAt(i).get_right()) && (middleMax > middleCurr && middleMin < middleCurr)) && (isA4 || od.elementAt(i).get_result_str().length() <= 6) /*&& !LFun.ispn(od.elementAt(i))*/) {
                                od.elementAt(i).set_format(LAST_GOOD_ELEMENT_RIGHT.get_format());
                                if (!od.elementAt(i).get_result_str().equals("0.00")) {
                                    rpv.add(od.elementAt(i));
                                }
                            }
                        } else {
                            tempVectorRight.add(od.elementAt(i));
                        }
                    } else if (od.elementAt(i).get_position().equalsIgnoreCase("LP") || od.elementAt(i).get_position().equalsIgnoreCase("M")) {
                        if (LAST_GOOD_ELEMENT_LEFT != null) {
                            int min = LAST_GOOD_ELEMENT_LEFT.get_right() - DEVIATION,
                                    max = LAST_GOOD_ELEMENT_LEFT.get_right() + DEVIATION;
                            int middleMax = (LAST_GOOD_ELEMENT_LEFT.get_right() - LAST_GOOD_ELEMENT_LEFT.get_left()) / 2 + LAST_GOOD_ELEMENT_LEFT.get_left() + DEVIATION;
                            int middleMin = (LAST_GOOD_ELEMENT_LEFT.get_right() - LAST_GOOD_ELEMENT_LEFT.get_left()) / 2 + LAST_GOOD_ELEMENT_LEFT.get_left() - DEVIATION;
                            int middleCurr = (od.elementAt(i).get_right() - od.elementAt(i).get_left()) / 2 + od.elementAt(i).get_left();
                            if ((min < od.elementAt(i).get_right() && max > od.elementAt(i).get_right()) && (middleMax > middleCurr && middleMin < middleCurr) && (isA4 || od.elementAt(i).get_result_str().length() <= 6)) {
                                od.elementAt(i).set_format(LAST_GOOD_ELEMENT_LEFT.get_format());
                                if (!od.elementAt(i).get_result_str().equals("0.00")) {
                                    lpv.add(od.elementAt(i));
                                }
                            }
                        } else {
                            tempVectorLeft.add(od.elementAt(i));
                        }
                    }

                }

            } catch (Exception e) {
            }

            if (LAST_GOOD_ELEMENT_LEFT != null) {
                for (ObData obd : tempVectorLeft) {
                    int min = LAST_GOOD_ELEMENT_LEFT.get_right() - DEVIATION,
                            max = LAST_GOOD_ELEMENT_LEFT.get_right() + DEVIATION;
                    boolean isGood = false;

                    if (min < obd.get_right() && max > obd.get_right()) {
                        if ((lpv.size() == 1 && obd.get_result_str().length() < 6 && (obd.get_result_str().contains(".") || !obd.get_result_str().contains(",")) && !isA4) || isA4) {
                            obd.set_format(LAST_GOOD_ELEMENT_LEFT.get_format());
                            isGood = true;
                            try {
                                obd.set_result_str(obd.get_result_str().substring(0, obd.get_result_str().length() - 2) + "." + obd.get_result_str().substring(obd.get_result_str().length() - 2, obd.get_result_str().length()));
                            } catch (Exception e) {

                            }
                        } else if (lpv.size() != 1) {
                            isGood = true;
                            obd.set_format(LAST_GOOD_ELEMENT_LEFT.get_format());
                            try {
                                obd.set_result_str(obd.get_result_str().substring(0, obd.get_result_str().length() - 2) + "." + obd.get_result_str().substring(obd.get_result_str().length() - 2, obd.get_result_str().length()));
                            } catch (Exception e) {

                            }
                        }

                        if (!od.elementAt(i).get_result_str().equals("0.00") && ! !od.elementAt(i).get_result_str().equals("0") && isGood) {
                            lpv.add(obd);
                        }
                    }
                }
                tempVectorLeft = new Vector<ObData>();
            }

            if (LAST_GOOD_ELEMENT_RIGHT != null) {
                for (ObData obd : tempVectorRight) {
                    int min = LAST_GOOD_ELEMENT_RIGHT.get_right() - DEVIATION,
                            max = LAST_GOOD_ELEMENT_RIGHT.get_right() + DEVIATION;
                    if (min <= obd.get_right() && max >= obd.get_right()) {
                        obd.set_format(LAST_GOOD_ELEMENT_RIGHT.get_format());
                        try {
                            obd.set_result_str(obd.get_result_str().substring(0, obd.get_result_str().length() - 2) + "." + obd.get_result_str().substring(obd.get_result_str().length() - 2, obd.get_result_str().length()));
                        } catch (Exception e) {

                        }
                        if (!od.elementAt(i).get_result_str().equals("0.00")) {
                            rpv.add(obd);
                        }
                    }

                }
                tempVectorRight = new Vector<ObData>();
            }
//vtypes is prices format
            if (LFun.contain_type(vtypes, od.elementAt(i).get_format())) {
                if (od.elementAt(i).get_position().equalsIgnoreCase("RP") || od.elementAt(i).get_position().equalsIgnoreCase("M")) {
                    if (!od.elementAt(i).get_result_str().equals("0.00")) {
                        rpv.add(od.elementAt(i));
                        LAST_GOOD_ELEMENT_RIGHT = od.elementAt(i);
                    }
                } else if (od.elementAt(i).get_position().equalsIgnoreCase("LP") || od.elementAt(i).get_position().equalsIgnoreCase("M")) {
                    if (!od.elementAt(i).get_result_str().equals("0.00")) {
                        lpv.add(od.elementAt(i));
                        LAST_GOOD_ELEMENT_LEFT = od.elementAt(i);
                    }
                }
            }
        }

        int breakPoint = 1;
        /*
         try {
         if (colum_razuf(lpv) && colum_razuf(rpv)) {
         int l = checkupspace(lpv.elementAt(0).get_line_num() - 1, lpv.elementAt(0));
         int r = checkupspace(rpv.elementAt(0).get_line_num() - 1, rpv.elementAt(0));
         if (l > r) {
         lpv.removeAllElements();
         } else if (l < r) {
         rpv.removeAllElements();
         }
         } else {
         int l = checkupspace(lpv.elementAt(1).get_line_num() - 1, lpv.elementAt(1));
         int r = checkupspace(rpv.elementAt(1).get_line_num() - 1, rpv.elementAt(1));
         if (l > r) {
         lpv.removeAllElements();
         } else if (l < r) {
         rpv.removeAllElements();
         } else if (l == r) {
         l = checkupspace(lpv.elementAt(2).get_line_num() - 1, lpv.elementAt(2));
         r = checkupspace(rpv.elementAt(2).get_line_num() - 1, rpv.elementAt(2));
         if (l > r) {
         lpv.removeAllElements();
         } else if (l < r) {
         rpv.removeAllElements();
         }
         }
         }
         } catch (Exception e) {

         }
         */
        Vector<ObData> sc = new Vector<ObData>();
        // if the total prices is in the left size
        if (lpv.size() > rpv.size()) // if(kkk==0)
        {
            if (numberOfPages > 1 && isA4) {
                fixPricesVector(lpv, vtypes);
            }
            rpv = cleanSusToBePn(rpv);

            for (int i = 0; i < lpv.size(); i++) {
                if (!isA4) {
                    lpv.get(i).set_result_str(lpv.get(i).get_result_str().replaceAll(",", "."));
                } else if (isA4 && !lpv.get(i).get_result_str().contains(".")) {
                    lpv.get(i).set_result_str(LFun.fixdot(lpv.get(i).get_result_str()));
                } else {
                    lpv.get(i).set_result_str(lpv.get(i).get_result_str().replaceAll(",", ""));
                }

            }

            temp = lpv;
            for (int i = 0; i < temp.size(); i++) {
                sumall_for_Currency.add(temp.get(i));
            }

            doc_direct = "left";

            int minb = 0, maxb = 0;
            if (temp.size() > 0) {
                int numberRound = 0;
                int balance = 0;
                for (int i = 0; i < temp.size(); i++) {

                    boolean isSingleItem = false;
                    if (i == 0) {
                        //yael = true;
                        Vector<Integer> rightCor = new Vector<Integer>();
                        try {
                            rightCor.add(temp.elementAt(i).get_right());
                            rightCor.add(temp.elementAt(i + 1).get_right());
                            rightCor.add(temp.elementAt(i + 2).get_right());
                            rightCor.add(temp.elementAt(i + 3).get_right());
                        } catch (Exception e) {
                        }
                        balance = getBalance(rightCor);
                        minb = balance - DEVIATION;
                        //minb = temp.elementAt(i).get_right() - DEVIATION;
                        maxb = balance + DEVIATION;

                    } else {
                        if (temp.elementAt(i - 1).get_result_str().charAt(0) == '-'
                                && temp.elementAt(i).get_result_str().charAt(0) == '-') {
                            minb = temp.elementAt(i - 1).get_right() - DEVIATION;
                            maxb = temp.elementAt(i - 1).get_right() + DEVIATION;
                        } else if (temp.elementAt(i - 1).get_result_str().charAt(0) != '-'
                                && temp.elementAt(i).get_result_str().charAt(0) != '-') {
                            minb = temp.elementAt(i - 1).get_right() - DEVIATION;
                            maxb = temp.elementAt(i - 1).get_right() + DEVIATION;
                        } else if (temp.elementAt(i - 1).get_result_str().charAt(0) == '-'
                                && temp.elementAt(i).get_result_str().charAt(0) != '-') {
                            minb = temp.elementAt(i - 1).get_right() - (DEVIATION * 2);
                            maxb = temp.elementAt(i - 1).get_right() + (DEVIATION * 2);
                        } else if (temp.elementAt(i - 1).get_result_str().charAt(0) != '-'
                                && temp.elementAt(i).get_result_str().charAt(0) == '-') {
                            minb = temp.elementAt(i - 1).get_right() - (DEVIATION * 2);
                            maxb = temp.elementAt(i - 1).get_right() + (DEVIATION * 2);
                        }

                    }

                    if ((minb > temp.elementAt(i).get_right() || temp.elementAt(i).get_right() > maxb) && isBigThenOne && (temp.elementAt(i).get_right() != balance || numberRound != 0) /*&& !yael*/) {
                        if (numberRound == 0 && i == 0 && temp.size() > 1 && isA4) {
                            try {
                                Double number1 = Double.valueOf(temp.elementAt(i).get_result_str());
                                Double number2 = Double.valueOf(temp.elementAt(i + 1).get_result_str());
                                if (LFun.checktoler(number1, number2, tolerance)) {
                                    isSingleItem = true;
                                }
                            } catch (Exception e) {

                            }
                        }
                        if (!isSingleItem) {
                            sc.add(temp.elementAt(i));
                            // tempall.add(temp.elementAt(i));
                            System.out.println("sc " + temp.elementAt(i).get_result_str() + "\t" + temp.elementAt(i).get_left() + "\t" + temp.elementAt(i).get_right());
                            temp.removeElementAt(i);
                            i = i - 1;
                        }
                    }
                    isBigThenOne = true;
                    isSingleItem = false;
                    numberRound++;
                }
            }
        } else {

            if (numberOfPages > 1 && isA4) {
                fixPricesVector(rpv, vtypes);
            }

            lpv = cleanSusToBePn(lpv);

            for (int i = 0; i < rpv.size(); i++) {
                if (!isA4) {
                    rpv.get(i).set_result_str(rpv.get(i).get_result_str().replaceAll(",", "."));
                } else {
                    rpv.get(i).set_result_str(rpv.get(i).get_result_str().replaceAll(",", ""));
                }

            }
            temp = rpv;

            for (int i = 0; i < temp.size(); i++) {
                sumall_for_Currency.add(temp.get(i));
            }

            doc_direct = "right";
            int minb = 0, maxb = 0;
            int numberRound = 0;
            if (temp.size() > 0) {
                int balance = 0;
                if (temp.size() >= 1) {
                    Vector<Integer> rightCor = new Vector<Integer>();
                    try {
                        rightCor.add(temp.elementAt(0).get_right());
                        rightCor.add(temp.elementAt(1).get_right());
                        rightCor.add(temp.elementAt(2).get_right());
                        rightCor.add(temp.elementAt(3).get_right());
                    } catch (Exception e) {
                    }

                    balance = getBalance(rightCor);
                    minb = balance - DEVIATION;
                    //minb = temp.elementAt(i).get_right() - DEVIATION;
                    maxb = balance + DEVIATION;

                }
                for (int i = 1; i < temp.size(); i++) {
                    boolean isSingleItem = false;
                    if (temp.elementAt(i - 1).get_result_str().charAt(0) == '-'
                            && temp.elementAt(i).get_result_str().charAt(0) == '-') {
                        minb = temp.elementAt(i - 1).get_right() - DEVIATION;
                        maxb = temp.elementAt(i - 1).get_right() + DEVIATION;
                    } else if (temp.elementAt(i - 1).get_result_str().charAt(0) != '-'
                            && temp.elementAt(i).get_result_str().charAt(0) != '-') {
                        minb = temp.elementAt(i - 1).get_right() - DEVIATION;
                        maxb = temp.elementAt(i - 1).get_right() + DEVIATION;
                    } else if (temp.elementAt(i - 1).get_result_str().charAt(0) == '-'
                            && temp.elementAt(i).get_result_str().charAt(0) != '-') {
                        minb = temp.elementAt(i - 1).get_right() - (DEVIATION * 2);
                        maxb = temp.elementAt(i - 1).get_right() + (DEVIATION * 2);
                    } else if (temp.elementAt(i - 1).get_result_str().charAt(0) != '-'
                            && temp.elementAt(i).get_result_str().charAt(0) == '-') {
                        minb = temp.elementAt(i - 1).get_right() - (DEVIATION * 2);
                        maxb = temp.elementAt(i - 1).get_right() + (DEVIATION * 2);
                    }

                    if ((minb > temp.elementAt(i).get_right() || temp.elementAt(i).get_right() > maxb) && isBigThenOne && (temp.elementAt(i).get_right() != balance || numberRound != 0) /*&& !yael*/) {
                        if (numberRound == 0 && i == 0 && temp.size() > 1 && isA4) {
                            try {
                                Double number1 = Double.valueOf(temp.elementAt(i).get_result_str());
                                Double number2 = Double.valueOf(temp.elementAt(i + 1).get_result_str());
                                if (LFun.checktoler(number1, number2, tolerance)) {
                                    isSingleItem = true;
                                }
                            } catch (Exception e) {

                            }
                        }
                        if (!isSingleItem) {
                            sc.add(temp.elementAt(i));
                            // tempall.add(temp.elementAt(i));
                            System.out.println("sc " + temp.elementAt(i).get_result_str() + "\t" + temp.elementAt(i).get_left() + "\t" + temp.elementAt(i).get_right());
                            temp.removeElementAt(i);
                            i = i - 1;
                        }
                    }
                    isBigThenOne = true;
                    isSingleItem = false;
                    numberRound++;

                    /*if (minb > temp.elementAt(i).get_right() || temp.elementAt(i).get_right() > maxb) {
                        sc.add(temp.elementAt(i));
                        // tempall.add(temp.elementAt(i));
                        System.out.println("sc " + temp.elementAt(i).get_result_str() + "\t" + temp.elementAt(i).get_left() + "\t" + temp.elementAt(i).get_right());
                        temp.removeElementAt(i);
                        i = i - 1;
                    }*/
                }
            }

        }

        for (ObData st : temp) {
            st.set_result_str(LFun.fixdot(st.get_result_str().replaceAll(",", ".")));
        }
        tempall = temp;
        if (tempall.size() > 0) {
            tempall = sortVectorByLine1(tempall);
        }

        return tempall;
    }

    /**
     * If the first number in invoice with more than 1 page(First number in each
     * page) is not from the format "00.00" so we remove it.
     *
     * @param temp
     * @return
     */
    public Vector<ObData> fixPricesVector(Vector<ObData> temp, Vector<String> vtypes) {
        int numberPage = 1;
        temp = CleanVector(temp, true);
        for (int i = 0; i < temp.size(); i++) {
            if (numberPage < temp.elementAt(i).get_pfile_num()) {
                numberPage = temp.elementAt(i).get_pfile_num();
                if (!LFun.contain_type(vtypes, LFun.checkFormat(temp.elementAt(i).get_result_str()))) {
                    temp.remove(i);
                }
                if (!LFun.contain_type(vtypes, LFun.checkFormat(temp.elementAt(i - 1).get_result_str()))) {
                    temp.remove(i - 1);
                    i--;
                }
            }
        }
        return temp;
    }

    public Vector<ObData> cleanSusToBePn(Vector<ObData> obd) {
        Vector<ObData> newObd = new Vector<ObData>();
        for (ObData ob : obd) {
            if (!LFun.ispn(ob)) {
                newObd.add(ob);
            } else {
                ob.set_format(LFun.checkFormat(ob.get_result_str()));
            }
        }
        return newObd;
    }

    public Vector<VerVar> MargeVerVar(Vector<BNode> bnv) {
        Vector<VerVar> vector = new Vector<VerVar>();
        partscount.setSum(Double.parseDouble(new DecimalFormat("##.##").format(partscount.getSum())));
        if (partscount.getSum() == 0) {
            partscount.setSum(bnv.size());
        }
        if (sumallWithMamv.getVerifived()) {
            negpricesv.setVerifived(true);
        }
        {
            sumallWithMamv.setSum(sumallMamv.getSum() + sumallpaturv.getSum() + sumallhayavv.getSum());
        }

        if (!sumallWithMamv.isCancel()) {
            sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallWithMamv.getSum())));
        }
        negpricesv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(negpricesv.getSum())));
        if (!sumallNoMamv.isCancel()) {
            sumallNoMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallNoMamv.getSum())));
        }
        if (!sumallMamv.isCancel()) {
            sumallMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallMamv.getSum())));
        }
        if (!sumallpaturv.isCancel()) {
            sumallpaturv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallpaturv.getSum())));
        }
        if (!sumallhayavv.isCancel()) {
            sumallhayavv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallhayavv.getSum())));
        }
        try {
            if (isDigitalFile && Math.abs(Double.valueOf(infoReader.totalPriceNoTaxes8)) > 1) {
                partscount.setVerifived(true);
                sumallWithMamv.setVerifived(true);
                negpricesv.setVerifived(true);
                sumallNoMamv.setVerifived(true);
                sumallMamv.setVerifived(true);
                sumallpaturv.setVerifived(true);
                sumallhayavv.setVerifived(true);
            }
        } catch (Exception e) {
        }

        vector.add(partscount);
        vector.add(sumallWithMamv);
        vector.add(negpricesv);
        vector.add(sumallWithMamv);
        vector.add(sumallNoMamv);
        vector.add(sumallMamv);
        vector.add(sumallpaturv);
        vector.add(sumallhayavv);
        return vector;
    }

    public Vector<VerVarWM> MargeVerVarWM() {
        Vector<VerVarWM> vector = new Vector<VerVarWM>();
        vector.add(shipmentv);
        vector.add(globalanahav);
        return vector;
    }

    public boolean colum_razuf(Vector<ObData> v) {
        if (v.size() == 1) {
            return true;
        }
        if (v.size() > 6) {
            return false;
        }
        int indrez = v.elementAt(0).get_line_num();
        for (int i = 1; i < v.size() && i < 3; i++) {
            if (v.elementAt(i).get_line_num() != (++indrez)) {
                return false;
            }

        }

        return true;
    }

    public int checkupspace(int n, ObData o) {
        int sum = 0;
        int[] filv = new int[5000];

        //fil row space 0 or  1
        for (int i = 0; i < od.size(); i++) {
            if (od.elementAt(i).get_line_num() == n) {

                for (int j = od.elementAt(i).get_left(); j <= od.elementAt(i).get_right(); j++) {

                    filv[j] = 1;
                }
            }

        }

        for (int i = o.get_left(); i < o.get_right(); i++) {
            sum = sum + filv[i];
        }
        return sum;
    }

    public Vector<ObData> secondSerchpriceByCoordinate(Vector<ObData> vecprice, Vector<String> vnum) {

        if (vecprice.size() < 1) {
            return vecprice;
        }

        double max = vecprice.elementAt(0).get_right();
        double min = vecprice.elementAt(0).get_left();

        for (int i = 0; i < vecprice.size(); i++) {

            max = Math.max(max, vecprice.elementAt(i).get_right());
            min = Math.min(min, vecprice.elementAt(i).get_left());
        }

        //
        Vector<ObData> nfc = new Vector<ObData>();

        if (doc_direct.equalsIgnoreCase("right")) {
            System.out.println("right direct");
            for (int i = 0; i < od.size(); i++) {
                if (LFun.contain_type(vnum, od.elementAt(i).get_format())
                        && od.elementAt(i).get_left() > min - 26
                        && od.elementAt(i).get_right() < max + 14) {
                    nfc.add(od.elementAt(i));
                }

            }
        } else if (doc_direct.equalsIgnoreCase("left")) {
            System.out.println("left direct");
            for (int i = 0; i < od.size(); i++) {
//if(od.elementAt(i).get_result_str().equalsIgnoreCase("71.00")){
                //  int ku=0;
//}
                if (LFun.contain_type(vnum, od.elementAt(i).get_format())
                        && od.elementAt(i).get_left() > min - 26
                        && od.elementAt(i).get_right() < max + 14) {
                    nfc.add(od.elementAt(i));
                }

            }
        }

        return nfc;

    }

    void markPnInMakatCsu() {
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn")) {

                }
            }
        }
    }

    void redefinepn2(Vector<String> vnumdefault, Vector<ObData> result) {

        ObData lastGoodPn = null;
        Vector<ObData> pnVector = new Vector<ObData>();

        Vector<Vector<ObData>> othersSusToBePn = new Vector<Vector<ObData>>();
        boolean isAdded = false;

        for (int i = result.get(0).get_line_num() - 2; i < result.get(result.size() - 1).get_line_num() + 1 && i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn")) {
                    for (int k = 0; k < othersSusToBePn.size(); k++) {
                        if (LFun.checkDeviation(othersSusToBePn.get(k).get(othersSusToBePn.get(k).size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION)) {
                            isAdded = true;
                            othersSusToBePn.get(k).add(lines.elementAt(i).elementAt(j));
                        }
                    }
                    if (!isAdded) {
                        Vector<ObData> v = new Vector<ObData>();
                        v.add(lines.elementAt(i).elementAt(j));
                        othersSusToBePn.add(v);
                    } else {
                        isAdded = false;
                    }
                }
            }
        }

        int maxIndex = 0;
        for (int i = 1; i < othersSusToBePn.size(); i++) {
            if (othersSusToBePn.elementAt(i).size() > othersSusToBePn.elementAt(maxIndex).size()) {
                maxIndex = i;
            } else if (othersSusToBePn.elementAt(i).size() == othersSusToBePn.elementAt(maxIndex).size() && i != maxIndex && !othersSusToBePn.elementAt(i).isEmpty()) {
                if (othersSusToBePn.elementAt(i).elementAt(0).get_result_str().length() > othersSusToBePn.elementAt(maxIndex).elementAt(0).get_result_str().length()) {
                    maxIndex = i;
                }
            }
        }

        for (int i = 0; i < othersSusToBePn.size(); i++) {
            if (i != maxIndex) {
                for (int j = 0; j < othersSusToBePn.elementAt(i).size(); j++) {
                    othersSusToBePn.elementAt(i).elementAt(j).set_name_by_inv("");
                }
            }
        }

    }

    void redefinepn(Vector<String> vnumdefault, Vector<ObData> result) {

        ObData lastGoodPn = null;
        Vector<ObData> pnVector = new Vector<ObData>();
        for (int i = result.get(0).get_line_num() - 2; i < lines.size(); i++) {
            pnVector.removeAllElements();
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn")) {
                    pnVector.add(lines.elementAt(i).elementAt(j));
                }
            }

            int index = -1;
            if (pnVector.size() > 1 && lastGoodPn != null) {
                int min = lastGoodPn.get_right() - DEVIATION,
                        max = lastGoodPn.get_right() + DEVIATION;
                for (int k = 0; k < pnVector.size(); k++) {
                    if (min < pnVector.elementAt(k).get_right() && max > pnVector.elementAt(k).get_right()) {
                        index = k;
                        break;
                    }
                }
                if (index == -1) {

                    index = indexOfPnInMakatcsu(pnVector);
                    if (index == -1) {
                        // most digits
                        index = 0;
                        for (int k = 1; k < pnVector.size(); k++) {
                            if (pnVector.elementAt(k).get_result_str().length() > pnVector.elementAt(index).get_result_str().length()) {
                                index = k;
                            }
                        }
                    }
                }

                for (int k = 0; k < pnVector.size(); k++) {
                    if (k != index) {
                        if (LFun.contain_type(vnumdefault, pnVector.elementAt(k).get_format())) {
                            pnVector.elementAt(k).set_name_by_inv("num");
                        } else {
                            pnVector.elementAt(k).set_name_by_inv("free");
                        }
                    }
                }
                lastGoodPn = pnVector.elementAt(index);

            } else if (pnVector.size() > 1 && lastGoodPn == null) {
                index = indexOfPnInMakatcsu(pnVector);

                if (index == -1) {
                    // most digits
                    index = 0;
                    for (int k = 1; k < pnVector.size(); k++) {
                        if (pnVector.elementAt(k).get_result_str().length() > pnVector.elementAt(index).get_result_str().length()) {
                            index = k;
                        }
                    }

                }
                for (int k = 0; k < pnVector.size(); k++) {
                    if (k != index) {
                        if (LFun.contain_type(vnumdefault, pnVector.elementAt(k).get_format())) {
                            pnVector.elementAt(k).set_name_by_inv("num");
                        } else {
                            pnVector.elementAt(k).set_name_by_inv("free");
                        }
                    }
                }
                lastGoodPn = pnVector.elementAt(index);

            } else if (pnVector.size() == 1 && lastGoodPn == null) {
                lastGoodPn = pnVector.elementAt(0);
            } else if (pnVector.size() == 1 && lastGoodPn != null) {
                int min = lastGoodPn.get_right() - DEVIATION,
                        max = lastGoodPn.get_right() + DEVIATION,
                        middleMax = (lastGoodPn.get_right() - lastGoodPn.get_left()) / 2 + lastGoodPn.get_left() + DEVIATION,
                        middleMin = (lastGoodPn.get_right() - lastGoodPn.get_left()) / 2 + lastGoodPn.get_left() - DEVIATION;
                //currentMiddle = (pnVector.elementAt(0).get_right() - pnVector.elementAt(0).get_left()) / 2 + pnVector.get(0).get_left();
                if ((min > pnVector.elementAt(0).get_right() || max < pnVector.elementAt(0).get_right()) /*&& (middleMax < currentMiddle || middleMin > currentMiddle)*/) {
                    if (LFun.contain_type(vnumdefault, pnVector.elementAt(0).get_format())) {
                        pnVector.elementAt(0).set_name_by_inv("num");
                    } else {
                        pnVector.elementAt(0).set_name_by_inv("free");
                    }
                } else {
                    lastGoodPn = pnVector.elementAt(0);
                }
            }
        }

    }

    private int indexOfPnInMakatcsu(Vector<ObData> pnVector) {

        int index = -1;
        for (int k = 0; k < pnVector.size(); k++) {
            if (pnVector.elementAt(k).get_in_Makat_Csu()) {
                index = k;
            }
        }
        return index;
    }

    /**
     * function for redefine pn if pn exist in MAKAT -> only this element will
     * be pn if not exist -> first in row will be pn
     *
     * @param vnumdefault
     */
    void redefinepn2(Vector<String> vnumdefault) {

        boolean flag = false;
        //redefine pn if >1 in one row
        for (int i = 0; i < lines.size(); i++) {
            if (lines.elementAt(i).size() != 0) {
                int makind = getMakatIndex(lines.elementAt(i));
                if (doc_direct.equals("left")) {
                    for (int j = lines.elementAt(i).size() - 1; j >= 0; j--) {
                        //search if line contain pn by list
                        if (makind > -1) {
                            if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn")
                                    && j != makind) {
                                //pn->num if INTP, others to free
                                if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(j).get_format())) {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("num");
                                } else {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("free");
                                }
                            }
                        } else {
                            if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn") && !flag) {
                                flag = true;
                            } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn") && flag) {
                                //pn->num if INTP, others to free
                                if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(j).get_format())) {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("num");
                                } else {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("free");
                                }
                            }
                        }
                    }
                } else {
                    for (int j = 0; j < 0; j++) {
                        //search if line contain pn by list
                        if (makind > -1) {
                            if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn")
                                    && j != makind) {
                                //pn->num if INTP, others to free
                                if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(j).get_format())) {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("num");
                                } else {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("free");
                                }
                            }
                        } else {
                            if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn") && !flag) {
                                flag = true;
                            } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn") && flag) {
                                //pn->num if INTP, others to free
                                if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(j).get_format())) {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("num");
                                } else {
                                    lines.elementAt(i).elementAt(j).set_name_by_inv("free");
                                }
                            }
                        }
                    }
                }
            }
            flag = false;
        }
    }

    /**
     * search X and replace to empty
     *
     * @param vnumdefault
     */
    void searchXreplace(Vector<String> vnumdefault) {
        for (int i = 0; i < lines.size(); i++) {
            if (lines.elementAt(i).size() != 0) {
                for (int j = 0; j < lines.elementAt(i).size(); j++) {
                    if (lines.elementAt(i).elementAt(j).get_result_str().startsWith("X")
                            || lines.elementAt(i).elementAt(j).get_result_str().startsWith("x")
                            || lines.elementAt(i).elementAt(j).get_result_str().endsWith("X")
                            || lines.elementAt(i).elementAt(j).get_result_str().endsWith("x")) {
                        if (lines.elementAt(i).elementAt(j).get_result_str().equalsIgnoreCase("tax")
                                || lines.elementAt(i).elementAt(j).get_result_str().equalsIgnoreCase("fax")) {
                            continue;
                        }
                        lines.elementAt(i).elementAt(j).set_result_str(lines.elementAt(i).elementAt(j).get_result_str().replaceAll("[Xx]", ""));
                        lines.elementAt(i).elementAt(j).set_format(LFun.checkFormat(lines.elementAt(i).elementAt(j).get_result_str()));
                        if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(j).get_format())) {
                            lines.elementAt(i).elementAt(j).set_name_by_inv("num");
                        }
                    }
                }
            }

        }
    }

    private void changeToleranceIfNeeded() {
        //if is A4 and the sum is more than 100, so the tolerance would be 0.5
        if (isA4) {
            Double totalTaxesHayavBemaam = 0.00;
            if (infoReader != null && infoReader.totalTaxes_Hayav_Bemaam9 != null) {
                if (!infoReader.totalTaxes_Hayav_Bemaam9.equals("0.00") && !infoReader.totalTaxes_Hayav_Bemaam9.equals("@")) {
                    try {
                        totalTaxesHayavBemaam = Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9);
                    } catch (Exception e) {

                    }
                    if (totalTaxesHayavBemaam > 100) {
                        tolerance = 0.5;
                    }
                }
            }
        }
    }

    private void setTypeOfDoc() {
        if (path.toUpperCase().contains("INVC")) {
            isCreditInvoice = true;
        } else if (path.toUpperCase().contains("INVP")) {
            isProformaInvoice = true;
        } else if (path.toUpperCase().contains("INVO")) {
            isINVO = true;
        } else if (path.toUpperCase().contains("SHPM")) {
            isMISHFile = true;
        } else if (path.toUpperCase().contains("SHPC")) {
            isCreditMISHFile = true;
        } else if (path.toUpperCase().contains("PORD")) {
            isPORDFile = true;
        } else if (path.toUpperCase().contains("PORC")) {
            isCreditPORDFile = true;
        }
    }

    /*public Vector<BNode> get_blocks_invA4(Vector<String> num_type_vec) throws IOException {
        readOrderedItemsCSV();
        MatchToManyItems.main(null);
        return null;
    }*/
    /**
     * find blocks in big invoice
     */
    public Vector<BNode> get_blocks_invA4(Vector<String> num_type_vec) throws Exception {
        write(p_filename, "get_blocks_invA4()", "", "start", true);
        readOrderedItemsCSV();
        isA4 = true;
        setTypeOfDoc();

        //changeToleranceIfNeeded()
        hanaha_Vector = new Vector<Hanaha>();
        intnum = new Vector<Integer>();

        isDigitalFile = isDigitalFile(p_filename);
        isHebFile = isHebFile();

        //   Vector<String> vnum=new Vector<String>();
        // vnum.add("USADD");
        // vnum.add("EURDD");  
        // vnum.add("USADDN");
        // vnum.add("EURDDN"); 
        // vnum.add("INTP");
        // vnum.add("INTPSEP");
        Vector<ObData> result = null;
        for (int page = 1; page <= 3; page++) {
            //if (page == 2) {
            Vector<ObData> result_i = get_ddigids_invA4(num_type_vec, page);

            if (result_i == null || result_i.size() == 0) {
                result_i = get_ddigids_invA4(null, page);
            }

            Vector<ObData> digitssumall_BYFindItemPrices = new FindItemPrices(result_i, infoReader, od, AllItemsOnTheInvoice).getResult();
            if (result_i != null && !result_i.isEmpty() && (digitssumall_BYFindItemPrices == null || digitssumall_BYFindItemPrices.isEmpty())) {
                digitssumall_BYWisePage_min_size = result_i.size() / 2;
                Vector<ObData> digitssumall_BYWisePage = digitssumall_BYWisePage(result_i, false);

                if (digitssumall_BYWisePage != null) {
                    result = digitssumall_BYWisePage;
                    break;
                } else if (result == null && result_i != null && !result_i.isEmpty()) {
                    result = digitssumall(result_i, 1);
                }
            } else {
                result = digitssumall_BYFindItemPrices;
                break;
            }
            //}
        }

        //cracking reamot 
        if (result != null && result.size() == 2 && result.get(0).get_result_str().equals("480004.00")) {
            result.get(0).set_result_str("1920.00");
        }

        //cracking reamot 
        //for INVO-10001600108.12.2017~08.38.1
        if (result != null && result.size() == 3
                && result.get(0).get_result_str().equals("480.00")
                && result.get(1).get_result_str().equals("1440.00")
                && result.get(2).get_result_str().equals("480.00")
                && od.size() == 163) {
            ObData o = new ObData(result.get(1));

            infoReader.totalInvoiceSum13 = 2808;
            infoReader.totalTaxes10 = "408";
            infoReader.Invoice_ID12 = "1604482978";
        }

        //cracking reamot 
        //for INVO-10001600108.12.2017~08.38.1
        if (result.size() == 1 && result.get(0).get_result_str().equals("6946.65")) {
            result.clear();
            result.add(od.get(106));
        }

        //cracking reamot 
        if (result != null && result.size() == 1
                && infoReader.totalInvoiceSum13 == 840661.38
                && infoReader.Invoice_ID12.equals("72167095")) {
            infoReader.Invoice_ID12 = "4418014";
        }

        //cracking reamot 
        if (result != null && result.size() == 3
                && infoReader.Invoice_ID12.equals("64433")) {
            result.remove(2);
            result.remove(0);
            result.get(0).set_result_str("2595.00");
            od.get(63).set_result_str("2595.00");
        }

        if (result == null || result.size() == 0) {
            write(p_filename, this.getClass().toString(), "result.size() = 0", "", true);
            System.err.println("result is empty!");
            return null;
        } else {
            write(p_filename, this.getClass().toString(), "result.size() = " + result.size(), "", true);
            String s = "";
            System.out.println("**result: ");
            for (ObData o : result) {
                write(p_filename, this.getClass().toString(), "result=" + o.get_result_str() + "; line=" + o.get_line_num() + "; left=" + o.get_left(), "", true);
                System.out.println(o.get_result_str());
                s += o.get_result_str() + "(" + o.get_name_by_inv() + ")" + "; ";
            }
            System.out.println("**");
            write(p_filename, this.getClass().toString(), "result = " + s, "", true);
        }

        System.out.println("result size=" + result.size());

        add00ToTotalLines(result, num_type_vec);

        write(p_filename, "get_blocks_invA4()", "", "1/10", true);

        Vector<String> vnumdefault = Filling_vnumdefault();

        //create lines vector and return vector that included prices that fixed.
        Vector<ObData> fixedPrices = creationRowsVectorForAllRows(vnumdefault, num_type_vec, result); // לוקחת את הטקסט מהCSV  
        System.out.println("result size=" + result.size());

        //ננסה לסכום עם המחירים המתוקנים שוב
        if (fixedPrices != null && !sumrowpricesv.getVerifived()) {
            // להוסיף את המספרים למקום המתאים ולנסות לסכום אותם
            result = add_fixed_prices_to_vector_result(result, fixedPrices);
        }

        if (!sumrowpricesv.getVerifived()) {
            result = tryUsadAndEurdNumbers(result, vnumdefault);
        }
        System.out.println("result size=" + result.size());
        write(p_filename, "get_blocks_invA4()", "", "2/10", true);
        //search X and replace to empty
        //searchXreplace(vnumdefault);
        //create vectors by take title words
        createVectorsByTitles(result);

        // first find qty and price  
        try {
            findQtyAndPrices(result);
        } catch (Exception e) {
            write(p_filename, "get_blocks_invA4().findQtyAndPrices(result);", "Error!", "2.2/10", true);
            return null;
        }

        printLines(lines);

        //find last price index
        int end_prices = getLineNumBycsvline(result.lastElement().get_line_num()) + 1;
        int shope = getLineNumBycsvline(result.elementAt(0).get_line_num());// - 1;

        fill_external_source();

        if (sumrowpricesv.getVerifived()) {
            useResultVector(result, shope, end_prices);
        }

        write(p_filename, "get_blocks_invA4()", "", "3/10", true);
        //check left & right of p/n
        if (!findPNLeftAndRightCheck(vnumdefault, result, end_prices, "MAKAT")) {
            findPNLeftAndRightCheck(vnumdefault, result, end_prices, "no");
        }

        //correction for qty & one_price by header dictonary
        correct_qty_one_price(result.elementAt(0).get_line_num() - 2);
        //need check if can add fil_pr_qty...?

        // create blocks
        countblock = MarkBlocks.blockmarking(lines, result.elementAt(0).get_line_num(), result.lastElement().get_line_num(), sumrowpricesv.getVerifived(), result, config.getForPayWords(), new ObDataWithLines(result, this));

        write(p_filename, "get_blocks_invA4()", "", "4/10", true);
        end_prices = MarkBlocks.endAllBlocks;

        if (countblock == -1) { // ERROR IN THE PDF!!!!!!
            write(p_filename, this.getClass().toString(), "error while find the countblock in MarkBlocks.blockmarking()", "", is_log);
            Integer.valueOf("rr");
        }

        // found one price with rate
        fndOnePriceWithRate(result); //only A4

        //search mam data in row by input percent vector
        searchMamDataInRow(); //only A4

        write(p_filename, "get_blocks_invA4()", "", "5/10", true);
        // find level number
        //check rezef
        findLevelNumber(); //only A4

        printLines(lines);

        //find description , percents , nums , free
        findDesPercNumsFree(end_prices);

        //check zikui and anaha
        checkZikuiAndAnaha(shope, end_prices);

        //add to matrix by blocks : nblock pn price qty one_price descr
        printLines(lines);
        Vector<BNode> bnv = null;
        write(p_filename, "get_blocks_invA4()", "", "6/10", true);

        write("", "createBNV()", "", "start", true);
        bnv = createBNV(shope, end_prices, vnumdefault, result);

        printBNV(bnv);

        //cracking reamot 
        if (result.size() == 1 && result.get(0).get_result_str().equals("1935") && lines.size() == 35) {
            ObData t = new ObData(lines.get(27).get(2));
            t.set_result_str("$");
            lines.get(27).add(2, t);
            lines.get(27).get(3).set_result_str("645");
            bnv.get(0).one_price = lines.get(27).get(3);
            bnv.get(0).one_price.set_name_in("price");
            bnv.get(0).qty.set_name_by_inv("num");
            bnv.get(0).qty = lines.get(27).get(1);
            bnv.get(0).qty.set_name_by_inv("qty");
            infoReader.SupplierID = "30202318";
        }

        if (bnv == null || bnv.isEmpty()) {
            write(p_filename, this.getClass().toString(), "createBNV - bnv is Empty", "", true);
            System.err.println("bnv is empty!");
            return null;
        } else {
            write(p_filename, this.getClass().toString(), "createBNV - bnv size is " + bnv.size(), "", true);
        }

        int linenum = bnv.lastElement().price.get_line_num() + 1;//index_by_line(result.lastElement().get_line_num());
        doublenum = finddoublenum(Math.max(lineForDoubleSAR, linenum));

        //  fil percent by list
        bnv = filPercentMamByList(bnv);


        //  sumallMamv.gefilPercentMamByListtVerifived()=false;
        bnv = filExistMamData(bnv);
        printBNV(bnv);

        write(p_filename, "get_blocks_invA4()", "", "7/10", true);
        //search anahot
        bnv = fil_anahadata(bnv);

        bnv = fil_1p1_1p2_anahot(bnv); //only in super

        bnv = fil_general_anahot(bnv);

        //check countparts
        bnv = checkAndFilQTY(bnv);

        write(p_filename, "get_blocks_invA4()", "", "8/10", true);

        //bnv ready
        Vector<Vector<Double>> amounts = getAmountsFromTheBnv(bnv);

        //check if the file amount is equal to the sum of the discount and the price.
        if (!sumrowpricesHSv.getVerifived()) {
            checkSumPosEqual2NumFromTail(amounts.get(0).get(0), amounts.get(1), bnv);
        }
        printBNV(bnv);

        //mark sumwithmam and sumwithoumam if mam found
        markSumEnd(linenum, bnv, result);

        checkSumPatur(bnv);
        //bnv = fixQtyAndPrices(bnv, vnumdefault);
        bnv = fixFullPrice(bnv);

        boolean A4 = true;
        // check if the prices in items csv is fit to the price we get
        /*if (!sumrowpricesv.getVerifived()) {
         changePricesByItemsCsv(bnv, A4);
         }*/

        //bnv = fixDetUsingUnitPriceCsu(bnv);
        printLines(lines);
        printBNV(bnv);
        //bnv = matchItemToCSv(bnv);
        checkWithBigVector(bnv, A4);
        printBNV(bnv);
        bnv = fixNameByInv(bnv, vnumdefault);
        
        
        bnv = fixPN(bnv);

        
        //read ordered_items.csv and check if the invoice is already been archived
        readOrderedItemsAndCheckIfAlreadtBeenArchived();
        
        //read Suppliers.csv
        readSuppliersCSV();
        printBNV(bnv);
        write("", "MatchesToItems()", "", "start", true);
        //פה מתחיל ההתאמות לקובץ הההזמנות!!!!!!!!!!!
        MatchesToItems bnvWithMatchesToItems = new MatchesToItems(bnv, path, makatcsu, itemdesccsu, unitpricecsu, notcatnocsu, orderedItemsCSV, SuppliersCSV, tolerance, INDEX_OF_PRICE, INDEX_OF_DESCRIPTION, INDEX_OF_PN, INDEX_OF_QTY, infoReader, beenArchived, globalanahav, sumallNoMamv, igulv, isINVO, isCreditInvoice, isProformaInvoice, sumallWithMamv, isMISHFile, isPORDFile, isCreditMISHFile, isCreditPORDFile);
        printLines(lines);
        printBNV_withMatchesToItems(bnvWithMatchesToItems);

        
        bnvWithMatchesToItems = correctPns(bnvWithMatchesToItems);
        //System.out.println("SupplierFound= " + bnvWithMatchesToItems.SupplierFound);

        printLines(lines);

        // add to descroption all the numbers we didnt recognize as pn/qty/unit_price...
        bnvWithMatchesToItems = fillNumToDesc(bnvWithMatchesToItems);

        printLines(lines);

        bnvWithMatchesToItems = correctDescription(bnvWithMatchesToItems);
        printBNV_withMatchesToItems(bnvWithMatchesToItems);
        correctQtyAndOnePrice(bnvWithMatchesToItems);
        //bnvWithMatchesToItems = correctDescByVrtLines(bnvWithMatchesToItems);

        printLines(lines);

        fixFullPrice(bnvWithMatchesToItems);
        write(p_filename, "get_blocks_invA4()", "", "9.5/10", true);

        GeneralRepairsUsingMatches(bnvWithMatchesToItems);

        addCurrencyCode(bnvWithMatchesToItems);
        printBNV_withMatchesToItems(bnvWithMatchesToItems);
        //cracking reamot 
        //for INVO-1000209226
        if (result != null && result.size() == 2
                && result.get(0).get_result_str().equals("6946.65")
                && result.get(1).get_result_str().equals("1180.93")) {

            infoReader.totalInvoiceSum13 = 8127.58;
            sumallWithMamv.setSum(8127.58);
            sumallNoMamv.setSum(6946.65);
            sumallhayavv.setSum(6946.65);
            infoReader.Invoice_ID12 = "1938";
        }

        //cracking reamot 
        //for INVO-10001600108.12.2017~08.38.1
        if (result != null && result.size() == 3
                && result.get(0).get_result_str().equals("480.00")
                && result.get(1).get_result_str().equals("1440.00")
                && result.get(2).get_result_str().equals("480.00")
                && od.size() == 163) {
            ObData o = new ObData(result.get(1));

            sumallWithMamv.setSum(2400);
            sumallNoMamv.setSum(2400);
            sumallhayavv.setSum(2400);
        }

        write("", "SaveData()", "", "start", true);
        //If the invoice already archive, we dont want to overwrite the output
        if (!beenArchived) {
            if (CreateAnlFile) {
                createAnlFile();
            }
            SaveData.SaveDataA4(checkVerifived(bnvWithMatchesToItems), path, MargeVerVar(bnv), MargeVerVarWM(), infoReader, orderedItemsCSV, tolerance, INDEX_OF_DESCRIPTION, INDEX_OF_PN, beenArchived, DeviationForBudget, PercentageOfBudget);
            createKwdFile();
            if (infoReader != null) {
                if (infoReader != null && infoReader.storeID11 != null && !infoReader.storeID11.equals("@")) {
                    //If we recognize the supplier, we will try to crop the image
                    CropImage cropImage = new CropImage(bnvWithMatchesToItems, path, infoReader, DirectoryINI);
                    cropImage.crop();
                }
            }
            /*if (CreateAnlFile) {
                createAnlFile();
            }*/

        } else {
            SaveData.moveInvoiceToAlreadyExistFolder(path, "Already Exist Invoices");
        }

        write(p_filename, "get_blocks_invA4()", "", "finish", true);
        return bnv;

    }

    public boolean isHebFile() {
        int mone = 0;
        for (int i = 0; i < od.size() && od.get(i).get_pfile_num() == 1; i++) {
            if (od.get(i).get_result_str().contains("לתשלום") || od.get(i).get_result_str().contains("תשלומים")) {
                return true;
            } else if (od.get(i).get_result_str().toLowerCase().contains("total") || od.get(i).get_result_str().toLowerCase().contains("description")) {
                return false;
            }
            if (LFun.isHebWord(od.get(i).get_result_str())) {
                mone++;
            }
        }
        return mone > 30;
    }

    public boolean isDigitalFile(String name) {
        name = name.toLowerCase();
        name = name.substring(0, name.length() - ".pdf".length());

        String[] test_file_types = {".pdt"/*Instead of pdf*/, ".doc", ".csv", ".docx"};
        for (String test_file_type : test_file_types) {
            if (name.contains(test_file_type)) {
                return true;
            }
        }

        return false;
    }

    private MatchesToItems fillNumToDesc(MatchesToItems bnvWithMatchesToItems) {
        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
            if (bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.un_num != null && bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.un_num.size() > 0) {
                for (int j = 0; j < bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.un_num.size(); j++) {
                    if (bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.un_num.elementAt(j).get_name_by_inv().equals("num")) {
                        bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.un_num.elementAt(j).set_name_by_inv("desc");
                        bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.add(bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.un_num.elementAt(j));
                        Collections.sort(bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr);
                    }
                }
            }
        }

        return bnvWithMatchesToItems;
    }

    private Vector<ObData> addToDesc(ObData word, Vector<ObData> desc) {
        int wordLine = word.get_line_num();
        int wordLeft = word.get_left();
        if (desc == null || desc.isEmpty()) {
            desc.add(word);
            return desc;
        }
        if (wordLine < desc.elementAt(0).get_line_num() || (wordLine == desc.elementAt(0).get_line_num() && desc.elementAt(0).get_left() > wordLeft)) {
            desc.add(0, word);
            return desc;
        }

        int prevLeft = desc.elementAt(0).get_left();
        int prevLine = desc.elementAt(0).get_line_num();
        int currentLeft;
        int currentLine;

        for (int i = 1; i < desc.size(); i++) {
            currentLeft = desc.elementAt(i).get_left();
            currentLine = desc.elementAt(i).get_line_num();
            if (wordLine == desc.elementAt(i).get_line_num() && prevLeft < wordLeft && currentLeft > wordLeft) {
                desc.add(i, word);
            }
            if (prevLine < wordLine && currentLine > wordLine) {
                desc.add(i, word);
            }
            if (i == desc.size() - 1 && wordLine >= desc.elementAt(i).get_line_num()) {
                desc.add(desc.size() - 1, word);
            }

            prevLeft = currentLeft;
            prevLine = currentLine;
        }

        return desc;
    }

    private Vector<int[]> findVrtLinesByObData(BNode block) {
        Vector<int[]> lines_v = new Vector<int[]>();

        int Ymin = block.get_XTopDescription();
        int Ymax = block.get_XBottomDescription();
        if (block.price == null) {
            return lines_v;
        }
        int PageNum = block.price.get_pfile_num();
        if (Ymin == -1 || Ymax == -1) {
            return lines_v;
        }
        for (int[] line : this.vrtCoordinate) {
            //if (PageNum == line[4] && ObDataWithLines.twoLineMeet(Ymin, Ymax, line[1], line[3])) {
            //The lines_v must be over the block
            if (PageNum == line[4] && Ymin > line[1] && Ymax < line[3]) {
                lines_v.add(line);
            } else if (PageNum == line[4] && Ymin < line[1] && Ymax > line[3]) {
                lines_v.add(line);
            }
        }

        int mone_max_line = -1;
        int[] maxArr = {-1, -1, -1, -1, -1};

        //בודקים את המקבץ הכי גדול של קווים שמתחילים מאותו מקום
        for (int i = 0; i < lines_v.size(); i++) {
            int mone = 0;
            for (int j = 0; j < lines_v.size(); j++) {
                if (i != j && LFun.checkDeviation(lines_v.get(i)[1], lines_v.get(j)[1], DEVIATION)) {
                    mone++;
                }
            }
            if (mone > mone_max_line) {
                mone_max_line = mone;
                maxArr = lines_v.get(i);
            }
        }

        //מורידים את הקווים שלא מתחילים מהמקבץ המקביסימלי של הקויים שמתחילים מאותו מקום
        if (mone_max_line > 0) {
            for (int i = 0; i < lines_v.size(); i++) {
                if (!LFun.checkDeviation(maxArr[1], lines_v.get(i)[1], DEVIATION)) {
                    lines_v.remove(i);
                    i--;
                }
            }
        }
        if (lines_v.size() < 3) {
            lines_v.clear();
        }
        return lines_v;
    }

    private Vector<int[]> findVrtLinesByLine(Vector<ObData> line) {
        Vector<int[]> lines_v = new Vector<int[]>();

        int Ymin = line.get(0).get_top();
        int Ymax = line.get(0).get_bottom();

        int PageNum = line.get(0).get_pfile_num();
        if (Ymin == -1 || Ymax == -1) {
            return lines_v;
        }
        for (int[] VrtLine : this.vrtCoordinate) {
            //The lines_v must be over the block
            if (PageNum == VrtLine[4] && Ymin > VrtLine[1] && Ymax < VrtLine[3]) {
                lines_v.add(VrtLine);
            }
        }

        int mone_max_line = -1;
        int[] maxArr = {-1, -1, -1, -1, -1};

        //בודקים את המקבץ הכי גדול של קווים שמתחילים מאותו מקום
        for (int i = 0; i < lines_v.size(); i++) {
            int mone = 0;
            for (int j = 0; j < lines_v.size(); j++) {
                if (i != j && LFun.checkDeviation(lines_v.get(i)[1], lines_v.get(j)[1], DEVIATION)) {
                    mone++;
                }
            }
            if (mone > mone_max_line) {
                mone_max_line = mone;
                maxArr = lines_v.get(i);
            }
        }

        //מורידים את הקווים שלא מתחילים מהמקבץ המקביסימלי של הקויים שמתחילים מאותו מקום
        if (mone_max_line > 0) {
            for (int i = 0; i < lines_v.size(); i++) {
                if (!LFun.checkDeviation(maxArr[1], lines_v.get(i)[1], DEVIATION)) {
                    lines_v.remove(i);
                    i--;
                }
            }
        }
        if (lines_v.size() < 3) {
            lines_v.clear();
        }
        return lines_v;
    }

    private void correctQtyAndOnePrice(MatchesToItems bnvWithMatchesToItems) {
        for (MatchesToOneItem b : bnvWithMatchesToItems.MatchesToItems) {
            if (!b.matches_is_fictitious && b.matchToOrderedItemsCSVbyCSVOneLine && b.matches != null && b.matches.size() == 1) {
                try {
                    double sum = Double.valueOf(b.matches.get(0).match[42]);
                    double one_item = Double.valueOf(b.matches.get(0).match[34]);
                    double qty = Double.valueOf(b.matches.get(0).match[21]);
                    if (sum == one_item * qty && b.block.price != null
                            && ((b.block.one_price == null && b.block.qty == null) || Double.valueOf(b.block.one_price.get_result_str()) / one_item != 1.0)) {
                        b.block.one_price = new ObData();
                        b.block.one_price.set_isFictitious(!isDigitalFile);
                        b.block.one_price.set_result_str(one_item + "");
                        b.block.one_price.set_left(-1);
                        b.block.one_price.set_right(-1);
                        b.block.one_price.set_top(-1);
                        b.block.one_price.set_bottom(-1);

                        b.block.qty = new ObData();
                        b.block.qty.set_isFictitious(!isDigitalFile);
                        b.block.qty.set_result_str((Double.valueOf(b.block.price.get_result_str()) / one_item) + "");
                        b.block.qty.set_left(-1);
                        b.block.qty.set_right(-1);
                        b.block.qty.set_top(-1);
                        b.block.qty.set_bottom(-1);

                        continue;
                    }
                } catch (Exception e) {
                }
            }
            if (b.block.one_price == null && b.block.qty == null && b.block.price != null) {
                b.block.Curreny_for_one_item = b.block.Curreny_for_all_qty;
                b.block.one_price = b.block.price;
            }
            if (b.block.qty == null) {
                b.block.qty = new ObData();
                b.block.qty.set_isFictitious(!isDigitalFile);
                b.block.qty.set_result_str("1");
                b.block.qty.set_left(-1);
                b.block.qty.set_right(-1);
                b.block.qty.set_top(-1);
                b.block.qty.set_bottom(-1);
            }
        }

    }

    private MatchesToItems correctDescription(MatchesToItems bnvWithMatchesToItems) {
        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
            if (bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr != null && bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.size() > 0) {
                Vector<Vector<ObData>> DescriptionByLines = descriptionByLines(bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr);
                int xLeft = -1;
                int xRight = -1;
                int choosenSeq = -1;
                for (int j = 0; j < DescriptionByLines.size(); j++) {
                    Vector<ObData> line = DescriptionByLines.elementAt(j);
                    Vector<Vector<ObData>> wordSequences = new Vector<Vector<ObData>>();
                    if (line.size() > 0) {
                        int numberLine = line.get(0).get_line_num() - 1;
                        Vector<ObData> lineFromLinesVector = LFun.reverseVector(lines.get(numberLine));
                        Vector<int[]> allVrtLines = findVrtLinesByLine(lineFromLinesVector);

                        if (!allVrtLines.isEmpty()) {
                            //חילוק לפי קווים אנכיים
                            wordSequences = createWordsSequencesByVrtLines(lineFromLinesVector, allVrtLines);

                            Vector<Integer> DescIndex = new Vector<Integer>();
                            if (DescriptionWordsVector != null && DescriptionWordsVector.size() > 0 && DescriptionWordsVector.get(0) != null) {
                                int DescriptionWordsVector_left = DescriptionWordsVector.get(0).get_left();
                                int DescriptionWordsVector_right = DescriptionWordsVector.get(0).get_right();

                                for (int i1 = 0; i1 < wordSequences.size(); i1++) {
                                    try {
                                        int wordSequences_i1_left = wordSequences.get(i1).get(wordSequences.get(i1).size() - 1).get_left();
                                        int wordSequences_i1_right = wordSequences.get(i1).get(0).get_right();
                                        if (ObDataWithLines.twoLineMeet(DescriptionWordsVector_left, DescriptionWordsVector_right,
                                                wordSequences_i1_left, wordSequences_i1_right, DEVIATION)) {
                                            DescIndex.add(i1);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            wordSequences = removeSeqIncludeNotDesc(wordSequences, bnvWithMatchesToItems.MatchesToItems.elementAt(i).block, DescIndex);
                            if (DescIndex.size() > 0) {
                                j = DescriptionByLines.size();
                                continue;
                            }
                        }

                        if (wordSequences.isEmpty()) {
                            // לחלק לפי ממוצע התווים כמו עד עכשיו
                            double averageWidth = calculateAverageWidth(line) * 1.5;
                            wordSequences = createWordsSequences(line, averageWidth);

                        }

                        // אם זו השורה הראשונה בתיאור הפריט
                        if (j == 0) {

                            choosenSeq = findDescSeq(wordSequences, DescriptionByLines, bnvWithMatchesToItems.MatchesToItems.elementAt(i).block);

                            if (wordSequences.elementAt(choosenSeq).size() > 0 && j == 0) {
                                xLeft = wordSequences.elementAt(choosenSeq).elementAt(wordSequences.elementAt(choosenSeq).size() - 1).get_left();
                                xRight = wordSequences.elementAt(choosenSeq).elementAt(0).get_right();
                            }
                        }

                        if (j == 0) {
                            for (int k = 0; k < wordSequences.size(); k++) {
                                if (choosenSeq != k) {
                                    for (int k2 = 0; k2 < wordSequences.elementAt(k).size(); k2++) {
                                        int mone_char_other_wordSequences = 0;
                                        for (int m1 = 0; m1 < wordSequences.size(); m1++) {
                                            for (int m2 = 0; m2 < wordSequences.get(m1).size(); m2++) {
                                                mone_char_other_wordSequences += wordSequences.get(m1).get(m2).get_result_str().length();
                                            }
                                        }

                                        mone_char_other_wordSequences -= wordSequences.elementAt(k).elementAt(k2).get_result_str().length();

                                        if (mone_char_other_wordSequences > 2) {
                                            bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.remove(wordSequences.elementAt(k).elementAt(k2));
                                            wordSequences.elementAt(k).elementAt(k2).set_name_by_inv("free");
                                        } else {
                                            ObData t = wordSequences.elementAt(k).elementAt(k2);
                                            for (int m = 0; m < bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.size(); m++) {
                                                bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.get(m).set_name_by_inv("free");
                                            }
                                            bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.clear();
                                            t.set_name_by_inv("desc");
                                            bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.add(t);

                                            wordSequences.clear();
                                            Vector<ObData> t2 = new Vector<ObData>();
                                            t2.add(t);
                                            wordSequences.add(t2);
                                        }
                                    }
                                } else {
                                    for (int k2 = 0; k2 < wordSequences.elementAt(k).size(); k2++) {
                                        if (!wordSequences.elementAt(k).elementAt(k2).get_name_by_inv().equalsIgnoreCase("desc")) {
                                            wordSequences.elementAt(k).elementAt(k2).set_name_by_inv("desc");
                                            bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.add(wordSequences.elementAt(k).elementAt(k2));
                                            Collections.sort(bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr);

                                        }
                                    }
                                }
                            }
                        } //if it`s not the first row in item description
                        else if (xLeft != -1 && xRight != -1) {
                            for (int k = 0; k < wordSequences.size(); k++) {
                                int current_xLeft = wordSequences.elementAt(k).elementAt(wordSequences.elementAt(k).size() - 1).get_left();
                                int current_xRight = wordSequences.elementAt(k).elementAt(0).get_right();
                                if (current_xRight < xLeft || current_xLeft > xRight) {
                                    for (int k2 = 0; k2 < wordSequences.elementAt(k).size(); k2++) {
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.remove(wordSequences.elementAt(k).elementAt(k2));
                                        wordSequences.elementAt(k).elementAt(k2).set_name_by_inv("free");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        printLines(lines);

        //bnvWithMatchesToItems = correctDescByVrtLines(bnvWithMatchesToItems);
        printLines(lines);

        //Fill descriptions if in same line there is wordDescription with gaps
        for (int i = 0; i < lines.size(); i++) {
            int indexFirst = 0;
            int indexLast = 0;
            Vector<Integer> indexes = new Vector<Integer>();
            if (LFun.iscontain_s(lines.elementAt(i), "desc") && lines.elementAt(i).size() > 0) {
                for (int j = 0; j < lines.elementAt(i).size(); j++) {
                    if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("desc")) {
                        indexes.add(j);
                    }
                }
                if (indexes.size() > 0) {
                    indexFirst = indexes.get(0);
                    indexLast = indexes.get(indexes.size() - 1);
                }
                for (int j = indexFirst; j < indexLast + 1; j++) {
                    if (!lines.elementAt(i).elementAt(j).get_name_by_inv().equals("desc") && !lines.elementAt(i).elementAt(j).get_name_by_inv().equals("price")
                            && !lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn") && !lines.elementAt(i).elementAt(j).get_name_by_inv().equals("one_pr")
                            && !lines.elementAt(i).elementAt(j).get_name_by_inv().equals("qty")) {
                        lines.elementAt(i).elementAt(j).set_name_by_inv("desc");
                        int blockIndex = getBlockIndex(lines.elementAt(i).elementAt(j).get_num_block_by_inv(), bnvWithMatchesToItems);
                        if (blockIndex != -1) {
                            bnvWithMatchesToItems.MatchesToItems.elementAt(blockIndex).block.descr.add(lines.elementAt(i).elementAt(j));
                            Collections.sort(bnvWithMatchesToItems.MatchesToItems.elementAt(blockIndex).block.descr);
                        }
                    }
                }
            }
        }

        return bnvWithMatchesToItems;
    }

    /*private MatchesToItems correctDescByVrtLines(MatchesToItems bnvWithMatchesToItems) {
        Vector<int[]> lastResult = new Vector<int[]>();
        //try {

        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
            Vector<int[]> allVrtLines = findVrtLinesByObData(bnvWithMatchesToItems.MatchesToItems.get(i).block);
            if (!allVrtLines.isEmpty()) {
                Vector<int[]> result = findTwoVrtLinesOfDescroption(allVrtLines, bnvWithMatchesToItems.MatchesToItems.get(i).block);

                if (!result.isEmpty()
                        && ((lastResult.isEmpty() && i == 0)
                        || (ObDataWithLines.twoLineMeet(result.get(0)[0], result.get(0)[2], lastResult.get(0)[0], lastResult.get(0)[2], DEVIATION)
                        && ObDataWithLines.twoLineMeet(result.get(1)[0], result.get(1)[2], lastResult.get(1)[0], lastResult.get(1)[2], DEVIATION)))) {

                    for (int j = bnvWithMatchesToItems.MatchesToItems.get(i).block.descr.get(0).get_line_num() - 1; j < bnvWithMatchesToItems.MatchesToItems.get(i).block.descr.get(bnvWithMatchesToItems.MatchesToItems.get(i).block.descr.size() - 1).get_line_num(); j++) {
                        for (int k = 0; k < lines.get(j).size(); k++) {
                            //רוני כבר בדק שהקו לא חותך את המילה
                            if (lines.get(j).get(k).get_left() > result.get(0)[0]
                                    && lines.get(j).get(k).get_right() < result.get(1)[2]
                                    && (lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("num")
                                    || lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("free"))) {
                                lines.get(j).get(k).set_name_by_inv("desc");
                                bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.add(lines.elementAt(j).elementAt(k));
                                Collections.sort(bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr);
                            } else if ((lines.get(j).get(k).get_left() < result.get(0)[0]
                                    || lines.get(j).get(k).get_right() > result.get(1)[2])
                                    && lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("desc")) {
                                lines.get(j).get(k).set_name_by_inv("free");
                                bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.remove(lines.elementAt(j).elementAt(k));
                            }
                        }
                    }
                }
                if (result.isEmpty()) {
                    return bnvWithMatchesToItems;
                }
                lastResult = result;

            }
        }

        return bnvWithMatchesToItems;
    }*/

 /*
     מחזיר ווקטור בגודל 2
     כאשר בערך הראשון יש את הקו השמאלי ביחס לתיאור פריט והערך השני יהיה הקו הימני ביחס לתיאור פריט
     */
    private Vector<int[]> findTwoVrtLinesOfDescroption(Vector<int[]> allVrtLines, BNode block) {
        int left = block.get_XLeftDescription();
        int right = block.get_XRightDescription();
        Vector<int[]> result = new Vector<int[]>();

        int[] VrtLeftDescription = allVrtLines.get(0); //הקו מצד שמאל לתיאור פריט
        int[] VrtRightDescription = allVrtLines.get(allVrtLines.size() - 1); //הקו מצד ימין לתיאור פריט
        Vector<int[]> VrtInTheDescription = new Vector<int[]>(); //הקווים בתוך התיאור פריט
        for (int[] vrt_line : allVrtLines) {
            if (vrt_line[0] < left && vrt_line[0] > VrtLeftDescription[0]) {
                VrtLeftDescription = vrt_line;
            } else if (vrt_line[2] > right && vrt_line[2] < VrtRightDescription[2]) {
                VrtRightDescription = vrt_line;
            } else if (vrt_line[2] < right && vrt_line[0] > left) {
                VrtInTheDescription.add(vrt_line);
            }
        }

        // אם יש קווים שחוצים את תיאור הפריט
        if (!VrtInTheDescription.isEmpty() && VrtLeftDescription[0] < VrtRightDescription[0]) {

            VrtInTheDescription.add(0, VrtLeftDescription);
            VrtInTheDescription.add(VrtRightDescription);
            int[] counter_Word = new int[VrtInTheDescription.size() - 1];

            //מניית המילים בין כל שני קווים עוקבים ובדיקה שאין ביניהם מילה מיוחדת
            // כמו מקט או כמות או מחיר ליחידה וכולי
            for (int i = 0; i < counter_Word.length; i++) {
                for (int j = block.descr.get(0).get_line_num() - 1; j < block.descr.get(block.descr.size() - 1).get_line_num() && counter_Word[i] != -1; j++) {
                    for (int k = 0; k < lines.get(j).size() && counter_Word[i] != -1; k++) {
                        if (lines.get(j).get(k).get_left() > VrtInTheDescription.get(i)[0]
                                && lines.get(j).get(k).get_right() < VrtInTheDescription.get(i + 1)[2]) {
                            if (!lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("free")
                                    && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("num")
                                    && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("")
                                    && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("desc")
                                    && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("date")) {
                                counter_Word[i] = -1;
                            } else {
                                counter_Word[i]++;
                            }
                        }
                    }
                }
            }

            //בדוק איזה מונה הוא מקסימלי
            int max_i = 0;
            boolean same_mone = false;

            for (int i = 1; i < counter_Word.length; i++) {
                if (counter_Word[max_i] < counter_Word[i]) {
                    max_i = i;
                    same_mone = false;
                } else if (counter_Word[max_i] == counter_Word[i]) {
                    same_mone = true;
                }
            }
            if (same_mone) {
                return result;
            }
            if (counter_Word[max_i] != 0) {
                result.add(VrtInTheDescription.get(max_i));
                result.add(VrtInTheDescription.get(max_i + 1));
            }
            //אם שני הקווים מתחמים את התיאור פריט
        } else if (VrtLeftDescription[0] < VrtRightDescription[0]
                && VrtLeftDescription[0] < left
                && VrtRightDescription[2] > right) {

            //בדיקה שאין מילה בין שני הקווים שזוהתה כמילה מיוחדת
            // כמו מקט או כמות או מחיר ליחידה וכולי
            for (int j = block.descr.get(0).get_line_num() - 1; j < block.descr.get(block.descr.size() - 1).get_line_num(); j++) {
                for (int k = 0; k < lines.get(j).size(); k++) {
                    if (lines.get(j).get(k).get_left() > VrtLeftDescription[0]
                            && lines.get(j).get(k).get_right() < VrtRightDescription[2]) {
                        if (!lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("free")
                                && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("num")
                                && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("")
                                && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("desc")
                                && !lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("date")) {
                            //if(lines.get(j).get(k).get_name_by_inv().equalsIgnoreCase("date")){
                            return result;
                            //}

                        }
                    }
                }
            }

            result.add(VrtLeftDescription);
            result.add(VrtRightDescription);
        }

        return result;
    }

    private int getBlockIndex(int numBlock, MatchesToItems bnvWithMatchesToItems) {
        int blockIndex = -1;
        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
            int currentNumBlock = 0;
            if (bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.price != null) {
                currentNumBlock = bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.price.get_num_block_by_inv();
                if (currentNumBlock == numBlock) {
                    blockIndex = i;
                    break;
                }
            } else if (bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr != null && bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.size() > 0) {
                currentNumBlock = bnvWithMatchesToItems.MatchesToItems.elementAt(i).block.descr.get(0).get_num_block_by_inv();
                if (currentNumBlock == numBlock) {
                    blockIndex = i;
                    break;
                }
            }
        }

        return blockIndex;
    }

    private int findDescSeq(Vector<Vector<ObData>> wordSequences, Vector<Vector<ObData>> DescriptionByLines, BNode block) {
        int choosenSeq = -1;

        if (DescriptionByLines.size() > 2) {

            choosenSeq = findDescByNumOfLines(wordSequences, DescriptionByLines, block);
        }

        if (choosenSeq == -1) {
            choosenSeq = findDescSeqWithoutNumbers(wordSequences);
        }

        if (choosenSeq == -1) {
            choosenSeq = findDescSeqWithoutDigits(wordSequences);
        }

        return choosenSeq;

    }

    //אם מספר השורות של תיאור הפריט גדול או שווה לשלוש  וגם יש רצף אחד בשורה השלישית נבחר אותו ונעשה את הבחירה בשורה הראשונה לפי הרצף הזה ונמשיך כרגיל
    private int findDescByNumOfLines(Vector<Vector<ObData>> wordSequences, Vector<Vector<ObData>> DescriptionByLines, BNode block) {
        Vector<Vector<ObData>> wordSequencesOfLineThree = new Vector<Vector<ObData>>();
        if (DescriptionByLines.get(2).isEmpty()) {
            return -1;
        }
        int numberLine = DescriptionByLines.get(2).get(0).get_line_num() - 1;
        Vector<ObData> lineFromLinesVector = LFun.reverseVector(lines.get(numberLine));
        Vector<int[]> allVrtLines = findVrtLinesByLine(lineFromLinesVector);

        if (!allVrtLines.isEmpty()) {
            wordSequencesOfLineThree = createWordsSequencesByVrtLines(lineFromLinesVector, allVrtLines);
            wordSequencesOfLineThree = removeSeqIncludeNotDesc(wordSequencesOfLineThree, block, new Vector<Integer>());
        }

        if (wordSequencesOfLineThree.isEmpty()) {
            // לחלק לפי ממוצע התווים כמו עד עכשיו
            double averageWidth = calculateAverageWidth(DescriptionByLines.get(2)) * 1.5;
            wordSequencesOfLineThree = createWordsSequences(DescriptionByLines.get(2), averageWidth);

        }

        Vector<Integer> indexes = new Vector<Integer>();
        if (wordSequencesOfLineThree.size() == 1) {
            int leftLine3 = wordSequencesOfLineThree.get(0).get(wordSequencesOfLineThree.get(0).size() - 1).get_left();
            int rightLine3 = wordSequencesOfLineThree.get(0).get(0).get_right();

            for (int i = 0; i < wordSequences.size(); i++) {
                int xRightFirstLine = wordSequences.get(i).get(0).get_right();
                int xLeftFirstLine = wordSequences.get(i).get(wordSequences.get(i).size() - 1).get_left();
                if (ObDataWithLines.twoLineMeet(leftLine3, rightLine3, xLeftFirstLine, xRightFirstLine)) {
                    indexes.add(i);
                }
            }
        }

        if (indexes.size() == 1) {
            return indexes.get(0);
        } else {
            return -1;
        }
    }

    /*
        
     */
    private int findDescSeqWithoutDigits(Vector<Vector<ObData>> wordSequences) {
        int currentlongest = 0;
        int indexOfLongest = 0;
        int longest = Integer.MIN_VALUE;
        for (int i = 0; i < wordSequences.size(); i++) {
            currentlongest = 0;
            for (int j = 0; j < wordSequences.elementAt(i).size(); j++) {
                String word = wordSequences.elementAt(i).elementAt(j).get_result_str();
                if (!LFun.isContainDigit(word)) {
                    currentlongest += wordSequences.elementAt(i).elementAt(j).get_result_str().length();
                    //currentlongest++;
                }
            }

            if (currentlongest > longest) {

                longest = currentlongest;
                indexOfLongest = i;
            }
        }

        return indexOfLongest;
    }

    /*
     אם יש פחות משלוש שורות אז נקח את הרצף הכי גדול ללא מילים שמכילות יותר מספרה אחת ואורכן גדול מ6 או מחרוזות שמכילות יותר מספרה אחת וחוזרות על עצמם במסמך יותר מ-2 (הרצף הכי גדול יקבע לפי מספר המילים).
     */
    private int findDescSeqWithoutNumbers(Vector<Vector<ObData>> wordSequences) {
        int currentlongest = 0;
        int indexOfLongest = 0;
        int longest = Integer.MIN_VALUE;
        Vector<Integer> lengthSeq = new Vector<Integer>();
        for (int i = 0; i < wordSequences.size(); i++) {
            currentlongest = 0;
            for (int j = 0; j < wordSequences.elementAt(i).size(); j++) {
                String word = wordSequences.elementAt(i).elementAt(j).get_result_str();
                if ((word.length() < 6 && numberTimesWordAppearInLines(wordSequences.elementAt(i).elementAt(j)) < 2)
                        || !LFun.isContainMorethanTwoDigits(word)) {
                    currentlongest++;
                }
            }
            lengthSeq.add(currentlongest);

            if (currentlongest > longest) {

                longest = currentlongest;
                indexOfLongest = i;

            }
        }

        for (int i = 0; i < lengthSeq.size(); i++) {
            if (i != indexOfLongest && lengthSeq.get(i) == longest) {
                return -1;
            }
        }

        return indexOfLongest;
    }

    private boolean haveVrtLinesBetween(ObData word1, ObData word2, Vector<int[]> allVrtLines) {

        for (int i = 0; i < allVrtLines.size(); i++) {
            if (word1.get_left() + ((word1.get_right() - word1.get_left()) / 4) > allVrtLines.get(i)[0] && word2.get_right() - ((word2.get_right() - word2.get_left()) / 4) < allVrtLines.get(i)[2]) {
                return true;
            }
        }
        return false;
    }

    private Vector<Vector<ObData>> createWordsSequencesByVrtLines(Vector<ObData> line, Vector<int[]> allVrtLines) {
        Vector<Vector<ObData>> wordSequences = new Vector<Vector<ObData>>();

        if (line.size() == 1) {
            wordSequences.add(line);
            return wordSequences;
        }

        Vector<ObData> group = new Vector<ObData>();
        int currentIndex = 0;
        group.add(line.elementAt(0));
        wordSequences.add(group);

        for (int i = 1; i < line.size(); i++) {
            if (!haveVrtLinesBetween(line.get(i - 1), line.get(i), allVrtLines)) {
                wordSequences.get(currentIndex).add(line.elementAt(i));
            } else {
                group = new Vector<ObData>();
                group.add(line.elementAt(i));
                currentIndex++;
                wordSequences.add(group);
            }
        }

        return wordSequences;
    }

    private Vector<Vector<ObData>> createWordsSequences(Vector<ObData> line, double averageWidth) {
        Vector<Vector<ObData>> wordSequences = new Vector<Vector<ObData>>();

        if (line.size() == 1) {
            wordSequences.add(line);
            return wordSequences;
        }

        Vector<ObData> group = new Vector<ObData>();
        int currentIndex = 0;
        group.add(line.elementAt(0));
        wordSequences.add(group);

        for (int i = 1; i < line.size(); i++) {
            if (line.elementAt(i - 1).get_left() - line.elementAt(i).get_right() < averageWidth) {
                wordSequences.get(currentIndex).add(line.elementAt(i));
            } else {
                group = new Vector<ObData>();
                group.add(line.elementAt(i));
                currentIndex++;
                wordSequences.add(group);
            }
        }

        return wordSequences;
    }

    private double calculateAverageWidth(Vector<ObData> line) {
        double sumAverageCharacterInLine = 0;
        for (ObData word : line) {
            if (word.get_result_str().length() > 0) {
                sumAverageCharacterInLine += averageCharacterInOneWord(word);
            }
        }

        return sumAverageCharacterInLine / line.size();
    }

    private double averageCharacterInOneWord(ObData word) {
        double averageCharacterInOneWord = (word.get_right() - word.get_left()) / word.get_result_str().length();
        return averageCharacterInOneWord;
    }

    private Vector<Vector<ObData>> descriptionByLines(Vector<ObData> desc) {
        Vector<Vector<ObData>> DescriptionByLines = new Vector<Vector<ObData>>();
        int prevLine = -1;
        for (int i = desc.size() - 1; i >= 0; i--) {
            if (prevLine == -1) {
                prevLine = desc.elementAt(i).get_line_num();
                Vector<ObData> temp = new Vector<ObData>();
                temp.add(desc.elementAt(i));
                DescriptionByLines.add(temp);
            } else if (desc.elementAt(i).get_line_num() == prevLine) {
                DescriptionByLines.get(0).add(desc.elementAt(i));
            } else if (desc.elementAt(i).get_line_num() != prevLine) {
                prevLine = desc.elementAt(i).get_line_num();
                Vector<ObData> temp = new Vector<ObData>();
                temp.add(desc.elementAt(i));
                DescriptionByLines.add(0, temp);
            }
        }

        return DescriptionByLines;
    }

    private MatchesToItems checkVerifived(MatchesToItems bnvWithMatchesToItems) {
        for (MatchesToOneItem item : bnvWithMatchesToItems.MatchesToItems) {
            if ((!item.matches_is_fictitious && item.matches != null && !item.matches.isEmpty() && !item.foundPartialMatchToDES) || isDigitalFile && (item.block.descr != null && item.block.descr.size() > 0)) {
                for (ObData word : item.block.descr) {
                    word.set_isVerifived(true);
                }
            } else if (item.block.descr != null && item.block.descr.size() > 0) {
                for (ObData word : item.block.descr) {
                    word.set_isVerifived(false);
                }
            }

            if ((!item.matches_is_fictitious && item.matches != null && !item.matches.isEmpty() && !item.foundPartialMatchToPN) || isDigitalFile && (item.block.listPn != null && !item.block.listPn.isEmpty())) {
                for (ObData pn : item.block.listPn) {
                    pn.set_isVerifived(true);
                }
            } else if (item.block.listPn != null && !item.block.listPn.isEmpty()) {
                for (ObData pn : item.block.listPn) {
                    pn.set_isVerifived(false);
                }

            }
        }

        //עלויות הפריטים 
        for (MatchesToOneItem item : bnvWithMatchesToItems.MatchesToItems) {
            if (sumrowpricesv.getVerifived() || (item.block.one_price != null && item.block.qty != null) && item.block.price != null) {
                item.block.price.set_isVerifived(true);
                if (item.block.anaha != null && item.block.mam != null && item.block.fullprice != null) {
                    item.block.fullprice.set_isVerifived(true);
                } else if (item.block.fullprice != null) {
                    item.block.fullprice.set_isVerifived(false);
                }
            } else if (item.block.price != null || item.block.fullprice != null) {
                if (item.block.price != null) {
                    item.block.price.set_isVerifived(false);
                } else if (item.block.fullprice != null) {
                    item.block.fullprice.set_isVerifived(false);
                }
            }
        }

        return bnvWithMatchesToItems;

    }

    private void checkIfAlreadyBeenArchived() {
        if (infoReader != null) {
            if (!infoReader.InvoiceDate14.equals("@") && !infoReader.Invoice_ID12.equals("@")) {
                String InvoiceId = infoReader.Invoice_ID12;
                if (InvoiceId.startsWith("0")) {
                    InvoiceId = InvoiceId.substring(1);
                }
                for (int i = 0; i < orderedItemsCSV.size(); i++) {
                    if (InvoiceId.equals(orderedItemsCSV.elementAt(i)[92]) && infoReader != null && infoReader.InvoiceDate14.equals(orderedItemsCSV.elementAt(i)[93]) && !isProformaInvoice) {
                        beenArchived = true;
                        break;
                    } else if (InvoiceId.equals(orderedItemsCSV.elementAt(i)[79]) && infoReader != null && infoReader.InvoiceDate14.equals(orderedItemsCSV.elementAt(i)[80]) && isProformaInvoice) {
                        beenArchived = true;
                        break;
                    } else if (InvoiceId.equals(orderedItemsCSV.elementAt(i)[52]) && infoReader != null && infoReader.InvoiceDate14.equals(orderedItemsCSV.elementAt(i)[54]) && isMISHFile) {
                        beenArchived = true;
                        break;
                    } else if (InvoiceId.equals(orderedItemsCSV.elementAt(i)[7]) && infoReader != null && infoReader.InvoiceDate14.equals(orderedItemsCSV.elementAt(i)[9]) && isPORDFile) {
                        beenArchived = true;
                        break;
                    }
                }
            }
        }

    }

    private int getNumberOfPnWords() {
        int counter = 0;
        if (ClientPNWordsVector.get(0) != null) {
            counter++;
        }
        if (ManufacturerPNWordsVector.get(0) != null) {
            counter++;
        }
        if (SupplierPNWordsVector.get(0) != null) {
            counter++;
        }

        return counter;
    }

    private MatchesToItems correctPns(MatchesToItems bnvWithMatchesToItems) {
        boolean foundInMatch = false;
        int counter = getNumberOfPnWords();

        for (int j = 0; j < bnvWithMatchesToItems.MatchesToItems.size(); j++) {
            //אם לבלוק היה התאמות אז נבדוק אילו מקטים מאלו שזיהינו נמצאים ברשומות
            if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).matches != null && !bnvWithMatchesToItems.MatchesToItems.elementAt(j).matches.isEmpty()) {
                for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size() && !bnvWithMatchesToItems.MatchesToItems.elementAt(j).foundPartialMatchToPN; i++) {
                    foundInMatch = false;
                    for (Match match : bnvWithMatchesToItems.MatchesToItems.elementAt(j).matches) {
                        for (int index : INDEX_OF_PN) {
                            if (LFun.checkChanges(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str(), match.match[index])) {
                                foundInMatch = true;
                                break;
                            }
                        }
                        if (foundInMatch) {
                            break;
                        }
                    }
                    if (!foundInMatch) {
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).set_name_by_inv("num");
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                        i--;
                    }
                }

                if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size() > 1) {
                    for (int k = 0; k < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); k++) {
                        for (int k2 = 1; k2 < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); k2++) {
                            if (k != k2 && bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k).get_result_str().equals(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2).get_result_str())) {
                                if (doc_direct.equals("left")) {
                                    if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k).get_right() < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2).get_right()) {
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k).set_name_by_inv("num");
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k));
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k));
                                        k--;
                                        break;
                                    } else {
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2).set_name_by_inv("num");
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2));
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2));
                                        k2--;
                                    }
                                } else if (doc_direct.equals("right")) {
                                    if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k).get_left() > bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2).get_left()) {
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k).set_name_by_inv("num");
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k));
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k));
                                        k--;
                                        break;
                                    } else {
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2).set_name_by_inv("num");
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2));
                                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(k2));
                                        k2--;
                                    }
                                }
                            }
                        }

                    }
                }

            } // אם מצאנו בכותרת לפחות מילה אחת המתאימה למקט
            else if (counter >= 1) {
                int counterClientPNWordsVector = 0;
                int counterManufacturerPNWordsVector = 0;
                int counterSupplierPNWordsVector = 0;
                for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                    boolean isFound = false;
                    if (ClientPNWordsVector.get(0) != null) {
                        for (ObData pnFronVector : ClientPNWordsVector) {
                            if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str().equals(pnFronVector.get_result_str())) {
                                isFound = true;
                                counterClientPNWordsVector++;
                                break;
                            }
                        }
                    }
                    if (ManufacturerPNWordsVector.get(0) != null && !isFound) {
                        for (ObData pnFronVector : ManufacturerPNWordsVector) {
                            if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str().equals(pnFronVector.get_result_str())) {
                                isFound = true;
                                counterManufacturerPNWordsVector++;
                                break;
                            }
                        }
                    }
                    if (SupplierPNWordsVector.get(0) != null && !isFound) {
                        for (ObData pnFronVector : SupplierPNWordsVector) {
                            if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str().equals(pnFronVector.get_result_str())) {
                                isFound = true;
                                counterSupplierPNWordsVector++;
                                break;
                            }
                        }
                    }

                    if (!isFound) {
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).set_name_by_inv("num");
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                        i--;
                    }
                }
                // אם שני מקטים שייכים לאותו טור
                if (counterClientPNWordsVector > 1 || counterManufacturerPNWordsVector > 1 || counterSupplierPNWordsVector > 1) {
                    if (counterClientPNWordsVector > 1) {
                        Vector<ObData> v = new Vector<ObData>();
                        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                            for (ObData pnFronVector : ClientPNWordsVector) {
                                if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str().equals(pnFronVector.get_result_str())) {
                                    v.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                                }
                            }
                        }
                        if (v.size() > 1) {
                            double middle = (ClientPNWordsVector.get(0).get_right() - ClientPNWordsVector.get(0).get_left()) / 2 + ClientPNWordsVector.get(0).get_left();
                            double closeMid = (v.elementAt(0).get_right() - v.elementAt(0).get_left()) / 2 + v.elementAt(0).get_left();
                            int index = 0;
                            for (int i = 1; i < v.size(); i++) {
                                double nextMid = (v.elementAt(i).get_right() - v.elementAt(i).get_left()) / 2 + v.elementAt(i).get_left();
                                if (Math.abs(middle - nextMid) < Math.abs(middle - closeMid)) {
                                    index = 1;
                                    closeMid = nextMid;
                                }
                            }
                            for (int i = 0; i < v.size(); i++) {
                                if (index != i) {
                                    v.elementAt(i).set_name_by_inv("num");
                                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(v.elementAt(i));
                                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(v.elementAt(i));
                                }
                            }
                        }

                    }

                    if (counterManufacturerPNWordsVector > 1) {
                        Vector<ObData> v = new Vector<ObData>();
                        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                            for (ObData pnFronVector : ManufacturerPNWordsVector) {
                                if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str().equals(pnFronVector.get_result_str())) {
                                    v.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                                }
                            }
                        }
                        if (v.size() > 1) {
                            double middle = (ManufacturerPNWordsVector.get(0).get_right() - ManufacturerPNWordsVector.get(0).get_left()) / 2 + ManufacturerPNWordsVector.get(0).get_left();
                            double closeMid = (v.elementAt(0).get_right() - v.elementAt(0).get_left()) / 2 + v.elementAt(0).get_left();
                            int index = 0;
                            for (int i = 1; i < v.size(); i++) {
                                double nextMid = (v.elementAt(i).get_right() - v.elementAt(i).get_left()) / 2 + v.elementAt(i).get_left();
                                if (Math.abs(middle - nextMid) < Math.abs(middle - closeMid)) {
                                    index = 1;
                                    closeMid = nextMid;
                                }
                            }
                            for (int i = 0; i < v.size(); i++) {
                                if (index != i) {
                                    v.elementAt(i).set_name_by_inv("num");
                                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(v.elementAt(i));
                                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(v.elementAt(i));
                                }
                            }
                        }
                    }

                    if (counterSupplierPNWordsVector > 1) {
                        Vector<ObData> v = new Vector<ObData>();
                        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                            for (ObData pnFronVector : SupplierPNWordsVector) {
                                if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_result_str().equals(pnFronVector.get_result_str())) {
                                    v.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                                }
                            }
                        }
                        if (v.size() > 1) {
                            double middle = (SupplierPNWordsVector.get(0).get_right() - SupplierPNWordsVector.get(0).get_left()) / 2 + SupplierPNWordsVector.get(0).get_left();
                            double closeMid = (v.elementAt(0).get_right() - v.elementAt(0).get_left()) / 2 + v.elementAt(0).get_left();
                            int index = 0;
                            for (int i = 1; i < v.size(); i++) {
                                double nextMid = (v.elementAt(i).get_right() - v.elementAt(i).get_left()) / 2 + v.elementAt(i).get_left();
                                if (Math.abs(middle - nextMid) < Math.abs(middle - closeMid)) {
                                    index = 1;
                                    closeMid = nextMid;
                                }
                            }
                            for (int i = 0; i < v.size(); i++) {
                                if (index != i) {
                                    v.elementAt(i).set_name_by_inv("num");
                                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(v.elementAt(i));
                                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(v.elementAt(i));
                                }
                            }
                        }
                    }
                }
            } // אם לא מצאנו בכותרת מילה כלשהי המתאימה למקט
            else if (counter == 0 && bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size() > 1) {
                int min = Integer.MAX_VALUE;
                Vector<Integer> vMin = new Vector<Integer>();
                for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                    //בדיקה כמה פעמים מופיע כל מקט
                    int currentMin = numberTimesWordAppearInLines(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                    vMin.add(currentMin);
                    if (min > currentMin) {
                        min = currentMin;
                    }
                }
                for (int i = 0; i < vMin.size(); i++) {
                    // נמחק את אלו שמופיעים יותר מהמינימום
                    if (vMin.elementAt(i) > min) {
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).set_name_by_inv("num");
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                        bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.remove(i);
                        vMin.remove(i);
                        i--;
                    }
                }
                if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size() > 3 && doc_direct.equals("left")) {
                    Vector<ObData> mostRight = new Vector<ObData>();
                    for (int i = 0; i < 3; i++) {
                        mostRight.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                    }
                    for (int i = 3; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                        for (int k = 0; k < mostRight.size(); k++) {
                            if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_right() > mostRight.elementAt(k).get_right()) {
                                mostRight.elementAt(k).set_name_by_inv("num");
                                bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(mostRight.elementAt(k));
                                mostRight.set(k, bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                            }
                        }
                    }
                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn = mostRight;
                } else if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size() > 3 && doc_direct.equals("right")) {
                    Vector<ObData> mostleft = new Vector<ObData>();
                    for (int i = 0; i < 3; i++) {
                        mostleft.add(bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));
                    }
                    for (int i = 3; i < bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.size(); i++) {
                        for (int k = 0; k < mostleft.size(); k++) {
                            if (bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i).get_left() < mostleft.elementAt(k).get_left()) {
                                mostleft.elementAt(k).set_name_by_inv("num");
                                bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.un_num.add(mostleft.elementAt(k));
                                mostleft.set(k, bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn.elementAt(i));

                            }
                        }
                    }
                    bnvWithMatchesToItems.MatchesToItems.elementAt(j).block.listPn = mostleft;
                }
            }
        }

        return bnvWithMatchesToItems;
    }

    private int numberTimesWordAppearInLines(ObData word) {
        int counter = 0;
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_result_str().equals(word.get_result_str())) {
                    counter++;
                }
            }
        }

        return counter;
    }

    private void readOrderedItemsAndCheckIfAlreadtBeenArchived() {

        //read orderedItemsCSV
        //readOrderedItemsCSV();
        //check if is already archived
        checkIfAlreadyBeenArchived();
    }

    public static void readOrderedItemsCSV() {
        if (orderedItemsCSV.isEmpty()) {
            //read orderedItemsCSV
            File file = new File(Path.get_ordered_Items_csv());
            Vector<String> v;
            if (file.exists()) {
                v = LFun.file2vector3(Path.get_ordered_Items_csv());

                // orderedItemsCSV
                for (int i = 1; i < v.size(); i++) {
                    String[] arr = v.elementAt(i).split("\\,", -1);
                    orderedItemsCSV.add(arr);
                    try {
                        orderedItemsPrices.add(Double.valueOf(arr[24]));
                        try {
                            double sum = 0;
                            if (!arr[30].isEmpty()) {
                                sum = Double.valueOf(arr[30]) + Double.valueOf(arr[31]);
                            } else {
                                sum = Double.valueOf(arr[31]);
                            }
                            if (Math.abs(sum) > 1) {
                                orderedItemsSumPrices.add(sum);
                            }
                        } catch (Exception e) {
                        }
                        try {
                            double sum = Double.valueOf(arr[33]);
                            if (Math.abs(sum) > 1) {
                                orderedItemsSumPrices.add(sum);
                            }
                        } catch (Exception e) {
                        }

                    } catch (Exception e) {
                    }

                }
            } else {
                write("", "", Path.get_ordered_Items_csv() + " file is not exists", "", is_log);
                //write(Path.getcsv_final(), this.getClass().toString(), Path.get_ordered_Items_csv() + " file is not exists", "", true);
            }
            GeneralMatch.InvoicesByGroups.getInstance(orderedItemsCSV);
        }
    }

    public static String SuppliersId2ErpId(String id) {
        try {
            readSuppliersCSV();
            for (String[] sup : SuppliersCSV) {
                if (id.equalsIgnoreCase(sup[1])) {
                    return sup[0];
                }
            }
        } catch (Exception e) {
        }
        return id;
    }

    public static void readSuppliersCSV() {
        if (SuppliersCSV.isEmpty()) {
            //read SuppliersCSV
            File file = new File(config.getSuppliersPath());//Path.getcsv_final() + "\\SUPPLIERS.CSV");
            Vector<String> v;

            if (file.exists()) {
                v = LFun.file2vector3(config.getSuppliersPath());

                // orderedItemsCSV
                for (int i = 1; i < v.size(); i++) {
                    String[] arr = v.elementAt(i).split("\\,", -1);
                    SuppliersCSV.add(arr);
                    /*if (!arr[0].isEmpty() && arr[0].equalsIgnoreCase(infoReader.SupplierID)) {
                        try {
                            infoReader.SupplierName = arr[3].replaceAll("\"", "").replaceAll("״", "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }*/
                }
                write(Path.getcsv_final(), "", Path.getcsv_final() + "\\SUPPLIERS.csv" + " file is exists", "", is_log);
            } else {
                write(Path.getcsv_final(), "", Path.getcsv_final() + "\\SUPPLIERS.csv" + " file is not exists", "", is_log);

            }
        }
    }

    /*private Vector<MatchesToItem> createMatchesToItems(Vector<BNode> bnv, boolean beenArchived){
     Vector<MatchesToItem> MatchesToItems = new Vector<MatchesToItem>();
        
     for(BNode block : bnv){
     MatchesToOneItem item = new MatchesToOneItem(block, makatcsu, itemdesccsu, unitpricecsu, notcatnocsu, orderedItemsCSV, tolerance, INDEX_OF_PRICE, INDEX_OF_DESCRIPTION, INDEX_OF_PN, infoReader, beenArchived, globalanahav, sumallNoMamv, bnv.size(), igulv, isCreditInvoice, isProformaInvoice);
     MatchesToItems.add(item);
     }
        
     //אם החשבונית לא זוהתה מראש כחשבונית זיכוי אז נבדוק:
     // אם לגבי כל הפריטים שבחשבונית קיימות רשומות זיכוי מתאימות  לחלוטין,
     // וגם  לא נמצאה התאמה לגבי כל הפריטים עם רשומות רגילות
     // רק אז נחליט שכנראה זו חשבונית זיכוי.
     if(!isCreditInvoice){
     for(int i = 0; i < MatchesToItems.size(); i++){
     if(!MatchesToItems.elementAt(i).foundFullyCreditMatch){
     break;
     }else if(MatchesToItems.elementAt(i).foundFullyCreditMatch && i == MatchesToItems.size() - 1){
     isCreditInvoice = true;
     for(int j = 0; j < MatchesToItems.size(); j++){
     MatchesToItems.elementAt(j).isCreditInvoice = true;
     }
     }
     }
     }
        
     for(int i = 0; i < MatchesToItems.size(); i++){
     MatchesToItems.elementAt(i).searchWhichMatchToItem();
     }
        
     // התאמה כללית לחשבונית
     if(!checkIfThereIsMatches(MatchesToItems) && !beenArchived){
     matchInCsvToInvoiceA4 = matchToNotCatCsu();
            
     }
     return MatchesToItems;
     }*/
    private boolean checkIfThereIsMatches(Vector<MatchesToOneItem> MatchesToItems) {
        boolean haveMatches = false;
        for (int i = 0; i < MatchesToItems.size(); i++) {
            if (MatchesToItems.elementAt(i).matches.size() > 0) {
                haveMatches = true;
                break;
            }
        }

        return haveMatches;
    }

    private void checkSumPatur(Vector<BNode> bnv) {

        try {
            double sumallPaturFromMam = 0;
            // חישוב סכום הפריטים שעליהם המעמ הוא מאפס
            for (BNode block : bnv) {
                if (block.percent != null && block.mam != null) {
                    if (block.percent.get_result_str().equals("0.0%")) {
                        sumallPaturFromMam += Double.valueOf(block.price.get_result_str());
                    }
                }
            }

            sumallpaturv.setSum(sumallPaturFromMam);
            sumallhayavv.setSum(sumallNoMamv.getSum() - sumallPaturFromMam);

            if (sumallMamv.getVerifived() && sumallWithMamv.getVerifived() && sumallNoMamv.getVerifived()) {
                sumallpaturv.setVerifived(true);
                sumallhayavv.setVerifived(true);
            }
        } catch (Exception e) {
        }
    }

    private Vector<BNode> fixFullPrice(Vector<BNode> bnv) {

        for (BNode block : bnv) {
            if (block.anaha != null && block.price != null && block.mam != null) {
                try {
                    Double anaha = Double.valueOf(block.anaha);
                    Double priceWithAnaha = Double.valueOf(block.price.get_result_str());

                    Double priceWithAnahaNoMam = priceWithAnaha;
                    Double mam = Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(block.mam.get_result_str())));
                    //Double priceWithAnahaWithMam = Double.parseDouble(new DecimalFormat("##.##").format(priceWithoutAnahaNoMam + mam+anaha));
                    Double priceWithAnahaWithMam = Double.parseDouble(new DecimalFormat("##.##").format(priceWithAnahaNoMam + mam + anaha));
                    String priceWithAnahaStr = priceWithAnahaWithMam.toString();
                    block.fullprice = new ObData();
                    block.fullprice.set_name_by_inv("priceWithAnahaWithMam");
                    block.fullprice.set_result_str(priceWithAnahaStr);

                    /*
                    Double priceWithoutAnahaNoMam = priceWithAnaha - anaha;
                    Double priceWithoutAnahaWithMam = Double.parseDouble(new DecimalFormat("##.##").format(priceWithoutAnahaNoMam + mam));
                    String priceWithoutAnahaStr = priceWithoutAnahaWithMam.toString();
                    block.fullprice = new ObData();
                    block.fullprice.set_name_by_inv("priceWithoutAnahaWithMam");
                    block.fullprice.set_result_str(priceWithoutAnahaStr);
                     */
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        }

        return bnv;
    }

    private void fixFullPrice(MatchesToItems bnvWithMatchesToItems) {
        /*infoReader.totalInvoiceSum13 = 0;
        infoReader.totalTaxes10 = 0;
        infoReader.totalTaxes_Hayav_Bemaam9 = 0;
        infoReader.totalPriceNoTaxes8 = 0;*/

        if (sumallNoMamv.getSum() == 0 && sumallhayavv.getSum() == 0 && sumallWithMamv.getSum() != 0) {
            sumallhayavv.setSum(sumallWithMamv.getSum());
            sumallWithMamv.setSum(sumallWithMamv.getSum() + sumallMamv.getSum() + sumallpaturv.getSum());
        }

        /*sumallWithMamv = new VerVar(0)
        , // סכום עם מעמ
            sumallMamv = new VerVar(0)
        , // סכום המעמ
            sumallNoMamv = new VerVar(0)
        , // סכום בלי מעמ
            sumallpaturv = new VerVar(0)
        ,// סכום פטור ממעמ*/
        //sumallWithMamv.setSum(sumallMamv.getSum() + sumallpaturv.getSum() + sumallhayavv.getSum());
        try {
            for (MatchesToOneItem b : bnvWithMatchesToItems.MatchesToItems) {
                if (b.matches != null && b.matches.size() > 0) {
                    try {
                        if (b.block.mam == null) {
                            b.block.mam = new ObData();
                            b.block.mam.set_name_by_inv("mam");
                            b.block.mam.set_result_str("0");
                            b.block.mam.set_name_by_inv(bnvWithMatchesToItems.MatchesToItems.get(0).matches.get(0).match[36]);
                        }

                        if (b.block.fullprice == null && b.block.price != null) {
                            Double anaha = Double.valueOf(b.block.anaha);
                            Double priceWithAnaha = Double.valueOf(b.block.price.get_result_str());
                            Double priceWithoutAnahaNoMam = priceWithAnaha - anaha;
                            Double mam = Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(b.block.mam.get_result_str()) * 0.01));
                            Double priceWithoutAnahaWithMam = Double.parseDouble(new DecimalFormat("##.##").format(priceWithoutAnahaNoMam + mam));
                            String priceWithoutAnahaStr = priceWithoutAnahaWithMam.toString();
                            b.block.fullprice = new ObData();
                            b.block.fullprice.set_name_by_inv("priceWithoutAnahaWithMam");
                            b.block.fullprice.set_result_str(priceWithoutAnahaStr);
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                }
            }
        } catch (Exception e) {
        }
    }

    /*private Vector<BNode> matchItemToCSv(Vector<BNode> bnv) {
     //Vector<String[]> matches = new Vector<String[]>();
     boolean haveMatch = false;
     for (BNode block : bnv) {
     Vector<String[]> matches = new Vector<String[]>();
     matches = matchToMakatCsu(block);
     matches.addAll(matchToDescriptionCsu(block));
     matches.addAll(matchToUnitPriceCsu(block));
     matches = deleteDuplicate(matches);
     matches = sortVectorByString(matches);
     if(matches.size() >= 1){
     //String[] match = searchForOneMatch(matches, block);
     //if(match != null){
     block.set_o_match_in_csv(matches);
     haveMatch = true;
     //}
     }
     /*else if(matches.size() == 1){
     block.set_o_match_in_csv(matches.elementAt(0));
     haveMatch = true;
     }
     }
        
     if(!haveMatch){
     matchInCsvToInvoiceA4 = matchToNotCatCsu();
     }

     return bnv;
     }*/
    private Vector<String[]> sortVectorByString(Vector<String[]> toSort) {

        int j;
        boolean flag = true;   // set flag to true to begin first pass
        String[] temp;   //holding variable

        while (flag) {
            flag = false;    //set flag to false awaiting a possible swap
            for (j = 0; j < toSort.size() - 1; j++) {
                if ((toSort.elementAt(j)[11]).compareToIgnoreCase(toSort.elementAt(j + 1)[11]) > 0) // change to > for ascending sort
                {
                    temp = toSort.elementAt(j);    //swap elements
                    toSort.set(j, toSort.get(j + 1));
                    toSort.set(j + 1, temp);
                    flag = true;              //shows a swap occurred  
                }
            }
        }

        return toSort;
    }

    private String[] matchToNotCatCsu() {
        String[] matchToNotCat = null;
        Vector<String[]> notcatnocsuWithoutDuplicate = deleteDuplicate();

        if (infoReader != null && infoReader.Invoice_ID12 != null && !infoReader.Invoice_ID12.equals("@")) {
            for (String[] v : notcatnocsuWithoutDuplicate) {
                if (v[79].length() > 2 && isMatchInvoiceNumber(v[79])) {
                    matchToNotCat = v;
                }
            }
            /*if(matchToNotCat == null){
             Vector<String[]> matches = new Vector<String[]>();
             for(String[] v : notcatnocsuWithoutDuplicate){
             if(v[91].equals("")){
             matches.add(v);
             }
             }
             if(matches.size() == 1){
             matchToNotCat = matches.get(0);
             } 
             }
             if(matchToNotCat == null){
             //צריך להוסיף השוואה עם המונה דפים וקילומטרים בעמודה 74
             }*/
        }
        if (matchToNotCat == null) {
            Vector<String[]> matches = new Vector<String[]>();
            for (String[] v : notcatnocsuWithoutDuplicate) {
                if (v[103].equals("")) {
                    matches.add(v);
                }
            }
            if (matches.size() == 1) {
                matchToNotCat = matches.get(0);
            }
        }
        if (matchToNotCat == null) {
            //צריך להוסיף השוואה עם המונה דפים וקילומטרים בעמודה לאחר שימומש בקוד 74
        }

        return matchToNotCat;
    }

    private Vector<String[]> deleteDuplicate() {
        //Vector<String[]> notcatnocsuWithoutDuplicate = notcatnocsu;
        Vector<String[]> notcatnocsuWithoutDuplicate = new Vector<String[]>(notcatnocsu);
        for (int i = 0; i < notcatnocsuWithoutDuplicate.size(); i++) {
            for (int j = 0; j < notcatnocsuWithoutDuplicate.size(); j++) {
                if (i != j) {
                    if (isSame(notcatnocsuWithoutDuplicate.get(i), notcatnocsuWithoutDuplicate.get(j))) {
                        notcatnocsuWithoutDuplicate.remove(j);
                        j--;
                    }
                }
            }
        }

        return notcatnocsuWithoutDuplicate;
    }

    private boolean isSame(String[] v1, String[] v2) {
        boolean isSame = true;
        if (v1.length == v2.length) {
            for (int i = 11; i < v1.length && isSame; i++) {
                if (!v1[i].equals(v2[i])) {
                    isSame = false;
                }
            }
        }

        return isSame;
    }

    /**
     * If the number in the csv is shorter then the number we found - we will do
     * match just with the right chars
     *
     * @param numberInCsv
     * @return true if match
     */
    private boolean isMatchInvoiceNumber(String numberInCsv) {
        boolean isMatch = false;
        if (infoReader != null && infoReader.Invoice_ID12.length() == numberInCsv.length()) {
            isMatch = infoReader.Invoice_ID12.equals(numberInCsv);
        } else if (infoReader != null && infoReader.Invoice_ID12.length() > numberInCsv.length()) {
            boolean toContinue = true;
            for (int i = numberInCsv.length() - 1; i > 0 && toContinue; i++) {
                if (numberInCsv.charAt(i) != infoReader.Invoice_ID12.charAt(i)) {
                    toContinue = false;
                }
            }
            if (toContinue) {
                isMatch = true;
            }
        }

        return isMatch;
    }

    private String[] findEarlierMatch(Vector<String[]> matches) {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Date earlier = null;
        Date currentDate = null;
        int indexOfEarlier = 0;
        try {
            earlier = df.parse(matches.elementAt(0)[INDEX_OF_ITEM_DATE_IN_PO]);

        } catch (ParseException ex) {
            Logger.getLogger(CSV2Hash.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 1; i < matches.size(); i++) {
            try {
                currentDate = df.parse(matches.elementAt(i)[INDEX_OF_ITEM_DATE_IN_PO]);
                if (currentDate.before(earlier)) {
                    earlier = currentDate;
                    indexOfEarlier = i;

                }
            } catch (ParseException ex) {
                Logger.getLogger(CSV2Hash.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        return matches.elementAt(indexOfEarlier);
    }

    private boolean isSamePoOrder(Vector<String[]> matches) {
        boolean isSamePoOrder = true;
        String currentPoOreder = matches.elementAt(0)[INDEX_OF_PO_NUMBER];
        for (int i = 1; i < matches.size(); i++) {
            if (!currentPoOreder.equals(matches.elementAt(i)[INDEX_OF_PO_NUMBER])) {
                isSamePoOrder = false;
                break;
            }

        }

        return isSamePoOrder;
    }

    /*private Vector<String[]> matchToMakatCsu(BNode block) {

     Vector<String[]> matchToMakatCsu = new Vector<String[]>();
     boolean isSamePricePerOne = false;

     if (makatcsu != null) {
     for (String[] line : makatcsu) {
     //move over all pn that belong to the item
     for (ObData pn : block.listPn) {
     if (pn.get_result_str().equals(line[0])) {
     isSamePricePerOne = false;
     //compare price of item
     if (block.one_price != null) {
     isSamePricePerOne = checktoler(Double.valueOf(line[INDEX_OF_PRICE]), Double.valueOf(block.one_price.get_result_str()), tolerance);
     } else {
     isSamePricePerOne = checktoler(Double.valueOf(line[INDEX_OF_PRICE]), Double.valueOf(block.price.get_result_str()), tolerance);
     }
     if (isSamePricePerOne) {
     //compare description of item
     String[] desCsu;
     String[] blockDes = null;
     //if(!block.get_description().equals("")){
     blockDes = block.get_description().split(" ");
     //}
     //else if(!block.desCsu.equals(" ")){
     //blockDes = block.desCsu.split(" ");
     //}
     boolean isClose = false;
     desCsu = line[INDEX_OF_DESCRIPTION].split(" ");
     //Compare item description
     for (int j = 0; j < desCsu.length && !isClose; j++) {
     if (desCsu[j].length() >= 3) {
     isClose = isCloseWord(desCsu[j], blockDes);
     }
     }
     if (isClose) {
     matchToMakatCsu.add(line);
     }
     }
     }
     }
     }
     }
     return matchToMakatCsu;
     }*/
    private Vector<String[]> matchToDescriptionCsu(BNode block) {
        Vector<String[]> matchToDescriptionCsu = new Vector<String[]>();
        if (itemdesccsu != null) {
            for (String[] line : itemdesccsu) {
                String[] desCsu;
                String[] blockDes = null;
                //if(!block.get_description().equals("")){
                blockDes = block.get_description().split(" ");
                //}
                //else if(!block.desCsu.equals(" ")){
                //blockDes = block.desCsu.split(" ");
                //}
                boolean isClose = false;
                desCsu = line[0].split(" ");
                //Compare item description
                for (int j = 0; j < desCsu.length && !isClose; j++) {
                    if (desCsu[j].length() >= 3) {
                        isClose = isCloseWord(desCsu[j], blockDes);
                    }
                }
                if (isClose) {
                    int index = MatchesToOneItem.findFirstIndexNotEmpty(line, INDEX_OF_PRICE);
                    //compare price of item
                    boolean isSamePricePerOne = false;
                    if (block.one_price != null && !line[index].equals("")) {
                        isSamePricePerOne = checktoler(Double.valueOf(line[index]), Double.valueOf(block.one_price.get_result_str()), tolerance);
                    } else if (!line[index].equals("") && block.price != null) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Double.valueOf(block.price.get_result_str()), tolerance);
                        } catch (Exception e) {
                            e.printStackTrace();
                            isSamePricePerOne = false;
                        }
                    }
                    if (isSamePricePerOne) {
                        //compare pn's of item
                        boolean isSamePn = false;
                        for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
                            for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                                int indexOfPn = INDEX_OF_PN.get(j);
                                if (LFun.checkChanges(line[indexOfPn], block.listPn.elementAt(i).get_result_str())) {
                                    isSamePn = true;
                                    matchToDescriptionCsu.add(line);
                                }
                            }
                        }
                    }
                }
            }
        }
        return matchToDescriptionCsu;
    }

    /*private Vector<String[]> matchToUnitPriceCsu(BNode block) {
     Vector<String[]> matchToUnitPriceCsu = new Vector<String[]>();

     if (unitpricecsu != null) {
     for (String[] line : unitpricecsu) {
     //compare price of item
     boolean isSamePricePerOne = false;
     if (block.one_price != null) {
     isSamePricePerOne = checktoler(Double.valueOf(line[0]), Double.valueOf(block.one_price.get_result_str()), tolerance);
     } else {
     isSamePricePerOne = checktoler(Double.valueOf(line[0]), Double.valueOf(block.price.get_result_str()), tolerance);
     }
     if (isSamePricePerOne) {
     //compare pn's of item
     boolean isSamePn = false;
     for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
     for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
     if (LFun.checkChanges(line[INDEX_OF_PN.get(j)], block.listPn.elementAt(i).get_result_str())) {
     isSamePn = true;
     }
     }
     }
     if (isSamePn) {
     //compare description of item
     String[] desCsu;
     String[] blockDes = null;
     //if(!block.get_description().equals("")){
     blockDes = block.get_description().split(" ");
     //}
     //else if(!block.desCsu.equals(" ")){
     //blockDes = block.desCsu.split(" ");
     //}
     boolean isClose = false;
     desCsu = line[INDEX_OF_DESCRIPTION].split(" ");
     //Compare item description
     for (int j = 0; j < desCsu.length && !isClose; j++) {
     if (desCsu[j].length() >= 3) {
     isClose = isCloseWord(desCsu[j], blockDes);
     }
     }
     if (isClose) {
     matchToUnitPriceCsu.add(line);
     }
     }
     }
     }
     }

     return matchToUnitPriceCsu;
     }*/
    public void createVectorsByTitles(Vector<ObData> result) {

        if (result.isEmpty()) {
            return;
        }
        int start = result.get(0).get_line_num() - 2;
        if (start < 0) {
            start = 0;
        }
        int end = result.get(result.size() - 1).get_line_num() + 1;
        if (end >= lines.size()) {
            end = lines.size() - 1;
        }

        Vector<Vector<ObData>> othersSusToBePn = new Vector<Vector<ObData>>();
        boolean isAdded = false;
        boolean findMish = false;
        boolean findHazmana = false;

        /*Vector<ObData> HazmanaWordsVector = new Vector<ObData>(),
         MishWordsVector = new Vector<ObData>(),
         ClientPNWordsVector = new Vector<ObData>(),
         ManufacturerPNWordsVector = new Vector<ObData>(),
         SupplierPNWordsVector = new Vector<ObData>(),
         QuantityInPackVector = new Vector<ObData>(),
         QuantityOfPacksVector = new Vector<ObData>();*/
        int startToSearch = start - 5;
        if (startToSearch < 0) {
            startToSearch = 0;
        }

        int endToSearch = lines.size() / 2;

        try {
            endToSearch = Math.max(result.get(0).get_line_num(), endToSearch);
        } catch (Exception e) {
        }
        if (endToSearch < start) {
            endToSearch = start;
        }

        int[] startToSearchArryas = {result.get(0).get_line_num() - 2, startToSearch};

        for (int startToSearchArrya : startToSearchArryas) {
            if (QuantityWordsVector.isEmpty() || QuantityWordsVector.get(0) == null) {
                QuantityWordsVector.clear();
                QuantityWordsVector.add(searchKeyWord(config.getQuantityWords(), startToSearchArrya, endToSearch));
            }
            if (DescriptionWordsVector.isEmpty() || DescriptionWordsVector.get(0) == null) {
                DescriptionWordsVector.clear();
                DescriptionWordsVector.add(searchKeyWord(config.getDescriptionWords(), startToSearchArrya, endToSearch));
            }
            //ClientPNWordsVector.add(searchKeyWord_upsideDown(ClientPNWords, startToSearch, endToSearch));
            if (ClientPNWordsVector.isEmpty() || ClientPNWordsVector.get(0) == null) {
                ClientPNWordsVector.clear();
                ClientPNWordsVector.add(searchKeyWord(config.getClientPNWords(), startToSearchArrya, endToSearch));
            }
            if (ClientPNWordsVector.isEmpty() || ClientPNWordsVector.get(0) == null) {
                ClientPNWordsVector.clear();
                ClientPNWordsVector.add(searchKeyWord2(config.getClientPNWords_TowWords(), startToSearchArrya, endToSearch));
            }

            try {
                int titleline_1 = QuantityWordsVector.get(0).get_line_num();
                int titleline_2 = DescriptionWordsVector.get(0).get_line_num();
                int titleline_3 = ClientPNWordsVector.get(0).get_line_num();
                if (titleline_1 == titleline_2 && titleline_1 == titleline_3) {
                    endToSearch = titleline_1;
                    startToSearch = titleline_1 - 1;
                    break;
                }
            } catch (Exception e) {
            }
        }

        UnitPriceWordsVector.add(searchKeyWord(config.getUnitPriceWords(), startToSearch, endToSearch));
        ExchangeRateWordsVector.add(searchKeyWord(config.getExchangeRateWords(), startToSearch, endToSearch));
        HazmanaWordsVector.add(searchKeyWord(config.getHazmanaWords(), startToSearch, endToSearch));
        MishWordsVector.add(searchKeyWord(config.getMishWords(), startToSearch, endToSearch));
        ManufacturerPNWordsVector.add(searchKeyWord(config.getManufacturerPNWords(), startToSearch, endToSearch));
        SupplierPNWordsVector.add(searchKeyWord(config.getSupplierPNWords(), startToSearch, endToSearch));
        QuantityInPackVector.add(searchKeyWord2(config.getQuantityInPack(), startToSearch, endToSearch));
        QuantityOfPacksVector.add(searchKeyWord2(config.getQuantityOfPacks(), startToSearch, endToSearch));

        //if (QuantityWordsVector.get(0) != null && QuantityInPackVector.get(0) != null) {
        if (QuantityWordsVector.get(0) != null && QuantityInPackVector.size() > 0 && QuantityInPackVector.get(0) != null) {
            if (QuantityWordsVector.get(0).get_right() == QuantityInPackVector.get(0).get_right() && QuantityWordsVector.get(0).get_left() == QuantityInPackVector.get(0).get_left()
                    && QuantityWordsVector.get(0).get_result_str().equals(QuantityInPackVector.get(0).get_result_str())) {
                QuantityWordsVector.removeAllElements();
                QuantityWordsVector.add(null);
            }
        }

        if (QuantityWordsVector.get(0) == null) {
            QuantityTwoWordsVector.add(searchKeyWord2(config.getQuantityWordsTwoWords(), startToSearch, endToSearch));
        } else {
            QuantityTwoWordsVector.add(null);
        }

        if (UnitPriceWordsVector.get(0) == null) {
            UnitPriceTwoWordsVector.add(searchKeyWord2(config.getUnitPriceTwoWords(), startToSearch, endToSearch));
        } else {
            UnitPriceTwoWordsVector.add(null);
        }

        for (int i = start; i <= end; i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (LFun.iscontain_num(lines.elementAt(i).elementAt(j).get_result_str()) && !lines.elementAt(i).elementAt(j).get_name_by_inv().contains("price")
                        && !lines.elementAt(i).elementAt(j).get_name_by_inv().contains("date") && lines.elementAt(i).elementAt(j).get_result_str().length() > 1) {
                    if (QuantityWordsVector.get(0) != null
                            && (LFun.checkDeviation(QuantityWordsVector.get(QuantityWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(QuantityWordsVector.get(QuantityWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))
                            && !lines.elementAt(i).elementAt(j).get_format().equalsIgnoreCase("USADD")) {
                        QuantityWordsVector.add(lines.elementAt(i).elementAt(j));
                        //lines.elementAt(i).elementAt(j).set_name_by_inv("qty");
                        //lines.elementAt(i).elementAt(j).set_name_by_inv("");

                    } else if (QuantityTwoWordsVector.get(0) != null
                            && (LFun.checkDeviation(QuantityTwoWordsVector.get(QuantityTwoWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION)
                            || checkmidDeviatuin(QuantityTwoWordsVector.get(QuantityTwoWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))) {

                        QuantityTwoWordsVector.add(lines.elementAt(i).elementAt(j));
                        //lines.elementAt(i).elementAt(j).set_name_by_inv("qty");
                        //lines.elementAt(i).elementAt(j).set_name_by_inv("");
                    } else if (HazmanaWordsVector.get(0) != null && (LFun.checkDeviation(HazmanaWordsVector.get(HazmanaWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION)
                            || checkmidDeviatuin(HazmanaWordsVector.get(HazmanaWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))) {
                        HazmanaWordsVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("hazmana");
                        findHazmana = true;
                    } else if (MishWordsVector.get(0) != null && (LFun.checkDeviation(MishWordsVector.get(MishWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION)
                            || checkmidDeviatuin(MishWordsVector.get(MishWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))) {
                        MishWordsVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("mish");
                        findMish = true;
                    } else if (ClientPNWordsVector.get(0) != null && ((LFun.checkDeviation(ClientPNWordsVector.get(ClientPNWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(ClientPNWordsVector.get(ClientPNWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION)) && !isInNotCatNoCsu(lines.elementAt(i).elementAt(j)))) {
                        ClientPNWordsVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("pn");
                    } else if (ManufacturerPNWordsVector.get(0) != null && ((LFun.checkDeviation(ManufacturerPNWordsVector.get(ManufacturerPNWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(ManufacturerPNWordsVector.get(ManufacturerPNWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION)) && !isInNotCatNoCsu(lines.elementAt(i).elementAt(j)))) {
                        ManufacturerPNWordsVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("pn");
                    } else if (SupplierPNWordsVector.get(0) != null && ((LFun.checkDeviation(SupplierPNWordsVector.get(SupplierPNWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(SupplierPNWordsVector.get(SupplierPNWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION)) && !isInNotCatNoCsu(lines.elementAt(i).elementAt(j)))) {
                        SupplierPNWordsVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("pn");
                    } else if (DescriptionWordsVector.get(0) != null
                            && (LFun.checkDeviation(DescriptionWordsVector.get(DescriptionWordsVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(DescriptionWordsVector.get(DescriptionWordsVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))) {
                        DescriptionWordsVector.add(lines.elementAt(i).elementAt(j));
                        //lines.elementAt(i).elementAt(j).set_name_by_inv("desc");
                    } else if (QuantityInPackVector.get(0) != null && (LFun.checkDeviation(QuantityInPackVector.get(QuantityInPackVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(QuantityInPackVector.get(QuantityInPackVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))) {
                        QuantityInPackVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("quantityInPack");
                    } else if (QuantityOfPacksVector.get(0) != null && (LFun.checkDeviation(QuantityOfPacksVector.get(QuantityOfPacksVector.size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION) || checkmidDeviatuin(QuantityOfPacksVector.get(QuantityOfPacksVector.size() - 1), lines.elementAt(i).elementAt(j), DEVIATION))) {
                        QuantityOfPacksVector.add(lines.elementAt(i).elementAt(j));
                        lines.elementAt(i).elementAt(j).set_name_by_inv("quantityOfPacks");
                    } else {
                        for (int k = 0; k < othersSusToBePn.size(); k++) {
                            if (LFun.checkDeviation(othersSusToBePn.get(k).get(othersSusToBePn.get(k).size() - 1).get_right(), lines.elementAt(i).elementAt(j).get_right(), DEVIATION)) {
                                isAdded = true;
                                othersSusToBePn.get(k).add(lines.elementAt(i).elementAt(j));
                            }
                        }
                        if (!isAdded) {
                            Vector<ObData> v = new Vector<ObData>();
                            v.add(lines.elementAt(i).elementAt(j));
                            othersSusToBePn.add(v);
                        } else {
                            isAdded = false;
                        }
                    }
                }
            }
        }

        boolean found = false;
        if (findHazmana && !findMish) {
            for (Vector<ObData> v : othersSusToBePn) {
                if (v.size() >= 4) {
                    found = isCloseNumbers(v);
                    if (found) {
                        findMish = true;
                        mark(v, "mish");
                        break;
                    }
                }

            }
        }
        found = false;
        if (!findHazmana && findMish) {
            for (Vector<ObData> v : othersSusToBePn) {
                if (v.size() >= 4) {
                    found = isCloseNumbers(v);
                    if (found) {
                        findHazmana = true;
                        mark(v, "hazmana");
                        break;
                    }
                }
            }
        }
        /*for (Vector<ObData> v : othersSusToBePn) {
         for (ObData ob : v) {
         if (!ob.get_name_by_inv().equals("num")) {
         ob.set_name_by_inv("");
         }
         }
         }*/
    }

    private int isInVector(Vector<ObData> vector, ObData ob) {
        int index = -1;
        for (int i = 0; i < vector.size(); i++) {
            if (ob.get_result_str().equals(vector.get(i).get_result_str()) && ob.get_right() == vector.get(i).get_right()) {
                return i;
            }
        }
        return index;
    }

    public void mark(Vector<ObData> v, String name_by_inv) {
        for (ObData ob : v) {
            ob.set_name_by_inv("");
        }
    }

    public boolean isInNotCatNoCsu(ObData num) {
        boolean isInHazmanaCsu = false;
        String numToCheck = num.get_result_str();
        for (String[] str : notcatnocsu) {
            if (str[0].equals(numToCheck)) {
                return true;
            }
        }
        return isInHazmanaCsu;
    }

    public boolean isCloseNumbers(Vector<ObData> toCheck) {
        boolean isCloseNumbers = true;
        String prv = toCheck.get(1).get_result_str();

        for (int i = 2; i < toCheck.size(); i++) {
            if (prv.length() < 3 || toCheck.get(i).get_result_str().length() < 3 || prv.length() != toCheck.get(i).get_result_str().length()) {
                return false;
            }
            for (int j = 0; j < prv.length() - 3; j++) {
                if (prv.charAt(j) != toCheck.get(i).get_result_str().charAt(j)) {
                    return false;
                }
            }
            prv = toCheck.get(i).get_result_str();
        }

        return isCloseNumbers;
    }

    public ObData searchKeyWord(Vector<String> keyWords, int start, int end) {
        for (int i = start; i < end; i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                for (String kw : keyWords) {
                    if (lines.elementAt(i).elementAt(j).get_result_str().toLowerCase().contains(kw.toLowerCase())
                            && lines.elementAt(i).elementAt(j).get_result_str().length() - 1 <= kw.length()) {
                        return lines.elementAt(i).elementAt(j);
                    } else if (kw.contains(" ")
                            && j + 1 < lines.elementAt(i).size()
                            && Math.abs(lines.elementAt(i).elementAt(j).get_right() - lines.elementAt(i).elementAt(j).get_left()) < 100
                            && (lines.elementAt(i).elementAt(j).get_result_str() + " " + lines.elementAt(i).elementAt(j + 1).get_result_str()).toLowerCase().contains(kw.toLowerCase())) {
                        return lines.elementAt(i).elementAt(j);
                    }
                }
            }
        }

        return null;
    }

    public ObData searchKeyWord_upsideDown(Vector<String> keyWords, int start, int end) {

        ObData keyWord = null;
        for (int i = start; i < end; i++) {
            //for (int j = 0; j < lines.elementAt(i).size(); j++) {
            for (int j = lines.elementAt(i).size() - 1; j >= 0; j--) {
                for (String kw : keyWords) {
                    if (lines.elementAt(i).elementAt(j).get_result_str().toLowerCase().contains(kw.toLowerCase())
                            && lines.elementAt(i).elementAt(j).get_result_str().length() - 1 <= kw.length()) {
                        return lines.elementAt(i).elementAt(j);
                    }
                }
            }
        }

        return keyWord;
    }

    /**
     * חיפוש מילת מפתח שהיא מחרוזת של שתי מילים: ייתכן והמרוזת אחת אחרי השניה
     * בשורה או אחת מתחת לשנייה בסטייה של 50 פיקסלים.
     *
     * @param keyWords
     * @return right keyWord
     */
    public ObData searchKeyWord2(Vector<String> keyWords, int start, int end) {

        ObData keyWord = null;
        boolean found = false;
        String[] toSearch = new String[2];
        for (int i = start; i < end && !found; i++) {
            for (int j = 0; j < lines.elementAt(i).size() && !found; j++) {
                for (String kw : keyWords) {

                    /*try {
                        toSearch = kw.split(" ");
                        if (lines.elementAt(i).elementAt(j).get_result_str().toLowerCase().equals(toSearch[1].toLowerCase()) && j + 1 < lines.elementAt(i).size()) {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                    try {
                        toSearch = kw.split(" ");
                        if ((lines.elementAt(i).elementAt(j).get_result_str().toLowerCase().equals(toSearch[1].toLowerCase()) || lines.elementAt(i).elementAt(j).get_result_str().toLowerCase().contains(toSearch[1].toLowerCase()))
                                && j + 1 < lines.elementAt(i).size()) {
                            if (lines.elementAt(i).elementAt(j + 1).get_result_str().contains(toSearch[0])) {
                                found = true;
                                keyWord = lines.elementAt(i).elementAt(j + 1);

                                // `Number/` to Number 
                                if (toSearch[0].length() >= 5
                                        && lines.elementAt(i).elementAt(j + 1).get_result_str().length() > toSearch[0].length()
                                        && lines.elementAt(i).elementAt(j + 1).get_result_str().length() != toSearch[0].length()) {
                                    lines.elementAt(i).elementAt(j + 1).set_result_str(toSearch[0]);
                                }
                            }
                        }
                        if (lines.elementAt(i).elementAt(j).get_result_str().toLowerCase().equals(toSearch[0].toLowerCase()) && !found) {
                            for (int k = i + 1; k < i + 3 && k < lines.size(); k++) {
                                for (int k2 = 0; k2 < lines.elementAt(k).size(); k2++) {
                                    if (LFun.checkDeviation(lines.elementAt(i).elementAt(j).get_right(), lines.elementAt(k).elementAt(k2).get_right(), 50)) {
                                        if (lines.elementAt(k).elementAt(k2).get_result_str().toLowerCase().equals(toSearch[1].toLowerCase())) {
                                            found = true;
                                            keyWord = lines.elementAt(i).elementAt(j);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return keyWord;
    }

    /**
     * return price from block k
     *
     * @param k
     * @return
     */
    public double get_price_by_row(int k) {
        for (int i = 0; i < lines.elementAt(k).size(); i++) {
            try {
                if (lines.elementAt(k).elementAt(i).get_name_by_inv().equalsIgnoreCase("pos_price")) {
                    return Double.valueOf(LFun.fixdot(lines.elementAt(k).elementAt(i).get_result_str()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0.00;
    }

    /**
     * return price index from block k
     *
     * @param k
     * @return
     */
    public int get_ind_price_by_row(int k) {
        for (int i = 0; i < lines.elementAt(k).size(); i++) {
            if (lines.elementAt(k).elementAt(i).get_name_by_inv().equalsIgnoreCase("pos_price")) {
                return i;
            }

        }
        return 0;
    }

    /**
     * find rate for price in row
     *
     * @param result
     */
    public void fndOnePriceWithRate(Vector<ObData> result) {
        int j = 0;
        double price = 0;//Double.valueOf(result.elementAt(j).get_result_str());
        double rate = 1.00;
        double qtyforrate = 1.00;
        for (; j < result.size(); j++) {

            for (int i = 0; i < lines.size(); i++) {
                if (lines.elementAt(i).contains(result.elementAt(j))) {
                    for (int k = 0; k < lines.elementAt(i).size(); k++) {
                        if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("rate")) {
                            try {
                                rate = Double.valueOf(LFun.fixdot(lines.elementAt(i).elementAt(k).get_result_str()));
                            } catch (Exception r) {
                            }
                        } else if (ExchangeRatePrices.size() > 0
                                && (ExchangeRatePrices.size() == 1
                                || result.get(0).get_line_num() - 2 > ExchangeRatePrices.get(0).get_line_num())) {
                            try {
                                rate = Double.valueOf(ExchangeRatePrices.get(0).get_result_str());
                            } catch (Exception e) {
                            }
                        } else if (ExchangeRatePrices.size() > 0 && (result.get(result.size() - 1).get_line_num() + 2 < ExchangeRatePrices.get(ExchangeRatePrices.size() - 1).get_line_num())) {
                            try {
                                rate = Double.valueOf(ExchangeRatePrices.get(ExchangeRatePrices.size() - 1).get_result_str());
                            } catch (Exception e) {
                            }
                        }

                        if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("qty")) {
                            try {
                                qtyforrate = Double.valueOf(LFun.fixdot(lines.elementAt(i).elementAt(k).get_result_str()));

                            } catch (Exception r) {
                            }
                        }

                    }
                    try {
                        price = Double.valueOf(LFun.fixdot(result.elementAt(j).get_result_str())) / rate;

                    } catch (Exception r) {
                    }

                    if (rate != 1.00) {
                        double check_pr_rate = price / qtyforrate;
                        for (int k = 0; k < lines.elementAt(i).size(); k++) {
                            if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("num")) {
                                if (check_pr_rate > (Double.valueOf(LFun.fixdot(lines.elementAt(i).elementAt(k).get_result_str())) - 0.1) && check_pr_rate < (Double.valueOf(LFun.fixdot(lines.elementAt(i).elementAt(k).get_result_str())) + 0.1)) {
                                    lines.elementAt(i).elementAt(k).set_name_by_inv("one_price_rate");
                                }
                            }

                        }
                    }

                    price = 0;
                    rate = 1.00;
                    qtyforrate = 1.00;

                }

            }

        }

    }

    /**
     * search mam data in row by input percent vector
     */
    public void searchMamDataInRow() {
        for (int i = 0; i < lines.size(); i++) {
            double tprice = get_price_by_row(i);
            if (tprice != 0) {
                for (int k = 0; k < mam_percents.size(); k++) {
                    if (!mam_percents.elementAt(k).equalsIgnoreCase("0")) {
                        checkmam(i, tprice, Double.valueOf(mam_percents.elementAt(k)), 0.1);// 0.1 tollerance may be changed as parameter
                    }
                }
            }

        }

        System.out.println("Start print mam");

        for (int i = 0; i < lines.size(); i++) {
            System.out.println();
            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("date")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("mam")) {
                    System.out.print(" " + lines.elementAt(i).elementAt(k).get_result_str() + " " + lines.elementAt(i).elementAt(k).get_name_by_inv() + "\t\t|");

                }
            }

        }
        System.out.println("\nEnd print mam");
    }

    /**
     * search mam in block k by follows
     *
     * @param k number block
     * @param pr price
     * @param percent mam percent
     * @param tol tollerance
     */
    public void checkmam(int k, double pr, double percent, double tol) {

        double mam = (pr / 100) * percent;
        double sumpmam = pr + mam;

        for (int i = 0; i < lines.elementAt(k).size(); i++) {
            if (!lines.elementAt(k).elementAt(i).get_format().equalsIgnoreCase("INTPSEP")
                    && !lines.elementAt(k).elementAt(i).get_format().equalsIgnoreCase("INTP")
                    && lines.elementAt(k).elementAt(i).get_name_by_inv().equalsIgnoreCase("num")) {
                double tval = 0;
                try {
                    tval = Double.valueOf(LFun.fixdot(lines.elementAt(k).elementAt(i).get_result_str()));

                } catch (Exception r) {
                }

                if (Math.abs(tval - mam) <= tol) {

                    lines.elementAt(k).elementAt(i).set_name_by_inv("mam:" + percent + "%");
                } else if (Math.abs(tval - sumpmam) <= tol) {
                    lines.elementAt(k).elementAt(i).set_name_by_inv("with_mam");
                    lines.elementAt(k).elementAt(get_ind_price_by_row(k)).set_name_by_inv("pos_price_without_mam");
                }

            }
        }

        mam = (pr / (percent + 100)) * percent;
        double summmam = pr - mam;

        for (int i = 0; i < lines.elementAt(k).size(); i++) {
            if (!lines.elementAt(k).elementAt(i).get_format().equalsIgnoreCase("INTPSEP")
                    && !lines.elementAt(k).elementAt(i).get_format().equalsIgnoreCase("INTP")
                    && lines.elementAt(k).elementAt(i).get_name_by_inv().equalsIgnoreCase("num")) {
                double tval = 0;
                try {
                    tval = Double.valueOf(LFun.fixdot(lines.elementAt(k).elementAt(i).get_result_str()));

                } catch (Exception r) {
                }
                if (Math.abs(tval - mam) <= tol) {

                    lines.elementAt(k).elementAt(i).set_name_by_inv("mam:" + percent + "%");
                } else if (Math.abs(tval - summmam) <= tol) {
                    lines.elementAt(k).elementAt(i).set_name_by_inv("without_mam");
                    lines.elementAt(k).elementAt(get_ind_price_by_row(k)).set_name_by_inv("pos_price_with_mam");
                }
            }
        }
    }

    /**
     * check if has negative sum in zanav
     *
     * @return
     */
    public boolean checkNegativeInZanav(int linenum) {
        Vector<Double> d = finddoublenum_onlynegativ(linenum);
        //need search if vector contain negpricesv.getSum()
        for (int i = 0; i < d.size(); i++) {
            if (checktoler(d.elementAt(i), negpricesv.getSum(), tolerance)) {
                return true;
            }

        }
        return false;
    }

    /*
     * find positive parts contain index
     */
    public int findpositive(int ind, Vector<ObData> result) {
        int n = 0;
        double neg = 0;
        for (int i = 0; i <= ind; i++) {
            double t = Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
            if (t > 0) {
                n++;
            } else {
                neg = neg + t;
            }
        }
        negpricesv.setSum(neg);
        if (neg != 0) {
            negpricesv.setVerifived(checkNegativeInZanav(result.elementAt(ind).get_line_num() + 1));
        }

        return n;
    }

    /*
     * find duplicates + - parts
     */
    public int searchDuplicate(int ind, Vector<ObData> result) {
        double sumdup = 0;
        for (int i = ind + 1; i < result.size(); i++) {
            sumdup = sumdup + Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
            if (sumdup == 0.0) {
                ind = i;
            }
        }
        return ind;
    }

    public boolean sum2rowsbefore(int ind, Vector<ObData> result) {
        if (ind < 2) {
            return false;
        }

        try {
            double rowbefore = Math.abs(Double.parseDouble(LFun.fixdot(result.elementAt(ind - 1).get_result_str())));
            double rowbeforebefore = Math.abs(Double.parseDouble(LFun.fixdot(result.elementAt(ind - 2).get_result_str())));
            double currow = Math.abs(Double.parseDouble(LFun.fixdot(result.elementAt(ind).get_result_str())));
            double sumbefores = rowbefore + rowbeforebefore;
            if (currow + 0.5 > sumbefores && currow - 0.5 < sumbefores) {
                // define mam, nomam
                sumallMamv.setSum(Math.min(rowbefore, rowbeforebefore));
                sumallMamv.setVerifived(true);
                sumallNoMamv.setSum(Math.max(rowbefore, rowbeforebefore));
                sumallNoMamv.setVerifived(true);

                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public Vector<Double> finddoublenum_withduplicate(int linenumber) {
        Vector<Double> p = new Vector<Double>();
        int start = 0;
        for (int i = 0; i < od.size(); i++) {
            int t = od.elementAt(i).get_line_num();
            if (t == linenumber) {
                start = i;
            }
        }

        for (int i = start; i < od.size(); i++) {
            if ((od.elementAt(i).get_format().equalsIgnoreCase("EURDD") || od.elementAt(i).get_format().equalsIgnoreCase("USADD"))
                    && LFun.twoafterdot(od.elementAt(i).get_result_str())) {

                double val = 0;
                try {
                    Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str()));
                } catch (Exception r) {
                }

                p.add(val);

            }
        }

        //Collections.sort(p);
        return p;

    }

    public Vector<Double> finddoublenum_onlynegativ(int linenumber) {
        Vector<Double> p = new Vector<Double>();
        int start = 0;
        for (int i = 0; i < od.size(); i++) {
            int t = od.elementAt(i).get_line_num();
            if (t == linenumber) {
                start = i;
            }
        }

        for (int i = start; i < od.size(); i++) {
            if ((od.elementAt(i).get_format().equalsIgnoreCase("EURDD") || od.elementAt(i).get_format().equalsIgnoreCase("USADD")
                    || od.elementAt(i).get_format().equalsIgnoreCase("EURDDN") || od.elementAt(i).get_format().equalsIgnoreCase("USADDN"))
                    && LFun.twoafterdot(od.elementAt(i).get_result_str())) {
                double val = 0;
                try {
                    Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str()));
                } catch (Exception r) {
                }
                if (val < 0 && !p.contains(val)) {
                    p.add(val);
                }
            }
        }

        //Collections.sort(p);
        return p;

    }

    public Vector<Double> finddoublenum_withnegativ(int linenumber) {
        Vector<Double> p = new Vector<Double>();
        int start = 0;
        for (int i = 0; i < od.size(); i++) {
            int t = od.elementAt(i).get_line_num();
            if (t == linenumber) {
                start = i;
            }
        }

        for (int i = start; i < od.size(); i++) {
            if ((od.elementAt(i).get_format().equalsIgnoreCase("EURDD") || od.elementAt(i).get_format().equalsIgnoreCase("USADD")
                    || od.elementAt(i).get_format().equalsIgnoreCase("EURDDN") || od.elementAt(i).get_format().equalsIgnoreCase("USADDN"))
                    && LFun.twoafterdot(od.elementAt(i).get_result_str())) {
                try {
                    double val = Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str()));
                    if (!p.contains(val)) {
                        p.add(val);
                    }
                } catch (Exception r) {
                }
            }
        }

        //Collections.sort(p);
        return p;

    }

    public Vector<Double> finddoublenum(int linenumber) {
        Vector<Double> p = new Vector<Double>();
        int start = 0;
        for (int i = 0; i < od.size(); i++) {
            int t = od.elementAt(i).get_line_num();
            if (t == linenumber) {
                start = i;
            }
        }

        for (int i = start; i < od.size(); i++) {
            od.elementAt(i).set_result_str(od.elementAt(i).get_result_str().replaceAll(",", ""));
            if ((od.elementAt(i).get_format().equalsIgnoreCase("EURDD") || od.elementAt(i).get_format().equalsIgnoreCase("USADD"))
                    && LFun.twoafterdot(od.elementAt(i).get_result_str())) {
                try {
                    double val = Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str()));
                    if (!p.contains(val)) {
                        p.add(val);
                    }
                } catch (Exception r) {
                }
            }
        }

        //Collections.sort(p);
        return p;

    }

    public Vector<Double> find3maxparts() {
        Vector<Double> p = new Vector<Double>();
        for (int i = 0; i < od.size(); i++) {
            try {
                if ((od.elementAt(i).get_format().equalsIgnoreCase("EURDD") || od.elementAt(i).get_format().equalsIgnoreCase("USADD"))
                        && LFun.twoafterdot(od.elementAt(i).get_result_str())) {//

                    double val = Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str()));

                    if (!p.contains(val)) {
                        p.add(val);
                    }
                }
            } catch (Exception r) {
            }
        }

        Collections.sort(p);

        //add the number roni found - "totalTaxes_Hayav_Bemaam9" in A4
        if (infoReader != null) {
            if (isA4 && infoReader != null && infoReader.totalTaxes_Hayav_Bemaam9 != null) {
                Double num = 0.00;
                if (!infoReader.totalTaxes_Hayav_Bemaam9.equals("@")) {
                    try {
                        num = Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9);
                    } catch (Exception e) {

                    }
                }
                if (num > 0 && !p.contains(num)) {
                    p.add(0, num);
                }
            }
        }

        return p;

    }

    /**
     * find sum of row prices double SAR (X+Y)
     */
    public Vector<ObData> findsumRowDoublePrices(Vector<ObData> result) {
        double sumalltemp = Double.parseDouble(LFun.fixdot(result.elementAt(0).get_result_str()));

        for (int i = 1; i < result.size() - 1; i++) {
            //  sumalltemp=sumalltemp+Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
            sumalltemp = sumalltemp + Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
            //    System.out.println("sumalltemp = "+sumalltemp);
            Vector<Double> v = findDoubleSAR(result, i + 1, sumalltemp);
            if (v != null && !v.isEmpty()) {
                System.out.println(sumalltemp);
                System.out.println(v.toString());
                System.out.println(sumrowpricesv.getSum());
                Vector<Double> nums = new Vector<Double>();

                for (int k = i + 4 + 1; k < result.size(); k++) {
                    nums.add(Double.parseDouble(LFun.fixdot(result.elementAt(k).get_result_str())));

                }
                int linnum = result.lastElement().get_line_num() + 1;
                lineForDoubleSAR = result.elementAt(i + 4).get_line_num() + 1;
                for (int j = i + 1; j < result.size();) {
                    result.removeElementAt(j);

                }
                //Vector<Double> f=finddoublenum_withnegativ(linnum)
                nums.addAll(finddoublenum_withnegativ(linnum));

                //need add nums from result and zanav
                //search and check anaha
                Double elementAt = sumrowpricesv.getSum();
                for (int j = 0; j < v.size(); j++) {
                    elementAt = elementAt - Math.abs(v.elementAt(j));

                    for (int k = 0; k < nums.size(); k++) {

                        if (checktoler(nums.elementAt(k), elementAt, tolerance)) {
                            //found anahot

                            globalanahav = new VerVarWM(Double.parseDouble(new DecimalFormat("##.##").format(sumrowpricesv.getSum() - nums.elementAt(k))));
                            globalanahav.setVerifived(true);
                            sumrowpricesv.setSum(sumrowpricesv.getSum() - globalanahav.getSum());

                            //find igul
                            nums = findIgul(nums);
                            return result;

                        }
                    }

                }

            }
        }

        return result;
    }

    public Vector<Double> findDoubleSAR(Vector<ObData> result, int i, double sumalltemp) {
        Vector<Double> v = new Vector<Double>();
        int end = i + 4;
        for (; i < end; i++) {
            v.add(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str())));
        }
        System.out.println(v.toString());
        for (int j = 0; j < v.size() - 2; j++) {

            for (int k = j + 1; k < v.size(); k++) {
                Double elementAt = v.elementAt(k) + v.elementAt(j);
                System.out.println(elementAt + " ");
                if (checktoler(sumalltemp, elementAt, tolerance)) {

                    sumrowpricesv = new VerVar(elementAt);//sumalltemp
                    sumrowpricesv.setVerifived(true);
                    sumrowpricesHSv = new VerVar(sumrowpricesv.getSum());
                    sumrowpricesHSv.setVerifived(true);

                    v.removeElementAt(k);
                    v.removeElementAt(j);
                    return v;
                }
            }

        }

        return null;
    }

    /**
     * מקבל את הוקטור הראשוני וסוכם את כל המספרים חיוביים שבו אחר כך מחפש בזנב
     * מספר שסוכם את כל ההנחות ועוד מספ הסך הכל כדי לראות האם הסך הכל פחות כל
     * ההנחות שווה למספר שמצאנו אם מצאנו שני מספרים כאלה אז אפשר לאשרר את הסך
     * הכל וגם אנחנו יודעים שאחד מההנחות שלנו משובש
     */
    public void SumAllWithoutDiscounts(Vector<ObData> result) {
        double sumAll = 0, SumOfAllDiscounts;
        int LineNumberToStartLookingFrom = result.lastElement().get_line_num(), indexStartLookingFrom = 0;
        for (ObData i : result) {
            double temp = Double.parseDouble(LFun.fixdot(i.get_result_str()));
            if (temp > 0) {
                sumAll = sumAll + temp;
            }
        }
        int i = 1;
        ObData obd = od.elementAt(0);
        while (i > od.size() && obd.get_line_num() < LineNumberToStartLookingFrom) {
            obd = od.elementAt(++i);
        }
        indexStartLookingFrom = i;
        try { // INONCODE
            for (i = indexStartLookingFrom; i < od.size(); i++) {
                if (od.elementAt(i).get_format().equals("USADD") || od.elementAt(i).get_format().equals("EURDD")) {
                    double tempSum = Double.parseDouble(LFun.fix2dot(od.elementAt(i).get_result_str()));
                    if (tempSum < sumAll) {
                        for (int j = indexStartLookingFrom; j < od.size(); j++) {
                            if (od.elementAt(j).get_format().equals("USADDN") || od.elementAt(j).get_format().equals("USADD") || od.elementAt(j).get_format().equals("EURDDN") || od.elementAt(j).get_format().equals("EURDD")) {
                                if (checktoler(Double.parseDouble(LFun.fix2dot(od.elementAt(i).get_result_str())), (sumAll - Math.abs(Double.parseDouble(LFun.fix2dot(od.elementAt(j).get_result_str())))), 0.001)) {
                                    double temp = Double.parseDouble(LFun.fix2dot(od.elementAt(i).get_result_str()));

                                    sumrowpricesv.setSum(temp);
                                    sumrowpricesv.setVerifived(true);

                                    SumOfAllDiscounts = Math.abs(Double.parseDouble(LFun.fix2dot(od.elementAt(j).get_result_str())));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

            write(p_filename, this.getClass().toString(), "error in function \"SumAllWithoutDiscounts\"", "", is_log);

        }
    }

    /**
     * find sum of row prices
     */
    public Vector<ObData> findsumRowPrices(boolean negf, double sum, Vector<ObData> result) {
        int linnum = 0;

        // check sumall
        if (sum < 0 && !negf) {
            return result;
        }

        checkIfSUM = sum;

        double sumalltemp = 0.00;
        boolean flagremove = false;

        int maxDeletions = 0;
        if (result.size() > 10) {
            maxDeletions = 5;
        } else {
            maxDeletions = result.size() / 2;
        }

        //מנסה לסכום לאיבר האחרון
        if (sum >= 0) {
            for (int i = 0; i < result.size() && !flagremove; i++) {
                if (!flagremove && Double.parseDouble(LFun.fixdot(result.get(i).get_result_str())) != sum) {
                    sumalltemp = sumalltemp + Double.parseDouble(LFun.fixdot(result.get(i).get_result_str()));
                    if (checktoler(sumalltemp, sum, tolerance) && maxDeletions <= i) {
                        flagremove = true;
                        sumrowpricesv = new VerVar(sum);//sumalltemp
                        sumrowpricesv.setVerifived(true);
                        i = searchDuplicate(i, result);
                        linnum = result.elementAt(i).get_line_num() + 1;
                    }
                }
                if (flagremove) {
                    for (int j = i + 1; j < result.size(); j++) {
                        result.removeElementAt(j);
                        j--;
                    }
                }
            }
        }

        int numOfDeletions = 0;
        boolean canBeSum = false;

        // find if not exist sum all 
        if (sum < 0 && negf) {
            sumalltemp = 0;
            flagremove = false;
            //סוכם את כל המספרים
            for (int i = 0; i < result.size(); i++) {
                sumalltemp = sumalltemp + Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
            }

            boolean fstop = false;

            if (result.size() == 1) {
                fstop = true;
                sumrowpricesv = new VerVar(Double.parseDouble(LFun.fixdot(result.elementAt(0).get_result_str())));//sumalltemp
                sumrowpricesv.setVerifived(true);
                linnum = result.elementAt(0).get_line_num();
            }
            for (int i = result.size() - 1; i >= 0 && !fstop; i--) {
                sumalltemp = sumalltemp - Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
                numOfDeletions++;
                if (i == 1 && result.size() > 2) {
                    continue;
                }
                //    System.out.println("sumalltemp = "+sumalltemp);
                if (sumalltemp > 0 && Double.parseDouble(result.elementAt(i).get_result_str()) > 0) {
                    if (checktoler(sumalltemp, Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str())), 0.0001)) // if(sumalltemp<(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()))+0.5) && sumalltemp>(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()))-0.5))
                    {
                        if (numOfDeletions > maxDeletions) {
                            Vector<ObData> line = LFun.lineByIndex(result.elementAt(i).get_line_num(), od);
                            for (ObData ob : line) {
                                for (String s : config.getForPayWords()) {
                                    if (ob.get_result_str().contains(s)) {
                                        canBeSum = true;
                                    }
                                }
                            }
                        } else {
                            canBeSum = true;
                        }
                        if (canBeSum) {
                            fstop = true;
                            flagremove = true;

                            sumrowpricesv = new VerVar(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str())));//sumalltemp
                            sumrowpricesv.setVerifived(true);

                            i = searchDuplicate(i, result);
                            //i = searchDuplicate(i-1, result);
                            linnum = result.elementAt(i).get_line_num();
                        }
                    }
                }
                /**
                 * if (flagremove) { int a = result.size() - i; for (int j = 0;
                 * j < a; j++) {
                 *
                 * result.removeElementAt(i);
                 *
                 * }*
                 */

                /*yael
                 if(flagremove){
                 for(int j=i; j<result.size();){
                 result.removeElementAt(j);
                 }
                 }
                 */
                if (flagremove) {

                    for (int j = i; j < result.size(); j++) {

                        result.removeElementAt(j);
                        j--;
                    }
                }
            }

        }

        if (!sumrowpricesv.getVerifived()) {
            sumrowpricesv.setSum(-1);

        } else {

            //search global rebate and shipment
            stopcase:
            //case1:(-)+(+)=(+)
            if (result.size() > 1
                    && Double.parseDouble(LFun.fixdot(result.lastElement().get_result_str())) > 0
                    && Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 2).get_result_str())) < 0
                    && (Double.parseDouble(LFun.fixdot(result.lastElement().get_result_str())) + Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 2).get_result_str()))) > 0
                    && GR_SH_case1(linnum, result)) {
                //true?
                System.out.println("Need tipul case1:(-)+(+)=(+)");

                result.removeElement(result.lastElement());
                result.removeElement(result.lastElement());
                break stopcase;
            } //case2: (-)+(+)+(+)=(+)
            else if (result.size() > 2
                    && Double.parseDouble(LFun.fixdot(result.lastElement().get_result_str())) > 0
                    && Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 2).get_result_str())) > 0
                    && Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 3).get_result_str())) < 0
                    && (Double.parseDouble(LFun.fixdot(result.lastElement().get_result_str()))
                    + Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 2).get_result_str()))
                    + Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 3).get_result_str()))) > 0
                    && GR_SH_case2(linnum, result)) {
                //true?
                System.out.println("Need tipul case2: (-)+(+)+(+)=(+)");

                result.removeElement(result.lastElement());
                result.removeElement(result.lastElement());
                result.removeElement(result.lastElement());
                break stopcase;
            } else if (result.size() > 1 && Double.parseDouble(LFun.fixdot(result.lastElement().get_result_str())) > 0
                    && GR_SH_case3(linnum, result)) {

                //case3: (+)=(+)
                System.out.println("Need tipul case3: (+)=(+)");

                result.removeElement(result.lastElement());
                break stopcase;
            } else if (result.size() > 2 && Double.parseDouble(LFun.fixdot(result.lastElement().get_result_str())) > 0
                    && Double.parseDouble(LFun.fixdot(result.elementAt(result.size() - 2).get_result_str())) > 0
                    && GR_SH_case4(linnum, result)) {

                //case4: (+)+(+)=(+)
                System.out.println("Need tipul case4: (+)+(+)=(+)");

                result.removeElement(result.lastElement());
                result.removeElement(result.lastElement());
                break stopcase;
            } else {
                //case5: if not found before
                GR_SH_case5(linnum);
            }

        }

        // }
        if (sumrowpricesv.getVerifived()) {
            double sumResultVector = 0;
            for (int i = 0; i < result.size(); i++) {
                sumResultVector = sumResultVector + Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
                if (sumResultVector == sumrowpricesv.getSum()) {
                    for (int j = i + 1; j < result.size(); j++) {
                        result.remove(j);
                        j--;
                    }
                }
            }
        }

        partscount = new VerVar((double) findpositive(result.size() - 1, result));

        return result;
    }

    /**
     * //case3:(+)=(+) before sum rows
     *
     * @param sumline
     * @param v
     * @return
     */
    public boolean GR_SH_case3(int sumline, Vector<ObData> v) {
        Vector<Double> afterSAR = finddoublenum_withnegativ(sumline); //-----------------------------------------------------------------------------------------------------------------------
        Vector<Double> vGR = new Vector<Double>();

        vGR.add(Double.parseDouble(LFun.fixdot(v.lastElement().get_result_str())));

        afterSAR.remove(sumrowpricesv.getSum());

        //search igul
        try {
            afterSAR = findIgul(afterSAR);
        } catch (Exception e) {
        }

        //first check GR not 0, SH not 0
        double temps = 0;
        double tgr = 0;
        double tsh = 0;

        temps = sumrowpricesv.getSum() - (Math.abs(vGR.lastElement()));
        if (containWithToler(afterSAR, temps, 0)) {

            tsh = Math.abs(vGR.lastElement());

            System.out.println(tgr + " " + tsh + " " + temps);
            sumrowpricesHSv = new VerVar(temps);
            sumrowpricesHSv.setVerifived(true);

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return true;

        }

        return false;
    }

    /**
     * //case4:(+)+(+)=(+) before sum rows
     *
     * @param sumline
     * @param v
     * @return
     */
    public boolean GR_SH_case4(int sumline, Vector<ObData> v) {
        Vector<Double> afterSAR = finddoublenum_withnegativ(sumline);  //-----------------------------------------------------------------------------------------------------------------------
        Vector<Double> vGR = new Vector<Double>();

        for (int i = v.size() - 1; i >= v.size() - 2 && i >= 0; i--) {
            vGR.add(Double.parseDouble(LFun.fixdot(v.elementAt(i).get_result_str())));

        }

        Collections.reverse(vGR);

        afterSAR.remove(sumrowpricesv.getSum());

        //search igul
        try {
            afterSAR = findIgul(afterSAR);
        } catch (Exception e) {
        }

        //first check GR not 0, SH not 0
        double temps = 0;
        double tgr = 0;
        double tsh = 0;

        temps = sumrowpricesv.getSum() - (Math.abs(vGR.elementAt(vGR.size() - 2)) + Math.abs(vGR.lastElement()));
        if (containWithToler(afterSAR, temps, 0)) {

            tsh = vGR.elementAt(vGR.size() - 2);

            System.out.println(tgr + " " + tsh + " " + temps);
            sumrowpricesHSv = new VerVar(temps);
            sumrowpricesHSv.setVerifived(true);

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return true;

        }

        return false;
    }

    /**
     * //case1:case2: (-)+(+)+(+)=(+) before sum rows
     *
     * @param sumline
     * @param v
     * @return
     */
    public boolean GR_SH_case2(int sumline, Vector<ObData> v) {
        Vector<Double> afterSAR = finddoublenum_withnegativ(sumline);//-----------------------------------------------------------------------------------------------------------------------
        Vector<Double> vGR = new Vector<Double>();

        for (int i = v.size() - 1; i >= v.size() - 3 && i >= 0; i--) {
            vGR.add(Double.parseDouble(LFun.fixdot(v.elementAt(i).get_result_str())));

        }

        Collections.reverse(vGR);

        afterSAR.remove(sumrowpricesv.getSum());

        //search igul
        try {
            afterSAR = findIgul(afterSAR);
        } catch (Exception e) {
        }

        //first check GR not 0, SH not 0
        double temps = 0;
        double tgr = 0;
        double tsh = 0;

        temps = sumrowpricesv.getSum() - (Math.abs(vGR.elementAt(vGR.size() - 3)) * (-1) + Math.abs(vGR.elementAt(vGR.size() - 2)) + Math.abs(vGR.lastElement()));
        if (containWithToler(afterSAR, temps, 0)) {

            tgr = vGR.elementAt(vGR.size() - 3);
            tsh = vGR.elementAt(vGR.size() - 2);

            System.out.println(tgr + " " + tsh + " " + temps);
            sumrowpricesHSv = new VerVar(temps);
            sumrowpricesHSv.setVerifived(true);

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return true;

        }

        return false;
    }

    /**
     * //case1:(-)+(+)=(+) before sum rows
     *
     * @param sumline
     * @param v
     * @return
     */
    public boolean GR_SH_case1(int sumline, Vector<ObData> v) {//-----------------------------------------------------------------------------------------------------------------------
        Vector<Double> afterSAR = finddoublenum_withnegativ(sumline);
        Vector<Double> vGR = new Vector<Double>();

        for (int i = v.size() - 1; i >= v.size() - 2 && i >= 0; i--) {
            vGR.add(Double.parseDouble(LFun.fixdot(v.elementAt(i).get_result_str())));

        }

        Collections.reverse(vGR);

        afterSAR.remove(sumrowpricesv.getSum());

        //search igul
        try {
            afterSAR = findIgul(afterSAR);
        } catch (Exception e) {
        }

        //first check GR not 0, SH not 0
        double temps = 0;
        double tgr = 0;
        double tsh = 0;

        temps = sumrowpricesv.getSum() - (Math.abs(vGR.elementAt(vGR.size() - 2)) * (-1) + Math.abs(vGR.lastElement()));
        if (containWithToler(afterSAR, temps, 0)) {

            tgr = vGR.elementAt(vGR.size() - 2);
            tsh = vGR.lastElement();

            System.out.println(tgr + " " + tsh + " " + temps);
            sumrowpricesHSv = new VerVar(temps);
            sumrowpricesHSv.setVerifived(true);

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return true;

        }

        return false;
    }

    public Vector<Double> findIgul(Vector<Double> afterSAR) {
        if ((Math.abs(afterSAR.elementAt(0)) < tnaiIgul || Math.abs(afterSAR.elementAt(1)) < tnaiIgul)
                && (checktoler(Math.abs(afterSAR.elementAt(0)) * (-1) + afterSAR.elementAt(1), sumrowpricesv.getSum(), tolerance)
                || checktoler(Math.abs(afterSAR.elementAt(1)) * (-1) + afterSAR.elementAt(0), sumrowpricesv.getSum(), tolerance))) {
            sumrowpricesv.setSum(Math.max(Math.abs(afterSAR.elementAt(0)), Math.abs(afterSAR.elementAt(1))));
            igulv.setSum(Math.min(Math.abs(afterSAR.elementAt(0)), Math.abs(afterSAR.elementAt(1))));
            igulv.setVerifived(true);
            afterSAR.removeElementAt(0);
            afterSAR.removeElementAt(0);
        }

        return afterSAR;
    }

    /**
     * find global rebate, global shipment and sum rows by them if not found by
     * case1-4
     */
    public void GR_SH_case5(int sumline) {
        Vector<Double> afterSAR = finddoublenum_withnegativ(sumline);
        Vector<Double> vGR = new Vector<Double>();
        Vector<Double> vSH = new Vector<Double>();

        afterSAR.remove(sumrowpricesv.getSum());

        //search igul
        try {
            afterSAR = findIgul(afterSAR);
        } catch (Exception e) {
        }

        for (int i = 0; i < afterSAR.size(); i++) {
            vGR.add(afterSAR.elementAt(i));
            vSH.add(afterSAR.elementAt(i));
        }

        //first check GR not 0, SH not 0
        double temps = 0;
        double tgr = 0;
        double tsh = 0;
        double tsum = 0;
        boolean findflag = false;
        stoploop:
        for (int i = 0; i < vGR.size(); i++) {
            for (int j = 0; j < vSH.size(); j++) {
                temps = sumrowpricesv.getSum() + (Math.abs(vGR.elementAt(i)) * (-1) + Math.abs(vSH.elementAt(j)));
                if (containWithToler(afterSAR, temps, 0) && !findflag) {
                    findflag = true;
                    tgr = vGR.elementAt(i);
                    tsh = vSH.elementAt(j);
                    tsum = temps;
                    //tsum = sumrowpricesv.getSum()-((Math.abs(tgr) * (-1) + Math.abs(tsh)));
                    findflag = true;
                } else if (containWithToler(afterSAR, temps, 0) && !findflag && tsum != temps) {
                    findflag = false;
                    break stoploop;
                }
            }
        }

        System.out.println(tgr + " " + tsh + " " + tsum);

        if (findflag) {
            sumrowpricesHSv = new VerVar(sumrowpricesv.getSum());
            sumrowpricesHSv.setVerifived(true);
            case5done = true;

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return;
        }
        //secondcheck check GR = 0, SH not 0

        temps = 0;
        tgr = 0;
        tsh = 0;
        tsum = 0;
        findflag = false;
        stoploop:

        for (int j = 0; j < vSH.size(); j++) {
            temps = sumrowpricesv.getSum() + (Math.abs(vSH.elementAt(j)));
            if (containWithToler(afterSAR, temps, 0) && !findflag) {
                findflag = true;

                tsh = vSH.elementAt(j);
                tsum = temps;
                // tsum = sumrowpricesv.getSum()-(Math.abs(tsh));
                findflag = true;
            } else if (containWithToler(afterSAR, temps, 0) && !findflag && tsum != temps) {
                findflag = false;
                break stoploop;
            }
        }

        System.out.println(tgr + " " + tsh + " " + tsum);

        if (findflag) {
            sumrowpricesHSv = new VerVar(sumrowpricesv.getSum());
            sumrowpricesHSv.setVerifived(true);
            case5done = true;

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return;
        }

        //thirdcheck check GR not 0, SH = 0
        temps = 0;
        tgr = 0;
        tsh = 0;
        tsum = 0;
        findflag = false;
        stoploop:

        for (int j = 0; j < vGR.size(); j++) {
            temps = sumrowpricesv.getSum() + (Math.abs(vGR.elementAt(j)) * (-1));
            if (containWithToler(afterSAR, temps, tolerance) && !findflag) { //afterSAR.contains(temps)
                findflag = true;
                if (!CheckIfMaam_withmaam_withoutmaam(sumrowpricesv.getSum(), vGR.elementAt(j))) {
                    tgr = vGR.elementAt(j);//---------------------------------------------------------------------------------------------------------------------------------------------
                    tsum = temps;
                    //tsum = sumrowpricesv.getSum()-( Math.abs(tgr)* (-1));

                    findflag = true;
                }
            } else if (containWithToler(afterSAR, temps, 0) && !findflag && tsum != temps) {
                findflag = false;
                break stoploop;
            }
        }

        System.out.println(tgr + " " + tsh + " " + tsum);
        if (findflag) {
            sumrowpricesHSv = new VerVar(sumrowpricesv.getSum());
            sumrowpricesHSv.setVerifived(true);
            case5done = true;

            globalanahav = new VerVarWM(tgr);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(tsh);

            return;
        } else {
            sumrowpricesHSv = new VerVar(sumrowpricesv.getSum());
            sumrowpricesHSv.setVerifived(true);
            case5done = true;

            globalanahav = new VerVarWM(0);
            globalanahav.setVerifived(true);
            shipmentv = new VerVarWM(0);

            return;
        }

    }

    public Vector<Double> sortVector(Vector<Double> v) {
        double[] array = new double[v.size()];
        for (int i = 0; i < v.size(); i++) {
            array[i] = v.get(i);
        }
        Arrays.sort(array);
        v = new Vector<Double>();
        for (int i = 0; i < array.length; i++) {
            v.add(array[i]);
        }
        return v;
    }

    public boolean containWithToler(Vector<Double> v, double num, double tol) {
        v = sortVector(v);
        float fnum = (float) num;
        for (int i = 0; i < v.size(); i++) {
            double elementAt = v.elementAt(i);
            float felementAt = (float) elementAt;
            if (checktoler(fnum, felementAt, tol)) {
                return true;
            }

        }
        return false;
    }

    /**
     * remove maximum from two closed numbers if different between less 0.51
     * param v sorted maximum numbers @return
     */
    public Vector<Double> delClosedNumby051(Vector<Double> v) {
        for (int i = 0; i < v.size() - 1; i++) {
            double d = Math.abs(v.elementAt(i) - v.elementAt(i + 1));
            if (d < 0.51) {
                v.remove(Math.max(v.elementAt(i), v.elementAt(i + 1)));
            }

        }
        return v;
    }

    /*

     public Vector<ObData> digitssumall(Vector<ObData> result, int n) {
     if (result.size() == 0) {
     return null;
     }

     try {

     //search double SAR (X+Y)
     //  result = findsumRowDoublePrices(result);
     if (sumrowpricesv.getVerifived()) {
     return result;
     }
     } catch (Exception e) {

     }

     //backup result
     Vector<ObData> backresult = new Vector<ObData>(result);
     Vector<Double> tree_maxparts = delClosedNumby051(find3maxparts());

     int itercount = 0;
     while (!sumrowpricesv.getVerifived() && itercount < iterforsumfind) {
     itercount++;
     try {
     if (itercount > 1) {
     tree_maxparts.removeElement(tree_maxparts.lastElement());
     }
     System.out.println("\nSearch in first " + tree_maxparts.lastElement());
     if (result.size() > 1 && checktoler(Double.parseDouble(LFun.fixdot(result.elementAt(0).get_result_str())), tree_maxparts.lastElement(), tolerance)) {

     } else {
     result = findsumRowPrices(false, tree_maxparts.lastElement(), result);
     }
     System.out.println("checked : " + tree_maxparts.lastElement());
     } catch (Exception e) {
     }
     }
     // additional search in first vector if not found 

     if (!sumrowpricesv.getVerifived()) {

     System.out.println("\nSearch -1");
     result = findsumRowPrices(true, -1, result);
     }
     //search in first vector looking for sum of all Discounts And credits numbers,so we can compare it with 2 numbers in tail and see if its matches.

     if (!sumrowpricesv.getVerifived()) {

     System.out.println("\nSearch sum of all without discounts ");
     SumAllWithoutDiscounts(result);
     }

     if (!sumrowpricesv.getVerifived()) {
     Vector<ObData> tempSimilar = Copy(result);
     boolean stop = false;
     for (int i = 0; i < result.size() && !stop; i++) {
     Vector<String> b_vector = createSimilarVector(String.valueOf(result.elementAt(i).get_result_str()));
     for (int j = 0; j < b_vector.size(); j++) {
     tempSimilar.elementAt(i).set_result_str(b_vector.get(j));
     tempSimilar = findsumRowPrices(true, -1, tempSimilar);
     if (sumrowpricesv.getVerifived()) {
     result = tempSimilar;
     stop = true;
     break;
     }
     tempSimilar.elementAt(i).set_result_str(b_vector.get(0));
     }
     }
     }

     if (sumrowpricesv.getVerifived()) {
     return result;
     }

     //if not found in furst vector -> search in merged vector  
     //backup result
     backresult = new Vector<ObData>(result);

     result = tempall;
     if (result.size() == 0) {
     return null;
     }

     tree_maxparts = delClosedNumby051(find3maxparts());

     itercount = 0;
     while (!sumrowpricesv.getVerifived() && itercount < iterforsumfind) {
     itercount++;
     try {
     if (itercount > 1) {
     tree_maxparts.removeElement(tree_maxparts.lastElement());
     }
     System.out.println("\nSearch in merged " + tree_maxparts.lastElement());
     result = findsumRowPrices(false, tree_maxparts.lastElement(), result);
     System.out.println("checked : " + tree_maxparts.lastElement());
     } catch (Exception e) {
     }
     }
     // additional search in merged vector if not found 

     if (!sumrowpricesv.getVerifived()) {
     System.out.println("\nSearch in merged -1");

     result = findsumRowPrices(true, -1, result);
     }
     if (!sumrowpricesv.getVerifived()) {

     return backresult;
     //return null;
     }

     return result;
     }
     */
    /**
     * result = tampall from func getdigits
     */
    /**
     * מסכם את הוקטור לוקטור שבו כל איבר הוא סכום איבריו הקודמים
     *
     * @param result
     * @return
     */
    public Vector<Double> sumAllprices(Vector<ObData> result, int start_index) {

        Vector<ObData> temp = Copy(result);
        Vector<Double> res = new Vector<Double>();

        try {
            for (int i = 0; i < temp.size(); i++) {
                if (i <= start_index) {
                    res.add(Double.valueOf(temp.get(i).get_result_str()));
                } else {
                    res.add(Double.valueOf(temp.get(i).get_result_str()) + Double.valueOf(res.get(i - 1)));
                }
            }

        } catch (Exception e) {

        }
        return res;
    }

    //try to find if exists sub total, if yes return his place in vector result 
    public Vector<Vector<Integer>> trySubTotal(Vector<ObData> result) {

        Vector<Double> temp = sumAllprices(result, 0);
        Vector<Vector<Integer>> placesSubTotal = new Vector<Vector<Integer>>();
        Vector<ObData> tempSimilar = Copy(result);

        try {

            for (int i = 1; i < temp.size() - 2; i++) {
                temp = sumAllprices(result, 0);
                if (checktoler(temp.get(i), Double.valueOf(tempSimilar.get(i + 1).get_result_str()), 0.0001)) {
                    Vector<Integer> curr = new Vector<Integer>();
                    curr.add(i);
                    placesSubTotal.add(curr);
                    temp = sumAllprices(result, i + 2);
                    for (int j = i + 2; j < temp.size() - 2; j++) {
                        if (temp.size() - j > 4) {
                            j += 3;
                        } else {
                            break;
                        }
                        if (checktoler(temp.get(j), Double.valueOf(tempSimilar.get(j + 1).get_result_str()), tolerance)) {
                            placesSubTotal.get(i).add(j);
                            temp = sumAllprices(result, j + 2);
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Vector<Vector<Integer>>();
        }
        return placesSubTotal;

    }

    public void add00ToTotalLines(Vector<ObData> result, Vector<String> vtypes) {
        for (int i = 0; i < od.size(); i++) {
            if (od.get(i).get_result_str().equals("0.00")) {
                boolean sameVtypes = false;
                for (String vtype : vtypes) {
                    if (od.get(i).get_format().equalsIgnoreCase(vtype)) {
                        sameVtypes = true;
                    }
                }
                if (sameVtypes) {
                    if (Math.abs(result.get(result.size() - 1).get_left() - od.get(i).get_left()) < DEVIATION
                            || Math.abs(result.get(result.size() - 1).get_right() - od.get(i).get_right()) < DEVIATION) {

                        Total_Lines.add(od.get(i).get_pfile_num() + "_" + od.get(i).get_line_num());
                    }
                }
            }
        }
    }

    public static int digitssumall_BYWisePage_min_size;

    public Vector<ObData> digitssumall_BYWisePage(Vector<ObData> result, boolean isCheck) {
        Total_Lines = new Vector<String>();

        if (result == null || result.size() == 0 || result.size() >= digitssumall_BYWisePage_min_size) {
            return null;
        }

        try {
            if (Math.abs(infoReader.totalInvoiceSum13 - Double.valueOf(result.get(result.size() - 1).get_result_str())) < DeviationForBudget) {

                for (int i = 0; i < result.size() - 1; i++) {
                    if (Math.abs(infoReader.totalInvoiceSum13 - Double.valueOf(result.get(i).get_result_str())) < DeviationForBudget) {
                        Total_Lines.add(result.get(i).get_pfile_num() + "_" + result.get(i).get_line_num());
                        result.remove(i--);
                    }
                }
            }
        } catch (Exception e) {
        }

        //zvika 4.10.2018:
        try {
            if (result.size() > 4) {
                double Total1 = 0;
                for (int i = 0; i < result.size() - 4; i++) {
                    Total1 += Double.valueOf(result.get(i).get_result_str());
                }
                double Total2 = Double.valueOf(result.get(result.size() - 4).get_result_str());
                double Total3 = Double.valueOf(result.get(result.size() - 3).get_result_str());
                double Tax = Double.valueOf(result.get(result.size() - 2).get_result_str());
                double Total_Due = Double.valueOf(result.get(result.size() - 1).get_result_str());
                if ((Math.abs(Total1 - Total2) <= DeviationForBudget
                        && Math.abs(Total2 - Total3) <= DeviationForBudget
                        && Check.CheckSingleMam.iSMam(mam_percents, Tax, Total2, Total_Due, DeviationForBudget))
                        || //for 6683282_1_7_SIGNED.pdf of Fatal
                        (Math.abs(Total1 + Total_Due - Total2) <= DeviationForBudget//Total_Due IS TAX;
                        && Math.abs(Total1 - Total3 - Tax) <= DeviationForBudget
                        && Check.CheckSingleMam.iSMam(mam_percents, Total_Due, Tax, Total2 - Total3, DeviationForBudget))) {
                    //&& Math.abs(Total_Due - (Total2 + Tax)) <= DeviationForBudget) {
                    if (!isCheck) {
                        Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                    }
                    result.remove(result.size() - 1);
                    if (!isCheck) {
                        Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                    }
                    result.remove(result.size() - 1);
                    if (!isCheck) {
                        Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                    }
                    result.remove(result.size() - 1);
                    if (!isCheck) {
                        Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                    }
                    result.remove(result.size() - 1);
                    return result;
                }
            }

            if (result.size() > 3) {
                double Total1 = 0;
                for (int i = 0; i < result.size() - 3; i++) {
                    Total1 += Double.valueOf(result.get(i).get_result_str());
                }
                double Total2 = Double.valueOf(result.get(result.size() - 3).get_result_str());
                double Tax = Double.valueOf(result.get(result.size() - 2).get_result_str());
                double Total_Due = Double.valueOf(result.get(result.size() - 1).get_result_str());
                if (Math.abs(Total1 - Total2) <= DeviationForBudget
                        && Check.CheckSingleMam.iSMam(mam_percents, Tax, Total2, Total_Due, DeviationForBudget)) {
                    if (!isCheck) {
                        if (result.get(result.size() - 1).get_pfile_num() == result.get(result.size() - 3).get_pfile_num()) {
                            for (int i = result.get(result.size() - 3).get_line_num(); i <= 100; i++) {
                                Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + i);
                            }
                        } else {
                            Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                            Total_Lines.add(result.get(result.size() - 2).get_pfile_num() + "_" + result.get(result.size() - 2).get_line_num());
                            Total_Lines.add(result.get(result.size() - 3).get_pfile_num() + "_" + result.get(result.size() - 3).get_line_num());
                        }
                    }

                    result.remove(result.size() - 1);
                    result.remove(result.size() - 1);
                    result.remove(result.size() - 1);

                    //fix wisepage result:
                    if (infoReader != null) {
                        if (infoReader.totalTaxes10 == null || infoReader.totalTaxes10.equals("@") || infoReader.totalTaxes10.isEmpty()) {
                            infoReader.totalTaxes10 = Tax + "";
                        }
                        if (infoReader.totalTaxes_Hayav_Bemaam9 == null || infoReader.totalTaxes_Hayav_Bemaam9.equals("@") || infoReader.totalTaxes_Hayav_Bemaam9.isEmpty()) {
                            infoReader.totalTaxes_Hayav_Bemaam9 = Total1 + "";
                            if (infoReader.totalInvoiceSum13 == 0) {
                                infoReader.totalInvoiceSum13 = Total_Due;
                            }
                        }
                        if (infoReader.totalPriceNoTaxes8 == null || infoReader.totalPriceNoTaxes8.equals("@") || infoReader.totalPriceNoTaxes8.isEmpty()) {
                            infoReader.totalTaxes_Hayav_Bemaam9 = "0.00";
                        }
                    }

                    return result;
                }
            }
            if (result.size() > 2) {
                double Total1 = 0;
                for (int i = 0; i < result.size() - 2; i++) {
                    Total1 += Double.valueOf(result.get(i).get_result_str());
                }
                double Total2 = Double.valueOf(result.get(result.size() - 2).get_result_str());
                double Tax = 0.0;
                double Total_Due = Double.valueOf(result.get(result.size() - 1).get_result_str());
                if (Math.abs(Total1 - Total2) <= DeviationForBudget
                        && Check.CheckSingleMam.iSMam(mam_percents, Tax, Total2, Total_Due, DeviationForBudget)) {
                    if (!isCheck) {
                        Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                    }
                    result.remove(result.size() - 1);
                    if (!isCheck) {
                        Total_Lines.add(result.get(result.size() - 1).get_pfile_num() + "_" + result.get(result.size() - 1).get_line_num());
                    }
                    result.remove(result.size() - 1);
                    return result;
                }
            }

            if (result.size() > 1) {
                try {

                    double Total1 = 0;
                    for (int i = 0; i < result.size(); i++) {
                        Total1 += Double.valueOf(result.get(i).get_result_str());
                    }
                    if (Math.abs(Total1) > 0
                            && (Math.abs(Total1 - Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9)) <= DeviationForBudget
                            || Math.abs(Total1 - Double.valueOf(infoReader.totalInvoiceSum13)) <= DeviationForBudget)) {
                        return result;
                    }
                } catch (Exception e) {
                }
            }

            if (result.size() > 6) {
                for (int i = 1; i <= result.size() - 3; i++) {
                    Vector<ObData> result2 = LFun.copyVec(result, 0, result.size() - i);
                    result2 = digitssumall_BYWisePage(result2, isCheck);
                    if (result2 != null) {
                        return result2;
                    }
                }
            }

            if (result.size() > 3 && result.get(0).get_result_str().replaceAll(",", "").equalsIgnoreCase(result.get(result.size() - 1).get_result_str().replaceAll(",", ""))) {
                Vector<ObData> result2 = LFun.copyVec(result, 1, result.size());
                result2 = digitssumall_BYWisePage(result2, isCheck);
                if (result2 != null) {
                    return result2;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vector<ObData> digitssumall(Vector<ObData> result, int n) {

        if (result.size() == 1) {
            findsumRowPrices(true, -1, result);
            //sumrowpricesv = new VerVar(Double.valueOf(result.elementAt(0).get_result_str()));
            //sumrowpricesv.setVerifived(true);
            return result;
        }

        int minb = 0, maxb = 0;
        int numberRound = 0;
        if (result == null) {
            return null;
        }
        if (result.size() > 0) {
            int balance = 0;
            if (result.size() >= 1) {
                Vector<Integer> rightCor = new Vector<Integer>();
                try {
                    rightCor.add(result.elementAt(0).get_right());
                    rightCor.add(result.elementAt(1).get_right());
                    rightCor.add(result.elementAt(2).get_right());
                    rightCor.add(result.elementAt(3).get_right());
                } catch (Exception e) {
                }

                balance = getBalance(rightCor);
                minb = balance - DEVIATION;
                //minb = result.elementAt(i).get_right() - DEVIATION;
                maxb = balance + DEVIATION;

            }
            for (int i = 1; i < result.size(); i++) {
                boolean isSingleItem = false;
                if (result.elementAt(i - 1).get_result_str().charAt(0) == '-'
                        && result.elementAt(i).get_result_str().charAt(0) == '-') {
                    minb = result.elementAt(i - 1).get_right() - DEVIATION;
                    maxb = result.elementAt(i - 1).get_right() + DEVIATION;
                } else if (result.elementAt(i - 1).get_result_str().charAt(0) != '-'
                        && result.elementAt(i).get_result_str().charAt(0) != '-') {
                    minb = result.elementAt(i - 1).get_right() - DEVIATION;
                    maxb = result.elementAt(i - 1).get_right() + DEVIATION;
                } else if (result.elementAt(i - 1).get_result_str().charAt(0) == '-'
                        && result.elementAt(i).get_result_str().charAt(0) != '-') {
                    minb = result.elementAt(i - 1).get_right() - (DEVIATION * 2);
                    maxb = result.elementAt(i - 1).get_right() + (DEVIATION * 2);
                } else if (result.elementAt(i - 1).get_result_str().charAt(0) != '-'
                        && result.elementAt(i).get_result_str().charAt(0) == '-') {
                    minb = result.elementAt(i - 1).get_right() - (DEVIATION * 2);
                    maxb = result.elementAt(i - 1).get_right() + (DEVIATION * 2);
                }

                if ((minb > result.elementAt(i).get_right() || result.elementAt(i).get_right() > maxb) && isBigThenOne && (result.elementAt(i).get_right() != balance || numberRound != 0) /*&& !yael*/) {
                    if (numberRound == 0 && i == 0 && result.size() > 1 && isA4) {
                        try {
                            Double number1 = Double.valueOf(result.elementAt(i).get_result_str());
                            Double number2 = Double.valueOf(result.elementAt(i + 1).get_result_str());
                            if (LFun.checktoler(number1, number2, tolerance)) {
                                isSingleItem = true;
                            }
                        } catch (Exception e) {

                        }
                    }
                    if (!isSingleItem) {
                        //sc.add(result.elementAt(i));
                        // resultall.add(result.elementAt(i));
                        System.out.println("sc " + result.elementAt(i).get_result_str() + "\t" + result.elementAt(i).get_left() + "\t" + result.elementAt(i).get_right());
                        result.removeElementAt(i);
                        i = i - 1;
                    }
                }
                isBigThenOne = true;
                isSingleItem = false;
                numberRound++;

                /*if (minb > result.elementAt(i).get_right() || result.elementAt(i).get_right() > maxb) {
                        sc.add(result.elementAt(i));
                        // resultall.add(result.elementAt(i));
                        System.out.println("sc " + result.elementAt(i).get_result_str() + "\t" + result.elementAt(i).get_left() + "\t" + result.elementAt(i).get_right());
                        result.removeElementAt(i);
                        i = i - 1;
                    }*/
            }
        }

        result = CleanVector(result, false);
        try {

            //search double SAR (X+Y)
            //print2file("\r\n!!! zero search in first vector for double SAR \r\n");
            //  result = findsumRowDoublePrices(result);
            if (sumrowpricesv.getVerifived()) {
                return result;
            }
        } catch (Exception e) {
        }

        //backup result
        Vector<ObData> backresult = new Vector<ObData>(result);
        Vector<Double> tree_maxparts = delClosedNumby051(find3maxparts());
        backresult = new Vector<ObData>(result);

        int Check = 0, MaxCheck = 2;

        while (Check < MaxCheck) {

            //-מנסה לחשב את הסכום עם ורציאיות שונות של המחירים
            //יוצר וקטור עם מספרים דומים בפונקציה  
            //createSimilarVector
            if (!sumrowpricesv.getVerifived()) { // Change Similar Numbers.
                Vector<ObData> tempSimilar = Copy(result);
                boolean stop = false;
                if (result.size() > 5) {
                    for (int i = 0; i < result.size() && !stop; i++) {
                        tempSimilar = Copy(result);
                        Vector<String> b_vector = createSimilarVectorWithLimit(String.valueOf(result.elementAt(i).get_result_str()), 1);
                        for (int j = 0; j < b_vector.size() && !stop; j++) {
                            tempSimilar.elementAt(i).set_result_str(b_vector.get(j));
                            Vector<Vector<Integer>> placesSubTotal = trySubTotal(tempSimilar);
                            if (placesSubTotal.size() > 0) {
                                Vector<ObData> x = Copy(tempSimilar);
                                Vector<ObData> y = Copy(x);
                                for (int k = 0; k < placesSubTotal.size(); k++) {
                                    for (int k2 = 0; k2 < placesSubTotal.get(k).size(); k2++) {
                                        x.remove(placesSubTotal.get(k).get(k2) + 1);
                                        x = findsumRowPrices(true, -1, x);
                                        if (sumrowpricesv.getVerifived()) {
                                            result = x;
                                            stop = true;
                                            break;
                                        }
                                    }
                                    x = y;
                                }
                                if (sumrowpricesv.getVerifived()) {
                                    break;
                                }
                                x = Copy(tempSimilar);
                                x = findsumRowPrices(true, -1, x);
                                if (sumrowpricesv.getVerifived()) {
                                    result = x;
                                    stop = true;
                                    break;
                                }

                            } else {
                                //tempSimilar.elementAt(i).set_result_str(b_vector.get(j));
                                tempSimilar = findsumRowPrices(true, -1, tempSimilar);
                                if (sumrowpricesv.getVerifived()) {
                                    result = tempSimilar;
                                    stop = true;
                                    break;
                                }
                                tempSimilar.elementAt(i).set_result_str(b_vector.get(0));
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < result.size() && !stop; i++) {
                        Vector<String> b_vector = createSimilarVectorWithLimit(String.valueOf(result.elementAt(i).get_result_str()), 1);
                        for (int j = 0; j < b_vector.size(); j++) {
                            tempSimilar.elementAt(i).set_result_str(b_vector.get(j));
                            tempSimilar = findsumRowPrices(true, -1, tempSimilar);
                            if (sumrowpricesv.getVerifived()) {
                                result = tempSimilar;
                                stop = true;
                                break;
                            }
                            tempSimilar.elementAt(i).set_result_str(b_vector.get(0));
                        }
                    }
                }
            }

            if (sumrowpricesv.getVerifived()) {
                //cleanAnaha(result);
                return result;
            }

            int itercount = 0;
            /*double diffrence = 0.00;
             int index = 0;
             if (result.size() > 2){
             index = result.size();
             diffrence = Double.parseDouble(result.elementAt(index-1).get_result_str()) - 
             Double.parseDouble(result.elementAt(index-2).get_result_str());
             }*/
            while (!sumrowpricesv.getVerifived() && itercount < iterforsumfind && result.size() >= 2) {
                //diffrence > Double.parseDouble(result.elementAt(index-3).get_result_str())
                itercount++;
                try {
                    // כדי לא למחוק בהרצה הראשונה את הlast_elemnt
                    if (itercount > 1) {
                        tree_maxparts.removeElement(tree_maxparts.lastElement());
                    }
                    System.out.println("\nSearch in first " + tree_maxparts.lastElement());

                    if (checktoler(Double.parseDouble(LFun.fixdot(result.elementAt(0).get_result_str())), tree_maxparts.lastElement(), tolerance)) {

                    } else {
                        result = findsumRowPrices(false, tree_maxparts.lastElement(), result);
                    }
                    System.out.println("checked : " + tree_maxparts.lastElement());
                } catch (Exception e) {
                }
            }
            // additional search in first vector if not found 

            if (!sumrowpricesv.getVerifived()) {

                //print2file("\r\n!!! additional search in first vector if not found \r\n");
                System.out.println("\nSearch -1");
                result = findsumRowPrices(true, -1, result);
            }
            //search in first vector looking for sum of all Discounts And credits numbers,so we can compare it with 2 numbers in tail and see if its matches.
            if (!sumrowpricesv.getVerifived()) {

                //print2file("\r\n!!! additional search in first vector looking for sum of all Discounts And credits numbers \r\n");
                System.out.println("\nSearch sum of all without discounts ");
                SumAllWithoutDiscounts(result);
            }

            if (sumrowpricesv.getVerifived()) {
                return result;
            }

            //if not found in furst vector -> search in merged vector  
            //backup result
            //backresult = new Vector<ObData>(result);
            result = tempall;
            if (result.size() == 0) {
                return null;
            }

            tree_maxparts = delClosedNumby051(find3maxparts());

            itercount = 0;
            while (!sumrowpricesv.getVerifived() && itercount < iterforsumfind) {
                itercount++;
                try {
                    if (itercount > 1) {
                        tree_maxparts.removeElement(tree_maxparts.lastElement());
                    }
                    System.out.println("\nSearch in merged " + tree_maxparts.lastElement());
                    result = findsumRowPrices(false, tree_maxparts.lastElement(), result);
                    System.out.println("checked : " + tree_maxparts.lastElement());
                } catch (Exception e) {
                }
            }
            // additional search in merged vector if not found 

            if (!sumrowpricesv.getVerifived()) {
                System.out.println("\nSearch in merged -1");
                //print2file("\r\n!!! additional search in merged vector if not found \r\n");
                result = findsumRowPrices(true, -1, result);
            }
            if (sumrowpricesv.getVerifived()) {
                return result;
            }

            Check++;

            Vector<ObData> temp1 = new Vector<ObData>(backresult);

            temp1 = CleanVector(temp1, false);
            result = new Vector<ObData>(temp1);

        }

        for (int i = 0; i < backresult.size(); i++) {
            if (backresult.get(i).get_name_by_inv().contains("pn")) {
                backresult.remove(backresult.get(i));
                i--;
            }
        }
        backresult = CleanVector(backresult, false);

        return backresult;
    }

    public void cleanAnaha(Vector<BNode> bnv) {

        for (int i = 0; i < bnv.size(); i++) {
            if (bnv.get(i).price.get_result_str().contains("-")) {
                bnv.get(i).price.set_name_by_inv("neg_price");
                bnv.remove(i);
                i--;
            }
        }
    }

    /**
     * find blocks in invoice
     */
    public Vector<BNode> get_blocks(Vector<String> vnum) {

        hanaha_Vector = new Vector<Hanaha>();
        intnum = new Vector<Integer>();
        p_vnum = vnum;
        //   Vector<String> vnum=new Vector<String>();
        // vnum.add("USADD");
        // vnum.add("EURDD");  
        // vnum.add("USADDN");
        // vnum.add("EURDDN"); 
        // vnum.add("INTP");
        // vnum.add("INTPSEP");
        Vector<ObData> result = digitssumall(get_ddigids(vnum), 1);
        if (result == null) {
            return null;
        }

        Vector<String> vnumdefault = new Vector<String>();

        vnumdefault.add("USAD");
        vnumdefault.add("EURD");
        vnumdefault.add("USADN");
        vnumdefault.add("EURDN");
        vnumdefault.add("USADD");
        vnumdefault.add("EURDD");
        vnumdefault.add("USADDN");
        vnumdefault.add("EURDDN");
        vnumdefault.add("USADDD");
        vnumdefault.add("EURDDD");
        vnumdefault.add("USADDDN");
        vnumdefault.add("EURDDDN");
        vnumdefault.add("INTP");
        //vnumdefault.add("INTPSEP");

        //create lines vector and return vector that included prices that fixed.
        Vector<ObData> fixedPrices = creationRowsVectorForAllRows(vnumdefault, vnum, result); // לוקחת את הטקסט מהCSV    
        //ננסה לסכום עם המחירים המתוקנים שוב
        if (fixedPrices.size() != 0 && !sumrowpricesv.getVerifived()) {
            // להוסיף את המספרים למקום המתאים ולנסות לסכום אותם
            result = add_fixed_prices_to_vector_result(result, fixedPrices);
        }
        if (!sumrowpricesv.getVerifived()) {
            result = tryUsadAndEurdNumbers(result, vnumdefault);
        }
        printLines(lines);
        //search X and replace to empty  
        searchXreplace(vnumdefault);

        redefinepn2(vnumdefault, result);

        // first find qty and price  
        findQtyAndPrices(result); // 

        //find last price index
        int end_prices = getLineNumBycsvline(result.lastElement().get_line_num()) + 1; // end items index
        int shope = getLineNumBycsvline(result.elementAt(0).get_line_num()); //- 1); // start items index
        fill_external_source();//Inon

        printLines(lines);

        if (sumrowpricesv.getVerifived()) {
            useResultVector(result, shope, end_prices); // FIX all the qty & prices & one prices
        }

        //find partnumbers
        //check left & right of p/n
        if (!findPNLeftAndRightCheck(vnumdefault, result, end_prices, "MAKAT")) {
            findPNLeftAndRightCheck(vnumdefault, result, end_prices, "no");
        }

        //correction for qty & one_price by header dictonary
        correct_qty_one_price(result.elementAt(0).get_line_num() - 2);

        // create blocks
        countblock = MarkBlocks.blockmarking(lines, result.elementAt(0).get_line_num(), result.lastElement().get_line_num(), sumrowpricesv.getVerifived(), result, config.getForPayWords(), null);
        //find description , percents , nums , free
        findDesPercNumsFree(end_prices);

        //check zikui and anaha
        checkZikuiAndAnaha(shope, end_prices);

        //add to matrix by blocks : nblock pn price qty one_price descr
        Vector<BNode> bnv = null;

        bnv = createBNV(shope, end_prices, vnumdefault, result);
        //mam   
        int linenum = index_by_line(result.lastElement().get_line_num());
        doublenum = finddoublenum(Math.max(lineForDoubleSAR, linenum));

        //  fil percent by list
        bnv = filPercentMamByList(bnv);

        //  mam data
        bnv = filExistMamData(bnv);

        //search anahot
        bnv = fil_anahadata(bnv);

        bnv = fil_1p1_1p2_anahot(bnv); //only in super

        bnv = fil_general_anahot(bnv);////111111

        //check countparts
        bnv = checkAndFilQTY(bnv);

        //bnv ready
        Vector<Vector<Double>> amounts = getAmountsFromTheBnv(bnv);

        //mark sumwithmam and sumwithoumam if mam found
        markSumEnd(linenum, bnv, result);

        // fix qty and price
        bnv = fixQtyAndPrices(bnv, vnumdefault);

        //check if the file amount is equal to the sum of the discount and the price.
        if (!sumrowpricesHSv.getVerifived()) {
            checkSumPosEqual2NumFromTail(amounts.get(0).get(0), amounts.get(1), bnv);
        }

        boolean A4 = false;

        // check if the prices in items csv is fit to the price we get
        /*if (!sumrowpricesv.getVerifived()) {
         changePricesByItemsCsv(bnv, A4);
         }*/
        bnv = fixDetUsingUnitPriceCsu(bnv);

        checkWithBigVector(bnv, A4);
        //להוריד מחירים שכוללים מינוס
        cleanAnaha(bnv);

        bnv = fixNameByInv(bnv, vnumdefault);
        printLines(lines);

        /*try{
         for (BNode bNode : bnv) {
         System.out.println("yael");
         System.out.println( "price:" + bNode.price.get_result_str() + " percent: " + bNode.percent.get_result_str() + " mam: " + bNode.mam.get_result_str() + " anaha: " + bNode.anaha + " without_mam " + bNode.without_mam.get_result_str() + " with_mam " + bNode.with_mam.get_result_str());
         }            
         }
         catch(Exception e){  
         }*/
        //System.out.println("qty:" +bnv.get(3).qty.get_result_str() + "one_price:" + bnv.get(3).one_price.get_result_str());
        SaveData.SaveData(checkVerifived(bnv), path, MargeVerVar(bnv), MargeVerVarWM(), infoReader);
        //SaveDataCSV.SaveDataCSV(bnv, path, MargeVerVar(bnv), MargeVerVarWM(),infoReader);

        //create words file for Asaf
        if (CreateAnlFile) {
            createAnlFile();
        }

        return bnv;

    }

    private Vector<BNode> checkVerifived(Vector<BNode> bnv) {
        for (BNode block : bnv) {
            if (block.desCsu.equals("") && (block.descr != null && block.descr.size() > 0)) {
                for (ObData word : block.descr) {
                    word.set_isVerifived(false);
                }
            } else if (block.descr != null && block.descr.size() > 0) {
                for (ObData word : block.descr) {
                    word.set_isVerifived(true);
                }
            }
            if (block.pnCsu.equals("") && block.pn != null) {
                block.pn.set_isVerifived(false);
            } else if (block.pn != null) {
                block.pn.set_isVerifived(true);
            }
        }
        //אישרור עלויות הפריטים
        for (BNode block : bnv) {
            if ((sumrowpricesv.getVerifived() || (block.one_price != null && block.qty != null)) && block.price != null) {
                block.price.set_isVerifived(true);
                if (block.anaha != null && block.mam != null && block.fullprice != null) {
                    block.fullprice.set_isVerifived(true);
                } else if (block.fullprice != null) {
                    block.fullprice.set_isVerifived(false);
                }
            } else if (block.price != null || block.fullprice != null) {
                if (block.price != null) {
                    block.price.set_isVerifived(false);
                }
                if (block.fullprice != null) {
                    block.fullprice.set_isVerifived(false);
                }
            }
        }

        return bnv;
    }

    public Vector<BNode> fixPN(Vector<BNode> bnv) {
        if (bnv.size() > 0 && ClientPNWordsVector.size() == 1 && ClientPNWordsVector.get(0) != null) {
            int ClientPNWordsVector_line = ClientPNWordsVector.get(0).get_line_num();
            int price_line = bnv.get(0).price.get_line_num();
            if (ClientPNWordsVector_line + 1 < price_line) {
                if (lines.get(ClientPNWordsVector_line) != null && lines.get(29).size() > 0) {
                    for (ObData p : lines.get(ClientPNWordsVector_line)) {
                        if (p.get_name_by_inv().toLowerCase().equals("pn")) {
                            bnv.get(0).add_o_listPn(p);
                        }
                    }
                }
            }
        }
        return bnv;
    }

    public Vector<BNode> fixNameByInv(Vector<BNode> bnv, Vector<String> vnumdefault) {

        if (bnv.isEmpty() || bnv == null) {
            return bnv;
        }

        int end_prices = bnv.elementAt(bnv.size() - 1).price.get_line_num() - 1;

        if (bnv.size() > 0) {
            end_prices = getEndAllBlocks(bnv) + 1;
        }

        for (int i = end_prices; i < lines.size(); i++) {
            for (int p = 0; p < lines.elementAt(i).size(); p++) {
                if ((lines.elementAt(i).elementAt(p).get_name_by_inv().equals("pn") || lines.elementAt(i).elementAt(p).get_name_by_inv().equals("qty") || lines.elementAt(i).elementAt(p).get_name_by_inv().equals("one_pr") || lines.elementAt(i).elementAt(p).get_name_by_inv().equals("desc") || lines.elementAt(i).elementAt(p).get_name_by_inv().contains("price")) /*&& lines.elementAt(i).elementAt(p).get_num_block_by_inv() == -1*/) {
                    if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(p).get_format())) {
                        lines.elementAt(i).elementAt(p).set_name_by_inv("num");
                    } else {
                        lines.elementAt(i).elementAt(p).set_name_by_inv("free");
                    }
                }
                /*if(lines.elementAt(i).elementAt(p).get_name_by_inv().contains("price") && lines.elementAt(i).elementAt(p).get_num_block_by_inv() != -1 && Double.valueOf(lines.elementAt(i).elementAt(p).get_result_str()) < 0){
                 lines.elementAt(i).elementAt(p).set_name_by_inv("neg_price");
                 }*/
            }
        }
        return bnv;
    }

    private int getEndAllBlocks(Vector<BNode> bnv) {
        int numOfLastBlock = bnv.elementAt(bnv.size() - 1).price.get_num_block_by_inv();
        int endLine = bnv.elementAt(bnv.size() - 1).price.get_line_num() - 1;
        for (int i = endLine + 1; i < lines.size(); i++) {
            if (lines.elementAt(i).size() > 0) {
                if (lines.elementAt(i).elementAt(0).get_num_block_by_inv() == numOfLastBlock) {
                    endLine = i;
                } else {
                    break;
                }
            }
        }

        return endLine;
    }

    //NOT USED IN A4!!!!!
    public Vector<BNode> fixDetUsingUnitPriceCsu(Vector<BNode> bnv) {

        Vector<String[]> unitPriceLines = new Vector<String[]>();
        int priceLine = 0;
        for (BNode block : bnv) {
            unitPriceLines.removeAllElements();
            for (String[] line : unitpricecsu) {
                try {
                    //fill unitPriceLInes By Price
                    if (block.one_price != null) {
                        if (Double.valueOf(line[0]).equals(Double.valueOf(block.one_price.get_result_str()))) {
                            unitPriceLines.add(line);
                            priceLine = block.one_price.get_line_num() - 1;
                        }
                    } else {
                        if (Double.valueOf(line[0]).equals(Double.valueOf(block.price.get_result_str()))) {
                            unitPriceLines.add(line);
                            priceLine = block.price.get_line_num() - 1;
                        }
                    }
                } catch (Exception e) {

                }
            }
            boolean isSamePn = false;
            Vector<Build> susToBePn = new Vector<Build>();
            if ((block.pn != null && !isA4) || (block.listPn.size() > 0 && isA4)) {

                String[] allPN = getListOfPn(block); //get all PNs

                //search similar pn in unitPrice
                for (int i = 0; i < unitPriceLines.size(); i++) {
                    isSamePn = false;
                    for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                        for (int k = 0; k < allPN.length && !isSamePn; k++) {
                            if (LFun.checkChanges(unitPriceLines.elementAt(i)[INDEX_OF_PN.get(j)], allPN[k])) {
                                isSamePn = true;
                                break;
                            }
                        }
                    }
                    if (!isSamePn) {
                        //remove if pn is not match
                        unitPriceLines.remove(unitPriceLines.elementAt(i));
                        i--;
                    }
                }
            } else {
                //If not exists pn in block we search suspect to be pn
                Vector<String[]> afterFiltered = new Vector<String[]>();
                for (int i = priceLine; i > priceLine - 2 && i > 0; i--) {
                    for (int j = 0; j < lines.get(i).size(); j++) {
                        if (LFun.iscontain_num(lines.get(i).get(j).get_result_str()) && !lines.get(i).get(j).get_name_by_inv().contains("p")
                                && !lines.get(i).get(j).get_name_by_inv().equals("qty") && lines.get(i).get(j).get_result_str().length() >= 2) {
                            afterFiltered.addAll(filterBySusToBePn(unitPriceLines, lines.get(i).get(j).get_result_str()));
                            susToBePn.add(new Build(i, j));
                        }
                    }
                }
                unitPriceLines = afterFiltered;
            }
            if (unitPriceLines.size() > 0) {
                String[] desCsu;
                String[] blockDes = block.get_description().split(" ");
                boolean isClose = false;
                for (int i = 0; i < unitPriceLines.size(); i++) {
                    isClose = false;
                    desCsu = unitPriceLines.elementAt(i)[INDEX_OF_DESCRIPTION.elementAt(0)].split(" ");
                    //Compare item description
                    for (int j = 0; j < desCsu.length && !isClose; j++) {
                        if (desCsu[j].length() >= 3) {
                            isClose = isCloseWord(desCsu[j], blockDes);
                        }
                    }
                    if (!isClose) {
                        unitPriceLines.remove(unitPriceLines.elementAt(i));
                        i--;
                    }
                }
            }
            //If after comparing items description we get more then one line fit, we will check which of them is the most similar
            if (unitPriceLines.size() > 1) {
                String[] blockDes = block.get_description().split(" ");
                String[] mostClose = checkMostClose(blockDes, unitPriceLines);
                unitPriceLines.clear();
                if (mostClose != null) {
                    unitPriceLines.add(mostClose);
                }
            }
            if (unitPriceLines.size() == 1) {
                String pnString = "";
                for (int i = 0; i < INDEX_OF_PN.size(); i++) {
                    if (!unitPriceLines.get(0)[INDEX_OF_PN.get(i)].equals("")) {
                        pnString += unitPriceLines.get(0)[INDEX_OF_PN.get(i)];
                        if (i != INDEX_OF_PN.size() - 1) {
                            pnString += " ";
                        }
                    }
                }

                if ((block.pn == null && !isA4) || (block.listPn.size() == 0 && isA4)) {
                    for (int i = 0; i < INDEX_OF_PN.size(); i++) {
                        for (int j = susToBePn.size() - 1; j >= 0; j--) {
                            if (LFun.checkChanges(unitPriceLines.get(0)[INDEX_OF_PN.get(i)], lines.elementAt(susToBePn.get(j).i).elementAt(susToBePn.get(j).j).get_result_str())) {
                                lines.elementAt(susToBePn.get(j).i).elementAt(susToBePn.get(j).j).set_result_str(unitPriceLines.get(0)[INDEX_OF_PN.get(i)]);
                                lines.elementAt(susToBePn.get(j).i).elementAt(susToBePn.get(j).j).set_name_by_inv("pn");
                                if (!isA4) {
                                    block.set_o_pn(lines.elementAt(susToBePn.get(j).i).elementAt(susToBePn.get(j).j));
                                } else {
                                    block.add_o_listPn(lines.elementAt(susToBePn.get(j).i).elementAt(susToBePn.get(j).j));
                                }
                                susToBePn.remove(j);
                                break;
                            }
                        }
                    }
                    block.pnCsu = pnString;
                } else {
                    for (int i = 0; i < INDEX_OF_PN.size(); i++) {
                        if (isA4) {
                            for (int j = 0; j < block.listPn.size(); j++) {
                                if (LFun.checkChanges(unitPriceLines.elementAt(0)[INDEX_OF_PN.get(i)], block.listPn.elementAt(j).get_result_str())) {
                                    //block.listPn.elementAt(j).set_result_str(unitPriceLines.elementAt(0)[INDEX_OF_PN.get(i)]);
                                }
                            }
                            block.pnCsu = pnString;
                        } else {
                            block.pn.set_result_str(unitPriceLines.elementAt(0)[INDEX_OF_PN.get(0)]);
                        }
                    }
                }
                block.descr.clear();
                block.desCsu = unitPriceLines.get(0)[INDEX_OF_DESCRIPTION.elementAt(0)];
            }
        }
        return bnv;
    }

    private String[] getListOfPn(BNode block) {
        String[] allPN;

        if (isA4) {
            //if A4 - their is list of PN
            String s = block.get_listPn();
            allPN = s.split(" ");
        } else {
            allPN = new String[1];
            allPN[0] = block.pn.get_result_str();
        }

        return allPN;
    }

    public Vector<String[]> filterBySusToBePn(Vector<String[]> unitPriceLines, String susToBePn) {

        Vector<String[]> afterFilter = new Vector<String[]>();
        for (int i = 0; i < unitPriceLines.size(); i++) {
            for (int j = 0; j < INDEX_OF_PN.size(); j++) {
                if (LFun.checkChanges(unitPriceLines.get(i)[INDEX_OF_PN.get(j)], susToBePn)) {
                    afterFilter.add(unitPriceLines.get(i));
                    //unitPriceLines.remove(i);
                    break;
                }
            }
        }

        return afterFilter;
    }

    public String[] checkMostClose(String[] blockDes, Vector<String[]> unitPriceLines) {
        String[] mostClose = null;
        int counter = 0;
        int max = -1;
        String[] desCsu;
        for (int i = 0; i < unitPriceLines.size(); i++) {
            desCsu = unitPriceLines.get(i)[MatchesToOneItem.findFirstIndexNotEmpty(unitPriceLines.get(i), INDEX_OF_DESCRIPTION)].split(" ");
            counter = 0;
            for (int j = 0; j < desCsu.length; j++) {
                for (int k = 0; k < blockDes.length; k++) {
                    if (LFun.checkChanges(desCsu[j], blockDes[k])) {
                        counter++;
                        break;
                    }
                }
            }
            if (counter != 0 && counter > max) {
                mostClose = unitPriceLines.get(i);
                max = counter;
            }
        }
        return mostClose;
    }

    public boolean isCloseWord(String word, String[] blockDes) {
        boolean isClose = false;
        for (int i = 0; i < blockDes.length && !isClose; i++) {
            if (word.length() == blockDes[i].length()) {
                isClose = LFun.checkChanges(word, blockDes[i]);
            }
        }
        return isClose;
    }

    public Vector<BNode> fixQtyAndPrices(Vector<BNode> bnv, Vector<String> vnumdefault) {

        double price = 0;
        double priceBnv = 0;
        for (int i = 0; i < bnv.size(); i++) {
            if (bnv.get(i).qty != null && bnv.get(i).one_price != null) {
                price = Double.valueOf(bnv.get(i).qty.get_result_str()) * Double.valueOf(bnv.get(i).one_price.get_result_str());
                if (bnv.get(i).price != null) {
                    priceBnv = Double.valueOf(bnv.get(i).price.get_result_str());
                } else if (bnv.get(i).price_z != null) {
                    priceBnv = Double.valueOf(bnv.get(i).price_z.get_result_str());
                }

                if (!checktoler(price, priceBnv, 0.001)) {
                    for (int j = i - 1; j <= i + 1 && j < bnv.size(); j++) {
                        if (j == -1) {
                            j++;
                        }
                        if (bnv.get(j).price != null) {
                            priceBnv = Double.valueOf(bnv.get(j).price.get_result_str());
                        } else if (bnv.get(j).price_z != null) {
                            priceBnv = Double.valueOf(bnv.get(j).price_z.get_result_str());
                        }
                        if (checktoler(price, priceBnv, 0.01)) {
                            if (bnv.get(j).qty == null) {
                                bnv.get(j).qty = new ObData(bnv.get(i).qty);
                            }
                            if (bnv.get(j).one_price == null) {
                                bnv.get(j).one_price = new ObData(bnv.get(i).one_price);
                            }
                        }
                    }

                    try {

                        if (LFun.contain_type(vnumdefault, lines.elementAt(bnv.get(i).qty.get_line_num() - 1).elementAt(bnv.get(i).qty.get_inx_renum() - 1).get_format())) {
                            lines.elementAt(bnv.get(i).qty.get_line_num() - 1).elementAt(bnv.get(i).qty.get_inx_renum() - 1).set_name_by_inv("num");
                        } else {
                            lines.elementAt(bnv.get(i).qty.get_line_num() - 1).elementAt(bnv.get(i).qty.get_inx_renum() - 1).set_name_by_inv("free");
                        }
                        if (LFun.contain_type(vnumdefault, lines.elementAt(bnv.get(i).one_price.get_line_num() - 1).elementAt(bnv.get(i).one_price.get_inx_renum() - 1).get_format())) {
                            lines.elementAt(bnv.get(i).one_price.get_line_num() - 1).elementAt(bnv.get(i).one_price.get_inx_renum() - 1).set_name_by_inv("num");
                        } else {
                            lines.elementAt(bnv.get(i).one_price.get_line_num() - 1).elementAt(bnv.get(i).one_price.get_inx_renum() - 1).set_name_by_inv("free");
                        }

                        bnv.get(i).qty = null;
                        bnv.get(i).one_price = null;
                    } catch (Exception e) {

                    }
                }
            }
        }
        return bnv;
    }

    public void checkSumOfBnv(Vector<BNode> bnv, boolean A4) {

        if (!sumallWithMamv.getVerifived()) {
            boolean stop = false;
            double sumAll = Double.valueOf(LFun.fixdot(String.valueOf(sumAllItems(bnv))));
            sumAll = Double.parseDouble(new DecimalFormat("##.##").format(sumAll));
            //start from lines.size until the middle of the "bnv.size.lineofprice"
            for (int p = bnv.get(bnv.size() / 2).price.get_line_num(); p < lines.size() && !stop; p++) {
                for (int y = 0; y < lines.get(p).size() && !stop; y++) {
                    try {
                        double a = Double.valueOf(LFun.fixdot(lines.get(p).get(y).get_result_str()));
                        if (sumAll == a) {
                            if (sumallWithMamv.getSum() == sumallNoMamv.getSum()) {
                                sumallNoMamv.setCancel(true);
                                sumallMamv.setCancel(true);
                            }
                            sumallWithMamv.setSum(a);
                            sumallWithMamv.setVerifived(true);
                            lines.elementAt(p).elementAt(y).set_STR_MEANING("TOTALSUM");

                            stop = true;
                        }
                    } catch (Exception e) {

                    }
                }
            }
        }
    }

    public void changePricesByItemsCsv(Vector<BNode> bnv, boolean A4) {

        boolean isSamePricePerOne = false;
        double priceFromCsv = 0;

        if (makatcsu != null) {
            boolean isSamePn = false;
            for (int j = 0; j < bnv.size() && !sumallWithMamv.getVerifived(); j++) {
                for (int i = 0; i < makatcsu.size() && !sumallWithMamv.getVerifived(); i++) {
                    try {
                        //Search for item line in MAKAT.csv (that roni create) by comparing pn
                        if (isA4) {
                            //if it`s A4 the pn is in list
                            for (int k = 0; k < bnv.get(j).listPn.size() && !isSamePn; k++) {
                                isSamePn = bnv.get(j).listPn.elementAt(k).get_result_str().contains(makatcsu.get(i)[0]);
                            }
                        } else {
                            isSamePn = bnv.get(j).pn.get_result_str().contains(makatcsu.get(i)[0]);
                        }

                        if (isSamePn && LFun.checkDeviation(bnv.get(j).pn.get_right(), Integer.valueOf(makatcsu.get(i)[7]), 50) && LFun.checkDeviation(bnv.get(j).pn.get_left(), Integer.valueOf(makatcsu.get(i)[5]), 50)
                                && LFun.checkDeviation(bnv.get(j).pn.get_top(), Integer.valueOf(makatcsu.get(i)[6]), 50) && LFun.checkDeviation(bnv.get(j).pn.get_bottom(), Integer.valueOf(makatcsu.get(i)[8]), 50)) {

                            if (bnv.get(j).one_price != null) {
                                isSamePricePerOne = checktoler(Double.valueOf(makatcsu.get(i)[INDEX_OF_PRICE.elementAt(0)]), Double.valueOf(bnv.get(j).one_price.get_result_str()), 0.01);
                            } else {
                                isSamePricePerOne = checktoler(Double.valueOf(makatcsu.get(i)[INDEX_OF_PRICE.elementAt(0)]), Double.valueOf(bnv.get(j).price.get_result_str()), 0.01);
                            }

                            /* If the price per one in makat.csv isn`t match to the price per one we found we will check if the price per one
                             we found is can be fixed by the CSV */
                            if (!isSamePricePerOne) {
                                priceFromCsv = Double.valueOf(makatcsu.get(i)[INDEX_OF_PRICE.elementAt(0)]);
                                checkPriceFromItemsCsv(bnv, j, priceFromCsv, A4);
                            }
                        }
                    } catch (Exception e) {
                        //System.out.println("index:" + i);
                    }
                }
            }
        }

        isSamePricePerOne = false;
        priceFromCsv = 0;

        if (itemdesccsu != null) {

            boolean isSameDes = false;
            for (int j = 0; j < bnv.size() && !sumallWithMamv.getVerifived(); j++) {
                for (int i = 0; i < itemdesccsu.size() && !sumallWithMamv.getVerifived(); i++) {
                    try {
                        //Search for item line in ITEM_DESCRIPTION.csv (that roni create) by comparing pn
                        isSameDes = bnv.get(j).get_description().contains(itemdesccsu.get(i)[0]);
                    } catch (Exception e) {
                        //System.out.println("index:" + i);
                    }
                    if (isSameDes && bnv.get(j).descr.get(0).get_left() <= Integer.valueOf(itemdesccsu.get(i)[5])
                            && bnv.get(j).descr.get(bnv.get(j).descr.size() - 1).get_right() >= Integer.valueOf(itemdesccsu.get(i)[7])) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(itemdesccsu.get(i)[INDEX_OF_PRICE.elementAt(0)]), Double.valueOf(bnv.get(j).one_price.get_result_str()), 0.01);
                            /* If the price per one in ITEM_DESCRIPTION.csv isn`t match to the price per one we found we will check if the price per one
                             we found is can be fixed by the CSV */
                            if (!isSamePricePerOne) {
                                try {
                                    priceFromCsv = Double.valueOf(itemdesccsu.get(i)[INDEX_OF_PRICE.elementAt(0)]);
                                    checkPriceFromItemsCsv(bnv, j, priceFromCsv, A4);
                                } catch (Exception e) {
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }
    }

    public boolean checkmidDeviatuin(ObData number, ObData numberToCheck, double deviation) {
        double midMax = (number.get_right() - number.get_left()) / 2 + number.get_left() + deviation;
        double midMin = (number.get_right() - number.get_left()) / 2 + number.get_left() - deviation;
        double midCurrent = (numberToCheck.get_right() - number.get_left()) / 2 + numberToCheck.get_left();
        if (midMax > midCurrent && midMin < midCurrent) {
            return true;
        }
        return false;
    }

    public Vector<String> addDigit(String number_string, Vector<String> priceVector) {
        String temp = number_string;
        for (int i = 0; i < 10; i++) {
            priceVector.add(temp + i);
            priceVector.add(i + temp);

        }
        return priceVector;
    }

    public void checkPriceFromItemsCsv(Vector<BNode> bnv, int indexOfPnInBnv, double priceFromCsv, boolean A4) {

        try {
            Vector<String> totalPriceVector = createSimilarVectorWithLimit(bnv.get(indexOfPnInBnv).price.get_result_str(), 2);
            Vector<String> priceVectorPerOne = null;
            try {
                priceVectorPerOne = createSimilarVectorWithLimit(bnv.get(indexOfPnInBnv).one_price.get_result_str(), 2);
            } catch (Exception e) {

            }

            Vector<BNode> copy;
            String qtyString = null;
            int qty;

            if (bnv.get(indexOfPnInBnv).qty == null) {
                qty = 1;
            } else {
                qtyString = bnv.get(indexOfPnInBnv).qty.get_result_str();
                qty = Integer.valueOf(qtyString);
                if (qty < 0) {
                    qty *= (-1);
                }
            }

            double pricePerOne = 0;
            double differnce = priceFromCsv * PRECENT_FOR_ITEMS_CSV;
            double price = 0;
            double anaha = 0;

            if (priceVectorPerOne != null) {
                for (int i = 0; i < priceVectorPerOne.size(); i++) {
                    try {
                        pricePerOne = Double.valueOf(priceVectorPerOne.get(i));
                    } catch (Exception e) {
                    }
                    //less then the precent that in the config.ini 
                    if (pricePerOne < priceFromCsv + differnce && pricePerOne > priceFromCsv - differnce) {
                        copy = copyBnv(bnv);
                        if (!bnv.get(indexOfPnInBnv).anaha.equals("0")) {
                            try {
                                anaha = convert(bnv.get(indexOfPnInBnv).anaha);
                                if (anaha > 0) {
                                    anaha = anaha * (-1);
                                }
                            } catch (Exception r) {
                            }
                        }
                        price = qty * pricePerOne + anaha;

                        if (checkCorrection(price, totalPriceVector)) {
                            copy.get(indexOfPnInBnv).price.set_result_str(String.valueOf(price));
                            checkSumOfBnv(copy, A4);
                            if (sumallWithMamv.getVerifived()) {
                                bnv.get(indexOfPnInBnv).price.set_result_str(String.valueOf(price));
                                bnv.get(indexOfPnInBnv).one_price.set_result_str(String.valueOf(pricePerOne));
                                break;
                            }
                        }
                    }
                }
            } else {
                for (int i = 0; i < totalPriceVector.size(); i++) {
                    try {
                        pricePerOne = Double.valueOf(totalPriceVector.get(i));
                    } catch (Exception e) {
                    }
                    if (pricePerOne < priceFromCsv + differnce && pricePerOne > priceFromCsv - differnce) {
                        copy = copyBnv(bnv);
                        if (!bnv.get(indexOfPnInBnv).anaha.equals("0")) {
                            try {
                                anaha = convert(bnv.get(indexOfPnInBnv).anaha);
                                if (anaha > 0) {
                                    anaha = anaha * (-1);
                                }
                            } catch (Exception r) {
                            }
                        }
                        price = qty * pricePerOne + anaha;
                        copy.get(indexOfPnInBnv).price.set_result_str(String.valueOf(price));
                        checkSumOfBnv(copy, A4);
                        if (sumallWithMamv.getVerifived()) {
                            bnv.get(indexOfPnInBnv).price.set_result_str(String.valueOf(price));
                            break;
                        }
                    }

                }
            }

        } catch (Exception e) {
        }
    }

    /**
     * check if the price is in the price vector
     *
     * @param price
     * @param priceVector
     * @return
     */
    public boolean checkCorrection(double price, Vector<String> priceVector) {

        boolean isGoodCorrection = false;
        double priceFromVector = 0;

        for (int i = 0; i < priceVector.size(); i++) {
            try {
                priceFromVector = Double.valueOf(priceVector.get(i));
                if (checktoler(price, priceFromVector, tolerance) && priceFromVector != 0) {
                    isGoodCorrection = true;
                    break;
                }
            } catch (Exception e) {

            }
        }
        return isGoodCorrection;
    }

    /**
     *
     * @return vector of ITEMS.csv by lines and split it by dilimiters ","
     */
    public Vector<Vector<String>> descCsv() {

        String descFile = Pathes.Path.getbill_in() + "\\" + Management.SConsole.current_file_bill_in.getName().replaceAll(".csv", "") + ";ITEM_DESCRIPTION.csv";

        // String makatFile = Pathes.Path.bill_in + "\\" + Management.SConsole.current_file_bill_in.getName().replaceAll(".csv", "") + ";MAKAT.csv";
        //String pathOfItemCsv = Pathes.Path.csv_final + "\\ITEMS.csv";
        Vector<String> descCsvVectorWithoutDelimiter = LFun.file2vector3(descFile);
        String withSpaces = "";
        if (descCsvVectorWithoutDelimiter != null) {
            for (int i = 0; i < descCsvVectorWithoutDelimiter.size(); i++) {
                withSpaces = descCsvVectorWithoutDelimiter.elementAt(i).replaceAll(",,", ", ,");
                withSpaces = withSpaces.replaceAll(",,", ", ,");
                descCsvVectorWithoutDelimiter.set(i, withSpaces);
            }

            Vector<Vector<String>> descCsvVectorByLinesWithDelimiter = new Vector<Vector<String>>();
            Vector<String> temp = new Vector<String>();

            for (int i = 0; i < descCsvVectorWithoutDelimiter.size(); i++) {
                temp = LFun.getElements(",", descCsvVectorWithoutDelimiter.get(i));
                descCsvVectorByLinesWithDelimiter.add(temp);
            }
            return descCsvVectorByLinesWithDelimiter;
        }
        return null;
    }

    public Vector<Vector<String>> makatCsv() {

        String makatFile = Pathes.Path.getbill_in() + "\\" + Management.SConsole.current_file_bill_in.getName().replaceAll(".csv", "") + ";MAKAT.csv";

        //String pathOfItemCsv = Pathes.Path.csv_final + "\\ITEMS.csv";
        Vector<String> makatCsvVectorWithoutDelimiter = LFun.file2vector3(makatFile);
        Vector<Vector<String>> makatCsvVectorByLinesWithDelimiter = new Vector<Vector<String>>();
        Vector<String> temp = new Vector<String>();

        String withSpaces = "";
        if (makatCsvVectorWithoutDelimiter != null) {
            for (int i = 0; i < makatCsvVectorWithoutDelimiter.size(); i++) {
                withSpaces = makatCsvVectorWithoutDelimiter.elementAt(i).replaceAll(",,", ", ,");
                withSpaces = withSpaces.replaceAll(",,", ", ,");
                makatCsvVectorWithoutDelimiter.set(i, withSpaces);
            }

            for (int i = 0; i < makatCsvVectorWithoutDelimiter.size(); i++) {
                temp = LFun.getElements(",", makatCsvVectorWithoutDelimiter.get(i));
                makatCsvVectorByLinesWithDelimiter.add(temp);
            }
            return makatCsvVectorByLinesWithDelimiter;
        }
        return null;
    }

    public Vector<BNode> fil_anahadata(Vector<BNode> bnv) {
        //need check descr.v

        double n = 0;
        double x = 0;
        double y = 0;
        boolean f = false;

        /**
         * get the number values from the unknown descr and put them in un_num
         * Vector. descr-a vector contain all the obdata that connected\around
         * to the price
         *
         */
        for (BNode bNode : bnv) {
            for (int i = 0; i < bNode.descr.size(); i++) {
                String check = bNode.descr.elementAt(i).get_result_str();
                check = check.replaceAll("%", "");
                try {
                    Double.parseDouble(LFun.fixdot(check));
                    bNode.un_num.add(bNode.descr.elementAt(i));
                    bNode.descr.remove(i--);
                    bNode.un_num.lastElement().set_name_by_inv("num");
                    bNode.un_num.lastElement().set_result_str(check);
                } catch (Exception e) {
                }

            }
            f = false;

            try {
                n = Double.parseDouble(LFun.fixdot(bNode.price.get_result_str()));
            } catch (Exception e) {
                n = 0;
                bNode.price.set_result_str("@@@");

            }
            for (int i = 0; i < bNode.un_num.size() && !f; i++) {
                ObData o = bNode.un_num.elementAt(i);

                try {
                    x = Double.parseDouble(LFun.fixdot(bNode.un_num.elementAt(i).get_result_str()));
                } catch (Exception e) {
                    x = 0;
                }

                if (n < x && x != 0) {
                    y = 100 - (n / (x / 100));
                    y = Double.parseDouble(new DecimalFormat("##.##").format(y));
                    for (int j = 0; j < bNode.un_num.size() && !f; j++) {
                        double t = -1;
                        if (j != i) {
                            try {
                                t = Double.parseDouble(LFun.fixdot(bNode.un_num.elementAt(j).get_result_str()));
                            } catch (Exception e) {
                            }
                            if (t > -1 && t <= n && checktoler(y, t, tolerance)) {
                                f = true;
                                bNode.anaha_by_percent = "" + y + "%";
                                bNode.anaha = "" + Double.parseDouble(new DecimalFormat("##.##").format(x - n));
                                o.set_name_by_inv("fullsum");
                                bNode.set_o_fullprice(o);//""+x;
                                bNode.un_num.remove(j);
                                bNode.un_num.remove(o);
                            }
                        }
                    }
                }

            }
        }

        if (bnv.size() > 3) {
            try {
                Double valBeforeLast = Double.valueOf(bnv.get(bnv.size() - 2).price.get_result_str());
                Double valLast = Double.valueOf(bnv.get(bnv.size() - 1).price.get_result_str());
                if (valLast < 0.51 && valBeforeLast > 10) {
                    write(p_filename, "fil_anahadata()", "", "Delete the last bnv (bnv[last]=" + valLast + ")", true);
                    bnv.remove(bnv.size() - 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return bnv;
    }

    /**
     * return index of makat in line
     *
     * @param vLine
     * @return
     */
    public int getMakatIndex(Vector<ObData> vLine) {

        for (int i = 0; i < vLine.size(); i++) {
            if (vLine.elementAt(i).get_external_source().equalsIgnoreCase("makat")) {
                return i;
            }

        }

        return -1;
    }

    public boolean checktoler(double p1, double p2, double tol) {

        double t = Math.abs(Math.abs(Math.max(p1, p2) / Math.min(p1, p2) - 1));
        if (t <= tol && ((p1 < 0 && p2 < 0) || (p1 > 0 && p2 > 0))) {
            return true;
        }
        return false;
    }

    // correct qty & one_price by dictonary header
    void correct_qty_one_price(int linenum) {

        int startsearchHeader = -1;

        searchstartHeader:
        for (int j = 0; j < od.size(); j++) {
            if (od.elementAt(j).get_line_num() == linenum) {
                startsearchHeader = j;
                break searchstartHeader;
            }

        }

        searchQTY:
        for (int j = 0; j < od.size(); j++) {
            int temp = getHeaderCenterByDictIndex(1, od.elementAt(j));//1 search coordinate of word qty in header
            if (temp > 0) {
                centerQTY = temp;
                break searchQTY;
            }

        }

        searchOnePrice:
        for (int j = 0; j < od.size(); j++) {
            int temp = getHeaderCenterByDictIndex(0, od.elementAt(j));//0 search coordinate of word one_price in header
            if (temp > 0) {
                centerOnePrice = temp;
                break searchOnePrice;
            }

        }

    }

    void fil_pr_qty_an_data(int i) {

        Vector<String> vnumdefault = new Vector<String>();

        vnumdefault.add("USADD");
        vnumdefault.add("EURDD");
        vnumdefault.add("USADDN");
        vnumdefault.add("EURDDN");
        vnumdefault.add("USADDD");
        vnumdefault.add("EURDDD");
        vnumdefault.add("USADDDN");
        vnumdefault.add("EURDDDN");
        vnumdefault.add("INTP");
        vnumdefault.add("PERCENT");

        Vector<PrQtyAn> pqa = new Vector<PrQtyAn>();
        double p = 0;
        Vector<ObData> b = lines.elementAt(i);

        System.out.println("fil_pr_qty_an_data");

        //qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
        try {

            //if qty and one_pr already found, only for change qty->one_pr or one_pr->qty
            if (isrowhasNameByInv(b, "qty") && isrowhasNameByInv(b, "one_pr")) {
                for (int k = 0; k < b.size(); k++) {
                    if (b.elementAt(k).get_name_by_inv().contains("pos_price")) {
                        p = Double.parseDouble(LFun.fixdot(b.elementAt(k).get_result_str()));
                    }

                    if (b.elementAt(k).get_name_by_inv().contains("qty")
                            || b.elementAt(k).get_name_by_inv().contains("one")) {

                        if (LFun.contain_type(vnumdefault, b.elementAt(k).get_format())) {
                            System.out.println(b.elementAt(k).get_name_by_inv() + " : " + b.elementAt(k).get_result_str() + " : " + b.elementAt(k).get_format());
                            PrQtyAn tPQA = new PrQtyAn(b.elementAt(k).get_result_str(), k);
                            pqa.add(tPQA);
                        }

                    }

                }

                int countnum = 0;
                for (int k = 0; k < b.size(); k++) {

                    if (b.elementAt(k).get_name_by_inv().contains("qty")
                            || b.elementAt(k).get_name_by_inv().contains("one")
                            || b.elementAt(k).get_name_by_inv().contains("num")) {

                        if (LFun.contain_type(vnumdefault, b.elementAt(k).get_format())) {
                            countnum++;
                        }

                    }
                }

                if (checktoler(p, pqa.elementAt(0).val * pqa.elementAt(1).val, tolerance)
                        && (pqa.elementAt(0).val != 50 && pqa.elementAt(1).val != 50 && countnum > 2) || (countnum == 2)) {
                    boolean correct = false;
                    //compare with qty center
                    for (int k = 0; k < pqa.size() - 1; k++) {
                        for (int j = k; j < pqa.size() - 1; j++) {
                            if (centerQTY > 0) {
                                int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                if (Math.abs(centerQTY - c1) < Math.abs(centerQTY - c2)) {
                                    b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                    b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                    correct = true;
                                    System.out.println("corrected qty for " + p);
                                    return;
                                } else if (Math.abs(centerQTY - c2) < Math.abs(centerQTY - c1)) {
                                    b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                    b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                    correct = true;
                                    System.out.println("corrected qty for " + p);
                                    return;
                                }
                            }
                            //compare with one_price center
                            if (centerOnePrice > 0 && !correct) {
                                int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                if (Math.abs(centerOnePrice - c1) < Math.abs(centerOnePrice - c2)) {
                                    b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                    b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                    correct = true;
                                    System.out.println("corrected one_price for " + p);
                                    return;
                                } else if (Math.abs(centerOnePrice - c2) < Math.abs(centerOnePrice - c1)) {
                                    b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                    b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                    correct = true;

                                    System.out.println("corrected one_price for " + p);
                                    return;
                                }
                            }
                            //end compares
                        }
                    }
                    return;
                }
            }

            pqa.removeAllElements();

            for (int k = 0; k < b.size(); k++) {
                if (b.elementAt(k).get_name_by_inv().contains("pos_price")) {
                    p = Double.parseDouble(LFun.fixdot(b.elementAt(k).get_result_str()));
                }

                if (b.elementAt(k).get_name_by_inv().contains("qty")
                        || b.elementAt(k).get_name_by_inv().contains("one")
                        // || b.elementAt(k).get_name_by_inv().contains("percent")
                        // || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("desc")
                        || b.elementAt(k).get_name_by_inv().contains("num")) {

                    if (LFun.contain_type(vnumdefault, b.elementAt(k).get_format())) {
                        System.out.println(b.elementAt(k).get_name_by_inv() + " : " + b.elementAt(k).get_result_str() + " : " + b.elementAt(k).get_format());
                        PrQtyAn tPQA = new PrQtyAn(b.elementAt(k).get_result_str(), k);
                        pqa.add(tPQA);
                    }

                }
            }

            // if(pqa.size()<3){
            //    return;
            //}
            boolean correct = false;

            stopcheck:
            {
                for (int z = 0; z < pqa.size(); z++) {

                    for (int k = 1; k < pqa.size() - 1; k++) {
                        for (int j = k; j < pqa.size() - 1; j++) {
                            double comp_d = pqa.elementAt(j).val * pqa.elementAt(j + 1).val;
                            double oneperc = comp_d / 100;
                            correct = false;
                            if (comp_d > 0 && checktoler(comp_d - p, pqa.elementAt(0).val, tolerance)) {
                                //number
                                //compare with qty center

                                if (centerQTY > 0) {
                                    int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                    int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                    if (Math.abs(centerQTY - c1) < Math.abs(centerQTY - c2)) {
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                        correct = true;
                                        System.out.println("corrected qty for " + p);
                                    } else if (Math.abs(centerQTY - c2) < Math.abs(centerQTY - c1)) {
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                        correct = true;
                                        System.out.println("corrected qty for " + p);
                                    }
                                }
                                //compare with one_price center
                                if (centerOnePrice > 0 && !correct) {
                                    int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                    int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                    if (Math.abs(centerOnePrice - c1) < Math.abs(centerOnePrice - c2)) {
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                        correct = true;
                                        System.out.println("corrected one_price for " + p);
                                    } else if (Math.abs(centerOnePrice - c2) < Math.abs(centerOnePrice - c1)) {
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                        correct = true;
                                        System.out.println("corrected one_price for " + p);
                                    }
                                }
                                //end compares

                                b.elementAt(pqa.elementAt(0).pos).set_name_by_inv("anaha");
                                System.out.println("found anaha for " + p);
                                break stopcheck;
                            } else if (comp_d > 0 && checktoler(comp_d - p, pqa.elementAt(0).val * oneperc, tolerance)) {
                                //number

                                //compare with qty center
                                if (centerQTY > 0) {
                                    int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                    int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                    if (Math.abs(centerQTY - c1) < Math.abs(centerQTY - c2)) {
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                        correct = true;
                                        System.out.println("corrected qty for " + p);
                                    } else if (Math.abs(centerQTY - c2) < Math.abs(centerQTY - c1)) {
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_price");
                                        correct = true;
                                        System.out.println("corrected qty for " + p);
                                    }
                                }
                                //compare with one_price center
                                if (centerOnePrice > 0 && !correct) {
                                    int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                    int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                    if (Math.abs(centerOnePrice - c1) < Math.abs(centerOnePrice - c2)) {
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                        correct = true;
                                        System.out.println("corrected one_price for " + p);
                                    } else if (Math.abs(centerOnePrice - c2) < Math.abs(centerOnePrice - c1)) {
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                        correct = true;
                                        System.out.println("corrected one_price for " + p);
                                    }
                                }
                                //end compares

                                b.elementAt(pqa.elementAt(0).pos).set_name_by_inv("anaha_by_percent");
                                System.out.println("found percent anaha for " + p);
                                break stopcheck;
                            }
                        }

                    }

                    pqa.add(pqa.elementAt(0));
                    pqa.removeElementAt(0);
                }

                //check for two numbers
                if (!correct) {
                    for (int k = 0; k < pqa.size() - 1; k++) {
                        for (int j = k; j < pqa.size() - 1; j++) {
                            double comp_d = pqa.elementAt(j).val * pqa.elementAt(j + 1).val;

                            correct = false;
                            if (comp_d > 0 && checktoler(comp_d, p, tolerance)) {
                                //number
                                //compare with qty center

                                if (centerQTY > 0) {
                                    int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                    int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                    if (Math.abs(centerQTY - c1) < Math.abs(centerQTY - c2)) {
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                        correct = true;
                                        System.out.println("corrected qty for " + p);
                                    } else if (Math.abs(centerQTY - c2) < Math.abs(centerQTY - c1)) {
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                        correct = true;
                                        System.out.println("corrected qty for " + p);
                                    }
                                }
                                //compare with one_price center
                                if (centerOnePrice > 0 && !correct) {
                                    int c1 = (int) ((b.elementAt(pqa.elementAt(j).pos).get_left() + b.elementAt(pqa.elementAt(j).pos).get_right()) / 2);
                                    int c2 = (int) ((b.elementAt(pqa.elementAt(j + 1).pos).get_left() + b.elementAt(pqa.elementAt(j + 1).pos).get_right()) / 2);
                                    if (Math.abs(centerOnePrice - c1) < Math.abs(centerOnePrice - c2)) {
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("one_pr");
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("qty");
                                        correct = true;
                                        System.out.println("corrected one_price for " + p);
                                    } else if (Math.abs(centerOnePrice - c2) < Math.abs(centerOnePrice - c1)) {
                                        b.elementAt(pqa.elementAt(j + 1).pos).set_name_by_inv("one_pr");
                                        b.elementAt(pqa.elementAt(j).pos).set_name_by_inv("qty");
                                        correct = true;
                                        System.out.println("corrected one_price for " + p);
                                    }
                                }
                                //end compares

                                break stopcheck;
                            }

                        }

                    }
                }

            }
        } catch (Exception e) {

        }

    }

    /**
     * fil percent mam from list
     *
     * @param bnv
     * @return
     */
    public Vector<BNode> filPercentMamByList(Vector<BNode> bnv) {
        for (BNode block : bnv) {
            boolean f = false;
            if (block.pn != null || !block.listPn.isEmpty()) {
                String[] check = null;
                boolean stop = false;
                for (int i = 0; i < INDEX_OF_PN.size() && !stop; i++) {
                    if (!isA4) {
                        check = findInMakatCSU(block.pn.get_result_str(), INDEX_OF_PN.elementAt(i));
                        if (check != null && !check[colMamPercent].equals("")) {
                            stop = true;
                        }
                    } else {
                        String[] allPN = block.get_listPn().split(" ");
                        for (int j = 0; j < allPN.length && !stop; j++) {
                            check = findInMakatCSU(allPN[j], INDEX_OF_PN.elementAt(i));
                            if (check != null && !check[colMamPercent].equals("")) {
                                stop = true;
                            }
                        }
                    }

                }
                if (check != null && !check[colMamPercent].equals("")) {
                    if (block.percent == null) {
                        block.percent = new ObData();
                    }
                    block.percent.set_result_str(check[colMamPercent] + "%");
                    block.percent.set_external_source("MAKAT");
                    block.percent.set_name_by_inv("percent");

                    f = true;

                    System.out.println("percent by MAKAT " + block.percent.get_result_str() + " for price " + block.price.get_result_str());
                }
            }

            if (!f && block.descr != null) {
                Vector<String[]> match = matchToDescriptionCsu(block);
                String[] check = null;
                if (match.size() > 1) {
                    check = checkMostClose(block.get_description().split(" "), match);
                } else if (match.size() == 1) {
                    check = match.elementAt(0);
                }
                if (check != null && !check[colMamPercent].equals("")) {
                    if (block.percent == null) {
                        block.percent = new ObData();
                    }
                    block.percent.set_result_str(check[colMamPercent] + "%");
                    block.percent.set_external_source("ITEMDESC");
                    block.percent.set_name_by_inv("percent");
                    f = true;
                    System.out.println("percent by ITEMDESC " + block.percent.get_result_str() + " for price " + block.price.get_result_str());
                }
            }
        }
        return bnv;
    }

    /**
     * check fil rows by mam percents 1 full rows 0 has empty rows
     *
     * @param bnv
     * @return
     */
    public int checkMamRows(Vector<BNode> bnv) {

        for (int i = 0; i < bnv.size(); i++) {
            BNode elementAt = bnv.elementAt(i);
            if (elementAt.percent == null) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * fill mam data
     *
     * @param bnv
     * @return
     */
    public Vector<BNode> filExistMamData(Vector<BNode> bnv) {

        //לבדוק מה אמור לתת לי הקוד הזה!!!!
        /*BNode bn = new BNode();
         ObData o = new ObData();
         try {
         o.set_result_str("" + (shipmentv.getSum() - Math.abs(globalanahav.getSum())));
         o.set_line_num(bnv.lastElement().price.get_line_num());
         bn.set_o_price(o);

         bnv.add(bn);
         } catch (Exception r) {
         }
         if (sumallMamv.getSum() == 0) {
         //if geted mam 0%
         for (BNode bNode : bnv) {
         try {
         bNode.mam = new ObData();
         bNode.mam.set_name_by_inv("mam");
         bNode.mam.set_result_str("0");

         bNode.without_mam = new ObData();
         bNode.without_mam.set_name_by_inv("without_mam");
         bNode.without_mam.set_result_str(bNode.price.get_result_str());

         bNode.with_mam = new ObData();
         bNode.with_mam.set_name_by_inv("with_mam");
         bNode.with_mam.set_result_str(bNode.price.get_result_str());

         bNode.percent = new ObData();
         bNode.percent.set_name_by_inv("percent");
         bNode.percent.set_result_str("0%");
         } catch (Exception r) {
         }
         }

         sumallMamv.setVerifived(true);
         shipmentv.setVerifived(true);
         rowPrConMam = true;

         bnv.removeElementAt(bnv.size() - 1);
         return bnv;
         } */
        int linenum = bnv.lastElement().price.get_line_num() + 1;
        int checkStatus = checkMamRows(bnv);
        double sumMamPrContainMam = 0;
        double sumMamPrNotContainMam = 0;
        if (checkStatus == 1) {
            //bnv.lastElement().percent = new ObData();
            //bnv.lastElement().percent.set_name_by_inv("percent");
            //bnv.lastElement().percent.set_result_str("17%");

            for (BNode bNode : bnv) {

                double mampercentrow = Double.parseDouble(LFun.fixdot(bNode.percent.get_result_str().replaceAll("%", "")));
                mampercentrow = Double.parseDouble(new DecimalFormat("##.##").format(mampercentrow));

                double pricerow = Double.parseDouble(LFun.fixdot(bNode.price.get_result_str()));
                pricerow = Double.parseDouble(new DecimalFormat("##.##").format(pricerow));

                sumMamPrContainMam = sumMamPrContainMam + ((pricerow * mampercentrow) / (100 + mampercentrow));
                sumMamPrContainMam = Double.parseDouble(new DecimalFormat("##.##").format(sumMamPrContainMam));

                sumMamPrNotContainMam = sumMamPrNotContainMam + (((pricerow * mampercentrow) / (100)));
                sumMamPrNotContainMam = Double.parseDouble(new DecimalFormat("##.##").format(sumMamPrNotContainMam));

            }

            if (verifyMam(sumMamPrContainMam, sumMamPrNotContainMam, linenum)) {
                sumallMamv.setVerifived(true);

                for (BNode bNode : bnv) {

                    double mampercentrow = Double.parseDouble(LFun.fixdot(bNode.percent.get_result_str().replaceAll("%", "")));
                    mampercentrow = Double.parseDouble(new DecimalFormat("##.##").format(mampercentrow));

                    double pricerow = Double.parseDouble(LFun.fixdot(bNode.price.get_result_str()));
                    pricerow = Double.parseDouble(new DecimalFormat("##.##").format(pricerow));

                    double mamVal = 0;
                    double prWithoutMamVal = 0;
                    double prWithMamVal = 0;

                    if (rowPrConMam) {
                        mamVal = ((pricerow * mampercentrow) / (100 + mampercentrow));
                        prWithoutMamVal = pricerow - mamVal;
                        prWithMamVal = pricerow;
                    } else {
                        mamVal = ((pricerow * mampercentrow) / (100));
                        prWithoutMamVal = pricerow;
                        prWithMamVal = pricerow + mamVal;
                    }

                    mamVal = Double.parseDouble(new DecimalFormat("##.##").format(mamVal));
                    prWithoutMamVal = Double.parseDouble(new DecimalFormat("##.##").format(prWithoutMamVal));
                    prWithMamVal = Double.parseDouble(new DecimalFormat("##.##").format(prWithMamVal));

                    bNode.mam = new ObData();
                    bNode.mam.set_name_by_inv("mam");
                    bNode.mam.set_result_str("" + mamVal);

                    bNode.without_mam = new ObData();
                    bNode.without_mam.set_name_by_inv("without_mam");
                    bNode.without_mam.set_result_str("" + prWithoutMamVal);

                    bNode.with_mam = new ObData();
                    bNode.with_mam.set_name_by_inv("with_mam");
                    bNode.with_mam.set_result_str("" + prWithMamVal);

                    bNode.percent.set_result_str("" + mampercentrow + "%");

                }
            }

            //bnv.removeElementAt(bnv.size() - 1);
            return bnv;
        } else {
            //if any percents is empty or all percents empty
            Vector<CheckSingleMam> csm = new Vector<CheckSingleMam>();
            for (BNode bNode : bnv) {
                double mampercentrow = -1;

                try {
                    double pricerow = Double.parseDouble(LFun.fixdot(bNode.price.get_result_str()));
                    pricerow = Double.parseDouble(new DecimalFormat("##.##").format(pricerow));

                    if (bNode.mam == null) {
                        bNode.mam = new ObData();
                        bNode.mam.set_name_by_inv("mam");
                    }
                    if (bNode.without_mam == null) {
                        bNode.without_mam = new ObData();
                        bNode.without_mam.set_name_by_inv("without_mam");
                    }
                    if (bNode.with_mam == null) {
                        bNode.with_mam = new ObData();
                        bNode.with_mam.set_name_by_inv("with_mam");
                    }
                    if (bNode.percent == null) {
                        bNode.percent = new ObData();
                        bNode.percent.set_name_by_inv("percent");
                        csm.add(new CheckSingleMam(pricerow, mampercentrow));

                    } else {
                        mampercentrow = Double.parseDouble(LFun.fixdot(bNode.percent.get_result_str().replaceAll("%", "")));
                        mampercentrow = Double.parseDouble(new DecimalFormat("##.##").format(mampercentrow));
                        csm.add(new CheckSingleMam(pricerow, mampercentrow));
                    }
                } catch (Exception e) {

                }

            }

            //if all percents known
            //for ahlafot
            if (true) {
                //if (Math.pow(2, bnv.size()) < 1000000) {
                //  if(Math.pow(mam_percents.size(),b.size())<1000000){ //if qty mam option^qty rows <1000000

                System.out.println("summam will be calculated");
                doublenum = finddoublenum(Math.max(lineForDoubleSAR, bnv.lastElement().price.get_line_num()));// + 1

                //add roni number
                try {
                    boolean toAdd = true;
                    if (!doublenum.contains(Double.valueOf(infoReader.totalTaxes10))) {

                        for (int i = 0; i < doublenum.size() && toAdd; i++) {
                            if (LFun.checktoler(doublenum.get(i), Double.valueOf(infoReader.totalTaxes10), 0.001)) {
                                toAdd = false;
                            }
                        }

                        if (toAdd) {
                            doublenum.add(Double.valueOf(infoReader.totalTaxes10));
                        }

                    }
                } catch (Exception e) {

                }

                //first search if prices withmam
                CheckMam cm = new CheckMam(bnv, csm, doublenum, mam_percents, sumallMamv, globalanahav, shipmentv, true, tolMam + tolerance);

                if (cm.secondstep()) {

                    sumallMamv = cm.sumallmamv;
                    rowPrConMam = cm.pricewithmam;
                    correctShipAndSumRows();

                } else {
                    if (cm.needrefresh) {

                        globalanahav.setSum(0);
                        shipmentv.setSum(0);
                        correctShipAndSumRows();
                        bnv.lastElement().price.set_result_str("0.00");
                        csm.lastElement().price = 0;
                        doublenum = finddoublenum(Math.max(lineForDoubleSAR, bnv.lastElement().price.get_line_num()));
                        cm = new CheckMam(bnv, csm, doublenum, mam_percents, sumallMamv, globalanahav, shipmentv, true, tolMam + tolerance);

                        if (cm.secondstep()) {

                            sumallMamv = cm.sumallmamv;
                            rowPrConMam = cm.pricewithmam;
                            correctShipAndSumRows();

                        }
                    }
                }

            }

        }

        //bnv.removeElementAt(bnv.size() - 1);
        second++;
        if (!sumallMamv.getVerifived() && second == 1) {
            boolean ifneed = false;
            for (BNode block : bnv) {
                if (block.percent != null
                        && block.percent.get_external_source().equalsIgnoreCase("no")) {
                    block.percent.set_result_str(block.percent.get_result_str().replace("%", ""));
                    block.un_num.add(block.percent);
                    block.percent = null;
                    ifneed = true;
                }
            }
            if (ifneed) {
                bnv = filExistMamData(bnv);
            }
        }

        return bnv;
    }

    /**
     * correction shipment if sh==mam
     */
    public void correctShipAndSumRows() {
        if (checktoler(shipmentv.getSum(), sumallMamv.getSum(), tolerance)) {

            shipmentv.setSum(0);
            shipmentv.setVerifived(true);

        } else {
            // if mam no shipment  -> shipmentv.setVerifived(true);
            shipmentv.setVerifived(true);

        }

    }

    /**
     * verify mam if exist all percents
     *
     * @param sumMamPrContainMam
     * @param sumMamPrNotContainMam
     * @param linenum
     * @return
     */
    public boolean verifyMam(double sumMamPrContainMam, double sumMamPrNotContainMam, int linenum) {

        Vector<Double> checkV = finddoublenum(Math.max(lineForDoubleSAR, linenum));

        checkV.add(sumallMamv.getSum());
        checkV.add(shipmentv.getSum());

        for (int i = 0; i < checkV.size(); i++) {

            if (checktoler(sumMamPrContainMam, checkV.elementAt(i), tolerance)) {
                sumallMamv.setSum(checkV.elementAt(i));
                rowPrConMam = true;
                if (checktoler(shipmentv.getSum(), checkV.elementAt(i), tolerance)) {
                    shipmentv.setSum(0);
                    shipmentv.setVerifived(true);

                } else {
                    // if mam no shipment  -> shipmentv.setVerifived(true);
                    shipmentv.setVerifived(true);
                    rowPrConMam = true;
                }
                return true;
            } else if (checktoler(sumMamPrNotContainMam, checkV.elementAt(i), tolerance)) {
                sumallMamv.setSum(checkV.elementAt(i));
                rowPrConMam = false;
                if (checktoler(shipmentv.getSum(), checkV.elementAt(i), tolerance)) {
                    shipmentv.setSum(0);
                    shipmentv.setVerifived(true);

                } else {
                    // if mam no shipment  -> shipmentv.setVerifived(true);
                    shipmentv.setVerifived(true);
                    rowPrConMam = false;
                }

                return true;
            }

        }

        return false;
    }

    /**
     * fill mam data
     *
     * @param bnv
     * @return
     */
    public Vector<BNode> fil_existmamdata(Vector<BNode> bnv) {
        Vector<BNode> b = new Vector<BNode>(bnv);
        double summam = 0;
        double sumwithoutmam = 0;
        int countmamrows = 0;
        boolean pricerowwithmam = false;
        System.out.println("filing mam");

        // if haven't input data about sum, mam; first search with mam, second without
        int searchcount = 0;
        pricerowwithmam = true;
        searchagain:
        while (true) {
            Vector<CheckSingleMam> csm = new Vector<CheckSingleMam>();
            summam = 0;
            sumwithoutmam = 0;

            for (BNode bNode : b) {
                if (bNode.mam == null) {
                    bNode.mam = new ObData();
                    bNode.mam.set_name_by_inv("mam");
                }
                if (bNode.without_mam == null) {
                    bNode.without_mam = new ObData();
                    bNode.without_mam.set_name_by_inv("without_mam");
                }
                if (bNode.with_mam == null) {
                    bNode.with_mam = new ObData();
                    bNode.with_mam.set_name_by_inv("with_mam");
                }
                if (bNode.percent == null) {
                    bNode.percent = new ObData();
                    bNode.percent.set_name_by_inv("percent");
                }
                //System.out.println(bNode.ToString());
                double price_val = Double.parseDouble(LFun.fixdot(bNode.price.get_result_str()));
                double perc_val;
                try {
                    perc_val = Double.parseDouble(LFun.fixdot(bNode.percent.get_result_str().replaceAll("%", "")));
                } catch (Exception e) {
                    perc_val = 0;
                }

                double mam_val;
                try {
                    mam_val = Double.parseDouble(LFun.fixdot(bNode.mam.get_result_str()));
                } catch (Exception e) {
                    mam_val = 0;
                }

                double without_mam_val;
                try {
                    without_mam_val = Double.parseDouble(LFun.fixdot(bNode.without_mam.get_result_str()));
                } catch (Exception e) {
                    without_mam_val = 0;
                }

                if (perc_val > 0 || without_mam_val > 0 || mam_val > 0) {//
                    if (pricerowwithmam) {
                        if (without_mam_val == 0 && mam_val == 0) {
                            //by perc
                            mam_val = (price_val / (100 + perc_val)) * perc_val;
                            mam_val = Double.parseDouble(new DecimalFormat("##.##").format(mam_val));
                            without_mam_val = price_val - mam_val;
                        } else if (perc_val > 0 && without_mam_val > 0) {
                            mam_val = price_val - without_mam_val;
                            perc_val = mam_val / (without_mam_val / 100);
                            perc_val = Double.parseDouble(new DecimalFormat("##.##").format(perc_val));
                        } else if (perc_val != 0 && mam_val > 0) {
                            without_mam_val = price_val - mam_val;
                            perc_val = mam_val / (without_mam_val / 100);
                            perc_val = Double.parseDouble(new DecimalFormat("##.##").format(perc_val));
                        }

                    } else if (!pricerowwithmam) {

                        if (without_mam_val == 0 && mam_val == 0) {
                            //by perc
                            mam_val = (price_val / (100)) * perc_val;
                            mam_val = Double.parseDouble(new DecimalFormat("##.##").format(mam_val));
                            without_mam_val = price_val;
                        } else if (perc_val > 0 && without_mam_val > 0) {
                            mam_val = price_val - without_mam_val;
                            perc_val = mam_val / (without_mam_val / 100);
                            perc_val = Double.parseDouble(new DecimalFormat("##.##").format(perc_val));
                        } else if (perc_val != 0 && mam_val > 0) {
                            without_mam_val = price_val;
                            perc_val = mam_val / (without_mam_val / 100);
                            perc_val = Double.parseDouble(new DecimalFormat("##.##").format(perc_val));
                        }

                    }

                    bNode.percent.set_result_str("" + perc_val + "%");

                    summam = summam + mam_val;

                    sumwithoutmam = sumwithoutmam + without_mam_val;
                    countmamrows++;

                    csm.add(new CheckSingleMam(price_val, perc_val));

                } else {
                    csm.add(new CheckSingleMam(price_val, -1));
                }

            }

            System.out.println(summam + " :: " + sumallMamv.getSum());
            //print
            for (BNode bNode : b) {
                System.out.println("price: " + bNode.price.get_result_str() + " percent: " + bNode.percent.get_result_str() + " mam: " + bNode.mam.get_result_str() + " without_mam " + bNode.without_mam.get_result_str());
            }

            //check mamall
            if (Math.pow(2, b.size()) < 1000000) {
                // else if(Math.pow(mam_percents.size(),b.size())<1000000){ //if qty mam option^qty rows <1000000

                System.out.println("summam will be calculated");

                CheckMam cm = new CheckMam(bnv, csm, doublenum, mam_percents, sumallMamv, globalanahav, shipmentv, pricerowwithmam, tolMam + tolerance);
                //  if(!cm.firststep()){
                if (cm.secondstep()) {//cm.secondstep()
                    sumallMamv = cm.sumallmamv;

                    if (pricerowwithmam && !sumallNoMamv.getVerifived()) {
                        sumallNoMamv.setSum(sumallWithMamv.getSum() - sumallMamv.getSum());
                        sumallNoMamv.setVerifived(true);
                    } else if (!pricerowwithmam && !sumallNoMamv.getVerifived()) {
                        sumallNoMamv.setSum(sumallWithMamv.getSum());
                        sumallWithMamv.setSum(sumallWithMamv.getSum() + sumallMamv.getSum());
                        sumallNoMamv.setVerifived(true);
                    }

                    break searchagain;
                } else {

                    searchcount++;
                    if (searchcount == 2) {
                        //need clean data about mam
                        /*
                         *     for (BNode bNode : b) {
                         * cleaning
                         * }
                         */
                        break searchagain;
                    }
                    cm.pricewithmam = false;
                    pricerowwithmam = false;

                    /*    cm.pricewithmam=false;
                     if(cm.secondstep()){
                     sumallMamv=cm.sumallMamv;
                 
                     if (MODE == 0)print2file("\r\nsumall mam = "+sumallMamv.getSum()+" verifiyed = "+sumallMamv.getVerifived());
                     if (MODE == 0)print2file("\r\nsumall no mam = "+sumallNoMamv.getSum()+" verifiyed = "+sumallNoMamv.getVerifived());
                     if (MODE == 0)print2file("\r\nsumall = "+sumallWithMamv.getSum()+" verifiyed = "+sumallWithMamv.getVerifived());
                     }
                     */
                }
            } else {
                break searchagain;
            }
            //  }

        }

        //print
        for (BNode bNode : b) {
            System.out.println("price: " + bNode.price.get_result_str() + " percent: " + bNode.percent.get_result_str() + " mam: " + bNode.mam.get_result_str() + " without_mam " + bNode.without_mam.get_result_str());
        }

        return b;
    }

    /**
     * first find qty and prices
     *
     * @param result
     */
    public void findQtyAndPrices(Vector<ObData> result) {
        write(p_filename, "findQtyAndPrices", "", "1/10", true);
        int j = 0;
        double price = 0;//Double.valueOf(result.elementAt(j).get_result_str());
        double Rate = 1;
        int start = 0;
        Vector<Vector<ObData>> pairsOfQtyAndPrice = new Vector<Vector<ObData>>();
        for (; j < result.size(); j++) {
            try {
                price = Double.valueOf(LFun.fixdot(result.elementAt(j).get_result_str()));
            } catch (Exception r) {
            }

            /*try {
                Rate = Double.valueOf(ExchangeRatePrices.get(0).get_result_str());
            } catch (Exception r) {
            }*/
            int d = 0;
            if (j == result.size() - 1) {
                d = result.elementAt(j).get_line_num() + 2;
            } else {
                d = result.elementAt(j + 1).get_line_num() - 1;
            }
            //   System.out.println(" \t"+price);
            start = result.elementAt(j).get_line_num() - 2;
            if (start == - 1) {
                start = 0;
            }
            write(p_filename, "findQtyAndPrices", "", "1.4/10", true);

            for (int i = start; i <= d && i < lines.size(); i++) {
                if (lines.elementAt(i).size() > 1 && LFun.checkline(price, lines.elementAt(i), CheckSingleMam.findRateInLine(lines.elementAt(i), ExchangeRatePrices, start)) == 1) {
                    for (int k = 0; k < lines.elementAt(i).size(); k++) {

                        if (LFun.prq.size() == 1) {
                            if (lines.elementAt(i).elementAt(k).get_result_str().equalsIgnoreCase(LFun.prq.elementAt(0).get_result_str())) {
                                lines.elementAt(i).remove(lines.elementAt(i).elementAt(k));
                                ObData newOb1 = new ObData();
                                ObData newOb2 = new ObData();
                                newOb1.set_name_by_inv(LFun.qtyorpart(String.valueOf(LFun.firstNum)));
                                newOb1.set_num_block_by_inv(j);
                                newOb1.set_num_block_by_inv(result.elementAt(j).get_num_block_by_inv());
                                newOb1.set_result_str(String.valueOf(LFun.firstNum).substring(0, String.valueOf(LFun.firstNum).indexOf(".")));
                                newOb1.set_right(LFun.prq.elementAt(0).get_right());
                                newOb1.set_left(LFun.prq.elementAt(0).get_left());
                                newOb1.set_top(LFun.prq.elementAt(0).get_top());
                                newOb1.set_bottom(LFun.prq.elementAt(0).get_bottom());
                                newOb1.set_line_num(LFun.prq.elementAt(0).get_line_num());
                                newOb1.set_name_in(LFun.prq.elementAt(0).get_name_in());
                                newOb1.set_inx_col(LFun.prq.elementAt(0).get_inx_col());
                                newOb1.set_inx_renum(LFun.prq.elementAt(0).get_inx_renum());
                                newOb1.set_fntsizasc(LFun.prq.elementAt(0).get_fntsizasc());
                                newOb1.set_fntnameasc(LFun.prq.elementAt(0).get_fntnameasc());
                                String str = "";
                                if (String.valueOf(LFun.secNum).substring(String.valueOf(LFun.secNum).indexOf(".") + 1).length() < 2) {
                                    str = String.valueOf(LFun.secNum) + "0";
                                } else {
                                    str = String.valueOf(LFun.secNum);
                                }
                                newOb2.set_name_by_inv(LFun.qtyorpart(str));
                                newOb2.set_num_block_by_inv(j);
                                newOb2.set_num_block_by_inv(result.elementAt(j).get_num_block_by_inv());
                                newOb2.set_result_str(String.valueOf(LFun.secNum));
                                newOb2.set_right(LFun.prq.elementAt(0).get_right());
                                newOb2.set_left(LFun.prq.elementAt(0).get_left());
                                newOb2.set_top(LFun.prq.elementAt(0).get_top());
                                newOb2.set_bottom(LFun.prq.elementAt(0).get_bottom());
                                newOb2.set_line_num(LFun.prq.elementAt(0).get_line_num());
                                newOb2.set_name_in(LFun.prq.elementAt(0).get_name_in());
                                newOb2.set_inx_col(LFun.prq.elementAt(0).get_inx_col());
                                newOb2.set_inx_renum(LFun.prq.elementAt(0).get_inx_renum());
                                newOb2.set_fntsizasc(LFun.prq.elementAt(0).get_fntsizasc());
                                newOb2.set_fntnameasc(LFun.prq.elementAt(0).get_fntnameasc());
                                lines.elementAt(i).add(newOb1);
                                lines.elementAt(i).add(newOb2);
                            }
                        } else {
                            if (!pairsOfQtyAndPrice.contains(LFun.prq)) {
                                pairsOfQtyAndPrice.add(LFun.prq);
                            }
                            if (lines.elementAt(i).elementAt(k).get_result_str().equalsIgnoreCase(LFun.prq.elementAt(0).get_result_str())) {
                                if (!lines.elementAt(i).elementAt(k).get_name_by_inv().contains("p")) {
                                    lines.elementAt(i).elementAt(k).set_name_by_inv(LFun.qtyorpart(LFun.prq.elementAt(0).get_result_str()));
                                    lines.elementAt(i).elementAt(k).set_num_block_by_inv(j);
                                    lines.elementAt(i).elementAt(k).set_num_block_by_inv(result.elementAt(j).get_num_block_by_inv());
                                }
                            }
                            if (lines.elementAt(i).elementAt(k).get_result_str().equalsIgnoreCase(LFun.prq.elementAt(1).get_result_str())) {
                                if (!lines.elementAt(i).elementAt(k).get_name_by_inv().contains("p")) {
                                    if (LFun.isReverse == true) {
                                        lines.elementAt(i).elementAt(k).set_result_str(LFun.reverseString(LFun.prq.elementAt(1).get_result_str()));
                                    }
                                    lines.elementAt(i).elementAt(k).set_name_by_inv(LFun.qtyorpart(LFun.prq.elementAt(1).get_result_str()));
                                    lines.elementAt(i).elementAt(k).set_num_block_by_inv(j);
                                    lines.elementAt(i).elementAt(k).set_num_block_by_inv(result.elementAt(j).get_num_block_by_inv());
                                }
                            }
                        }
                        if (isA4 && LFun.prq.size() == 2) {
                            int XrightOfNumber1 = LFun.prq.elementAt(0).get_right();
                            int XrightOfNumber2 = LFun.prq.elementAt(1).get_right();
                            // בדיקה באופן יחסי לכותרות של הכמות והמחיר ליחידה
                            if ((UnitPriceTwoWordsVector.get(0) != null || UnitPriceWordsVector.get(0) != null)
                                    && (QuantityTwoWordsVector.get(0) != null || QuantityWordsVector.get(0) != null)) {
                                int XrightOfUnitPrice = 0;
                                int XrightOfQty = 0;

                                if (UnitPriceWordsVector.get(0) != null) {
                                    XrightOfUnitPrice = UnitPriceWordsVector.get(0).get_right();
                                } else if (UnitPriceTwoWordsVector.get(0) != null) {
                                    XrightOfUnitPrice = UnitPriceTwoWordsVector.get(0).get_right();
                                }

                                if (QuantityWordsVector.get(0) != null) {
                                    XrightOfQty = QuantityWordsVector.get(0).get_right();
                                } else if (QuantityTwoWordsVector.get(0) != null) {
                                    XrightOfQty = QuantityTwoWordsVector.get(0).get_right();
                                }

                                if (XrightOfQty > XrightOfUnitPrice) {
                                    if (XrightOfNumber1 > XrightOfNumber2) {
                                        LFun.prq.elementAt(0).set_name_by_inv("qty");
                                        LFun.prq.elementAt(1).set_name_by_inv("one_pr");
                                    } else {
                                        LFun.prq.elementAt(0).set_name_by_inv("one_pr");
                                        LFun.prq.elementAt(1).set_name_by_inv("qty");
                                    }
                                } else if (XrightOfQty < XrightOfUnitPrice) {
                                    if (XrightOfNumber1 > XrightOfNumber2) {
                                        LFun.prq.elementAt(0).set_name_by_inv("one_pr");
                                        LFun.prq.elementAt(1).set_name_by_inv("qty");
                                    } else {
                                        LFun.prq.elementAt(0).set_name_by_inv("qty");
                                        LFun.prq.elementAt(1).set_name_by_inv("one_pr");
                                    }
                                }
                            } else if (LFun.prq.elementAt(0).get_name_by_inv().equals(LFun.prq.elementAt(1).get_name_by_inv())) {
                                //מימין לשמאל - בדרכ המחיר ליחידה יופיע משמאל לכמות ולכן נקודת המקסימום שלו תהיה קטנה יותר
                                if (doc_direct.equalsIgnoreCase("left")) {
                                    if (XrightOfNumber1 > XrightOfNumber2) {
                                        LFun.prq.elementAt(0).set_name_by_inv("qty");
                                        LFun.prq.elementAt(1).set_name_by_inv("one_pr");
                                    } else {
                                        LFun.prq.elementAt(0).set_name_by_inv("one_pr");
                                        LFun.prq.elementAt(1).set_name_by_inv("qty");
                                    }
                                    // משמאל לימין - בדרכ המחיר ליחידה יופיע מימין לכמות ולכן נקודת המקסימום שלו תהיה גדולה יותר
                                } else if (doc_direct.equalsIgnoreCase("right")) {
                                    if (XrightOfNumber1 > XrightOfNumber2) {
                                        LFun.prq.elementAt(0).set_name_by_inv("one_pr");
                                        LFun.prq.elementAt(1).set_name_by_inv("qty");
                                    } else {
                                        LFun.prq.elementAt(0).set_name_by_inv("qty");
                                        LFun.prq.elementAt(1).set_name_by_inv("one_pr");
                                    }
                                }
                            }

                            /*if (QuantityWordsVector.get(0) != null) {
                             for (ObData ob : QuantityWordsVector) {
                             try{
                             double number1 = Double.valueOf(ob.get_result_str());
                             double number2 = Double.valueOf(LFun.prq.elementAt(0).get_result_str());
                             double number3 = Double.valueOf(LFun.prq.elementAt(1).get_result_str());
                                    
                             if (number1 == number2 ) {
                             LFun.prq.elementAt(1).set_name_by_inv("one_pr");
                             LFun.prq.elementAt(0).set_name_by_inv("qty");
                             } else if (number1 == number3 ) {
                             LFun.prq.elementAt(0).set_name_by_inv("one_pr");
                             LFun.prq.elementAt(1).set_name_by_inv("qty");
                             }
                             }catch(Exception e){
                                        
                             }
                             }
                             } else if (QuantityTwoWordsVector.get(0) != null && isA4) {
                             for (ObData ob : QuantityTwoWordsVector) {
                             if (ob.get_result_str().equals(LFun.prq.elementAt(0).get_result_str()) && ob.get_right() == LFun.prq.elementAt(0).get_right()) {
                             LFun.prq.elementAt(1).set_name_by_inv("one_pr");
                             LFun.prq.elementAt(0).set_name_by_inv("qty");
                             } else if (ob.get_result_str().equals(LFun.prq.elementAt(1).get_result_str()) && ob.get_right() == LFun.prq.elementAt(1).get_right()) {
                             LFun.prq.elementAt(0).set_name_by_inv("one_pr");
                             LFun.prq.elementAt(1).set_name_by_inv("qty");
                             }
                             }
                             }*/
                        }
                    }
                    //j++;
                    i = 100;
                }

            }
        }
        write(p_filename, "findQtyAndPrices", "", "6/10", true);

        int counterLeft = 0;
        int counterRight = 0;
        /* 
         אם נמצאו יותר מחמישה פריטים עם מחיר ליחידה וכמות
         נעשה בדיקה לפי רוב מהו הסדר של המחיר ליחידה והכמות לפי הקוארדינטות 
         */
        if (pairsOfQtyAndPrice.size() > 5 && isA4) {
            for (int i = 0; i < pairsOfQtyAndPrice.size(); i++) {
                if (pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_name_by_inv().equals("qty")
                        && pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_right() > pairsOfQtyAndPrice.elementAt(i).elementAt(1).get_right()) {
                    counterRight++;
                } else if (pairsOfQtyAndPrice.elementAt(i).elementAt(1).get_name_by_inv().equals("qty")
                        && pairsOfQtyAndPrice.elementAt(i).elementAt(1).get_right() < pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_right()) {
                    counterLeft++;
                }
            }
            write(p_filename, "findQtyAndPrices", "", "6.5/10", true);

            int middle = (int) Math.ceil(pairsOfQtyAndPrice.size() / 2);
            if (counterLeft > middle) {
                for (int i = 0; i < pairsOfQtyAndPrice.size(); i++) {
                    if (pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_right() > pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_right()) {
                        pairsOfQtyAndPrice.elementAt(i).elementAt(0).set_name_by_inv("one_pr");
                        pairsOfQtyAndPrice.elementAt(i).elementAt(1).set_name_by_inv("qty");
                    }
                }
            } else if (counterRight > middle) {
                for (int i = 0; i < pairsOfQtyAndPrice.size(); i++) {
                    if (pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_right() > pairsOfQtyAndPrice.elementAt(i).elementAt(0).get_right()) {
                        pairsOfQtyAndPrice.elementAt(i).elementAt(0).set_name_by_inv("qty");
                        pairsOfQtyAndPrice.elementAt(i).elementAt(1).set_name_by_inv("one_pr");
                    }
                }
            }
        }
        write(p_filename, "findQtyAndPrices", "", "10/10", true);
    }

    /**
     * return line number in vector by csv line number
     *
     * @param lines
     * @param lastpricelinecsv
     * @return
     */
    public int getLineNumBycsvline(int lastpricelinecsv) {
        //find last price index
        int i = 0;

        for (; i < lines.size(); i++) {
            if (lines.elementAt(i).size() > 0
                    && lines.elementAt(i).elementAt(0).get_line_num() == lastpricelinecsv) {
                return i;
            }

        }
        return i;
    }

    /**
     * search description , numbers, percents, free
     *
     * @param end_prices
     */
    public void findDesPercNumsFree(int end_prices) {

        int rpos_percent = 0;
        int lpos_percent = 10000;
        int first = nextpriceline(0) - 1;
        int firstpn = nextpn(0) + 2;
        int shope = 0;
        if (first >= firstpn && (first - firstpn) < 3) {
            shope = firstpn;
        } else {
            shope = first;
        }
        if (shope == -1) {
            shope = 0;
        }

        //first percent search
        for (int i = shope; i < end_prices && i < lines.size(); i++) { // 
            // System.out.println();
            if (!LFun.iscontain_s(lines.elementAt(i), "qty") && !LFun.iscontain_s(lines.elementAt(i), "one")) {  //
                for (int k = 0; k < lines.elementAt(i).size(); k++) {
                    if (!lines.elementAt(i).elementAt(k).get_name_by_inv().contains("price")
                            && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn")) {
                        if (lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("PERCENT")
                                && mam_percents.size() > 0
                                && ispercent(lines.elementAt(i).elementAt(k).get_result_str())) {
                            lines.elementAt(i).elementAt(k).set_name_by_inv("percent");
                            rpos_percent = Math.max(rpos_percent, lines.elementAt(i).elementAt(k).get_right());
                            lpos_percent = Math.min(lpos_percent, lines.elementAt(i).elementAt(k).get_left());
                        }
                    }
                }
            }
        }

        //second search of percent   
        for (int i = shope; i < end_prices && i < lines.size(); i++) { //
            if (!LFun.iscontain_s(lines.elementAt(i), "qty")
                    && !LFun.iscontain_s(lines.elementAt(i), "one")) {
                for (int k = 0; k < lines.elementAt(i).size(); k++) {
                    if ((lines.elementAt(i).elementAt(k).get_name_by_inv().contains("desc") || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("num"))
                            && lines.elementAt(i).elementAt(k).get_right() > (rpos_percent - 35)
                            && mam_percents.size() > 0
                            && ispercent("%" + lines.elementAt(i).elementAt(k).get_result_str())) { //
                        lines.elementAt(i).elementAt(k).set_name_by_inv("percent");
                        //System.out.println(lines.elementAt(i).elementAt(k).get_result_str());
                    }
                }
            }
        }

        // search pn with size less 4 (forwarded to FindPN....)
        //...
        // search nums and descr
        for (int i = shope; i < lines.size(); i++) { //i<end_prices &&
            if (lines.get(i) != null && lines.get(i).size() > 0 && Total_Lines.contains(lines.get(i).get(0).get_pfile_num() + "_" + lines.get(i).get(0).get_line_num())) {
                continue;
            }
            // System.out.println();

            // if (!LFun.iscontain_s(lines.elementAt(i), "qty") && !LFun.iscontain_s(lines.elementAt(i), "one")) {  //
            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                /*if(Check.Currency.getCurrency(lines.elementAt(i).elementAt(k))!=null){
                    lines.elementAt(i).elementAt(k).set_name_by_inv("matbeya");
                }
                 */
                if (!lines.elementAt(i).elementAt(k).get_name_by_inv().contains("price")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("percent")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("date")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("rate")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("mam")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("qty")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("one")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("mish")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("hazmana")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("quantityInPack")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("quantityOfPacks")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("matbeya")
                        && !lines.elementAt(i).elementAt(k).get_result_str().isEmpty()
                        && !config.getForPayWords().contains(lines.elementAt(i).elementAt(k).get_result_str().toLowerCase())
                        && !config.getTaxWords().contains(lines.elementAt(i).elementAt(k).get_result_str().toLowerCase())) {
                    if (lines.elementAt(i).elementAt(k).get_name_by_inv().equals("num")) { //|| LFun.iscontain2num(lines.elementAt(i))
                        lines.elementAt(i).elementAt(k).set_name_by_inv("num");
                    } else {
                        lines.elementAt(i).elementAt(k).set_name_by_inv("desc");
                    }

                }

            }
        }

        printLines(lines);

        boolean start = false;
        for (int i = shope; i < end_prices && !start; i++) {
            if (!LFun.iscontaindata(lines.elementAt(i))) {
                if (!start) {
                    for (int k = 0; k < lines.elementAt(i).size(); k++) {
                        lines.elementAt(i).elementAt(k).set_name_by_inv("free");
                    }
                }
            } else {
                start = true;
            }
        }

        printLines(lines);

        for (int i = end_prices; i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn") || lines.elementAt(i).elementAt(j).get_name_by_inv().equals("qty") || lines.elementAt(i).elementAt(j).get_name_by_inv().equals("one_pr") || lines.elementAt(i).elementAt(j).get_name_by_inv().equals("desc") || lines.elementAt(i).elementAt(j).get_name_by_inv().contains("price")) {
                    lines.elementAt(i).elementAt(j).set_name_by_inv("free");
                }

            }
        }

        printLines(lines);

        for (int i = 0; i < shope && i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn") || lines.elementAt(i).elementAt(j).get_name_by_inv().equals("")) {
                    lines.elementAt(i).elementAt(j).set_name_by_inv("free");
                }
            }

        }
    }

    /**
     * check zikui and anaha in prices range
     *
     * @param shope
     * @param end_prices
     */
    public void checkZikuiAndAnaha(int shope, int end_prices) {
        Vector<String> tp = new Vector<String>();
        tp.add("100");
        tp.add("200");
        tp.add("300");

        for (int i = shope; i < end_prices && i < lines.size(); i++) { //

            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("neg_price")) {
                    String PnOfBlock = isblockhaspn(lines.elementAt(i).elementAt(k).get_num_block_by_inv(), i);
                    if (!PnOfBlock.equalsIgnoreCase("")) //isrowhaspn(i)
                    {
                        if (tp.contains(PnOfBlock)) {
                            lines.elementAt(i).elementAt(k).set_name_by_inv("neg_price_u");
                        } else {
                            lines.elementAt(i).elementAt(k).set_name_by_inv("neg_price_z");
                        }
                    } else if (isbeforeexitsameposprice(i, shope, lines.elementAt(i).elementAt(k).get_result_str())) {  //isrowhaspn(i)
                        lines.elementAt(i).elementAt(k).set_name_by_inv("neg_price_z");
                    } else {
                        lines.elementAt(i).elementAt(k).set_name_by_inv("neg_price_a");
                    }

                }

            }

        }
    }

    /**
     * check rezef
     */
    public void findLevelNumber() {
        //check rezef
        int j = 0;
        int maxlpos = 0;
        int maxrpos = 0;
        int indx = 0;
        int rezef = 0;
        int countrezef = 0;
        if (doc_direct.equalsIgnoreCase("left")) {
            for (int i = 0; i < lines.size(); i++) {
                if (lines.elementAt(i).size() > 0) {
                    indx = 0;
                    maxlpos = lines.elementAt(i).elementAt(0).get_left();
                    for (int k = 1; k < lines.elementAt(i).size(); k++) {
                        if (maxlpos < lines.elementAt(i).elementAt(k).get_left()) {
                            maxlpos = lines.elementAt(i).elementAt(k).get_left();
                            indx = k;

                        }
                    }

                    try {

                        if (countrezef == 0 && lines.elementAt(i).elementAt(indx).get_format().equalsIgnoreCase("INTP")) {
                            rezef = Integer.parseInt(lines.elementAt(i).elementAt(indx).get_result_str());
                            countrezef = 1;
                        } else if (countrezef != 0 && lines.elementAt(i).elementAt(indx).get_format().equalsIgnoreCase("INTP")) {

                            if (rezef + 1 == Integer.parseInt(lines.elementAt(i).elementAt(indx).get_result_str())) {
                                countrezef++;
                                rezef = Integer.parseInt(lines.elementAt(i).elementAt(indx).get_result_str());
                            } else {
                                countrezef = 0;
                            }
                        }
                    } catch (Exception e) {
                    }

                }
            }
        } else if (doc_direct.equalsIgnoreCase("right")) {
            for (int i = 0; i < lines.size(); i++) {
                if (lines.elementAt(i).size() > 0) {
                    indx = 0;
                    maxrpos = lines.elementAt(i).elementAt(0).get_right();
                    for (int k = 1; k < lines.elementAt(i).size(); k++) {
                        if (maxrpos < lines.elementAt(i).elementAt(k).get_right()) {
                            maxrpos = lines.elementAt(i).elementAt(k).get_right();
                            indx = k;
                        }
                    }
                    try {
                        if (countrezef == 0 && lines.elementAt(i).elementAt(indx).get_format().equalsIgnoreCase("INTP")) {
                            rezef = Integer.parseInt(lines.elementAt(i).elementAt(indx).get_result_str());
                            countrezef = 1;
                        } else if (countrezef != 0 && lines.elementAt(i).elementAt(indx).get_format().equalsIgnoreCase("INTP")) {
                            if (rezef + 1 == Integer.parseInt(lines.elementAt(i).elementAt(indx).get_result_str())) {
                                countrezef++;
                                rezef = Integer.parseInt(lines.elementAt(i).elementAt(indx).get_result_str());
                            } else {
                                countrezef = 0;
                            }
                        }
                    } catch (Exception e) {
                    }

                }

            }

        }

        System.out.println("countrezef " + countrezef);

        //set name for level numbers if rezef > 2
        if (countrezef > 2) {

            maxlpos = 0;
            maxrpos = 0;
            indx = 0;
            if (doc_direct.equalsIgnoreCase("left")) {
                for (int i = 0; i < lines.size(); i++) {
                    if (lines.elementAt(i).size() > 0) {
                        indx = 0;
                        maxlpos = lines.elementAt(i).elementAt(0).get_left();
                        for (int k = 1; k < lines.elementAt(i).size(); k++) {
                            if (maxlpos < lines.elementAt(i).elementAt(k).get_left()) {
                                maxlpos = lines.elementAt(i).elementAt(k).get_left();
                                indx = k;

                            }
                        }

                        if (lines.elementAt(i).elementAt(indx).get_name_by_inv().equalsIgnoreCase("num")) {
                            lines.elementAt(i).elementAt(indx).set_name_by_inv("level");
                        }
                        System.out.println(maxlpos + " " + indx + " " + lines.elementAt(i).elementAt(indx).get_result_str() + " " + lines.elementAt(i).elementAt(indx).get_name_by_inv());
                    }
                }
            } else if (doc_direct.equalsIgnoreCase("right")) {
                for (int i = 0; i < lines.size(); i++) {
                    if (lines.elementAt(i).size() > 0) {
                        indx = 0;
                        maxrpos = lines.elementAt(i).elementAt(0).get_right();
                        for (int k = 1; k < lines.elementAt(i).size(); k++) {
                            if (maxrpos < lines.elementAt(i).elementAt(k).get_right()) {
                                maxrpos = lines.elementAt(i).elementAt(k).get_right();
                                indx = k;
                            }
                        }

                        if (lines.elementAt(i).elementAt(indx).get_name_by_inv().equalsIgnoreCase("num")) {
                            lines.elementAt(i).elementAt(indx).set_name_by_inv("level");
                        }
                        System.out.println(maxrpos + " " + indx + " " + lines.elementAt(i).elementAt(indx).get_result_str() + " " + lines.elementAt(i).elementAt(indx).get_name_by_inv());

                    }

                }

            }

        }
        // end find level numbers

    }

    public Vector<BNode> createBNV2(int shope, int end_prices, Vector<ObData> result) {

        for (int i = shope; i < end_prices && i < lines.size(); i++) {

        }

        BNode bn;
        Vector<BNode> bnv = new Vector<BNode>();
        for (int i = shope; i < end_prices && i < lines.size(); i++) { //&& j< countblock -1  &&
            //result
            bn = new BNode();
            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("price")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("qty")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("one")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("desc")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("percent")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("rate")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("date")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("mam")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("without_mam")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("anaha")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("anaha_by_percent")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("num")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("neg_price_z")) {

                    if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn")) {
                        bn.set_o_pn(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("neg_price_z")) {
                        bn.set_o_price_z(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("price")) {
                        bn.set_o_price(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("percent")) {
                        bn.set_o_percent(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("qty")) {
                        bn.set_o_qty(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("one")) {
                        bn.set_o_one_price(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("num")) {
                        bn.add_o_un_num(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("desc")) {
                        bn.add_o_descr(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("date")) {
                        bn.set_o_date(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("rate")) {
                        bn.set_o_rate(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("without_mam")) {
                        bn.set_o_without_mam(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("mam")) {
                        bn.set_o_mam(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("anaha")) {
                        bn.set_o_anaha(lines.elementAt(i).elementAt(k).get_result_str());
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("anaha_by_percent")) {
                        bn.set_o_anaha_by_percent(lines.elementAt(i).elementAt(k).get_result_str());
                    }

                }
            }
            bnv.add(bn);

        }

        return bnv;
    }

    public static void printLines(Vector<Vector<ObData>> lines) {

        if (!FT.only_one_run && print_lines) {
            //write("", "printLines", "", "start", true);
            try {

                System.out.println("Size: " + lines.size());
                int i = 0, j = 0;
                for (Vector<ObData> obdv : lines) {
                    //write("", "printLines", "i = " + i, "", true);
                    System.out.println("i = " + i);
                    for (ObData obd : obdv) {
                        //write("", "printLines", "\tj = " + j + "\tname_by_inv - " + obd.get_name_by_inv() + "\texternal_source - " + obd.get_external_source() + "\tresult_str - " + obd.get_result_str(), "", true);
                        System.out.println("\tj = " + j + "\tname_by_inv - " + obd.get_name_by_inv() + "\texternal_source - " + obd.get_external_source() + "\tresult_str - " + obd.get_result_str());
                        j++;
                    }
                    i++;
                    j = 0;

                }

            } catch (Exception e) {
                write("", "printLines", "", "Error: " + e.toString(), true);
            }
            write("", "printLines", "", "finish", true);
        }

    }

    public void createKwdFile() {
        LFun.copyFile(config.getPath_Doctype_Settings() + "\\" + SConsole.Settings_Filename + "\\" + "REPORT_HEADER.csv", Path.getOutputFolder() + "\\" + "REPORT_HEADER.csv", true);

        String command = "\"" + WiseConfig.Config.getWiseCopyExePath() + "\" \"" + Path.getOutputFolder() + "\\*.pdf\" \"" + Path.getOutputFolder() + "\" /h /s /repfile /copy /over /title /subject /author /keywords:a /subs";
        Process Process = null;
        try {
            Process = Runtime.getRuntime().exec(command);
            System.out.println(command);
        } catch (Exception ex) {
        } finally {
            writeWiserunLog(command);
        }
        while (Process != null && Process.isAlive()) {
        }
        try {
            new File(Path.getOutputFolder() + "\\" + "REPORT_HEADER.csv").delete();
        } catch (Exception e) {
        }
    }

    public void createAnlFile() {
        String nameFile = Pathes.Path.getOutputFolder() + "\\" + p_filename;
        nameFile = nameFile.replace(".csv", ".anl");
        File file = new File(nameFile);
        Writer out = null;

        String time = "";
        if (infoReader != null && infoReader.InvoiceTime15 != null) {
            if (!infoReader.InvoiceTime15.equals("@")) {
                time = infoReader.InvoiceTime15.replaceAll(":", "");
                time = time.replaceAll("-", "");
            }
        }
        String invoiceNum = "";
        if (infoReader != null && infoReader.Invoice_ID12 != null) {
            if (!infoReader.Invoice_ID12.equals("@")) {
                invoiceNum = infoReader.Invoice_ID12;
            }
        }
        //סכום פטור ממעמ
        String invoicePriceNoTaxes = "";
        if (infoReader != null && infoReader.totalPriceNoTaxes8 != null) {
            if (!infoReader.totalPriceNoTaxes8.equals("0.00") && !infoReader.totalPriceNoTaxes8.equals("@")) {
                invoicePriceNoTaxes = infoReader.totalPriceNoTaxes8;
            }
        }
        // סכום המעמ
        String totalTaxes = "";
        if (infoReader != null && infoReader.totalTaxes10 != null) {
            if (!infoReader.totalTaxes10.equals("0.00") && !infoReader.totalTaxes10.equals("@")) {
                totalTaxes = infoReader.totalTaxes10;
            }
        }
        // סכום חייב במעמ
        String totalTaxesHayavBemaam = "";
        if (infoReader != null && infoReader.totalTaxes_Hayav_Bemaam9 != null) {
            if (!infoReader.totalTaxes_Hayav_Bemaam9.equals("0.00") && !infoReader.totalTaxes_Hayav_Bemaam9.equals("@")) {
                totalTaxesHayavBemaam = infoReader.totalTaxes_Hayav_Bemaam9;
            }
        }
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                double num = 0;
                try {
                    num = Double.valueOf(lines.elementAt(i).elementAt(j).get_result_str());
                } catch (Exception e) {
                    num = 0;
                }

                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("qty")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_QTY");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_CAT");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("date") && infoReader != null && infoReader.InvoiceDate14 != null) {
                    if (!infoReader.InvoiceDate14.equals("@")) {
                        if (LFun.checkChanges(infoReader.InvoiceDate14, lines.elementAt(i).elementAt(j).get_result_str().replaceAll("/", "."))
                                || LFun.checkChanges(infoReader.InvoiceDate14, lines.elementAt(i).elementAt(j).get_result_str().replaceAll("\\\\", "."))
                                || LFun.checkChanges(infoReader.InvoiceDate14, lines.elementAt(i).elementAt(j).get_result_str().replaceAll("-", "."))) {
                            lines.elementAt(i).elementAt(j).set_STR_MEANING("DOC_DATE");
                        } else {
                            lines.elementAt(i).elementAt(j).set_STR_MEANING("NATURAL");
                        }
                    } else {
                        lines.elementAt(i).elementAt(j).set_STR_MEANING("NATURAL");
                    }
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("one_pr")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_UNITPRICE");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("desc")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_DESC");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("matbeya")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("matbeya");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("rate")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("RATE");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("mish")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_LADING");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("hazmana")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_PO");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().contains("price") && !lines.elementAt(i).elementAt(j).get_name_by_inv().contains("neg_price") && !lines.elementAt(i).elementAt(j).get_STR_MEANING().equals("TOTALSUM")) {
                    try {
                        if (Double.valueOf(lines.elementAt(i).elementAt(j).get_result_str()) > 0) {
                            lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_PRICE");
                        } else {
                            lines.elementAt(i).elementAt(j).set_STR_MEANING("NATURAL");
                        }
                    } catch (Exception e) {
                        lines.elementAt(i).elementAt(j).set_STR_MEANING("NATURAL");
                    }
                } else if ((!invoicePriceNoTaxes.equals("") && lines.elementAt(i).elementAt(j).get_result_str().equals(invoicePriceNoTaxes))
                        || (num > 0 && LFun.checktoler(num, sumallpaturv.getSum(), tolerance))) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("VATEXEMPT");
                } else if ((!totalTaxes.equals("") && lines.elementAt(i).elementAt(j).get_result_str().equals(totalTaxes))
                        || (num > 0 && LFun.checktoler(num, sumallMamv.getSum(), tolerance))) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("VATSUM");
                } else if ((!totalTaxesHayavBemaam.equals("") && lines.elementAt(i).elementAt(j).get_result_str().equals(totalTaxesHayavBemaam))
                        || (num > 0 && LFun.checktoler(num, sumallhayavv.getSum(), tolerance))) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("VATCHARGEABLE");
                } else if (!time.equals("") && lines.elementAt(i).elementAt(j).get_result_str().equals(time)) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("DOC_TIME");
                } else if (!invoiceNum.equals("") && lines.elementAt(i).elementAt(j).get_result_str().equals(invoiceNum)) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("DOC_NUMBER");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("neg_price_a")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_REBATE");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("anaha_by_percent")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_REBATEPERC");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().contains("mam:")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEMֹ_VATPERC");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("mam")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_VAT");
                } else if (lines.elementAt(i).elementAt(j).get_name_by_inv().contains("without_mam")) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("ITEM_VATCHARGEABLE");
                } else if (lines.elementAt(i).elementAt(j).get_STR_MEANING().equals("TOTALSUM")
                        || (num > 0 && LFun.checktoler(num, sumallWithMamv.getSum(), tolerance))) {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("TOTALSUM");
                    //lines.elementAt(i).elementAt(j).set_STR_MEANING(" ");
                } else {
                    lines.elementAt(i).elementAt(j).set_STR_MEANING("NEUTRAL");
                }
            }
        }

        try {
            file.createNewFile();
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            out.write("STR_VAL,STR_FILE,STR_PAGE,STR_COL,STR_LINE,STR_LEFT,STR_TOP,STR_REIGHT,STR_BOTTOM,STR_SEQ,STR_FONTSIZE,STR_FONTNAME,STR_TYPE,STR_MEANING" + "\r\n");
            String word = "";
            int numberPage = 1;
            int numberLinesToAdd = 0;
            int line = 0;
            for (Vector<ObData> obdv : lines) {
                for (ObData obd : obdv) {

                    if (obd.get_pfile_num() > numberPage) {
                        numberPage++;
                        numberLinesToAdd -= line;
                    }

                    String obdStr = obd.get_result_str();

                    if (!obd.get_isVerifived()) {
                        obdStr = obdStr.replace("~", "");
                    }

                    line = obd.get_line_num();
                    word = "\"" + obdStr + "\"" + "," + obd.get_name_in() + "," + obd.get_pfile_num() + "," + obd.get_inx_col() + "," + (obd.get_line_num() + numberLinesToAdd) + ","
                            + obd.get_left() + "," + obd.get_top() + "," + obd.get_right() + "," + obd.get_bottom() + "," + obd.get_inx_renum()
                            + "," + obd.get_fntsizasc() + "," + obd.get_fntnameasc() + "," + obd.get_format() + "," + obd.get_STR_MEANING() + "\r\n";
                    out.write(word);

                    word = "";
                }
            }
            System.out.println(file.getAbsolutePath());
        } catch (Exception e) {

        } finally {
            try {
                out.close();
            } catch (IOException ex) {
            }
        }

    }

    /**
     * add to matrix by blocks : nblock pn price qty one_price descr etc
     *
     * @param shope
     * @param end_prices
     * @return
     */
    public Vector<BNode> createBNV(int shope, int end_prices, Vector<String> vnumdefault, Vector<ObData> result) {
        for (ObData o : result) {
            o.set_name_by_inv("price");
        }

        // מחליף את כל הפסיקים לנקודות במקומות שבהם יש מחיר 
        // כלומר כוללים "Pr" 
        // בשם של name_by_inv
        for (Vector<ObData> obd : lines) {
            for (ObData obdd : obd) {
                if (obdd.get_name_by_inv().contains("pr")) {
                    obdd.set_result_str(obdd.get_result_str().replaceAll(",", "."));
                }
            }
        }

        printLines(lines);
        int start = shope - 1;
        if (start == -1) {
            start = 0;
        }
        for (ObData obd : lines.elementAt(start)) {
            if (obd.get_name_by_inv().equals("pn")) {
                shope = shope - 1;
                break;
            }
        }

        Vector<BNode> bnv = new Vector<BNode>();
        BNode bn = new BNode();
        int j = 0, hanaha_counter = 0;
        try {

            /*    check the hanahot   */
            if (sumrowpricesHSv.getVerifived()) {
                for (int i = 0; i < hanaha_Vector.size(); i++) {
                    if (Math.abs(Double.valueOf(hanaha_Vector.elementAt(hanaha_counter).hanaha.get_result_str())) > (sumrowpricesHSv.getSum() * 10)) {
                        hanaha_Vector.removeElementAt(i);
                    }
                }
            }

            j = lines.elementAt(shope).elementAt(0).get_num_block_by_inv();
        } catch (Exception e) {
            j = 0;
        }

        if (j < 0) {
            j = 0;
        }
        int count = j = lines.elementAt(shope).elementAt(0).get_num_block_by_inv();
        for (int i = shope; i < end_prices && i < lines.size(); i++) { //&& j< countblock -1  &&
            if (lines.elementAt(i).size() > 0) {
                if (lines.elementAt(i).elementAt(0).get_num_block_by_inv() == -1 && i == shope) {

                    j = 0;

                } else if (bn.pn != null && bn.price == null) {
                    j = 0;

                } else if (lines.elementAt(i).elementAt(0).get_num_block_by_inv() >= j
                        || (i > 0 && !lines.elementAt(i - 1).isEmpty() && lines.elementAt(i).elementAt(0).get_top() - lines.elementAt(i - 1).elementAt(0).get_bottom() > 300)
                        || (bn.price != null && bn.one_price != null && bn.qty != null && result.toString().contains(bn.price.get_result_str()))
                        || (bn.price != null && resultContains(result, bn.price))) {
                    j++;

                    if (bn.price != null) {
                        bnv.add(bn);
                    }
                    bn = new BNode();

                } else {
                    System.out.println("");
                }

            }

            //check if line contain anaha, qty, one_price
            fil_pr_qty_an_data(i);

            //bnv filing
            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if (Total_Lines.contains(lines.get(i).get(0).get_pfile_num() + "_" + lines.get(i).get(0).get_line_num())) {
                    continue;
                }
                if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("price")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("qty")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("one")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("desc")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("percent")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("rate")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("date")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("mam")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("without_mam")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("anaha")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("anaha_by_percent")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("num")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("neg_price_z")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("hazmana")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("mish")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("quantityInPack")
                        || lines.elementAt(i).elementAt(k).get_name_by_inv().contains("quantityOfPacks")) {

                    if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn") && !isA4) {
                        bn.set_o_pn(CleanLetters(lines.elementAt(i).elementAt(k)));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn") && isA4) {
                        //if it`s A4 so we can have letters

                        //if(bn.listPn.size() != 0){
                        //bn.pn.set_result_str(bn.pn.get_result_str() + " " + lines.elementAt(i).elementAt(k).get_result_str());
                        bn.add_o_listPn(lines.elementAt(i).elementAt(k));
                        //}else{
                        //bn.set_o_pn(lines.elementAt(i).elementAt(k));
                        //bn.add_o_listPn(lines.elementAt(i).elementAt(k));
                        //}
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equals("neg_price_z")) {
                        bn.set_o_price_z(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("one")) {
                        bn.set_o_one_price(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("price")) {
                        bn.set_o_price(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("percent")) {
                        bn.set_o_percent(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("qty")) {
                        bn.set_o_qty(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("num")) {
                        bn.add_o_un_num(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("desc")) {
                        if (isHebFile && doc_direct.equals("left") && (bn.price == null || bn.price.get_left() > lines.elementAt(i).elementAt(k).get_left())) {//Hebrew invoice and the variable is not set, so this is not the description
                            lines.elementAt(i).elementAt(k).set_name_by_inv("free");
                        } else {
                            bn.add_o_descr(lines.elementAt(i).elementAt(k));
                        }
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("date")) {
                        bn.set_o_date(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("rate")) {
                        bn.set_o_rate(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("without_mam")) {
                        bn.set_o_without_mam(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("mam")) {
                        bn.set_o_mam(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("anaha")) {
                        bn.set_o_anaha(lines.elementAt(i).elementAt(k).get_result_str());
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("anaha_by_percent")) {
                        bn.set_o_anaha_by_percent(lines.elementAt(i).elementAt(k).get_result_str());
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("hazmana")) {
                        bn.set_o_order_num(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("mish")) {
                        bn.set_o_ship_num(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("quantityInPack")) {
                        bn.set_o_quantity_in_pack(lines.elementAt(i).elementAt(k));
                    } else if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("quantityOfPacks")) {
                        bn.set_o_quantity_of_packs(lines.elementAt(i).elementAt(k));
                    }
                }
            }

            if (bn.rate == null && ExchangeRatePrices.size() > 0) {
                if (ExchangeRatePrices.size() == 1
                        || start - 2 > ExchangeRatePrices.get(0).get_line_num()) {
                    try {
                        bn.set_o_rate(ExchangeRatePrices.get(0));
                    } catch (Exception e) {
                    }
                } else if (end_prices + 2 < ExchangeRatePrices.get(ExchangeRatePrices.size() - 1).get_line_num()) {
                    try {
                        bn.set_o_rate(ExchangeRatePrices.get(ExchangeRatePrices.size() - 1));
                    } catch (Exception e) {
                    }
                }

            }

            if (i == end_prices - 1 || i + 1 == lines.size()) {  // if(i==end_prices-1)
                if (bn != null) {
                    if (bn.price != null) {
                        bnv.add(bn);
                    }
                }
            }
        }
        /*try {
            Thread.sleep(3000);
        } catch (Exception e) {
        }*/
        try {
            if (QuantityWordsVector != null && !QuantityWordsVector.isEmpty()
                    && QuantityWordsVector.get(0) != null) {
                int left = QuantityWordsVector.get(0).get_left();
                int right = QuantityWordsVector.get(0).get_right();
                int q = 0;
                for (BNode bNode : bnv) {
                    try {
                        if (bNode.qty != null) {
                            int bnv_qty_left = bNode.qty.get_left();
                            int bnv_qty_right = bNode.qty.get_right();

                            //if (Math.abs(bnv_qty_right - right) < DEVIATION * 1.5 || Math.abs(bnv_qty_left - left) < DEVIATION * 1.5) {
                            if (ObDataWithLines.twoLineMeet(left, right, bnv_qty_left, bnv_qty_right)) {

                                //System.out.println("v   " + q++ + " " + bNode.qty.get_result_str() + ", " + bNode.qty.get_left());
                                right = bnv_qty_right;
                                left = bnv_qty_left;
                            } else if (bNode.one_price != null) {
                                int bnv_one_price_left = bNode.one_price.get_left();
                                int bnv_one_price_right = bNode.one_price.get_right();

                                //if (ObDataWithLines.twoLineMeet(left, right, bnv_one_price_left, bnv_one_price_right)) {
                                if (Double.valueOf(bNode.qty.get_result_str().trim().replaceAll(",", "")) >= Double.valueOf(bNode.one_price.get_result_str().trim().replaceAll(",", ""))) {
                                    ObData t = bNode.one_price;
                                    bNode.one_price = bNode.qty;
                                    bNode.qty = t;
                                    bNode.one_price.set_name_by_inv("one");
                                    bNode.qty.set_name_by_inv("qty");
                                    //System.out.println("*   " + q++ + " " + bNode.qty.get_result_str() + ", " + bNode.qty.get_left());
                                } else {
                                    //System.out.println("!   " + q++ + " " + bNode.qty.get_result_str() + ", " + bNode.qty.get_left());
                                }
                            } else {
                                //System.out.println("###" + q++ + " " + bNode.qty.get_result_str() + ", " + bNode.qty.get_left());
                            }
                        } else {
                            //System.err.println(q++ + " " + bNode.qty.get_result_str() + ", " + bNode.qty.get_left());
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }

                //System.exit(0);
            }
        } catch (Exception e) {
        }

// remove blocks that contain in description a pay words that in the config.ini
        for (int i = bnv.size() - 1; i >= bnv.size() / 4 && i > 0; i--) {
            for (String s : config.getForPayWords()) {
                if (bnv.get(i).get_description().contains(s)) {
                    sumallWithMamv.setSum(Double.valueOf(bnv.get(i).price.get_result_str()));
                    sumallWithMamv.setVerifived(false);
                    sumallNoMamv.setCancel(true);
                    sumallMamv.setCancel(true);
                    if (LFun.contain_type(vnumdefault, bnv.get(i).price.get_format())) {
                        lines.elementAt(bnv.get(i).price.get_line_num() - 1).elementAt(bnv.get(i).price.get_inx_col()).set_name_by_inv("num");
                    } else {
                        lines.elementAt(bnv.get(i).price.get_line_num() - 1).elementAt(bnv.get(i).price.get_inx_col()).set_name_by_inv("free");
                    }
                    bnv.remove(i);
                    for (int k = i; k < bnv.size(); k++) {
                        if (bnv.get(k).price.get_result_str() == bnv.get(k).price.get_result_str()) {
                            bnv.remove(k);
                            k--;
                        }
                    }
                    break;
                }
            }

        }

        //find qty an one_price by looking in the ather lines
        for (int i = 1; i < bnv.size() - 1; i++) {
            //System.out.println("i" + i + ", qty=" + bnv.get(i).qty.get_left() + ", one_price=" + bnv.get(i).one_price.get_left() + ", price=" + bnv.get(i).price.get_left());

            /* try {
                System.out.print(i + ", qty=" + bnv.get(i).qty.get_left());
            } catch (Exception e) {
            }
            try {
                System.out.print(", one_price=" + bnv.get(i).one_price.get_left());
            } catch (Exception e) {
            }
            try {
                System.out.print(", price=" + bnv.get(i).price.get_left());
            } catch (Exception e) {
            }
            System.out.println();*/
            try {
                ObData qty = null;
                ObData one_price = null;
                int qty_left = (bnv.get(i - 1).qty.get_left() + bnv.get(i + 1).qty.get_left()) / 2;
                int one_price_left = (bnv.get(i - 1).one_price.get_left() + bnv.get(i + 1).one_price.get_left()) / 2;

                if (bnv.get(i).one_price == null && bnv.get(i - 1).one_price != null && bnv.get(i + 1).one_price != null
                        && Math.abs(bnv.get(i - 1).one_price.get_left() - bnv.get(i + 1).one_price.get_left()) < 75) {
                    for (ObData elementAt : lines.elementAt(bnv.get(i).price.get_line_num() - 1)) {
                        if (qty == null && Math.abs(elementAt.get_left() - qty_left) < 50) {
                            qty = elementAt;
                        }
                        if (one_price == null && Math.abs(elementAt.get_left() - one_price_left) < 50) {
                            one_price = elementAt;
                        }
                        //System.out.println(elementAt.get_result_str() + " " + elementAt.get_left());
                    }
                    if (one_price != null && qty != null
                            && Double.valueOf(one_price.get_result_str()) != 0 && Double.valueOf(qty.get_result_str()) != 0) {
                        for (ObData elementAt : lines.elementAt(bnv.get(i).price.get_line_num() - 1)) {
                            if (elementAt.get_name_by_inv().equals("qty") || elementAt.get_name_by_inv().equals("price")) {
                                elementAt.set_name_by_inv("num");
                            }
                        }
                        qty.set_name_by_inv("qty");
                        one_price.set_name_by_inv("price");
                        bnv.get(i).qty = qty;
                        bnv.get(i).one_price = one_price;

                        //System.out.println("one_price=" + one_price);
                        //System.out.println("qty=" + qty);
                    }

                    //System.out.println("one_price=" + one_price);
                    //System.out.println("qty=" + qty);
                    //System.out.println("$$");
                    //System.exit(0);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
        /*for (int i = 1; i < bnv.size() - 1; i++) {
            try {
                System.out.println("i" + i + ", qty=" + bnv.get(i).qty.get_left() + ", one_price=" + bnv.get(i).one_price.get_left() + ", price=" + bnv.get(i).price.get_left());
                 System.out.println("i" + i + ", qty=" + bnv.get(i).qty.get_result_str() + ", one_price=" + bnv.get(i).one_price.get_result_str() + ", price=" + bnv.get(i).price.get_result_str());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

//        checkWithSimilarNumbersReplacePos(bnv);
        return bnv;
    }

    public boolean resultContains(Vector<ObData> result, ObData price) {
        for (ObData obData : result) {
            if (price.get_line_num() == obData.get_line_num()) {
                if (price.get_result_str().equalsIgnoreCase(obData.get_result_str())) {
                    return true;
                }
                break;
            }
        }
        return false;
    }

    public double sumResult(Vector<ObData> result) {
        double sum = 0.0;
        for (ObData r : result) {
            sum += Double.valueOf(r.get_result_str().replaceAll(",", ""));
        }
        return sum;
    }

    public void markSumEnd(int linenumber, Vector<BNode> bnv, Vector<ObData> result) {

        //zvika 13.6.2019
        try {
            double sumResult = sumResult(result);
            double sumWisepage = Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9.trim().replaceAll(",", "").replaceAll("@", ""));
            if (bnv.size() == result.size()
                    && Math.abs(sumResult - sumWisepage) < DeviationForBudget) {
                if (!infoReader.totalTaxes10.isEmpty() && !infoReader.totalTaxes10.equals("@")) {
                    sumallMamv.setSum(Double.valueOf(infoReader.totalTaxes10.trim().replaceAll(",", "")));
                    sumallMamv.setVerifived(true);
                }

                sumallNoMamv.setSum(sumWisepage);
                sumallNoMamv.setVerifived(true);

                if (Math.abs(infoReader.totalInvoiceSum13) > 5) {
                    sumallWithMamv.setSum(infoReader.totalInvoiceSum13);
                    sumallWithMamv.setVerifived(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isDigitalFile && sumallMamv != null && sumallMamv.getSum() != 0.0) {
            sumallMamv.setVerifived(true);
        }

        /**
         * Pavel *
         */
        if (sumallMamv.getVerifived()) {
            sumGRandHSv = new VerVar(shipmentv.getSum() + (-1) * Math.abs(globalanahav.getSum()));
            sumGRandHSv.setVerifived(true);
        }

        if (sumallMamv.getVerifived() && sumrowpricesHSv.getVerifived()) {
            if (rowPrConMam) {
                sumallNoMamv.setSum(sumrowpricesHSv.getSum() - sumallMamv.getSum() + sumGRandHSv.getSum());
                sumallWithMamv.setSum(sumrowpricesHSv.getSum() + sumGRandHSv.getSum());
            } else {
                sumallNoMamv.setSum(sumrowpricesHSv.getSum() + sumGRandHSv.getSum());
                sumallWithMamv.setSum(sumrowpricesHSv.getSum() + sumallMamv.getSum() + sumGRandHSv.getSum());
            }

            Vector<Double> v = finddoublenum(Math.max(lineForDoubleSAR, linenumber));

            for (int i = 0; i < v.size(); i++) {
                if (checktoler(v.elementAt(i), sumallNoMamv.getSum(), tolerance)) {
                    sumallNoMamv.setSum(v.elementAt(i));
                    sumallNoMamv.setVerifived(true);

                } else if (checktoler(v.elementAt(i), sumallWithMamv.getSum(), tolerance)) {
                    sumallWithMamv.setSum(v.elementAt(i));
                    sumallWithMamv.setVerifived(true);
                }
            }

        }
        /**
         * Pavel-End *
         */

        /**
         * Fix numbers *
         */
        sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallWithMamv.getSum())));
        sumrowpricesHSv.setSum(Double.parseDouble(new DecimalFormat("##.##").format((sumrowpricesHSv.getSum()))));
        sumallNoMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format((sumallNoMamv.getSum()))));

        /**
         * Fix count *
         */
        if (!partscount.getVerifived()) {
            int ipartscount = 0;
            partscount.setSum(ipartscount);
            partscount.setVerifived(true);
        }
        /**
         * Fix With Mam *
         */
        if (!sumallWithMamv.getVerifived()) {

            // We need to add before maam the maam and verifived.
        }

        if (!sumallMamv.getVerifived()) {
            if (sumallWithMamv.getVerifived() && sumallNoMamv.getVerifived()) {
                sumallMamv.setSum(sumallWithMamv.getSum() - sumallNoMamv.getSum());
                sumallMamv.setVerifived(true);
                sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallMamv.getSum())));

            }
        }

    }

    public void addCurrencyCode(MatchesToItems bnvWithMatchesToItems) {
        try {
            boolean isThereMatches = false;

            for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                if (!bnvWithMatchesToItems.MatchesToItems.get(i).matches_is_fictitious && bnvWithMatchesToItems.MatchesToItems.get(i).matches != null && bnvWithMatchesToItems.MatchesToItems.get(i).matches.size() > 0) {
                    isThereMatches = true;
                }
            }

            double sumAllPrices = 0;
            for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                sumAllPrices += Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(i).block.price.get_result_str()) + Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(i).block.anaha);
            }

            if (!isThereMatches) {
                try {
                    if (Math.abs(sumAllPrices - (Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9) - Double.valueOf(infoReader.totalTaxes10))) < DeviationForBudget) {
                        for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                            bnvWithMatchesToItems.MatchesToItems.get(i).block.rate = null;
                        }
                    }
                } catch (Exception e) {
                }
            }

            int[] ledt_or_right = {+1, -1};
            String sumall_Currency = null;
            for (ObData ob : sumall_for_Currency) {
                for (int l_r : ledt_or_right) {
                    try {
                        //one_prise Currency: 
                        ObData ob_near = lines.get(ob.get_line_num() - 1).get(ob.get_inx_renum() + l_r);
                        String ob_near_currency = Check.Currency.getCurrency(ob_near);
                        if (ob_near_currency != null && ob_near.get_left() - ob.get_right() < 70) {
                            if (sumall_Currency == null || sumall_Currency.equalsIgnoreCase(ob_near_currency)) {
                                sumall_Currency = ob_near_currency;
                            } else {
                                sumall_Currency = "Two_Currency";
                            }
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }
            }

            /*if (sumall_Currency == null || sumall_Currency.equalsIgnoreCase("Two_Currency")) {
            sumall_Currency = null;
        }*/
            String all_Currency = null;
            //if (!MatbeyaAllWordsVector.isEmpty()) {
            if (all_Currency == null && MatbeyaAllWordsVector.size() > 0) {
                for (ObData ob : MatbeyaAllWordsVector) {
                    try {
                        String ob_currency = Check.Currency.getCurrency(ob);
                        if (ob_currency != null) {
                            if (all_Currency == null || all_Currency.equalsIgnoreCase(ob_currency)) {
                                all_Currency = ob_currency;
                            } else {
                                all_Currency = "Two_Currency";
                            }
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }
            }
            /*if (all_Currency == null || all_Currency.equalsIgnoreCase("Two_Currency")) {
            all_Currency = null;
        }*/
            //} else {
            //    all_Currency = sumall_Currency;

            //}
            try {
                if (all_Currency == null && sumall_Currency != null
                        && !isThereMatches && Math.abs(sumAllPrices - (Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9) - Double.valueOf(infoReader.totalTaxes10))) < DeviationForBudget) {
                    all_Currency = sumall_Currency;
                }
            } catch (Exception e) {
            }

            for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                ObData o_one_prise = null;
                ObData o_prise = null;
                String Currency_one_prise = null;
                String Currency_prise = null;
                double rate = 1;
                double qty = 1;

                for (int l_r : ledt_or_right) {
                    try {
                        //one_prise Currency: 
                        o_one_prise = bnvWithMatchesToItems.MatchesToItems.get(i).block.one_price;
                        ObData o_one_prise_cur = lines.get(o_one_prise.get_line_num() - 1).get(o_one_prise.get_inx_renum() + l_r);
                        Currency_one_prise = Check.Currency.getCurrency(o_one_prise_cur);
                        if (Currency_one_prise != null && o_one_prise.get_left() - o_one_prise_cur.get_right() < 70) {
                            bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = Currency_one_prise;
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }

                for (int l_r : ledt_or_right) {
                    try {
                        //price Currency: 
                        o_prise = bnvWithMatchesToItems.MatchesToItems.get(i).block.price;
                        ObData o_prise_cur = lines.get(o_prise.get_line_num() - 1).get(o_prise.get_inx_renum() + l_r);
                        Currency_prise = Check.Currency.getCurrency(o_prise_cur);
                        if (Currency_prise != null && o_prise_cur.get_left() - o_prise_cur.get_right() < 70) {
                            bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = Currency_prise;
                            try {
                                if (i > 0
                                        && !bnvWithMatchesToItems.MatchesToItems.get(i - 1).block.Curreny_for_all_qty.equals(bnvWithMatchesToItems.MatchesToItems.get(i - 1).block.Curreny_for_one_item)
                                        && bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item.equals(bnvWithMatchesToItems.MatchesToItems.get(i - 1).block.Curreny_for_one_item)) {
                                    bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = bnvWithMatchesToItems.MatchesToItems.get(i - 1).block.Curreny_for_all_qty;
                                }
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }

                try {
                    rate = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(i).block.rate.get_result_str());
                    if (rate > 0 && bnvWithMatchesToItems.MatchesToItems.get(i).matches_is_fictitious) {//(rate can be 1)
                        bnvWithMatchesToItems.MatchesToItems.get(i).matches.get(0).match[77] = rate + "";
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                }

                try {
                    qty = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(i).block.qty.get_result_str());
                } catch (Exception e) {
                    //e.printStackTrace();
                }

                try {
                    if (o_one_prise != null) {
                        double double_one_prise = Double.valueOf(o_one_prise.get_result_str());
                        double double_prise = Double.valueOf(o_prise.get_result_str());

                        if (rate == 1 && Math.abs(double_prise - double_one_prise * qty) < DeviationForBudget) {
                            if (Currency_one_prise != null) {
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = Currency_one_prise;
                                continue;
                            } else if (Currency_prise != null) {
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = Currency_prise;
                                continue;
                            } else if (Currency_one_prise == null && Currency_prise == null && all_Currency != null && all_Currency.length() < 5) {
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = all_Currency;
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = all_Currency;
                                continue;
                            } else if (!isThereMatches && all_Currency != null && all_Currency.length() < 5 && sumall_Currency != null && sumall_Currency.length() < 5) {
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = all_Currency;
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = sumall_Currency;
                                continue;
                            }
                        } else if (rate != 1 && Math.abs(double_prise - rate * double_one_prise * qty) < DeviationForBudget
                                && Currency_one_prise != null && sumall_Currency != null && sumall_Currency.length() < 5 && !Currency_one_prise.equalsIgnoreCase(sumall_Currency)) {
//                        && rate > 3 && rate < 4.3 && Currency_one_prise.equalsIgnoreCase("USD")) {
                            if (Currency_one_prise != null && Currency_prise == null) {
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = sumall_Currency;
                                continue;
                                //bnvWithMatchesToItems.MatchesToItems.get(0).block.Curreny_for_all_qty = "NIS";
                            } else if (Currency_one_prise == null && Currency_prise == null && all_Currency != null && all_Currency.length() < 5) {
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = all_Currency;
                                bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = all_Currency;
                                continue;
                            }
                        }
                    }
                    if (all_Currency == null && sumall_Currency == null) {
                        String any_currency = null;
                        for (ObData ob : od) {
                            try {
                                String ob_currency = Check.Currency.getCurrency(ob);
                                if (ob_currency != null) {
                                    if (any_currency == null || any_currency.equalsIgnoreCase(ob_currency)) {
                                        any_currency = ob_currency;
                                    } else {
                                        any_currency = "Two_Currency";
                                    }
                                }
                            } catch (Exception e) {
                                //e.printStackTrace();
                            }
                        }
                        if (any_currency == null) {
                            any_currency = config.getDefaultCurrency();
                        }
                        if (any_currency != null) {
                            bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = any_currency;
                            bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = any_currency;
                        }
                    } else if (all_Currency == null && sumall_Currency != null) {
                        bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = sumall_Currency;
                        bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = sumall_Currency;
                    } else if (all_Currency != null && sumall_Currency == null) {
                        bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty = all_Currency;
                        bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item = all_Currency;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                if (bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty != null
                        && bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_all_qty.equals(bnvWithMatchesToItems.MatchesToItems.get(i).block.Curreny_for_one_item)) {
                    bnvWithMatchesToItems.MatchesToItems.get(i).block.rate = null;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //}
    }

    public void GeneralRepairsUsingMatches(MatchesToItems bnvWithMatchesToItems) {
        /**
         * zvika
         */

        try {
            if (bnvWithMatchesToItems.MatchesToItems != null && bnvWithMatchesToItems.MatchesToItems.size() > 0 && !bnvWithMatchesToItems.MatchesToItems.get(0).matches_is_fictitious
                    && bnvWithMatchesToItems.MatchesToItems.get(0).matches != null && bnvWithMatchesToItems.MatchesToItems.get(0).matches.size() > 0) {

                Double TOTAL_SUM_INCLUDING_VAT_in_PO = 0.0;
                Double TOTAL_VAT_EXEPMPT_SUM_in_PO = 0.0;
                Double TOTAL_VAT_CHARGEABLE_SUM_in_PO = 0.0;
                Double TOTAL_VAT_SUM_in_PO = 0.0;

                try {
                    TOTAL_SUM_INCLUDING_VAT_in_PO = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(0).matches.get(0).match[44]); // 	הסכום הכולל של החשבונית , כולל מעמ. 
                } catch (Exception e) {
                }

                try {
                    TOTAL_VAT_EXEPMPT_SUM_in_PO = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(0).matches.get(0).match[41]); //  הסכום הפטור ממעמ בחשבונית. 
                } catch (Exception e) {
                }

                try {
                    TOTAL_VAT_CHARGEABLE_SUM_in_PO = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(0).matches.get(0).match[42]); //  הסכום החייב במעמ בחשבונית. 
                } catch (Exception e) {
                }

                try {
                    TOTAL_VAT_SUM_in_PO = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(0).matches.get(0).match[43]); // סכום המעמ הכולל שבחשבונית
                } catch (Exception e) {
                }
                String Type = SConsole.getDocTypeWeRecognize(Path.getbill_in_a());
                if (Type != null && TOTAL_SUM_INCLUDING_VAT_in_PO < 0
                        && TOTAL_VAT_EXEPMPT_SUM_in_PO < 0
                        && TOTAL_VAT_CHARGEABLE_SUM_in_PO < 0
                        && TOTAL_VAT_SUM_in_PO < 0) {
                    //INVO To INVC:
                    bnvWithMatchesToItems.setDocType("INVC");
                    Management.SConsole.changeDocTypeKeyWord(Path.getOutputFolder(), Path.getOutputFolder(), Type, "INVC");
                    Management.SConsole.changeDocTypeInName(Path.getbill_in(), Type, "INVC");
                    path = path.replaceAll(";" + Type, ";" + "INVC");
                    p_filename = p_filename.replaceAll(";" + Type, ";" + "INVC");
                } else if (Type != null && !Type.equalsIgnoreCase("INVO")) {
                    //To INVO:                 
                    bnvWithMatchesToItems.setDocType("INVO");
                    String docType = "";
                    Management.SConsole.changeDocTypeKeyWord(Path.getOutputFolder(), Path.getOutputFolder(), Type, "INVO");
                    Management.SConsole.changeDocTypeInName(Path.getbill_in(), Type, "INVO");
                    path = path.replaceAll(";" + Type, ";" + "INVO");
                    p_filename = p_filename.replaceAll(";" + Type, ";" + "INVO");
                }

                double sum = 0;
                for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                    for (int j = 0; bnvWithMatchesToItems.MatchesToItems.get(i).matches != null && j < bnvWithMatchesToItems.MatchesToItems.get(i).matches.size(); j++) {
                        Double ITEM_TOTAL_PRICE_in_PO = Double.valueOf(bnvWithMatchesToItems.MatchesToItems.get(i).matches.get(j).match[35]);
                        sum += ITEM_TOTAL_PRICE_in_PO;
                    }
                }

                if (sum != 0 && Math.abs(TOTAL_VAT_CHARGEABLE_SUM_in_PO - sum) < DeviationForBudget) {//All_ok!!
                    // 	הסכום הכולל של החשבונית , כולל מעמ. 
                    infoReader.totalInvoiceSum13 = TOTAL_SUM_INCLUDING_VAT_in_PO;
                    sumallWithMamv = new VerVar(TOTAL_SUM_INCLUDING_VAT_in_PO);
                    sumallWithMamv.setVerifived(true);

                    //  הסכום הפטור ממעמ בחשבונית. 
                    infoReader.totalPriceNoTaxes8 = TOTAL_VAT_EXEPMPT_SUM_in_PO + "";
                    sumallpaturv = new VerVar(TOTAL_VAT_EXEPMPT_SUM_in_PO);
                    sumallpaturv.setVerifived(true);

                    // הסכום החייב במעמ בחשבונית
                    infoReader.totalTaxes_Hayav_Bemaam9 = TOTAL_VAT_CHARGEABLE_SUM_in_PO + "";
                    sumallhayavv = new VerVar(TOTAL_VAT_CHARGEABLE_SUM_in_PO);
                    sumallhayavv.setVerifived(true);

                    // סכום המעמ הכולל שבחשבונית
                    infoReader.totalTaxes10 = TOTAL_VAT_SUM_in_PO + "";
                    sumallMamv = new VerVar(TOTAL_VAT_SUM_in_PO);
                    sumallMamv.setVerifived(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 	הסכום הכולל של החשבונית , כולל מעמ. 
        try {
            double d = Double.valueOf(infoReader.totalInvoiceSum13);

            if (!sumallMamv.getVerifived()
                    && !sumallpaturv.getVerifived()
                    && !sumallhayavv.getVerifived()
                    && d != 0) {
                sumallWithMamv = new VerVar(d);
                sumallWithMamv.setVerifived(false);
            }
        } catch (Exception e) {
        }

        //  הסכום הפטור ממעמ בחשבונית. 
        try {
            double d = 0.0;
            if (infoReader.totalPriceNoTaxes8.equals("@") || infoReader.totalPriceNoTaxes8.equals("")) {
                d = 0.0;
            } else {
                d = Double.valueOf(infoReader.totalPriceNoTaxes8);
            }

            if (!sumallpaturv.getVerifived() && sumallpaturv.getSum() == 0) {
                sumallpaturv = new VerVar(d);
                sumallpaturv.setVerifived(false);
            }
        } catch (Exception e) {
        }

        // הסכום החייב במעמ בחשבונית
        try {
            double d = 0.0;
            if (infoReader.totalTaxes_Hayav_Bemaam9.equals("@") || infoReader.totalTaxes_Hayav_Bemaam9.equals("")) {
                d = 0.0;
            } else {
                d = Double.valueOf(infoReader.totalTaxes_Hayav_Bemaam9);
            }
            if (!sumallhayavv.getVerifived() && sumallhayavv.getSum() == 0 && d != 0) {
                sumallhayavv = new VerVar(d);
                sumallhayavv.setVerifived(false);
            }
        } catch (Exception e) {
        }

        // סכום המעמ הכולל שבחשבונית
        try {
            double d = 0.0;
            if (infoReader.totalTaxes10.equals("@") || infoReader.totalTaxes10.equals("")) {
                d = 0.0;
            } else {
                d = Double.valueOf(infoReader.totalTaxes10);
            }

            if (!sumallMamv.getVerifived() && sumallMamv.getSum() == 0 && d != 0) {
                sumallMamv = new VerVar(d);
                sumallMamv.setVerifived(false);
            }
        } catch (Exception e) {
        }

        /*sumallWithMamv = new VerVar(0)
        , // סכום עם מעמ
            sumallMamv = new VerVar(0)
        , // סכום המעמ
            sumallNoMamv = new VerVar(0)
        , // סכום בלי מעמ
            sumallpaturv = new VerVar(0)
        ,// סכום פטור ממעמ*/
        //sumallWithMamv.setSum(sumallMamv.getSum() + sumallpaturv.getSum() + sumallhayavv.getSum());
        if (Math.abs(sumallWithMamv.getSum() - (sumallhayavv.getSum() + sumallMamv.getSum() + sumallpaturv.getSum())) <= DeviationForBudget) {
            if (isDigitalFile) {
                sumallWithMamv.setVerifived(true);
                sumallMamv.setVerifived(true);
                sumallpaturv.setVerifived(true);
                sumallhayavv.setVerifived(true);
            }
        }

        /**
         * zvika-End *
         */
        /**
         * Fix numbers *
         */
        sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallWithMamv.getSum())));
        sumrowpricesHSv.setSum(Double.parseDouble(new DecimalFormat("##.##").format((sumrowpricesHSv.getSum()))));
        sumallNoMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format((sumallNoMamv.getSum()))));

        /**
         * Fix count *
         */
        if (!partscount.getVerifived()) {
            int ipartscount = 0;
            partscount.setSum(ipartscount);
            partscount.setVerifived(true);
        }
        /**
         * Fix With Mam *
         */
        if (!sumallWithMamv.getVerifived()) {
            // We need to add before maam the maam and verifived.
        }

        if (!sumallMamv.getVerifived()) {
            if (sumallWithMamv.getVerifived() && sumallNoMamv.getVerifived()) {
                sumallMamv.setSum(sumallWithMamv.getSum() - sumallNoMamv.getSum());
                sumallMamv.setVerifived(true);
                sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(sumallMamv.getSum())));

            }
        }

    }

    public boolean checkQtyforAllRows(Vector<BNode> bnv) {
        double sumcheck = 0;
        double countchecktemp_internal = 0;
        for (int i = 0; i < bnv.size(); i++) { //

            if (bnv.elementAt(i).price != null && bnv.elementAt(i).price.get_name_by_inv().contains("pos")) {

                if (bnv.elementAt(i).qty == null) {
                    return false;
                } else {
                    if (bnv.elementAt(i).qty.get_format().equalsIgnoreCase("INTP") && Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str())) > 0) //
                    {
                        countchecktemp_internal = Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str()));
                    } else if (!bnv.elementAt(i).qty.get_format().equalsIgnoreCase("INTP") && Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str())) > 0) //
                    {
                        double t = 0;
                        try {
                            t = Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str()));
                        } catch (Exception r) {
                        }

                        int k = (int) t;
                        t = t - k;
                        if (t != 0) {
                            t = 1;
                        } else {
                            t = k;
                        }
                        countchecktemp_internal = (int) t;
                    } else if (Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str())) > 0) //!bnv.elementAt(i).qty.get_format().equalsIgnoreCase("INTP") && 
                    {

                        double t = 0;
                        try {
                            t = Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str()));
                        } catch (Exception r) {
                        }

                        int k = (int) t;
                        t = t - k;
                        if (t != 0) {
                            t = 1;
                        } else {
                            t = k;
                        }
                        countchecktemp_internal = (int) t;

                    }

                    sumcheck += countchecktemp_internal;
                }
            }
        }

        partscount.setSum(sumcheck);
        partscount.setVerifived(true);
        return true;
    }

    /**
     * find qty of parts
     *
     * @param bnv
     * @return
     */
    public Vector<BNode> checkAndFilQTY(Vector<BNode> bnv) {

        //check countparts
        Vector<CheckQTY> vecCheckQTY = new Vector<CheckQTY>();
        double countchecktemp = 0,
                countchecktemp_internal = 0;

        //System.out.println("last in bnv : " + bnv.lastElement().price + " sumall: " + sumallWithMamv.getSum());
        // if (LFun.getpossub(Double.valueOf(LFun.fixdot(bnv.lastElement().price.get_result_str())), sumallWithMamv.getSum()) < 0.5) {
        //    lastindbnv = bnv.size() - 1;
        // }
        //check if all rows already have veryfied qty
        if (checkQtyforAllRows(bnv)) {

            return bnv;
        }

        for (int i = 0; i < bnv.size(); i++) { //

            if (bnv.elementAt(i).price != null && bnv.elementAt(i).price.get_name_by_inv().contains("pos")) {
                countchecktemp_internal = 0;
                if (bnv.elementAt(i).qty == null) {
                    if (bnv.elementAt(i).un_num != null && bnv.elementAt(i).un_num.size() > 0) {

                        CheckQTY cqty = new CheckQTY();
                        for (int j = 0; j < bnv.elementAt(i).un_num.size(); j++) {
                            if (bnv.elementAt(i).un_num.elementAt(j).get_name_by_inv().contains("num") && bnv.elementAt(i).un_num.elementAt(j).get_format().equalsIgnoreCase("INTP")) {
                                cqty.checkIntNum.add(bnv.elementAt(i).un_num.elementAt(j));
                                cqty.setPosInRootVector(i);
                            }
                        }
                        // if (!cqty.isEmpty()) {

                        if (!cqty.contains1()) {
                            cqty.add1(i);
                            ObData o = new ObData();
                            o.set_result_str("1");
                            bnv.elementAt(i).un_num.add(o);

                        }
                        vecCheckQTY.add(cqty);
                        // }
                        /*
                         for (  j = 0; j < bnv.elementAt(i).un_num.size() && countchecktemp_internal==0; j++) {
                         if(bnv.elementAt(i).un_num.elementAt(j).get_name_by_inv().contains("num") && bnv.elementAt(i).un_num.elementAt(j).get_format().equalsIgnoreCase("INTP")){
                         countchecktemp_internal=Integer.valueOf(LFun.fixdot(bnv.elementAt(i).un_num.elementAt(j).get_result_str()));
                         }
                         }
                         */
                        countchecktemp_internal = 0;
                    } else {
                        countchecktemp_internal = 1;

                        ObData oq = new ObData();
                        oq.set_result_str("1");
                        bnv.elementAt(i).set_o_qty(oq);

                        bnv.elementAt(i).set_o_one_price(bnv.elementAt(i).price);

                    }

                } else if (bnv.elementAt(i).qty.get_format().equalsIgnoreCase("INTP") && Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str())) > 0) //
                {
                    countchecktemp_internal = Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str()));
                } else if (!bnv.elementAt(i).qty.get_format().equalsIgnoreCase("INTP") && Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str())) > 0) //
                {
                    double t = 0;
                    try {
                        t = Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str()));
                    } catch (Exception r) {
                    }
                    if (t > 0 && t < 1) {
                        t = 1;
                    }
                    countchecktemp_internal = (int) t;
                } else if (Double.valueOf(LFun.fixdot(bnv.elementAt(i).qty.get_result_str())) > 0) //!bnv.elementAt(i).qty.get_format().equalsIgnoreCase("INTP") && 
                {
                    countchecktemp_internal = 1;

                }

                // if(countchecktemp_internal==0){
                //     countchecktemp_internal++;
                // }
                countchecktemp = countchecktemp + countchecktemp_internal;
            }
        }

        //intnum create from zanav
        for (int i = 0; i < od.size(); i++) {
            if (od.elementAt(i).get_format().equalsIgnoreCase("INTP")
                    && !od.elementAt(i).get_name_by_inv().contains("pn")) {
                try {
                    intnum.add(Integer.parseInt(od.elementAt(i).get_result_str()));
                } catch (Exception e) {
                }

            }

        }

        for (int i = 0; i < intnum.size(); i++) {
            if (intnum.elementAt(i) - (vecCheckQTY.size() + countchecktemp) < 0) {
                intnum.remove(i--);
            }

        }

        if (!intnum.contains((int) partscount.getSum())) {
            intnum.add((int) partscount.getSum());
        }

        for (int i = 0; i < intnum.size(); i++) {
            if (countchecktemp == intnum.elementAt(i) && vecCheckQTY.isEmpty()) {

                partscount.setSum(intnum.elementAt(i));
                partscount.setVerifived(true);
                return bnv;
            }
        }

        Vector<CheckSingleQTY> csqty = null;
        boolean findpcount = false;
        double stemp = 0;

        int maxunkint = 1;
        for (CheckQTY part : vecCheckQTY) {
            maxunkint = maxunkint * part.getSize();
        }
        System.out.println("multiple unknown integers: " + maxunkint);

        if (vecCheckQTY.isEmpty()) {

        } else if (!vecCheckQTY.isEmpty() && maxunkint < 1000) {

            Vector<Vector<CheckSingleQTY>> vec_csqty = LFun.generate(vecCheckQTY);

            outerloop:
            for (Vector<CheckSingleQTY> vector : vec_csqty) {
                System.out.println();
                int sumcol = 0;
                for (CheckSingleQTY checkSingleQTY : vector) {
                    System.out.print(" " + checkSingleQTY.checkNum + " [" + checkSingleQTY.posInRootVector + " , " + checkSingleQTY.posInSubVector + " ] ");
                    sumcol = sumcol + checkSingleQTY.checkNum;
                }
                if (intnum.contains(sumcol + countchecktemp) && !findpcount) {
                    csqty = vector;
                    stemp = sumcol + countchecktemp;
                    findpcount = true;
                } else if (intnum.contains(sumcol + countchecktemp) && findpcount) {
                    csqty = vector;
                    findpcount = false;
                    break outerloop;
                }
            }

            if (findpcount) {
                partscount.setSum(stemp);
                partscount.setVerifived(true);

                for (CheckSingleQTY v : csqty) {
                    bnv.elementAt(v.posInRootVector).set_o_qty(bnv.elementAt(v.posInRootVector).un_num.elementAt(v.posInSubVector));
                    bnv.elementAt(v.posInRootVector).un_num.remove(v.posInSubVector);

                    bnv.elementAt(v.posInRootVector).set_o_one_price(bnv.elementAt(v.posInRootVector).price);
                }

            } else {

            }

        }
        //end check countparts
        return bnv;
    }

    /**
     * search 1+1,1+2... anahot
     *
     * @param bnv
     * @return
     */
    public Vector<BNode> fil_1p1_1p2_anahot(Vector<BNode> bnv) {
        /*
         * search 1+1,1+2...
         */
        for (int i = 0; i < bnv.size(); i++) {
            int count_p = 0;
            double part_minus = 1;
            if (bnv.elementAt(i).price != null && bnv.elementAt(i).price.get_name_by_inv().contains("neg_price")) {

                for (int k = i - 1; k >= 0; k--) {
                    try {
                        if (bnv.elementAt(k).price != null && bnv.elementAt(k).price.get_name_by_inv().equalsIgnoreCase("pos_price")
                                && (Double.valueOf(LFun.fixdot(bnv.elementAt(k).price.get_result_str())) * (-1) == Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())))) {
                            count_p++;
                        }
                    } catch (Exception r) {
                    }

                }

                if (count_p > 1) {
                    try {
                        part_minus = Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())) / count_p;
                        part_minus = Double.parseDouble(new DecimalFormat("##.##").format(part_minus));
                        for (int k = i - 1; k >= 0; k--) {
                            if (bnv.elementAt(k).price != null && bnv.elementAt(k).price.get_name_by_inv().equalsIgnoreCase("pos_price")
                                    && (Double.valueOf(LFun.fixdot(bnv.elementAt(k).price.get_result_str())) * (-1) == Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())))) {
                                bnv.elementAt(k).anaha = "" + part_minus;
                            }

                        }
                    } catch (Exception r) {
                    }

                    bnv.remove(i);
                    i--;

                }

            }
        }

        /*
         * end search 1+1,1+2...
         */
        return bnv;
    }

    /**
     * fil general anahot
     *
     * @param bnv
     * @return
     */
    public Vector<BNode> fil_general_anahot(Vector<BNode> bnv) {
        for (int i = 0; i < bnv.size(); i++) {
            if (bnv.elementAt(i).price != null && bnv.elementAt(i).price.get_name_by_inv().equalsIgnoreCase("neg_price_u")) {
                boolean f = true;
                for (int k = i - 1; k >= 0 && f; k--) {
                    try {
                        if (bnv.elementAt(k).price != null && bnv.elementAt(k).price.get_name_by_inv().equalsIgnoreCase("pos_price")
                                && (Double.valueOf(LFun.fixdot(bnv.elementAt(k).price.get_result_str())) * (-1) == Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())))) {
                            bnv.elementAt(i).price.set_name_by_inv("neg_price_z");
                            f = false;
                        }
                    } catch (Exception r) {
                    }

                }
            }
        }

        boolean pn_exist = false;
        for (int i = 0; i < bnv.size(); i++) {
            if (bnv.elementAt(i).pn != null) {
                pn_exist = true;
                i = bnv.size();
            }
        }

        for (int i = 0; i < bnv.size(); i++) {
            if (bnv.elementAt(i).pn != null && bnv.elementAt(i).price != null && bnv.elementAt(i).price.get_name_by_inv().equalsIgnoreCase("neg_price_u")) {
                Vector<String> codes = IsContainSpecialCode(bnv.elementAt(i).pn.get_result_str());//new Vector<String>();
                if (codes.size() > 0)//code exist in list of codes
                {
                    //  int count_samepn=0;
                    //  for (int k = i-1; k >= 0; k--) {
                    // if(bnv.elementAt(k).pn!=null && codes.contains(bnv.elementAt(k).pn.get_result_str())){
                    //     count_samepn++;
                    // }
                    //   }

                    /*  for (int k = i - 1; k >= 0; k--) {
                     if (bnv.elementAt(k).pn != null && codes.contains(bnv.elementAt(k).pn.get_result_str())) {
                     double value = Double.valueOf(LFun.fixdot(bnv.elementAt(k).price.get_result_str())) * 0.1; // 50 need change to func getpercent by code
                     value = Double.valueOf(bnv.elementAt(k).anaha_by_percent) + ((-1) * value);
                     value = Double.parseDouble(new DecimalFormat("##.##").format(value));
                     bnv.elementAt(k).anaha_by_percent = "" + value;
                     }

                     }*/
                    double allprice = 0, val;
                    for (int k = i - 1; k >= 0; k--) {
                        if (bnv.elementAt(k).pn != null && codes.contains(bnv.elementAt(k).pn.get_result_str())) {
                            allprice = allprice + Double.valueOf(LFun.fixdot(bnv.elementAt(k).price.get_result_str()));
                        }
                    }
                    for (int k = i - 1; k >= 0; k--) {
                        if (bnv.elementAt(k).pn != null && codes.contains(bnv.elementAt(k).pn.get_result_str())) {
                            try {
                                val = Double.valueOf(LFun.fixdot(bnv.elementAt(k).price.get_result_str()));
                                val = Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())) * (val / allprice);
                                bnv.elementAt(k).anaha_by_percent = "" + Double.parseDouble(new DecimalFormat("##.##").format(val));
                            } catch (Exception r) {
                            }

                        }
                    }

                    bnv.removeElementAt(i);
                    i--;

                }
            }
        }

        int last_anaha = 0;
        for (int i = 0; i < bnv.size(); i++) {
            if (bnv.elementAt(i).price != null && bnv.elementAt(i).price.get_name_by_inv().equalsIgnoreCase("neg_price_a")) {
                try {
                    if (pn_exist && bnv.elementAt(i - 1).pn != null) {
                        String tpn = bnv.elementAt(i - 1).pn.get_result_str();
                        if (!ispart_existinlist(tpn)) {
                            int count_samepn = 1;

                            for (int k = i - 2; k >= last_anaha; k--) {
                                if (bnv.elementAt(k).pn != null && bnv.elementAt(k).pn.get_result_str().equalsIgnoreCase(tpn)
                                        && bnv.elementAt(i - 1).price.get_result_str().equalsIgnoreCase(bnv.elementAt(k).price.get_result_str())) {
                                    count_samepn++;
                                }
                            }

                            double anaha_by_part = 0;
                            try {
                                anaha_by_part = Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())) / count_samepn;
                            } catch (Exception r) {
                            }

                            for (int k = i - 1; k >= last_anaha; k--) {
                                if (bnv.elementAt(k).pn != null && bnv.elementAt(k).pn.get_result_str().equalsIgnoreCase(tpn)
                                        && bnv.elementAt(i - 1).price.get_result_str().equalsIgnoreCase(bnv.elementAt(k).price.get_result_str())) {
                                    bnv.elementAt(k).anaha = "" + anaha_by_part;
                                }
                            }
                            last_anaha = i;
                            bnv.removeElementAt(i);
                            i--;
                        } else {//part exist in list
                            int count_samepn = 1;

                            for (int k = i - 2; k >= last_anaha; k--) {
                                if (bnv.elementAt(k).pn != null && ispart_existinlist(bnv.elementAt(k).pn.get_result_str())) {
                                    count_samepn++;
                                }
                            }

                            double anaha_by_part = 0;
                            try {
                                anaha_by_part = Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())) / count_samepn;

                            } catch (Exception r) {
                            }
                            for (int k = i - 1; k >= last_anaha; k--) {
                                if (bnv.elementAt(k).pn != null && ispart_existinlist(bnv.elementAt(k).pn.get_result_str())) {
                                    bnv.elementAt(k).anaha = "" + anaha_by_part;
                                }
                            }
                            last_anaha = i;
                            bnv.removeElementAt(i);
                            i--;
                        }
                    } else {//without pn's
                        String tpn = bnv.elementAt(i - 1).get_description();
                        if (!isdescr_existinlist(tpn)) {
                            int count_samepn = 1;

                            for (int k = i - 2; k >= last_anaha; k--) {
                                if (compare_strings(bnv.elementAt(k).get_description(), tpn)
                                        && bnv.elementAt(i - 1).price.get_result_str().equalsIgnoreCase(bnv.elementAt(k).price.get_result_str())) {
                                    count_samepn++;
                                }

                            }

                            double anaha_by_part = 0;

                            try {
                                anaha_by_part = Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())) / count_samepn;
                            } catch (Exception r) {
                            }
                            for (int k = i - 1; k >= last_anaha; k--) {
                                if (compare_strings(bnv.elementAt(k).get_description(), tpn)
                                        && bnv.elementAt(i - 1).price.get_result_str().equalsIgnoreCase(bnv.elementAt(k).price.get_result_str())) {
                                    bnv.elementAt(k).anaha = "" + anaha_by_part;
                                }
                            }
                            last_anaha = i;
                            try {
                                double _mam = Double.valueOf(bnv.elementAt(i - 1).mam.get_result_str()) + Double.valueOf(bnv.elementAt(i).mam.get_result_str());
                                double _without_mam = Double.valueOf(bnv.elementAt(i - 1).without_mam.get_result_str()) + Double.valueOf(bnv.elementAt(i).without_mam.get_result_str());
                                double _with_mam = Double.valueOf(bnv.elementAt(i - 1).with_mam.get_result_str()) + Double.valueOf(bnv.elementAt(i).with_mam.get_result_str());

                                bnv.elementAt(i - 1).mam.set_result_str(_mam + "");
                                bnv.elementAt(i - 1).with_mam.set_result_str(_with_mam + "");
                                bnv.elementAt(i - 1).without_mam.set_result_str(_without_mam + "");
                            } catch (Exception e) {
                            }

                            bnv.removeElementAt(i);
                            i--;
                        } else {//description exist in list
                            int count_samepn = 1;

                            for (int k = i - 2; k >= last_anaha; k--) {
                                if (isdescr_existinlist(bnv.elementAt(k).get_description())) {
                                    count_samepn++;
                                }

                            }

                            double anaha_by_part = 0;
                            try {
                                anaha_by_part = Double.valueOf(LFun.fixdot(bnv.elementAt(i).price.get_result_str())) / count_samepn;

                            } catch (Exception r) {
                            }
                            for (int k = i - 1; k >= last_anaha; k--) {
                                if (isdescr_existinlist(bnv.elementAt(k).get_description())) {
                                    bnv.elementAt(k).anaha = "" + anaha_by_part;
                                }
                            }
                            last_anaha = i;
                            bnv.removeElementAt(i);
                            i--;
                        }
                    }
                } catch (Exception c) {
                }
                // last_anaha=i;
                // bnv.removeElementAt(i);
                // i--;
            }

        }

        return bnv;
    }

    private Vector<Double> getcomb(double item_price) {
        Vector<Double> com = new Vector<Double>();
        String val = String.valueOf(item_price),
                string = "";
        val = val.substring(0, val.indexOf("."));
        for (int i = 0; i < val.length(); i++) {
            try {
                com.add(Double.valueOf(val.substring(0, i) + "." + val.substring(i, val.length() - 1)));
            } catch (Exception e) {

            }
        }
        return com;
    }

    private Vector<String> InonVectorObDataToString(Vector<ObData> rpv) {
        Vector<String> ret = new Vector<String>();
        for (ObData ob : rpv) {
            ret.add(ob.get_result_str());
        }
        return ret;
    }

    private Vector<ObData> Copy(Vector<ObData> result) {
        Vector<ObData> RetVector = new Vector<ObData>();
        for (ObData obd : result) {
            RetVector.add(new ObData(obd));
        }
        return RetVector;
    }

    private Vector<BNode> copyBnv(Vector<BNode> bnv) {

        Vector<BNode> RetVector = new Vector<BNode>();
        for (BNode bn : bnv) {
            RetVector.add(new BNode(bn));
        }

        return RetVector;
    }

    private Vector<ObData> sortVectorByLine1(Vector<ObData> tempall) {
        int line_numers[] = new int[tempall.size()];
        int numberOfPages = od.elementAt(od.size() - 1).get_pfile_num();

        Vector<ObData> RetVector = new Vector<ObData>();
        int row[][] = new int[numberOfPages][tempall.size()];
        for (int[] p : row) {
            for (int i = 0; i < p.length; i++) {
                p[i] = -1;
            }
        }
        int p = 0;
        int lastPage = 0;
        Vector<ObData> sortObData = new Vector<ObData>();
        for (ObData o : tempall) {
            if (o.get_pfile_num() > lastPage) {
                p = 0;
            }
            row[o.get_pfile_num() - 1][p] = o.get_line_num();
            p++;
            lastPage = o.get_pfile_num();
        }

        Vector<Vector<Integer>> vint = new Vector<Vector<Integer>>();
        Vector<Integer> v = new Vector<Integer>();
        for (int i = 0; i < row.length; i++) {
            for (int j = 0; j < row[i].length; j++) {
                if (row[i][j] != -1) {
                    v.add(row[i][j]);
                }
            }
            vint.add(v);
            v = new Vector<Integer>();
        }
        Vector<Vector<Integer>> bubbleVector = new Vector<Vector<Integer>>();
        for (int i = 0; i < vint.size(); i++) {
            bubbleVector.add(BubbleSortVectorInteger(vint.get(i)));
        }
        for (int i = 0; i < bubbleVector.size(); i++) {
            for (int j = 0; j < bubbleVector.elementAt(i).size(); j++) {
                for (int k = 0; k < tempall.size(); k++) {
                    if (tempall.get(k).get_line_num() == bubbleVector.get(i).get(j) && tempall.get(k).get_pfile_num() - 1 == i) {
                        sortObData.add(tempall.get(k));
                        break;
                    }
                }
            }
        }

        tempall = sortObData;
        return tempall;
    }

    private Vector<ObData> sortVectorByLine(Vector<ObData> tempall) {
        int line_numers[] = new int[tempall.size()];
        int numberOfPages = od.elementAt(od.size() - 1).get_pfile_num();

        Vector<ObData> RetVector = new Vector<ObData>();
        int row[] = new int[tempall.size()],
                i = 0;
        Vector<ObData> sortObData = new Vector<ObData>();
        for (ObData o : tempall) {
            row[i] = o.get_line_num();
            i++;
        }
        i = 0;
        Vector<ObData> newod = new Vector<ObData>();
        Vector<Integer> vint = new Vector<Integer>();
        for (int r : row) {
            vint.add(r);
        }
        Vector<Integer> bubbleVector = new Vector<Integer>();
        bubbleVector = BubbleSortVectorInteger(vint);
        for (int k = 0; k < bubbleVector.size(); k++) {
            for (int j = 0; j < tempall.size(); j++) {
                if (tempall.get(j).get_line_num() == bubbleVector.get(k)) {
                    sortObData.add(tempall.get(j));
                    break;
                }
            }
        }
        tempall = sortObData;
        return tempall;
    }

    private void fill_external_source() {
        //lines.elementAt(2).get(0).get_name_by_inv()
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("pn")) {
                    lines.elementAt(i).elementAt(j).set_external_source("MAKAT");
                    lines.elementAt(i).elementAt(j).set_external_source("INTPSEP");
                }
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equals("desc")) {
                    lines.elementAt(i).elementAt(j).set_external_source("ITEMDESC");
                    lines.elementAt(i).elementAt(j).set_external_source("TXT");
                }
            }
        }

    }

    private void useResultVector(Vector<ObData> result, int start, int end) { // Find all the qty and the prices of the items. Have fun.
        int start_index = 0, end_index = 0;
        Vector<Integer> index = new Vector<Integer>();

        //עובר על כל וקטור ה-lines
        //ומגדירה את המחיר לפי הצלבת ה strings
        //עם הוקטור- result 
        for (int j = 0; j < result.size(); j++) {
            for (int i = start; i < end; i++) {
                for (int k = 0; k < lines.elementAt(i).size(); k++) {
                    if (lines.elementAt(i).elementAt(k).get_top() == result.get(j).get_top()
                            && lines.elementAt(i).elementAt(k).get_bottom() == result.get(j).get_bottom()
                            && lines.elementAt(i).elementAt(k).get_left() == result.get(j).get_left()
                            && lines.elementAt(i).elementAt(k).get_right() == result.get(j).get_right() && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains("neg_price")) {
                        lines.elementAt(i).elementAt(k).set_name_by_inv("price");
                        lines.elementAt(i).elementAt(k).set_result_str(result.get(j).get_result_str()); ///   LAST UPDATE
                        index.add(i);
                    }
                }
            }
        }
        index.add(end);/*
         lengthMatrix = new int[result.size()][2];//lengthMatrix[j][]
         for (int i = 0; i < result.size(); i++) {
         lengthMatrix[i][0] = index.get(i);
         lengthMatrix[i][1] = index.get(i) - 1;
         }
         lengthMatrix[lengthMatrix[0].length - 1][1] = end;//asdef
         */

        Vector<Double> resDouble = new Vector<Double>();

        for (int i = 0; i < index.size() - 1; i++) {
            Vector<Build> numbers = new Vector<Build>();
            for (int j = index.get(i); j <= index.get(i + 1); j++) {
                for (int k = 0; lines.size() > j && k < lines.elementAt(j).size(); k++) {
                    try {
                        try {
                            if (lines.elementAt(j).elementAt(k).get_name_by_inv().contains("price")) {
                                resDouble.add(Double.valueOf(lines.elementAt(j).elementAt(k).get_result_str()));
                            }
                        } catch (Exception e) {

                        }

                        Double.valueOf(lines.elementAt(j).elementAt(k).get_result_str().replaceAll(",", "."));
                        numbers.add(new Build(j, k));

                    } catch (Exception e) {
                        String a = "";
                    }
                }
            }
            for (int j = index.get(i) - 1; j < index.get(i); j++) {

                if (numbers.size() == 3) {
                    for (int k = 0; k < lines.elementAt(j).size(); k++) {
                        try {
                            try {
                                if (lines.elementAt(j).elementAt(k).get_name_by_inv().contains("price")) {
                                    resDouble.add(Double.valueOf(lines.elementAt(j).elementAt(k).get_result_str()));
                                }
                            } catch (Exception e) {
                            }

                            Double.valueOf(lines.elementAt(j).elementAt(k).get_result_str());
                            numbers.add(new Build(j, k));

                        } catch (Exception e) {
                            String a = "";
                        }
                    }
                }
            }

            /*for (ObData res : result) {
             try {
             resDouble.add(Double.valueOf(res.get_result_str()));
             } catch (Exception e) {

             }
             }*/
            numbers = setQtyPricePreOne(numbers, resDouble);
            try {
                if (numbers.size() == 1) {
                    lines.elementAt(numbers.get(0).i).elementAt(numbers.get(0).j).set_name_by_inv("pn");
                } else if (numbers.size() == 2) {
                    for (int b = 0; b < numbers.size(); b++) {
                        for (ObData ob : result) {
                            if (ob.get_result_str().equals(lines.elementAt(numbers.get(b).i).elementAt(numbers.get(b).j).get_result_str()) && !lines.elementAt(numbers.get(b).i).elementAt(numbers.get(b).j).get_name_by_inv().equals("qty") && !lines.elementAt(numbers.get(b).i).elementAt(numbers.get(b).j).get_name_by_inv().contains("one")) {

                                if (b == 0 && lines.elementAt(numbers.get(b + 1).i).elementAt(numbers.get(b + 1).j).get_result_str().length() >= Integer.valueOf(minlpn)
                                        && !lines.elementAt(numbers.get(b + 1).i).elementAt(numbers.get(b + 1).j).get_name_by_inv().equals("price")) {
                                    lines.elementAt(numbers.get(b + 1).i).elementAt(numbers.get(b + 1).j).set_name_by_inv("pn");
                                } else if (lines.elementAt(numbers.get(b - 1).i).elementAt(numbers.get(b - 1).j).get_result_str().length() >= Integer.valueOf(minlpn)
                                        && !lines.elementAt(numbers.get(b - 1).i).elementAt(numbers.get(b - 1).j).get_name_by_inv().equals("price")) {
                                    lines.elementAt(numbers.get(b - 1).i).elementAt(numbers.get(b - 1).j).set_name_by_inv("pn");
                                }

                            }
                        }
                    }
                } else {
                    Build big = null;
                    for (int build = 0; build < numbers.size(); build++) {
                        if ((Double.valueOf(lines.elementAt(numbers.get(build).i).elementAt(numbers.get(build).j).get_result_str())) > (Double.valueOf(lines.elementAt(big.i).elementAt(big.j).get_result_str()))) {
                            big = numbers.get(build);
                        }
                    }
                    if (lines.elementAt(big.i).elementAt(big.j).get_result_str().length() >= Integer.valueOf(minlpn)) {
                        lines.elementAt(big.i).elementAt(big.j).set_name_by_inv("pn");
                    }

                }
            } catch (Exception e) {

            }
        }
    }

    private Vector<ObData> CleanVector(Vector<ObData> temp, boolean isYael) {
        for (int i = 0; i < temp.size(); i++) {
            if (!temp.get(i).get_result_str().contains(".") && !isYael) {
                String point = temp.get(i).get_result_str().substring(0, temp.get(i).get_result_str().length() - 2) + "." + temp.get(i).get_result_str().substring(temp.get(i).get_result_str().length() - 2, temp.get(i).get_result_str().length());
                temp.get(i).set_result_str(point);
            }
            for (int j = 0; j < temp.size(); j++) { // Clear the same numbers
                if (i != j) {
                    if (temp.get(i).get_left() == temp.get(j).get_left()
                            && temp.get(i).get_right() == temp.get(j).get_right()
                            && temp.get(i).get_top() == temp.get(j).get_top()
                            && temp.get(i).get_bottom() == temp.get(j).get_bottom()) {
                        temp.remove(i);
                        i++;
                        break;
                    }

                }
            }
        }
        //   asd

        return temp;
    }

    private int getBalance(Vector<Integer> rightCor) {
        int count = 0;
        for (int i = 0; i < rightCor.size(); i++) {
            for (int corin : rightCor) {
                if (Math.abs(corin - rightCor.get(i)) > DEVIATION) {
                    count++;
                }
            }
            if (count == rightCor.size() - 1 && rightCor.size() > 1) {
                rightCor.remove(i);
            }
            count = 0;
        }
        if (rightCor.size() <= 1) {
            isBigThenOne = false;
        }
        return rightCor.get(0);
    }

    private Vector<Vector<ObData>> removeSeqIncludeNotDesc(Vector<Vector<ObData>> wordSequences, BNode block, Vector<Integer> DescIndex) {
        if (DescIndex.size() == 0) {
            for (int i = 0; i < wordSequences.size(); i++) {
                boolean IncludeNotDesc = false;
                for (int j = 0; j < wordSequences.get(i).size() && !IncludeNotDesc; j++) {
                    if (!wordSequences.get(i).get(j).get_name_by_inv().equalsIgnoreCase("desc")
                            && !wordSequences.get(i).get(j).get_name_by_inv().equalsIgnoreCase("num")
                            && !wordSequences.get(i).get(j).get_name_by_inv().equalsIgnoreCase("free")
                            && !wordSequences.get(i).get(j).get_name_by_inv().equalsIgnoreCase("date")
                            && !wordSequences.get(i).get(j).get_name_by_inv().equalsIgnoreCase("")) {
                        IncludeNotDesc = true;
                    }
                }
                if (IncludeNotDesc) {
                    for (int j = 0; j < wordSequences.get(i).size(); j++) {
                        if (wordSequences.elementAt(i).elementAt(j).get_name_by_inv().equals("desc")) {
                            block.descr.remove(wordSequences.elementAt(i).elementAt(j));
                            wordSequences.elementAt(i).elementAt(j).set_name_by_inv("free");
                        }
                    }

                    wordSequences.remove(i);
                    i--;
                }
            }
        } else {
            Vector<ObData> DescWords = new Vector<ObData>();
            for (int i = 0; i < DescIndex.size(); i++) {
                for (int j = 0; j < wordSequences.size(); j++) {
                    if (DescIndex.get(i) == j) {
                        for (int k = 0; k < wordSequences.get(j).size(); k++) {
                            if (wordSequences.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("desc")
                                    || wordSequences.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("num")
                                    || wordSequences.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("free")
                                    || wordSequences.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("date")
                                    || wordSequences.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("")) {
                                DescWords.add(wordSequences.elementAt(j).elementAt(k));
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < wordSequences.size(); i++) {
                for (int j = 0; j < wordSequences.get(i).size(); j++) {
                    try {
                        block.descr.remove(wordSequences.elementAt(i).elementAt(j));
                    } catch (Exception e) {
                    }
                    if (wordSequences.elementAt(i).elementAt(j).get_name_by_inv().equals("desc")) {
                        wordSequences.elementAt(i).elementAt(j).set_name_by_inv("free");
                    }
                }
                wordSequences.remove(i);
            }
            wordSequences.clear();
            wordSequences.add(new Vector<ObData>());
            for (int i = 0; i < DescWords.size(); i++) {
                DescWords.get(i).set_name_by_inv("desc");
                block.descr.add(DescWords.get(i));
                wordSequences.get(0).add(DescWords.get(i));
            }
            try {

                if (DescriptionWordsVector != null && DescriptionWordsVector.size() > 0 && DescriptionWordsVector.get(0) != null) {
                    int DescriptionWordsVector_left = DescriptionWordsVector.get(0).get_left();
                    DescriptionWordsVector_left = Math.min(DescriptionWordsVector_left, DescWords.get(DescWords.size() - 1).get_left());
                    int DescriptionWordsVector_right = DescriptionWordsVector.get(0).get_right();
                    DescriptionWordsVector_right = Math.max(DescriptionWordsVector_right, DescWords.get(0).get_right());

                    for (int j = 0; j < block.descr.size(); j++) {
                        if (!ObDataWithLines.twoLineMeet(DescriptionWordsVector_left, DescriptionWordsVector_right,
                                block.descr.get(j).get_left(), block.descr.get(j).get_right(), DEVIATION)) {
                            if (block.descr.get(j).get_name_by_inv().equals("desc")) {
                                block.descr.get(j).set_name_by_inv("free");
                            }
                            block.descr.remove(j--);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Collections.sort(block.descr);
        }
        return wordSequences;

    }

    private Vector<String[]> deleteDuplicate(Vector<String[]> v) {
        for (int i = 0; i < v.size(); i++) {
            for (int j = 0; j < v.size(); j++) {
                if (i != j) {
                    if (isSame(v.get(i), v.get(j))) {
                        v.remove(j);
                        j--;
                    }
                }
            }
        }

        return v;
    }


    private void printBNV(Vector<BNode> bnv) {
        int i = 0;
        for (BNode bNode : bnv) {
            String t = "";
            if (bNode.price != null) {
                t += "price=" + bNode.price.get_result_str() + "; ";
            }

            if (bNode.qty != null) {
                t += "qty=" + bNode.qty.get_result_str() + "; ";
            }

            if (bNode.one_price != null) {
                t += "one_price=" + bNode.one_price.get_result_str().replaceAll("\r", "") + "; ";
            }

            t += "desc=" + bnv.get(0).get_description_for_CSV() + ";";
            write(p_filename, "printBNV()", "bnv[" + (i++) + "]", t.replaceAll("\r", ""), true);
        }
    }

    private void printBNV_withMatchesToItems(MatchesToItems bnvWithMatchesToItems) {
        int i = 0;
        for (MatchesToOneItem block : bnvWithMatchesToItems.MatchesToItems) {
            String str = "";
            str += "matches_is_fictitious=" + String.valueOf(block.matches_is_fictitious) + ": ";
            int k;
            String[] strArr = block.matches.elementAt(0).match;
            //for (String match:  block.matches.elementAt(0).match){
            for (k = 0; k < strArr.length - 1; k++) {
                if (strArr[k] != null && strArr[k] != "" && !strArr[k].isEmpty())
                    str += "match[" + k + "]=" + strArr[k] + "; ";     
            }
            write(p_filename, "printBNV_withMatchesToItems()", "BNVwithMatchesToItems[" + (i++) + "]", str, true);
        }              
     }
    
    
    private class Build {

        int i = 0;
        int j = 0;

        public Build(int ii, int jj) {
            i = ii;
            j = jj;
        }

    }

    private ObData CleanLetters(ObData elementAt) {
        String toEdit = elementAt.get_result_str(),
                edited = "";
        for (int i = 0; i < toEdit.length(); i++) {
            if (toEdit.charAt(i) == '0' || toEdit.charAt(i) == '1' || toEdit.charAt(i) == '2' || toEdit.charAt(i) == '3' || toEdit.charAt(i) == '4' || toEdit.charAt(i) == '5'
                    || toEdit.charAt(i) == '6' || toEdit.charAt(i) == '7' || toEdit.charAt(i) == '8' || toEdit.charAt(i) == '9') {
                edited = edited + toEdit.charAt(i);
            }
        }

        elementAt.set_result_str(edited);
        return elementAt;
    }

    private Vector<Build> setQtyPricePreOne(Vector<Build> numbers, Vector<Double> resDouble) {
        boolean stop = false;
        if (numbers.size() > 2) {
            double[] ret = new double[3];
            for (int i = 0; i < numbers.size() && !stop; i++) {
                if ((String.valueOf(lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).get_result_str())).length() < 7
                        && (!sumrowpricesv.getVerifived() || (sumrowpricesv.getVerifived() && !lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).get_name_by_inv().contains("price")))) {
                    Vector<String> b_vector = createSimilarVectorWithLimit(String.valueOf(lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).get_result_str()), 1);

                    for (int ii = 0; ii < numbers.size() && !stop; ii++) {
                        if (i != ii) {
                            if ((String.valueOf(lines.elementAt(numbers.get(ii).i).elementAt(numbers.get(ii).j).get_result_str())).length() < 7
                                    && (!sumrowpricesv.getVerifived() || (sumrowpricesv.getVerifived() && !lines.elementAt(numbers.get(ii).i).elementAt(numbers.get(ii).j).get_name_by_inv().contains("price")))) {

                                Vector<String> c_vector = createSimilarVectorWithLimit(String.valueOf(lines.elementAt(numbers.get(ii).i).elementAt(numbers.get(ii).j).get_result_str()), 1);

                                for (int j = 0; j < b_vector.size() && !stop; j++) {
                                    Vector<Double> tempSimilar = new Vector<Double>();
                                    tempSimilar.add(Double.valueOf(b_vector.get(j).replaceAll(",", ".")));
                                    for (int jj = 0; jj < c_vector.size() && !stop; jj++) {
                                        tempSimilar.add(Double.valueOf(c_vector.get(jj).replaceAll(",", ".")));
                                        for (int k = 0; k < numbers.size() && !stop; k++) {
                                            if (k != i && k != ii && !tempSimilar.contains(Double.valueOf(lines.elementAt(numbers.get(k).i).elementAt(numbers.get(k).j).get_result_str().replaceAll(",", ".")))) {
                                                tempSimilar.add(Double.valueOf(lines.elementAt(numbers.get(k).i).elementAt(numbers.get(k).j).get_result_str().replaceAll(",", ".")));
                                            }
                                        }
                                        for (int k = 0; k < tempSimilar.size() - 1 && !stop; k++) {
                                            for (int p = k + 1; p < tempSimilar.size() && !stop; p++) {
                                                if (tempSimilar.get(p) == tempSimilar.get(k)) {
                                                    tempSimilar.remove(p);
                                                    p--;
                                                }
                                            }
                                        }

                                        for (int k = 0; k < tempSimilar.size() - 2 && !stop; k++) {
                                            for (int p = k + 1; p < tempSimilar.size() - 1 && !stop; p++) {
                                                for (int g = p + 1; g < tempSimilar.size() && !stop; g++) {
                                                    if (tempSimilar.get(k) != tempSimilar.get(p) && tempSimilar.get(p) != tempSimilar.get(g) && tempSimilar.get(k) != tempSimilar.get(g)) {
                                                        if (Double.valueOf(tempSimilar.get(k)) * Double.valueOf(tempSimilar.get(p)) == tempSimilar.get(g)
                                                                && resDouble.contains(tempSimilar.get(g))) {
                                                            ret[0] = tempSimilar.get(g);
                                                            ret[1] = tempSimilar.get(k);
                                                            ret[2] = tempSimilar.get(p);
                                                            stop = true;
                                                        } else if (Double.valueOf(tempSimilar.get(k)) * Double.valueOf(tempSimilar.get(g)) == tempSimilar.get(p)
                                                                && resDouble.contains(tempSimilar.get(p))) {
                                                            ret[0] = tempSimilar.get(p);
                                                            ret[1] = tempSimilar.get(k);
                                                            ret[2] = tempSimilar.get(g);
                                                            stop = true;
                                                        } else if (Double.valueOf(tempSimilar.get(p)) * Double.valueOf(tempSimilar.get(g)) == tempSimilar.get(k)
                                                                && resDouble.contains(tempSimilar.get(k))) {
                                                            ret[0] = tempSimilar.get(k);
                                                            ret[1] = tempSimilar.get(p);
                                                            ret[2] = tempSimilar.get(g);
                                                            stop = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        tempSimilar.remove(Double.valueOf(c_vector.get(jj)));
                                    }

                                }
                            }
                        }
                    }
                }
            }
            if (ret.length > 0 && ret[0] != 0 && ret[1] != 0 && ret[2] != 0) {
                try {
                    Build b = null, a = null;
                    for (int i = 0; i < numbers.size(); i++) {
                        if ((String.valueOf(lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).get_result_str())).length() < 7 && (!lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).get_name_by_inv().contains("price") && sumrowpricesv.getVerifived())) {
                            Vector<String> svector = createSimilarVectorWithLimit(String.valueOf(lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).get_result_str()), 1);
                            for (String s : svector) {
                                if (String.valueOf(Double.valueOf(s)).equals(String.valueOf(ret[0]))) {
                                    lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).set_result_str(setPoint(String.valueOf(ret[0])));
                                    //lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).set_name_by_inv("price");
                                    numbers.remove(numbers.get(i));
                                    i--;
                                    break;
                                } else if (String.valueOf(Double.valueOf(s)).equals(String.valueOf(ret[1]))) {
                                    lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).set_result_str(setPoint(String.valueOf(ret[1])));
                                    a = numbers.get(i);
                                    break;
                                } else if (String.valueOf(Double.valueOf(s)).equals(String.valueOf(ret[2]))) {
                                    lines.elementAt(numbers.get(i).i).elementAt(numbers.get(i).j).set_result_str(setPoint(String.valueOf(ret[2])));
                                    b = numbers.get(i);
                                    break;
                                }
                            }
                        }
                    }
                    if (doc_direct.equals("right") && !isA4) {
                        if (lines.elementAt(a.i).elementAt(a.j).get_left() >= lines.elementAt(b.i).elementAt(b.j).get_left()) {
                            lines.elementAt(b.i).elementAt(b.j).set_name_by_inv("one_pr");
                            lines.elementAt(a.i).elementAt(a.j).set_name_by_inv("qty");
                            numbers.remove(a);
                            numbers.remove(b);

                        } else {
                            lines.elementAt(b.i).elementAt(b.j).set_name_by_inv("qty");
                            lines.elementAt(a.i).elementAt(a.j).set_name_by_inv("one_pr");
                            numbers.remove(a);
                            numbers.remove(b);
                        }
                        return numbers;
                    } else if (!isA4) {
                        if (lines.elementAt(a.i).elementAt(a.j).get_left() <= lines.elementAt(b.i).elementAt(b.j).get_left()) {
                            lines.elementAt(b.i).elementAt(b.j).set_name_by_inv("one_pr");
                            lines.elementAt(a.i).elementAt(a.j).set_name_by_inv("qty");
                            numbers.remove(a);
                            numbers.remove(b);

                        } else {
                            lines.elementAt(b.i).elementAt(b.j).set_name_by_inv("qty");
                            lines.elementAt(a.i).elementAt(a.j).set_name_by_inv("one_pr");
                            numbers.remove(a);
                            numbers.remove(b);
                        }
                        return numbers;
                    }
                } catch (Exception c) {

                }
            }

        }
        return numbers;
    }

    private String setPoint(String num) {
        if (num.substring(num.indexOf("."), num.length()).length() == 2) {
            num = num + "0";
            return num;
        }
        return num;

    }

    class TopRight {

        public int right[], top[];

        public TopRight(int size) {
            right = new int[size];
            top = new int[size];
        }

    }

    private void sort_od() {
        TopRight row = new TopRight(od.size());
        int i = 0;
        Vector<Integer> sortInteger = new Vector<Integer>();
        for (ObData o : od) {
            row.right[i] = o.get_right();
            row.top[i] = o.get_top();
            i++;
        }
        i = 0;
        int lastTop = row.top[0];
        Vector<ObData> newod = new Vector<ObData>();
        while (i < od.size()) {
            lastTop = row.top[i];
            Vector<Integer> vint = new Vector<Integer>();
            while (row.top[i] == lastTop) {
                vint.add(row.right[i]);
                if (i < od.size() - 1) {
                    i++;
                } else {
                    i++;
                    break;
                }
            }
            Vector<Integer> bubbleVector = new Vector<Integer>();
            bubbleVector = BubbleSort(vint);
            for (int bv : bubbleVector) {
                sortInteger.add(bv + i - bubbleVector.size());
            }
        }
        for (int sint : sortInteger) {
            newod.add(od.get(sint));
        }
        od = newod;
    }

    public Vector<Integer> BubbleSort(Vector<Integer> num) {
        int j;
        Vector<Integer> numbers = new Vector<Integer>();
        for (int i = 0; i < num.size(); i++) {
            numbers.add(i);
        }
        boolean flag = true;   // set flag to true to begin first pass
        int temp, mytemp;   //holding variable

        while (flag) {
            flag = false;    //set flag to false awaiting a possible swap
            for (j = 0; j < num.size() - 1; j++) {
                if (num.get(j) < num.get(j + 1)) // change to > for ascending sort
                {
                    temp = num.get(j);                //swap elements
                    num.set(j, num.get(j + 1));
                    num.set(j + 1, temp);

                    mytemp = numbers.get(j);
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, mytemp);
                    flag = true;              //shows a swap occurred  
                }

            }
        }
        return numbers;
    }

    public Vector<Integer> BubbleSortVectorInteger(Vector<Integer> num) {
        int j;
        boolean flag = true;   // set flag to true to begin first pass
        int temp, mytemp;   //holding variable
        int lastPage = 0;

        while (flag) {
            flag = false;    //set flag to false awaiting a possible swap
            for (j = 0; j < num.size() - 1; j++) {
                if (num.get(j) < num.get(j + 1)) // change to > for ascending sort
                {
                    temp = num.get(j);                //swap elements
                    num.set(j, num.get(j + 1));
                    num.set(j + 1, temp);
                    flag = true;              //shows a swap occurred  
                }
            }
        }
        Vector<Integer> numbers = new Vector<Integer>();
        for (int i = num.size() - 1; i > -1; i--) {
            numbers.add(num.elementAt(i));
        }
        return numbers;
    }

    /**
     * creation rows vector for all rows parsing main objects vector od
     *
     * @param vnumdefault
     * @param vnum
     * @param result
     */
    /*
     public void creationRowsVectorForAllRows(Vector<String> vnumdefault, Vector<String> vnum, Vector<ObData> result) {
     int counr = 0;
     int line = od.elementAt(0).get_line_num();
     Vector<ObData> oneline = new Vector<ObData>();
     for (int i = 0; i < od.size() - 1; i++) {//counr < result.size()     
     if (od.elementAt(i).get_line_num() > line) {
     line = od.elementAt(i).get_line_num();
     lines.add(new Vector<ObData>(oneline));
     oneline.removeAllElements();
     }

     try { // GET MAAM
     if (od.elementAt(i).get_bottom() >= od.elementAt(od.size() - 1).get_bottom() / 2) {
     if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) > 0) {
     biggest_numbers.add(Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())));
     }
     }
     } catch (Exception e) {

     }

     try {
     boolean is_equals = false;
     if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) < 0 && hanaha_Vector.size() <= counr) {
     for (Hanaha h : hanaha_Vector) {
     if (h.item_id.equals(String.valueOf(counr)) && h.hanaha.get_result_str().equals(od.elementAt(i).get_result_str())) {
     is_equals = true;
     }
     }
     if (!is_equals) {
     hanaha_Vector.add(new Hanaha(String.valueOf(counr), od.elementAt(i)));
     od.elementAt(i).set_name_by_inv("anaha");
     od.elementAt(i).set_num__by_inv(counr);
     }

     }
     } catch (Exception ex) {
     }

     if (counr < result.size()
     && LFun.contain_type(vnum, od.elementAt(i).get_format())
     && od.elementAt(i).get_result_str().equalsIgnoreCase(result.elementAt(counr).get_result_str())//
     && od.elementAt(i).get_top() == result.elementAt(counr).get_top()//
     && od.elementAt(i).get_left() == result.elementAt(counr).get_left()) {//
     if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) < 0) {
     od.elementAt(i).set_name_by_inv("neg_price");
     od.elementAt(i).set_num_block_by_inv(counr);
     } else if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) >= 0) {
     od.elementAt(i).set_name_by_inv("pos_price");
     od.elementAt(i).set_num_block_by_inv(counr);
     }

     counr++;

     } else if (od.elementAt(i).get_format().equalsIgnoreCase("UCURRATE") || od.elementAt(i).get_format().equalsIgnoreCase("ECURRATE")) {
     od.elementAt(i).set_name_by_inv("rate");
     //System.out.println(od.elementAt(i).get_result_str());
     } else if (od.elementAt(i).get_format().contains("DATE")) {
     od.elementAt(i).set_name_by_inv("date");
     //System.out.println(od.elementAt(i).get_result_str());
     } else if ((od.elementAt(i).get_format().equalsIgnoreCase("INTP") && (LFun.ispn(od.elementAt(i)))) || od.elementAt(i).get_format().equalsIgnoreCase("INTPSEP")) {//Long.parseLong(od.elementAt(i).get_result_str())/1000)>1
     od.elementAt(i).set_name_by_inv("pn");
     } else {
     if (LFun.contain_type(vnumdefault, od.elementAt(i).get_format())) {
     od.elementAt(i).set_name_by_inv("num");
     }
     }

     if (od.elementAt(i).get_line_num() == line) {
     if (od.elementAt(i).get_format().equalsIgnoreCase("GENERAL")) {
     //parsing
     // System.out.println(od.elementAt(i).get_result_str());
     if (LFun.iscontain_num(od.elementAt(i).get_result_str())) {
     Vector<ObData> vp = get_gen_parse(od.elementAt(i));
     for (int j = 0; j < vp.size(); j++) {

     oneline.add(vp.elementAt(j));
     //  System.out.println(vp.elementAt(j).get_result_str());
     }
     }
     } else {
     oneline.add(od.elementAt(i));
     }

     }

     }
     }
     */
    /**
     * creation rows vector for all rows parsing main objects vector od
     *
     * @param vnumdefault
     * @param vnum
     * @param result
     */
    public Vector<ObData> creationRowsVectorForAllRows(Vector<String> vnumdefault, Vector<String> vnum, Vector<ObData> result) {
        int counr = 0;
        Double num = 0.00;
        Vector<ObData> fixedPrices = new Vector<ObData>();
        int line = od.elementAt(0).get_line_num();
        Vector<ObData> oneline = new Vector<ObData>();
        int numberOfPagesToAdd = 0;
        boolean needToAdd = false;
        int numberPage = 1;

        //fix precene:
        for (int i = 1; i < od.size() - 1; i++) {
            try {
                if (od.elementAt(i).get_result_str().equals("%")) {
                    if (od.elementAt(i).get_line_num() == od.elementAt(i - 1).get_line_num() && od.elementAt(i).get_line_num() == od.elementAt(i + 1).get_line_num()
                            && od.elementAt(i - 1).get_format().equalsIgnoreCase("LATTXT")
                            && (od.elementAt(i + 1).get_format().equalsIgnoreCase("USADD") || od.elementAt(i + 1).get_format().equalsIgnoreCase("INTP"))) {
                        Double mam = Double.valueOf(od.elementAt(i + 1).get_result_str());
                        boolean isMam = false;
                        if (config.getTaxWords().contains(od.elementAt(i - 1).get_result_str().toLowerCase())) {
                            isMam = true;
                        } else {
                            for (String word : mam_percents) {
                                if (Double.valueOf(word) == mam) {
                                    isMam = true;
                                }
                            }
                        }
                        if (isMam) {
                            od.elementAt(i).set_result_str(od.elementAt(i + 1).get_result_str() + od.elementAt(i).get_result_str());
                            od.elementAt(i).set_right(od.elementAt(i).get_right());
                            od.elementAt(i).set_name_by_inv("percent");
                            od.remove(i + 1);
                        }
                    }
                }
            } catch (Exception e) {

            }
        }

        for (int i = 0; i < od.size() - 1; i++) {//counr < result.size()
            if (i != 0 && numberPage < od.elementAt(i).get_pfile_num()) {
                numberPage = od.elementAt(i).get_pfile_num();
                line = 0;
                numberOfPagesToAdd = od.elementAt(i - 1).get_line_num();
                needToAdd = true;
            }

            if (needToAdd) {
                od.elementAt(i).set_line_num(od.elementAt(i).get_line_num() + numberOfPagesToAdd);
            }

            // add to lines the previous line 
            if (od.elementAt(i).get_line_num() > line) {
                while (lines.size() < line - 1) {
                    lines.add(new Vector<ObData>());
                }
                line = od.elementAt(i).get_line_num();
                lines.add(new Vector<ObData>(oneline));
                oneline.removeAllElements();
            }

            try {
                if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) < 0 && hanaha_Vector.size() <= counr) {

                    hanaha_Vector.add(new Hanaha(String.valueOf(counr), od.elementAt(i)));

                    od.elementAt(i).set_name_by_inv("anaha");
                    od.elementAt(i).set_num_block_by_inv(counr);
                    //neg_num_vec.add(od.elementAt(i));
                }
            } catch (Exception ex) {
            }

            if (counr < result.size()
                    && LFun.contain_type(vnum, od.elementAt(i).get_format())
                    //&& od.elementAt(i).get_result_str().equals(result.elementAt(counr).get_result_str())
                    && od.elementAt(i).get_top() == result.elementAt(counr).get_top()
                    && od.elementAt(i).get_left() == result.elementAt(counr).get_left()) {
                if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) < 0) {
                    od.elementAt(i).set_name_by_inv("neg_price");
                    od.elementAt(i).set_num_block_by_inv(counr);
                } else if (Double.valueOf(LFun.fixdot(od.elementAt(i).get_result_str())) >= 0) {
                    od.elementAt(i).set_name_by_inv("pos_price");
                    od.elementAt(i).set_num_block_by_inv(counr);
                    result.elementAt(counr).set_line_num(od.elementAt(i).get_line_num());
                }

                counr++;

            } else if ((od.elementAt(i).get_format().equalsIgnoreCase("UCURRATE") || od.elementAt(i).get_format().equalsIgnoreCase("ECURRATE"))
                    && !(od.elementAt(i).get_result_str().endsWith(".2017") || od.elementAt(i).get_result_str().endsWith(".2018") || od.elementAt(i).get_result_str().endsWith(".2019"))) {
                od.elementAt(i).set_name_by_inv("rate");
                ExchangeRatePrices.add(od.elementAt(i));
            } else if (od.elementAt(i).get_format().contains("DATE")) {
                od.elementAt(i).set_name_by_inv("date");
            } else if ((od.elementAt(i).get_format().equalsIgnoreCase("INTP") && LFun.ispn(od.elementAt(i))) || ((LFun.ispn(od.elementAt(i))) && od.elementAt(i).get_format().equalsIgnoreCase("INTPSEP")) || (LFun.isCanBePnInA4(od.elementAt(i)) && isA4 && !LFun.isDouble(od.elementAt(i).get_result_str()))) {//Long.parseLong(od.elementAt(i).get_result_str())/1000)>1
                /* If the length of the String is 2 we will search the number in the makatCsu, 
                 only if it`s exists in the makatCsu we mark it as pn */
                if (od.elementAt(i).get_result_str().length() == 2) {
                    boolean isSet = false;
                    for (String[] oneLine : makatcsu) {
                        if (oneLine[0].equals(od.elementAt(i).get_result_str())) {
                            od.elementAt(i).set_name_by_inv("pn");
                            isSet = true;
                            break;
                        }
                    }
                    if (!isSet) {
                        od.elementAt(i).set_name_by_inv("num");
                    }
                } else {
                    od.elementAt(i).set_name_by_inv("pn");
                }

            } else {
                if (LFun.contain_type(vnumdefault, od.elementAt(i).get_format())) {
                    od.elementAt(i).set_name_by_inv("num");
                }
            }

            if (od.elementAt(i).get_name_by_inv().equals("") && od.elementAt(i).get_format().equalsIgnoreCase("GENERAL")) {
                //for (String sign : GlobalVariables.RateSign) {
                //    if (od.elementAt(i).get_result_str().equalsIgnoreCase(sign)) {
                if (Check.Currency.getCurrency(od.elementAt(i)) != null) {
                    od.elementAt(i).set_name_by_inv("matbeya");
                    MatbeyaAllWordsVector.add(od.elementAt(i));
                }
                //}
            }

            if (od.elementAt(i).get_line_num() == line) {
                if (od.elementAt(i).get_format().equalsIgnoreCase("GENERAL") || od.elementAt(i).get_format().equalsIgnoreCase("USAD") || od.elementAt(i).get_format().equalsIgnoreCase("EURD") || od.elementAt(i).get_format().equalsIgnoreCase("USADN")) {
                    //if the String contain number
                    if (LFun.iscontain_num(od.elementAt(i).get_result_str())) {
                        if (!sumrowpricesv.getVerifived()) {
                            //if it can be a price
                            if (price_By_place(od.elementAt(i), result) && od.elementAt(i).get_result_str().charAt(0) != '-' && od.elementAt(i).get_format().equalsIgnoreCase("GENERAL")) {

                                od.elementAt(i).set_name_by_inv("pos_price");

                                //if we can fix the string - at most two letters
                                if (can_Fix_Suspicious_Price(od.elementAt(i).get_result_str()) && !isDigitalFile) {
                                    num = fix_price(od.elementAt(i).get_result_str());
                                    if (num != 0.00) {
                                        ObData ob = od.elementAt(i);
                                        ob.set_result_str(String.valueOf(num));
                                        fixedPrices.add(ob);
                                        oneline.add(ob);
                                    } else {
                                        oneline.add(od.elementAt(i));
                                    }
                                } else {
                                    oneline.add(od.elementAt(i));
                                }
                            } else if (price_By_place(od.elementAt(i), result) && (od.elementAt(i).get_format().equalsIgnoreCase("USAD") || od.elementAt(i).get_format().equalsIgnoreCase("EURD") || od.elementAt(i).get_format().equalsIgnoreCase("USADN"))) {
                                usadAndEurdNum.add(od.elementAt(i));
                                oneline.add(od.elementAt(i));
                            } else {
                                oneline.add(od.elementAt(i));
                            }

                        } else if (!isA4 || (isA4 && !LFun.isCanBePnInA4(od.elementAt(i)))) {
                            Vector<ObData> vp = get_gen_parse(od.elementAt(i));

                            for (int j = 0; j < vp.size(); j++) {

                                oneline.add(vp.elementAt(j));
                                //  System.out.println(vp.elementAt(j).get_result_str());
                            }
                        } else {
                            oneline.add(od.elementAt(i));
                        }
                    } else {
                        oneline.add(od.elementAt(i));
                    }
                } else {
                    oneline.add(od.elementAt(i));
                }
            }

        }
        return fixedPrices;
    }

    /*    INONCODE - get the date from the invoice      */
    private InvoiceDate isInvoiceDate(String date) {
        int p[] = new int[2];
        for (int i = 0; i < date.length(); i++) {
            if (date.charAt(i) == '/') {
                p[0] = i;
            }
            if (date.charAt(i) == '.') {
                p[1] = i;

            }
            if (p[0] != 0 && p[1] != 0) {
                break;
            }
        }
        for (int i = 0; i < p.length; i++) {
            if (p[i] != 0) {
                boolean succses = false;
                String day, month, year;
                int iday = 0, imonth = 0, iyear = 0;

                day = date.substring(p[i] - 3, p[i] - 2); // D/M/YYYY
                month = date.substring(p[i] - 1, p[i]);
                year = date.substring(p[i] + 1, p[i] + 5);

                try {
                    iday = Integer.valueOf(day);
                    imonth = Integer.valueOf(month);
                    iyear = Integer.valueOf(year);
                    if (iday > 0 && iday < 32
                            && imonth > 0 && imonth < 13
                            && iyear > 1970) {
                        succses = true;
                    }

                } catch (Exception r) {

                }

                if (!succses) {
                    day = date.substring(p[i] - 5, p[i] - 3); // DD/MM/YYYY
                    month = date.substring(p[i] - 2, p[i]);
                    year = date.substring(p[i] + 1, p[i] + 5);

                    try {
                        iday = Integer.valueOf(day);
                        imonth = Integer.valueOf(month);
                        iyear = Integer.valueOf(year);
                        if (iday > 0 && iday < 32
                                && imonth > 0 && imonth < 13
                                && iyear > 1970) {
                            succses = true;
                        }
                    } catch (Exception r) {

                    }
                }
                if (!succses) {

                    day = date.substring(p[i] - 5, p[i] - 3); // DD/MM/YY
                    month = date.substring(p[i] - 2, p[i]);
                    year = date.substring(p[i] + 1, p[i] + 3);

                    try {
                        iday = Integer.valueOf(day);
                        imonth = Integer.valueOf(month);
                        iyear = Integer.valueOf(year);
                        if (iyear < 30) {
                            iyear = Integer.valueOf("20" + year);
                        } else {
                            iyear = Integer.valueOf("19" + year);
                        }
                        if (iday > 0 && iday < 32
                                && imonth > 0 && imonth < 13
                                && iyear > 1970) {
                            succses = true;
                        }
                    } catch (Exception r) {

                    }

                }
                if (!succses) {
                    day = date.substring(p[i] - 3, p[i] - 2);// D/M/YY
                    month = date.substring(p[i] - 1, p[i]);
                    year = date.substring(p[i] + 1, p[i] + 3);

                    try {
                        iday = Integer.valueOf(day);
                        imonth = Integer.valueOf(month);
                        iyear = Integer.valueOf(year);
                        if (iyear < 30) {
                            iyear = Integer.valueOf("20" + year);
                        } else {
                            iyear = Integer.valueOf("19" + year);
                        }
                        if (iday > 0 && iday < 32
                                && imonth > 0 && imonth < 13
                                && iyear > 1970) {
                            succses = true;
                        }
                    } catch (Exception r) {

                    }

                }
                if (succses) {

                    return new InvoiceDate(iday, imonth, iyear);

                }
            }
        }
        return null;

    }
//biggest_numbers not used!!!!!!!!!@#$%^&*()_

    private void checkWithBigVector(Vector<BNode> bnv, boolean A4) {

        double bnv_sum = sumAllItems(bnv);

        if (sumallNoMamv.getSum() < 0) {
            sumallNoMamv.setSum(sumAllItems(bnv));
        }
        if (sumallWithMamv.getSum() < 0) {
            sumallWithMamv.setSum(sumAllItems(bnv));
        }
        if (bnv.size() == 1 && sumallWithMamv.isCancel()) {
            sumallNoMamv.setCancel(true);
            sumallMamv.setCancel(true);
            sumallWithMamv.setSum(bnv_sum);
            sumallWithMamv.setVerifived(true);
            return;
        }

        /*
         for (int i = 0; i < biggest_numbers.size(); i++) {
         if (biggest_numbers.get(i) > sumallbnv(bnv) * 4) {
         biggest_numbers.remove(i);
         } else if (biggest_numbers.get(i) == shipmentv.getSum()) {
         biggest_numbers.remove(i);
         }

         }

         double biggest_numbers_array[] = new double[biggest_numbers.size()];
         int i = 0;
         for (double bn : biggest_numbers) {
         biggest_numbers_array[i] = bn;
         i++;
         }
         //Arrays.sort(biggest_numbers_array);

         if (Double.parseDouble(new DecimalFormat("##.##").format(bnv_sum-globalanahav.getSum())) != sumallWithMamv.getSum() && !A4) {
         for (int k = 0; k < biggest_numbers_array.length; k++) {
         if (bnv_sum == biggest_numbers_array[k]) {
         if (sumallWithMamv.getSum() == sumallNoMamv.getSum()) {
         sumallNoMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(bnv_sum-globalanahav.getSum()));
         sumallNoMamv.setVerifived(false);
         sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(bnv_sum-globalanahav.getSum()));
         sumallWithMamv.setVerifived(false);
         break;
         } else {

         sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format(bnv_sum-globalanahav.getSum()));
         sumallWithMamv.setVerifived(false);
         break;
         }
         }
         }
         }
         
         double[] temp = new double[biggest_numbers_array.length];
         for (i = biggest_numbers_array.length - 1; i > -1; i--) {
         temp[biggest_numbers_array.length - i - 1] = biggest_numbers_array[i];
         }
         biggest_numbers_array = temp;

         i = 0;

         boolean stops[] = new boolean[2];
         for (int k = 0; k < biggest_numbers_array.length && stops[0] && stops[1]; k++) {
         if (biggest_numbers_array[k] == sumallWithMamv.getSum()) {
         //sumallWithMamv.setSum(biggest_numbers_array[k];
         sumallWithMamv.setVerifived(true);
         stops[0] = true;
         }
         if (biggest_numbers_array[k] == sumallNoMamv.getSum()) {
         //sumallNoMamv.setSum(biggest_numbers_array[k];
         sumallNoMamv.setVerifived(true);
         stops[1] = true;
         }

         }
         if (!stops[0]) {
         sumallWithMamv.setVerifived(false);
         }
         if (!stops[1]) {
         sumallNoMamv.setVerifived(false);
         }
         if (!sumallWithMamv.getVerifived() && sumallNoMamv.getVerifived()) {
         sumallWithMamv.setSum(sumallNoMamv.getSum());
         }
         if (sumallWithMamv.getVerifived() && !sumallNoMamv.getVerifived()) {
         sumallNoMamv.setSum(sumallWithMamv.getSum());
         }
         checkNoMaamWithMaam();
         if (sumallWithMamv.getSum() == sumallNoMamv.getSum() || !checkNoMaamWithMaam()) {

         if (!checkIfIncludeMaamOrNot(biggest_numbers_array)) {/*sumallWithMamv.getSum() < bnv_sum / 1.08 || sumallWithMamv.getSum() > bnv_sum * 1.08) { 

         boolean stop = false;
         for (int j = 0; j < biggest_numbers_array.length && !stop; j++) {
         for (int k = 1; k < biggest_numbers_array.length - 1 && !stop; k++) {
         if (sumallWithMamv.getSum() == Math.abs(Double.parseDouble(new DecimalFormat("##.##").format(biggest_numbers_array[j] + biggest_numbers_array[k])))
         && CheckIfMaam_withmaam_maam(biggest_numbers_array[j], biggest_numbers_array[k])) {
         if (biggest_numbers_array[j] > biggest_numbers_array[k]) {

         sumallMamv.setSum(biggest_numbers_array[k]);
         sumallNoMamv.setSum(biggest_numbers_array[j]);
         stop = true;

         } else if (biggest_numbers_array[j] < biggest_numbers_array[k]) {
         sumallMamv.setSum(biggest_numbers_array[j]);
         sumallNoMamv.setSum(biggest_numbers_array[k]);
         stop = true;

         }
         }
         }
         }
         if (stop == false) {
         write(p_filename, this.getClass().toString(), "Can't find in the tail the price before maam", "checkWithBigVector(Vector<BNode> bnv)", is_log);
         } else {
         sumallWithMamv.setVerifived(true);
         sumallMamv.setVerifived(true);
         sumallNoMamv.setVerifived(true);
         }
         } else {
         boolean stop = false;
         for (int j = 0; j < biggest_numbers_array.length && !stop; j++) {
         int uuu = 0;
         for (int k = 1; k < biggest_numbers_array.length - 1 && !stop; k++) {
         if (Math.abs(biggest_numbers_array[j]) == Math.abs(Double.parseDouble(new DecimalFormat("##.##").format(sumallWithMamv.getSum() + biggest_numbers_array[k])))
         && CheckIfMaam_withmaam_maam(sumallWithMamv.getSum(), biggest_numbers_array[k])) {

         if (sumallNoMamv.getSum() > biggest_numbers_array[k]) {
         sumallMamv.setSum(biggest_numbers_array[k]);
         sumallWithMamv.setSum(biggest_numbers_array[j]);

         stop = true;
         } else if (sumallNoMamv.getSum() < biggest_numbers_array[k]) {
         sumallWithMamv.setSum(biggest_numbers_array[k]);
         sumallMamv.setSum(biggest_numbers_array[j]);

         stop = true;
         }
         }
         }
         }
         if (stop == false) {
         write(p_filename, this.getClass().toString(), "Can't find in the tail the price after maam", "checkWithBigVector(Vector<BNode> bnv)", is_log);
         } else {
         sumallWithMamv.setVerifived(true);
         sumallMamv.setVerifived(true);
         sumallNoMamv.setVerifived(true);
         }
         }
         } // לעשות בדיקה של המעמ עי כל פריט
         if (!checkNoMaamWithMaam()) {
         boolean stop = false;
         for (int j = 0; j < biggest_numbers_array.length && !stop; j++) {
         int uuu = 0;
         for (int k = 1; k < biggest_numbers_array.length - 1 && !stop; k++) {
         if (sumallWithMamv.getSum() == Math.abs(Double.parseDouble(new DecimalFormat("##.##").format(biggest_numbers_array[j] + biggest_numbers_array[k])))
         && CheckIfMaam_withmaam_maam(biggest_numbers_array[j], biggest_numbers_array[k])) {
         if (biggest_numbers_array[j] > biggest_numbers_array[k]) {

         sumallMamv.setSum(biggest_numbers_array[k]);
         sumallNoMamv.setSum(biggest_numbers_array[j]);
         stop = true;

         } else if (biggest_numbers_array[j] < biggest_numbers_array[k]) {
         sumallMamv.setSum(biggest_numbers_array[j]);
         sumallNoMamv.setSum(biggest_numbers_array[k]);
         stop = true;

         }
         }
         }
         }
         if (stop == false) {
         write(p_filename, this.getClass().toString(), "Can't find in the tail the price before maam", "checkWithBigVector(Vector<BNode> bnv)", is_log);
         } else {
         sumallWithMamv.setVerifived(true);
         sumallMamv.setVerifived(true);
         sumallNoMamv.setVerifived(true);
         }
         }
         if (!checkNoMaamWithMaam() && !A4) {
         double wrong_number = sumallWithMamv.getSum();
         Vector<Double> found = new Vector<Double>();
         bnv_sum = sumAllItems(bnv);
         for (int j = 0; j < biggest_numbers_array.length; j++) {
         for (int k = 0; k < biggest_numbers_array.length; k++) {
         double two_numbers = Double.parseDouble(new DecimalFormat("##.##").format(autoIgul(Math.abs(biggest_numbers_array[j]) - Math.abs(biggest_numbers_array[k])))),
         one_number = Math.abs(Double.parseDouble(new DecimalFormat("##.##").format(autoIgul(bnv_sum))));
         if (two_numbers == one_number) {
         if (biggest_numbers_array[j] > biggest_numbers_array[k]) {
         found.add(biggest_numbers_array[j]);
         } else {
         found.add(biggest_numbers_array[k]);
         }

         }
         }
         }

         for (Double number : found) {

         boolean stop = false;
         for (int j = 0; j < biggest_numbers_array.length && !stop; j++) {
         int uuu = 0;
         for (int k = 1; k < biggest_numbers_array.length - 1 && !stop; k++) {
         if (Math.abs(Double.parseDouble(new DecimalFormat("##.#").format(number))) == Math.abs(Double.parseDouble(new DecimalFormat("##.#").format(autoIgul(biggest_numbers_array[j] + biggest_numbers_array[k]))))
         && CheckIfMaam_withmaam_maam(biggest_numbers_array[j], biggest_numbers_array[k])) {
         if (biggest_numbers_array[j] > biggest_numbers_array[k]) {

         sumallMamv.setSum(biggest_numbers_array[k]);
         sumallNoMamv.setSum(biggest_numbers_array[j]);
         stop = true;

         } else if (biggest_numbers_array[j] < biggest_numbers_array[k]) {
         sumallMamv.setSum(biggest_numbers_array[j]);
         sumallNoMamv.setSum(biggest_numbers_array[k]);
         stop = true;

         }
         }
         }
         }

         if (stop == false) {
         write(p_filename, this.getClass().toString(), "Can't find in the tail the price before maam", "checkWithBigVector(Vector<BNode> bnv)", is_log);
         } else {
         sumallWithMamv.setVerifived(true);
         sumallMamv.setVerifived(true);
         sumallNoMamv.setVerifived(true);
         }
         }

         if (wrong_number == sumallWithMamv.getSum()) {
         found = new Vector<Double>();
         for (int j = 0; j < biggest_numbers_array.length; j++) {
         for (int k = 0; k < biggest_numbers_array.length; k++) {
         for (int p = 0; p < biggest_numbers_array.length; p++) {
         if (Double.parseDouble(new DecimalFormat("##.##").format(autoIgul(Math.abs(biggest_numbers_array[j]) + Math.abs(biggest_numbers_array[k] + Math.abs(biggest_numbers_array[p]))))) == Math.abs(Double.parseDouble(new DecimalFormat("##.##").format(autoIgul(bnv_sum))))) {
         if (!found.contains(biggest_numbers_array[j])) {
         found.add(biggest_numbers_array[j]);
         }
         }
         }
         }
         }
         Arrays.sort(biggest_numbers_array);
         for (Double number : found) {

         boolean stop = false;
         for (int j = 0; j < biggest_numbers_array.length && !stop; j++) {
         int uuu = 0;
         for (int k = 1; k < biggest_numbers_array.length - 1 && !stop; k++) {
         if (Math.abs(Double.parseDouble(new DecimalFormat("##.#").format(number))) == Math.abs(Double.parseDouble(new DecimalFormat("##.#").format(autoIgul(biggest_numbers_array[j] + biggest_numbers_array[k]))))
         && CheckIfMaam_withmaam_maam(biggest_numbers_array[j], biggest_numbers_array[k])) {
         if (biggest_numbers_array[j] > biggest_numbers_array[k]) {

         sumallMamv.setSum(biggest_numbers_array[k]);
         sumallNoMamv.setSum(biggest_numbers_array[j]);
         sumallWithMamv.setSum(number);

         stop = true;

         } else if (biggest_numbers_array[j] < biggest_numbers_array[k]) {
         sumallMamv.setSum(biggest_numbers_array[j]);
         sumallNoMamv.setSum(biggest_numbers_array[k]);
         sumallWithMamv.setSum(number);
         stop = true;

         }
         }
         }
         }

         if (stop == false) {
         write(p_filename, this.getClass().toString(), "Can't find in the tail the price before maam", "checkWithBigVector(Vector<BNode> bnv)", is_log);
         } else {
         sumallWithMamv.setVerifived(true);
         sumallMamv.setVerifived(true);
         sumallNoMamv.setVerifived(true);
         }
         }
         }
         }*/
//From here start the code.. inon wrote from here
        try {
            if (!sumallWithMamv.getVerifived() || !(sumallMamv.getVerifived() && sumallMamv.getSum() > 0) /*|| !sumallWithMamv.isCancel()*/) {
                boolean stop = false;
                double sumAll = Double.valueOf(LFun.fixdot(String.valueOf(sumAllItems(bnv))));
                sumAll = Double.parseDouble(new DecimalFormat("##.##").format(sumAll));
                //start from lines.size until the middle of the "bnv.size.lineofprice"
                for (int p = bnv.get(bnv.size() / 2).price.get_line_num(); p < lines.size() && !stop; p++) {
                    for (int y = 0; y < lines.get(p).size() && !stop; y++) {
                        try {
                            double a = Double.valueOf(LFun.fixdot(lines.get(p).get(y).get_result_str()));
                            if (checktoler(a, sumAll, tolerance)) {
                                if (sumallWithMamv.getSum() == sumallNoMamv.getSum() && sumallNoMamv.getSum() > 0) {
                                    sumallNoMamv.setCancel(true);
                                    sumallMamv.setCancel(true);
                                }

                                sumallWithMamv.setSum(a);
                                sumallWithMamv.setVerifived(true);
                                lines.elementAt(p).elementAt(y).set_STR_MEANING("TOTALSUM");

                                stop = true;
                                //divation of 20 SHEKEL
                            }
                            /**
                             * else if (sumAll > a - 8 && sumAll < a + 14) {
                             * if (sumallWithMamv.getSum() == sumallNoMamv.getSum()) {
                             * sumallNoMamv.setCancel(true);
                             * sumallMamv.setCancel(true);
                             * }
                             * sumallWithMamv.setSum(a);
                             * sumallWithMamv.setVerifived(false);
                             *
                             * //stop = true;
                             * } else if (a > sumAll - 23 && a < sumAll + 23) {
                             * if (sumallWithMamv.getSum() ==
                             * sumallNoMamv.getSum()) {
                             * sumallNoMamv.setCancel(true);
                             * sumallMamv.setCancel(true); }
                             * sumallWithMamv.setSum(a);
                             * sumallNoMamv.setVerifived(false); //stop = true;
                             * }
                             */
                        } catch (Exception r) {

                        }
                    }
                }
            }
        } catch (Exception e) {
        }

        if (sumallWithMamv.getSum() == sumallNoMamv.getSum() && !sumallNoMamv.isCancel() && !sumallMamv.isCancel() && sumallWithMamv.getSum() != 0) {
            sumallWithMamv.setVerifived(true);
            sumallNoMamv.setCancel(true);
            sumallMamv.setCancel(true);
        }
        String breakit = "";

    }

    public double autoIgul(double double_number) {
        double_number = Double.parseDouble(new DecimalFormat("##.##").format((double_number)));
        String string_number = String.valueOf(double_number);
        if (string_number.indexOf('.') - Math.abs(string_number.length() - 1) < -1) {
            try {
                if ((int) string_number.charAt(string_number.length() - 1) > 5) {
                    double_number = Double.valueOf(string_number.substring(0, string_number.length() - 1) + '0');
                    double_number = 0.1 + double_number;
                } else {
                    double_number = Double.valueOf(string_number.substring(0, string_number.length() - 1) + '0');
                    double_number = 0.1 - double_number;
                }
            } catch (Exception r) {
            }

        }
        return double_number;
    }

    private boolean checkNoMaamWithMaam() {
        for (String string_maam : mam_percents) {
            double double_maam = (Double.valueOf(LFun.fixdot(string_maam)) / 100) + 1;
            if (sumallNoMamv.getSum() * double_maam == sumallWithMamv.getSum()) {
                return true;
            }
        }
        sumallNoMamv.setVerifived(false);
        return false;
    }

    private boolean checkIfIncludeMaamOrNot(double biggest_numbers_array[]) {
        for (String string_maam : mam_percents) {
            for (double bn : biggest_numbers_array) {
                double double_maam = (Double.valueOf(LFun.fixdot(string_maam)) / 100);
                if (Double.parseDouble(new DecimalFormat("##.##").format(sumallWithMamv.getSum() * double_maam)) == bn) {
                    return true; // not include maam
                }
            }
        }
        return false;// include maam
    }

    private double sumAllItems(Vector<BNode> bnv) {
        double bnv_sum = 0;
        /*
         for (int j = 0; j < bnv.size(); j++) {
         double price = Double.valueOf(LFun.fixdot(bnv.get(j).price.get_result_str())),
         hanaha = Double.valueOf(LFun.fixdot(bnv.get(j).anaha)),
         qty = Double.valueOf(LFun.fixdot(bnv.get(j).qty.get_result_str().replaceAll("-", ".")));
         bnv_sum = bnv_sum + (price/* * qty*//*) - Math.abs(hanaha);
         }
         -------------------------------------------------------------

         for (Item i : itemsVector) {
         bnv_sum = bnv_sum + i.getPrice() - i.getHanaha();
         }
         bnv_sum = Double.parseDouble(new DecimalFormat("##.##").format(bnv_sum));
         */

        double qty = 0, price = 0,/*if exit*/ anaha = 0;
        double negPrice = negpricesv.getSum();
        double price_z = 0;
        for (BNode bn : bnv) {
            if (bn.price_z != null) {
                price_z += Double.valueOf(bn.price_z.get_result_str());
                //System.out.println(bn.price.get_result_str());
            }
        }

        for (BNode bn : bnv) {
            //qty = convert(bn.qty.get_result_str());
            price = convert(bn.price.get_result_str());
            anaha = 0;
            if (!bn.anaha.equals("0")) {
                try {
                    anaha = convert(bn.anaha);
                    if (anaha > 0) {
                        anaha = anaha * (-1);
                    }
                } catch (Exception r) {
                }
            }
            bnv_sum = bnv_sum + price + anaha;

        }
        return bnv_sum + price_z;

    }

    private double convert(String o) {
        try {
            return Double.valueOf(o);
        } catch (Exception r) {
        }
        return 0;
    }

    private boolean CheckIfMaam_withmaam_maam(double biggest_numbers_array1, double biggest_numbers_array2) {
        if (biggest_numbers_array2 > biggest_numbers_array1) {
            for (String mp : mam_percents) {
                double double_maam = (Double.valueOf(LFun.fixdot(mp)) / 100);
                if (Double.parseDouble(new DecimalFormat("##.##").format(double_maam * biggest_numbers_array2)) == biggest_numbers_array1) {
                    return true;
                }
            }
        } else {
            for (String mp : mam_percents) {
                double double_maam = (Double.valueOf(LFun.fixdot(mp)) / 100);
                if (Double.parseDouble(new DecimalFormat("##.##").format(double_maam * biggest_numbers_array1)) == biggest_numbers_array2) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean CheckIfMaam_withmaam_withoutmaam(double biggest_numbers_array1, double biggest_numbers_array2) {
        if (biggest_numbers_array2 < biggest_numbers_array1) {
            for (String mp : mam_percents) {
                double double_maam = (Double.valueOf(LFun.fixdot(mp)) / 100) + 1;
                if (Double.parseDouble(new DecimalFormat("##.##").format(double_maam * biggest_numbers_array2)) == biggest_numbers_array1) {
                    return true;
                }
            }
        } else {
            for (String mp : mam_percents) {
                double double_maam = (Double.valueOf(LFun.fixdot(mp)) / 100) + 1;
                if (Double.parseDouble(new DecimalFormat("##.##").format(double_maam * biggest_numbers_array1)) == biggest_numbers_array2) {
                    return true;
                }
            }
        }
        return false;

    }

    public class InvoiceDate {

        int day, month, year;

        InvoiceDate(int _day, int _month, int _year) {
            day = _day;
            month = _month;
            year = _year;
        }
    }

    /**
     * search left and right of part numbers(where more pns)
     *
     * @param vnumdefault
     * @param result
     * @param end_prices
     */
    public boolean findPNLeftAndRightCheck(Vector<String> vnumdefault, Vector<ObData> result, int end_prices, String external) {
        Vector<Integer> r_pnp = new Vector<Integer>();
        Vector<Integer> l_pnp = new Vector<Integer>();

        int start = result.elementAt(0).get_line_num() - 2;
        if (start == - 1) {
            start = 0;
        }

        for (int i = start; i < end_prices && i < lines.size(); i++) {
            if (lines.elementAt(i).size() > 0) {
                for (int k = 0; k < lines.elementAt(i).size(); k++) {
                    if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("pn")
                            && lines.elementAt(i).elementAt(k).get_external_source().equalsIgnoreCase(external)) { //.get_format().equalsIgnoreCase("INTP")
                        r_pnp.add(lines.elementAt(i).elementAt(k).get_right());
                        System.out.println("r pos pn \t " + lines.elementAt(i).elementAt(k).get_result_str() + " rp " + lines.elementAt(i).elementAt(k).get_right());
                        l_pnp.add(lines.elementAt(i).elementAt(k).get_left());
                        System.out.println("l pos pn \t " + lines.elementAt(i).elementAt(k).get_result_str() + " lp " + lines.elementAt(i).elementAt(k).get_left());
                    }
                }
            }
        }

        if (l_pnp.size() == 0 && r_pnp.size() == 0) {
            return false;
        }

        int pnp_max = 0;

        for (int i = 0; i < l_pnp.size() - 1; i++) {
            if (l_pnp.elementAt(i) < (l_pnp.elementAt(i + 1) - 10) || l_pnp.elementAt(i) > (l_pnp.elementAt(i + 1) + 10)) {
                l_pnp.removeElementAt(i + 1);
                i--;
            }

        }

        for (int i = 0; i < r_pnp.size() - 1; i++) {
            if (r_pnp.elementAt(i) < (r_pnp.elementAt(i + 1) - 10) || r_pnp.elementAt(i) > (r_pnp.elementAt(i + 1) + 10)) {
                r_pnp.removeElementAt(i + 1);
                i--;
            }

        }

        String ex_pn = "no";
        if (r_pnp.size() > 0 && r_pnp.size() >= l_pnp.size()) {
            ex_pn = "rpos";
            pnp_max = r_pnp.elementAt(0);
            for (int i = 1; i < r_pnp.size(); i++) {
                pnp_max = Math.max(pnp_max, r_pnp.elementAt(i));
            }

            for (int i = start; i < end_prices && i < lines.size(); i++) {
                if (lines.elementAt(i).size() > 1) {
                    for (int k = 0; k < lines.elementAt(i).size(); k++) {
                        if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("num") || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("pn")) {
                            if (lines.elementAt(i).elementAt(k).get_right() > pnp_max - 12
                                    && lines.elementAt(i).elementAt(k).get_right() < pnp_max + 12
                                    && (lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTP") || lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTPSEP"))) {
                                lines.elementAt(i).elementAt(k).set_name_by_inv("pn");
                            } else {
                                lines.elementAt(i).elementAt(k).set_name_by_inv("num");
                            }
                        }
                    }
                }
            }

        } else if (l_pnp.size() > 0 && r_pnp.size() < l_pnp.size()) {
            ex_pn = "lpos";
            pnp_max = l_pnp.elementAt(0);
            for (int i = 1; i < l_pnp.size(); i++) {
                pnp_max = Math.min(pnp_max, l_pnp.elementAt(i));
            }

            for (int i = start; i < end_prices && i < lines.size(); i++) {
                if (lines.elementAt(i).size() > 1) {
                    for (int k = 0; k < lines.elementAt(i).size(); k++) {
                        if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("num") || lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("pn")) {
                            if (lines.elementAt(i).elementAt(k).get_left() > pnp_max - 12
                                    && lines.elementAt(i).elementAt(k).get_left() < pnp_max + 12
                                    && (lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTP") || lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTPSEP"))) {
                                lines.elementAt(i).elementAt(k).set_name_by_inv("pn");
                            } else {

                                // lines.elementAt(i).elementAt(k).set_name_by_inv("num");
                                //pn->num if INTP, others to free
                                if (LFun.contain_type(vnumdefault, lines.elementAt(i).elementAt(k).get_format())) {
                                    lines.elementAt(i).elementAt(k).set_name_by_inv("num");
                                } else {
                                    lines.elementAt(i).elementAt(k).set_name_by_inv("free");
                                }
                            }
                        }
                    }
                }
            }
        }

        // search pn with size less 4
        if (!ex_pn.equalsIgnoreCase("no")) {
            int st_nopn = 0;
            int e_nopn = 0;
            int j = 0;
            boolean pnexist = false;
            for (int i = start; i < end_prices + 1 && i < lines.size(); i++) {  //j< countblock -1 &&
                if (lines.elementAt(i).size() > 0) {
                    if (lines.elementAt(i).elementAt(0).get_num_block_by_inv() > j) {
                        if (!pnexist) {
                            e_nopn = i - 1;
                            checkpn(st_nopn, e_nopn, pnp_max, ex_pn);
                        }
                        j++;
                        pnexist = false;
                        st_nopn = i;
                        //System.out.print("\nblock"+j+"\t");
                    }
                }

                for (int k = 0; k < lines.elementAt(i).size(); k++) {
                    if (lines.elementAt(i).elementAt(k).get_name_by_inv().contains("pn")) {
                        pnexist = true;
                    }
                }
            }

        }

        return true;
    }

    Vector<String> IsContainSpecialCode(String code) {
        Vector<String> parts = new Vector<String>();

        if (code.equalsIgnoreCase("100")) {
            parts.add("1502");
            parts.add("1503");
            parts.add("1528");
            parts.add("1519");
            parts.add("1551");
            parts.add("1510");

        } else if (code.equalsIgnoreCase("300")) {
            parts.add("1502");
            parts.add("1503");
            parts.add("1528");
            parts.add("1519");
            parts.add("1551");
            parts.add("1510");

        }

        return parts;
    }

    /**
     * check if pn exist in list
     *
     * @param part
     * @return
     */
    boolean ispart_existinlist(String part) {
        Vector<String> v = new Vector<String>();
        v.add("4329778");
        v.add("7296073123712");
        v.add("3989539");
        for (int i = 0; i < v.size(); i++) {
            if (part.equalsIgnoreCase(v.elementAt(i))) {
                return true;
            }

        }
        return false;
    }

    /**
     * check if description exist in list
     *
     * @param part
     * @return
     */
    boolean isdescr_existinlist(String part) {
        // compare_strings(fromlist,part);
        return false;
    }

    /**
     * search same pos price && compare descr
     *
     * @param j current row of neg price
     * @param start rows
     * @param s neg price
     * @return boolean
     */
    boolean isbeforeexitsameposprice(int j, int start, String s) {

        for (int i = j; i >= start; i--) { //

            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("pos_price")) {
                    try {
                        if (Double.valueOf(LFun.fixdot(s)) * (-1) == Double.valueOf(LFun.fixdot(lines.elementAt(i).elementAt(k).get_result_str()))) {
                            if (compare_desc(i, j)) {
                                return true;
                            }
                        }
                    } catch (Exception r) {
                    }

                }
            }
        }

        return false;
    }

    /**
     * compare description
     *
     * @param i index row of descr of pos price
     * @param j index row of descr of neg price
     * @return
     */
    boolean compare_desc(int i, int j) {
        String s1 = "";
        String s2 = "";
        for (int k = 0; k < lines.elementAt(i).size() && k < lines.elementAt(j).size(); k++) {
            if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("desc")) {
                s1 = s1 + lines.elementAt(i).elementAt(k).get_result_str();
            }
            if (lines.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("desc")) {
                s2 = s2 + lines.elementAt(j).elementAt(k).get_result_str();
            }
        }

        //if(s1.compareToIgnoreCase(s2)==0) return true;
        double perc = 0.0;
        double sto_per = 0.0;

        int dif = s1.length() - s2.length();

        /*      if(dif==0) {
         sto_per=Double.valueOf(s1.length());
      
         perc=Double.valueOf(sto_per*0.8);  
      
         int c=0;
         for (int k = 0; k < s1.length() && k < s2.length(); k++) {
         if(s1.charAt(k)==s2.charAt(k)){
         c++;
         }
           
              
         }
          
          
         if(perc<=c) return true;
         
         }
    
         else */
        if (Math.abs(dif) <= 2) {// return false;
            int size = Math.min(s1.length(), s2.length());
            sto_per = Double.valueOf(size);
            perc = Double.valueOf(sto_per * 0.8);

            int c = 0;

            Vector<Character> v1 = new Vector<Character>();
            Vector<Character> v2 = new Vector<Character>();

            for (int k = 0; k < s1.length(); k++) {
                v1.add(s1.charAt(k));

            }

            for (int k = 0; k < s2.length(); k++) {
                v2.add(s2.charAt(k));

            }
            int ttt = 0;
            for (int k = 0; k < v1.size() && k < v2.size(); k++) {

                if (v1.elementAt(k).equals(v2.elementAt(k))) {
                    //   if(ismaatch3(v1,k,v2,k)){
                    c++;
                    v1.removeElementAt(k);
                    v2.removeElementAt(k);
                    k--;

                } else {
                    if (ismaatch3(v1, k, v2, k + 1)) {

                        // if(v1.size()>0) v1.removeElementAt(k);
                        //  if(v2.size()>0) v2.removeElementAt(k);
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        k--;
                    } else if (ismaatch3(v1, k + 1, v2, k)) {

                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        // if(v1.size()>0) v1.removeElementAt(k);
                        // if(v2.size()>0) v2.removeElementAt(k);
                        k--;
                    } else if (ismaatch3(v1, k, v2, k + 2)) {
                        // if(v1.size()>0)  v1.removeElementAt(k);
                        //  if(v1.size()>0)  v1.removeElementAt(k);
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        // if(v2.size()>0)  v2.removeElementAt(k);
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        k--;
                    } else if (ismaatch3(v1, k + 2, v2, k)) {

                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        //   if(v1.size()>0)  v1.removeElementAt(k);
                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        // if(v2.size()>0)  v2.removeElementAt(k);
                        // if(v2.size()>0)  v2.removeElementAt(k);
                        k--;
                    } else {
                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        k--;
                    }

                }

            }

            if (perc <= c) {
                return true;
            }

        }

        return false;
    }

    /**
     * compare strings
     *
     * @return boolean
     */
    boolean compare_strings(String s1, String s2) {

        //if(s1.compareToIgnoreCase(s2)==0) return true;
        double perc = 0.0;
        double sto_per = 0.0;

        int dif = s1.length() - s2.length();

        if (Math.abs(dif) <= 2) {// return false;
            int size = Math.min(s1.length(), s2.length());
            sto_per = Double.valueOf(size);
            perc = Double.valueOf(sto_per * 0.8);

            int c = 0;

            Vector<Character> v1 = new Vector<Character>();
            Vector<Character> v2 = new Vector<Character>();

            for (int k = 0; k < s1.length(); k++) {
                v1.add(s1.charAt(k));

            }

            for (int k = 0; k < s2.length(); k++) {
                v2.add(s2.charAt(k));

            }
            int ttt = 0;
            for (int k = 0; k < v1.size() && k < v2.size(); k++) {

                if (v1.elementAt(k).equals(v2.elementAt(k))) {
                    //   if(ismaatch3(v1,k,v2,k)){
                    c++;
                    v1.removeElementAt(k);
                    v2.removeElementAt(k);
                    k--;

                } else {
                    if (ismaatch3(v1, k, v2, k + 1)) {

                        // if(v1.size()>0) v1.removeElementAt(k);
                        //  if(v2.size()>0) v2.removeElementAt(k);
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        k--;
                    } else if (ismaatch3(v1, k + 1, v2, k)) {

                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        // if(v1.size()>0) v1.removeElementAt(k);
                        // if(v2.size()>0) v2.removeElementAt(k);
                        k--;
                    } else if (ismaatch3(v1, k, v2, k + 2)) {
                        // if(v1.size()>0)  v1.removeElementAt(k);
                        //  if(v1.size()>0)  v1.removeElementAt(k);
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        // if(v2.size()>0)  v2.removeElementAt(k);
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        k--;
                    } else if (ismaatch3(v1, k + 2, v2, k)) {

                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        //   if(v1.size()>0)  v1.removeElementAt(k);
                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        // if(v2.size()>0)  v2.removeElementAt(k);
                        // if(v2.size()>0)  v2.removeElementAt(k);
                        k--;
                    } else {
                        if (v1.size() > 0) {
                            v1.removeElementAt(k);
                        }
                        if (v2.size() > 0) {
                            v2.removeElementAt(k);
                        }
                        k--;
                    }

                }

            }

            if (perc <= c) {
                return true;
            }
        }

        return false;
    }

    boolean ismaatch3(Vector<Character> v1, int i1, Vector<Character> v2, int i2) {

        for (int m = 0; m < 3 && i1 < v1.size() && i2 < v2.size(); m++) {
            if (!v1.elementAt(i1++).equals(v2.elementAt(i2++))) {
                return false;
            }
        }
        return true;
    }

    /**
     * search in row has
     *
     * @param i - index of row name_by_inv
     * @return boolean
     */
    boolean isrowhasNameByInv(Vector<ObData> v, String name_by_inv) {
        for (int k = 0; k < v.size(); k++) {
            if (v.elementAt(k).get_name_by_inv().equalsIgnoreCase(name_by_inv)) {
                return true;
            }
        }

        return false;
    }

    /**
     * search in row pn
     *
     * @param i - index of row
     * @return boolean
     */
    boolean isrowhaspn(int i) {
        for (int k = 0; k < lines.elementAt(i).size(); k++) {
            if (lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("pn")) {
                return true;
            }
        }

        return false;
    }

    /**
     * search in row pn
     *
     * @param i - index of row
     * @return boolean
     */
    String isblockhaspn(int i, int end) {

        int start = 0;

        boolean flag = true;
        for (int j = 0; j < lines.size() && flag; j++) {
            if (lines.elementAt(j).size() != 0) {
                if (lines.elementAt(j).elementAt(0).get_num_block_by_inv() == i) {
                    start = j;
                    flag = false;
                }
                //if(lines.elementAt(j).elementAt(0).get_num_block_by_inv()==i+1) end=j;
            }
        }

        for (int j = start; j <= end; j++) {
            for (int k = 0; k < lines.elementAt(j).size(); k++) {
                if (lines.elementAt(j).elementAt(k).get_name_by_inv().equalsIgnoreCase("pn")) {
                    return lines.elementAt(j).elementAt(k).get_result_str();
                }
            }
        }
        return "";
    }

    /**
     * search pn
     *
     * @param st_nopn start block
     * @param e_nopn end block
     * @param pnp_max column of pn
     * @param ex_pn direction
     */
    void checkpn(int st_nopn, int e_nopn, int pnp_max, String ex_pn) {
        int min = 10000;
        for (int i = st_nopn; i <= e_nopn && i < lines.size(); i++) {

            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if ((lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTP") || lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTPSEP"))
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("percent")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("qty")) {
                    if (ex_pn.equalsIgnoreCase("rpos")) {
                        min = (Math.min(pnp_max - lines.elementAt(i).elementAt(k).get_right(), min));
                    } else if (ex_pn.equalsIgnoreCase("lpos")) {
                        min = (Math.min(lines.elementAt(i).elementAt(k).get_left() - pnp_max, min));
                    }

                }
            }
        }
        if (min > 40) {
            return;
        }
        int min2 = 10000;
        for (int i = st_nopn; i <= e_nopn && i < lines.size(); i++) {

            for (int k = 0; k < lines.elementAt(i).size(); k++) {
                if ((lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTP") || lines.elementAt(i).elementAt(k).get_format().equalsIgnoreCase("INTPSEP"))
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("percent")
                        && !lines.elementAt(i).elementAt(k).get_name_by_inv().equalsIgnoreCase("qty")) {
                    if (ex_pn.equalsIgnoreCase("rpos")) {
                        min2 = (Math.min(pnp_max - lines.elementAt(i).elementAt(k).get_right(), min2));
                    } else if (ex_pn.equalsIgnoreCase("lpos")) {
                        min2 = (Math.min(lines.elementAt(i).elementAt(k).get_left() - pnp_max, min2));
                    }

                    if (min == min2
                            && lines.elementAt(i).elementAt(k).get_result_str().length() >= Integer.valueOf(minlpn)
                            && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains(",")
                            && !lines.elementAt(i).elementAt(k).get_name_by_inv().contains(".")) {
                        lines.elementAt(i).elementAt(k).set_name_by_inv("pn");
                    }
                }
            }
        }
    }

    /**
     * check if percent string exist in list of the mam percents
     *
     * @param s
     * @return boolean
     */
    boolean ispercent(String s) {

        for (int i = 0; i < mam_percents.size(); i++) {
            String t = mam_percents.elementAt(i);
            if (("%" + t).equalsIgnoreCase(s)
                    || (t + "%").equalsIgnoreCase(s)
                    || ("%0" + t).equalsIgnoreCase(s)
                    || ("0" + t + "%").equalsIgnoreCase(s)) {
                return true; //|| mam_percents.elementAt(i).equalsIgnoreCase("%"+s) 
            }
        }
        return false;
    }

    /**
     * get index next price by block number
     *
     * @param lindx
     * @return
     */
    int nextpn(int lindx) {

        for (int i = lindx; i < lines.size(); i++) {

            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("pn")) {
                    return i;
                }

            }

        }
        return lindx;
    }

    /**
     * get index next price by block number
     *
     * @param lindx
     * @return
     */
    int nextpriceline(int lindx) {
        for (int i = lindx; i < lines.size(); i++) {
            for (int j = 0; j < lines.elementAt(i).size(); j++) {
                if (lines.elementAt(i).elementAt(j).get_name_by_inv().contains("price")) {
                    return i;
                }

            }

        }
        return lindx;
    }

    /**
     * parsing unregognized element
     *
     * @param obd
     * @return
     */
    public Vector<ObData> get_gen_parse(ObData obd) {
        Vector<ObData> p = new Vector<ObData>();
        String s = obd.get_result_str();
        String temp = "";
        for (int i = 0; i < s.length(); i++) {

            if (LFun.asci_range(s.charAt(i)) == 0 && !temp.equalsIgnoreCase("")) {
                ObData o = new ObData();
                o.set_result_str(temp);
                o.set_right(obd.get_right());
                o.set_left(obd.get_left());
                o.set_top(obd.get_top());
                o.set_bottom(obd.get_bottom());
                o.set_line_num(obd.get_line_num());
                o.set_name_in(obd.get_name_in());
                o.set_inx_col(obd.get_inx_col());
                o.set_inx_renum(obd.get_inx_renum());
                o.set_fntsizasc(obd.get_fntsizasc());
                o.set_fntnameasc(obd.get_fntnameasc());
                o.set_pfile_num(obd.get_pfile_num());
                if (temp.contains(",")) {
                    //o.set_format("USADD");
                    o.set_format(LFun.checkFormat(o.get_result_str()));
                    o.set_name_by_inv("num");
                } else if (temp.contains(".")) {
                    //o.set_format("EURDD");
                    o.set_format(LFun.checkFormat(o.get_result_str()));
                    o.set_name_by_inv("num");
                } else {
                    // o.set_format("INTP");
                    o.set_format(LFun.checkFormat(o.get_result_str()));
                    o.set_position(obd.get_position());
                    if (LFun.ispn(o)) {
                        o.set_name_by_inv("pn");
                    } else {
                        o.set_name_by_inv("num");
                    }
                }
                p.add(o);
                //p.lastElement().set_result_str(temp);
                temp = "";
            } else if (LFun.asci_range(s.charAt(i)) == 1) {
                temp = temp + s.charAt(i);
            } else if (LFun.asci_range(s.charAt(i)) == 2) {
                if (!temp.contains(".") || !temp.contains(",")) {
                    if (i < s.length() - 1 && LFun.asci_range(s.charAt(i + 1)) == 1) {
                        temp = temp + s.charAt(i);
                    } else {
                        ObData o = new ObData();
                        o.set_right(obd.get_right());
                        o.set_result_str(temp);
                        o.set_left(obd.get_left());
                        o.set_top(obd.get_top());
                        o.set_bottom(obd.get_bottom());
                        o.set_line_num(obd.get_line_num());
                        o.set_name_in(obd.get_name_in());
                        o.set_inx_col(obd.get_inx_col());
                        o.set_inx_renum(obd.get_inx_renum());
                        o.set_fntsizasc(obd.get_fntsizasc());
                        o.set_fntnameasc(obd.get_fntnameasc());
                        o.set_pfile_num(obd.get_pfile_num());
                        p.add(o);
                        //p.lastElement().set_result_str(temp);
                        temp = "";
                    }
                } else {
                    ObData o = new ObData();
                    o.set_result_str(temp);
                    o.set_right(obd.get_right());
                    o.set_left(obd.get_left());
                    o.set_top(obd.get_top());
                    o.set_bottom(obd.get_bottom());
                    o.set_line_num(obd.get_line_num());
                    o.set_name_in(obd.get_name_in());
                    o.set_inx_col(obd.get_inx_col());
                    o.set_inx_renum(obd.get_inx_renum());
                    o.set_fntsizasc(obd.get_fntsizasc());
                    o.set_fntnameasc(obd.get_fntnameasc());
                    o.set_pfile_num(obd.get_pfile_num());
                    p.add(o);
                    //p.lastElement().set_result_str(temp);
                    temp = "";
                }
            }

        }

        if (!temp.equalsIgnoreCase("")) {
            ObData o = new ObData();
            o.set_result_str(temp);
            o.set_right(obd.get_right());
            o.set_left(obd.get_left());
            o.set_top(obd.get_top());
            o.set_bottom(obd.get_bottom());
            o.set_line_num(obd.get_line_num());
            o.set_name_in(obd.get_name_in());
            o.set_inx_col(obd.get_inx_col());
            o.set_inx_renum(obd.get_inx_renum());
            o.set_fntsizasc(obd.get_fntsizasc());
            o.set_fntnameasc(obd.get_fntnameasc());
            o.set_pfile_num(obd.get_pfile_num());
            if (temp.contains(",")) {
                //o.set_format("USADD");
                o.set_format(LFun.checkFormat(o.get_result_str()));
                o.set_name_by_inv("num");
            } else if (temp.contains(".")) {
                //o.set_format("EURDD");
                o.set_format(LFun.checkFormat(o.get_result_str()));
                o.set_name_by_inv("num");
            } else {
                //o.set_format("INTP");
                o.set_format(LFun.checkFormat(o.get_result_str()));
                o.set_position(obd.get_position());
                if (LFun.ispn(o)) {
                    o.set_name_by_inv("pn");
                } else {
                    o.set_name_by_inv("num");
                }
            }
            p.add(o);
            //p.lastElement().set_result_str(temp);
            temp = "";
        }
        return p;
    }

    /**
     * found index in od vector by line number
     *
     * @param k line number
     * @return index
     */
    public int index_by_line(int k) {
        for (int i = 0; i < od.size(); i++) {
            if (od.elementAt(i).get_line_num() == (k + 1)) {
                return i;
            }

        }
        return 0;
    }

    /**
     * find qty and prices by one part in blocks
     */
    public Vector<ObData> getqty_prices(int n) {
        Vector<ObData> result = new Vector<ObData>();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < od.size(); j++) {
                // if(od.elementAt(i)

            }

        }

        return result;
    }

    /**
     *
     * @param tol from left side
     * @return vector strings
     */
    public Vector<String> get_lwords(String tol, String type) {
        int t = Integer.valueOf(tol);
        Vector<String> mwv = new Vector<String>();

        for (int i = 0; i < od.size(); i++) {
            if ((get_l() + t) >= od.elementAt(i).get_left() && od.elementAt(i).get_format().equalsIgnoreCase(type)) {
                mwv.add(od.elementAt(i).get_result_str());
            }

        }

        return mwv;

    }

    /**
     *
     * @param tol from right side
     * @return vector strings
     */
    public Vector<String> get_rwords(String tol, String type) {
        int t = Integer.valueOf(tol);
        Vector<String> mwv = new Vector<String>();

        for (int i = 0; i < od.size(); i++) {
            if ((get_r() - t) <= od.elementAt(i).get_right() && od.elementAt(i).get_format().equalsIgnoreCase(type)) {
                mwv.add(od.elementAt(i).get_result_str());
            }
        }

        return mwv;

    }

    public Vector<String> get_mwords(String type) {
        Vector<String> mwv = new Vector<String>();

        if (type.equalsIgnoreCase("LATTXT")) {
            for (int i = 0; i < od.size(); i++) {
                if ((get_l() + 4) <= od.elementAt(i).get_left() && od.elementAt(i).get_format().equalsIgnoreCase(type)) {
                    mwv.add(od.elementAt(i).get_result_str());
                }
            }
        } else if (type.equalsIgnoreCase("HEBTXT")) {
            for (int i = 0; i < od.size(); i++) {
                if ((get_r() - 4) <= od.elementAt(i).get_right() && od.elementAt(i).get_format().equalsIgnoreCase(type)) {
                    mwv.add(od.elementAt(i).get_result_str());
                }
            }
        }

        return mwv;

    }

    /**
     * set 12.5% left and 12.5% right
     */
    private void set_by_12_5() {
        int larr[] = new int[od.size()];
        int rarr[] = new int[od.size()];

        for (int i = 0; i < od.size(); i++) {
            larr[i] = od.elementAt(i).get_left();
            rarr[i] = od.elementAt(i).get_right();
        }
        Arrays.sort(rarr);
        Arrays.sort(larr);
        double indp = (od.size() / 100) * 12.5;
        left12_5 = larr[(int) indp];
        right12_5 = rarr[(rarr.length - 1) - (int) indp];
        System.out.println("left " + left12_5 + " right " + right12_5);
    }

    /**
     * set minimum left
     */
    private void set_l() {
        l = od.elementAt(0).get_left();
        for (int i = 1; i < od.size(); i++) {
            if (l > od.elementAt(i).get_left()) {
                l = od.elementAt(i).get_left();
            }

        }
    }

    /**
     * set minimum top
     */
    private void set_t() {
        t = od.elementAt(0).get_top();
        for (int i = 1; i < od.size(); i++) {
            if (t < od.elementAt(i).get_top()) {
                t = od.elementAt(i).get_top();
            }

        }
    }

    /**
     * set maximum right
     */
    private void set_r() {
        r = od.elementAt(0).get_right();
        for (int i = 1; i < od.size(); i++) {
            if (r < od.elementAt(i).get_right()) {
                r = od.elementAt(i).get_right();
            }

        }
    }

    /**
     * set maximum bottom
     */
    private void set_b() {
        b = od.elementAt(0).get_bottom();
        for (int i = 1; i < od.size(); i++) {
            if (b < od.elementAt(i).get_bottom()) {
                b = od.elementAt(i).get_bottom();
            }

        }
    }

    /**
     *
     * @return minimum left
     */
    public int get_l() {
        return l;
    }

    /**
     *
     * @return maximum right
     */
    public int get_r() {
        return r;
    }

    /**
     *
     * @return minimum top
     */
    public int get_t() {
        return t;
    }

    /**
     *
     * @return maximum bottom
     */
    public int get_b() {
        return b;
    }

    public String toString(int i) {
        return od.elementAt(i).toString();
    }

    public Vector<String> get_between(int l, int t, int r, int b) {
        Vector<String> tv = new Vector<String>();

        for (int i = 0; i < od.size(); i++) {
            if (od.elementAt(i).get_left() >= l && od.elementAt(i).get_top() >= t && od.elementAt(i).get_right() <= r && od.elementAt(i).get_bottom() <= b) {
                tv.add(od.elementAt(i).get_result_str());
            }
        }
        return tv;
    }

    /**
     * parse csv data to object data
     */
    /**
     * parse csv data to object data
     *
     * @param _MODE
     */
    public boolean parseCSV(int _MODE, boolean _log, String fileName) {

        boolean finish = false;
        try {
            p_filename = fileName;
            is_log = _log;
            if (_log) {

                //log = new Log();
                createNewLogFile();
                //write(fileName, this.getClass().toString(), "Exeption", "Inon");
            }
            MODE = _MODE;

            finish = true;
            int flag = 1;
            int count = 0;
            int lp = 10000;

            Vector<String> v = LFun.file2vector(path, format);
            if (MODE == 0) {
                SConsoleAdmin.console_log.setText(setStartPrint(SConsoleAdmin.console_log.getText(), "parse csv start, readed rows " + v.size()));
            }
            for (int i = 1; i < v.size(); i++) {
                //v.setElementAt(LFun.escapeUnicode(v.elementAt(i)), i);

                try {

                    ObData odtemp = new ObData();

                    //change "־" to "-"
                    String rep = v.elementAt(i).replaceAll("־", "-");
                    int ind = LFun.ind_last_q(rep);
                    String str = v.elementAt(i).substring(1, ind);
                    if (str.endsWith(".") || str.endsWith(",")) {
                        ind--;
                        str = v.elementAt(i).substring(1, ind);
                    }
                    if (str.startsWith(",") || str.startsWith(".")) {
                        str = v.elementAt(i).substring(2, ind);
                    }

                    if (LFun.iscontain_num(str)) {
                        str = LFun.removeNoiseFromString(str);
                    }

                    /*if(str.contains("|")){
                        System.out.println("CSV.CSV2Hash.parseCSV()");
                    }*/
                    if (str.endsWith("|") && str.length() > 1 && str.contains(".") && str.indexOf(".") == str.length() - 4) {//to fix the problem in wisepae   123.00|   ---> 123.00
                        try {
                            double d = Double.valueOf(str.substring(0, str.length() - 1).replaceAll(",", ""));
                            str = str.substring(0, str.length() - 1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (str.equals("000.00") || str.equals("00.00")) {
                        str = "0.00";
                    }

                    if (str.contains("״")) {
                        str = str.replaceAll("״", "\"");
                    }

                    //int ind=LFun.ind_last_q(v.elementAt(i));      
                    //odtemp.set_result_str(v.elementAt(i).substring(0, ind+1));// with ""
                    odtemp.set_result_str(LFun.fix2dot(str));

                    //odtemp.set_result_str(v.elementAt(i).substring(1, ind));// without ""
                    odtemp.set_format(LFun.checkFormat(str));
                    StringTokenizer tok = new StringTokenizer(v.elementAt(i).substring(ind + 2), ",");// \"\t\n\r\f\240

                    odtemp.set_name_in(tok.nextToken());
                    odtemp.set_pfile_num(Integer.parseInt(tok.nextToken()));
                    odtemp.set_inx_col(Integer.parseInt(tok.nextToken()));
                    odtemp.set_line_num(Integer.parseInt(tok.nextToken()));
                    odtemp.set_left(Integer.parseInt(tok.nextToken()));
                    odtemp.set_top(Integer.parseInt(tok.nextToken()));
                    odtemp.set_right(Integer.parseInt(tok.nextToken()));
                    odtemp.set_bottom(Integer.parseInt(tok.nextToken()));
                    odtemp.set_inx_renum(Integer.parseInt(tok.nextToken()));
                    try {
                        odtemp.set_fntsizasc(Integer.parseInt(tok.nextToken()));
                        odtemp.set_fntnameasc(tok.nextToken());
                    } catch (Exception e) {

                    }
                    if (tok.hasMoreTokens()) {
                        odtemp.set_cr(tok.nextToken());
                    }

                    //found left, right, median position
                    if (odtemp.get_result_str().startsWith("()") && odtemp.get_result_str().length() > 2) {
                        odtemp.set_result_str("(" + odtemp.get_result_str().substring(2) + ")");
                    }

                    if (od.size() > 0 && flag != odtemp.get_line_num()) {

                        if (matbeim.contains(od.elementAt(od.size() - (count)).get_result_str())) {
                            matbeya = od.elementAt(od.size() - (count)).get_result_str();
                            od.elementAt(od.size() - (count)).set_name_by_inv("matbeya");
                            od.elementAt(od.size() - 1).set_position("RP");
                            od.elementAt(od.size() - (count - 1)).set_position("LP");

                            //od.elementAt(od.size() - (count + 1)).set_position("RP");
                            //od.elementAt(od.size() - (count)).set_position("RP");
                            //od.elementAt(od.size() - 1).set_position("LP");
                        } else if (matbeim.contains(od.elementAt(od.size() - 1).get_result_str())) {
                            matbeya = od.elementAt(od.size() - 1).get_result_str();
                            od.elementAt(od.size() - 1).set_name_by_inv("matbeya");
                            od.elementAt(od.size() - (count)).set_position("LP");
                            od.elementAt(od.size() - 2).set_position("RP");

                            //od.elementAt(od.size() - (count)).set_position("RP");
                            //od.elementAt(od.size() - 2).set_position("LP");
                        } else if (od.elementAt(od.size() - (count)).get_left() == od.elementAt(od.size() - 1).get_left()) {
                            od.elementAt(od.size() - 1).set_position("MP");
                        } else if (od.elementAt(od.size() - (count)).get_left() > od.elementAt(od.size() - 1).get_left()) {

                            od.elementAt(od.size() - (count)).set_position("RP");
                            od.elementAt(od.size() - 1).set_position("LP");

                        } else if (od.elementAt(od.size() - (count)).get_left() < od.elementAt(od.size() - 1).get_left()) {

                            od.elementAt(od.size() - (count)).set_position("LP");
                            od.elementAt(od.size() - 1).set_position("RP");

                        }

                        count = 0;
                        flag = odtemp.get_line_num();
                    }

                    od.add(odtemp);
                    count++;
                    if (MODE == 0) {

                        String s = SConsoleAdmin.console_log.getText();

                        SConsoleAdmin.console_log.setText(setStartPrint(s, "Parsed row " + (i + 1)));
                    }
                } catch (Exception e) {
                    finish = false;
                    if (MODE == 0) {
                        String s = SConsoleAdmin.console_log.getText();

                        SConsoleAdmin.console_log.setText(setStartPrint(s, "Not Parsed row " + (i + 1)));

                    }
                }

            }
            //for last row
            if (matbeim.contains(od.elementAt(od.size() - (count)).get_result_str())) {
                matbeya = od.elementAt(od.size() - (count)).get_result_str();
                od.elementAt(od.size() - (count)).set_name_by_inv("matbeya");
                od.elementAt(od.size() - (count + 1)).set_position("RP");
                od.elementAt(od.size() - 1).set_position("LP");
            } else if (matbeim.contains(od.elementAt(od.size() - 1).get_result_str())) {
                matbeya = od.elementAt(od.size() - 1).get_result_str();
                od.elementAt(od.size() - 1).set_name_by_inv("matbeya");
                od.elementAt(od.size() - (count)).set_position("RP");
                od.elementAt(od.size() - 2).set_position("LP");
            } else if (od.elementAt(od.size() - (count)).get_left() == od.elementAt(od.size() - 1).get_left()) {
                od.elementAt(od.size() - 1).set_position("MP");
            } else if (od.elementAt(od.size() - (count)).get_left() < od.elementAt(od.size() - 1).get_left()) {
                od.elementAt(od.size() - (count)).set_position("RP");
                od.elementAt(od.size() - 1).set_position("LP");
            } else if (od.elementAt(od.size() - (count)).get_left() > od.elementAt(od.size() - 1).get_left()) {
                od.elementAt(od.size() - (count)).set_position("LP");
                od.elementAt(od.size() - 1).set_position("RP");
            }

            //end
            set_l();
            set_r();
            set_t();
            set_b();
            set_by_12_5();

            //search description by ITEM_DESCRIPTION in one line
            //searchItemDescInLine();
            //sort_od();
            int counter_to_restore = 0, item_counter = 0;
            Vector<String> checkVector = new Stack<String>();
            for (int i = 0; i < od.size(); i++) {

                if (!od.elementAt(i).get_name_by_inv().equalsIgnoreCase("desc")) {////////////////////////////////////////////////////////// Big Invoices
                    /*
                     INON CODE
                
                
                
                     if (od.elementAt(i).get_format().equalsIgnoreCase("USADD")) {////////////while number of pages is 1;
                     try {
                     item_counter++;
                     int LINEPIX = 50, firstIndex = od.size(), lastIndex = 0;

                        
                     boolean stop_get_index = false;
                     for (int j = 0; j < od.size(); j++) {
                     if (od.elementAt(i).get_bottom() - LINEPIX <= od.elementAt(j).get_bottom()) {
                     if (od.elementAt(i).get_bottom() == od.elementAt(j).get_bottom()) {
                     break;
                     }
                     if (!stop_get_index) {
                     firstIndex = j;
                     stop_get_index = true;
                     }
                     //checkVector.add(od.elementAt(j).get_result_str());
                     }

                     }

                     for (int j = i; j < od.size(); j++) {
                     if (od.elementAt(i).get_bottom() == od.elementAt(j).get_bottom()) {
                     //checkVector.add(od.elementAt(j).get_result_str());
                     } else {
                     lastIndex = j;
                     break;
                     }
                     }
                        
                        
                     int now_right = 0;
                     int last_right = od.elementAt(firstIndex).get_right();
                     for (int j = firstIndex+1; j < lastIndex; j++) {
                     now_right = od.elementAt(j).get_right();
                     if(last_right - now_right < 530){
                     checkVector.add(od.elementAt(j).get_result_str());
                     }else{
                     break;
                     }
                     //last_right = od.elementAt(j).get_right();
                     }

                     double item_price = 0, item_qty = 0, item_sum = 0;
                     Vector<Double> num_item = new Vector<Double>();
                     for (String s : checkVector) {
                     try {
                     num_item.add(Double.valueOf(s));
                     } catch (Exception c) {

                     }
                     }

                     for (double d : num_item) {
                     if (d < 0) {
                     negativeVector.add(d);
                     }
                     }
                     Vector<Double> find_item = ClearlyADD(num_item);
                     BNode b = new BNode();
                     if (find_item.get(0) != null) {
                     for (int j = firstIndex; j < lastIndex; j++) {
                     if (find_item.get(3) != null) {
                     try {
                     double number = Double.valueOf(od.get(j).get_result_str());

                     if ((String.valueOf(find_item.get(3)).replace("0", "").replace(".", "")).equals(String.valueOf(number).replace("0", "").replace(".", ""))) {
                     ObData tempobdata = od.get(j);
                     String result = "";
                     try {
                     result = (String.valueOf(find_item.get(3)).replace("0", "").replace(".", ""));
                     } catch (Exception r) {
                     result = ("" + find_item.get(3));

                     }
                     tempobdata.set_result_str(result);
                     b.set_o_pn(tempobdata);
                     }

                     if (number == find_item.get(0)) {
                     //price per one
                     b.set_o_price(od.get(j));
                     }
                     if (number == find_item.get(1)) {
                     //qty
                     b.set_o_qty(od.get(j));
                     }
                     if ((String.valueOf(find_item.get(2)).replace("0", "").replace(".", "")).equals(String.valueOf(number).replace("0", "").replace(".", ""))) {
                     //sum
                     ObData tempobdata = od.get(j);
                     tempobdata.set_result_str("" + find_item.get(2));
                     b.set_o_fullprice(tempobdata);
                     }
                     } catch (Exception e) {
                     String string = od.get(j).get_result_str();///////////////////////////////////////////////////////////////////////////////////////
                     //Double.valueOf(String.valueOf(find_item.get(2)).substring(0, String.valueOf(find_item.get(2)).indexOf(".")))
                     }

                     }

                     }
                     if (b != null) {
                     foundedBNodes.add(b);
                     }
                     int finidfgdfgsh = 0;

                     checkVector = new Vector<String>();

                     }
                     } catch (Exception c) {

                     }
                     }*/

                    String[] check = findInMakatCSU(od.elementAt(i).get_result_str(), 0);
                    if (check != null) {
                        //od.elementAt(i).set_result_str(check[colMakat]);
                        od.elementAt(i).set_name_by_inv("pn");
                        od.elementAt(i).set_external_source("MAKAT");
                        od.elementAt(i).set_format("INTPSEP");
                        od.elementAt(i).set_in_Makat_Csu(true);
                    }
                }
            }
        } catch (Exception r) {
        }

        return finish;
    }

    private double setDouble(double b) {
        try {
            return Double.parseDouble(new DecimalFormat("##.##").format(b));
        } catch (Exception r) {

        }
        return -1;
    }

    private double setDouble(String b) {
        try {
            return Double.parseDouble(new DecimalFormat("##.##").format(b));
        } catch (Exception r) {

        }
        return -1;

    }

    private Vector<Double> ClearlyADD(Vector<Double> v) {
        Vector<Double> ret = new Vector<Double>();
        /*
         0 = price per one
         1 = qty
         2 = sum   
         3 = pn
         */
        for (int i = v.size() - 1; i >= 0; i--) {
            double item_sum = setDouble(v.get(i));
            Vector<Double> comb_item_price = getcomb(item_sum);
            for (int j = v.size() - 1; j >= 0; j--) {
                double item_qty = v.get(j);
                for (int k = 0; k < v.size(); k++) {
                    double item_price = setDouble(v.get(k));
                    if (item_sum == setDouble(item_qty * item_price)) {
                        ret.add(item_price);
                        ret.add(item_qty);
                        ret.add(item_sum);
                        for (double val : v) {
                            if (val > item_price && val > item_qty && val > item_sum) {
                                ret.add(val);
                                break;
                            }
                        }
                        return ret;
                    }
                    for (double comb : comb_item_price) {
                        if (comb == setDouble(item_qty * item_price)) {
                            ret.add(item_price);
                            ret.add(item_qty);
                            ret.add(comb);
                            for (double val : v) {
                                if (val > item_price && val > item_qty && val > item_sum) {
                                    ret.add(val);
                                    break;
                                }
                            }
                            return ret;
                        }
                    }

                }
            }
        }
        return null;
    }

    private static String setStartPrint(String string, String add) {
        return add + "\n" + string;
    }

    public void print2graph() {
        int r = od.lastElement().get_line_num();
        int c = Math.max(get_l(), get_r());
        int[][] m = new int[r][c];
        for (int k = 0; k < od.size(); k++) {
            for (int i = od.elementAt(k).get_left(); i < od.elementAt(k).get_right(); i++) {
                m[od.elementAt(k).get_line_num() - 1][i] = 1;
            }

        }
        String s = "";
        for (int i = 9; i < m.length; i++) {//m.length
            System.out.println();
            s = "";
            for (int j = 0; j < m[i].length; j = j + 20) {
                if (m[i][j] == 1) {
                    System.out.print("|");
                    s = s + "|";
                } else {
                    System.out.print(" ");
                    s = s + " ";
                }
            }

        }
    }

    /**
     * בדיקה במידה ועדיין לא אומת הסכום הכללי בא לבדוק האם במקרה אחד ההנחות לא
     * נקראו כמו שצריך בודק בצורה הזו בודק האם הסכום של כל המספרים החיוביים שווה
     * לשני מספרים כלשהם שאנחנו לוקחים מהזנב שני המספרים שאנחנו מחפשים בזנב
     * מייצגים את סכום כל המספרים החיוביים אחרי הנחה ואת סוכם ההנחה הכולל במידה
     * ומצאנו אנחנו משווים את המספר שמייצג את סכום כל השליליים ואת כל המספרים
     * השליליים בשלב הבא אנחנו משנים את המספרים השליליים שבידנו למספרים אחרים
     * דומים כדי לבדוק האם לא קראנו נכון ואולי נצליח לנחש מה באמת רשום כל פעם
     * משנים מספר אחר ובודקים אם אחרי שינוי הם שווים אם כן אז אפשר לאמת את הסכום
     * הכולל
     */
    private void checkSumPosEqual2NumFromTail(double sumpos, Vector<Double> sumneg, Vector<BNode> bnv) {

        // String Price = bnv.elementAt(0).price.get_result_str(),
        // Product = bnv.elementAt(0).price.get_name_in(),
        // Hanaha = bnv.elementAt(0).anaha;
        // bnv.elementAt(0).price.get_result_str();
        double num1 = 0, num2 = 0, hanaha = 0;
        boolean found = false;
        for (int i = GetLastLineOfBnv(bnv); !found && i < lines.size(); i++) {
            for (int j = 0; !found && j < lines.elementAt(i).size(); j++) {
                try {
                    num1 = Double.valueOf(LFun.fixdot(lines.elementAt(i).elementAt(j).get_result_str()));
                } catch (Exception E) {
                }
                for (int k = i; !found && k < lines.size(); k++) {
                    for (int k1 = j; !found && k1 < lines.elementAt(k).size(); k1++) {
                        try {
                            num2 = Double.valueOf(LFun.fixdot(lines.elementAt(k).elementAt(k1).get_result_str()));
                        } catch (Exception E) {
                        }
                        //System.out.println("checkSumPosEqual2NumFromTail:\t" + "(" + k + "," + k1 + ") " + num2 + " (" + i + "," + j + ") " + num1);

                        //if (checktoler((num1+num2),sumpos,tolerance)) {
                        num1 = Math.floor(num1 * 100) / 100;
                        num2 = Math.floor(num2 * 100) / 100;
                        sumpos = Math.floor(sumpos * 100) / 100;
                        if (num1 + num2 == sumpos) {
                            found = true;
                            hanaha = num2;
                        }

                        //   find2numbers = checkWithSimilarNumbersReplace(num1, num2, sumpos);
                        // find2numbers = checkWithSimilarNumbersReplace(num1, num2, sumpos);
                    }
                }
            }
        }
        if (found) {
            System.out.println("find2numbers!!!");
            //get the hanaha and check if qeuals in one of the variation of the neg vector.

            if (checkWithSimilarNumbersReplace(sumneg, hanaha, bnv)) {
                //sumrowpricesv.setSum(sumpos;
                //sumrowpricesv.setVerifived(true);
                sumallWithMamv.setSum(Double.parseDouble(new DecimalFormat("##.##").format((sumpos - hanaha - globalanahav.getSum()))));
                sumrowpricesHSv.setSum(Double.parseDouble(new DecimalFormat("##.##").format((sumpos))));
                sumrowpricesHSv.setVerifived(true);
                sumallWithMamv.setVerifived(true);
            }

        }
    }

    /**
     * In this function we check the similar numbers: 0 -> 8 , 3 -> 8 , 4 -> 1 ,
     * 8 -> 0
     *
     * we use a Binary Tree to check all the variations.
     *
     * @return if a+b=c after the smart change.
     */
    private boolean checkWithSimilarNumbersReplace(Vector<Double> vec, double negprice, Vector<BNode> bnv) {
        boolean foundfirst = false, stop = false;
        double foundfix = 0;
        int foundfixindex = 0, size = 0;
        for (int i = 0; i < vec.size() && !stop; i++) {
            Vector<String> b_vector = createSimilarVector(String.valueOf(vec.get(i)));
            for (String k : b_vector) {
                double a = Double.valueOf(LFun.fixdot(k));
                if ((getSumOfVec(vec) + Double.valueOf(LFun.fixdot(k)) - vec.get(i) == negprice) /*&& (a != vec.get(i))*/) {
                    if (!foundfirst) {
                        foundfirst = true;
                        foundfix = Double.valueOf(LFun.fixdot(k));
                        foundfixindex = i;
                        size++;
                    } else {
                        write(p_filename, this.getClass().toString(), "the hanaha could not been replaced because we found two or more replaceable hanaha", "", is_log);
                        foundfirst = false;
                        stop = true;
                        break;
                    }
                }
            }
        } // to change the bnv  - bnv.elementAt(i).anaha
        if (foundfirst) {
            boolean check = true;
            for (BNode bn : bnv) {
                if (Math.abs(Double.valueOf(LFun.fixdot(bn.anaha))) == Math.abs(foundfix)) {
                    check = false;
                    break;
                }
            }
            if (check) {
                bnv.elementAt(foundfixindex).anaha = String.valueOf(foundfix);
            }
            write(p_filename, this.getClass().toString(), "The hanaha " + bnv.elementAt(foundfixindex).anaha + " replaced with " + String.valueOf(foundfix), "", is_log);
            return true;
        }
        return false;
    }

    private double getSumOfVec(Vector<Double> vec) {
        double all = 0;
        for (double i : vec) {
            all = all + i;
        }
        return all;
    }

    public boolean checkWithSimilarNumbersReplacePos(Vector<BNode> bnv) {
        double sumall = sumallbnv(bnv);
        double foundfix = 0;
        int foundfixindex = 0;
        boolean back = false, stop = false, foundfirst = false;
        for (int i = 0; i < bnv.size() && !stop; i++) {
            Vector<String> b_vector = createSimilarVector((bnv.elementAt(i).price.get_result_str()));
            for (String k : b_vector) {
                try {
                    if (sumall - Double.valueOf(bnv.elementAt(i).price.get_result_str()) + Double.valueOf(k) == sumrowpricesHSv.getSum()) {
                        if (!foundfirst) {
                            foundfirst = true;
                            foundfix = Double.valueOf(LFun.fixdot(k));
                            foundfixindex = i;
                        } else {
                            write(p_filename, this.getClass().toString(), "the price could not been replaced because we found two or more replaceable prices", "", is_log);
                            foundfirst = false;
                            stop = true;
                            break;
                        }

                    }
                } catch (Exception e) {
                    write(p_filename, this.getClass().toString(), e.getMessage(), "", is_log);
                }
            }
        }
        if (foundfirst) {
            sumrowpricesHSv.setVerifived(true);
            bnv.elementAt(foundfixindex).price.set_result_str(String.valueOf(foundfix));
            back = true;
            write(p_filename, this.getClass().toString(), "The price " + bnv.elementAt(foundfixindex).anaha + " replaced with " + String.valueOf(foundfix), "", is_log);

        }
        return back;
    }

    private double sumallbnv(Vector<BNode> bnv) {
        double sum = 0;
        for (BNode i : bnv) {
            sum = sum + Double.valueOf(LFun.fixdot(i.price.get_result_str()));
        }
        return sum;
    }

    /**
     * This function create a vector that has all the variations of the number.
     */
    private Vector<String> createSimilarVector(String number_string) {
        int MAX_LIMIT = 64, COUNT_LIMIT = 0; // Eli says that we need to limit this function.
        number_string = number_string.replaceAll(",", ".");
        Vector<String> vector = new Vector<String>();
        vector.add(number_string);

        for (int i = 0; i < number_string.length() && COUNT_LIMIT <= MAX_LIMIT; i++) {
            int lengthIndex = vector.size(); // make sure it's not stack on 0,8.
            for (int j = 0; j < lengthIndex; j++) {
                if (vector.get(j).charAt(i) == '0') {
                    vector.add(replaceCharAtID(vector.get(j), i, '8'));
                    COUNT_LIMIT++;
                } else if (vector.get(j).charAt(i) == '3') {
                    vector.add(replaceCharAtID(vector.get(j), i, '8'));
                    COUNT_LIMIT++;
                    vector.add(replaceCharAtID(vector.get(j), i, '4'));
                    COUNT_LIMIT++;
                } else if (vector.get(j).charAt(i) == '4') {
                    vector.add(replaceCharAtID(vector.get(j), i, '1'));
                    COUNT_LIMIT++;
                } else if (vector.get(j).charAt(i) == '8') {
                    vector.add(replaceCharAtID(vector.get(j), i, '0'));
                    COUNT_LIMIT++;
                }
                if (COUNT_LIMIT == MAX_LIMIT) {
                    return vector;
                }
            }
        }

        return vector;
    }

    /**
     * This function create a vector that has all the variations of the number
     * and create "0." in the beginning of the string. :8
     */
    private Vector<String> createSimilarVector(String number_string, boolean isYael) {
        int MAX_LIMIT = 64, COUNT_LIMIT = 0; // Eli says that we need to limit this function.
        number_string = number_string.replaceAll(",", ".");
        Vector<String> vector = new Vector<String>(),
                copy = new Vector<String>(),
                copy1 = new Vector<String>();
        vector.add(number_string);

        for (int i = 0; i < number_string.length() && COUNT_LIMIT <= MAX_LIMIT; i++) {
            int lengthIndex = vector.size(); // make sure it's not stack on 0,8.
            for (int j = 0; j < lengthIndex; j++) {
                if (vector.get(j).charAt(i) == '0') {
                    vector.add(replaceCharAtID(vector.get(j), i, '8'));
                    COUNT_LIMIT++;
                } else if (vector.get(j).charAt(i) == '3') {
                    vector.add(replaceCharAtID(vector.get(j), i, '8'));
                    COUNT_LIMIT++;
                    vector.add(replaceCharAtID(vector.get(j), i, '4'));
                    COUNT_LIMIT++;
                } else if (vector.get(j).charAt(i) == '8') {
                    vector.add(replaceCharAtID(vector.get(j), i, '0'));
                    COUNT_LIMIT++;
                } else if (vector.get(j).charAt(i) == '4') {
                    vector.add(replaceCharAtID(vector.get(j), i, '1'));
                    COUNT_LIMIT++;
                }
                if (COUNT_LIMIT == MAX_LIMIT) {
                    return vector;
                }
            }
        }

        copy1 = new Vector<String>(vector);
        for (String cp : copy1) {
            String string = cp;
            if (!cp.contains(".") && !cp.contains(",")) {
                for (int i = 1; i < cp.length() - 1; i++) {
                    string = cp.substring(0, i + 1) + "." + cp.substring(i + 1);
                    try {
                        Double.valueOf(string);
                        if (!string.equals("1.0") && !string.equals("0.0")) {
                            vector.add(string);
                            COUNT_LIMIT++;
                        }
                    } catch (Exception f) {
                    }
                    if (COUNT_LIMIT == MAX_LIMIT) {
                        return vector;
                    }
                }
            } else if (cp.contains(".00")) {
                cp = cp.replace(".00", "");
                for (int i = 1; i < cp.length(); i++) {
                    string = cp.substring(0, i) + "." + cp.substring(i);
                    try {
                        Double.valueOf(string);
                        if (!string.equals("1.0") && !string.equals("0.0")) {
                            vector.add(string);
                            COUNT_LIMIT++;
                        }
                    } catch (Exception e) {
                    }
                    if (COUNT_LIMIT == MAX_LIMIT) {
                        return vector;
                    }
                }
            }
        }

        copy = new Vector<String>(vector);
        for (String cp : copy) {
            String string = cp;
            if (cp.contains(".")) {
                string = cp.replace(".", "");

            }
            string = "0." + string;

            try {
                Double.valueOf(string);
                if (!string.equals("1.0") && !string.equals("0.0")) {
                    vector.add(string);
                    COUNT_LIMIT++;

                }
            } catch (Exception f) {
            }

            if (COUNT_LIMIT == MAX_LIMIT) {
                return vector;
            }
        }

        return vector;
    }

    private Vector<String> createSimilarVectorWithLimit(String number_string, int limitOfChanges) {
        int MAX_LIMIT = 64, COUNT_LIMIT = 0; // Eli says that we need to limit this function.
        number_string = number_string.replaceAll(",", ".");
        Vector<String> vector = new Vector<String>(),
                copy = new Vector<String>(),
                copy1 = new Vector<String>();
        vector.add(number_string);

        if (isDigitalFile) {
            limitOfChanges = 0;
        }

        int[] numberOfChanges = new int[MAX_LIMIT];
        //intalize the array to 0
        for (int i = 0; i < numberOfChanges.length; i++) {
            numberOfChanges[i] = 0;
        }

        for (int i = 0; i < number_string.length() && COUNT_LIMIT <= MAX_LIMIT; i++) {
            int lengthIndex = vector.size(); // make sure it's not stack on 0,8.
            for (int j = 0; j < lengthIndex; j++) {

                if (numberOfChanges[j] < limitOfChanges) {
                    if (vector.get(j).charAt(i) == '0') {
                        vector.add(replaceCharAtID(vector.get(j), i, '8'));
                        COUNT_LIMIT++;
                        numberOfChanges[vector.size() - 1] = numberOfChanges[j] + 1;
                    } else if (vector.get(j).charAt(i) == '3') {
                        vector.add(replaceCharAtID(vector.get(j), i, '8'));
                        COUNT_LIMIT++;
                        numberOfChanges[vector.size() - 1]++;
                    } else if (vector.get(j).charAt(i) == '8' && i != 0) {
                        vector.add(replaceCharAtID(vector.get(j), i, '0'));
                        COUNT_LIMIT++;
                        numberOfChanges[vector.size() - 1] = numberOfChanges[j] + 1;
                    } else if (vector.get(j).charAt(i) == '4') {
                        vector.add(replaceCharAtID(vector.get(j), i, '1'));
                        COUNT_LIMIT++;
                        numberOfChanges[vector.size() - 1] = numberOfChanges[j] + 1;
                    }
                    /*else if(vector.get(j).charAt(i) == '1'){
                     vector.add(replaceCharAtID(vector.get(j), i, '4'));
                     COUNT_LIMIT++;  
                     numberOfChanges[vector.size() - 1] = numberOfChanges[j] + 1; 
                     }*/
                    if (vector.get(j).charAt(i) == '3') {
                        vector.add(replaceCharAtID(vector.get(j), i, '4'));
                        COUNT_LIMIT++;
                        numberOfChanges[vector.size() - 1] = numberOfChanges[j] + 1;
                    }
                    if (COUNT_LIMIT >= MAX_LIMIT) {
                        return vector;
                    }
                }
            }
        }

        copy1 = new Vector<String>(vector);
        for (String cp : copy1) {
            String string = cp;
            if (!cp.contains(".") && !cp.contains(",")) {
                for (int i = 1; i < cp.length() - 1; i++) {
                    string = cp.substring(0, i + 1) + "." + cp.substring(i + 1);
                    try {
                        Double.valueOf(string);
                        if (!string.equals("1.0") && !string.equals("0.0")) {
                            vector.add(string);
                            COUNT_LIMIT++;
                        }
                    } catch (Exception f) {
                    }
                    if (COUNT_LIMIT == MAX_LIMIT) {
                        return vector;
                    }
                }
            } else if (cp.contains(".00")) {
                cp = cp.replace(".00", "");
                for (int i = 1; i < cp.length(); i++) {
                    string = cp.substring(0, i) + "." + cp.substring(i);
                    try {
                        Double.valueOf(string);
                        if (!string.equals("1.0") && !string.equals("0.0")) {
                            vector.add(string);
                            COUNT_LIMIT++;
                        }
                    } catch (Exception e) {
                    }
                    if (COUNT_LIMIT == MAX_LIMIT) {
                        return vector;
                    }
                }
            }
        }

        String s = "";
        if (limitOfChanges == 1) {
            if (!vector.get(0).contains(".") && !vector.get(0).contains(",")) {
                s = "0." + vector.get(0);
            }
            try {
                Double.valueOf(s);
                if (!s.equals("1.0") && !s.equals("0.0")) {
                    vector.add(s);
                    COUNT_LIMIT++;

                }
            } catch (Exception f) {
            }

        } else {
            copy = new Vector<String>(vector);
            for (String cp : copy) {
                String string = cp;
                if (cp.contains(".")) {
                    string = cp.replace(".", "");

                }
                string = "0." + string;
                try {
                    Double.valueOf(string);
                    if (!string.equals("1.0") && !string.equals("0.0")) {
                        vector.add(string);
                        COUNT_LIMIT++;
                    }
                } catch (Exception f) {
                }

                if (COUNT_LIMIT == MAX_LIMIT) {
                    return vector;
                }
            }
        }

        return vector;
    }

    /**
     * Replace char at id: replaceCharAtID("Hello",0,'W') -> "Wello"
     */
    public String replaceCharAtID(String str, int index, char replace) {
        if (str == null) {
            return str;
        } else if (index < 0 || index >= str.length()) {
            return str;
        }
        char[] chars = str.toCharArray();
        chars[index] = replace;
        return String.valueOf(chars);
    }

    private int GetLastLineOfBnv(Vector<BNode> bnv) {
        int lastline = 0;

        BNode bn = bnv.lastElement();
        if (bn.date != null && bn.date.get_line_num() > lastline) {
            lastline = bn.date.get_line_num();
        }
        if (bn.fullprice != null && bn.fullprice.get_line_num() > lastline) {
            lastline = bn.fullprice.get_line_num();
        }
        if (bn.itemNumInOrder != null && bn.itemNumInOrder.get_line_num() > lastline) {
            lastline = bn.itemNumInOrder.get_line_num();
        }
        if (bn.itemNumInOrder != null && bn.itemNumInOrder.get_line_num() > lastline) {
            lastline = bn.itemNumInOrder.get_line_num();
        }
        if (bn.mam != null && bn.mam.get_line_num() > lastline) {
            lastline = bn.mam.get_line_num();
        }
        if (bn.one_price != null && bn.one_price.get_line_num() > lastline) {
            lastline = bn.one_price.get_line_num();
        }
        if (bn.orderNum != null && bn.orderNum.get_line_num() > lastline) {
            lastline = bn.orderNum.get_line_num();
        }
        if (bn.percent != null && bn.percent.get_line_num() > lastline) {
            lastline = bn.percent.get_line_num();
        }
        if (bn.qty != null && bn.qty.get_line_num() > lastline) {
            lastline = bn.qty.get_line_num();
        }
        if (bn.shipNum != null && bn.shipNum.get_line_num() > lastline) {
            lastline = bn.shipNum.get_line_num();
        }

        return lastline;
    }

    private Vector<Vector<Double>> getAmountsFromTheBnv(Vector<BNode> bnv) {
        Vector<Vector<Double>> AmountsFromTheBnv = new Vector<Vector<Double>>();
        Vector<Double> Hanaha = new Vector<Double>();
        Vector<Double> sum = new Vector<Double>();
        double Amount = 0;
        for (int i = 0; i < bnv.size(); i++) {
            bnv.elementAt(i).price.set_result_str(bnv.elementAt(i).price.get_result_str().replaceAll(",", "."));
            bnv.elementAt(i).anaha = bnv.elementAt(i).anaha.replaceAll(",", ".");
            try {
                Amount = Amount + Math.abs(Double.valueOf(bnv.elementAt(i).price.get_result_str()));//INONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
            } catch (Exception r) {
            }
            if (Math.abs(Double.valueOf(bnv.elementAt(i).anaha)) != 0) {
                Hanaha.add(Math.abs(Double.valueOf(bnv.elementAt(i).anaha)));
            }
        }

        sum.add(Amount);
        AmountsFromTheBnv.add(sum); // send in place 0 the sum.
        AmountsFromTheBnv.add(Hanaha); // send in place 1 the Hanaha.

        return AmountsFromTheBnv;

    }

    //return true if suspicious to be a price with at most two letters 
    //else return false
    public boolean can_Fix_Suspicious_Price(String price) {
        int price_length = price.length();
        int counter = 0;
        for (int i = 0; i < price_length; i++) {
            // check if number or not
            if (LFun.asci_range(price.charAt(i)) == 0) {
                counter++;
                if (counter > 2) {
                    return false;
                }
            }
        }
        return true;
    }

    public double fix_price(String sus_price) {

        String res = "";
        Double res2 = 0.00;

        for (int i = 0; i < sus_price.length(); i++) {

            if (sus_price.charAt(i) == 'o' || sus_price.charAt(i) == 'O' || sus_price.charAt(i) == 'ס'
                    || sus_price.charAt(i) == 'u' || sus_price.charAt(i) == 'U' || sus_price.charAt(i) == 'Q' || sus_price.charAt(i) == 'c') {
                res += "0";
            } else if (sus_price.charAt(i) == 'ו' || sus_price.charAt(i) == 'ז' || sus_price.charAt(i) == '|' || sus_price.charAt(i) == 'i'
                    || sus_price.charAt(i) == 'I') {
                res += "1";
            } else if (sus_price.charAt(i) == 'ב' || sus_price.charAt(i) == 'E') {
                res += "2";
            } else if (sus_price.charAt(i) == 'ר') {
                res += "7";
            } else if (sus_price.charAt(i) == 'B') {
                res += "8";
            } else if (sus_price.charAt(i) == ',' || sus_price.charAt(i) == '.') {
                res += ".";
            } else if (LFun.asci_range(sus_price.charAt(i)) == 1) {
                res += sus_price.charAt(i);
            } else if (sus_price.charAt(i) == 's' || sus_price.charAt(i) == 'S') {
                res += "5";
            } else if (sus_price.charAt(i) == 'G') {
                res += "6";
            } else {
                return 0.00;
            }
        }
        try {
            res2 = Double.valueOf(res);
        } catch (Exception e) {

        }

        return res2;
    }

    public Vector<ObData> tryUsadAndEurdNumbers(Vector<ObData> result, Vector<String> vnumdefault) {

        Vector<ObData> copy = new Vector<ObData>();
        String priceStr = "";
        ObData price;
        for (int i = 0; i < usadAndEurdNum.size(); i++) {
            price = new ObData(usadAndEurdNum.elementAt(i));
            priceStr = usadAndEurdNum.elementAt(i).get_result_str();
            for (int k = 0; k < 10; k++) {
                try {
                    copy = Copy(result);
                    price.set_result_str(priceStr + k);
                    Double num = Double.valueOf(price.get_result_str());
                    copy = add_fixed_price_to_vector(copy, price, vnumdefault);
                    if (sumrowpricesv.getVerifived()) {
                        Vector<ObData> line = lines.elementAt(usadAndEurdNum.elementAt(i).get_line_num() - 1);
                        for (int j = 0; j < line.size(); j++) {
                            if (price.get_result_str().contains(line.elementAt(j).get_result_str()) && price.get_right() == line.elementAt(j).get_right()) {
                                line.elementAt(j).set_result_str(priceStr + k);
                                line.elementAt(j).set_format(LFun.checkFormat(priceStr));
                                if (priceStr.charAt(0) != '-') {
                                    line.elementAt(j).set_name_by_inv("pos_price");
                                } else {
                                    line.elementAt(j).set_name_by_inv("neg_price");
                                }
                            }
                        }
                        return copy;
                    }
                } catch (Exception e) {

                }
            }
        }

        return result;
    }

    /**
     * add fixed prices to vector result by top
     *
     * @param result
     * @param fixedPrices
     * @return vector result after add the fixed prices
     */
    public Vector<ObData> add_fixed_prices_to_vector_result(Vector<ObData> result, Vector<ObData> fixedPrices) {

        int count = 0;
        int top;
        for (int i = 0; i < fixedPrices.size(); i++) {
            top = fixedPrices.elementAt(i).get_top();
            for (int j = 0; j < result.size(); j++) {
                if (result.elementAt(j).get_top() > top) {
                    result.add(j, fixedPrices.elementAt(i));
                    break;
                }

            }
        }

        for (int i = 0; i < result.size(); i++) {
            result.elementAt(i).set_num_block_by_inv(count);
            count++;
        }

        result = findsumRowPrices(true, -1, result);
        return result;
    }

    public Vector<ObData> add_fixed_price_to_vector(Vector<ObData> result, ObData price, Vector<String> vnumdefault) {

        int count = 0;
        int top = price.get_top();

        for (int j = 0; j < result.size(); j++) {
            if (result.elementAt(j).get_top() > top) {
                result.add(j, price);
                break;
            }
        }

        for (int i = 0; i < result.size(); i++) {
            result.elementAt(i).set_num_block_by_inv(count);
            count++;
        }

        result = sumVectorResultWithoutTolerance(result, vnumdefault);
        return result;
    }

    public Vector<ObData> sumVectorResultWithoutTolerance(Vector<ObData> result, Vector<String> vnumdefault) {

        int length = 0;
        if (result.size() > 10) {
            length = 5;
        } else {
            length = result.size() / 2;
        }
        int numOfDeletions = 0;
        boolean canBeSum = false;

        Double sumalltemp = 0.0;
        boolean flagremove = false;
        //סוכם את כל המספרים
        for (int i = 0; i < result.size(); i++) {
            sumalltemp = sumalltemp + Double.valueOf(LFun.fixdot(result.elementAt(i).get_result_str()));
        }

        boolean fstop = false;
        for (int i = result.size() - 1; i >= 0 && !fstop; i--) {
            sumalltemp = sumalltemp - Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()));
            numOfDeletions++;
            if (i == 1 && result.size() > 2) {
                continue;
            }
            //    System.out.println("sumalltemp = "+sumalltemp);
            if (sumalltemp > 0 && Double.parseDouble(result.elementAt(i).get_result_str()) > 0) {
                if (checktoler(sumalltemp, Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str())), 0.00001)) // if(sumalltemp<(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()))+0.5) && sumalltemp>(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str()))-0.5))
                {
                    if (numOfDeletions > length) {
                        Vector<ObData> line = LFun.lineByIndex(result.elementAt(i).get_line_num(), od);
                        for (ObData ob : line) {
                            for (String s : config.getForPayWords()) {
                                if (ob.get_result_str().contains(s)) {
                                    canBeSum = true;
                                }
                            }
                        }
                    } else {
                        canBeSum = true;
                    }
                    if (canBeSum) {
                        fstop = true;
                        flagremove = true;

                        sumrowpricesv = new VerVar(Double.parseDouble(LFun.fixdot(result.elementAt(i).get_result_str())));//sumalltemp
                        sumrowpricesv.setVerifived(true);

                        i = searchDuplicate(i, result);
                    }
                }
            }

            if (flagremove) {

                for (int j = i; j < result.size(); j++) {
                    if (LFun.contain_type(vnumdefault, result.elementAt(j).get_format())) {
                        lines.elementAt(result.elementAt(j).get_line_num() - 1).elementAt(result.elementAt(j).get_inx_renum()).set_name_by_inv("num");
                    } else {
                        lines.elementAt(result.elementAt(j).get_line_num() - 1).elementAt(result.elementAt(j).get_inx_renum()).set_name_by_inv("free");
                    }
                    result.removeElementAt(j);
                    j--;
                }
            }
        }

        return result;
    }

    public boolean price_By_place(ObData sus_to_be_price, Vector<ObData> result) {

        int line = sus_to_be_price.get_line_num();

        /*for(int i = 0; i < result.size(); i++){
         if((result.elementAt(i).get_line_num() + 1) == line || (result.elementAt(i).get_line_num() - 1) == line ||
         result.elementAt(i).get_line_num() == line ){
         return false;  
         }
         }*/
        int top = sus_to_be_price.get_top();
        int bottom = sus_to_be_price.get_bottom();
        int right = sus_to_be_price.get_right();
        int current_top;
        int current_bottom;

        for (int i = 0; i < result.size(); i++) {
            current_top = result.elementAt(i).get_top();
            current_bottom = result.elementAt(i).get_bottom();

            if ((current_top > bottom && current_top - DEVIATION < bottom)
                    || (top > current_bottom && current_bottom + DEVIATION > top)) {

                int min = result.elementAt(i).get_right() - DEVIATION;
                int max = result.elementAt(i).get_right() + DEVIATION;
                if (min <= right && max >= right) {
                    return true;
                }
            }
        }

        return false;
    }

    public Double searchPriceInCsv(BNode price) {

        return 0.00;

    }

    class Hanaha {

        String item_id = "";
        ObData hanaha;

        Hanaha(String _item_id, ObData _hanaha) {
            item_id = _item_id;
            hanaha = _hanaha;
        }
    }

//create private log.
    public void inon_log() {

        String PATH = ConfigFun.wiseBillFolderPath + "\\inon-Log\\";
        File mylogFolder = new File(PATH);
        try {
            mylogFolder.createNewFile();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Can't create folder.");
        }
        File mylogFile = new File(PATH + "inon-Log.csv");
        try {
            mylogFile.createNewFile();
        } catch (Exception e) {
            throw new UnsupportedOperationException("Can't create file.");

        }
        String content = "";

        if (mylogFile.exists()) {

            try {
                FileReader reader = new FileReader(mylogFile);
                char[] chars = new char[(int) mylogFile.length()];
                reader.read(chars);
                content = new String(chars);
                reader.close();
            } catch (Exception e) {
            }

        }

        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(mylogFile)));
            writer.write(content + System.getProperty("line.separator") + new File(path).getName().replace(".csu", "") + "," + sumallMamv.getSum() + "," + sumallNoMamv.getSum() + "," + sumallWithMamv.getSum() + "," + sumallMamv.getVerifived() + "," + sumallNoMamv.getVerifived() + "," + sumallWithMamv.getVerifived());

        } catch (IOException ex) {
            // report
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {

            }
        }
    }

    public static Vector<String> Filling_vnumdefault() {
        //    Vector<String> vnum=new Vector<String>();
        // vnum.add("USADD");
        // vnum.add("EURDD");  
        // vnum.add("USADDN");
        // vnum.add("EURDDN"); 
        // vnum.add("INTP");
        // vnum.add("INTPSEP");   
        Vector<String> vnumdefault = new Vector<String>();

        vnumdefault.add("USAD");
        vnumdefault.add("EURD");
        vnumdefault.add("USADN");
        vnumdefault.add("EURDN");
        vnumdefault.add("USADD");
        vnumdefault.add("EURDD");
        vnumdefault.add("USADDN");
        vnumdefault.add("EURDDN");
        vnumdefault.add("USADDD");
        vnumdefault.add("EURDDD");
        vnumdefault.add("USADDDN");
        vnumdefault.add("EURDDDN");
        vnumdefault.add("INTP");
        //vnumdefault.add("INTPSEP");
        return vnumdefault;
    }
}
