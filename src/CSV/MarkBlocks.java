/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import Constructors.ObData;
import java.util.Vector;
import javax.swing.JOptionPane;
import static CSV.Log.write;

/**
 *
 * class for marking blocks
 */
public class MarkBlocks {

    public static int endAllBlocks = 0;
    public static Vector<String> ForPayWords = new Vector<String>();

    /**
     * marking blocking
     *
     * @param lines
     * @param firstpricelinecsv
     * @param lastpricelinecsv
     */
    static int blockmarking(Vector<Vector<ObData>> lines, int firstpricelinecsv, int lastpricelinecsv, boolean isVerifived, Vector<ObData> result, Vector<String> i_ForPayWords, ObDataWithLines _ObDataWithLines) {
        ForPayWords = i_ForPayWords;

        //start block 0 from first price row -1
        int start_prices = getLineNumBycsvline(lines, firstpricelinecsv) - 1;
        if (start_prices == -1) {
            start_prices = 0;
        }

        //find start first block
        start_prices = getStartFirstBlock(lines, start_prices);
        if (start_prices == -1) {
            write("", "MarkBlocks", "NOT found blocks", "", true);

            return 0;
        }

        //end last block  after last price row +1
        int end_prices = getLineNumBycsvline(lines, lastpricelinecsv) + 1;

        //block counter
        int countbloks = 0;

        /*boolean shema[][] = new boolean[lines.size()][4];

        //fill shema
        for (int i = start_prices; i < end_prices && i < lines.size(); i++) {

            if (lines.elementAt(i).size() > 0) {
                if (isLineContain(lines.elementAt(i), "pn", "MAKAT")) {

                    shema[i][2] = true;
                }
                if (isLineContain(lines.elementAt(i), "desc", "ITEMDESC")) {

                    shema[i][1] = true;
                }
                if (isLineContain(lines.elementAt(i), "pn", "no")) {

                    shema[i][3] = true;
                }
                if (isLineContain(lines.elementAt(i), "price", "no")) {

                    shema[i][0] = true;
                }

            }
        }

        printshema(shema);*/
        printLines(lines);
        /*
         if (isVerifived) {
         countbloks = result.size();
         } else {
         */
        int counter = 0, counter_item = 0;
        int lastEnd = 0;
        int e = 0;
        for (int i = start_prices; i < end_prices && i < lines.size();) {
            if (lines.elementAt(i).size() > 0) {
                counter++;

                int[] separatorLine = null;
                if (_ObDataWithLines != null && _ObDataWithLines.getSize() > 0 && counter < result.size()) {
                    separatorLine = _ObDataWithLines.getSeparatorLine(result.get(counter_item));
                }
                if (separatorLine == null) {
                    e = getEndBlock(lines, i);
                } else {
                    e = getEndBlockBySeparator(lines, i, separatorLine);
                }
                if (lastEnd > e) {
                    break;
                }

                if (e < 0) {
                    e = i + 2;
                }

                if (CSV2Hash.isA4 && separatorLine == null) {
                    e = endBlockInA4(lines, e);
                }
                markRow(lines, countbloks, i, e);

                countbloks++;
                i = e + 1;
                lastEnd = e;
                //System.out.println(e);
                if (counter > 3000) {
                    return -1;
                }

            } else {
                i++;
            }

            counter_item++;
        }

        end_prices = e + 1;
//        }

        //print temporary
        //print
        for (int i = start_prices; i < end_prices && i < lines.size(); i++) {//
//            System.out.println("\nblock: " + lines.elementAt(i).elementAt(0).get_num_block_by_inv());
            for (int j = 0; j < lines.elementAt(i).size(); j++) {

                if (lines.elementAt(i).elementAt(j).get_name_by_inv().contains("pn")
                        || lines.elementAt(i).elementAt(j).get_name_by_inv().contains("price")
                        || lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("qty")
                        || lines.elementAt(i).elementAt(j).get_name_by_inv().equalsIgnoreCase("one_pr")) {
                    System.out.print("\t " + lines.elementAt(i).elementAt(j).get_result_str() + " " + lines.elementAt(i).elementAt(j).get_name_by_inv());
                }

            }

        }

        // int en=0;
        correctBlocksByQTYPrice(lines, start_prices, end_prices);

        endAllBlocks = end_prices;

        try {
            if (result != null && !result.isEmpty() && end_prices - start_prices < result.size() - 2) {
                endAllBlocks = result.get(result.size() - 1).get_line_num();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return countbloks;
    }

    static public int endBlockInA4(Vector<Vector<ObData>> lines, int currentEnd) {
        int newEnd = currentEnd;
        /*If the next line have less then ~45% words - 
        so, we will add the next line to the current block (probably it`s line that contain description words)*/
        while (newEnd + 1 < lines.size() && checkIfAddLine(lines, newEnd)) {
            newEnd++;
        }
        return newEnd;
    }

    static public boolean checkIfAddLine(Vector<Vector<ObData>> lines, int currentEnd) {
        boolean addLine = false;
        double maxWords = 0;
        if (lines.get(currentEnd).size() > 10) {
            maxWords = lines.get(currentEnd).size() * 0.5;
        } else if (lines.get(currentEnd).size() <= 2) {
            maxWords = 1;
        } else {
            maxWords = lines.get(currentEnd).size() * 0.4;
        }

        if (maxWords >= lines.get(currentEnd + 1).size() && !LFun.iscontain_s(lines.get(currentEnd + 1), "price") && !haveForPayWord(lines.get(currentEnd + 1))/*&& !LFun.iscontain_s(lines.get(currentEnd + 1), "pn")*/) {
            addLine = true;
        }
        return addLine;
    }

    /**
     * correct mark blocks by up down price
     *
     * @param lines
     * @param firstpricelinecsv
     * @param lastpricelinecsv
     */
    static public void correctBlocksByQTYPrice(Vector<Vector<ObData>> lines, int firstpricelinecsv, int lastpricelinecsv) {

        int start = firstpricelinecsv - 1;
        if (start == -1) {
            start = 0;
        }
        for (int i = start; i < lastpricelinecsv; i++) {
            double priceUP = 0;
            double priceDOWN = 0;
            double ch = isContainQTYandOnePrice(lines.elementAt(i));
            if (ch > 0) {
                if (i > firstpricelinecsv - 1) {
                    priceUP = getPriceByRow(lines.elementAt(i - 1));
                }

                if (i < lastpricelinecsv - 1) {
                    priceDOWN = getPriceByRow(lines.elementAt(i + 1));
                }

                if (priceUP > 0 && LFun.checktoler(ch, priceUP, 0.01)) {

                    for (int k = 0; k < lines.elementAt(i).size(); k++) {
                        lines.elementAt(i).elementAt(k).set_num_block_by_inv(lines.elementAt(i - 1).elementAt(0).get_num_block_by_inv());
                    }
                } else if (priceDOWN > 0 && LFun.checktoler(ch, priceDOWN, 0.01)) {

                    for (int k = 0; k < lines.elementAt(i).size(); k++) {
                        lines.elementAt(i).elementAt(k).set_num_block_by_inv(lines.elementAt(i + 1).elementAt(0).get_num_block_by_inv());
                    }
                } else {
                    System.out.println("need check in list one price");

                    double oPr = getOnePriceFromRow(lines.elementAt(i));

                    System.out.println(oPr);

                }
            }

        }
    }

    /**
     * get price by row
     *
     * @param row
     * @return
     */
    static double getOnePriceFromRow(Vector<ObData> row) {
        for (ObData rd : row) {
            if (rd.get_name_by_inv().equalsIgnoreCase("one_pr")) {
                return Double.parseDouble(LFun.fixdot(rd.get_result_str()));
            }
        }
        return 0;
    }

    /**
     * get price by row
     *
     * @param row
     * @return
     */
    static double getPriceByRow(Vector<ObData> row) {
        for (ObData rd : row) {
            if (rd.get_name_by_inv().equalsIgnoreCase("pos_price")) {
                try {
                    return Double.parseDouble(LFun.fixdot(rd.get_result_str()));
                } catch (Exception e) {

                }

            }
        }
        return 0;
    }

    /**
     * if row contain QTY and Price
     *
     * @param elementAt
     * @return
     */
    static double isContainQTYandOnePrice(Vector<ObData> row) {
        double contQTY = 0;
        double contOPrice = 0;
        for (ObData rd : row) {
            if (rd.get_name_by_inv().equalsIgnoreCase("qty")) {
                contQTY = Double.parseDouble(LFun.fixdot(rd.get_result_str()));
            }
            if (rd.get_name_by_inv().equalsIgnoreCase("one_pr")) {
                contOPrice = Double.parseDouble(LFun.fixdot(rd.get_result_str()));
            }
        }

        double res = contQTY * contOPrice;
        if (res > 0) {
            return res;
        }

        return 0;
    }

    /**
     * print shema
     *
     * @param shema
     */
    static public void printshema(boolean[][] shema) {
        String p = "|\t";
        for (int idx = 0; idx < shema.length; idx++) {
            System.out.print("|\n---------------------------------------------------------\n|" + idx + "\t");
            for (int i = 0; i < shema[idx].length; i++) {
                if (shema[idx][i] && i == 0) {
                    p = "|Price\t";
                } else if (shema[idx][i] && i == 1) {
                    p = "|DescL\t";
                } else if (shema[idx][i] && i == 2) {
                    p = "|PnL\t";
                } else if (shema[idx][i] && i == 3) {
                    p = "|Pn\t";
                } else {
                    p = "|\t";
                }
                System.out.print(p);
                p = "|\t";
            }
        }
    }

    /**
     * return follow index by input param
     *
     * @param line
     * @param start
     */
    static int getFollowInd(Vector<Vector<ObData>> lines, int start, String name, String external) {

        for (int i = start; i < lines.size(); i++) {
            if (lines.elementAt(i).size() > 0
                    && isLineContain(lines.elementAt(i), name, external)) {
                return i;

            }

        }

        return -1;
    }

    static int numberOfPrices(Vector<Vector<ObData>> lines, int start, String name, String external) {

        Vector<ObData> line = lines.elementAt(start);
        int numberOfPrices = 0;
        for (int k = 0; k < line.size(); k++) {
            if (line.elementAt(k).get_name_by_inv().contains(name)
                    && line.elementAt(k).get_external_source().contains(external)) {
                numberOfPrices++;
            }
        }
        return numberOfPrices;
    }

    static int getEndBlockBySeparator(Vector<Vector<ObData>> lines, int start, int[] separator) {
        int end = -1;

        for (int i = start; i < lines.size(); i++) {
            if (lines.get(i).size() > 0) {
                if ((lines.get(i).get(0).get_top() + lines.get(i).get(0).get_bottom()) / 2 < separator[1]) {
                    end = i;
                } else {
                    return end;
                }
            }

        }
        return end;
    }

    /**
     * return end first block index
     *
     * @param line
     * @param start
     */
    static int getEndBlock(Vector<Vector<ObData>> lines, int start) {

        int pr = getFollowInd(lines, start, "price", "no");
        pr = getFollowInd(lines, pr + 1, "price", "no");

        int pnl = getFollowInd(lines, start, "pn", "INTP");
        /*int neg = getFollowInd(lines, start, "neg_price", "no");
        
        boolean onePrice = false;
        int endBlock = 0;
        
        if(pr == neg){
            if(numberOfPrices(lines, pr, "price", "no") == 1){
              pr = getFollowInd(lines, pr + 1, "price", "no");  
              onePrice = true;
            }
        }
        
        if(neg == pnl && onePrice){
            endBlock = pnl;
            pnl = getFollowInd(lines, pnl + 1, "pn", "INTP");
            if(pnl == -1 && pr == -1){
               pnl = endBlock++; 
            }
        }*/

        if (pr != -1 && pnl != -1 && pnl < pr) {
            pnl = getFollowInd(lines, pnl + 1, "pn", "INTP");
        }
        int descl = getFollowInd(lines, start, "desc", "ITEMDESC");

        if (pr != -1 && descl != -1 && descl < pr) {
            descl = getFollowInd(lines, descl + 1, "desc", "ITEMDESC");
        }
        int pn = getFollowInd(lines, start, "pn", "no");

        if (pr != -1 && pn != -1 && pn < pr) {
            pn = getFollowInd(lines, pn + 1, "pn", "no");
        }

        int minpoint = 0;

        if (pr > 0 && pn > 0) {
            minpoint = Math.min(pr, pn);
        } else {
            minpoint = Math.max(pr, pn);
        }

        if (minpoint > 0 && descl > 0) {
            minpoint = Math.min(minpoint, descl);
        } else {
            minpoint = Math.max(minpoint, descl);
        }

        if (minpoint > 0 && pnl > 0) {
            minpoint = Math.min(minpoint, pnl);
        } else {
            minpoint = Math.max(minpoint, pnl);
        }

        if (minpoint == -1) {
            minpoint = getFollowInd(lines, start, "price", "no");
            return minpoint;
        }

        if (minpoint == start) {
            return minpoint;
        }
        return minpoint - 1;
    }

    /**
     * return start first block index
     *
     * @param line
     * @param start
     */
    static int getStartFirstBlock(Vector<Vector<ObData>> lines, int start) {
        int bef = Math.max(0, start);
        start = getFollowInd(lines, start, "price", "no");

        if (start == -1) {
            write("", "MarkBlocks", "NOT found blocks", "", true);

            return -1;
        }

        //search pn by list
        for (int i = start - 1; i >= bef; i--) {
            if (lines.elementAt(i).size() > 0
                    && isLineContain(lines.elementAt(i), "pn", "INTPSEP")) {
                return i;

            }

        }

        //search pn by list
        for (int i = start - 1; i >= bef; i--) {
            if (lines.elementAt(i).size() > 0
                    && isLineContain(lines.elementAt(i), "pn", "MAKAT")) {
                return i;

            }

        }

        //search desc by list
        for (int i = start - 1; i >= bef; i--) {
            if (lines.elementAt(i).size() > 0
                    && isLineContain(lines.elementAt(i), "desc", "ITEMDESC")) {
                return i;

            }

        }

        //search pn by regex
        for (int i = start - 1; i >= bef; i--) {
            if (lines.elementAt(i).size() > 0
                    && isLineContain(lines.elementAt(i), "pn", "no")) {
                return i;

            }

        }

        //if not found return price index
        return start;
    }

    /**
     * add block number to objects
     *
     * @param line
     * @param c
     */
    static void markRow(Vector<Vector<ObData>> lines, int c, int start, int end) {

        for (int i = start; i <= end; i++) {
            if (lines.elementAt(i).size() > 0) {
                for (int k = 0; k < lines.elementAt(i).size(); k++) {
                    lines.elementAt(i).elementAt(k).set_num_block_by_inv(c);
                }
            }
        }
    }

    /**
     * search if line contain s in name by inv and external name in external
     * source
     *
     * @param line
     * @param s
     * @return
     */
    static boolean isLineContain(Vector<ObData> line, String s, String external) {
        for (int k = 0; k < line.size(); k++) {
            if (line.elementAt(k).get_name_by_inv().contains(s)
                    && line.elementAt(k).get_external_source().contains(external)) {
                return true;
            }
        }
        return false;
    }

    /**
     * return line number in vector by csv line number
     *
     * @param lines
     * @param lastpricelinecsv
     * @return
     */
    static int getLineNumBycsvline(Vector<Vector<ObData>> lines, int lastpricelinecsv) {
        //find last price index
        int i = 0;
        for (; i < lines.size(); i++) {
            if (lines.elementAt(i).size() > 0
                    && lines.elementAt(i).elementAt(0).get_line_num() == lastpricelinecsv) {
                return i;
            }

        }

        return i;
    }

    //old search from main class
    /* 
     int first=nextpriceline(0);
     int end=nextpriceline(first+1);
        
         
         
     int shope=0;
     int ehope=0;
         
         
     shope=first;
     ehope=end;
        
     // temporary
     //  ehope=shope;
     //  shope=shope-1; 
           
          
     //temporary print
     for (int i = shope; i<lines.size() ; i++) {
     if(lines.elementAt(i).size()>0 ){
     //   System.out.println();
     for (int k = 0; k < lines.elementAt(i).size(); k++) {
     //   System.out.print(" "+lines.elementAt(i).elementAt(k).get_result_str());
     }
     }
     }
         
         
     Vector<Vector<ObData>> lines_t=new Vector<Vector<ObData>>();
     Vector<ObData> oneblock=new Vector<ObData>();  
         
         
     for (int i = shope; i <= ehope && i<=end_prices && i<lines.size() ; i++) {
     if(lines.elementAt(i).size()>0 ){
     if(i==ehope ){//
                         
     ehope=nextpriceline(i+1);//i+1
                        
     j++;  
     if(oneblock!=null && oneblock.size()>0)
     {
     lines_t.add(new Vector<ObData>(oneblock));
     oneblock=new Vector<ObData>();
     }
     }  
                  
     for (int k = 0; k < lines.elementAt(i).size(); k++) {
                     
                                     
                    
                     
                 
     lines.elementAt(i).elementAt(k).set_num_block_by_inv(j);      
     oneblock.add(lines.elementAt(i).elementAt(k)); 
     }}}
         
     System.out.println("\ncountblock "+j); 
     countblock=j;
        
    
          
       
     lines=new Vector<Vector<ObData>>(lines_t);
     System.out.println("Start print");    
     for (int i = 0; i < lines.size(); i++) {
     System.out.println();
     for (int k = 0; k < lines.elementAt(i).size(); k++) {
     System.out.print(" "+lines.elementAt(i).elementAt(k).get_result_str());
                 
     }
             
     }
     System.out.println("\nEnd print");     
     */
    public static void printLines(Vector<Vector<ObData>> lines) {
        System.out.println("Size: " + lines.size());
        int i = 0, j = 0;
        for (Vector<ObData> obdv : lines) {
            System.out.println("i = " + i);
            for (ObData obd : obdv) {
                System.out.println("\tj = " + j + "\tname_by_inv - " + obd.get_name_by_inv() + "\texternal_source - " + obd.get_external_source() + "\tresult_str - " + obd.get_result_str());
                j++;
            }
            i++;
            j = 0;
        }
        String a = "";
    }

    private static boolean haveForPayWord(Vector<ObData> line) {
        for (ObData ob : line) {
            for (String s : ForPayWords) {
                if (ob.get_result_str().contains(s)) {
                    return true;
                }
            }
        }

        return false;
    }
}
