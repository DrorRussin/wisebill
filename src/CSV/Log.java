/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author Inon David
 */
/**
 * Example: write(fileName, this.getClass().toString(), "Exeption", "Inon");
 *
 *
 */
public class Log {

    static String FILE_NAME = "WiseBill",
            //PATH = System.getProperty("user.dir") + "\\log\\",
            PATH = "C:\\HyperOCR\\log\\WiseBill\\",
            TYPE = ".csv",
            Date,
            Hour;
    static String WISE_FILE_NAME = "WiserunWiseBill";
    static File logFile =null,
            logWiserunFile=null;

    public static File wiseLogFile() {
        new File(PATH).mkdirs();
        logWiserunFile = new File(PATH + "" + WISE_FILE_NAME + "" + TYPE);
        if (!logWiserunFile.exists()) {
            try {
                logWiserunFile.createNewFile();
                writeFirstLine();
            } catch (Exception e) {
                throw new UnsupportedOperationException("Can't create file.");

            }
        }
        return logWiserunFile;
    }

    public static void createNewLogFile() {
        Date = getDate();
        Hour = getHour();
        new File(PATH).mkdirs();
        logFile = new File(PATH + "" + Date + "" + TYPE);

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
                writeFirstLine();
            } catch (Exception e) {
                throw new UnsupportedOperationException("Can't create file.");

            }

        }
    }

    public static void writeFirstLine() {

        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(logFile)));

            writer.write("Hour,File Name,Class Name,Exception,Comments");

        } catch (IOException ex) {
            // report
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/

            }
        }
    }

    public static void writeWiserunLog(String command) {
        File file = wiseLogFile();
        Writer writer = null;
        String oldString = readFile(file);
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));
            
            writer.write(oldString + System.getProperty("line.separator") + getDate() +","+ getHour() +","+ command);

        } catch (IOException ex) {
            // report
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/

            }
        }
    }

    /**
     * Get the date
     *
     * @return String Date - 2014/08/06 16:00:22 *
     */
    public static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime()); /*  2014/08/06 16:00:22   */

    }

    public static String getHour() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime()); /*  2014/08/06 16:00:22   */

    }

    public static void write(String file_name, String class_name, String exeption, String comments, boolean on_off) {
        if (on_off) {
            createNewLogFile();

            Writer writer = null;
            String old = readFile(logFile);
            if (old == null) {
                old = "";
            }

            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(logFile)));

                writer.write(old + System.getProperty("line.separator") + Hour + "," + file_name + "," + class_name + "," + exeption + "," + comments);               
                if(exeption.toLowerCase().contains("error")||comments.toLowerCase().contains("error")){
                    System.err.println(Hour + "," + file_name + "," + class_name + "," + exeption + "," + comments);
                }else{
                    System.out.println(Hour + "," + file_name + "," + class_name + "," + exeption + "," + comments);
                }

            } catch (IOException ex) {
                // report
            } finally {
                try {
                    writer.close();
                } catch (Exception ex) {/*ignore*/

                }
            }
        }
    }

    /**
     * @param file
     * @return text from file*
     */
    public static String readFile(File file) {
        String content = null;
        try {
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (Exception e) {
        }
        return content;
    }

}
