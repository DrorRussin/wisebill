/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

/**
 *
 * @author zvika
 */
public class GlobalVariables {

    public static Double ExchangeRate_Genral_to_NIS = 3.5900;
    //public static String [] RateSign= {"ש''ח", "$", "NIS", "dollar", "₪", "שח"};
    
    public static String ExchangeRate_Genral_to_NIS(String str_number){
        if(str_number!=null){
            String startsWith="";
            if(str_number.startsWith("~")){
                startsWith = "~";
                str_number = str_number.substring(1);
            }
            double num = 0.0;
            
            try {
                num = Double.valueOf(str_number);
            } catch (Exception e) {
            }
            return startsWith +(num*ExchangeRate_Genral_to_NIS);
        }
        return str_number;
    }
}
