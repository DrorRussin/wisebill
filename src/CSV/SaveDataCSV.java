/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import Constructors.BNode;
import Constructors.InfoReader;
import Constructors.VerVar;
import Constructors.VerVarWM;
import Pathes.Path;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Vector;
import static CSV.Log.write;

/**
 *
 * @author user
 */
public class SaveDataCSV extends CSV.Log {

    /**
     * save all found to file
     *
     * @return
     */
    public static boolean SaveDataCSV(Vector<BNode> bnv, String path, Vector<VerVar> vVerVar, Vector<VerVarWM> vVerVarWM,InfoReader infoReader) {
        String orig_name = "";
        try {
//create name
            
            
            //header of title  File Name, Branch code, Cashier voucher,Date (DD.MM.YYYY), Time (HHMMSS),Cashier number,Items number, Total items price, Total items discounts, Delivery cost, Global discount,Total items price with taxes, Total tax-free price, Total tax price,Tax price, Member number (if exist), last 4 digits from credit card
            
            String[] filename = {
                "@;", //1
                "@;", //2
                "@;", //3
                "@;", //4
                "@;", //5
                "@;", //6
                "@;", //7
                "@;", //8
                "@;", //9
                "@;", //10
                "@;", //11
                "@;", //12
                "@;", //13
                "@;", //14
                "@;", //15
                "@;", //16
                "@;", //17
                "@;", //18
                "@;", //19
                "@;", //20
                "@;", //21
                "@"};//22  new String[22];
            orig_name = new File(path).getName().replace(".csu", "");
            orig_name = orig_name.replace(".csv", "");
            int index[] = new int[6], index_count = 1;

            for (int i = 0; i < orig_name.length(); i++) {
                if (orig_name.charAt(i) == ';') {
                    index[index_count] = i;
                    index_count++;
                }
            }
            index_count = 0;
            filename[0] = orig_name.substring(index[index_count], index[index_count + 1]) + ";";
            index_count++;
            filename[1] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //2 מספר הלקוח הרלבנטי במערכת הממוחשבת ומימינו הסימן
            index_count++;
            filename[2] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //3 התאריך בו צולמה התמונה בפורמט DD.MM.YYYY ומימינו הסימן
            index_count++;
            filename[3] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //4 הזמן בו צולמה התמונה בפורמט HHMMSS ומימינו הסימן
            index_count++;
            filename[4] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //5 הנ.צ. (GPS) של המיקום בו צולמה התמונה ומימינו הסימן
            index_count++;
            filename[5] = orig_name.substring(index[index_count] + 1, orig_name.length()) + ";"; //6 קידומת כלשהי שתציין את סוג המכשיר ממנו נשלחה התמונה (למשל, GX2).
            //filename[6] =;//7 בית העסק שהפיק את שובר הקופה כפי שמופיע במערכת הממוחשבת
            //filename[7] =;//8 מספר שובר הקופה
            //filename[8] =;//תאריך שובר הקופה
            //filename[9] =;//10 ההפקה של שובר הקופה,בפורמט HHMMSS 
            //filename[10] =;// 11.	מספר הקופה בה הודפס שובר הקופה
            filename[11] = vVerVar.get(0).toString() + ";";//12.	מספר הפריטים הכולל בשובר הקופה
            filename[12] = vVerVar.get(1).toString() + ";";//13
            filename[13] = vVerVar.get(2).toString() + ";";//14
            if (vVerVarWM.get(0).getSum() == 0) {
                filename[14] = "0" + ";";//15
            } else {
                filename[14] = vVerVarWM.get(0).toString() + ";";//15
            }
            filename[15] = vVerVarWM.get(1).toString() + ";";//16
            filename[16] = vVerVar.get(3).toString() + ";";//17
            //filename[17] = ;//18.	סה"כ סכום פטור ממע"מ.
            filename[18] = vVerVar.get(4).toString() + ";";//19
            filename[19] = vVerVar.get(5).toString() + ";";//20
            //filename[20] =f;//21.	מספר כרטיס מועדון לקוחות (אם קיים ואושרר באינדקס הלקוחות).
            //filename[21] =f;//22.	ארבע ספרות אחרונות של כרטיס האשראי (אם אותרו ו/או אושררו מול אינדקס הלקוחות).

            String f = "";
            for (int i = 0; i < filename.length; i++) {
                f = f + filename[i];

            }
            //create file
            //new File("output").mkdir();
            File dir = new File(Path.getOutputFolder());
            try {
                dir.mkdir();
            }catch(Exception exception){
                exception.getMessage();
            }

            File data = new File(dir.getPath()+"\\"+  f + ".csv");
            try{
                data.createNewFile();

            }catch(Exception exception){
                exception.getMessage();
            }
            //add data to file ///////////////////////////////////////////////////////// in the output folder
            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(data), "Unicode"));

            try {
                int i = 1;
                String header = "Catalog Number,Item description,Item number,Price per unit,Quantity,Item price without discount,Price discount,Item price,Taxes price of item,Left,Top,Right,Bottom";
                out.write(header + "\r\n");
                for (BNode b : bnv) {
                    String k = "";

                    if (b.pn != null) {                                             // 1
                        k = k + b.pn.get_result_str().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    if (b.descr != null && b.descr.size() > 0) {                    //2
                        k = k + b.get_description().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    k = k + (i++) + ",";                                            //3

                    if (b.one_price != null) {                                      //4
                        k = k + b.one_price.get_result_str().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    if (b.qty != null) {                                            //5
                        k = k + b.qty.get_result_str().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    if (b.fullprice != null) {                                      //6
                        k = k + b.fullprice.get_result_str().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    if (b.anaha != null) {                                          //7
                        k = k + b.anaha.replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }
                    /*
                     if (b.price_z != null) {                                        //
                     k = k + b.price_z.get_result_str().replace(",", ".") + ",";
                     } else {
                     k = k + "@,";
                     }
                     */
                    if (b.price != null) {                                          //8
                        k = k + b.price.get_result_str().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    if (b.mam != null) {                                            //9
                        k = k + b.mam.get_result_str().replace(",", ".") + ",";
                    } else {
                        k = k + "@,";
                    }

                    if (b.price != null) {
                        k = k + b.price.get_left() + ",";
                    } else {
                        k = k + "@,";
                    }
                    if (b.price != null) {
                        k = k + b.price.get_top() + ",";
                    } else {
                        k = k + "@,";
                    }
                    if (b.price != null) {
                        k = k + b.price.get_right() + ",";
                    } else {
                        k = k + "@,";
                    }
                    if (b.price != null) {
                        k = k + b.price.get_bottom();
                    } else {
                        k = k + "@";
                    }

                    out.write(k + "\r\n");
                }

            } finally {
                out.close();
                write(orig_name, "CSV.SaveData", "", "output successfully issued", true);

            }

            /*
             //This log checking the sumallNoMamv, sumallMamv, sumallWithMamv
             inon_log();
             */
        } catch (Exception r) {
            write(orig_name, "CSV.SaveData", "can't save data \"saveData\"", "", true);
        }
        return true;
    }

}
