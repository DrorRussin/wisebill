/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import Constructors.InfoReader;
import Constructors.ObData;
import Pathes.Path;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.imgcodecs.Imgcodecs;
import javax.swing.JOptionPane;

/**
 *
 * @author YAEL-MOBILE
 */
public class CropImage {

    private boolean error = false;
    private MatchesToItems matchesToItems = null;
    //private String OPENCV_DIR = System.getProperty("user.dir") + "\\opencv";
    //private String OPENCV_DIR = "C:\\HyperOCR\\opencv";
    private InfoReader infoReader = null;
    private Vector<Mat> allImages = new Vector<Mat>();
    private String orig_name = "";

    public CropImage(MatchesToItems MatchesToItems, String path, InfoReader infoReader, String DirectoryINI) throws IOException {
        this.matchesToItems = MatchesToItems;
        this.infoReader = infoReader;
        this.orig_name = new File(path).getName().replace(".csu", "");
        this.orig_name = orig_name.replace(".csv", "");
        try {
            //upload OPEN-CV
            System.setProperty("java.library.path", Path.getOpencvFolder() + "\\build\\java\\x64");
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
            System.loadLibrary("opencv_java310");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "\"open-cv\" is not exists",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);
            error = true;
        }

        //load path of images
        String pathImages = loadPath(DirectoryINI);

        //upload IMG
        File imagesFile = new File(pathImages);
        Vector<File> ImagesVector = new Vector<File>();
        if (imagesFile.exists()) {
            File[] listOfImages = imagesFile.listFiles();
            for (int i = 0; i < listOfImages.length; i++) {
                if (listOfImages[i].getName().contains("CURRENT_PAGES")) {
                    ImagesVector.add(listOfImages[i]);
                }
            }
        }

        //sort images by name (by number page)
        Collections.sort(ImagesVector, new Comparator() {
            @Override
            public int compare(Object a, Object b) {
                return (((File) a).getName()).compareTo(((File) b).getName());
            }
        });

        for (File file : ImagesVector) {
            allImages.add(Imgcodecs.imread(file.getAbsolutePath()));
        }
    }

    public void crop() throws IOException {
        if (!error && allImages.size() > 0) {
            String path = Path.getAI() + "\\" + infoReader.storeID11;
            if (findAndMakeDir(path)) {
                cropPNs(path + "\\PN");
                cropDescriptions(path + "\\DESCRIPTION");
            }
        }
    }

    private void cropPNs(String pathToStore) {
        String basePath = pathToStore;
        if (findAndMakeDir(pathToStore)) {
            File file = new File(pathToStore);
            File[] listDirectories = file.listFiles();
            for (MatchesToOneItem item : matchesToItems.MatchesToItems) {
                if (item.block.pn != null || (item.block.listPn != null && item.block.listPn.size() > 0)) {
                    // if it`s not A4
                    if (item.block.pn != null && item.block.pn.get_isVerifived()) {
                        //found directory name
                        int indexOfDirectoy = -1;
                        String nameDirectory = "";
                        //if found matches for item
                        if (item.matches != null && item.matches.size() > 0) {
                            String pnStrFromMatches = item.getPnStrFromMatches();
                            String[] pnsArrayFromMatches = pnStrFromMatches.split(" ");
                            String pnStrWeFound = item.block.pn.get_result_str();
                            for (String pn : pnsArrayFromMatches) {
                                if (LFun.checkChanges(pn, pnStrWeFound)) {
                                    nameDirectory = pn;
                                }
                            }

                        } //if it`s verify, but not matches have found, so we will take the string we found because it`s digital file
                        else {
                            nameDirectory = item.block.pn.get_result_str();
                        }

                        if (!nameDirectory.equals("")) {
                            //find index of directory if already exists
                            for (int i = 0; i < listDirectories.length; i++) {
                                if (nameDirectory.trim().equals(listDirectories[i].getName().trim())) {
                                    indexOfDirectoy = i;
                                }
                            }

                            //create path to store
                            if (indexOfDirectoy != -1) {
                                pathToStore += "\\" + listDirectories[indexOfDirectoy].getName();
                            } else {
                                pathToStore += "\\" + LFun.removeCharCantBeSaved(nameDirectory);
                            }

                            if (findAndMakeDir(pathToStore)) {
                                //crop pn and save
                                cropObData(item.block.pn.get_left(), item.block.pn.get_right(), item.block.pn.get_top(),
                                        item.block.pn.get_bottom(), item.block.pn.get_pfile_num(), pathToStore);
                            }
                        }

                    } //if it's A4
                    else if (item.block.listPn != null && item.block.listPn.size() > 0) {
                        for (int j = 0; j < item.block.listPn.size(); j++) {
                            pathToStore = basePath;
                            //found directory name
                            int indexOfDirectoy = -1;
                            String nameDirectory = "";
                            if (item.block.listPn.elementAt(j).get_isVerifived()) {
                                // find directory name
                                if (item.matches != null && item.matches.size() > 0) {
                                    String pnStrFromMatches = item.getPnStrFromMatches();
                                    String[] pnsArrayFromMatches = pnStrFromMatches.split(" ");
                                    String pnStrWeFound = item.block.listPn.elementAt(j).get_result_str();
                                    for (String pn : pnsArrayFromMatches) {
                                        if (LFun.checkChanges(pn, pnStrWeFound)) {
                                            nameDirectory = pn;
                                            break;
                                        }
                                    }
                                } //if it`s verify, but not matches have found, so we will take the string we found because it`s digital file
                                else {
                                    nameDirectory = item.block.listPn.elementAt(j).get_result_str();
                                }

                                if (!nameDirectory.equals("")) {
                                    //find index of directory if already exists
                                    for (int i = 0; i < listDirectories.length; i++) {
                                        if (nameDirectory.trim().equals(listDirectories[i].getName().trim())) {
                                            indexOfDirectoy = i;
                                        }
                                    }

                                    //create path to store
                                    if (indexOfDirectoy != -1) {
                                        pathToStore += "\\" + listDirectories[indexOfDirectoy].getName();
                                    } else {
                                        pathToStore += "\\" + LFun.removeCharCantBeSaved(nameDirectory);
                                    }

                                    if (findAndMakeDir(pathToStore)) {
                                        //crop pn and save
                                        cropObData(item.block.listPn.elementAt(j).get_left(), item.block.listPn.elementAt(j).get_right(),
                                                item.block.listPn.elementAt(j).get_top(), item.block.listPn.elementAt(j).get_bottom(),
                                                item.block.listPn.elementAt(j).get_pfile_num(), pathToStore);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void cropDescriptions(String pathToStore) {
        String basePath = pathToStore;
        if (findAndMakeDir(pathToStore)) {
            File file = new File(pathToStore);
            File[] listDirectories = file.listFiles();
            for (MatchesToOneItem item : matchesToItems.MatchesToItems) {
                pathToStore = basePath;
                if (item.block.descr != null && item.block.descr.size() > 0) {
                    if (item.block.descr.get(0).get_isVerifived()) {
                        //found directory name
                        String nameDirectory = "";
                        int indexOfDirectoy = -1;
                        if (item.matches != null && item.matches.size() > 0) {
                            nameDirectory = item.getDescriptionStrFromMatches();
                        } //if it`s verify, but not matches have found, so we will take the string we found because it`s digital file
                        else {
                            nameDirectory = item.block.get_description();
                        }

                        if (!nameDirectory.equals("")) {
                            //find index of directory if already exists
                            for (int i = 0; i < listDirectories.length; i++) {
                                if (listDirectories[i].getName().trim().equals(nameDirectory.trim())) {
                                    indexOfDirectoy = i;
                                }
                            }

                            //create path to store
                            String toChange = "";
                            if (indexOfDirectoy != -1) {
                                toChange = listDirectories[indexOfDirectoy].getName();
                                File f = new File(listDirectories[indexOfDirectoy].getPath().replace(listDirectories[indexOfDirectoy].getName(), "temp"));
                                listDirectories[indexOfDirectoy].renameTo(f);
                                pathToStore += "\\temp";
                            } else {
                                toChange = LFun.removeCharCantBeSaved(nameDirectory);
                                pathToStore += "\\temp";
                            }

                            if (findAndMakeDir(pathToStore)) {
                                try {
                                    //crop pn and save
                                    cropObData(item.block.get_XLeftDescription(), item.block.get_XRightDescription(),
                                            item.block.get_XTopDescription(), item.block.get_XBottomDescription(),
                                            item.block.descr.elementAt(0).get_pfile_num(), pathToStore);
                                    File FileWithNameToChange = new File(pathToStore.replace("temp", toChange));
                                    File fileChanged = new File(pathToStore);
                                    fileChanged.renameTo(FileWithNameToChange);
                                } catch (Exception e) {
                                    System.out.println("can`t crop " + item.block.get_description());
                                    File f = new File(pathToStore);
                                    deleteFile(f);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void deleteFile(File f) {
        if (f.exists()) {
            File[] files = f.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    if (!aFile.getName().equals("wisecopy.lck")) {
                        aFile.delete();
                    }
                }
            }
            f.delete();
        }

    }

    private boolean findAndMakeDir(String dirToCreate) {
        boolean isCraete = false;
        File file = new File(dirToCreate);
        if (!file.exists()) {
            try {
                isCraete = file.mkdirs();
                //isCraete = true;
            } catch (Exception e) {
                System.err.println("can't make " + dirToCreate + " directory");
                isCraete = false;
            }
        } else {
            isCraete = true;
        }

        return isCraete;
    }

    private void cropObData(int left, int right, int top, int bottom, int page, String pathToStore) {
        try {
            //crop
            int start_x = left, start_y = top,
                    end_x = right, end_y = bottom;

            Mat crop = new Mat(allImages.get(page - 1), new Range(start_y, end_y), new Range(start_x, end_x));

            //Create name of crop
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy~HHmmss");
            LocalDateTime now = LocalDateTime.now();
            String coordinates = start_x + "_" + start_y + "_" + end_x + "_" + end_y;
            String nameFile = orig_name + ";" + dtf.format(now) + ";" + page + ";" + coordinates + ";;;";

            //Save crop Of Image
            Imgcodecs.imwrite(pathToStore + "\\" + nameFile + ".jpg", crop);
        } catch (Exception e) {
            System.out.println("can`t crop " + pathToStore);
        }
    }

    private String loadPath(String DirectoryINI) {
        String Path = "";

        File INIFile = new File(DirectoryINI + "\\" + "Wise_VERIFY_INV_WISE_BILL.ini");
        if (INIFile.exists()) {
            try {
                FileInputStream fr = new FileInputStream(INIFile);
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new InputStreamReader(fr));
                } catch (Exception e) {
                }
                String s = "";
                while ((s = br.readLine()) != null) {
                    if (s.contains("CURRENT_PAGES")) {
                        Path = s.substring(s.indexOf("=") + 1, s.length()).replace("\\CURRENT_PAGES.tif", "");
                        break;
                    }
                    // System.out.println(s);
                }
            } catch (Exception r) {
            }
        }
        return Path;
    }
}
