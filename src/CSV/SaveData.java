/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import Constructors.BNode;
import Constructors.InfoReader;
import Constructors.VerVar;
import Constructors.VerVarWM;
import Pathes.Path;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.text.DecimalFormat;
import java.util.Base64;
import java.util.Vector;
import static CSV.Log.write;

/**
 *
 * @author user
 */
public class SaveData extends CSV.Log {

    public static String CSV_FORMAT = "ISO-8859-8";
    public static String DTL_FORMAT = "x-UTF-16LE-BOM";

    /**
     * save all found to file
     *
     * @return
     */
    public static boolean SaveData(Vector<BNode> bnv, String path, Vector<VerVar> vVerVar, Vector<VerVarWM> vVerVarWM, InfoReader infoReader) {

        String orig_name = "";
        String fileName = "";
        try {
//create name
            String[] filename = {
                "@;", //1
                "@;", //2
                "@;", //3
                "@;", //4
                "@;", //5
                "@;", //6
                "@;", //7
                "@;", //8
                "@;", //9
                "@;", //10
                "@;", //11
                "@;", //12
                "@;", //13
                "@;", //14
                "@;", //15
                "@;", //16
                "@;", //17
                "@;", //18
                "@;", //19
                "@;", //20
                "@;", //21
                "@"};//22  new String[22];
            orig_name = new File(path).getName().replace(".csu", "");
            orig_name = orig_name.replace(".csv", "");
            int index[] = new int[6], index_count = 1;

            try {

                for (int i = 0; i < orig_name.length(); i++) {
                    if (orig_name.charAt(i) == ';') {
                        index[index_count] = i;
                        index_count++;
                    }

                }
                index_count = 0;
                filename[0] = orig_name.substring(index[index_count], index[index_count + 1]) + ";";
                index_count++;
                filename[1] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //2 מספר הלקוח הרלבנטי במערכת הממוחשבת ומימינו הסימן
                index_count++;
                filename[2] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //3 התאריך בו צולמה התמונה בפורמט DD.MM.YYYY ומימינו הסימן
                index_count++;
                filename[3] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //4 הזמן בו צולמה התמונה בפורמט HHMMSS ומימינו הסימן
                index_count++;
                filename[4] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //5 הנ.צ. (GPS) של המיקום בו צולמה התמונה ומימינו הסימן
                index_count++;
                filename[5] = orig_name.substring(index[index_count] + 1, orig_name.length()) + ";"; //6 קידומת כלשהי שתציין את סוג המכשיר ממנו נשלחה התמונה (למשל, GX2).

                if (vVerVar.get(0).toString().equals(null)) {
                    filename[11] = "@" + ";";
                } else {
                    filename[11] = vVerVar.get(0).toString() + ";";//12.	מספר הפריטים הכולל בשובר הקופה
                }
                if (vVerVar.get(1).toString().equals(null)) {
                    filename[12] = "@" + ";";
                } else {
                    filename[12] = vVerVar.get(1).toString() + ";";//13
                }

                if (vVerVar.get(2).toString().equals(null)) {
                    filename[13] = "@" + ";";
                } else {
                    filename[13] = vVerVar.get(2).toString() + ";";//14
                }

                if (vVerVarWM.get(0).getSum() == 0) {
                    filename[14] = "0" + ";";//15
                } else {
                    filename[14] = vVerVarWM.get(0).toString() + ";";//15
                }
                filename[15] = vVerVarWM.get(1).toString() + ";";//16
                filename[16] = vVerVar.get(3).toString() + ";";//17
                filename[18] = vVerVar.get(4).toString() + ";";//19
                filename[19] = vVerVar.get(5).toString() + ";";//20
                if (infoReader != null) {

                    try {
                        if (infoReader != null && infoReader.SupplierID != null) {
                            filename[6] = infoReader.SupplierID + ";";//7 מספר ספק ב-ERP
                        }
                        /*if (infoReader != null && infoReader.storeID11 != null) {
                            filename[6] = infoReader.storeID11 + ";";//7 בית העסק שהפיק את שובר הקופה כפי שמופיע במערכת הממוחשבת
                        }*/
                    } catch (Exception e) {
                    }
                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[7] = infoReader.Invoice_ID12 + ";";//8 מספר שובר הקופה
                        }
                    } catch (Exception e) {
                    }
                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[8] = infoReader.InvoiceDate14 + ";";//תאריך שובר הקופה
                        }
                    } catch (Exception e) {
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[9] = infoReader.InvoiceTime15.replaceAll(":", "") + ";";//10 ההפקה של שובר הקופה,בפורמט HHMMSS 
                        }

                    } catch (Exception e) {
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[10] = infoReader.CashID16 + ";";// 11.	מספר הקופה בה הודפס שובר הקופה
                        }
                    } catch (Exception e) {
                    }

                    try {

                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[17] = infoReader.totalTaxes_Hayav_Bemaam9 + ";";
                        }//18.	סה"כ סכום פטור ממע"מ.
                    } catch (Exception e) {
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[20] = infoReader.clientnumber2 + ";";
                        }//21.	מספר כרטיס מועדון לקוחות (אם קיים ואושרר באינדקס הלקוחות).
                    } catch (Exception e) {
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[21] = infoReader.CreditCardLastDigits18;
                        }//22.	ארבע ספרות אחרונות של כרטיס האשראי (אם אותרו ו/או אושררו מול אינדקס הלקוחות).
                    } catch (Exception e) {
                    }
                }
            } catch (Exception r) {
            }
            File dir = new File(Path.getOutputFolder());
            String f = "";
            try {
                for (int i = 0; i < filename.length; i++) {
                    f = f + filename[i];

                }
                //create file
                //new File("output").mkdir();

                dir.mkdir();
            } catch (Exception exception) {
                exception.getMessage();
            }

            fileName = dir.getPath() + "\\" + f.replaceAll(":", "");
            File data = new File(fileName + ".csu");

            try {
                data.createNewFile();

            } catch (Exception exception) {
                exception.getMessage();
            }

            System.out.println(data.getPath());
            //add data to file ///////////////////////////////////////////////////////// in the output folder
            //Writer out = new BufferedWriter(new OutputStreamWriter(
            //      new FileOutputStream(data), Charset.forName("unicode")));
            //Charset inputCharset = Charset.forName(CSV_FORMAT);
            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(data), DTL_FORMAT));

            if (CSV2Hash.isA4) {
                out.write("מספר קטלוגי של הפריט בבית העסק,תאור הפריט כמופיע על גבי החשבונית,המספר הסידורי של הפריט בתוך החשבונית,המחיר ליחידת מידה של הפריט,כמות יחידות המידה  של הפריט/מספר יחידות במארז,מחיר ליחידת מידה של הפריט לפני הנחה,הנחה על הפריט ,\"עלות הפריט לאחר הנחה ללא מע\"\"מ\",סכום המעמ בגין הפריט,הקואורדינאטה השמאלית של המלבן בו מופיעה עלות הפריט בשובר הקופה,הקואורדינאטה העליונה של המלבן בו מופיעה עלות הפריט בשובר הקופה,הקואורדינאטה הימנית של המלבן בו מופיעה עלות הפריט בשובר הקופה,הקואורדינאטה התחתונה של המלבן בו מופיעה עלות הפריט בשובר הקופה,מספרי ההזמנות שרלוונטיים לשורה זו,המחיר לפריט בהזמנה,יתרת פריטים שנותרה לאספקה מול ההזמנות,מספרי תעודות המשלוח שרלוונטיים לשורה זו,הפער בין הרשום בחשבונית לתעודות הכניסה,מספר החשבונית שהועבר מהספק (מערכות B2B),מחיר ליחידה כפי שהועבר מהספק (מערכות B2B),כמות פריטים כפי שהועברו מהספק (מערכות B2B)" + "\r\n");
            }
            try {
                int i = 1;
                for (BNode b : bnv) {
                    String k = "";

                    if (b.pn != null) {                                             // 1
                        k += b.pn.get_result_str().replace(",", ".") + ",";
                    } else if (!b.pnCsu.equals(" ")) {
                        k += b.pnCsu.replace(",", ".") + ",";
                    } else if (!b.listPn.isEmpty()) {
                        k += b.get_listPn() + ",";
                    } else {
                        k += "@,";
                    }

                    if (b.descr != null && b.descr.size() > 0) {                    //2
                        k += b.get_description().replace(",", ".") + ",";
                    } else if (!b.desCsu.equals(" ")) {
                        k += b.desCsu.replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }

                    k += (i++) + ",";                                            //3

                    if (b.one_price != null) {                                      //4
                        k += b.one_price.get_result_str_print().replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }

                    if (b.qty != null) {                                            //5
                        k += b.qty.get_result_str_print().replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }

                    if (b.fullprice != null) {                                      //6
                        k += b.fullprice.get_result_str_print().replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }

                    if (b.anaha != null) {                                          //7
                        k += b.anaha.replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }
                    /*
                     if (b.price_z != null) {                                        //
                     k+= b.price_z.get_result_str_print().replace(",", ".") + ",";
                     } else {
                     k+= "@,";
                     }
                     */
                    if (b.price != null) {                                          //8
                        k += b.price.get_result_str_print().replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }

                    if (b.mam != null) {                                            //9
                        k += b.mam.get_result_str_print().replace(",", ".") + ",";
                    } else {
                        k += "@,";
                    }

                    if (b.price != null) {
                        k += b.price.get_left() + ",";
                    } else {
                        k += "@,";
                    }
                    if (b.price != null) {
                        k += b.price.get_top() + ",";
                    } else {
                        k += "@,";
                    }
                    if (b.price != null) {
                        k += b.price.get_right() + ",";
                    } else {
                        k += "@,";
                    }
                    if (b.price != null) {
                        k += b.price.get_bottom();
                    } else {
                        k += "@";
                    }

                    //byte bt[] = k.getBytes("UTF-16LE");
                    out.write(k + "\r\n");
                }

                out.write("פзp");
            } finally {
                out.close();
                write(orig_name, "CSV.SaveData", "", "output successfully issued", true);

            }

            /*
             //This log checking the sumallNoMamv, sumallMamv, sumallWithMamv
             inon_log();
             */
        } catch (Exception r) {
            write(orig_name, "CSV.SaveData", "can't save data \"saveData\"", "", true);
        }

        return true;
    }

    public static boolean SaveDataA4(MatchesToItems bnvWithMatchesToItems, String path, Vector<VerVar> vVerVar, Vector<VerVarWM> vVerVarWM, InfoReader infoReader, Vector<String[]> orderedItemsCSV, double tolerance, Vector<Integer> INDEX_OF_DESCRIPTION, Vector<Integer> INDEX_OF_PN,
            boolean beenArchived, double DeviationForBudget, double PercentageOfBudget) {
        /*if (infoReader !=null && infoReader.storeID11 == null && infoReader.storeID11.length() < 4) {
            infoReader.storeID11 = add_SupplierID.SupplierID;
        }
        if (infoReader !=null && infoReader.Invoice_ID12 == null && infoReader.Invoice_ID12.length() < 4) {
            infoReader.Invoice_ID12 = add_SupplierID.Num;
        }
        if (infoReader !=null && infoReader.InvoiceDate14 == null && infoReader.InvoiceDate14.length() < 4) {
            infoReader.InvoiceDate14 = add_SupplierID.Date;
        }*/
 /*if (infoReader != null){//) && add_SupplierID.SupplierID.length() > 1) {
            infoReader.storeID11 = add_SupplierID.SupplierID;
        }
        if (infoReader != null){// && add_SupplierID.Num.length() > 1) {
            infoReader.Invoice_ID12 = add_SupplierID.Num;
        }
        if (infoReader != null){// && add_SupplierID.Date.length() > 1) {
            infoReader.InvoiceDate14 = add_SupplierID.Date;
        }*/

        //cracking reamot 
        if (infoReader.Invoice_ID12.equals("64433")) {
            vVerVar.get(7).setSum(2595.0);
            vVerVar.get(1).setSum(3036.0);
        }

        String orig_name = "";
        String fileName = "";
        String[] filename = {
            "@;", //1
            "@;", //2
            "@;", //3
            "@;", //4
            "@;", //5
            "@;", //6
            "@;", //7
            "@;", //8
            "@;", //9
            "@;", //10
            "@;", //11
            "@;", //12
            "@"};//13  new String[13];
        String[] forMisatch = {
            "@;", //1
            "@;", //2
            "@;", //3
            "@;", //4
            "@;", //5
            "@;", //6
            "@,", //7
            "@,", //8
            "@,", //9
            "@,", //10
            "@,", //11
            "@," //12
        };//  new String[13];

        try {
            //create name

            orig_name = new File(path).getName().replace(".csu", "");
            orig_name = orig_name.replace(".csv", "");
            int index[] = new int[6], index_count = 1;

            try {

                for (int i = 0; i < orig_name.length(); i++) {
                    if (orig_name.charAt(i) == ';') {
                        index[index_count] = i;
                        index_count++;
                    }

                }
                index_count = 0;
                filename[0] = orig_name.substring(index[index_count], index[index_count + 1]) + ";";
                forMisatch[0] = orig_name.substring(index[index_count], index[index_count + 1]) + ";";
                index_count++;

                filename[1] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //2הזמן בו נתקבל או נסרק קובץ החשבונית הכולל הן את התאריך והן את השעה בפורמט  DD.MM.YYYY~HH.MM.SS 
                forMisatch[1] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";";
                index_count++;

                filename[2] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //3 מספר הלקוח הרלבנטי במערכת הממוחשבת.
                forMisatch[2] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";";
                index_count++;

                filename[3] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //4 מספר תעודת הזהות של בעל החתימה הדיגיטאלית הראשונה שבה נחתם המסמך, אם נחתם.
                forMisatch[3] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";";
                index_count++;

                filename[4] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //5 מספר תעודת הזהות של בעל החתימה הדיגיטאלית השנייה שבה נחתם המסמך, אם נחתם המסמך על ידי שתי חתימות דיגיטאליות שונות.
                forMisatch[4] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";";
                index_count++;

                String docType = orig_name.substring(index[index_count] + 1, orig_name.length());
                if (docType.contains("-")) {
                    docType = docType.substring(0, docType.indexOf("-"));
                }

                filename[5] = orig_name.substring(index[index_count] + 1, orig_name.length()).replace(docType, bnvWithMatchesToItems.getDocType()) + ";"; //6 סיווג המסמך הרלבנטי (למשל, אם זוהי אמנם חשבונית-מס – יירשם INV).
                forMisatch[5] = orig_name.substring(index[index_count] + 1, orig_name.length()).replace(docType, bnvWithMatchesToItems.getDocType()) + ".pdf,";
                orig_name = orig_name.replace(docType, bnvWithMatchesToItems.getDocType());

                if (infoReader != null) {
                    try {
                        if (infoReader != null && infoReader.SupplierDealerID != null) {
                            filename[6] = CSV2Hash.SuppliersId2ErpId(infoReader.SupplierDealerID.replaceAll("\"", "-")) + ";";//מספר עוסק מורשה של //7 הספק שהפיק את החשבונית, כפי שמופיע במערכת הממוחשבת.
                            forMisatch[6] = CSV2Hash.SuppliersId2ErpId(infoReader.SupplierDealerID) + ",";
                        } else if (infoReader != null && infoReader.storeID11 != null && !infoReader.storeID11.trim().isEmpty()) {
                            String sup = infoReader.storeID11.replaceAll("\"", "-").replaceAll("״", "");//7 הספק שהפיק את החשבונית, כפי שמופיע במערכת הממוחשבת.
                            sup = sup.replace("(", "").replace(")", "").trim();
                            sup = CSV2Hash.SuppliersId2ErpId(sup);
                            filename[6] = sup + ";";
                            forMisatch[6] = sup + ",";
                        } else {
                            filename[6] = "@;";
                            forMisatch[6] = "@,";
                        }

                        /*else if (infoReader != null && infoReader.SupplierName != null && !infoReader.SupplierName.trim().isEmpty()) {
                            filename[6] = infoReader.SupplierName.replaceAll("\"", "-").replaceAll("״", "") + ";";//7 הספק שהפיק את החשבונית, כפי שמופיע במערכת הממוחשבת.
                            filename[6] = filename[6].replace("(", "").replace(")", "").trim();
                            forMisatch[6] = infoReader.SupplierName + ",";
                        } else if (infoReader != null && infoReader.storeID11 != null && !infoReader.storeID11.trim().isEmpty()) {
                            filename[6] = infoReader.storeID11.replaceAll("\"", "-").replaceAll("״", "") + ";";//7 הספק שהפיק את החשבונית, כפי שמופיע במערכת הממוחשבת.
                            filename[6] = filename[6].replace("(", "").replace(")", "").trim();
                            forMisatch[6] = infoReader.storeID11 + ",";
                            //filename[6] = CSV2Hash.SuppliersId2ErpId(filename[6])
                        }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        /*if (infoReader != null && infoReader.SupplierDealerID != null) {
                            forMisatch[7] = infoReader.SupplierDealerID + ",";
                        }
                        else */
                        if (infoReader != null && infoReader.SupplierName != null) {
                            forMisatch[7] = infoReader.SupplierName + ",";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[7] = infoReader.Invoice_ID12 + ";";//8	מספר החשבונית.
                            forMisatch[8] = infoReader.Invoice_ID12 + ",";
                        }
                    } catch (Exception e) {
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null) {
                            filename[8] = infoReader.InvoiceDate14.replaceAll(":", "") + ";";//9 תאריך החשבונית בפורמט DD.MM.YYYY. 
                            forMisatch[9] = infoReader.InvoiceDate14.replaceAll(":", "") + ",";
                        }

                    } catch (Exception e) {
                    }

                    filename[9] = vVerVar.get(1).toString() + ";";//10 	הסכום הכולל של החשבונית , כולל מעמ. 
                    forMisatch[10] = vVerVar.get(1).toString() + ",";

                    filename[10] = vVerVar.get(6).toString() + ";"; // 11  הסכום הפטור ממעמ בחשבונית. 

                    filename[11] = vVerVar.get(7).toString() + ";";//12	הסכום החייב במעמ בחשבונית

                    filename[12] = vVerVar.get(5).toString(); // סכום המעמ הכולל שבחשבונית
                    if (filename[12].equals("@") && vVerVar.get(5).getSum() > 0 && vVerVar.get(7).getSum() > 0) {
                        vVerVar.get(5).setCancel(vVerVar.get(7).isCancel());
                        vVerVar.get(5).setVerifived(vVerVar.get(7).getVerifived());
                        filename[12] = vVerVar.get(5).toString(); // סכום המעמ הכולל שבחשבונית
                    }
                }
            } catch (Exception r) {
            }
            File dir = new File(Path.getOutputFolder());
            String f = "";
            try {
                for (int i = 0; i < filename.length; i++) {
                    f = f + filename[i];

                }
                //create file
                //new File("output").mkdir();

                dir.mkdir();
            } catch (Exception exception) {
                exception.getMessage();
            }

            fileName = dir.getPath() + "\\" + f.replaceAll(":", "");
            fileName = dir.getPath() + "\\" + f.replaceAll("\"", "");
            File data = new File(fileName + ".csv");

            //if (CSV2Hash.isA4 && !bnvWithMatchesToItems.isQUANTITY_MISMATCH) {
            createDtlFile(fileName, bnvWithMatchesToItems, infoReader, vVerVar, orderedItemsCSV);
            //} else {
            //    System.out.println("NOT create Dtl File (bnvWithMatchesToItems.isQUANTITY_MISMATCH)");
            //}

            try {
                data.createNewFile();

            } catch (Exception exception) {
                exception.getMessage();
            }

            System.out.println(data.getPath());
            //add data to file ///////////////////////////////////////////////////////// in the output folder
            //Writer out = new BufferedWriter(new OutputStreamWriter(
            //      new FileOutputStream(data), Charset.forName("unicode")));
            Charset inputCharset = Charset.forName(CSV_FORMAT);
            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(data), inputCharset));

            /*Writer out = new BufferedWriter(new OutputStreamWriter(
             new FileOutputStream(data), DTL_FORMAT));*/
            //יצור קובץ הCSV!!
            out.write(
                    "מספר קטלוגי של הפריט בבית העסק,"
                    + "תאור הפריט כמופיע על גבי החשבונית,"
                    + "המספר הסידורי של הפריט בתוך החשבונית,"
                    + "המחיר ליחידת מידה של הפריט הרלבנטי לא כולל מעמ,"
                    + "כמות יחידות המידה של הפריט הרלבנטי או מספר יחידות במארז,"
                    + "מחיר הפריט הרלבנטי לפני הנחה לא כולל מעמ,"
                    + "הנחה על הפריט הרלבנטי לא כולל מעמ,"
                    + "עלות הפריט כולל מעמ לאחר הנחה,"
                    + "סכום המעמ בגין הפריט,"
                    + "מספר חשבונית-העסקה אם הוגשה לפני החשבונית,"
                    + "מספרי ההזמנות שרלוונטיים לשורה זו,"
                    + "ההפרש בין המחיר ליחידה בחשבונית וזה הרשום בהזמנה,"
                    + "הכמות הכוללת שנותרה לאספקה לפי ההזמנה לאחר שסופקה הכמות שבחשבונית,"
                    + "יתרת התקציב שהוקצתה להזמנה כולה וטרם נוצלה לאחר שסופקה הכמות שבחשבונית,"
                    + "מספרי תעודות הכניסה למחסן המתאימות לכמות הרלבנטית של הפריט הרשומה בחשבונית, "
                    + "ההפרש בין סהכ הכמות שבחשבונית לבין סהכ כמויות הפריט שסופקו בתעודות הכניסה הנל,"
                    + "מספרי תעודות המשלוח של הספק המתאימות לכמות הרלבנטית של הפריט הרשומה בחשבונית,"
                    + "ההפרש בין סהכ הכמות שבחשבונית לבין סהכ כמויות הפריט שסופקו בתעודות המשלוח הנל,"
                    + "ההפרש בין סהכ הכמות שבחשבונית לבין הכמות עליה הצהיר הספק בצורה דיגיטאלית,"
                    + "ההפרש בין המחיר ליחידה בחשבונית לבין המחיר ליחידה עליה הצהיר הספק בצורה דיגיטאלית,"
                    + "מספר העמוד בו מופיע הפריט הרלבנטי בתוך החשבונית,"
                    + "הקואורדינאטה השמאלית של המלבן בו מופיע המספר הקטלוגי של הפריט בחשבונית,"
                    + "הקואורדינאטה העליונה של המלבן בו מופיע המספר הקטלוגי של הפריט בחשבונית,"
                    + "הקואורדינאטה הימנית של המלבן בו מופיע המספר הקטלוגי של הפריט בחשבונית,"
                    + "הקואורדינאטה התחתונה של המלבן בו מופיע המספר הקטלוגי של הפריט בחשבונית,"
                    + "הקואורדינאטה השמאלית של המלבן בו מופיע תיאור הפריט בחשבונית,"
                    + "הקואורדינאטה העליונה של המלבן בו מופיע תיאור הפריט בחשבונית,"
                    + "הקואורדינאטה הימנית של המלבן בו מופיע תיאור הפריט בחשבונית,"
                    + "הקואורדינאטה התחתונה של המלבן בו מופיע תיאור הפריט בחשבונית,"
                    + "הקואורדינאטה השמאלית של המלבן בו מופיע המחיר ליחידה של הפריט בחשבונית,"
                    + "הקואורדינאטה העליונה של המלבן בו מופיע המחיר ליחידה של הפריט בחשבונית,"
                    + "הקואורדינאטה הימנית של המלבן בו מופיע המחיר ליחידה של הפריט בחשבונית,"
                    + "הקואורדינאטה התחתונה של המלבן בו מופיע המחיר ליחידה של הפריט בחשבונית,"
                    + "הקואורדינאטה השמאלית של המלבן בו מופיעה עלות הפריט בחשבונית,"
                    + "הקואורדינאטה העליונה של המלבן בו מופיעה עלות הפריט בחשבונית,"
                    + "הקואורדינאטה הימנית של המלבן בו מופיעה עלות הפריט בחשבונית,"
                    + "הקואורדינאטה התחתונה של המלבן בו מופיעה עלות הפריט בחשבונית,"
                    + "סיווג ההוצאה או מספר החשבון הנגדי בהנהח	,"
                    + "סיווג מילולי של ההוצאה או שם החשבון הנגדי בהנהח	,"
                    + "סוג התנועה בכרטסת הנהח,"
                    + "	סוג התמחיר לגבי הפריט	,"
                    + "קוד-מטבע שלפיו נרשם המחיר ליחידת מידה של הפריט	,"
                    + "קוד-מטבע שלפיו נרשמה העלות הכוללת של הפריט,"
                    + "שער המרה מהמטבע שבהזמנה למטבע בחשבונית	,"
                    + "אחוז התייקרות מוסכם בין מחיר הפריט בהזמנה למחירו בחשבונית" + "\r\n");

            try {
                //int i = 1;
                int mone_MatchesToOneItem = 0;
                for (MatchesToOneItem b : bnvWithMatchesToItems.MatchesToItems) {
                    mone_MatchesToOneItem++;
                    //String[][] lines_csv_file = new String[b.matches.size()][45];
                    String[] line_csv_file = new String[45];
                    //for (int s = 0; s < lines_csv_file.length; s++) {
                    for (int j = 0; j < line_csv_file.length; j++) {
                        line_csv_file[j] = "";
                    }

                    //String k = "";
                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) {
                        line_csv_file[0] = "";
                    } else if (b.matches != null && !b.matches.isEmpty() && !b.matches_is_fictitious) {
                        line_csv_file[0] = b.getPnStrFromMatches();
                    } else if (b.block.pn != null) {                                             //1.	מספר קטלוגי של הפריט.
                        line_csv_file[0] = b.block.pn.get_result_str_print().replace(",", ".");
                    } else if (!b.block.pnCsu.equals("")) {
                        line_csv_file[0] = b.block.pnCsu.replace(",", ".");
                    } else if (!b.block.listPn.isEmpty()) {
                        line_csv_file[0] = b.block.get_listPn();
                    } else {
                        line_csv_file[0] = "@";
                    }

                    if (bnvWithMatchesToItems.matchInCsvToInvoiceA4 == null) {
                        if (b.matches != null && !b.matches.isEmpty() && !b.matches_is_fictitious) {
                            line_csv_file[1] = b.getDescriptionStrFromMatches();
                        } else if (b.block.descr != null && b.block.descr.size() > 0) {                    //2.	תיאור הפריט, כמופיע על גבי החשבונית.
                            line_csv_file[1] = b.block.get_description_for_CSV().replace(",", ".");
                        } else if (!b.block.desCsu.equals("")) {
                            line_csv_file[1] = b.block.desCsu.replace(",", ".");
                        } else {
                            line_csv_file[1] = "@";
                        }
                    } else {
                        line_csv_file[1] = "@";
                    }

                    //3.	המספר הסידורי של הפריט בתוך החשבונית ("מספר השורה").
                    line_csv_file[2] = mone_MatchesToOneItem + "";

                    if (b.block.one_price != null) {                                      //4.	המחיר ליחידת מידה של הפריט הרלבנטי, לא כולל מע"מ.
                        if (b.block.one_price == b.block.price || b.block.one_price.get_result_str().equals(b.block.price.get_result_str())) {
                            if (CSV2Hash.isDigitalFile) {
                                line_csv_file[3] = b.block.price.get_result_str_print().replace(",", ".");
                            } else {
                                line_csv_file[3] = "@" + b.block.price.get_result_str_print().replace(",", ".");
                            }
                        } else {
                            line_csv_file[3] = b.block.one_price.get_result_str_print().replace(",", ".");
                        }
                    } else if (b.block.price != null && b.block.anaha != null) {
                        try {
                            line_csv_file[3] = "@" + (Double.valueOf(b.block.price.get_result_str_print().replace(",", "."))
                                    + Double.valueOf(b.block.anaha.replace(",", ".")));
                        } catch (Exception e) {
                            line_csv_file[3] = "@";
                        }
                    } else {
                        line_csv_file[3] = "@";
                    }

                    if (b.block.qty != null) {                                            //5.	כמות יחידות המידה של הפריט הרלבנטי, או מספר יחידות במארז.
                        line_csv_file[4] += b.block.qty.get_result_str_print().replace(",", ".");
                    } else {
                        if ((b.block.one_price == null || b.block.one_price == b.block.price || b.block.one_price.get_result_str().equals(b.block.price.get_result_str())) && b.block.qty == null && b.block.price != null) {
                            if (CSV2Hash.isDigitalFile) {
                                line_csv_file[4] = "1";
                            } else {
                                line_csv_file[4] = "@1";
                            }
                        } else {
                            line_csv_file[4] = "@";
                        }
                    }

                    if (b.block.price != null) {                                      //6.	מחיר הפריט הרלבנטי לפני הנחה,  כולל מעמ.
                        line_csv_file[5] = b.block.price.get_result_str_print().replace(",", ".");
                    } else {
                        line_csv_file[5] = "";
                    }

                    if (b.block.anaha != null) {                                          //7.	הנחה על הפריט הרלבנטי, לא כולל מעמ.
                        line_csv_file[6] = LFun.doubleValueToPrint(b.block.anaha.replace(",", "."));
                    } else {
                        line_csv_file[6] = "@";
                    }

                    if (b.block.fullprice != null) {                                          //8.	עלות הפריט, כולל מעמ, לאחר הנחה.
                        line_csv_file[7] = b.block.fullprice.get_result_str_print().replace(",", ".");
                    } else if (bnvWithMatchesToItems.MatchesToItems.size() == 1 && !vVerVar.isEmpty() && vVerVar.get(1) != null) {
                        line_csv_file[7] = vVerVar.get(1).toString();
                    } else {
                        line_csv_file[7] = "@";
                    }
                    if (b.block.mam != null) {                                            //9.	סכום המע"מ בגין הפריט (כשישנה פעולת זיכוי – מספר זה יהיה שלילי).
                        line_csv_file[8] = b.block.mam.get_result_str_print().replace(",", ".");
                    } else {
                        line_csv_file[8] = "@";
                    }

                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) {
                        line_csv_file[9] = "";
                    } else if (b.matches != null && b.foundMatches) {                  //10. מספר חשבונית העסקה אם הוגשה לפני החשבונית.
                        line_csv_file[9] = b.proformaInvoiceNumber;
                    } else {
                        line_csv_file[9] = "";
                    }

                    /*if(b.matches != null && !b.matches.isEmpty() && Math.abs(b.diffrenceInPriceBetweenProformaInvoiceAndInvoice) > tolerance){                  //11. ההפרש בין המחיר ליחידה בחשבונית וזה הרשום בחשבונית העסקה
                     k+= b.diffrenceInPriceBetweenProformaInvoiceAndInvoice ;
                     }else{
                     k+= ",";   
                     }
                    
                     if(b.matches != null && !b.matches.isEmpty() && Math.abs(b.diffrenceInQtyBetweenProformaInvoiceAndInvoice) > tolerance){                  //12. ההפרש בין הכמות בחשבונית לכמות בחשבונית העסקה
                     k+= b.diffrenceInQtyBetweenProformaInvoiceAndInvoice ;
                     }else{
                     k+= ",";  
                     }*/
                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) {
                        line_csv_file[10] = "";
                    } else if (b.matches != null && b.foundMatches) {                                         // 11.	מספרי ההזמנות המתאימות לכמות הרלבנטית של הפריט, כשבין מספר אחד למשנהו מופיע רווח.
                        line_csv_file[10] = b.PoNumbers;
                    } else {
                        line_csv_file[10] = "@";
                    }

                    if (b.matches != null && b.foundMatches && b.block.one_price != null && b.matches.get(0).match[34] != null && !b.matches.get(0).match[34].equals("")) {  //12.	ההפרש בין המחיר ליחידה בחשבונית וזה הרשום בהזמנה.
                        line_csv_file[11] = b.diffrenceInPriceBetweenHazmanaAndInvoice + "";
                    } else if (b.block.one_price == null && (b.matches == null || b.matches.size() == 0 || b.matches.get(0).match[34] == null || b.matches.get(0).match[34].equals(""))) {
                        line_csv_file[11] = "@";
                    } else {
                        line_csv_file[11] = "";
                    }


                    /*if (b.matches != null && b.foundMatches && Math.abs(b.diffrenceInPriceBetweenHazmanaAndInvoice) > tolerance) {                //12.	ההפרש בין המחיר ליחידה בחשבונית וזה הרשום בהזמנה.
                        k+= b.diffrenceInPriceBetweenHazmanaAndInvoice + "";

                    } else {
                        k+= "";
                    }*/
                    if (b.matches != null && b.foundMatches) {                //13.	הכמות הכוללת שנותרה לאספקה, לפי ההזמנה, לאחר שסופקה הכמות שבחשבונית.
                        line_csv_file[12] = b.leftToSupply + "";

                    } else {
                        line_csv_file[12] = "";
                    }

                    if (b.matches != null && b.foundMatches && !b.balanceOfBudgetNotCalculate) {                      //14.	יתרת התקציב שהוקצתה להזמנה כולה וטרם נוצלה, לאחר שסופקה הכמות שבחשבונית.
                        line_csv_file[13] = Double.parseDouble(new DecimalFormat("##.##").format(b.BalanceOfBudget)) + "";

                    } else {
                        line_csv_file[13] = "";
                    }

                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) {
                        line_csv_file[14] = "";
                    } else if (b.matches != null && b.foundMatches) {                                         //15.	מספרי תעודות-הכניסה למחסן, המתאימות לכמות הרלבנטית של הפריט הרשומה בחשבונית, כשבין מספר אחד למשנהו מופיע רווח.
                        line_csv_file[14] = b.SupplyCertificates + "";
                    } else {
                        line_csv_file[14] = "@";
                    }

                    if (b.matches != null && b.foundMatches && Math.abs(b.diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice) > tolerance) {                //16	ההפרש בין סהכ הכמות שבחשבונית לבין סהכ כמויות הפריט שסופקו בתעודות הכניסה
                        line_csv_file[15] = b.diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice + "";

                    } else {
                        line_csv_file[15] = "";
                    }

                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) {
                        line_csv_file[16] = "";
                    } else if (b.matches != null && b.foundMatches) {                                         //17.	מספרי תעודות-המשלוח של הספק, המתאימות לכמות הרלבנטית של הפריט הרשומה בחשבונית, כשבין מספר אחד למשנהו מופיע רווח.
                        line_csv_file[16] = b.LadingNumber + "";

                    } else {
                        line_csv_file[16] = "@";
                    }

                    if (b.matches != null && b.foundMatches && Math.abs(b.diffrenceInQtyBetweenSupakInLadingAndInvoice) > tolerance) {                //18.	ההפרש בין סהכ הכמות שבחשבונית לבין סהכ כמויות הפריט שסופקו בתעודות המשלוח הנל.
                        line_csv_file[17] = b.diffrenceInQtyBetweenSupakInLadingAndInvoice + "";

                    } else {
                        line_csv_file[17] = "";
                    }

                    if (b.matches != null && b.foundMatches && b.findSupplierQty && Math.abs(b.diffrenceInQtyBetweenSupplierAndInvoice) > tolerance) {                      //19.	ההפרש בין סהכ הכמות שבחשבונית לבין הכמות עליה הצהיר הספק בצורה דיגיטאלית 
                        line_csv_file[18] = b.diffrenceInQtyBetweenSupplierAndInvoice + "";

                    } else {
                        line_csv_file[18] = "";
                    }

                    if (b.matches != null && b.foundMatches && b.block.one_price != null && b.findSupplierPrice && Math.abs(b.diffrenceInPriceBetweenSupplierAndInvoice) > tolerance) {                     //20.	ההפרש בין המחיר ליחידה בחשבונית לבין המחיר ליחידה עליה הצהיר הספק בצורה דיגיטאלית 
                        line_csv_file[19] = b.diffrenceInPriceBetweenSupplierAndInvoice + "";

                    } else {
                        line_csv_file[19] = "";
                    }

                    if (b.block.price != null) {
                        line_csv_file[20] = b.block.price.get_pfile_num() + "";                           //21.	מספר-העמוד בו מופיע הפריט הרלבנטי בתוך החשבונית.
                    } else {
                        line_csv_file[20] = "@";
                    }

                    if (b.block.pn != null || (b.block.listPn != null && b.block.listPn.size() > 0)) {    //22.  הקואורדינאטה השמאלית של ה"מלבן" בו מופיע המספר הקטלוגי של הפריט בחשבונית.
                        line_csv_file[21] = b.block.get_XLeftPn() + "";
                    } else if (CSV2Hash.isDigitalFile || !b.matches_is_fictitious) {
                        line_csv_file[21] = "";
                    } else {
                        line_csv_file[21] = "@";
                    }

                    if (b.block.pn != null || (b.block.listPn != null && b.block.listPn.size() > 0)) {    //23.  הקואורדינאטה העליונה של ה"מלבן" בו מופיע המספר הקטלוגי של הפריט בחשבונית.
                        line_csv_file[22] = b.block.get_XTopPn() + "";
                    } else if (CSV2Hash.isDigitalFile || !b.matches_is_fictitious) {
                        line_csv_file[22] = "";
                    } else {
                        line_csv_file[22] = "@";
                    }

                    if (b.block.pn != null || (b.block.listPn != null && b.block.listPn.size() > 0)) {   //24.  הקואורדינאטה הימנית של ה"מלבן" בו מופיע המספר הקטלוגי של הפריט בחשבונית.
                        line_csv_file[23] = b.block.get_XRightPn() + "";
                    } else if (CSV2Hash.isDigitalFile || !b.matches_is_fictitious) {
                        line_csv_file[23] = "";
                    } else {
                        line_csv_file[23] = "@";
                    }

                    if (b.block.pn != null || (b.block.listPn != null && b.block.listPn.size() > 0)) {   //25.  הקואורדינאטה התחתונה של ה"מלבן" בו מופיע המספר הקטלוגי של הפריט בחשבונית.
                        line_csv_file[24] = b.block.get_XBottomPn() + "";
                    } else if (CSV2Hash.isDigitalFile || !b.matches_is_fictitious) {
                        line_csv_file[24] = "";
                    } else {
                        line_csv_file[24] = "@";
                    }

                    if (b.block.descr != null && b.block.descr.size() > 0) {     //26.  הקואורדינאטה השמאלית של ה"מלבן" בו מופיע תיאור הפריט בחשבונית.
                        line_csv_file[25] = b.block.get_XLeftDescription() + "";
                    } else {
                        line_csv_file[25] = "@";
                    }

                    if (b.block.descr != null && b.block.descr.size() > 0) {     //27.  הקואורדינאטה העליונה של ה"מלבן" בו מופיע תיאור הפריט בחשבונית.
                        line_csv_file[26] = b.block.get_XTopDescription() + "";
                    } else {
                        line_csv_file[26] = "@";
                    }

                    if (b.block.descr != null && b.block.descr.size() > 0) {      //28.  הקואורדינאטה הימנית של ה"מלבן" בו מופיע תיאור הפריט בחשבונית.
                        line_csv_file[27] = b.block.get_XRightDescription() + "";
                    } else {
                        line_csv_file[27] = "@";
                    }

                    if (b.block.descr != null && b.block.descr.size() > 0) {     //29.  הקואורדינאטה התחתונה של ה"מלבן" בו מופיע תיאור הפריט בחשבונית.
                        line_csv_file[28] = b.block.get_XBottomDescription() + "";
                    } else {
                        line_csv_file[28] = "@";
                    }

                    if (b.block.one_price != null && !b.block.one_price.get_isFictitious()) {                              //30.  הקואורדינאטה השמאלית של ה"מלבן" בו מופיע המחיר ליחידה של הפריט בחשבונית.
                        if ((b.block.one_price == b.block.price || b.block.one_price.get_result_str().equals(b.block.price.get_result_str())) && !CSV2Hash.isDigitalFile) {
                            line_csv_file[29] = "@";
                        }
                        line_csv_file[29] = b.block.get_XLeftUnitPrice() + "";
                    } else if (b.block.one_price != null && b.block.one_price.get_isFictitious()) {
                        line_csv_file[29] = "";
                    } else {
                        line_csv_file[29] = "@";
                    }

                    if (b.block.one_price != null && !b.block.one_price.get_isFictitious()) {                              //31.  הקואורדינאטה העליונה של ה"מלבן" בו מופיע המחיר ליחידה של הפריט בחשבונית.
                        if ((b.block.one_price == b.block.price || b.block.one_price.get_result_str().equals(b.block.price.get_result_str())) && !CSV2Hash.isDigitalFile) {
                            line_csv_file[30] = "@";
                        }
                        line_csv_file[30] = b.block.get_XTopUnitPrice() + "";
                    } else if (b.block.one_price != null && b.block.one_price.get_isFictitious()) {
                        line_csv_file[30] = "";
                    } else {
                        line_csv_file[30] = "@";
                    }

                    if (b.block.one_price != null && !b.block.one_price.get_isFictitious()) {                              //32.  הקואורדינאטה הימנית של ה"מלבן" בו מופיע המחיר ליחידה של הפריט בחשבונית.
                        if ((b.block.one_price == b.block.price || b.block.one_price.get_result_str().equals(b.block.price.get_result_str())) && !CSV2Hash.isDigitalFile) {
                            line_csv_file[31] = "@";
                        }
                        line_csv_file[31] = b.block.get_XRightUnitPrice() + "";
                    } else if (b.block.one_price != null && b.block.one_price.get_isFictitious()) {

                    } else {
                        line_csv_file[31] = "@";
                    }

                    if (b.block.one_price != null && !b.block.one_price.get_isFictitious()) {                              //33.  הקואורדינאטה התחתונה של ה"מלבן" בו מופיע המחיר ליחידה של הפריט בחשבונית.
                        if ((b.block.one_price == b.block.price || b.block.one_price.get_result_str().equals(b.block.price.get_result_str())) && !CSV2Hash.isDigitalFile) {
                            line_csv_file[32] = "@";
                        }
                        line_csv_file[32] = b.block.get_XBottomUnitPrice() + "";
                    } else if (b.block.one_price != null && b.block.one_price.get_isFictitious()) {

                    } else {
                        line_csv_file[32] = "@";
                    }

                    if (b.block.price != null) {                                          //34.	הקואורדינאטה השמאלית של ה"מלבן" בו מופיעה עלות הפריט בחשבונית. 
                        line_csv_file[33] = b.block.get_XLeftPrice() + "";
                    } else {
                        line_csv_file[33] = "@";
                    }
                    if (b.block.price != null) {
                        line_csv_file[34] = b.block.get_XTopPrice() + "";                           // 35.	הקואורדינאטה העליונה של ה"מלבן" בו מופיעה עלות הפריט בחשבונית.
                    } else {
                        line_csv_file[34] = "@";
                    }
                    if (b.block.price != null) {                                         //36.	הקואורדינאטה הימנית של ה"מלבן" בו מופיעה עלות הפריט בחשבונית.
                        line_csv_file[35] = b.block.get_XRightPrice() + "";
                    } else {
                        line_csv_file[35] = "@";
                    }
                    if (b.block.price != null) {                                         //37.	הקואורדינאטה התחתונה של ה"מלבן" בו מופיעה עלות הפריט בחשבונית.
                        line_csv_file[36] = b.block.get_XBottomPrice() + "";
                    } else {
                        line_csv_file[36] = "@";
                    }

                    if (b.matches.size() > 0) {
                        line_csv_file[37] = b.matches.get(0).match[31];
                        line_csv_file[38] = b.matches.get(0).match[32];
                        line_csv_file[39] = SaveLastFields.getLast2Fields(b.matches.get(0).match[24] + "" + b.matches.get(0).match[25]);
                    } else {
                        line_csv_file[39] = ",";
                    }

                    if (b.block.Curreny_for_one_item != null) {   //"קוד מטבע שלפיו נרשם המחיר ליחידת מידה של הפריט	,"
                        //line_csv_file[38]= b.matches.get(0).match[77];
                        line_csv_file[41] = b.block.Curreny_for_one_item;
                    } else {
                        if (b.matches.size() > 0) {
                            line_csv_file[41] = b.matches.get(0).match[85];
                        } else {
                            line_csv_file[41] = "@";
                        }
                    }

                    if (b.block.Curreny_for_all_qty != null) {   //"קוד מטבע שלפיו נרשמה העלות הכוללת של הפריט	,"
                        //line_csv_file[39]= b.matches.get(0).match[77];
                        line_csv_file[42] = b.block.Curreny_for_all_qty;
                    } else {
                        if (b.matches.size() > 0) {
                            line_csv_file[42] = b.matches.get(0).match[40];
                        } else {
                            line_csv_file[42] = "@";
                        }
                    }

                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) { //"שער המרה מהמטבע שבהזמנה למטבע בחשבונית	"
                    } else if (b.block.rate != null) {
                        //line_csv_file[43]= b.matches.get(0).match[77];
                        line_csv_file[43] = b.block.rate.get_result_str_print();
                    } else if (b.matches != null && !b.matches.isEmpty() && !b.matches_is_fictitious) {
                    } else {
                        line_csv_file[43] = "@";
                    }

                    if (b.matches == null || b.matches.isEmpty() || b.matches_is_fictitious) { // אחוז התייקרות מוסכם בין מחיר הפריט בהזמנה למחירו בחשבונית
                    } else if (b.matches.size() > 0) {
                        line_csv_file[44] = b.matches.get(0).match[78];
                    } else {
                        line_csv_file[44] = "@";
                    }

                    {
                        //The first line per item in the invoice:
                        String line = "";
                        if (b.matches.size() > 1) {
                            line += "999999999" + ",,";
                        } else {
                            line += line_csv_file[0] + "," + line_csv_file[1] + ",";
                        }
                        for (int j = 2; j < line_csv_file.length; j++) {
                            if (j != 40) {
                                line += line_csv_file[j] + ",";
                            }
                        }
                        out.write(line + "\r\n");
                    }

                    {
                        //Line per item in the ordered_Items.CSV:

                        if (b.matches.size() > 1) {
                            for (int s = 0; s < b.matches.size(); s++) {
                                String[] lines_per_ordered_Items = new String[line_csv_file.length];

                                for (int j = 0; j < line_csv_file.length; j++) {
                                    lines_per_ordered_Items[j] = line_csv_file[j];
                                }

                                try {
                                    lines_per_ordered_Items[0] = b.matches.get(s).match[31];
                                    if ((s + 1) < 10) {
                                        lines_per_ordered_Items[2] = line_csv_file[2] + "-00" + (s + 1);
                                    } else if ((s + 1) < 100) {
                                        lines_per_ordered_Items[2] = line_csv_file[2] + "-0" + (s + 1);
                                    } else {
                                        lines_per_ordered_Items[2] = line_csv_file[2] + "-" + (s + 1);
                                    }
                                    lines_per_ordered_Items[3] = LFun.doubleValueToPrint(b.matches.get(s).match[34]);
                                    lines_per_ordered_Items[4] = b.matches.get(s).match[21];
                                    //lines_per_ordered_Items[5] = b.matches.get(s).match[35];
                                    try {
                                        lines_per_ordered_Items[5] = LFun.doubleValueToPrint((Double.valueOf(lines_per_ordered_Items[3]) * Double.valueOf(lines_per_ordered_Items[4])) + "");
                                    } catch (Exception e) {
                                    }

                                    double div = Double.valueOf(lines_per_ordered_Items[5]) / (0.0 + (Double.valueOf(line_csv_file[5])));
                                    lines_per_ordered_Items[6] = LFun.doubleValueToPrint((Double.valueOf(line_csv_file[6]) * div) + "");
                                    lines_per_ordered_Items[7] = LFun.doubleValueToPrint((Double.valueOf(line_csv_file[7]) * div) + "");
                                    lines_per_ordered_Items[8] = LFun.doubleValueToPrint((Double.valueOf(line_csv_file[8]) * div) + "");
                                    try {
                                        lines_per_ordered_Items[11] = (Double.valueOf(line_csv_file[11]) * div) + "";
                                    } catch (Exception e) {
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                String line = "";
                                for (int j = 0; j < lines_per_ordered_Items.length; j++) {
                                    if (j != 40) {
                                        line += lines_per_ordered_Items[j] + ",";
                                    }
                                }
                                out.write(line + "\r\n");
                            }
                        }
                    }

                }

                //out.write("פзp");
            } finally {
                out.close();
                write(orig_name, "CSV.SaveData", "", "output successfully issued", true);
            }

            try {
                bnvWithMatchesToItems.set_BudgetMisMatch(beenArchived, DeviationForBudget, PercentageOfBudget);
                createMisMatchAndTransferToError(bnvWithMatchesToItems, forMisatch, orig_name, data.getAbsolutePath());
            } catch (Exception e) {

            }

            /*
             //This log checking the sumallNoMamv, sumallMamv, sumallWithMamv
             inon_log();
             */
        } catch (Exception r) {
            write(orig_name, "CSV.SaveData", "can't save data \"saveData\"", "", true);
        }

        //fix names:
        {
            String[] endings = {".anl", ".tif", ".pdf", ".rcw"};
            for (String ending : endings) {
                try {
                    File file = LFun.getFile(Path.getOutputFolder(), filename[0] + filename[1] + filename[2], ending);
                    if (file != null) {
                        // System.out.println("from:"+file.getAbsolutePath());
                        java.nio.file.Path from = Paths.get(file.getAbsolutePath());

                        //System.out.println("to:"+ fileName + ending);
                        //String s = (fileName + ending).substring(97);
                        java.nio.file.Path to = Paths.get(fileName + ending);

                        Files.move(from, to, REPLACE_EXISTING);

                    } else {
                        System.out.println("file==null: ending=" + ending);
                    }

                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }

        }

        return true;
    }

    public static void moveInvoiceToAlreadyExistFolder(String path, String dest) {
        File[] folder = new File(Path.getOutputFolder()).listFiles();
        String orig_name = new File(path).getName().replace(".csv", "");
        boolean needToMove = true;
        for (int i = 0; i < folder.length; i++) {
            if (folder[i].getName().contains(orig_name) && folder[i].getName().endsWith(".csv")) {
                needToMove = false;
                break;
            }
        }
        moveOrCopyTif(orig_name, dest, needToMove);
    }

    static void moveOrCopyTif(String orig_name, String dest, boolean needToMove) {
        File Invoice = new File(Path.getOutputFolder() + "\\" + orig_name + ".tif");
        String moveOrCopy = "";
        if (needToMove) {
            moveOrCopy = "/move";
        } else {
            moveOrCopy = "/copy";
        }

        if (Invoice.exists()) {
            Process MoveIni = null;
            String command = "\"" + WiseConfig.Config.getWiseCopyExePath() + "\" \"" + Invoice.getAbsolutePath() + "\" \"" + Pathes.Path.getOutputFolder() + "\\" + "Error" + "\\" + dest + "\" /s /h /l " + moveOrCopy + " /over";
            try {
                MoveIni = Runtime.getRuntime().exec(command);
                System.out.println(command);
            } catch (Exception ex) {
            } finally {
                writeWiserunLog(command);
            }
            while (MoveIni.isAlive()) {
            }
        }
    }

    private static void createMisMatchAndTransferToError(MatchesToItems bnvWithMatchesToItems, String[] forMisatch, String orig_name, String pathOfData) {
        if (bnvWithMatchesToItems.isCURRENCY_CONVERSION_MISMATCH) {
            forMisatch[11] = "CURRENCY_CONVERSION_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        if (bnvWithMatchesToItems.isQUANTITY_MISMATCH) {
            forMisatch[11] = "QUANTITY_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        if (bnvWithMatchesToItems.isB2B_MISMATCH) {
            forMisatch[11] = "B2B_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        if (bnvWithMatchesToItems.isBUDGET_MISMATCH) {
            forMisatch[11] = "BUDGET_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        if (bnvWithMatchesToItems.isORDER_MISMATCH) {
            forMisatch[11] = "ORDER_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }

        if (bnvWithMatchesToItems.isENTRY_MISMATCH) {
            forMisatch[11] = "ENTRY_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        if (bnvWithMatchesToItems.isSHIPPING_MISMATCH) {
            forMisatch[11] = "SHIPPING_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        if (bnvWithMatchesToItems.isTRANSACTION_MISMATCH) {
            forMisatch[11] = "TRANSACTION_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }

        if (bnvWithMatchesToItems.isPRICE_MISMATCH) {
            forMisatch[11] = "PRICE_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }
        /*if(bnvWithMatchesToItems.isPROFORMA_MISMATCH){
         forMisatch[11] = "PROFORMA_MISMATCH";
         addToMisMatchByName(forMisatch);
         }*/
        if (bnvWithMatchesToItems.isTOTAL_MISMATCH) {
            forMisatch[11] = "TOTAL_MISMATCH";
            addToMisMatchByNameToInvoice_Mismatches(forMisatch);
        }

        if (bnvWithMatchesToItems.isPRICE_MISMATCH || bnvWithMatchesToItems.isQUANTITY_MISMATCH) {
            addInvoiceToError(orig_name, pathOfData, "QUANTITY or PRICE");
        }
        if (bnvWithMatchesToItems.isORDER_MISMATCH) {
            addInvoiceToError(orig_name, pathOfData, "ORDER");
        }
        if (bnvWithMatchesToItems.isBUDGET_MISMATCH) {
            addInvoiceToError(orig_name, pathOfData, "BUDGET");
        }
        if (bnvWithMatchesToItems.isProformalError) {
            addInvoiceToError(orig_name, pathOfData, "Proformal Error");
        }
    }

    static void addInvoiceToError(String orig_name, String pathOfData, String dest) {
        File Invoice = new File(Path.getOutputFolder() + "\\" + orig_name + ".tif");

        if (Invoice.exists()) {
            Process MoveIni = null;
            String command = "\"" + WiseConfig.Config.getWiseCopyExePath() + "\" \"" + Invoice.getAbsolutePath() + "\" \"" + Pathes.Path.getOutputFolder() + "\\" + "Error" + "\\" + dest + "\" /s /h /l /copy /over";
            try {
                MoveIni = Runtime.getRuntime().exec(command);
                System.out.println(command);
            } catch (Exception ex) {
            } finally {
                writeWiserunLog(command);
            }
            while (MoveIni.isAlive()) {
            }
        }

        File Data = new File(pathOfData);
        if (Data.exists()) {
            Process MoveIni = null;
            String command = "\"" + WiseConfig.Config.getWiseCopyExePath() + "\" \"" + Data.getAbsolutePath() + "\" \"" + Pathes.Path.getOutputFolder() + "\\" + "Error" + "\\" + dest + "\" /s /h /l /copy /over";
            try {
                MoveIni = Runtime.getRuntime().exec(command);
                System.out.println(command);
            } catch (Exception ex) {
            } finally {
                writeWiserunLog(command);
            }
            while (MoveIni.isAlive()) {
            }
        }
    }

    private static void addToMisMatchByNameToInvoice_Mismatches(String[] forMisatch) {
        try {
            File f = new File(Path.getOutputFolder() + "\\" + "Invoice_Mismatches.csv");
            String toWrite = "";
            for (int i = 0; i < forMisatch.length; i++) {
                toWrite += forMisatch[i];
            }
            if (!f.exists()) {
                try {
                    toWrite = "FILENAME,RECOGNIZED_SENDER,SENDER_STREET_ADDRESS,DOC_NUMBER,DOC_DATE,TOTALSUM,ERROR_TYPE,\n" + toWrite;
                    f.createNewFile();
                } catch (Exception e) {

                }
            }

            Charset inputCharset = Charset.forName(CSV_FORMAT);
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, true), inputCharset));
            out.write(toWrite + "\r\n");
            out.close();
        } catch (Exception e) {

        }
    }

    public static boolean createDtlFile(String fileName, MatchesToItems bnvWithMatchesToItems, InfoReader infoReader,
            Vector<VerVar> vVerVar, Vector<String[]> orderedItemsCSV) {

        try {
            File data = new File(fileName + ".dtl");
            try {
                data.createNewFile();
            } catch (Exception exception) {
                exception.getMessage();
            }

            System.out.println(data.getPath());

            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(data), DTL_FORMAT));
            //out.write("Supply_Certificate,ITEM_NUMBER_in_Supply_Certificate,ITEM_DATE_in_Supply_certificate,ITEM_QUANTITY_in_Supply_Certificate,VERIFIER_ID_of_the_Supply_Certificate,VERIFIER_PHONE_in_the_Supply_Certificate,VERIFIER_EMAIL_of_the_Supply_Certificate,PO_NUMBER,ITEM_NUMBER_in_PO,ITEM_DATE_in_PO,ITEM_QUANTITY_in_PO,PACKAGES_QUANTITY_PER_ITEM_in_PO,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PO,SUPPLIER_ID_IN_ERP,Customer_ITEM_CATNO,Supplier_ITEM_CATNO,Manufacturer_ITEM_CATNO,ITEM_DESCRIPTION_in_PO,ITEM_MODEL_NUMBER,ITEM_SERIAL_NUMBER,ITEM_CLASSIFICATION_NUMBER_in_ERP,ITEM_CLASSIFICATION_DESCRIPTION_in_ERP,CURRENCY_of_ITEM_in_PO,ITEM_UNIT_PRICE_in_PO,ITEM_TOTAL_PRICE_in_PO,VAT_PERCENT_PER_ITEM_in_PO,PROJECT_NAME_referring_to_ordered_item,PROJECT_NUMBER_referring_to_ordered_item,BUDGET_NUMBER_for_ordered_item,CURRENCY_for_total_sum_in_PO,TOTAL_VAT_EXEPMPT_SUM_in_PO,TOTAL_VAT_CHARCHABLE_SUM_in_PO,TOTAL_VAT_SUM_in_PO,TOTAL_SUM_EXCLUDING_VAT_in_PO,ORDERER_COMPANY_ID_IN_ERP,ORDERER_NAME,ORDERER_PHONE,ORDERER_EMAIL,PO_APPROVER_ID,PO_APPROVER_PHONE,PO_APPROVER_EMAIL,PO_PAYMENT_TERMS,PQ_NUMBER,ITEM_NUMBER_in_PQ,ITEM_DATE_in_PQ,ITEM_QUANTITY_in_PQ,PACKAGES_QUANTITY_PER_ITEM_in_PQ,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PQ,CURRENCY_of_ITEM_in_PQ,ITEM_UNIT_PRICE_in_PQ,ITEM_TOTAL_PRICE_in_PQ,VAT_PERCENT_PER_ITEM_in_PQ,Bill_Of_Lading_NUMBER,ITEM_NUMBER_in_Bill_Of_Lading,ITEM_DATE_in_Bill_Of_Lading,ITEM_QUANTITY_in_Bill_Of_Lading,IMPORT_FILE_NUMBER,IMPORT_ENTRY_Number,ITEM_NUMBER_in_IMPORT_ENTRY,ITEM_DATE_in_IMPORT_ENTRY,ITEM_QUANTITY_in_IMPORT_ENTRY,IMPORT_AGENCY_Name,IMPORT_AGENCY_ID_IN_ERP,IMPORT_AGENCY_FILE_NUMBER,IMPORT_TAXES_PAID_EXCLUDING_VAT,IMPORT_VAT_PAID,CURRENCY_CONVERSION_RATE_between_PO_and_INVOICE,AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO,Reported_INVOICE_NUMBER,Reported_INVOICE_DATE,Reported_ITEM_NUMBER_in_INVOICE,Reported_ITEM_CATNO_in_INVOICE,Reported_ITEM_DESCRIPTION_in_INVOICE,Reported_ITEM_INTERNAL_COUNTER_in_INVOICE,Reported_CURRENCY_of_item_in_INVOICE,Reported_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Reported_ITEM_QUANTITY_in_INVOICE,Reported_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Reported_ITEM_VAT_SUM_in_INVOICE,Verified_PROFORMA_INVOICE_NUMBER,Verified_PROFORMA_INVOICE_DATE,Verified_ITEM_NUMBER_in_PROFORMA_INVOICE,Verified_ITEM_CATNO_in_PROFORMA_INVOICE,Verified_ITEM_DESCRIPTION_in_PROFORMA_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_PROFORMA_INVOICE,Verified_CURRENCY_of_item_in_PROFORMA_INVOICE,Verified_ITEM_UNIT_PRICE_in_PROFORMA_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_PROFORMA_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_PROFORMA_INVOICE,Verified_ITEM_VAT_SUM_in_PROFORMA_INVOICE,Verified_PROFORMA_INVOICE_ID_IN_ERP,Verified_PROFORMA_INVOICE_FILENAME,Verified_INVOICE_NUMBER,Verified_INVOICE_DATE,Verified_ITEM_NUMBER_in_INVOICE,Verified_ITEM_CATNO_in_INVOICE,Verified_ITEM_DESCRIPTION_in_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_INVOICE,Verified_CURRENCY_of_item_in_INVOICE,Verified_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Verified_ITEM_VAT_SUM_in_INVOICE,Verified_INVOICE_ID_IN_ERP,Verified_INVOICE_FILENAME,ITEM_PAYMENT_APPROVER_NAME,ITEM_PAYMENT_APPROVER_PHONE,ITEM_PAYMENT_APPROVER_EMAIL,PREAPPROVED_PAYMENT_INSTRUCTION_NUMBER,PREAPPROVED_PAYMENT_INSTRUCTION_DATE,APPROVED_PAYMENT_INSTRUCTION_NUMBER,APPROVED_PAYMENT_INSTRUCTION_DATE,REMAIDER_BUDGET_EXCLUDING_VAT,REMAINDER_BUDGET_INCLUDING_VAT" + "\r\n");
            out.write("Supply_Certificate,ITEM_NUMBER_in_Supply_Certificate,ITEM_DATE_in_Supply_certificate,ITEM_QUANTITY_in_Supply_Certificate,VERIFIER_ID_of_the_Supply_Certificate,VERIFIER_PHONE_in_the_Supply_Certificate,VERIFIER_EMAIL_of_the_Supply_Certificate,PO_NUMBER,ITEM_NUMBER_in_PO,ITEM_DATE_in_PO,ITEM_QUANTITY_in_PO,PACKAGES_QUANTITY_PER_ITEM_in_PO,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PO,SUPPLIER_ID_IN_ERP,Customer_ITEM_CATNO,Supplier_ITEM_CATNO,Manufacturer_ITEM_CATNO,ITEM_DESCRIPTION_in_PO,ITEM_MODEL_NUMBER,ITEM_SERIAL_NUMBER,ITEM_CLASSIFICATION_NUMBER_in_ERP,ITEM_CLASSIFICATION_DESCRIPTION_in_ERP,CURRENCY_of_ITEM_in_PO,ITEM_UNIT_PRICE_in_PO,ITEM_TOTAL_PRICE_in_PO,VAT_PERCENT_PER_ITEM_in_PO,PROJECT_NAME_referring_to_ordered_item,PROJECT_NUMBER_referring_to_ordered_item,BUDGET_NUMBER_for_ordered_item,CURRENCY_for_total_sum_in_PO,TOTAL_VAT_EXEPMPT_SUM_in_PO,TOTAL_VAT_CHARCHABLE_SUM_in_PO,TOTAL_VAT_SUM_in_PO,TOTAL_SUM_EXCLUDING_VAT_in_PO,ORDERER_COMPANY_ID_IN_ERP,ORDERER_NAME,ORDERER_PHONE,ORDERER_EMAIL,PO_APPROVER_ID,PO_APPROVER_PHONE,PO_APPROVER_EMAIL,PO_PAYMENT_TERMS,PQ_NUMBER,ITEM_NUMBER_in_PQ,ITEM_DATE_in_PQ,ITEM_QUANTITY_in_PQ,PACKAGES_QUANTITY_PER_ITEM_in_PQ,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PQ,CURRENCY_of_ITEM_in_PQ,ITEM_UNIT_PRICE_in_PQ,ITEM_TOTAL_PRICE_in_PQ,VAT_PERCENT_PER_ITEM_in_PQ,Bill_Of_Lading_NUMBER,ITEM_NUMBER_in_Bill_Of_Lading,ITEM_DATE_in_Bill_Of_Lading,ITEM_QUANTITY_in_Bill_Of_Lading,IMPORT_FILE_NUMBER,IMPORT_ENTRY_Number,ITEM_NUMBER_in_IMPORT_ENTRY,ITEM_DATE_in_IMPORT_ENTRY,ITEM_QUANTITY_in_IMPORT_ENTRY,IMPORT_AGENCY_Name,IMPORT_AGENCY_ID_IN_ERP,IMPORT_AGENCY_FILE_NUMBER,IMPORT_TAXES_PAID_EXCLUDING_VAT,IMPORT_VAT_PAID,CURRENCY_CONVERSION_RATE_between_PO_and_INVOICE,AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO,Reported_INVOICE_NUMBER,Reported_INVOICE_DATE,Reported_ITEM_NUMBER_in_INVOICE,Reported_ITEM_CATNO_in_INVOICE,Reported_ITEM_DESCRIPTION_in_INVOICE,Reported_ITEM_INTERNAL_COUNTER_in_INVOICE,Reported_CURRENCY_of_item_in_INVOICE,Reported_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Reported_ITEM_QUANTITY_in_INVOICE,Reported_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Reported_ITEM_VAT_SUM_in_INVOICE,Verified_PROFORMA_INVOICE_NUMBER,Verified_PROFORMA_INVOICE_DATE,Verified_ITEM_NUMBER_in_PROFORMA_INVOICE,Verified_ITEM_CATNO_in_PROFORMA_INVOICE,Verified_ITEM_DESCRIPTION_in_PROFORMA_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_PROFORMA_INVOICE,Verified_CURRENCY_of_item_in_PROFORMA_INVOICE,Verified_ITEM_UNIT_PRICE_in_PROFORMA_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_PROFORMA_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_PROFORMA_INVOICE,Verified_ITEM_VAT_SUM_in_PROFORMA_INVOICE,Verified_PROFORMA_INVOICE_ID_IN_ERP,Verified_PROFORMA_INVOICE_FILENAME,Verified_INVOICE_NUMBER,Verified_INVOICE_DATE,Verified_ITEM_NUMBER_in_INVOICE,Verified_ITEM_CATNO_in_INVOICE,Verified_ITEM_DESCRIPTION_in_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_INVOICE,Verified_CURRENCY_of_item_in_INVOICE,Verified_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Verified_ITEM_VAT_SUM_in_INVOICE,Verified_INVOICE_ID_IN_ERP,Verified_INVOICE_FILENAME,ITEM_PAYMENT_APPROVER_NAME,ITEM_PAYMENT_APPROVER_PHONE,ITEM_PAYMENT_APPROVER_EMAIL,PREAPPROVED_PAYMENT_INSTRUCTION_NUMBER,PREAPPROVED_PAYMENT_INSTRUCTION_DATE,APPROVED_PAYMENT_INSTRUCTION_NUMBER,APPROVED_PAYMENT_INSTRUCTION_DATE,REMAIDER_BUDGET_EXCLUDING_VAT,REMAINDER_BUDGET_INCLUDING_VAT,BUDGET_CURRENCY,ACCOUNTING_TRANSACTION_TYPE,ACCOUNTING_COSTING_TYPE,ACCOUNTING_BATCH_NUMBER,Percent_difference_at_CONVERSION_RATE_between_invoice_and_order" + "\r\n");

            int index = 1;
            int indexOfMatch = 0;
            IndexesOfCsv indexes = new IndexesOfCsv(bnvWithMatchesToItems);

            try {
                for (MatchesToOneItem block : bnvWithMatchesToItems.MatchesToItems) {
                    if (block.matches != null && bnvWithMatchesToItems.matchInCsvToInvoiceA4 == null) {
                        block.orderedItemsCSV = orderedItemsCSV;
                        block.updateAndFillBalanceOfBudget();
                        for (int i = 0; i < block.matches.size(); i++) {
                            if ((block.matches.elementAt(i).match.length == 125 || block.matches.elementAt(i).match.length == 129 || block.matches.elementAt(i).match.length == 118)
                                    && (block.matches_is_fictitious || (block.matches.elementAt(i).get_connect() || (block.matches.elementAt(i).get_needNewLine() && block.matches.elementAt(i).get_qtyToNewLine() != 0)))
                                    && !(block.matchToOrderedItemsCSVbyCSVOneLine && bnvWithMatchesToItems.MatchesToItems.get(0) != block)) {
                                String[] s = new String[118];
                                String[] sForIndex = new String[block.matches.elementAt(i).match.length - 11];
                                for (int j = 0; j < s.length && j < sForIndex.length; j++) {
                                    if (j > 78 && j < 105 && bnvWithMatchesToItems.isProformalInvoice) {
                                        s[j] = "";
                                    } else if (((j >= 7 && j <= 12) || (j >= 17 && j <= 41)) && bnvWithMatchesToItems.isPORDFile) {
                                        s[j] = "";
                                    } else if (j >= 52 && j <= 55 && bnvWithMatchesToItems.isMISHFile) {
                                        s[j] = "";
                                    } else {
                                        s[j] = block.matches.elementAt(i).match[j + 11];
                                    }
                                    sForIndex[j] = block.matches.elementAt(i).match[j + 11];
                                }

                                indexOfMatch = MatchesToOneItem.getIndexInOrderedItemsCSV(orderedItemsCSV, sForIndex);

                                try {
                                    if (infoReader != null && infoReader.Invoice_ID12 != null && indexes.getIndexOfFileNumber() != -1) {
                                        s[indexes.getIndexOfFileNumber()] = infoReader.Invoice_ID12; // מספר החשבונית בה סופק הפריט
                                    } else if (indexes.getIndexOfFileNumber() != -1) {
                                        s[indexes.getIndexOfFileNumber()] = "";
                                    }
                                } catch (Exception e) {
                                }

                                try {
                                    if (infoReader != null && infoReader.InvoiceDate14 != null && indexes.getIndexOfFileDate() != -1) {
                                        s[indexes.getIndexOfFileDate()] = infoReader.InvoiceDate14; //תאריך החשבונית (בפורמט אחיד DD.MM.YYYY)
                                    } else if (indexes.getIndexOfFileDate() != -1) {
                                        s[indexes.getIndexOfFileDate()] = "";
                                    }
                                } catch (Exception e) {
                                }

                                if (indexes.getIndexOfItemLine() != -1) {
                                    s[indexes.getIndexOfItemLine()] = String.valueOf(index); //מספר השורה בחשבונית בה מופיע הפריט הרלבנטי
                                }

                                if (!block.block.listPn.isEmpty() && indexes.getIndexOfPN() != -1) {
                                    s[indexes.getIndexOfPN()] += block.block.get_listPn(); // מספר קטלוגי של הפריט בחשבונית
                                } else if (indexes.getIndexOfPN() != -1) {
                                    s[indexes.getIndexOfPN()] = ""; // מספר קטלוגי של הפריט בחשבונית
                                }

                                if (block.block.descr != null && block.block.descr.size() > 0 && indexes.getIndexOfDescription() != -1) {
                                    s[indexes.getIndexOfDescription()] += block.block.get_description().replace(",", ".").trim(); //תיאור הפריט בחשבונית  
                                } else if (indexes.getIndexOfDescription() != -1) {
                                    s[indexes.getIndexOfDescription()] = ""; //תיאור הפריט בחשבונית  
                                }

                                if (indexes.getIndexOfInteranalCounter() != -1) {
                                    s[indexes.getIndexOfInteranalCounter()] = ""; //מונה דפים/קילומטרים לגבי הפריט 
                                }

                                if (indexes.getIndexOfCurrencyItem() != -1) {//קוד מטבע של הפריט הרלבנטי בחשבונית(לדוגמא: USD אוILS)
                                    if (block.matches_is_fictitious) {
                                        s[indexes.getIndexOfCurrencyItem()] = block.block.Curreny_for_one_item;
                                    } else {
                                        s[indexes.getIndexOfCurrencyItem()] = "";
                                    }
                                }

                                if (indexes.getIndexOfSupplierID() != -1) {//קוד המטבע שבו רשומים סכום החשבונית והתקציב
                                    if (s[indexes.getIndexOfSupplierID()].isEmpty() && infoReader != null && infoReader.SupplierID != null && infoReader.SupplierID.length() > 2) {
                                        s[indexes.getIndexOfSupplierID()] = infoReader.SupplierID;
                                    }
                                }

                                if (indexes.getindexOfBUDGET_CURRENCY() != -1) {//קוד המטבע שבו רשומים סכום החשבונית והתקציב
                                    if (block.matches_is_fictitious || (s.length < indexes.getindexOfBUDGET_CURRENCY() && s[indexes.getindexOfBUDGET_CURRENCY()].isEmpty())) {
                                        s[indexes.getindexOfBUDGET_CURRENCY()] = block.block.Curreny_for_all_qty;
                                    }
                                }

                                if (block.block.one_price != null && indexes.getIndexOfUnitPrice() != -1) {
                                    s[indexes.getIndexOfUnitPrice()] = block.block.one_price.get_result_str().replace(",", "."); //מחיר ליחידה בחשבונית (ללא מעמ)
                                } else if (indexes.getIndexOfUnitPrice() != -1) {
                                    s[indexes.getIndexOfUnitPrice()] = ""; //מחיר ליחידה בחשבונית (ללא מעמ) 
                                }

                                if (block.block.qty != null && indexes.getIndexOfItemQuantity() != -1) {
                                    double num = 0;
                                    try {
                                        num = Double.valueOf(block.block.qty.get_result_str().replace(",", "."));
                                        if ((bnvWithMatchesToItems.isCreditInvoice || bnvWithMatchesToItems.isCreditMISHFile || bnvWithMatchesToItems.isCreditPORDFile) && num > 0) {
                                            num *= (-1);
                                        }
                                    } catch (Exception e) {
                                        s[indexes.getIndexOfItemQuantity()] = block.block.qty.get_result_str().replace(",", "."); //כמות הפריט בחשבונית
                                    }

                                    s[indexes.getIndexOfItemQuantity()] = String.valueOf(num); //כמות הפריט בחשבונית
                                } else if (indexes.getIndexOfItemQuantity() != -1) {
                                    s[indexes.getIndexOfItemQuantity()] = ""; //כמות הפריט בחשבונית
                                }

                                if (block.block.price != null && indexes.getIndexOfItemTotalPriceExcludingVat() != -1) {
                                    double num = 0;
                                    try {
                                        num = Double.valueOf(block.block.price.get_result_str().replace(",", "."));
                                        if ((bnvWithMatchesToItems.isCreditInvoice || bnvWithMatchesToItems.isCreditPORDFile) && num > 0) {
                                            num *= (-1);
                                        }
                                    } catch (Exception e) {
                                        s[indexes.getIndexOfItemTotalPriceExcludingVat()] = block.block.price.get_result_str().replace(",", "."); //מחיר הכמות שסופקה בחשבונית(ללא מעמ)
                                    }
                                    s[indexes.getIndexOfItemTotalPriceExcludingVat()] = String.valueOf(num);  //מחיר הכמות שסופקה בחשבונית(ללא מעמ)

                                } else if (indexes.getIndexOfItemTotalPriceExcludingVat() != -1) {
                                    s[indexes.getIndexOfItemTotalPriceExcludingVat()] = ""; //מחיר הכמות שסופקה בחשבונית(ללא מעמ)
                                }

                                if (block.block.mam != null && indexes.getIndexOfItemVatSum() != -1) {
                                    double num = 0;
                                    try {
                                        num = Double.valueOf(block.block.mam.get_result_str().replace(",", "."));
                                        if ((bnvWithMatchesToItems.isCreditInvoice || bnvWithMatchesToItems.isCreditPORDFile) && num > 0) {
                                            num *= (-1);
                                        }
                                    } catch (Exception e) {
                                        s[indexes.getIndexOfItemVatSum()] = block.block.mam.get_result_str().replace(",", "."); // המעמ בגין הפריט שבחשבונית
                                    }
                                    s[indexes.getIndexOfItemVatSum()] = String.valueOf(num);  // המעמ בגין הפריט שבחשבונית
                                } else if (indexes.getIndexOfItemVatSum() != -1) {
                                    s[indexes.getIndexOfItemVatSum()] = ""; // המעמ בגין הפריט שבחשבונית
                                }

                                if (block.matchToOrderedItemsCSVbyCSVOneLine || block.matchToOrderedItemsCSVbyCSVManyLine) {
                                    s[113] = "0";
                                }

                                if (indexes.getIndexOfFileName() != -1) { // שם הקובץ 104
                                    try {
                                        s[indexes.getIndexOfFileName()] = "\"" + new File(fileName).getName() + ".pdf\"";
                                    } catch (Exception e) {
                                        s[indexes.getIndexOfFileName()] = "\"" + fileName + ".pdf\"";
                                    }
                                }

                                if (indexes.getIndexOfVatPercentPerItem() != -1) {
                                    if (block.block.percent != null) {
                                        s[indexes.getIndexOfVatPercentPerItem()] = block.block.percent.get_result_str().replace(",", ".").replaceAll("%", "");  // אחוז המעמ
                                    } else {
                                        s[indexes.getIndexOfVatPercentPerItem()] = "";
                                    }
                                }

                                String text = "";
                                for (int j = 0; j < s.length; j++) {
                                    //d.m.yyyy --> dd.mm.yyyy
                                    if (j == 2 || j == 9 || j == 44 || j == 54 || j == 59 || j == 69 || j == 80 || j == 93 || j == 109 || j == 111) {
                                        s[j] = LFun.ToDataFormat(s[j]);
                                    }

                                    text += s[j];
                                    if (j != s.length - 1) {
                                        text += ",";
                                    }
                                }
                                if (block.matches.elementAt(i).match.length == 125) {
                                    text += "," + SaveLastFields.getLastFields(s[13] + "," + s[14]);
                                }

                                if (block.block.Percent_difference_rate_block_vs_matches != 0) {
                                    text += "," + block.block.Percent_difference_rate_block_vs_matches;
                                } else {
                                    text += ",";
                                }

                                out.write(text + "\r\n");

                                if (block.matches.elementAt(i).get_connect() && indexOfMatch != -1) {
                                    orderedItemsCSV.setElementAt(s, indexOfMatch);
                                } else if (block.matches.elementAt(i).get_needNewLine() && block.matches.elementAt(i).get_qtyToNewLine() != 0) {
                                    orderedItemsCSV.insertElementAt(s, indexOfMatch + 1);
                                }

                                orderedItemsCSV = MatchesToOneItem.updateBalanceOgBudgetInAllRows(orderedItemsCSV, s);
                            }
                        }

                        index++;
                        try {
                            updateOrderedItemsCSV(orderedItemsCSV);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                // במידה ויש רשומה אחת שמתאימה לכל החשבונית
                if (bnvWithMatchesToItems.matchInCsvToInvoiceA4 != null && bnvWithMatchesToItems.matchInCsvToInvoiceA4.length == 125) {
                    String[] s = new String[bnvWithMatchesToItems.matchInCsvToInvoiceA4.length - 11];
                    for (int j = 0; j < s.length; j++) {
                        s[j] = bnvWithMatchesToItems.matchInCsvToInvoiceA4[j + 11];
                    }

                    indexOfMatch = MatchesToOneItem.getIndexInOrderedItemsCSV(orderedItemsCSV, s);

                    //String[] result = CSV2Hash.matchInCsvToInvoiceA4;
                    String Pns = "";
                    String Descriptions = "";

                    for (int i = 0; i < bnvWithMatchesToItems.MatchesToItems.size(); i++) {
                        if (!bnvWithMatchesToItems.MatchesToItems.get(i).block.listPn.isEmpty()) {
                            Pns += bnvWithMatchesToItems.MatchesToItems.get(i).block.get_listPn(); // מספר קטלוגי של הפריט בחשבונית
                        } else {
                            Pns += "@"; // מספר קטלוגי של הפריט בחשבונית
                        }

                        if (bnvWithMatchesToItems.MatchesToItems.get(i).block.descr != null && bnvWithMatchesToItems.MatchesToItems.get(i).block.descr.size() > 0) {
                            Descriptions += bnvWithMatchesToItems.MatchesToItems.get(i).block.get_description().replace(",", "."); //תיאור הפריט בחשבונית  
                        } else {
                            Descriptions += "@"; //תיאור הפריט בחשבונית  
                        }

                        if (i != bnvWithMatchesToItems.MatchesToItems.size() - 1) {
                            Pns += ";";
                            Descriptions += ";";
                        }
                    }

                    try {
                        if (infoReader != null && infoReader.Invoice_ID12 != null && indexes.getIndexOfFileNumber() != -1) {
                            s[indexes.getIndexOfFileNumber()] = infoReader.Invoice_ID12; // מספר המסמך 
                        }
                    } catch (Exception e) {
                    }

                    try {
                        if (infoReader != null && infoReader.InvoiceDate14 != null && indexes.getIndexOfFileDate() != -1) {
                            s[indexes.getIndexOfFileDate()] = infoReader.InvoiceDate14; //תאריך המסמך (בפורמט אחיד DD.MM.YYYY)
                        } else if (indexes.getIndexOfFileDate() != -1) {
                            s[indexes.getIndexOfFileDate()] = "";
                        }
                    } catch (Exception e) {
                    }

                    //s[94] = ""; //מספר השורה בחשבונית בה מופיע הפריט הרלבנטי
                    if (indexes.getIndexOfPN() != -1) {
                        s[indexes.getIndexOfPN()] = Pns; // כל המקטים
                    }

                    if (indexes.getIndexOfDescription() != -1) {
                        s[indexes.getIndexOfDescription()] = "\"" + Descriptions + "\""; // כל תיאורי הפריטים 
                    }

                    if (indexes.getIndexOfInteranalCounter() != -1) {
                        s[indexes.getIndexOfInteranalCounter()] = ""; //מונה דפים/קילומטרים לגבי הפריט
                    }

                    if (indexes.getIndexOfCurrencyItem() != -1) {
                        s[indexes.getIndexOfCurrencyItem()] = ""; //קוד מטבע של הפריט הרלבנטי בחשבונית(לדוגמא: USD אוILS)
                    }

                    /*if(bnvWithMatchesToItems.isINVO || bnvWithMatchesToItems.isCreditInvoice || bnvWithMatchesToItems.isProformalInvoice){
                     s[99] = ""; //מחיר ליחידה בחשבונית (ללא מעמ)
                     }else if(bnvWithMatchesToItems.isPORDFile || bnvWithMatchesToItems.isCreditPORDFile){
                     s[23] = ""; //מחיר ליחידה בחשבונית (ללא מעמ)
                     }*/
                    if (indexes.getIndexOfItemQuantity() != -1) {
                        if ((bnvWithMatchesToItems.isCreditInvoice || bnvWithMatchesToItems.isCreditPORDFile) && vVerVar.get(0).getSum() > 0) {
                            vVerVar.get(0).setSum(vVerVar.get(0).getSum() * (-1));
                        }
                        s[indexes.getIndexOfItemQuantity()] = vVerVar.get(0).toString(); //כמות הפריטים בחשבונית
                    }

                    if (indexes.getIndexOfItemTotalPriceExcludingVat() != -1) {
                        if ((bnvWithMatchesToItems.isCreditInvoice || bnvWithMatchesToItems.isCreditPORDFile) && vVerVar.get(4).getSum() > 0) {
                            vVerVar.get(4).setSum(vVerVar.get(4).getSum() * (-1));
                        }
                        s[indexes.getIndexOfItemTotalPriceExcludingVat()] = vVerVar.get(4).toString(); // סכום החשבונית ללא מעמ (במקום סכום הפריט ללא מעמ)
                    }

                    if (indexes.getIndexOfItemVatSum() != -1) {
                        if (bnvWithMatchesToItems.isCreditInvoice && vVerVar.get(5).getSum() > 0) {
                            vVerVar.get(5).setSum(vVerVar.get(5).getSum() * (-1));
                        }
                        s[indexes.getIndexOfItemVatSum()] = vVerVar.get(5).toString(); // המעמ בגין החשבונית כולה (במקום המעמ בגין הפריט בחשבונית)
                    }

                    if (indexes.getIndexOfFileName() != -1) {
                        s[indexes.getIndexOfFileName()] = "\"" + fileName + "\""; // שם הקובץ שבו מופיעה חשבונית המס הרלבנטית.
                    }

                    String text = "";
                    for (int i = 0; i < s.length; i++) {
                        text += s[i];
                        if (i != s.length - 1) {
                            text += ",";
                        }
                    }

                    orderedItemsCSV.insertElementAt(s, indexOfMatch);
                    updateOrderedItemsCSV(orderedItemsCSV);
                    out.write(text + "\r\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                out.close();
                write(fileName, "CSV.SaveData", "", "output successfully issued", true);

            }

        } catch (Exception r) {
            r.printStackTrace();
            write(fileName, "CSV.SaveData", "can't save data \"saveData\"", "", true);
        }

        return true;
    }

    private static void updateOrderedItemsCSV(Vector<String[]> orderedItemsCSV) throws IOException {
        //Write orderedItemsCSV
        try {

            File file = new File(Path.get_ordered_Items_csv());
            Charset inputCharset = Charset.forName(CSV_FORMAT);
            OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(file), inputCharset);

            //FileWriter fw = new FileWriter(file);
            //BufferedWriter bw = new BufferedWriter(fw);
            //Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            //os.write("Supply_Certificate,ITEM_NUMBER_in_Supply_Certificate,ITEM_DATE_in_Supply_certificate,ITEM_QUANTITY_in_Supply_Certificate,VERIFIER_ID_of_the_Supply_Certificate,VERIFIER_PHONE_in_the_Supply_Certificate,VERIFIER_EMAIL_of_the_Supply_Certificate,PO_NUMBER,ITEM_NUMBER_in_PO,ITEM_DATE_in_PO,ITEM_QUANTITY_in_PO,PACKAGES_QUANTITY_PER_ITEM_in_PO,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PO,SUPPLIER_ID_IN_ERP,Customer_ITEM_CATNO,Supplier_ITEM_CATNO,Manufacturer_ITEM_CATNO,ITEM_DESCRIPTION_in_PO,ITEM_MODEL_NUMBER,ITEM_SERIAL_NUMBER,ITEM_CLASSIFICATION_NUMBER_in_ERP,ITEM_CLASSIFICATION_DESCRIPTION_in_ERP,CURRENCY_of_ITEM_in_PO,ITEM_UNIT_PRICE_in_PO,ITEM_TOTAL_PRICE_in_PO,VAT_PERCENT_PER_ITEM_in_PO,PROJECT_NAME_referring_to_ordered_item,PROJECT_NUMBER_referring_to_ordered_item,BUDGET_NUMBER_for_ordered_item,CURRENCY_for_total_sum_in_PO,TOTAL_VAT_EXEPMPT_SUM_in_PO,TOTAL_VAT_CHARCHABLE_SUM_in_PO,TOTAL_VAT_SUM_in_PO,TOTAL_SUM_EXCLUDING_VAT_in_PO,ORDERER_COMPANY_ID_IN_ERP,ORDERER_NAME,ORDERER_PHONE,ORDERER_EMAIL,PO_APPROVER_ID,PO_APPROVER_PHONE,PO_APPROVER_EMAIL,PO_PAYMENT_TERMS,PQ_NUMBER,ITEM_NUMBER_in_PQ,ITEM_DATE_in_PQ,ITEM_QUANTITY_in_PQ,PACKAGES_QUANTITY_PER_ITEM_in_PQ,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PQ,CURRENCY_of_ITEM_in_PQ,ITEM_UNIT_PRICE_in_PQ,ITEM_TOTAL_PRICE_in_PQ,VAT_PERCENT_PER_ITEM_in_PQ,Bill_Of_Lading_NUMBER,ITEM_NUMBER_in_Bill_Of_Lading,ITEM_DATE_in_Bill_Of_Lading,ITEM_QUANTITY_in_Bill_Of_Lading,IMPORT_FILE_NUMBER,IMPORT_ENTRY_Number,ITEM_NUMBER_in_IMPORT_ENTRY,ITEM_DATE_in_IMPORT_ENTRY,ITEM_QUANTITY_in_IMPORT_ENTRY,IMPORT_AGENCY_Name,IMPORT_AGENCY_ID_IN_ERP,IMPORT_AGENCY_FILE_NUMBER,IMPORT_TAXES_PAID_EXCLUDING_VAT,IMPORT_VAT_PAID,CURRENCY_CONVERSION_RATE_between_PO_and_INVOICE,AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO,Reported_INVOICE_NUMBER,Reported_INVOICE_DATE,Reported_ITEM_NUMBER_in_INVOICE,Reported_ITEM_CATNO_in_INVOICE,Reported_ITEM_DESCRIPTION_in_INVOICE,Reported_ITEM_INTERNAL_COUNTER_in_INVOICE,Reported_CURRENCY_of_item_in_INVOICE,Reported_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Reported_ITEM_QUANTITY_in_INVOICE,Reported_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Reported_ITEM_VAT_SUM_in_INVOICE,Verified_PROFORMA_INVOICE_NUMBER,Verified_PROFORMA_INVOICE_DATE,Verified_ITEM_NUMBER_in_PROFORMA_INVOICE,Verified_ITEM_CATNO_in_PROFORMA_INVOICE,Verified_ITEM_DESCRIPTION_in_PROFORMA_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_PROFORMA_INVOICE,Verified_CURRENCY_of_item_in_PROFORMA_INVOICE,Verified_ITEM_UNIT_PRICE_in_PROFORMA_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_PROFORMA_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_PROFORMA_INVOICE,Verified_ITEM_VAT_SUM_in_PROFORMA_INVOICE,Verified_PROFORMA_INVOICE_ID_IN_ERP,Verified_PROFORMA_INVOICE_FILENAME,Verified_INVOICE_NUMBER,Verified_INVOICE_DATE,Verified_ITEM_NUMBER_in_INVOICE,Verified_ITEM_CATNO_in_INVOICE,Verified_ITEM_DESCRIPTION_in_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_INVOICE,Verified_CURRENCY_of_item_in_INVOICE,Verified_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Verified_ITEM_VAT_SUM_in_INVOICE,Verified_INVOICE_ID_IN_ERP,Verified_INVOICE_FILENAME,ITEM_PAYMENT_APPROVER_NAME,ITEM_PAYMENT_APPROVER_PHONE,ITEM_PAYMENT_APPROVER_EMAIL,PREAPPROVED_PAYMENT_INSTRUCTION_NUMBER,PREAPPROVED_PAYMENT_INSTRUCTION_DATE,APPROVED_PAYMENT_INSTRUCTION_NUMBER,APPROVED_PAYMENT_INSTRUCTION_DATE,REMAIDER_BUDGET_EXCLUDING_VAT,REMAINDER_BUDGET_INCLUDING_VAT" + "\r\n");
            os.write("Supply_Certificate,ITEM_NUMBER_in_Supply_Certificate,ITEM_DATE_in_Supply_certificate,ITEM_QUANTITY_in_Supply_Certificate,VERIFIER_ID_of_the_Supply_Certificate,VERIFIER_PHONE_in_the_Supply_Certificate,VERIFIER_EMAIL_of_the_Supply_Certificate,PO_NUMBER,ITEM_NUMBER_in_PO,ITEM_DATE_in_PO,ITEM_QUANTITY_in_PO,PACKAGES_QUANTITY_PER_ITEM_in_PO,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PO,SUPPLIER_ID_IN_ERP,Customer_ITEM_CATNO,Supplier_ITEM_CATNO,Manufacturer_ITEM_CATNO,ITEM_DESCRIPTION_in_PO,ITEM_MODEL_NUMBER,ITEM_SERIAL_NUMBER,ITEM_CLASSIFICATION_NUMBER_in_ERP,ITEM_CLASSIFICATION_DESCRIPTION_in_ERP,CURRENCY_of_ITEM_in_PO,ITEM_UNIT_PRICE_in_PO,ITEM_TOTAL_PRICE_in_PO,VAT_PERCENT_PER_ITEM_in_PO,PROJECT_NAME_referring_to_ordered_item,PROJECT_NUMBER_referring_to_ordered_item,BUDGET_NUMBER_for_ordered_item,CURRENCY_for_total_sum_in_PO,TOTAL_VAT_EXEPMPT_SUM_in_PO,TOTAL_VAT_CHARGEABLE_SUM_in_PO,TOTAL_VAT_SUM_in_PO,TOTAL_SUM_EXCLUDING_VAT_in_PO,ORDERER_COMPANY_ID_IN_ERP,ORDERER_NAME,ORDERER_PHONE,ORDERER_EMAIL,PO_APPROVER_ID,PO_APPROVER_PHONE,PO_APPROVER_EMAIL,PO_PAYMENT_TERMS,PQ_NUMBER,ITEM_NUMBER_in_PQ,ITEM_DATE_in_PQ,ITEM_QUANTITY_in_PQ,PACKAGES_QUANTITY_PER_ITEM_in_PQ,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PQ,CURRENCY_of_ITEM_in_PQ,ITEM_UNIT_PRICE_in_PQ,ITEM_TOTAL_PRICE_in_PQ,VAT_PERCENT_PER_ITEM_in_PQ,Bill_Of_Lading_NUMBER,ITEM_NUMBER_in_Bill_Of_Lading,ITEM_DATE_in_Bill_Of_Lading,ITEM_QUANTITY_in_Bill_Of_Lading,IMPORT_FILE_NUMBER,IMPORT_ENTRY_Number,ITEM_NUMBER_in_IMPORT_ENTRY,ITEM_DATE_in_IMPORT_ENTRY,ITEM_QUANTITY_in_IMPORT_ENTRY,IMPORT_AGENCY_Name,IMPORT_AGENCY_ID_IN_ERP,IMPORT_AGENCY_FILE_NUMBER,IMPORT_TAXES_PAID_EXCLUDING_VAT,IMPORT_VAT_PAID,CURRENCY_CONVERSION_RATIO_between_PO_and_INVOICE,AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO,Reported_INVOICE_NUMBER,Reported_INVOICE_DATE,Reported_ITEM_NUMBER_in_INVOICE,Reported_ITEM_CATNO_in_INVOICE,Reported_ITEM_DESCRIPTION_in_INVOICE,Reported_ITEM_INTERNAL_COUNTER_in_INVOICE,Reported_CURRENCY_of_item_in_INVOICE,Reported_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Reported_ITEM_QUANTITY_in_INVOICE,Reported_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Reported_ITEM_VAT_SUM_in_INVOICE,Verified_PROFORMA_INVOICE_NUMBER,Verified_PROFORMA_INVOICE_DATE,Verified_ITEM_NUMBER_in_PROFORMA_INVOICE,Verified_ITEM_CATNO_in_PROFORMA_INVOICE,Verified_ITEM_DESCRIPTION_in_PROFORMA_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_PROFORMA_INVOICE,Verified_CURRENCY_of_item_in_PROFORMA_INVOICE,Verified_ITEM_UNIT_PRICE_in_PROFORMA_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_PROFORMA_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_PROFORMA_INVOICE,Verified_ITEM_VAT_SUM_in_PROFORMA_INVOICE,Verified_PROFORMA_INVOICE_ID_IN_ERP,Verified_PROFORMA_INVOICE_FILENAME,Verified_INVOICE_NUMBER,Verified_INVOICE_DATE,Verified_ITEM_NUMBER_in_INVOICE,Verified_ITEM_CATNO_in_INVOICE,Verified_ITEM_DESCRIPTION_in_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_INVOICE,Verified_CURRENCY_of_item_in_INVOICE,Verified_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Verified_ITEM_VAT_SUM_in_INVOICE,Verified_INVOICE_ID_IN_ERP,Verified_INVOICE_FILENAME,ITEM_PAYMENT_APPROVER_NAME,ITEM_PAYMENT_APPROVER_PHONE,ITEM_PAYMENT_APPROVER_EMAIL,PREAPPROVED_PAYMENT_INSTRUCTION_NUMBER,PREAPPROVED_PAYMENT_INSTRUCTION_DATE,APPROVED_PAYMENT_INSTRUCTION_NUMBER,APPROVED_PAYMENT_INSTRUCTION_DATE,REMAIDER_BUDGET_EXCLUDING_VAT,REMAINDER_BUDGET_INCLUDING_VAT,BUDGET_CURRENCY,ACCOUNTING_TRANSACTION_TYPE,ACCOUNTING_COSTING_TYPE,ACCOUNTING_BATCH_NUMBER" + "\r\n");
            if (file.exists()) {
                // orderedItemsCSV
                for (int i = 0; i < orderedItemsCSV.size(); i++) {
                    for (int j = 0; j < orderedItemsCSV.elementAt(i).length; j++) {
                        String t = orderedItemsCSV.elementAt(i)[j];
                        if (t == null) {
                            t = "";
                        }

                        if (j != orderedItemsCSV.elementAt(i).length - 1) {
                            os.write(t + ",");
                        } else {
                            os.write(t);
                        }

                    }
                    String s = "";
                    if (orderedItemsCSV.elementAt(i).length == 114) {
                        s = "," + SaveLastFields.getLastFields(orderedItemsCSV.elementAt(i)[13] + "," + orderedItemsCSV.elementAt(i)[14]);
                    }
                    os.write(s + "\r\n");
                }
                os.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean SaveData(File path) { //jpg
        String orig_name = "";
//create name
        String[] filename = {
            "@;", //1
            "@;", //2
            "@;", //3
            "@;", //4
            "@;", //5
            "@;", //6
            "@;", //7
            "@;", //8
            "@;", //9
            "@;", //10
            "@;", //11
            "@;", //12
            "@;", //13
            "@;", //14
            "@;", //15
            "@;", //16
            "@;", //17
            "@;", //18
            "@;", //19
            "@;", //20
            "@;", //21
            "@"};//22  new String[22];
        orig_name = path.getName().replace(".jpg", "");
        int index[] = new int[6], index_count = 1;

        try {

            for (int i = 0; i < orig_name.length(); i++) {
                if (orig_name.charAt(i) == ';') {
                    index[index_count] = i;
                    index_count++;
                }
            }
            index_count = 0;
            filename[0] = orig_name.substring(index[index_count], index[index_count + 1]) + ";";
            index_count++;
            filename[1] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //2 מספר הלקוח הרלבנטי במערכת הממוחשבת ומימינו הסימן
            index_count++;
            filename[2] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //3 התאריך בו צולמה התמונה בפורמט DD.MM.YYYY ומימינו הסימן
            index_count++;
            filename[3] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //4 הזמן בו צולמה התמונה בפורמט HHMMSS ומימינו הסימן
            index_count++;
            filename[4] = orig_name.substring(index[index_count] + 1, index[index_count + 1]) + ";"; //5 הנ.צ. (GPS) של המיקום בו צולמה התמונה ומימינו הסימן
            index_count++;
            filename[5] = orig_name.substring(index[index_count] + 1, orig_name.length()) + ";"; //6 קידומת כלשהי שתציין את סוג המכשיר ממנו נשלחה התמונה (למשל, GX2).

            File dir = new File(Path.getOutputFolder());
            String f = "";
            try {
                for (int i = 0; i < filename.length; i++) {
                    f = f + filename[i];

                }
                //create file
                //new File("output").mkdir();

                dir.mkdir();
            } catch (Exception exception) {
                exception.getMessage();
            }

            File data = new File(dir.getPath() + "\\" + f + ".csu");
            try {
                data.createNewFile();

            } catch (Exception exception) {
                exception.getMessage();
            }


            /*
             //This log checking the sumallNoMamv, sumallMamv, sumallWithMamv
             inon_log();
             */
        } catch (Exception r) {
            write(orig_name, "CSV.SaveData", "can't save data \"saveData\"", "", true);
        }

        return true;
    }

}
