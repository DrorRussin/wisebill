/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import Constructors.ObData;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author Zvika-ERA
 */
public class ObDataWithLines {

    HashMap<ObData, int[]> SeparatorLines = new HashMap<ObData, int[]>();
    private int size = 0;

    ObDataWithLines(Vector<ObData> result, CSV2Hash _CSV2Hash) {
        for (int i = 0; i < result.size(); i++) {
            Vector<int[]> linesBetweenObData = new Vector<int[]>();
            if ((i + 1 < result.size() && result.get(i).get_pfile_num() < result.get(i + 1).get_pfile_num())
                    //must be line beetwen all the result
                    || size != i
                    //not the last item
                    || i == result.size() - 1) {
                continue;
            }

            for (int j = 0; j < _CSV2Hash.hrzCoordinate.size(); j++) {
                int line1 = (result.get(i).get_bottom() + result.get(i).get_top()) / 2;
                int line2;
                if (i == result.size() - 1) {
                    line2 = Integer.MAX_VALUE;
                } else {
                    line2 = (result.get(i + 1).get_bottom() + result.get(i + 1).get_top()) / 2;
                }

                //if Between the two lines:
                if (_CSV2Hash.hrzCoordinate.get(j)[4] == result.get(i).get_pfile_num()
                        && _CSV2Hash.hrzCoordinate.get(j)[1] > line1
                        && _CSV2Hash.hrzCoordinate.get(j)[3] < line2
                        && twoLineMeet(result.get(i).get_left(), result.get(i).get_right(), _CSV2Hash.hrzCoordinate.get(j)[0], _CSV2Hash.hrzCoordinate.get(j)[2])) {
                    linesBetweenObData.add(_CSV2Hash.hrzCoordinate.get(j));
                    System.out.println("ObDataWithLines, " + i + " _CSV2Hash.hrzCoordinate.get(" + j + ")" + Arrays.toString(_CSV2Hash.hrzCoordinate.get(j)));
                }
            }
            System.out.println("ObDataWithLines, " + i + " linesBetweenObData.size=" + linesBetweenObData.size());

            if (result.size() - 1 == i && size == 0) {
                return;
            }

            if ((result.size() - 1 != i && linesBetweenObData.size() == 1)
                    || (result.size() - 1 == i && SeparatorLines.size() > 0)) {
                SeparatorLines.put(result.get(i), linesBetweenObData.get(0));
                size++;
            } else if (linesBetweenObData.size() > 1) {
                //Apparently between the lines there is an unspecified amount!! - need to fix it.
            }
        }
        
    }

    public static boolean twoLineMeet(int L_start, int L_end, int P_start, int P_end) {
        if (L_end < P_start || P_end < L_start) {
            return false;
        } else {
            return true;
        }

        //return L_end > P_start && L_end < P_end || P_end > L_start && P_end < L_end;
    }

    public static boolean twoLineMeet(int L_start, int L_end, int P_start, int P_end, int diff) {
        if (L_end + diff < P_start || P_end + diff < L_start) {
            return false;
        } else {
            return true;
        }

        //return L_end > P_start && L_end < P_end || P_end > L_start && P_end < L_end;
    }

    public int[] getSeparatorLine(ObData O) {
        try {
            return (int[])SeparatorLines.get(O);
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
        
    }

    public int getSize() {
        return size;
    }
}
