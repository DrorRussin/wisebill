/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import Constructors.BNode;
import Constructors.InfoReader;
import Constructors.ObData;
import Constructors.VerVar;
import Constructors.VerVarWM;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author YAEL-MOBILE
 */
public class MatchesToOneItem {

    static String SupplierId = null;
    BNode block = null;
    Vector<Match> matches = new Vector<Match>();
    public boolean matches_is_fictitious = false;
    private Vector<String[]> makatcsu = new Vector<String[]>();
    private boolean matchToOrderedItemsCSVbySupplierID = false;
    public boolean matchToOrderedItemsCSVbyCSVOneLine = false;
    public boolean matchToOrderedItemsCSVbyCSVManyLine = false;

    private Vector<String[]> itemdesccsu = new Vector<String[]>();
    private Vector<String[]> unitpricecsu = new Vector<String[]>();
    private Vector<String[]> notcatnocsu = new Vector<String[]>();
    private Vector<String[]> connectToPreviousInvoice = new Vector<String[]>(); // ווקטור שמכיל את הרשומות ששיכות לחשבונית קודמת
    private Vector<Match> connectToJustPreviousPerformalInvoice = new Vector<Match>(); //וקטור שמכיל את כל הרשומות ששייכות כבר לחשבונית עסקה ואך ורק חשבונית עסקה.
    private Vector<Match> connectToPreviousPerformalInvoice = new Vector<Match>(); // וקטור שמכיל את כל הרשומות ששייכות לחשבונית עסקה (ואולי גם לחשבונית מס)
    private Vector<Match> connectToPrevOrder = new Vector<Match>(); //וקטור שמכיל את כל הרשומות ששייכות כבר להזמנות קודמות
    private Vector<Match> connectToPrevMish = new Vector<Match>(); // וקטור שמכיל את כל הרשומות ששייכות כבר לתעודות משלוח קודמות
    private Vector<Match> dontHaveSupakDtl = new Vector<Match>(); // וקטור שמכיל את כל הרשומות שאין בהם נתונים לגבי מה סופק בפועל
    Vector<String[]> orderedItemsCSV = new Vector<String[]>();
    double tolerance = 0.01;
    private Vector<Integer> INDEX_OF_PRICE = new Vector<Integer>();
    private Vector<Integer> INDEX_OF_DESCRIPTION = new Vector<Integer>();
    private Vector<Integer> INDEX_OF_QTY = new Vector<Integer>();
    private int supak = 0;
    Vector<Integer> INDEX_OF_PN = new Vector<Integer>();
    InfoReader infoReader;
    String PoNumbers = ""; // מספרי הזמנות ששייכים לפריט
    String SupplyCertificates = ""; // //מספרי תעודת כניסה ששייכים לפריט בחשבונית
    String LadingNumber = ""; // // מספרי תעודות משלוח ששייכים לפריט בחשבונית
    String proformaInvoiceNumber = ""; //מספר חשבונית העסקה אם הוגשה לפני החשבונית.
    double leftToSupply = 0;
    double diffrenceInPriceBetweenHazmanaAndInvoice = 0;
    double diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice = 0;
    double diffrenceInQtyBetweenSupakInLadingAndInvoice = 0;
    double diffrenceInQtyBetweenSupplierAndInvoice = 0;
    double diffrenceInPriceBetweenSupplierAndInvoice = 0;
    //double diffrenceInPriceBetweenProformaInvoiceAndInvoice = 0; //ההפרש בין המחיר ליחידה בחשבונית וזה הרשום בחשבונית העסקה
    //double diffrenceInQtyBetweenProformaInvoiceAndInvoice = 0; //ההפרש בין הכמות בחשבונית לכמות בחשבונית העסקה (אם הוגשה).
    double BalanceOfBudget = 0;
    double waitForInvoice = 0;
    boolean findSupplierPrice = false;
    boolean findSupplierQty = false;
    boolean foundProformalInvoice = false;
    boolean foundMatches = false;
    boolean foundPartialMatchToPN = false;
    boolean foundPartialMatchToDES = false;
    boolean foundFullyCreditMatch = false;
    boolean isCreditInvoice = false;
    boolean isProformalInvoice = false;
    boolean allConnectToPrevInvoice = false;
    boolean isMISHFile = false;
    boolean isPORDFile = false;
    boolean isINVO = false;
    private boolean isBeenArchived;
    public boolean isCreditMISHFile = false;
    public boolean isCreditPORDFile = false;
    boolean balanceOfBudgetNotCalculate = false;
    private boolean findMatchWithoutSupak = false;
    double budgetForAllorders = 0;
    private VerVarWM globalanahav;
    private VerVar sumallNoMamv;
    private VerVarWM igulv;
    private int numberOfItems = 0;

    public MatchesToOneItem(BNode i_Block, Vector<String[]> i_makatcsu, Vector<String[]> i_itemdesccsu,
            Vector<String[]> i_unitpricecsu, Vector<String[]> i_notcatnocsu, Vector<String[]> i_OrderedItemsCSV, double i_Tolerance,
            Vector<Integer> i_INDEX_OF_PRICE, Vector<Integer> i_INDEX_OF_DESCRIPTION, Vector<Integer> i_INDEX_OF_PN, Vector<Integer> i_INDEX_OF_QTY, InfoReader i_InfoReader, boolean i_BeenArchived,
            VerVarWM i_Globalanahav, VerVar i_SumallNoMamv, int i_NumberOfItems, VerVarWM i_Igulv, boolean i_isINVO, boolean i_IsCreditInvoice, boolean i_IsPfoformalInvoice,
            boolean i_isMISHFile, boolean i_isPORDFile, boolean i_isCreditMISHFile, boolean i_isCreditPORDFile) {
        this.block = i_Block;
        this.makatcsu = i_makatcsu;
        this.itemdesccsu = i_itemdesccsu;
        this.unitpricecsu = i_unitpricecsu;
        this.notcatnocsu = i_notcatnocsu;
        this.orderedItemsCSV = i_OrderedItemsCSV;
        this.tolerance = i_Tolerance;
        this.INDEX_OF_PRICE = i_INDEX_OF_PRICE;
        this.INDEX_OF_DESCRIPTION = i_INDEX_OF_DESCRIPTION;
        this.INDEX_OF_PN = i_INDEX_OF_PN;
        this.INDEX_OF_QTY = i_INDEX_OF_QTY;
        this.infoReader = i_InfoReader;
        this.globalanahav = i_Globalanahav;
        this.sumallNoMamv = i_SumallNoMamv;
        this.numberOfItems = i_NumberOfItems;
        this.igulv = i_Igulv;
        this.isCreditInvoice = i_IsCreditInvoice;
        this.isProformalInvoice = i_IsPfoformalInvoice;
        this.isMISHFile = i_isMISHFile;
        this.isPORDFile = i_isPORDFile;
        this.isCreditMISHFile = i_isCreditMISHFile;
        this.isCreditPORDFile = i_isCreditPORDFile;
        this.isINVO = i_isINVO;
        this.isBeenArchived = i_BeenArchived;
    }

    public enum MatchType {
        Regular,
        SingleLineForAllItems,
        ManyLinesToManyLines,
        ManyLinesToManyLinesNoSupp;
    }

    public void findMatch(MatchType _match_type) {
        //אם החשבונית לא אורכבה כבר
        if (!isBeenArchived) {
            //חיפוש התאמות לבלוק הנוכחי

            switch (_match_type) {
                case Regular:
                    matchBlockToCSv();
                    break;
                case SingleLineForAllItems:
                    matchBlockToCSvOneLine(_match_type);
                    break;
                case ManyLinesToManyLines:
                    matchBlockToCSvOneLine(_match_type);
                    break;
                case ManyLinesToManyLinesNoSupp:
                    matchBlockToCSvOneLine(_match_type);
                    break;
            }
        }
    }

    public void fillAllDetails() {
        //אם החשבונית לא אורכבה כבר
        if (!isBeenArchived) {
            try {
                Collections.sort(this.matches);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //בדיקה  כמה נשאר לי לספק מהפריט
            fillLeftToSupply();
            //בדיקה איפה אין נתונים לגבי מה שסופק בפועל תעודת משלוח והזמנה
            checkWhichDontHaveSupakOrOrderOrMishDtl();
            //השלמת הווקטורים
            FillVectors();
            //בדיקה כמה התאמות מחכות לחשבונית המס
            if (isINVO) {
                checkHowManyWaitForInvoice();
            }
            // בדיקה האם קיימת רשומת זיכוי שמתאימה לחלוטין לפריט
            checkIfThereIsFullyCreditMatch();
            //בדיקה האם כל הרשומות שייכות לחשבונית מס קודמת
            checkIfAllConnectToPrevInvoice();
            //חיפוש מי באמת מתאים לחשבונית
            //searchWhichMatchToItem();

            //מספרי הזמנות, מספרי תעודות משלוח, מספרי תעודות כניסה, יתרת התקציב של ההזמנות, הפרש בין הכמות בחשבונית לבין הכמות שסופקה
            //fillDetails();
        }
    }

    public void removeMatches() {
        this.matches.clear();
        connectToPreviousInvoice.clear();
        connectToJustPreviousPerformalInvoice.clear();
        connectToPreviousPerformalInvoice.clear();
        connectToPrevOrder.clear();
        connectToPrevMish.clear();
        dontHaveSupakDtl.clear();
        supak = 0;
        PoNumbers = ""; // מספרי הזמנות ששייכים לפריט
        SupplyCertificates = ""; // //מספרי תעודת כניסה ששייכים לפריט בחשבונית
        LadingNumber = ""; // // מספרי תעודות משלוח ששייכים לפריט בחשבונית
        proformaInvoiceNumber = ""; //מספר חשבונית העסקה אם הוגשה לפני החשבונית.
        leftToSupply = 0;
        diffrenceInPriceBetweenHazmanaAndInvoice = 0;
        diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice = 0;
        diffrenceInQtyBetweenSupakInLadingAndInvoice = 0;
        diffrenceInQtyBetweenSupplierAndInvoice = 0;
        diffrenceInPriceBetweenSupplierAndInvoice = 0;
        BalanceOfBudget = 0;
        waitForInvoice = 0;
        findSupplierPrice = false;
        findSupplierQty = false;
        foundProformalInvoice = false;
        foundMatches = false;
        foundPartialMatchToPN = false;
        foundPartialMatchToDES = false;
        foundFullyCreditMatch = false;
        balanceOfBudgetNotCalculate = false;
        findMatchWithoutSupak = false;
        budgetForAllorders = 0;
    }

    public void searchWhichMatchToItem() {
        Vector<Vector<String[]>> sameSupplyCertificate = createVectorOfSameSupplyCertificateWithPrevInvoiceOrMishOrOrder();
        double qtyInPrevInvoices = 0;
        double qtySupakPrevInvoices = 0;
        Vector<String[]> vInvoices = new Vector<String[]>();
        int indexOfLast = 0;
        boolean isAdded = false;
        for (int i = 0; i < sameSupplyCertificate.size(); i++) {
            for (int j = 0; j < sameSupplyCertificate.elementAt(i).size(); j++) {
                if (!isContainInvoice(vInvoices, sameSupplyCertificate.elementAt(i).elementAt(j), 103, 115) && !isCreditInvoice && !isProformalInvoice && !isMISHFile && !isPORDFile && !isCreditMatch(sameSupplyCertificate.elementAt(i).elementAt(j))) {
                    vInvoices.add(sameSupplyCertificate.elementAt(i).elementAt(j));
                    //111 - כמות בחשבונית כפי שזוהה
                    qtyInPrevInvoices += Double.valueOf(sameSupplyCertificate.elementAt(i).elementAt(j)[111]);
                } else if (!isContainInvoice(vInvoices, sameSupplyCertificate.elementAt(i).elementAt(j), 103, 115) && isCreditInvoice && isCreditMatch(sameSupplyCertificate.elementAt(i).elementAt(j))) {
                    vInvoices.add(sameSupplyCertificate.elementAt(i).elementAt(j));
                    //111 - כמות בחשבונית כפי שזוהה
                    qtyInPrevInvoices += Double.valueOf(sameSupplyCertificate.elementAt(i).elementAt(j)[111]);
                } else if (!isContainInvoice(vInvoices, sameSupplyCertificate.elementAt(i).elementAt(j), 90, 102) && isProformalInvoice) {
                    vInvoices.add(sameSupplyCertificate.elementAt(i).elementAt(j));
                    //98 - כמות בחשבונית עסקה כפי שזוהה
                    qtyInPrevInvoices += Double.valueOf(sameSupplyCertificate.elementAt(i).elementAt(j)[98]);
                } else if (!isContainInvoice(vInvoices, sameSupplyCertificate.elementAt(i).elementAt(j), 63, 66) && isMISHFile) {
                    vInvoices.add(sameSupplyCertificate.elementAt(i).elementAt(j));
                    //66 - כמות בתעודת משלוח קודמת כפי שזוהה
                    qtyInPrevInvoices += Double.valueOf(sameSupplyCertificate.elementAt(i).elementAt(j)[66]);
                } else if (!isContainInvoice(vInvoices, sameSupplyCertificate.elementAt(i).elementAt(j), 18, 52) && isPORDFile) {
                    vInvoices.add(sameSupplyCertificate.elementAt(i).elementAt(j));
                    //21 - כמות בהזמנה קודמת כפי שזוהה
                    qtyInPrevInvoices += Double.valueOf(sameSupplyCertificate.elementAt(i).elementAt(j)[21]);
                }
                if (!isAdded && ((isCreditInvoice && isCreditMatch(sameSupplyCertificate.elementAt(i).elementAt(j))) || (!isCreditInvoice && !isProformalInvoice && !isMISHFile && !isPORDFile && !isCreditMatch(sameSupplyCertificate.elementAt(i).elementAt(j))) || isProformalInvoice || isMISHFile || isPORDFile)) {
                    qtySupakPrevInvoices += Double.valueOf(sameSupplyCertificate.elementAt(i).elementAt(j)[14]);
                    isAdded = true;
                    indexOfLast = i;
                }
            }

            isAdded = false;
        }

        // כמה פריטים נותרו לקבל עליהם חשבונית
        double qtyLeft = qtySupakPrevInvoices - qtyInPrevInvoices;
        double qtyInInvoice = 0;

        if (block.qty != null) {
            try {
                qtyInInvoice = Double.valueOf(block.qty.get_result_str());
                if (isMISHFile || isPORDFile) {
                    qtyInInvoice = searchWhichMatchMishAndPordFile(qtyLeft, qtyInInvoice, sameSupplyCertificate, indexOfLast);
                } else if (isCreditMISHFile || isCreditPORDFile) {
                    qtyInInvoice = searchWhichMatchCreditMishAndCreditPordFile(qtyLeft, qtyInInvoice, sameSupplyCertificate, indexOfLast);
                } else if (waitForInvoice <= qtyInInvoice && qtyInInvoice > 0 && waitForInvoice > 0 && isINVO) {
                    qtyInInvoice = searchWhichMatchInvoFileFirstCase(qtyLeft, qtyInInvoice, sameSupplyCertificate, indexOfLast);
                } else if (waitForInvoice > qtyInInvoice && qtyInInvoice > 0 && waitForInvoice > 0 && isINVO) {
                    qtyInInvoice = searchWhichMatchInvoFileSecondCase(qtyLeft, qtyInInvoice, sameSupplyCertificate, indexOfLast);
                } else if (isCreditInvoice /* אם זה חשבונית זיכוי */) {
                    qtyInInvoice = searchWhichMatchCreditInvoice(qtyLeft, qtyInInvoice, sameSupplyCertificate, indexOfLast);
                } else if (isProformalInvoice) {
                    qtyInInvoice = searchWhichMatchProformalInvoice(qtyLeft, qtyInInvoice, sameSupplyCertificate, indexOfLast);
                } else if (matchToOrderedItemsCSVbySupplierID) {

                } else {
                    setAllMatchesNotConnectToInvoice();
                }
            } catch (Exception e) {

            }

            fillInMatchesWithoutSupak(qtyInInvoice);
        }

        setFoundMatches();
    }

    public void fillDetails() {
        if (block.qty != null && foundMatches) {
            fillPoNumbersSupplyCertificatesLadingNumbers();
            if (!findMatchWithoutSupak && !isMISHFile && !isPORDFile && !isCreditMISHFile && !isCreditPORDFile) {
                fillDiffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice();
                fillDiffrenceInQtyBetweenSupakInLadingAndInvoice();
            }
            if (!isMISHFile && !isPORDFile && !isCreditMISHFile && !isCreditPORDFile) {
                fillDiffrenceInQtyBetweenSupplierAndInvoice();
                fillDiffrenceInPriceBetweenSupplierAndInvoice();
            }

            fillProformaInvoiceDetails(); // חישוב הפרשים בין החשבונית הנוכחית לחשבונית עסקה שהוגשה קודם
        }
    }

    public void updateAndFillBalanceOfBudget() {
        if (block.qty != null && foundMatches && !isMISHFile && !isPORDFile && !isCreditMISHFile && !isCreditPORDFile) {
            updateBalanceOfBudgetInMatches();
            if (!foundProformalInvoice /* אם נמצאה חשבונית עסקה שעודכנה לפני חשבונית המס אז לא נעדכן שוב את יתרת התקציב*/) {
                fillBalanceOfBudget();
            } else {
                for (int i = 0; i < matches.size(); i++) {
                    if (matches.elementAt(i).get_connect()) {
                        try {
                            BalanceOfBudget += Double.valueOf(matches.elementAt(i).match[123]);
                        } catch (Exception e) {

                        }
                    }
                }
            }
        } else {
            balanceOfBudgetNotCalculate = true;
        }
    }

    private void checkWhichDontHaveSupakOrOrderOrMishDtl() {
        for (int i = 0; i < matches.size(); i++) {
            if (matches.elementAt(i).match[14].equals("")) {
                matches.elementAt(i).set_dontHaveSupak(true);
                matches.elementAt(i).set_connect(false);
                dontHaveSupakDtl.add(matches.elementAt(i));
            }
            if (!isHaveDetails(matches.elementAt(i).match, 18, 23)) {
                matches.elementAt(i).set_dontHaveOrderDtl(true);
            }
            if (!isHaveDetails(matches.elementAt(i).match, 63, 66)) {
                matches.elementAt(i).set_dontHaveMishDtl(true);
            }
        }
    }

    private void checkIfAllConnectToPrevInvoice() {
        if (isProformalInvoice) {
            for (int i = 0; i < matches.size(); i++) {
                if (!matches.elementAt(i).get_connectToPrevious()) {
                    allConnectToPrevInvoice = false;
                    break;
                } else if (matches.elementAt(i).get_connectToPrevious() && matches.size() - 1 == i) {
                    allConnectToPrevInvoice = true;
                }
            }
        }
    }

    private void updateBalanceOfBudgetInMatches() {
        int index = 0;
        for (int i = 0; i < matches.size(); i++) {
            String[] s = new String[matches.elementAt(i).match.length - 11];
            for (int j = 0; j < s.length; j++) {
                s[j] = matches.elementAt(i).match[j + 11];
            }
            index = getIndexInOrderedItemsCSV(orderedItemsCSV, s);
            if (index > 0) {
                matches.elementAt(i).match[123] = orderedItemsCSV.elementAt(index)[112];
                matches.elementAt(i).match[124] = orderedItemsCSV.elementAt(index)[113];
            }
        }
    }

    private void fillProformaInvoiceDetails() {
        for (int i = 0; i < matches.size(); i++) {
            if (!matches.elementAt(i).match[90].equals("") && !matches.elementAt(i).match[97].equals("") && !matches.elementAt(i).match[98].equals("") && matches.elementAt(i).get_connect() == true && !isProformalInvoice) {
                proformaInvoiceNumber = matches.elementAt(i).match[90];
                foundProformalInvoice = true;
                /*try{
                 Double proformaInvoicePrice = Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(matches.elementAt(i).match[97])));
                 Double invoicePrice = Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(block.price.get_result_str())));
                 diffrenceInPriceBetweenProformaInvoiceAndInvoice = invoicePrice - proformaInvoicePrice;
                 }catch(Exception e){
                   
                 }
               
                 try{
                 Double proformaInvoiceQty = Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(matches.elementAt(i).match[98])));
                 Double invoiceQty = Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(block.qty.get_result_str())));
                 diffrenceInPriceBetweenProformaInvoiceAndInvoice = invoiceQty - proformaInvoiceQty;
                 }catch(Exception e){
                   
                 }*/
                break;
            }
        }
    }

    private void fillDiffrenceInQtyBetweenSupplierAndInvoice() {

        double qtyInInvoice = 0;
        double qtySupplier = 0;
        if (infoReader != null) {
            if (block.qty != null && infoReader.Invoice_ID12 != null && !infoReader.Invoice_ID12.equals("@")) {
                try {
                    qtyInInvoice = Double.valueOf(block.qty.get_result_str());
                    if (isCreditInvoice && qtyInInvoice > 0) {
                        qtyInInvoice *= (-1);
                    }
                } catch (Exception e) {

                }

                for (Match m : matches) {
                    if (!m.match[79].equals("") && infoReader.Invoice_ID12.equals(m.match[79])) {
                        try {
                            qtySupplier = Double.valueOf(m.match[87]);
                            findSupplierQty = true;
                            break;
                        } catch (Exception e) {

                        }
                    }
                }

                diffrenceInQtyBetweenSupplierAndInvoice = qtyInInvoice - qtySupplier;
            }
        }
    }

    private void fillDiffrenceInPriceBetweenSupplierAndInvoice() {

        double priceInInvoice = 0;
        double priceSupplier = 0;
        if (block.one_price != null && infoReader != null && infoReader.Invoice_ID12 != null && !infoReader.Invoice_ID12.equals("@")) {
            try {
                priceInInvoice = Double.valueOf(block.one_price.get_result_str());
            } catch (Exception e) {

            }

            for (Match m : matches) {
                if (!m.match[79].equals("") && infoReader.Invoice_ID12.equals(m.match[79])) {
                    try {
                        priceSupplier = Double.valueOf(m.match[88]);
                        findSupplierPrice = true;
                        break;
                    } catch (Exception e) {

                    }
                }
            }

            diffrenceInPriceBetweenSupplierAndInvoice = priceInInvoice - priceSupplier;
        }
    }

    private void fillDiffrenceInQtyBetweenSupakInLadingAndInvoice() {
        if (block.qty != null) {
            // סך הכמויות שהיו בחשבוניות קודמות
            double qtyInInvoices = 0;
            Vector<String[]> vInvoices1 = new Vector<String[]>();
            Vector<String[]> vInvoices = new Vector<String[]>();
            for (String[] matchStr : connectToPreviousInvoice) {
                try {
                    if (!isContainInvoice(vInvoices1, matchStr, 103, 115) && !isCreditInvoice && !isCreditMatch(matchStr)) {
                        vInvoices1.add(matchStr);
                        //111 - כמות בחשבונית כפי שזוהה
                        qtyInInvoices += Double.valueOf(matchStr[111]);
                    } else if (!isContainInvoice(vInvoices1, matchStr, 103, 115) && isCreditInvoice && isCreditMatch(matchStr)) {
                        vInvoices1.add(matchStr);
                        //111 - כמות בחשבונית כפי שזוהה
                        qtyInInvoices += Double.valueOf(matchStr[111]);
                    }
                } catch (Exception e) {

                }
            }

            for (Match match : connectToJustPreviousPerformalInvoice) {
                if (!isContainInvoice(vInvoices, match.match, 90, 102) && !match.get_connect()) {
                    try {
                        //98- כמות בחשבונית העיסקה כפי שזוהה
                        qtyInInvoices += Double.valueOf(match.match[98]);
                    } catch (Exception e) {

                    }
                }
            }

            try {
                double qty = Double.valueOf(block.qty.get_result_str());
                if (isCreditInvoice && qty > 0) {
                    qty *= (-1);
                }
                qtyInInvoices += qty;
            } catch (Exception e) {

            }

            double supakInLading = 0;
            boolean FindInLading = false;
            Vector<String> vLadingNumber = new Vector<String>();
            for (Match m : matches) {
                try {
                    if (!vLadingNumber.contains(m.match[18]) && !isCreditInvoice && !isCreditMatch(m.match) && !m.match[18].equals("")) {
                        vLadingNumber.add(m.match[18]);
                        if (!m.match[66].equals("")) {
                            FindInLading = true;
                            supakInLading += Double.valueOf(m.match[66]);
                        }
                    } else if (!vLadingNumber.contains(m.match[18]) && isCreditInvoice && isCreditMatch(m.match) && !m.match[18].equals("")) {
                        vLadingNumber.add(m.match[18]);
                        if (!m.match[66].equals("")) {
                            FindInLading = true;
                            supakInLading += Double.valueOf(m.match[66]);
                        }
                    }
                } catch (Exception e) {

                }
            }

            if (FindInLading) {
                diffrenceInQtyBetweenSupakInLadingAndInvoice = qtyInInvoices - supakInLading;
            }
        }
    }

    private void fillBalanceOfBudget() {
        Vector<String> vPoNumber = new Vector<String>();
        for (int i = 0; i < matches.size(); i++) {
            if (!vPoNumber.contains(matches.elementAt(i).match[18]) && !matches.elementAt(i).match[18].equals("")) {
                vPoNumber.add(matches.elementAt(i).match[18]);
                try {
                    budgetForAllorders += Double.valueOf(matches.elementAt(i).match[44]) - Double.valueOf(matches.elementAt(i).match[43]);
                } catch (Exception e) {

                }
            }
        }

        double price = 0;
        double rate = 1;

        if (block.Curreny_for_one_item != block.Curreny_for_all_qty) {
            try {
                rate = Double.valueOf(block.rate.get_result_str());
            } catch (Exception e) {
            }
        }

        /*
         חישוב מחיר הפריט הנוכחי 
         */ {
            if (block.one_price != null) {
                price = Double.valueOf(block.one_price.get_result_str());
            } else if (block.price != null) {
                rate = 1;
                price = Double.valueOf(block.price.get_result_str());
            }
        }

        boolean needToChange = false;
        String csvMamStr = "";

        for (int j = 0; j < vPoNumber.size(); j++) {
            double qtyToOnePoNumber = 0;
            // עוברים על כל ההתאמות שנמצאו ובודקים אילו מהם שווים למספר ההזמנה הנוכחי ושייכים לחשבונית
            // אם ההתאמה עומדת בתנאים נסכום את הכמויות ששייכות לחשבונית
            for (int i = 0; i < matches.size(); i++) {
                if (vPoNumber.elementAt(j).equals(matches.elementAt(i).match[18]) && (matches.elementAt(i).get_connect() || matches.elementAt(i).get_needNewLine())) {
                    if (matches.elementAt(i).get_connect()) {
                        qtyToOnePoNumber += matches.elementAt(i).get_qtyConnect();
                        needToChange = true;
                        // 36- אחוז המעמ החל על הפריט המוזמן
                        csvMamStr = matches.elementAt(i).match[36];

                    } else if (matches.elementAt(i).get_needNewLine()) {
                        qtyToOnePoNumber += matches.elementAt(i).get_qtyToNewLine();
                        needToChange = true;
                        // 36- אחוז המעמ החל על הפריט המוזמן
                        csvMamStr = matches.elementAt(i).match[36];
                    }
                }
            }
            try {
                if (matchToOrderedItemsCSVbyCSVOneLine || matchToOrderedItemsCSVbyCSVManyLine) {
                    qtyToOnePoNumber = Double.valueOf(block.qty.get_result_str());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // אם צריך לשנות את יתרת התקציב עבור ההזמנה הזו
            if (needToChange) {
                double totalForOnePoNumberNoMam = Double.parseDouble(new DecimalFormat("##.##").format(rate * price * qtyToOnePoNumber));
                try {
                    // הנחה כללית:
                    if (globalanahav.getVerifived() && globalanahav.getSum() != 0) {
                        if (sumallNoMamv.getVerifived()) {
                            //אם סכום החשבונית אושרר אז אחלק את ההנחה באופן יחסי לסכום הפריט וסכום החשבונית
                            double anaha = globalanahav.getSum() * (totalForOnePoNumberNoMam / sumallNoMamv.getSum());
                            totalForOnePoNumberNoMam = totalForOnePoNumberNoMam - Math.abs(anaha);
                            totalForOnePoNumberNoMam = Double.parseDouble(new DecimalFormat("##.##").format(totalForOnePoNumberNoMam));
                        } else {
                            //מחלקים את ההנחה הגלובלית באופן שווה בין הפריטים
                            double anaha = globalanahav.getSum() / numberOfItems;
                            totalForOnePoNumberNoMam = totalForOnePoNumberNoMam - Math.abs(anaha);
                            totalForOnePoNumberNoMam = Double.parseDouble(new DecimalFormat("##.##").format(totalForOnePoNumberNoMam));
                        }
                    }

                } catch (Exception e) {

                }

                double totalForOnePoNumberWithMam = 0;
                double mam = 0;
                double percent = 0;
                //חישוב המעמ להזמנה
                if (block.percent != null && !block.percent.get_result_str().equals("")) {
                    percent = Double.valueOf(block.percent.get_result_str().replace("%", ""));
                    mam = Double.parseDouble(new DecimalFormat("##.##").format(totalForOnePoNumberNoMam * (percent / 100)));
                    totalForOnePoNumberWithMam = totalForOnePoNumberNoMam + mam;
                } else {
                    try {
                        percent = Double.valueOf(csvMamStr.replace("%", ""));
                        mam = Double.parseDouble(new DecimalFormat("##.##").format(totalForOnePoNumberNoMam * (percent / 100)));
                        totalForOnePoNumberWithMam = totalForOnePoNumberNoMam + mam;
                    } catch (Exception e) {

                    }
                }

                boolean isAdded = false;
                for (int i = 0; i < matches.size(); i++) {
                    if (vPoNumber.elementAt(j).equals(matches.elementAt(i).match[18])) {
                        // 123 - יתרת תקציב ללא מעמ
                        if (matchToOrderedItemsCSVbyCSVOneLine || matchToOrderedItemsCSVbyCSVManyLine) {
                            matches.elementAt(i).match[123] = "0";
                        } else if ("".equals(matches.elementAt(i).match[123])) {
                            try {
                                matches.elementAt(i).match[123] = String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(
                                        Double.valueOf(matches.elementAt(i).match[44]) - Double.valueOf(matches.elementAt(i).match[43]) - totalForOnePoNumberNoMam)));
                            } catch (Exception e) {

                            }
                        } else {
                            try {
                                matches.elementAt(i).match[123] = String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(matches.elementAt(i).match[123]) - totalForOnePoNumberNoMam)));
                            } catch (Exception e) {

                            }
                        }
                        // 124 - יתרת תקציב עם מעמ
                        if (matchToOrderedItemsCSVbyCSVOneLine || matchToOrderedItemsCSVbyCSVManyLine) {
                            matches.elementAt(i).match[124] = "0";
                        }
                        if ("".equals(matches.elementAt(i).match[124])) {
                            try {
                                matches.elementAt(i).match[124] = String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(matches.elementAt(i).match[44]) - totalForOnePoNumberWithMam)));
                            } catch (Exception e) {

                            }
                        } else {
                            try {
                                matches.elementAt(i).match[124] = String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(Double.valueOf(matches.elementAt(i).match[124]) - totalForOnePoNumberWithMam)));
                            } catch (Exception e) {

                            }
                        }
                        if (!isAdded) {
                            try {
                                BalanceOfBudget += Double.valueOf(matches.elementAt(i).match[123]);
                                isAdded = true;
                            } catch (Exception e) {

                            }
                        }
                    }
                }
            }

            needToChange = false;
        }
    }

    private void fillDiffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice() {

        // סך הכמויות שהיו בחשבוניות קודמות
        double qtyInPrevInvoices1 = 0;
        Vector<String[]> vInvoices1 = new Vector<String[]>();
        Vector<String[]> vInvoices = new Vector<String[]>();

        for (String[] matchStr : connectToPreviousInvoice) {
            try {
                if (!isContainInvoice(vInvoices1, matchStr, 103, 115) && !isCreditInvoice && !isCreditMatch(matchStr)) {
                    vInvoices1.add(matchStr);
                    //111 - כמות בחשבונית כפי שזוהה
                    qtyInPrevInvoices1 += Double.valueOf(matchStr[111]);
                } else if (!isContainInvoice(vInvoices1, matchStr, 103, 115) && isCreditInvoice && isCreditMatch(matchStr)) {
                    vInvoices1.add(matchStr);
                    //111 - כמות בחשבונית כפי שזוהה
                    qtyInPrevInvoices1 += Double.valueOf(matchStr[111]);
                }
            } catch (Exception e) {

            }
        }

        for (Match match : connectToJustPreviousPerformalInvoice) {
            if (!isContainInvoice(vInvoices, match.match, 90, 102) && !match.get_connect() && !isCreditInvoice) {
                try {
                    //98- כמות בחשבונית העיסקה כפי שזוהה
                    qtyInPrevInvoices1 += Double.valueOf(match.match[98]);
                } catch (Exception e) {
                }
            }
        }

        double qtyInInvoice = Double.valueOf(block.qty.get_result_str());
        if (isCreditInvoice && qtyInInvoice > 0) {
            qtyInInvoice *= (-1);
        }
        if (!isCreditInvoice) {
            //חישוב הכמות שמחכה לחשבונית
            double waitForInvoice = supak - qtyInPrevInvoices1;
            if (waitForInvoice <= qtyInInvoice && !isCreditInvoice) {
                double gap = qtyInInvoice - waitForInvoice;
                diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice = gap;
            }
        } else if (isCreditInvoice) {
            double supakInCredirMatches = 0;
            Vector<String> vSupplyCertificate = new Vector<String>();
            for (int i = 0; i < matches.size(); i++) {
                if (isCreditMatch(matches.elementAt(i).match) && !vSupplyCertificate.contains(matches.elementAt(i).match[11]) && !matches.elementAt(i).match[11].equals("")) {
                    try {
                        supakInCredirMatches += Double.valueOf(matches.elementAt(i).match[14]);
                        vSupplyCertificate.add(matches.elementAt(i).match[11]);
                    } catch (Exception e) {

                    }
                }
            }
            double waitForInvoice = supakInCredirMatches - qtyInPrevInvoices1;
            if (Math.abs(waitForInvoice) <= Math.abs(qtyInInvoice)) {
                diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice = qtyInInvoice - waitForInvoice;
            }
        }

        /*double supak = 0;
         double qtyInInvoice = 0;
         if(block.qty != null){
         try{
         qtyInInvoice = Double.valueOf(block.qty.get_result_str());  
         for(Match m : matches){
         if(m.get_connectToInvoice()){
         supak += m.get_qtyConnectToInvoice();
         }else if(m.get_needNewLine()){
         supak += m.get_qtyToNewLine();
         }
         }
         diffrenceInQtyBetweenSupakInSupplyCertificatesAndInvoice = qtyInInvoice - supak;
         }catch(Exception e){
                
         }
         }*/
    }

    private void fillPoNumbersSupplyCertificatesLadingNumbers() {
        Vector<String> vPoNumber = new Vector<String>();
        Vector<String> vSupplyCertificate = new Vector<String>();
        Vector<String> vLadingNumber = new Vector<String>();

        try {
            for (int i = 0; i < matches.size(); i++) {
                if (matches.elementAt(i).get_connect() == true) {
                    if (!vPoNumber.contains(matches.elementAt(i).match[18]) && !matches.elementAt(i).match[18].equals("")) {
                        //מספרי הזמנות ששייכים לפריט
                        vPoNumber.add(matches.elementAt(i).match[18]);
                        PoNumbers += matches.elementAt(i).match[18] + " ";
                    }
                    if (!vSupplyCertificate.contains(matches.elementAt(i).match[11]) && !matches.elementAt(i).match[11].equals("")) {
                        //מספרי תעודת כניסה ששייכים לפריט בחשבונית
                        vSupplyCertificate.add(matches.elementAt(i).match[11]);
                        SupplyCertificates += matches.elementAt(i).match[11] + " ";
                    }
                    if (!vLadingNumber.contains(matches.elementAt(i).match[63]) && !matches.elementAt(i).match[63].equals("")) {
                        // מספרי תעודות משלוח ששייכים לפריט בחשבונית
                        vLadingNumber.add(matches.elementAt(i).match[63]);
                        LadingNumber += matches.elementAt(i).match[63] + " ";
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void fillLeftToSupply() {
        Vector<String> vPoNumber = new Vector<String>();
        Vector<String> vSupplyCertificate = new Vector<String>();
        double qtyFromAllOrders = 0;
        double qty = 0;

        try {
            for (int i = 0; i < matches.size(); i++) {
                if (!matches_is_fictitious) {
                    if (!matches.elementAt(i).match[21].isEmpty() && !matches.elementAt(i).match[18].equals("") /*&& !vPoNumber.contains(matches.elementAt(i).match[18])*/) {
                        if (!vPoNumber.contains(matches.elementAt(i).match[18])) {
                            vPoNumber.add(matches.elementAt(i).match[18]);
                        }
                        qtyFromAllOrders += Double.valueOf(matches.elementAt(i).match[21]);
                    }
                    if (!matches.elementAt(i).match[11].equals("")) {
                        /* if ((!vSupplyCertificate.contains(matches.elementAt(i).match[11]) || matchToOrderedItemsCSVbyCSVManyLine || matchToOrderedItemsCSVbyCSVOneLine)
                            && !matches.elementAt(i).match[11].equals("")) {*/
                        vSupplyCertificate.add(matches.elementAt(i).match[11]);

                        try {
                            qty = Double.valueOf(matches.elementAt(i).match[14]);
                        } catch (Exception e) {
                        }

                        try {
                            if (qty == 0) {
                                qty = Double.valueOf(matches.elementAt(i).match[21]);
                            }
                        } catch (Exception e) {
                        }

                        if (qty > 0) {
                            supak += qty;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        leftToSupply = qtyFromAllOrders - supak;
    }

    /**
     * פונקציה זו בודקת האם יש התאמת זיכוי שמתאימה לחלוטין לפריט ורק במידה וכל
     * הרשומות שמתאימות הן רשומות זיכוי אז היא מחזירה מסמנת true
     */
    private void checkIfThereIsFullyCreditMatch() {

        if (block.qty != null) {
            double qtyInInvoice = Double.valueOf(block.qty.get_result_str());
            for (int i = 0; i < matches.size(); i++) {
                if (!matches.elementAt(i).get_connectToPrevious() && !matches.elementAt(i).match[14].equals("") && (isINVO || isCreditInvoice || isMISHFile || isCreditMISHFile)) {
                    try {
                        double supakInOrder = Double.valueOf(matches.elementAt(i).match[14]);
                        boolean isSameProformalInvoice = false;
                        boolean haveProformalInvoice = isHaveDetails(matches.elementAt(i).match, 90, 102);
                        /*אם קיימת חשבונית עסקה כבר*/

                        if (haveProformalInvoice) {
                            isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(i).match);
                        }
                        if (isCreditMatch(matches.elementAt(i).match) && Math.abs(qtyInInvoice) == Math.abs(supakInOrder) && (((!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice)) && (isINVO || isCreditInvoice)) || isMISHFile || isCreditMISHFile)) {
                            foundFullyCreditMatch = true;
                            matches.elementAt(i).set_isFullyCreditMatchConnect(true);
                        } //צריך להוסיף פה לפי התשובה של אלי מהי התאמה מלאה עבור תעודות משלוח והזמנה
                        else if (!isCreditMatch(matches.elementAt(i).match) && !isCreditInvoice && !isCreditMISHFile) {
                            foundFullyCreditMatch = false;
                            break;
                        }
                    } catch (Exception e) {

                    }
                } else if (!matches.elementAt(i).get_connectToPrevious() && !matches.elementAt(i).match[21].equals("") && (isINVO || isCreditInvoice || isMISHFile || isCreditMISHFile)) {
                    try {
                        double qtyInOrder = Double.valueOf(matches.elementAt(i).match[21]);
                        boolean isSameProformalInvoice = false;
                        boolean haveProformalInvoice = isHaveDetails(matches.elementAt(i).match, 90, 102);
                        /*אם קיימת חשבונית עסקה כבר*/

                        if (haveProformalInvoice) {
                            isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(i).match);
                        }
                        if (isCreditMatch(matches.elementAt(i).match) && Math.abs(qtyInInvoice) == Math.abs(qtyInOrder) && (((!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice)) && (isINVO || isCreditInvoice)) || isMISHFile || isCreditMISHFile)) {
                            foundFullyCreditMatch = true;
                            matches.elementAt(i).set_isFullyCreditMatchConnect(true);
                        } //צריך להוסיף פה לפי התשובה של אלי מהי התאמה מלאה עבור תעודות משלוח והזמנה
                        else if (!isCreditMatch(matches.elementAt(i).match) && !isCreditInvoice && !isCreditMISHFile) {
                            foundFullyCreditMatch = false;
                            break;
                        }
                    } catch (Exception e) {

                    }
                } else if (!matches.elementAt(i).get_connectToPrevious() && !matches.elementAt(i).match[56].equals("") && (isPORDFile || isCreditPORDFile)) {
                    try {
                        double qtyInPQ = Double.valueOf(matches.elementAt(i).match[56]); // כמות בהצעת מחיר
                        if (isCreditMatch(matches.elementAt(i).match) && Math.abs(qtyInInvoice) == Math.abs(qtyInPQ)) {
                            foundFullyCreditMatch = true;
                            matches.elementAt(i).set_isFullyCreditMatchConnect(true);
                        } //צריך להוסיף פה לפי התשובה של אלי מהי התאמה מלאה עבור תעודות משלוח והזמנה
                        else if (!isCreditMatch(matches.elementAt(i).match) && !isCreditPORDFile) {
                            foundFullyCreditMatch = false;
                            break;
                        }
                    } catch (Exception e) {

                    }
                }
            }
        }
    }

    private void checkHowManyWaitForInvoice() {
        // סך הכמויות שהיו בחשבוניות קודמות
        double qtyInPrevInvoices1 = 0;
        Vector<String[]> vInvoices1 = new Vector<String[]>();
        for (String[] matchStr : connectToPreviousInvoice) {
            if (!isContainInvoice(vInvoices1, matchStr, 103, 115) && !isCreditInvoice && !isProformalInvoice && !isCreditMatch(matchStr)) {
                vInvoices1.add(matchStr);
                try {
                    //111 - כמות בחשבונית כפי שזוהה
                    qtyInPrevInvoices1 += Double.valueOf(matchStr[111]);
                } catch (Exception e) {
                }
            }/*else if((!isContainInvoice(vInvoices1, matchStr, 103, 115) && isCreditInvoice && !isProformalInvoice && isCreditMatch(matchStr))){
             vInvoices1.add(matchStr);
             //111 - כמות בחשבונית כפי שזוהה
             qtyInPrevInvoices1 += Double.valueOf(matchStr[111]);
             }*/

        }

        waitForInvoice = supak - qtyInPrevInvoices1;
    }

    private boolean isSameProformalInvoice(String[] Match) {
        boolean isSamePricePerOne = false;
        boolean isSameQty = false;
        if (block.one_price != null && !Match[97].equals("")) {
            try {
                isSamePricePerOne = checktoler(Double.valueOf(Match[97]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
            } catch (Exception e) {
            }
        } else if (!Match[97].equals("") && block.price != null) {
            isSamePricePerOne = checktoler(Double.valueOf(Match[97]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);

        }
        if (isSamePricePerOne) {
            try {
                isSameQty = checktoler(Double.valueOf(Match[98]), Double.valueOf(block.qty.get_result_str()), tolerance);
            } catch (Exception e) {

            }
        }
        return isSameQty;
    }

    private Vector<Vector<String[]>> createVectorOfSameSupplyCertificateWithPrevInvoiceOrMishOrOrder() {
        Vector<Vector<String[]>> sameSupplyCertificate = new Vector<Vector<String[]>>();
        boolean isAdded = false;
        //אם זו חשבונית זיכוי או חשבונית מס
        if (!isProformalInvoice && !isMISHFile && !isPORDFile) {
            // יצירת וקטור שמכיל וקטורים בעלי אותו מספר תעודת כניסה
            for (int i = 0; i < connectToPreviousInvoice.size(); i++) {
                for (int j = 0; j < sameSupplyCertificate.size(); j++) {
                    if (connectToPreviousInvoice.elementAt(i)[11].equals(sameSupplyCertificate.elementAt(j).elementAt(0)[11])) {
                        sameSupplyCertificate.elementAt(j).add(connectToPreviousInvoice.elementAt(i));
                        isAdded = true;
                    }
                }
                if (!isAdded) {
                    Vector<String[]> v = new Vector<String[]>();
                    v.add(connectToPreviousInvoice.elementAt(i));
                    sameSupplyCertificate.add(v);
                } else {
                    isAdded = false;
                }
            }
        } // אם זאת חשבונית עסקה
        else if (isProformalInvoice) {
            // יצירת וקטור שמכיל וקטורים בעלי אותו מספר תעודת כניסה
            for (int i = 0; i < connectToPreviousPerformalInvoice.size(); i++) {
                for (int j = 0; j < sameSupplyCertificate.size(); j++) {
                    if (connectToPreviousPerformalInvoice.elementAt(i).match[11].equals(sameSupplyCertificate.elementAt(j).elementAt(0)[11])) {
                        sameSupplyCertificate.elementAt(j).add(connectToPreviousPerformalInvoice.elementAt(i).match);
                        isAdded = true;
                    }
                }
                if (!isAdded) {
                    Vector<String[]> v = new Vector<String[]>();
                    v.add(connectToPreviousPerformalInvoice.elementAt(i).match);
                    sameSupplyCertificate.add(v);
                } else {
                    isAdded = false;
                }
            }
        } // אם זו תעודת משלוח
        else if (isMISHFile) {
            // יצירת וקטור שמכיל וקטורים בעלי אותו מספר תעודת כניסה
            for (int i = 0; i < connectToPrevMish.size(); i++) {
                for (int j = 0; j < sameSupplyCertificate.size(); j++) {
                    if (connectToPrevMish.elementAt(i).match[11].equals(sameSupplyCertificate.elementAt(j).elementAt(0)[11])) {
                        sameSupplyCertificate.elementAt(j).add(connectToPrevMish.elementAt(i).match);
                        isAdded = true;
                    }
                }
                if (!isAdded) {
                    Vector<String[]> v = new Vector<String[]>();
                    v.add(connectToPrevMish.elementAt(i).match);
                    sameSupplyCertificate.add(v);
                } else {
                    isAdded = false;
                }
            }
        } // אם זאת הזמנה
        else if (isPORDFile) {
            // יצירת וקטור שמכיל וקטורים בעלי אותו מספר תעודת כניסה
            for (int i = 0; i < connectToPrevOrder.size(); i++) {
                for (int j = 0; j < sameSupplyCertificate.size(); j++) {
                    if (connectToPrevOrder.elementAt(i).match[11].equals(sameSupplyCertificate.elementAt(j).elementAt(0)[11])) {
                        sameSupplyCertificate.elementAt(j).add(connectToPrevOrder.elementAt(i).match);
                        isAdded = true;
                    }
                }
                if (!isAdded) {
                    Vector<String[]> v = new Vector<String[]>();
                    v.add(connectToPrevOrder.elementAt(i).match);
                    sameSupplyCertificate.add(v);
                } else {
                    isAdded = false;
                }
            }
        }

        return sameSupplyCertificate;
    }

    private double searchWhichMatchMishAndPordFile(double qtyLeft, double qtyInInvoice, Vector<Vector<String[]>> sameSupplyCertificate, int indexOfLast) {
        double qtySupak = 0;
        boolean first = false;
        if (qtyLeft > 0) {
            int index = getIndexOfMatchInMatchesVector(sameSupplyCertificate.elementAt(indexOfLast).elementAt(sameSupplyCertificate.elementAt(indexOfLast).size() - 1));
            qtySupak = Double.valueOf(matches.elementAt(index).match[14]);
            if (qtySupak > 0 /* בדיקה שזה לא רשומת זיכוי */) {
                //אם נותר פריטים שסופקו בפועל ואין עליהם תעודת משלוח
                if (qtyLeft > 0) {
                    qtyInInvoice = qtyInInvoice - qtyLeft;
                    if (qtyInInvoice >= 0) {
                        matches.elementAt(index).set_needNewLine(true);
                        matches.elementAt(index).set_qtyToNewLine(qtyLeft);
                        matches.elementAt(index).set_allConnect(false);
                        matches.elementAt(index).set_connect(false);
                        matches.elementAt(index).set_connectToPrevious(true);
                        matches.elementAt(index).set_qtyConnect(0);
                        if (qtyInInvoice == 0) {
                            first = true;
                        }
                    } else if (qtyInInvoice < 0 && first == false) {
                        matches.elementAt(index).set_needNewLine(true);
                        matches.elementAt(index).set_qtyToNewLine(qtyInInvoice + qtyLeft);
                        matches.elementAt(index).set_allConnect(false);
                        matches.elementAt(index).set_connect(false);
                        matches.elementAt(index).set_connectToPrevious(true);
                        matches.elementAt(index).set_qtyConnect(0);
                        first = true;
                    } else if (qtyInInvoice < 0 && first == true) {
                        matches.elementAt(index).set_needNewLine(false);
                        matches.elementAt(index).set_qtyConnect(0);
                        matches.elementAt(index).set_allConnect(false);
                        matches.elementAt(index).set_connect(false);
                        matches.elementAt(index).set_connectToPrevious(true);
                        matches.elementAt(index).set_qtyConnect(0);
                    }
                }
            } else {
                matches.elementAt(index).set_connect(false);
            }
        }
        if (!first) {
            try {
                for (int i = 0; i < matches.size(); i++) {
                    double currentAmount = Double.valueOf(matches.elementAt(i).match[14]);
                    if (matches.elementAt(i).get_connect() == true && !matches.elementAt(i).get_dontHaveSupak() && currentAmount > 0 /* בדיקה שזה לא רשומת זיכוי */) {
                        qtyInInvoice = qtyInInvoice - currentAmount;
                        if (qtyInInvoice >= 0) {
                            matches.elementAt(i).set_connect(true);
                            matches.elementAt(i).set_allConnect(true);
                            matches.elementAt(i).set_qtyConnect(currentAmount);
                            if (qtyInInvoice == 0) {
                                first = true;
                            }
                        } else if (qtyInInvoice < 0 && first == false) {
                            matches.elementAt(i).set_connect(true);
                            matches.elementAt(i).set_qtyConnect(qtyInInvoice + currentAmount);
                            first = true;
                        } else if (qtyInInvoice < 0 && first == true) {
                            matches.elementAt(i).set_connect(false);
                        }
                    } else {
                        matches.elementAt(i).set_connect(false);
                    }
                }
            } catch (Exception e) {
                for (Match match : matches) {
                    match.set_connect(false);
                }
            }
        } else if (first) {
            for (Match match : matches) {
                match.set_connect(false);
                match.set_allConnect(false);
                match.set_qtyConnect(0);
            }
        }

        return qtyInInvoice;
    }

    private double searchWhichMatchCreditMishAndCreditPordFile(double qtyLeft, double qtyInInvoice, Vector<Vector<String[]>> sameSupplyCertificate, int indexOfLast) {
        if (qtyInInvoice > 0) {
            qtyInInvoice = qtyInInvoice * (-1);
        }
        boolean first = false;
        // אם נמצאה רשומה שמתאימה באופן מלא לפריט
        if (foundFullyCreditMatch) {
            for (int i = 0; i < matches.size(); i++) {
                if (!matches.elementAt(i).get_connectToPrevious() && matches.elementAt(i).get_isFullyCreditMatchConnect()) {
                    matches.elementAt(i).set_allConnect(true);
                    matches.elementAt(i).set_connect(true);
                    matches.elementAt(i).set_qtyConnect(qtyInInvoice);
                } else {
                    matches.elementAt(i).set_connect(false);
                }
            }
        } else {
            double qtySupak = 0;
            if (qtyLeft < 0) {
                int index = getIndexOfMatchInMatchesVector(sameSupplyCertificate.elementAt(indexOfLast).elementAt(sameSupplyCertificate.elementAt(indexOfLast).size() - 1));
                qtySupak = Double.valueOf(matches.elementAt(index).match[14]);
                if (qtySupak < 0 /* אם זו רשומת זיכוי*/) {
                    //אם נותר פריטים שסופקו בפועל ואין עליהם חשבונית
                    if (qtyLeft < 0) {
                        qtyInInvoice = qtyInInvoice - qtyLeft;
                        if (qtyInInvoice <= 0) {
                            matches.elementAt(index).set_needNewLine(true);
                            matches.elementAt(index).set_qtyToNewLine(qtyLeft);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                            if (qtyInInvoice == 0) {
                                first = true;
                            }
                        } else if (qtyInInvoice > 0 && first == false) {
                            matches.elementAt(index).set_needNewLine(true);
                            matches.elementAt(index).set_qtyToNewLine(qtyInInvoice + qtyLeft);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                            first = true;
                        } else if (qtyInInvoice > 0 && first == true) {
                            matches.elementAt(index).set_needNewLine(false);
                            matches.elementAt(index).set_qtyConnect(0);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                        }
                    }
                } else {
                    matches.elementAt(index).set_connect(false);
                }
            }
            if (!first) {
                try {
                    for (int i = 0; i < matches.size(); i++) {
                        double currentAmount = Double.valueOf(matches.elementAt(i).match[14]);
                        if (matches.elementAt(i).get_connect() == true && !matches.elementAt(i).get_dontHaveSupak() && currentAmount < 0) {
                            qtyInInvoice = qtyInInvoice - currentAmount;
                            if (qtyInInvoice <= 0) {
                                matches.elementAt(i).set_connect(true);
                                matches.elementAt(i).set_allConnect(true);
                                matches.elementAt(i).set_qtyConnect(currentAmount);
                                if (qtyInInvoice == 0) {
                                    first = true;
                                }
                            } else if (qtyInInvoice > 0 && first == false) {
                                matches.elementAt(i).set_connect(true);
                                matches.elementAt(i).set_qtyConnect(qtyInInvoice + currentAmount);
                                first = true;
                            } else if (qtyInInvoice > 0 && first == true) {
                                matches.elementAt(i).set_connect(false);
                            }
                        } else {
                            matches.elementAt(i).set_connect(false);
                        }
                    }
                } catch (Exception e) {
                    for (Match match : matches) {
                        match.set_connect(false);
                    }
                }
            } else if (first) {
                for (Match match : matches) {
                    match.set_connect(false);
                    match.set_allConnect(false);
                    match.set_qtyConnect(0);
                }
            }
        }

        return qtyInInvoice;
    }

    private double searchWhichMatchInvoFileFirstCase(double qtyLeft, double qtyInInvoice, Vector<Vector<String[]>> sameSupplyCertificate, int indexOfLast) {
        double gap = qtyInInvoice - waitForInvoice;
        double qtySupak = 0;
        if (qtyLeft > 0) {
            int index = getIndexOfMatchInMatchesVector(sameSupplyCertificate.elementAt(indexOfLast).elementAt(sameSupplyCertificate.elementAt(indexOfLast).size() - 1));
            qtySupak = Double.valueOf(matches.elementAt(index).match[14]);
            boolean isSameProformalInvoice = false;
            boolean haveProformalInvoice = isHaveDetails(matches.elementAt(index).match, 90, 102);
            /*אם קיימת חשבונית עסקה כבר*/

            if (haveProformalInvoice) {
                isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(index).match);
            }
            if (qtySupak > 0 /* לבדוק שזה לא רשומת זיכוי*/ && (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice))) {
                // כמה פריטים נותרו לקבל עליהם חשבונית
                qtyInInvoice = qtyInInvoice - qtyLeft;
                matches.elementAt(index).set_needNewLine(true);
                if (gap == qtyInInvoice) {
                    matches.elementAt(index).set_qtyToNewLine(qtyLeft + gap);
                } else {
                    matches.elementAt(index).set_qtyToNewLine(qtyLeft);
                }
                matches.elementAt(index).set_allConnect(false);
                matches.elementAt(index).set_connect(false);
                matches.elementAt(index).set_connectToPrevious(true);
                matches.elementAt(index).set_qtyConnect(0);
            } else {
                matches.elementAt(index).set_connect(false);
            }
        }

        for (Match m : matches) {
            if (!m.get_connectToPrevious() && !m.get_dontHaveSupak()) {
                qtySupak = Double.valueOf(m.match[14]);
                boolean isSameProformalInvoice = false;
                boolean haveProformalInvoice = isHaveDetails(m.match, 90, 102);
                /*אם קיימת חשבונית עסקה כבר*/

                if (haveProformalInvoice) {
                    isSameProformalInvoice = isSameProformalInvoice(m.match);
                }
                if (qtySupak > 0 /* בדיקה שזה לא רשומת זיכוי */ && (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice))) {
                    qtyInInvoice = qtyInInvoice - qtySupak;
                    m.set_connect(true);
                    m.set_allConnect(true);
                    if (gap == qtyInInvoice) {
                        m.set_qtyConnect(Double.valueOf(m.match[14]) + gap);
                    } else {
                        m.set_qtyConnect(Double.valueOf(m.match[14]));
                    }
                } else {
                    m.set_connect(false);
                }
            }
        }

        return qtyInInvoice;
    }

    private double searchWhichMatchInvoFileSecondCase(double qtyLeft, double qtyInInvoice, Vector<Vector<String[]>> sameSupplyCertificate, int indexOfLast) {
        double qtySupak = 0;
        boolean first = false;
        int index = getIndexOfMatchInMatchesVector(sameSupplyCertificate.elementAt(indexOfLast).elementAt(sameSupplyCertificate.elementAt(indexOfLast).size() - 1));
        if (qtyLeft > 0) {
            qtySupak = Double.valueOf(matches.elementAt(index).match[14]);
            boolean isSameProformalInvoice = false;
            boolean haveProformalInvoice = isHaveDetails(matches.elementAt(index).match, 90, 102);
            /*אם קיימת חשבונית עסקה כבר*/

            if (haveProformalInvoice) {
                isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(index).match);
            }
            if (qtySupak > 0 /* בדיקה שזה לא רשומת זיכוי */ && (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice))) {
                //אם נותר פריטים שסופקו בפועל ואין עליהם חשבונית
                if (qtyLeft > 0) {
                    qtyInInvoice = qtyInInvoice - qtyLeft;
                    if (qtyInInvoice >= 0) {
                        matches.elementAt(index).set_needNewLine(true);
                        matches.elementAt(index).set_qtyToNewLine(qtyLeft);
                        matches.elementAt(index).set_allConnect(false);
                        matches.elementAt(index).set_connect(false);
                        matches.elementAt(index).set_connectToPrevious(true);
                        matches.elementAt(index).set_qtyConnect(0);
                        if (qtyInInvoice == 0) {
                            first = true;
                        }
                    } else if (qtyInInvoice < 0 && first == false) {
                        matches.elementAt(index).set_needNewLine(true);
                        matches.elementAt(index).set_qtyToNewLine(qtyInInvoice + qtyLeft);
                        matches.elementAt(index).set_allConnect(false);
                        matches.elementAt(index).set_connect(false);
                        matches.elementAt(index).set_connectToPrevious(true);
                        matches.elementAt(index).set_qtyConnect(0);
                        first = true;
                    } else if (qtyInInvoice < 0 && first == true) {
                        matches.elementAt(index).set_needNewLine(false);
                        matches.elementAt(index).set_qtyConnect(0);
                        matches.elementAt(index).set_allConnect(false);
                        matches.elementAt(index).set_connect(false);
                        matches.elementAt(index).set_connectToPrevious(true);
                        matches.elementAt(index).set_qtyConnect(0);
                    }
                }
            }
        } else {
            matches.elementAt(index).set_connect(false);
        }

        if (!first) {
            try {
                for (int i = 0; i < matches.size(); i++) {
                    qtySupak = Double.valueOf(matches.elementAt(i).match[14]);
                    boolean isSameProformalInvoice = false;
                    boolean haveProformalInvoice = isHaveDetails(matches.elementAt(i).match, 90, 102);
                    /*אם קיימת חשבונית עסקה כבר*/

                    if (haveProformalInvoice) {
                        isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(i).match);
                    }
                    if (matches.elementAt(i).get_connect() == true && !matches.elementAt(i).get_dontHaveSupak() && qtySupak > 0 /* בדיקה שזה לא רשומת זיכוי */ && (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice))) {
                        double currentAmount = Double.valueOf(matches.elementAt(i).match[14]);
                        qtyInInvoice = qtyInInvoice - currentAmount;
                        if (qtyInInvoice >= 0) {
                            matches.elementAt(i).set_connect(true);
                            matches.elementAt(i).set_allConnect(true);
                            matches.elementAt(i).set_qtyConnect(currentAmount);
                            if (qtyInInvoice == 0) {
                                first = true;
                            }
                        } else if (qtyInInvoice < 0 && first == false) {
                            matches.elementAt(i).set_connect(true);
                            matches.elementAt(i).set_qtyConnect(qtyInInvoice + currentAmount);
                            first = true;
                        } else if (qtyInInvoice < 0 && first == true) {
                            matches.elementAt(i).set_connect(false);
                        }
                    } else {
                        matches.elementAt(i).set_connect(false);
                    }
                }
            } catch (Exception e) {
                for (Match match : matches) {
                    match.set_connect(false);
                }
            }
        } else if (first) {
            for (Match match : matches) {
                match.set_connect(false);
                match.set_allConnect(false);
                match.set_qtyConnect(0);
            }
        }

        return qtyInInvoice;
    }

    private double searchWhichMatchCreditInvoice(double qtyLeft, double qtyInInvoice, Vector<Vector<String[]>> sameSupplyCertificate, int indexOfLast) {
        if (qtyInInvoice > 0) {
            qtyInInvoice = qtyInInvoice * (-1);
        }
        boolean first = false;
        // אם נמצאה רשומה שמתאימה באופן מלא לפריט
        if (foundFullyCreditMatch) {
            for (int i = 0; i < matches.size(); i++) {
                if (!matches.elementAt(i).get_connectToPrevious() && matches.elementAt(i).get_isFullyCreditMatchConnect()) {
                    matches.elementAt(i).set_allConnect(true);
                    matches.elementAt(i).set_connect(true);
                    matches.elementAt(i).set_qtyConnect(qtyInInvoice);
                } else {
                    matches.elementAt(i).set_connect(false);
                }
            }
        } else {
            double qtySupak = 0;
            if (qtyLeft < 0) {
                int index = getIndexOfMatchInMatchesVector(sameSupplyCertificate.elementAt(indexOfLast).elementAt(sameSupplyCertificate.elementAt(indexOfLast).size() - 1));

                qtySupak = Double.valueOf(matches.elementAt(index).match[14]);
                boolean isSameProformalInvoice = false;
                boolean haveProformalInvoice = isHaveDetails(matches.elementAt(index).match, 90, 102);
                /*אם קיימת חשבונית עסקה כבר*/

                if (haveProformalInvoice) {
                    isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(index).match);
                }
                if (qtySupak < 0 /* אם זו רשומת זיכוי*/ && (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice))) {
                    //אם נותר פריטים שסופקו בפועל ואין עליהם חשבונית
                    if (qtyLeft < 0) {
                        qtyInInvoice = qtyInInvoice - qtyLeft;
                        if (qtyInInvoice <= 0) {
                            matches.elementAt(index).set_needNewLine(true);
                            matches.elementAt(index).set_qtyToNewLine(qtyLeft);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);

                            if (qtyInInvoice == 0) {
                                first = true;
                            }
                        } else if (qtyInInvoice > 0 && first == false) {
                            matches.elementAt(index).set_needNewLine(true);
                            matches.elementAt(index).set_qtyToNewLine(qtyInInvoice + qtyLeft);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                            first = true;
                        } else if (qtyInInvoice > 0 && first == true) {
                            matches.elementAt(index).set_needNewLine(false);
                            matches.elementAt(index).set_qtyConnect(0);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                        }
                    }
                } else {
                    matches.elementAt(index).set_connect(false);
                }
            }
            if (!first) {
                try {
                    for (int i = 0; i < matches.size(); i++) {
                        double currentAmount = Double.valueOf(matches.elementAt(i).match[14]);
                        boolean isSameProformalInvoice = false;
                        boolean haveProformalInvoice = isHaveDetails(matches.elementAt(i).match, 90, 102);
                        /*אם קיימת חשבונית עסקה כבר*/

                        if (haveProformalInvoice) {
                            isSameProformalInvoice = isSameProformalInvoice(matches.elementAt(i).match);
                        }
                        if (matches.elementAt(i).get_connect() == true && !matches.elementAt(i).get_dontHaveSupak() && currentAmount < 0 && (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice))) {
                            qtyInInvoice = qtyInInvoice - currentAmount;
                            if (qtyInInvoice <= 0) {
                                matches.elementAt(i).set_connect(true);
                                matches.elementAt(i).set_allConnect(true);
                                matches.elementAt(i).set_qtyConnect(currentAmount);
                                if (qtyInInvoice == 0) {
                                    first = true;
                                }
                            } else if (qtyInInvoice > 0 && first == false) {
                                matches.elementAt(i).set_connect(true);
                                matches.elementAt(i).set_qtyConnect(qtyInInvoice + currentAmount);
                                first = true;
                            } else if (qtyInInvoice > 0 && first == true) {
                                matches.elementAt(i).set_connect(false);
                            }
                        } else {
                            matches.elementAt(i).set_connect(false);
                        }
                    }
                } catch (Exception e) {
                    for (Match match : matches) {
                        match.set_connect(false);
                    }
                }
            } else if (first) {
                for (Match match : matches) {
                    match.set_connect(false);
                    match.set_allConnect(false);
                    match.set_qtyConnect(0);
                }
            }
        }

        return qtyInInvoice;
    }

    private double searchWhichMatchProformalInvoice(double qtyLeft, double qtyInInvoice, Vector<Vector<String[]>> sameSupplyCertificate, int indexOfLast) {
        double qtySupak = 0;
        boolean first = false;
        if (qtyLeft > 0) {
            int index = getIndexOfMatchInMatchesVector(sameSupplyCertificate.elementAt(indexOfLast).elementAt(sameSupplyCertificate.elementAt(indexOfLast).size() - 1));
            if (!isHaveDetails(matches.elementAt(index).match, 103, 115)) {
                qtySupak = Double.valueOf(matches.elementAt(index).match[14]);
                if (qtySupak > 0 /* בדיקה שזה לא רשומת זיכוי */) {
                    //אם נותר פריטים שסופקו בפועל ואין עליהם חשבונית
                    if (qtyLeft > 0) {
                        qtyInInvoice = qtyInInvoice - qtyLeft;
                        if (qtyInInvoice >= 0) {
                            matches.elementAt(index).set_needNewLine(true);
                            matches.elementAt(index).set_qtyToNewLine(qtyLeft);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                            if (qtyInInvoice == 0) {
                                first = true;
                            }
                        } else if (qtyInInvoice < 0 && first == false) {
                            matches.elementAt(index).set_needNewLine(true);
                            matches.elementAt(index).set_qtyToNewLine(qtyInInvoice + qtyLeft);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                            first = true;
                        } else if (qtyInInvoice < 0 && first == true) {
                            matches.elementAt(index).set_needNewLine(false);
                            matches.elementAt(index).set_qtyConnect(0);
                            matches.elementAt(index).set_allConnect(false);
                            matches.elementAt(index).set_connect(false);
                            matches.elementAt(index).set_connectToPrevious(true);
                            matches.elementAt(index).set_qtyConnect(0);
                        }
                    }
                }
            } else {
                matches.elementAt(index).set_connect(false);
            }
        }

        if (!first) {
            try {
                for (int i = 0; i < matches.size(); i++) {
                    double currentAmount = Double.valueOf(matches.elementAt(i).match[14]);
                    if (matches.elementAt(i).get_connect() == true && !matches.elementAt(i).get_dontHaveSupak() && currentAmount > 0 /* בדיקה שזה לא רשומת זיכוי */) {
                        qtyInInvoice = qtyInInvoice - currentAmount;
                        if (qtyInInvoice >= 0) {
                            matches.elementAt(i).set_connect(true);
                            matches.elementAt(i).set_allConnect(true);
                            matches.elementAt(i).set_qtyConnect(currentAmount);
                            if (qtyInInvoice == 0) {
                                first = true;
                            }
                        } else if (qtyInInvoice < 0 && first == false) {
                            matches.elementAt(i).set_connect(true);
                            matches.elementAt(i).set_qtyConnect(qtyInInvoice + currentAmount);
                            first = true;
                        } else if (qtyInInvoice < 0 && first == true) {
                            matches.elementAt(i).set_connect(false);
                        }
                    } else {
                        matches.elementAt(i).set_connect(false);
                    }
                }
            } catch (Exception e) {
                for (Match match : matches) {
                    match.set_connect(false);
                }
            }
        } else if (first) {
            for (Match match : matches) {
                match.set_connect(false);
                match.set_allConnect(false);
                match.set_qtyConnect(0);
            }
        }

        return qtyInInvoice;
    }

    private void fillInMatchesWithoutSupak(double qtyLeft) {
        if (qtyLeft > 0 && !isCreditInvoice && !isCreditMISHFile && !isCreditPORDFile) {
            if (isMISHFile || isPORDFile) {
                for (int i = 0; i < dontHaveSupakDtl.size(); i++) {
                    if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 63, 66) && isMISHFile) {
                        dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                        dontHaveSupakDtl.elementAt(i).set_connect(false);
                    } else if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 18, 23) && isPORDFile) {
                        dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                        dontHaveSupakDtl.elementAt(i).set_connect(false);
                    } else {
                        dontHaveSupakDtl.elementAt(i).set_connect(true);
                        dontHaveSupakDtl.elementAt(i).set_qtyConnect(qtyLeft);
                        dontHaveSupakDtl.elementAt(i).set_allConnect(true);
                        findMatchWithoutSupak = true;
                    }
                }
            } else if (isProformalInvoice) {
                for (int i = 0; i < dontHaveSupakDtl.size(); i++) {
                    if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 90, 102)) {
                        dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                        dontHaveSupakDtl.elementAt(i).set_connect(false);
                    } else if (!isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 103, 115)) {
                        dontHaveSupakDtl.elementAt(i).set_connect(true);
                        dontHaveSupakDtl.elementAt(i).set_qtyConnect(qtyLeft);
                        dontHaveSupakDtl.elementAt(i).set_allConnect(true);
                        findMatchWithoutSupak = true;
                    }
                }
            } else if (!isProformalInvoice) {
                for (int i = 0; i < dontHaveSupakDtl.size(); i++) {
                    boolean isSameProformalInvoice = false;
                    boolean haveProformalInvoice = isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 90, 102);
                    /*אם קיימת חשבונית עסקה כבר*/

                    if (haveProformalInvoice) {
                        isSameProformalInvoice = isSameProformalInvoice(dontHaveSupakDtl.elementAt(i).match);
                    }
                    if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 103, 115)) {
                        dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                    } else if (!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice)) {
                        dontHaveSupakDtl.elementAt(i).set_connect(true);
                        dontHaveSupakDtl.elementAt(i).set_qtyConnect(qtyLeft);
                        dontHaveSupakDtl.elementAt(i).set_allConnect(true);
                        findMatchWithoutSupak = true;
                    }
                }
            }
        } else if (qtyLeft < 0 && (isCreditInvoice || isCreditMISHFile || isCreditPORDFile)) {
            for (int i = 0; i < dontHaveSupakDtl.size(); i++) {
                boolean isSameProformalInvoice = false;
                boolean haveProformalInvoice = isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 90, 102);
                /*אם קיימת חשבונית עסקה כבר*/

                if (haveProformalInvoice) {
                    isSameProformalInvoice = isSameProformalInvoice(dontHaveSupakDtl.elementAt(i).match);
                }
                if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 103, 115) && isCreditInvoice) {
                    dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                    dontHaveSupakDtl.elementAt(i).set_connect(false);
                } else if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 63, 66) && isCreditMISHFile) {
                    dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                    dontHaveSupakDtl.elementAt(i).set_connect(false);
                } else if (isHaveDetails(dontHaveSupakDtl.elementAt(i).match, 18, 23) && isCreditPORDFile) {
                    dontHaveSupakDtl.elementAt(i).set_connectToPrevious(true);
                    dontHaveSupakDtl.elementAt(i).set_connect(false);
                } else if (((!haveProformalInvoice || (haveProformalInvoice && isSameProformalInvoice)) && isCreditInvoice) || isCreditMISHFile || isCreditPORDFile) {
                    dontHaveSupakDtl.elementAt(i).set_connect(true);
                    dontHaveSupakDtl.elementAt(i).set_qtyConnect(qtyLeft);
                    dontHaveSupakDtl.elementAt(i).set_allConnect(true);
                    findMatchWithoutSupak = true;
                }
            }
        }

        if (!findMatchWithoutSupak && dontHaveSupakDtl.size() > 0) {
            dontHaveSupakDtl.elementAt(dontHaveSupakDtl.size() - 1).set_needNewLine(true);
            dontHaveSupakDtl.elementAt(dontHaveSupakDtl.size() - 1).set_qtyToNewLine(qtyLeft);
            dontHaveSupakDtl.elementAt(dontHaveSupakDtl.size() - 1).set_allConnect(true);
            findMatchWithoutSupak = true;
        }
    }

    private void setFoundMatches() {
        for (int i = 0; i < matches.size(); i++) {
            if (matchToOrderedItemsCSVbyCSVOneLine || matchToOrderedItemsCSVbyCSVManyLine) {
                foundMatches = true;
            } else if ((matches.elementAt(i).get_connect() && matches.elementAt(i).get_qtyConnect() > 0 && (isINVO || isProformalInvoice || isPORDFile || isMISHFile || isPORDFile))
                    || (matches.elementAt(i).get_connect() && matches.elementAt(i).get_qtyConnect() < 0 && (isCreditInvoice || isCreditMISHFile || isCreditPORDFile))
                    || (matches.elementAt(i).get_needNewLine() && matches.elementAt(i).get_qtyToNewLine() > 0 && (isINVO || isProformalInvoice || isPORDFile || isMISHFile || isPORDFile))
                    || (matches.elementAt(i).get_needNewLine() && matches.elementAt(i).get_qtyToNewLine() < 0 && (isCreditInvoice || isCreditMISHFile || isCreditPORDFile))) {
                foundMatches = true;
            }
        }
    }

    private void setAllMatchesNotConnectToInvoice() {
        for (int i = 0; i < matches.size(); i++) {
            matches.elementAt(i).set_connect(false);
        }
    }

    private int getIndexOfMatchInMatchesVector(String[] match) {
        int index = 0;
        boolean isSame = false;
        for (int i = 0; i < matches.size() && !isSame; i++) {
            for (int j = 0; j < match.length; j++) {
                if (!match[j].trim().equals(matches.elementAt(i).match[j].trim())) {
                    break;
                } else if (match[j].equals(matches.elementAt(i).match[j]) && j == match.length - 1) {
                    isSame = true;
                    index = i;
                }
            }
        }
        return index;
    }

    private void FillVectors() {

        for (Match match : matches) {
            if (isHaveDetails(match.match, 103, 115) && !match.get_dontHaveSupak()) {
                match.set_connect(false);
                match.set_connectToPrevious(true);
                connectToPreviousInvoice.add(match.match);
            }
        }

        for (Match match : matches) {
            if (isHaveDetails(match.match, 90, 102) && !isHaveDetails(match.match, 103, 115) && !match.get_dontHaveSupak()) {
                if (isProformalInvoice) {
                    match.set_connect(false);
                    match.set_connectToPrevious(true);
                }
                //וקטור ששייך רק לחשבוניות עסקה ולא לחשבוניות מס
                connectToJustPreviousPerformalInvoice.add(match);
                match.set_connectToPreviousProformalInvoice(true);
            }
        }

        for (Match match : matches) {
            if (isHaveDetails(match.match, 90, 102) && !match.get_dontHaveSupak()) {
                if (isProformalInvoice) {
                    match.set_connect(false);
                    match.set_connectToPrevious(true);
                }
                //וקטור ששייך לחשבוניות עסקה
                connectToPreviousPerformalInvoice.add(match);
                match.set_connectToPreviousProformalInvoice(true);
            }
        }

        for (Match match : matches) {
            if (!match.get_dontHaveMishDtl() && !match.get_dontHaveSupak()) {
                if (isMISHFile || isCreditMISHFile) {
                    match.set_connect(false);
                    match.set_connectToPrevious(true); // שייך לתעודת משלוח קודמת במידה ואני עכשיו עובדת על תעודות משלוח
                }
                connectToPrevMish.add(match);
            }
        }

        for (Match match : matches) {
            if (!match.get_dontHaveOrderDtl() && !match.get_dontHaveSupak()) {
                if (isPORDFile || isCreditPORDFile) {
                    match.set_connect(false);
                    match.set_connectToPrevious(true); // שייך להזמנה קודמת במידה ואני עכשיו עובדת על הזמנות
                }
                connectToPrevOrder.add(match);
            }
        }
    }

    private boolean isHaveDetails(String[] match, int start, int end) {
        boolean isConnect = false;
        for (int i = start; i <= end; i++) {
            if (!match[i].equals("")) {
                isConnect = true;
                break;
            }
        }
        return isConnect;
    }

    private boolean isContainInvoice(Vector<String[]> invoices, String[] invoice, int start, int end) {
        boolean isContain = false;
        for (int i = 0; i < invoices.size(); i++) {
            if (isSameInvoice(invoices.elementAt(i), invoice, start, end)) {
                isContain = true;
                break;
            }
        }

        return isContain;
    }

    private boolean isSameInvoice(String[] invoice1, String[] invoice2, int start, int end) {
        boolean isSame = false;
        for (int j = start; j <= end; j++) {
            if (!invoice1[j].equals(invoice2[j])) {
                break;
            } else if (invoice1[j].equals(invoice2[j]) && j == 105) {
                isSame = true;
            }
        }
        return isSame;
    }

    private void matchBlockToCSvOneLine(MatchType _match_type) {
        matchToOrderedItemsCSVOneLine(block, _match_type);

        //Create a fictitious match:
        if (this.matches.isEmpty()) {
            matches_is_fictitious = true;

            String[] t = new String[129];
            for (int i = 0; i < t.length; i++) {
                t[i] = "";
            }
            Match m = new Match(t);
            this.matches.add(m);
            //haveMatch = true;
            Collections.sort(this.matches);
        }
    }
    public static int mone = 0;

    private void matchBlockToCSv() {
        mone++;
        Vector<String[]> matches = new Vector<String[]>();
        Vector<Match> matchVector = new Vector<Match>();
        //match with pn, description and unit price
        matches = matchToMakatCsu(block);
        matches.addAll(matchToDescriptionCsu(block));
        matches.addAll(matchToUnitPriceCsu(block));

        if (matches.size() == 0) {//tsdfbgdxgfdhgfdgdfsgfdgfdgfdgfdgfd!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
            matches.addAll(matchTomatchToUnitPriceCsuBySupplierId(block));
            matches = matchToMakatCsu(block);
        }

        matches = deleteDuplicate(matches);
        if (matches.size() >= 1) {
            matchVector.clear();
            if (SupplierId == null) {
                SupplierId = matches.elementAt(0)[24];
            }
            for (int i = 1; i < matches.size(); i++) {
                if (SupplierId != null && matches.elementAt(i)[24] != null && !matches.elementAt(i)[24].equalsIgnoreCase(SupplierId)) {
                    SupplierId = ",,notthesame!,,";
                }
            }

            for (int i = 0; i < matches.size(); i++) {
                Match m = new Match(matches.elementAt(i));
                matchVector.add(m);
            }
            this.matches = matchVector;
            //haveMatch = true;
            Collections.sort(this.matches);
        } else if (matches.isEmpty() || matches == null) {
            matchVector.clear();
            matchVector = matchToUnitPriceAndDescriptionOrPn();
            if (matchVector.size() == 1 && matchVector.get(0) != null) {
                this.matches = matchVector;
                if (matchVector.get(0).get_partialDescriptionMatch()) {
                    foundPartialMatchToDES = true;
                }
                if (matchVector.get(0).get_partialPnMatch()) {
                    foundPartialMatchToPN = true;
                }
                //haveMatch = true;
                Collections.sort(this.matches);
            }
        }
        if (matchVector.isEmpty() || matchVector == null) {
            matchVector.clear();
            matches = matchToUnitPriceAndQtyWithoutMakatAndDescription();
            if (matches.size() >= 1) {
                matchVector = new Vector<Match>();
                for (int i = 0; i < matches.size(); i++) {
                    Match m = new Match(matches.elementAt(i));
                    m.set_partialDescriptionMatch(true);
                    m.set_partialPnMatch(true);
                    foundPartialMatchToPN = true;
                    foundPartialMatchToDES = true;
                    matchVector.add(m);
                }
                this.matches = matchVector;
                Collections.sort(this.matches);
            }
        }
        if (matchVector.isEmpty() || matchVector == null) {
            matchVector.clear();
            matchVector = matchToDescriptionOrMakatAndQtyWithoutUnitPrice();
            if (matchVector.size() == 1 && matchVector.get(0) != null) {
                this.matches = matchVector;
                if (matchVector.get(0).get_partialDescriptionMatch()) {
                    foundPartialMatchToDES = true;
                }
                if (matchVector.get(0).get_partialPnMatch()) {
                    foundPartialMatchToPN = true;
                }
                Collections.sort(this.matches);
            }
        }

        if (this.matches.isEmpty()) {
            matchToOrderedItemsCSV(block);
        }

        //Create a fictitious match:
        if (this.matches.isEmpty()) {
            matches_is_fictitious = true;

            String[] t = new String[129];
            for (int i = 0; i < t.length; i++) {
                t[i] = "";
            }
            Match m = new Match(t);
            this.matches.add(m);
            //haveMatch = true;
            Collections.sort(this.matches);
        }

    }

    private Vector<String[]> matchToUnitPriceAndQtyWithoutMakatAndDescription() {
        /*
         אם  יש התאמה במחיר ליחידה אך אין התאמה במקט או בתיאור הפריט 
         וגם בכל הרשומות שבהם המחיר ליחידה זהה מופיע אותו מקט ואותו תיאור פריט בכל העמודות בהן יכולים להופיע נתונים כאלה  או  שלא מופיעים בכלל נתונים אלה ברשומה
         וגם  סהכ כמות הפריט שסופקה אם מופיעה ברשומות או  סהכ הכמות שהוזמנה או סהכ הכמות המופיעה בהצעות מחיר   זהה לחלוטין לכמות הפריט שבמסמך.  
         אז נחליט שיש התאמה והמקט שבחשבונית הוא כנראה מקט ספק למוצר ואילו זה שברשומה הוא מקט לקוח או מקט יצרן.
         במקרה כזה נרשום גם את המקט ותיאור הפריט שמצאנו בקובץ ה           ORDERED_ITEMS שבפלט ובקובץ הDTL אך עם סימן אי וודאות בקצה השמאלי של השדה.
         */
        Vector<String[]> allMatches = new Vector<String[]>();
        Vector<String[]> tempMatches = new Vector<String[]>();
        if (unitpricecsu != null) {
            for (String[] line : unitpricecsu) {
                //compare price of item
                boolean isSamePricePerOne = false;
                if (block.one_price != null) {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                } else {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                }
                if (isSamePricePerOne) {
                    tempMatches.add(line);
                }
            }

            if (ifAllMatchesHaveSameMakatAndDescription(tempMatches) && tempMatches.size() >= 1) {
                for (String[] line : tempMatches) {
                    if (block.qty != null) {
                        try {
                            double qty = Double.valueOf(block.qty.get_result_str());
                            if (qty > 0 && (isCreditMISHFile || isCreditInvoice || isCreditPORDFile)) {
                                qty *= (-1);
                            }
                            boolean isSameQty = isSameQty(line, qty);
                            if (isSameQty) {
                                allMatches.add(line);
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }

            allMatches = deleteDuplicate(allMatches);
        }

        return allMatches;
    }

    private boolean ifAllMatchesHaveSameMakatAndDescription(Vector<String[]> tempMatches) {
        if (tempMatches.size() == 1) {
            return true;
        }
        for (int i = 1; i < tempMatches.size(); i++) {
            for (int index : INDEX_OF_PN) {
                if (!tempMatches.elementAt(i)[index].equals(tempMatches.elementAt(0)[index])) {
                    return false;
                }
            }
        }

        for (int i = 1; i < tempMatches.size(); i++) {
            for (int index : INDEX_OF_DESCRIPTION) {
                if (!tempMatches.elementAt(i)[index].equals(tempMatches.elementAt(0)[index])) {
                    return false;
                }
            }
        }

        return true;
    }

    private Vector<Match> matchToDescriptionOrMakatAndQtyWithoutUnitPrice() {
        /*  אם  יש התאמה במקט או בתיאור הפריט ולא מופיע בכלל ברשומה המחיר ליחידה
         וגם  סהכ כמות הפריט שסופקה אם מופיעה ברשומות או  סהכ הכמות שהוזמנה או סהכ הכמות המופיעה בהצעות מחיר  זהה לחלוטין לכמות הפריט שבמסמך
         אז נחליט שיש התאמה. */

        Vector<Match> allMatches = new Vector<Match>();
        if (makatcsu != null) {
            boolean dontHaveUnitPrice = false;
            for (String[] line : makatcsu) {
                for (ObData pn : block.listPn) {
                    if (pn.get_result_str().equals(line[0])) {
                        dontHaveUnitPrice = dontHaveUnitPrice(line);// if match dont have unit price
                        if (block.qty != null && dontHaveUnitPrice) {
                            try {
                                double qty = Double.valueOf(block.qty.get_result_str());
                                if (qty > 0 && (isCreditMISHFile || isCreditInvoice || isCreditPORDFile)) {
                                    qty *= (-1);
                                }
                                boolean isSameQty = isSameQty(line, qty);
                                if (isSameQty) {
                                    String[] desCsu;
                                    String[] blockDes = null;
                                    blockDes = block.get_description().split(" ");
                                    boolean isClose = false;
                                    desCsu = line[findFirstIndexNotEmpty(line, INDEX_OF_DESCRIPTION)].split(" ");
                                    //Compare item description
                                    for (int j = 0; j < desCsu.length && !isClose; j++) {
                                        if (desCsu[j].length() >= 3) {
                                            isClose = isCloseWord(desCsu[j], blockDes);
                                        }
                                    }
                                    Match m = new Match(line);
                                    if (!isClose) {
                                        m.set_partialDescriptionMatch(true);
                                    }
                                    allMatches.add(m);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
            allMatches = deleteDuplicate2(allMatches);
        }

        if (itemdesccsu != null) {
            for (String[] line : itemdesccsu) {
                String[] desCsu;
                String[] blockDes = null;
                blockDes = block.get_description().split(" ");
                boolean isClose = false;
                desCsu = line[0].split(" ");
                //Compare item description
                for (int j = 0; j < desCsu.length && !isClose; j++) {
                    if (desCsu[j].length() >= 3) {
                        isClose = isCloseWord(desCsu[j], blockDes);
                    }
                }
                if (isClose) {
                    boolean dontHaveUnitPrice = dontHaveUnitPrice(line);// if match dont have unit price
                    if (block.qty != null && dontHaveUnitPrice) {
                        try {
                            double qty = Double.valueOf(block.qty.get_result_str());
                            if (qty > 0 && (isCreditMISHFile || isCreditInvoice || isCreditPORDFile)) {
                                qty *= (-1);
                            }
                            boolean isSameQty = isSameQty(line, qty);
                            if (isSameQty) {
                                boolean isSamePn = false;
                                for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
                                    for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                                        if (LFun.checkChanges(line[INDEX_OF_PN.get(j)], block.listPn.elementAt(i).get_result_str())) {
                                            isSamePn = true;
                                            break;
                                        }
                                    }
                                }

                                Match m = new Match(line);
                                if (!isSamePn) {
                                    m.set_partialPnMatch(true);
                                }
                                allMatches.add(m);
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }

            allMatches = deleteDuplicate2(allMatches);
        }

        return allMatches;
    }

    private boolean isSameQty(String[] line, double qtyInInvoice) {
        boolean isSameQty = false;
        for (int index : INDEX_OF_QTY) {
            try {
                if (!line[index].equals("")) {
                    double qtyInMatch = Double.valueOf(line[index]);
                    if (qtyInMatch == qtyInInvoice) {
                        isSameQty = true;
                        break;
                    }
                }
            } catch (Exception e) {

            }
        }

        return isSameQty;
    }

    private Vector<Match> matchToUnitPriceAndDescriptionOrPn() {
        Vector<Match> allMatches = new Vector<Match>();
        if (unitpricecsu != null) {
            for (String[] line : unitpricecsu) {
                //compare price of item
                boolean isSamePricePerOne = false;
                if (block.one_price != null) {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                } else {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                }
                if (isSamePricePerOne) {
                    //compare pn's of item
                    boolean isSamePn = false;
                    for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
                        for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                            if (LFun.checkChanges(line[INDEX_OF_PN.get(j)], block.listPn.elementAt(i).get_result_str())) {
                                isSamePn = true;
                                break;
                            }
                        }
                    }

                    //compare description of item
                    String[] desCsu;
                    String[] blockDes = null;
                    blockDes = block.get_description().split(" ");
                    boolean isClose = false;
                    desCsu = line[findFirstIndexNotEmpty(line, INDEX_OF_DESCRIPTION)].split(" ");
                    //Compare item description
                    for (int j = 0; j < desCsu.length && !isClose; j++) {
                        if (desCsu[j].length() >= 3) {
                            isClose = isCloseWord(desCsu[j], blockDes);
                        }
                    }
                    if (isClose || isSamePn) {
                        Match m = new Match(line);
                        if (!isClose) {
                            m.set_partialDescriptionMatch(true);
                        }
                        if (!isSamePn) {
                            m.set_partialPnMatch(true);
                        }
                        allMatches.add(m);

                    }
                }
            }
            allMatches = deleteDuplicate2(allMatches);
        }

        if (makatcsu != null) {
            boolean isSamePricePerOne = false;
            for (String[] line : makatcsu) {
                for (ObData pn : block.listPn) {
                    if (pn.get_result_str().equals(line[0])) {
                        isSamePricePerOne = false;
                        //compare price of item
                        int index = findFirstIndexNotEmpty(line, INDEX_OF_PRICE);
                        if (block.one_price != null) {
                            try {
                                isSamePricePerOne = checktoler(Double.valueOf(line[index]), Double.valueOf(block.one_price.get_result_str()), tolerance);
                            } catch (Exception e) {

                            }
                        } else if (block.price != null) {
                            try {
                                isSamePricePerOne = checktoler(Double.valueOf(line[index]), Double.valueOf(block.price.get_result_str()), tolerance);
                            } catch (Exception e) {

                            }
                        }
                        if (isSamePricePerOne) {
                            Match m = new Match(line);
                            m.set_partialDescriptionMatch(true);
                            allMatches.add(m);
                        }
                    }
                }
            }
            allMatches = deleteDuplicate2(allMatches);
        }

        if (itemdesccsu != null) {
            for (String[] line : itemdesccsu) {
                String[] desCsu;
                String[] blockDes = null;
                blockDes = block.get_description().split(" ");
                boolean isClose = false;
                desCsu = line[0].split(" ");
                //Compare item description
                for (int j = 0; j < desCsu.length && !isClose; j++) {
                    if (desCsu[j].length() >= 3) {
                        isClose = isCloseWord(desCsu[j], blockDes);
                    }
                }
                if (isClose) {
                    //compare price of item
                    boolean isSamePricePerOne = false;
                    int index = findFirstIndexNotEmpty(line, INDEX_OF_PRICE);
                    if (block.one_price != null && !line[index].equals("")) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Double.valueOf(block.one_price.get_result_str()), tolerance);
                        } catch (Exception e) {

                        }
                    } else if (!line[index].equals("") && block.price != null) {
                        try {

                        } catch (Exception e) {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Double.valueOf(block.price.get_result_str()), tolerance);
                        }
                    }
                    if (isSamePricePerOne) {
                        Match m = new Match(line);
                        m.set_partialPnMatch(true);
                        allMatches.add(m);
                    }
                }
            }

            allMatches = deleteDuplicate2(allMatches);
        }

        return allMatches;
    }

    private Vector<String[]> matchToMakatCsu(BNode block) {

        Vector<String[]> matchToMakatCsu = new Vector<String[]>();
        boolean isSamePricePerOne = false;

        if (makatcsu != null) {
            for (String[] line : makatcsu) {
                //move over all pn that belong to the item
                for (ObData pn : block.listPn) {

                    if (pn.get_result_str().equals(line[0])) {
                        isSamePricePerOne = false;
                        int index = findFirstIndexNotEmpty(line, INDEX_OF_PRICE);
                        //compare price of item
                        if (block.one_price != null) {
                            try {
                                isSamePricePerOne = checktoler(Double.valueOf(line[index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                            } catch (Exception e) {

                            }
                        } else if (block.price != null) {
                            try {
                                isSamePricePerOne = checktoler(Double.valueOf(line[index]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                            } catch (Exception e) {

                            }
                        }
                        try {
                            if (!isSamePricePerOne && block.one_price != null) {
                                int QTY_index = findFirstIndexNotEmpty(line, INDEX_OF_QTY);
                                isSamePricePerOne = checktoler(Double.valueOf(line[35]) / Double.valueOf(line[QTY_index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                            }
                        } catch (Exception e) {
                        }
                        if (isSamePricePerOne) {
                            //compare description of item
                            String[] desCsu;
                            String[] blockDes = null;
                            //if(!block.get_description().equals("")){
                            blockDes = block.get_description().split(" ");
                            //}
                            //else if(!block.desCsu.equals(" ")){
                            //blockDes = block.desCsu.split(" ");
                            //}

                            //התאמה לdesc:
                            boolean isClose = false;
                            desCsu = line[findFirstIndexNotEmpty(line, INDEX_OF_DESCRIPTION)].split(" ");
                            //Compare item description
                            for (int j = 0; j < desCsu.length && !isClose; j++) {
                                if (desCsu[j].length() >= 3) {
                                    isClose = isCloseWord(desCsu[j], blockDes);
                                }
                            }

                            for (int j = 0; j < desCsu.length && !isClose; j++) {
                                String s = "";
                                try {
                                    for (int i = blockDes.length - 1; i >= 0; i--) {
                                        s += blockDes[i].replaceAll("\"", "").replaceAll("״", "");
                                        if (s.length() >= 3) {
                                            String[] s_arr = {s};
                                            isClose = isCloseWord(desCsu[j], s_arr);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            if (isClose) {
                                matchToMakatCsu.add(line);
                            }
                        }
                    }
                }
            }

            if (matchToMakatCsu.size() == 0) {
                for (String[] line : makatcsu) {
                    //move over all pn that belong to the item

                    isSamePricePerOne = false;
                    int index = findFirstIndexNotEmpty(line, INDEX_OF_PRICE);
                    //compare price of item
                    if (block.one_price != null) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                        } catch (Exception e) {

                        }
                    } else if (block.price != null) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                        } catch (Exception e) {

                        }
                    }
                    try {
                        if (!isSamePricePerOne && block.one_price != null) {
                            int QTY_index = findFirstIndexNotEmpty(line, INDEX_OF_QTY);
                            isSamePricePerOne = checktoler(Double.valueOf(line[35]) / Double.valueOf(line[QTY_index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                        }
                    } catch (Exception e) {
                    }
                    if (isSamePricePerOne) {
                        //compare description of item
                        String[] desCsu;
                        String[] blockDes = null;
                        //if(!block.get_description().equals("")){
                        blockDes = block.get_description().split(" ");
                        //}
                        //else if(!block.desCsu.equals(" ")){
                        //blockDes = block.desCsu.split(" ");
                        //}

                        //התאמה לdesc:
                        boolean isClose = false;
                        desCsu = line[findFirstIndexNotEmpty(line, INDEX_OF_DESCRIPTION)].split(" ");
                        //Compare item description
                        for (int j = 0; j < desCsu.length && !isClose; j++) {
                            if (desCsu[j].length() >= 3) {
                                isClose = isCloseWord(desCsu[j], blockDes);
                            }
                        }

                        for (int j = 0; j < desCsu.length && !isClose; j++) {
                            String s = "";
                            try {
                                for (int i = blockDes.length - 1; i >= 0; i--) {
                                    s += blockDes[i].replaceAll("\"", "").replaceAll("״", "");
                                    if (s.length() >= 3) {
                                        String[] s_arr = {s};
                                        isClose = isCloseWord(desCsu[j], s_arr);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        if (isClose) {
                            matchToMakatCsu.add(line);
                        }
                    }

                }
            }
        }

        matchToMakatCsu = deleteDuplicate(matchToMakatCsu);

        //many to many
        if (matchToMakatCsu.size() > 1) {
            int moneTheSameLine = 0;
            for (int i = 0; i < matchToMakatCsu.size(); i++) {
                try {
                    String[] line = matchToMakatCsu.get(i);
                    if (makatcsu.contains(line)) {
                        if (block.one_price != null) {
                            int QTY_index = findFirstIndexNotEmpty(line, INDEX_OF_QTY);
                            int Price_index = 35;
                            int OnePrice_index = INDEX_OF_PRICE.get(0);
                            if (checktoler(Double.valueOf(line[Price_index]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance)
                                    && checktoler(Double.valueOf(line[OnePrice_index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance)
                                    && Math.abs(Double.valueOf(line[QTY_index]) - Double.valueOf(block.qty.get_result_str())) < tolerance) {

                                moneTheSameLine++;
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (moneTheSameLine >= 1) {
                moneTheSameLine = 0;
                for (int i = 0; i < matchToMakatCsu.size(); i++) {
                    try {
                        String[] line = matchToMakatCsu.get(i);
                        if (makatcsu.contains(line)) {
                            if (block.one_price != null) {
                                int QTY_index = findFirstIndexNotEmpty(line, INDEX_OF_QTY);
                                int Price_index = 35;
                                int OnePrice_index = INDEX_OF_PRICE.get(0);
                                if (checktoler(Double.valueOf(line[Price_index]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance)
                                        && checktoler(Double.valueOf(line[OnePrice_index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance)
                                        && Math.abs(Double.valueOf(line[QTY_index]) - Double.valueOf(block.qty.get_result_str())) < tolerance) {

                                    if (moneTheSameLine > 0) {
                                        makatcsu.remove(line);
                                        matchToMakatCsu.remove(line);
                                        i--;
                                    }
                                    moneTheSameLine++;
                                } else {
                                    matchToMakatCsu.remove(line);
                                    i--;
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return matchToMakatCsu;
    }

    private void matchToOrderedItemsCSV(BNode block) {
        if (orderedItemsCSV != null && infoReader != null && infoReader.SupplierID != null && infoReader.SupplierID.length() > 2) {
            final int wisepage_output_size = 11;
            boolean allok = true;
            for (int i = 0; i < orderedItemsCSV.size(); i++) {
                if (orderedItemsCSV.get(i)[IndexesOfCsv.indexOfSupplierID] != null && orderedItemsCSV.get(i)[IndexesOfCsv.indexOfSupplierID].length() > 2
                        && infoReader.SupplierID.trim().equals(orderedItemsCSV.get(i)[IndexesOfCsv.indexOfSupplierID].trim())) {
                    String[] bigger_arr = new String[wisepage_output_size + orderedItemsCSV.get(i).length];
                    for (int j = 0; j <= wisepage_output_size; j++) {
                        bigger_arr[j] = "##";
                    }

                    for (int j = 0; j < orderedItemsCSV.get(i).length; j++) {
                        bigger_arr[wisepage_output_size + j] = orderedItemsCSV.get(i)[j];
                    }

                    Match m = new Match(bigger_arr);
                    //Match m = new Match(orderedItemsCSV.get(i));
                    this.matches.add(m);
                }
            }

            final int indexOfItemTotalPriceExcludingVat = 24;
            final int indexOfItemQUANTITY = 10;

            double sum_matches = 0;
            for (int i = 0; i < this.matches.size(); i++) {
                try {
                    if (this.matches.get(i).match[indexOfItemTotalPriceExcludingVat + wisepage_output_size] != null && !this.matches.get(0).match[i].isEmpty()) {
                        sum_matches += (Double.valueOf(this.matches.get(i).match[indexOfItemTotalPriceExcludingVat + wisepage_output_size])
                                * Double.valueOf(this.matches.get(i).match[indexOfItemQUANTITY + wisepage_output_size]));
                    } else {
                        allok = false;
                    }
                } catch (Exception e) {
                    this.matches.remove(i--);
                    //e.printStackTrace();
                    //allok = false;
                }

            }

            double sum_block = 0;
            try {
                sum_block = Double.valueOf(block.price.get_result_str());
            } catch (Exception e) {
                e.printStackTrace();
                allok = false;
            }

            if (!allok || Math.abs(sum_matches - sum_block) > 1 && Math.abs(sum_matches) > 0) {
                this.matches.clear();
            } else {
                Collections.sort(this.matches);
                matchToOrderedItemsCSVbySupplierID = true;
                //foundMatches = true;
            }
        }
    }

    private void matchToOrderedItemsCSVOneLine(BNode block, MatchType _match_type) {
        if (orderedItemsCSV != null && infoReader != null
                && ((infoReader.SupplierID != null && infoReader.SupplierID.length() > 2) || _match_type.equals(MatchType.ManyLinesToManyLinesNoSupp))) {
            final int wisepage_output_size = 11;
            boolean allok = true;
            for (int i = 0; i < orderedItemsCSV.size(); i++) {
                try {
                    if ((orderedItemsCSV.get(i)[IndexesOfCsv.indexOfSupplierID] != null && orderedItemsCSV.get(i)[IndexesOfCsv.indexOfSupplierID].length() > 2
                            && infoReader.SupplierID.trim().equals(orderedItemsCSV.get(i)[IndexesOfCsv.indexOfSupplierID].trim())
                            && infoReader.totalInvoiceSum13 != 0 && infoReader.totalInvoiceSum13 == Double.valueOf(orderedItemsCSV.get(i)[33]))
                            //                          or
                            || (_match_type.equals(MatchType.ManyLinesToManyLinesNoSupp) && infoReader.totalInvoiceSum13 != 0 && infoReader.totalInvoiceSum13 == Double.valueOf(orderedItemsCSV.get(i)[33]))) {
                        String[] bigger_arr = new String[wisepage_output_size + orderedItemsCSV.get(i).length];
                        for (int j = 0; j <= wisepage_output_size; j++) {
                            bigger_arr[j] = "##";
                        }

                        for (int j = 0; j < orderedItemsCSV.get(i).length; j++) {
                            bigger_arr[wisepage_output_size + j] = orderedItemsCSV.get(i)[j];
                        }

                        Match m = new Match(bigger_arr);
                        //Match m = new Match(orderedItemsCSV.get(i));
                        this.matches.add(m);
                    }
                } catch (Exception e) {
                }
            }
            boolean isSameSupp = true;
            if (_match_type.equals(MatchType.ManyLinesToManyLinesNoSupp)) {
                for (int i = 1; i < this.matches.size(); i++) {
                    if(!this.matches.get(i).match[24].trim().equalsIgnoreCase(this.matches.get(0).match[24].trim())){
                        isSameSupp = false;
                    }
                }
            }

            if (this.matches.size() != 1) {
                if (isSameSupp && !_match_type.equals(MatchType.ManyLinesToManyLines) && !_match_type.equals(MatchType.ManyLinesToManyLinesNoSupp)) {
                    this.matches.clear();
                } else {
                    matchToOrderedItemsCSVbySupplierID = true;
                    matchToOrderedItemsCSVbyCSVManyLine = true;
                    this.block.rate = null;
                }
            } else {
                Collections.sort(this.matches);
                matchToOrderedItemsCSVbySupplierID = true;
                matchToOrderedItemsCSVbyCSVOneLine = true;
                this.block.rate = null;
                //foundMatches = true;
            }
        }
    }

    private Vector<String[]> matchToDescriptionCsu(BNode block) {

        Vector<String[]> matchToDescriptionCsu = new Vector<String[]>();
        if (itemdesccsu != null) {
            for (String[] line : itemdesccsu) {
                String[] desCsu;
                String[] blockDes = null;
                //if(!block.get_description().equals("")){
                blockDes = block.get_description().split(" ");
                //}
                //else if(!block.desCsu.equals(" ")){
                //blockDes = block.desCsu.split(" ");
                //}
                boolean isClose = false;
                desCsu = line[0].split(" ");
                //Compare item description
                for (int j = 0; j < desCsu.length && !isClose; j++) {
                    if (desCsu[j].length() >= 3) {
                        isClose = isCloseWord(desCsu[j], blockDes);
                    }
                }
                if (isClose) {
                    //compare price of item
                    boolean isSamePricePerOne = false;
                    int index = findFirstIndexNotEmpty(line, INDEX_OF_PRICE);
                    if (block.one_price != null && !line[index].equals("")) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                        } catch (Exception e) {

                        }

                    } else if (!line[index].equals("") && block.price != null) {
                        try {
                            isSamePricePerOne = checktoler(Double.valueOf(line[index]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                        } catch (Exception e) {

                        }
                    }
                    if (isSamePricePerOne) {
                        //compare pn's of item
                        boolean isSamePn = false;
                        for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
                            for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                                int indexOfPn = INDEX_OF_PN.get(j);
                                if (LFun.checkChanges(line[indexOfPn], block.listPn.elementAt(i).get_result_str())) {
                                    isSamePn = true;
                                    matchToDescriptionCsu.add(line);
                                }
                            }
                        }
                    }
                }
            }
        }
        return matchToDescriptionCsu;
    }

    private Vector<String[]> matchTomatchToUnitPriceCsuBySupplierId(BNode block) {
        Vector<String[]> matchToUnitPriceCsu = new Vector<String[]>();

        if (unitpricecsu != null) {
            for (String[] line : unitpricecsu) {
                //compare price of item
                boolean isSamePricePerOne = false;
                //zvika 30.9.18
                if (block.one_price != null) {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                } else {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                }

                if (isSamePricePerOne && SupplierId != null && SupplierId.equalsIgnoreCase(line[24])) {
                    matchToUnitPriceCsu.add(line);
                }
            }
        }

        return matchToUnitPriceCsu;

    }

    private Vector<String[]> matchToUnitPriceCsu(BNode block) {
        Vector<String[]> matchToUnitPriceCsu = new Vector<String[]>();

        if (unitpricecsu != null) {
            for (String[] line : unitpricecsu) {
                //compare price of item
                boolean isSamePricePerOne = false;
                //zvika 30.9.18
                if (block.one_price != null) {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.one_price.get_result_str())), tolerance);
                } else {
                    isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                }

                if (isSamePricePerOne) {
                    //compare pn's of item
                    boolean isSamePn = false;
                    for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
                        for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                            if (LFun.checkChanges(line[INDEX_OF_PN.get(j)], block.listPn.elementAt(i).get_result_str())) {
                                isSamePn = true;
                            }
                        }
                    }
                    if (isSamePn) {
                        //compare description of item
                        String[] desCsu;
                        String[] blockDes = null;
                        //if(!block.get_description().equals("")){
                        blockDes = block.get_description().split(" ");
                        //}
                        //else if(!block.desCsu.equals(" ")){
                        //blockDes = block.desCsu.split(" ");
                        //}
                        boolean isClose = false;
                        desCsu = line[findFirstIndexNotEmpty(line, INDEX_OF_DESCRIPTION)].split(" ");
                        //Compare item description
                        for (int j = 0; j < desCsu.length && !isClose; j++) {
                            if (desCsu[j].length() >= 3) {
                                isClose = isCloseWord(desCsu[j], blockDes);
                            }
                        }
                        if (isClose) {
                            matchToUnitPriceCsu.add(line);
                        }
                    }
                }
            }
            if (matchToUnitPriceCsu.size() == 0) {
                for (String[] line : unitpricecsu) {
                    //compare price of item
                    boolean isSamePricePerOne = false;
                    //zvika 30.9.18
                    if (!isSamePricePerOne) {
                        isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(block.price.get_result_str())), tolerance);
                    }
                    if (block.one_price != null) {
                        isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(GlobalVariables.ExchangeRate_Genral_to_NIS(block.one_price.get_result_str()))), tolerance);
                    }
                    if (!isSamePricePerOne) {
                        isSamePricePerOne = checktoler(Double.valueOf(line[0]), Math.abs(Double.valueOf(GlobalVariables.ExchangeRate_Genral_to_NIS(block.price.get_result_str()))), tolerance);
                    }

                    if (isSamePricePerOne) {
                        //compare pn's of item
                        boolean isSamePn = false;
                        for (int i = 0; i < block.listPn.size() && !isSamePn; i++) {
                            for (int j = 0; j < INDEX_OF_PN.size() && !isSamePn; j++) {
                                if (LFun.checkChanges(line[INDEX_OF_PN.get(j)], block.listPn.elementAt(i).get_result_str())) {
                                    isSamePn = true;
                                }
                            }
                        }
                        if (isSamePn) {
                            //compare description of item
                            String[] desCsu;
                            String[] blockDes = null;
                            //if(!block.get_description().equals("")){
                            blockDes = block.get_description().split(" ");
                            //}
                            //else if(!block.desCsu.equals(" ")){
                            //blockDes = block.desCsu.split(" ");
                            //}
                            boolean isClose = false;
                            desCsu = line[findFirstIndexNotEmpty(line, INDEX_OF_DESCRIPTION)].split(" ");
                            //Compare item description
                            for (int j = 0; j < desCsu.length && !isClose; j++) {
                                if (desCsu[j].length() >= 3) {
                                    isClose = isCloseWord(desCsu[j], blockDes);
                                }
                            }
                            if (isClose) {
                                matchToUnitPriceCsu.add(line);
                            }
                        }
                    }
                }
            }
        }

        return matchToUnitPriceCsu;
    }

    public static boolean isCloseWord(String word, String[] blockDes) {
        boolean isClose = false;
        for (int i = 0; i < blockDes.length && !isClose; i++) {
            if (word.length() == blockDes[i].length()) {
                isClose = LFun.checkChanges(word, blockDes[i]);
            }
        }
        return isClose;
    }

    /**
     *
     * @return Vector<String[]> without duplicate
     */
    private Vector<String[]> deleteDuplicate(Vector<String[]> v) {
        for (int i = 0; i < v.size(); i++) {
            for (int j = 0; j < v.size(); j++) {
                if (i != j) {
                    if (isSame(v.get(i), v.get(j))) {
                        v.remove(j);
                        j--;
                    }
                }
            }
        }

        return v;
    }

    private Vector<Match> deleteDuplicate2(Vector<Match> v) {
        for (int i = 0; i < v.size(); i++) {
            for (int j = 0; j < v.size(); j++) {
                if (i != j) {
                    if (isSame(v.get(i).match, v.get(j).match)) {
                        v.remove(j);
                        j--;
                    }
                }
            }
        }

        return v;
    }

    private boolean isSame(String[] v1, String[] v2) {
        boolean isSame = true;
        if (v1.length == v2.length) {
            for (int i = 11; i < v1.length && isSame; i++) {
                if (!v1[i].equalsIgnoreCase(v2[i])) {
                    isSame = false;
                }
            }
        }

        return isSame;
    }

    /**
     * If the number in the csv is shorter then the number we found - we will do
     * match just with the right chars
     *
     * @param numberInCsv
     * @return true if match
     */
    private boolean isMatchInvoiceNumber(String numberInCsv) {
        boolean isMatch = false;
        if (infoReader != null && infoReader.Invoice_ID12.length() == numberInCsv.length()) {
            isMatch = infoReader.Invoice_ID12.equals(numberInCsv);
        } else if (infoReader != null && infoReader.Invoice_ID12.length() > numberInCsv.length()) {
            boolean toContinue = true;
            for (int i = numberInCsv.length() - 1; i > 0 && toContinue; i++) {
                if (numberInCsv.charAt(i) != infoReader.Invoice_ID12.charAt(i)) {
                    toContinue = false;
                }
            }
            if (toContinue) {
                isMatch = true;
            }
        }

        return isMatch;
    }

    private boolean isCreditMatch(String[] match) {
        boolean isCreditMatch = false;
        double num = 0;
        int[] indexes = {14, 21, 23, 35, 41, 42, 43, 44, 56, 58, 61, 66, 71, 87, 88, 89, 98, 99, 100, 111, 112, 113};

        for (int i = 0; i < indexes.length; i++) {
            if (!match[indexes[i]].equals("")) {
                try {
                    num = Double.valueOf(match[indexes[i]]);
                    if (num < 0) {
                        isCreditMatch = true;
                        break;
                    }
                } catch (Exception e) {
                }
            }
        }

        return isCreditMatch;
    }

    private boolean checktoler(double p1, double p2, double tol) {
        double t = Math.abs(Math.abs(Math.max(p1, p2) / Math.min(p1, p2) - 1));
        if (t <= tol && ((p1 < 0 && p2 < 0) || (p1 > 0 && p2 > 0))) {
            return true;
        }
        return false;
    }

    private boolean dontHaveUnitPrice(String[] line) {
        boolean dontHaveUnitPrice = true;
        for (int index : INDEX_OF_PRICE) {
            if (!line[index].equals("")) {
                dontHaveUnitPrice = false;
                break;
            }
        }

        return dontHaveUnitPrice;
    }

    public String getPnStrFromMatches() {
        String result = "";
        Vector<String> PNS = new Vector<String>();
        for (Match match : matches) {
            for (int index : INDEX_OF_PN) {
                if (!PNS.contains(match.match[index]) && !match.match[index].equals("")) {
                    PNS.add(match.match[index]);
                }
            }
        }

        for (int i = 0; i < PNS.size(); i++) {
            result += PNS.elementAt(i);
            if (i != PNS.size() - 1) {
                result += " ";
            }
        }

        return result;
    }

    public String getDescriptionStrFromMatches() {
        String des = matches.elementAt(0).match[MatchesToOneItem.findFirstIndexNotEmpty(matches.elementAt(0).match, INDEX_OF_DESCRIPTION)];

        return des;
    }

    static int getIndexInOrderedItemsCSV(Vector<String[]> orderedItemsCSV, String[] match) {
        int index = -1;
        boolean IsSame = false;
        for (int i = 0; i < orderedItemsCSV.size() && !IsSame; i++) {
            for (int j = 0; j < match.length && !IsSame; j++) {
                //113,112 הם השדות של יתרת התקציב שמשתנות ולכן הם לא צריכים להיות זהים למה שהיה
                if (!orderedItemsCSV.elementAt(i)[j].trim().equals(match[j].trim()) && j != 112 && j != 113) {
                    break;
                } else if (orderedItemsCSV.elementAt(i)[j].trim().equals(match[j].trim()) && j == match.length - 3) {
                    IsSame = true;
                    index = i;
                }
            }
        }

        return index;
    }

    static Vector<String[]> updateBalanceOgBudgetInAllRows(Vector<String[]> orderedItemsCSV, String[] lineToUpdate) {

        for (int i = 0; i < orderedItemsCSV.size(); i++) {
            if (lineToUpdate[7].equals(orderedItemsCSV.elementAt(i)[7])) {
                //יתרת תקציב ללא מעמ
                orderedItemsCSV.elementAt(i)[112] = lineToUpdate[112];
                // יתרת תקציב עם מעמ
                orderedItemsCSV.elementAt(i)[113] = lineToUpdate[113];
            }
        }

        return orderedItemsCSV;
    }

    public static int findFirstIndexNotEmpty(String[] line, Vector<Integer> listOfIndexes) {
        int index = listOfIndexes.elementAt(0);
        for (int i = 0; i < listOfIndexes.size(); i++) {
            if (!line[listOfIndexes.elementAt(i)].equals("")) {
                index = listOfIndexes.elementAt(i);
                break;
            }
        }
        return index;
    }
}
