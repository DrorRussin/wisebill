package CSV;

import java.util.HashMap;

/**
 *
 * @author YAEL-MOBILE
 */
public class SaveLastFields {

    static HashMap<String, String> LastFields = new HashMap<String, String>();

    public static String getLastFields(String key) {
        try {
            if (LastFields.get("?" + key) != null) {
                return LastFields.get("?" + key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ",,,,";
    }

    public static String getLast2Fields(String key) {
        try {
            if (LastFields.get("#" + key) != null) {
                return LastFields.get("#" + key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ",";
    }

    public static void putLastFields(String key, String last_fields) {
        try {
            LastFields.put(key, last_fields);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String[] add_ITEM_CATNO(String[] data) {
        putLastFields("?" + data[24] + "," + data[25], data[125] + "," + data[126] + "," + data[127] + "," + data[128]);
        putLastFields("#" + data[24] + "," + data[25], data[127] + "," + data[128]);

        //remove the last two var:
        String[] newData = new String[data.length - 4];
        for (int i = 0; i < newData.length; i++) {
            newData[i] = data[i];
        }
        return newData;
    }
}
