/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

/**
 *
 * @author YAEL-MOBILE
 */
public class Match implements Comparable<Match>{
    
    public String[] match = null; // ההתאמה
    private double qtyConnect = 0; // במידה וההתאמה קשורה אז מהי הכמות הקשורה
    private boolean needNewLine = false; // האם נדרש לפתוח שורה חדשה
    private double qtyToNewLine = 0; //כמות שנדרשת לשורה חדשה
    //private double leftToSupplyInMatch = 0; // כמה פריטים נותרו לספק על ידי חשבונית
    private boolean connect = true; // האם ההתאמה קשורה לחשבונית או תעודת משלוח או הזמנה
    private boolean partialPnMatch = false; // התאמה חלקית למקט
    private boolean partialDescriptionMatch = false; // התאמה חלקית לתיאור פריט
    private boolean allConnect = false; // האם כל הרשומה שייכת לחשבונית או תעודת משלוח או הזמנה
    private boolean connectToPrevious = false; // האם הרשומה שייכת לחשבונית או תעודת משלוח או הזמנה קודמת
    private boolean connectToPreviousProformalInvoice = false; // האם הרשומה שייכת לחשבונית עסקה קודמת
    private boolean isFullyCreditMatchConnect = false; // האם הרשומה היא רשומת זיכוי המתאימה באופן מלא לחשבונית או תעודת משלוח או הזמנה
    private boolean dontHaveSupak = false; // אין נתונים לגבי מה סופק בפועל
    private boolean dontHaveOrderDtl = false; // אין נתונים לגבי הזמנה
    private boolean dontHaveMishDtl = false; // אין נתונים לגבי תעודת משלוח
    
    public Match(String[] i_Match){
        this.match = i_Match;
    }
    
    public boolean get_connect(){
        return connect; 
    }
    
    public void set_connect(boolean i_Connect){
        this.connect = i_Connect;
    }
    
    public boolean get_dontHaveSupak(){
        return dontHaveSupak; 
    }
    
    public void set_dontHaveSupak(boolean i_dontHaveSupak){
        this.dontHaveSupak = i_dontHaveSupak;
    }
    
    public boolean get_dontHaveOrderDtl(){
        return dontHaveOrderDtl; 
    }
    
    public void set_dontHaveOrderDtl(boolean i_dontHaveOrderDtl){
        this.dontHaveOrderDtl = i_dontHaveOrderDtl;
    }
    
    public boolean get_dontHaveMishDtl(){
        return dontHaveMishDtl; 
    }
    
    public void set_dontHaveMishDtl(boolean i_dontHaveMishDtl){
        this.dontHaveMishDtl = i_dontHaveMishDtl;
    }

    public boolean get_isFullyCreditMatchConnect(){
        return isFullyCreditMatchConnect; 
    }
    
    public void set_isFullyCreditMatchConnect(boolean i_isFullyCreditMatchConnect){
        this.isFullyCreditMatchConnect = i_isFullyCreditMatchConnect;
    }
    
    public boolean get_connectToPreviousProformalInvoice(){
        return connectToPreviousProformalInvoice; 
    }
    
    public void set_connectToPreviousProformalInvoice(boolean i_connectToPreviousProformalInvoice){
        this.connectToPreviousProformalInvoice = i_connectToPreviousProformalInvoice;
    }
    
    public double get_qtyConnect(){
        return qtyConnect; 
    }
    
    public void set_qtyConnect(double i_QtyConnect){
        this.qtyConnect = i_QtyConnect;
    }
    
    public double get_qtyToNewLine(){
        return qtyToNewLine; 
    }
    
    public void set_qtyToNewLine(double i_QtyToNewLine){
        this.qtyToNewLine = i_QtyToNewLine;
    }
    
    public boolean get_allConnect(){
        return allConnect; 
    }
    
    public void set_allConnect(boolean i_AllConnect){
        this.allConnect= i_AllConnect;
    }
    
    public boolean get_connectToPrevious(){
        return connectToPrevious; 
    }
    
    public void set_connectToPrevious(boolean i_ConnectToPrevious){
        this.connectToPrevious = i_ConnectToPrevious;
    }
    
    public boolean get_needNewLine(){
        return needNewLine; 
    }
    
    public void set_needNewLine(boolean i_NeedNewLine){
        this.needNewLine = i_NeedNewLine;
    }
    
    public boolean get_partialPnMatch(){
        return partialPnMatch; 
    }
    
    public void set_partialPnMatch(boolean i_PartialPnMatch){
        this.partialPnMatch = i_PartialPnMatch;
    }
    
    public boolean get_partialDescriptionMatch(){
        return partialDescriptionMatch; 
    }
    
    public void set_partialDescriptionMatch(boolean i_PartialDescriptionMatch){
        this.partialDescriptionMatch = i_PartialDescriptionMatch;
    }

    @Override
    public int compareTo(Match t) {

        // First by Supply Certificate - stop if this gives a result.
        int supplyCertificateResult = this.match[11].compareTo(t.match[11]);
        if (supplyCertificateResult != 0)
        {
            return supplyCertificateResult;
        }

        // Next by PO number
        int poNumberResult = this.match[18].compareTo(t.match[18]);
        if (poNumberResult != 0)
        {
            return poNumberResult;
        }

        // Finally by invoice number if exists
        if(!this.match[103].equals("") && !t.match[103].equals("")){
            return this.match[103].compareTo(t.match[103]);
        }else if(!this.match[103].equals("") && t.match[103].equals("")){
            return -1;
        }else if(this.match[103].equals("") && !t.match[103].equals("")){
            return 1;
        }
        
        return poNumberResult;
    }
}
