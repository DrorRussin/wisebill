/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hyperOCR;

import Management.SConsole;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;

/**
 *
 * @author ERA (Work)
 */
public class wiserun {

    public static void wiserun(Vector<String> comv) {
        String s = "[WiseRun Options]";
        for (int i = 0; i < comv.size(); i++) {

            String t = "\r\nwiseproc" + (i + 1) + "=" + comv.elementAt(i);

            s = s + t;
        }
        s = s + "\r\n\r\nQUOTE={";
        //[Installation_path]WiseRUN.exe  "+Wise_Tools.wise_folder_path+"\\ini\\wiserun_wf.ini 

        File tt = new File("C:\\HyperOCR\\ini\\wiseruntemp.ini");
        if (tt.exists()) {
            tt.delete();
        }

        try {

            tt.createNewFile();
            FileOutputStream fos = new FileOutputStream(tt);

            fos.write(s.getBytes());

            fos.close();

            //  String st = space_fix(wise_folder_path + "\\WiseRUN.exe") + space_fix(wise_folder_path + "\\ini\\wiserun_temp.ini");
            String command = space_fix(WiseConfig.Config.getWiseRunExePath()) + space_fix("wiseruntemp.ini");
            if (!command.equals("")) {
                System.out.println(command);
                try {
                    SConsole.process[0] = Runtime.getRuntime().exec(command); System.out.println(command);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                System.out.println("no command");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String space_fix(String k) {
        k = "\"" + k + "\" ";
        // k=k+" ";
        return k;
    }

    public static String space_fix_6(String k) {
        k = "{" + k + "{ ";

        return k;
    }
}
