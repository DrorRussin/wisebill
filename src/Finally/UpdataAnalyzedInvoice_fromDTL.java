/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Finally;

import static CSV.LFun.convert2Unicode;
import CSV.SaveData;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;

/**
 *
 * @author zvika
 */
public class UpdataAnalyzedInvoice_fromDTL {
    
    /*public static void main(String[] args) {
        WiseConfig.Config config = new WiseConfig.Config(args);
        String Folder = config.getWisePageOutput();
        String[][] listFilesDtl = findDtlFiles(Folder);
        String[][] listFilesDtlAndCsv = findCSVFiles(Folder, listFilesDtl);

        for (int i = 0; i < listFilesDtlAndCsv.length; i++) {
            updateCSV(listFilesDtlAndCsv[i][0], listFilesDtlAndCsv[i][1]);
        }

    }*/

    public static void updateCSV(String dtl_path, String csv_path) {
        DefaultListModel<String> DTL_model = readFile(new File(dtl_path), SaveData.DTL_FORMAT);
        DefaultListModel<String> CSV_model = readFile(new File(csv_path), SaveData.CSV_FORMAT);

        Vector<String[]> DTL_StringArrays = DefaultListModel2StringArrays(DTL_model);
        Vector<String[]> CSV_StringArrays = DefaultListModel2StringArrays(CSV_model);

        for (int i = 1; i < DTL_StringArrays.size(); i++) {
            for (int j = 1; j < CSV_StringArrays.size(); j++) {
                String dtl_item = DTL_StringArrays.get(i)[14];
                String csv_item = CSV_StringArrays.get(i)[0];

                double dtl_PRICE_in_PO = Double.valueOf(DTL_StringArrays.get(i)[23]);
                double csv_PRICE_in_PO = Double.valueOf(CSV_StringArrays.get(i)[3]);

                if (dtl_item.toLowerCase().equals(csv_item.toLowerCase())
                        && Math.abs(dtl_PRICE_in_PO - csv_PRICE_in_PO) < 0.10) {
                    //System.out.println("CSV-"+i+", DTL-"+j);

                    if (CSV_StringArrays.get(i)[39] == null || CSV_StringArrays.get(i)[39].length() < 2) {//ACCOUNTING_COSTING_TYPE
                        CSV_StringArrays.get(i)[39] = DTL_StringArrays.get(i)[116];
                    }
                    if (CSV_StringArrays.get(i)[40] == null || CSV_StringArrays.get(i)[40].length() < 2) {//ACCOUNTING_BATCH_NUMBER
                        CSV_StringArrays.get(i)[40] = DTL_StringArrays.get(i)[117];
                    }

                    j = CSV_StringArrays.size();
                }
            }
        }
        
        //save csv:
        CSV_model.clear();
        for (String[] csv_line: CSV_StringArrays) {
            CSV_model.addElement(StringArray2String(csv_line));
        }
        writeModelToFile(new File(csv_path), CSV_model, SaveData.CSV_FORMAT);
    }

    public static Vector<String[]> DefaultListModel2StringArrays(DefaultListModel<String> model) {
        Vector<String[]> tablefile = new Vector<String[]>();
        for (int i = 0; i < model.size(); i++) {
            Pattern patt = Pattern.compile(",\"(.*)\",");
            Matcher m = patt.matcher(model.elementAt(i).replaceAll("\"", ""));
            while (m.find()) {
                model.set(i, model.elementAt(i).replace(m.group(1), m.group(1).replace(",", ";")));
            }
            String[] arr = model.elementAt(i).split("\\,", -1);
            tablefile.add(arr);
        }
        return tablefile;
    }

    public static String[][] findDtlFiles(String Folder) {
        File[] listFilesDtl = new File(Folder).listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().toLowerCase().endsWith(".dtl");
            }
        });

        String[][] listDtlAndCsv = new String[listFilesDtl.length][2];
        for (int i = 0; i < listFilesDtl.length; i++) {
            listDtlAndCsv[i][0] = listFilesDtl[i].getAbsolutePath();

        }
        return listDtlAndCsv;
    }

    public static String[][] findCSVFiles(String Folder, String[][] listFilesDtlAndCsv) {
        File[] listFilesCsv = new File(Folder).listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().toLowerCase().endsWith(".csv");
            }
        });

        for (int i = 0; i < listFilesDtlAndCsv.length; i++) {
            for (int j = 0; j < listFilesCsv.length; j++) {
                if (listFilesCsv[j].getAbsolutePath().toLowerCase().startsWith(listFilesDtlAndCsv[i][0].substring(0, listFilesDtlAndCsv[i][0].indexOf(";")).toLowerCase())) {
                    listFilesDtlAndCsv[i][1] = listFilesCsv[j].getAbsolutePath();
                    //listFilesDtlAndCsv[i][1] = convert2Unicode(listFilesDtlAndCsv[i][1]);

                    j = listFilesDtlAndCsv.length;//break
                }

            }
        }
        return listFilesDtlAndCsv;
    }

    public static String StringArray2String(String[] arr) {
        String s = "";
        if (arr != null) {
            if (arr.length > 0) {
                s = arr[0];
            }
            for (int i = 1; i < arr.length; i++) {
                s+=","+arr[i];
            }
        }
        return s;
    }

    public static boolean writeModelToFile(File file, DefaultListModel list, String Type) {
        FileWriter File_Writer;
        BufferedWriter Buffered_Writer;

        String line;
        int size = list.getSize();

        try {
            if (list == null) {
                return false;
            }
            if (file == null) {
                return false;
            }
            if (!file.exists()) {
                file.createNewFile();
            }
            if (!file.exists()) {
                return false;
            }

            Buffered_Writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), Type));

            for (int i = 0; i < size; i++) {
                line = list.getElementAt(i).toString();
                if (line != null && !line.equals("")) {
                    Buffered_Writer.write(line.trim());
                    Buffered_Writer.newLine();
                }
            }

            Buffered_Writer.close();

        } catch (IOException ex) {
            //ex.printStackTrace();
            System.err.println("Error in writeModelToFile()");
            return false;
        }
        return true;
    }

    public static DefaultListModel<String> readFile(File f, String Type) {
        String line;
        DefaultListModel<String> model = new DefaultListModel<String>();
        FileReader file;
        BufferedReader Reader;
        if (!f.exists()) {
            return model;
        }

        try {
            Reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), Type));

            line = Reader.readLine();
            while (line != null) {
                if (line == null || line.length() < 1) {
                    line = " ";
                }
                model.addElement(line);

                line = Reader.readLine();
            }

            Reader.close();
            //file.close();

        } catch (IOException ex) {
            System.err.println(ex);
            return model;
        }
        return model;
    }
}
