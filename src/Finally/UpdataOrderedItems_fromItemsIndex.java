package Finally;

import CSV.SaveData;
import static Finally.UpdataAnalyzedInvoice_fromDTL.DefaultListModel2StringArrays;
import static Finally.UpdataAnalyzedInvoice_fromDTL.StringArray2String;
import static Finally.UpdataAnalyzedInvoice_fromDTL.readFile;
import static Finally.UpdataAnalyzedInvoice_fromDTL.writeModelToFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;

/**
 *
 * @author YAEL-MOBILE
 */
public class UpdataOrderedItems_fromItemsIndex {

    /*     public static int[] Customer_ITEM_CATNO = {15, 1};
    public static int[] Supplier_ITEM_CATNO = {16, 2};
    public static int[] Manufacturer_ITEM_CATNO = {17, 3};
    public static int[] ITEM_DESCRIPTION_in_PO = {18, 4};
    public static int[] SUPPLIER_ID_IN_ERP = {14, 5};
    public static int[] ITEM_UNIT_PRICE_in_PO = {24, 6, 8};//<-------
    public static int[] CURRENCY_of_ITEM_in_PO = {23, 7};

    public static int[] CURRENCY_CONVERSION_RATE_between_PO_and_INVOICE = {67, 9};
    public static int[] CURRENCY_for_total_sum_in_PO = {30, 10};
    public static int[] AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO = {68, 11};
    public static int[] ITEM_CLASSIFICATION_NUMBER_in_ERP = {21, 12};
    public static int[] ITEM_CLASSIFICATION_DESCRIPTION_in_ERP = {22, 13};
    public static int[] ACCOUNTING_TRANSACTION_TYPE = {116, 14};
    public static int[] ACCOUNTING_COSTING_TYPE = {117, 15};
    public static int[] VAT_PERCENT_PER_ITEM_in_PO = {26, 16};
    public static int[] additional_ITEM_CATNO = {-1, 17};*/
    public static int[] Customer_ITEM_CATNO = {14, 0};
    public static int[] Supplier_ITEM_CATNO = {15, 1};
    public static int[] Manufacturer_ITEM_CATNO = {16, 2};
    public static int[] ITEM_DESCRIPTION_in_PO = {17, 3};
    public static int[] SUPPLIER_ID_IN_ERP = {13, 4};
    public static int[] ITEM_UNIT_PRICE_in_PO = {23, 5, 7};//<-------
    public static int[] CURRENCY_of_ITEM_in_PO = {22, 6};

    public static int[] CURRENCY_CONVERSION_RATE_between_PO_and_INVOICE = {66, 8};
    public static int[] CURRENCY_for_total_sum_in_PO = {29, 9};
    public static int[] AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO = {67, 10};
    public static int[] ITEM_CLASSIFICATION_NUMBER_in_ERP = {20, 11};
    public static int[] ITEM_CLASSIFICATION_DESCRIPTION_in_ERP = {21, 12};
    public static int[] ACCOUNTING_TRANSACTION_TYPE = {115, 13};
    public static int[] ACCOUNTING_COSTING_TYPE = {116, 14};
    public static int[] VAT_PERCENT_PER_ITEM_in_PO = {25, 15};
    public static int[] additional_ITEM_CATNO = {-1, 16};

    public static int[][] CatalogNumbers = {Customer_ITEM_CATNO, Supplier_ITEM_CATNO, Manufacturer_ITEM_CATNO, additional_ITEM_CATNO};
    public static int[][] NeedToFillIfEmpty = {Customer_ITEM_CATNO, Supplier_ITEM_CATNO, Manufacturer_ITEM_CATNO, ITEM_DESCRIPTION_in_PO,
        SUPPLIER_ID_IN_ERP, CURRENCY_of_ITEM_in_PO, CURRENCY_CONVERSION_RATE_between_PO_and_INVOICE, CURRENCY_for_total_sum_in_PO, AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO,
        ITEM_CLASSIFICATION_NUMBER_in_ERP, ITEM_CLASSIFICATION_DESCRIPTION_in_ERP, ACCOUNTING_TRANSACTION_TYPE, ACCOUNTING_COSTING_TYPE, VAT_PERCENT_PER_ITEM_in_PO};

    public static void main(String[] args) {
        //new config:
        WiseConfig.Config config = new WiseConfig.Config(args);
        config.getWisePageOutput();

        DefaultListModel<String> ORDERED_ITEMS_string = readFile(new File(Pathes.Path.get_ordered_Items_csv()), SaveData.CSV_FORMAT);
        DefaultListModel<String> ITEMS_INDEX_string = readFile(new File("C:\\transfers\\out\\items.csv"), SaveData.CSV_FORMAT);
        Vector<String[]> ORDERED_ITEMS_list = DefaultListModel2StringArrays(ORDERED_ITEMS_string);
        Vector<String[]> ITEMS_INDEX_list = DefaultListModel2StringArrays(ITEMS_INDEX_string);

        int ORDERED_ITEMS_size = ORDERED_ITEMS_list.size();//Because the array can expand

        for (int i = 1; i < ITEMS_INDEX_list.size(); i++) {
            boolean found = false;
            for (int o = 1; o < ORDERED_ITEMS_size && !found; o++) {
                if (!ORDERED_ITEMS_list.get(o)[ITEM_UNIT_PRICE_in_PO[0]].equals("")
                        && IsTheSameItem(ORDERED_ITEMS_list.get(o), ITEMS_INDEX_list.get(i))) {
                    found = true;
                    System.out.println("fillTheSameItem(): o=" + o + ", i=" + i);
                    fillTheSameItem(ORDERED_ITEMS_list.get(o), ITEMS_INDEX_list.get(i));
                }
            }
            if (!found) {
                System.out.println("addItem");
                String[] newItem = addItem(ITEMS_INDEX_list.get(i));
                ORDERED_ITEMS_list.add(newItem);
            }
        }

        //Save ORDERED_ITEMS:
        ORDERED_ITEMS_string.clear();
        for (String[] csv_line : ORDERED_ITEMS_list) {
            ORDERED_ITEMS_string.addElement(StringArray2String(csv_line));
        }
        writeModelToFile(new File(Pathes.Path.get_ordered_Items_csv()), ORDERED_ITEMS_string, SaveData.CSV_FORMAT);
    }

    private static boolean IsTheSameItem(String[] ORDERED_ITEMS_line, String[] ITEMS_INDEX_line) {
        try {

            //find ITEMS_INDEX_line percent:
            double Percent = 1;
            try {
                Percent = Double.valueOf(ITEMS_INDEX_line[ITEM_UNIT_PRICE_in_PO[2]]);
            } catch (Exception e) {
            }

            double ITEMS_INDEX_prise = Double.valueOf(ITEMS_INDEX_line[ITEM_UNIT_PRICE_in_PO[1]]);
            double ITEMS_INDEX_prise_fix = ITEMS_INDEX_prise - ITEMS_INDEX_prise * 0.01 * Percent;

            //Chack if the same prise:
            if (Math.abs(Double.valueOf(ORDERED_ITEMS_line[ITEM_UNIT_PRICE_in_PO[0]]) - ITEMS_INDEX_prise) > 0.05) {
                ITEMS_INDEX_prise = ITEMS_INDEX_prise_fix;
                if (Math.abs(Double.valueOf(ORDERED_ITEMS_line[ITEM_UNIT_PRICE_in_PO[0]]) - ITEMS_INDEX_prise) > 0.05) {
                    return false;
                }
            }

            //Chack if the same "item code":
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < CatalogNumbers.length; j++) {
                    if (ITEMS_INDEX_line[CatalogNumbers[j][1]].equalsIgnoreCase(ORDERED_ITEMS_line[CatalogNumbers[i][0]])) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void fillTheSameItem(String[] ORDERED_ITEMS_line, String[] ITEMS_INDEX_line) {
        //System.out.println("fillTheSameItem()");
        for (int i = 0; i < NeedToFillIfEmpty.length; i++) {
            if (ORDERED_ITEMS_line[NeedToFillIfEmpty[i][0]].isEmpty() || ORDERED_ITEMS_line[NeedToFillIfEmpty[i][0]].equals("") || ORDERED_ITEMS_line[NeedToFillIfEmpty[i][0]] == null) {
                ORDERED_ITEMS_line[NeedToFillIfEmpty[i][0]] = ITEMS_INDEX_line[NeedToFillIfEmpty[i][1]];
            }

        }
    }

    private static String[] addItem(String[] ITEMS_INDEX_line) {
        String[] ORDERED_ITEMS_line = new String[118];
        for (int i = 0; i < ORDERED_ITEMS_line.length; i++) {
            ORDERED_ITEMS_line[i] = "";
        }

        for (int i = 0; i < NeedToFillIfEmpty.length; i++) {
            ORDERED_ITEMS_line[NeedToFillIfEmpty[i][0]] = ITEMS_INDEX_line[NeedToFillIfEmpty[i][1]];
        }

        double Percent = 1;
        try {
            Percent = Double.valueOf(ITEMS_INDEX_line[ITEM_UNIT_PRICE_in_PO[2]]);
        } catch (Exception e) {
        }

        double ITEMS_INDEX_prise = Double.valueOf(ITEMS_INDEX_line[ITEM_UNIT_PRICE_in_PO[1]]);
        ITEMS_INDEX_prise = ITEMS_INDEX_prise - ITEMS_INDEX_prise * 0.01 * Percent;
        ORDERED_ITEMS_line[ITEM_UNIT_PRICE_in_PO[0]] = "" + ITEMS_INDEX_prise;
        //ORDERED_ITEMS_line[ITEM_UNIT_PRICE_in_PO[0]] = ITEMS_INDEX_line[ITEM_UNIT_PRICE_in_PO[1]]; //לתקן לפי עמודה 8!!!

        return ORDERED_ITEMS_line;
    }

    public static void print(Vector<String[]> StringList) {
        for (int i = 0; i < StringList.size(); i++) {
            System.out.println(Arrays.toString(StringList.get(i)));
        }
    }
}
