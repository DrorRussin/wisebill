/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Check;

import Constructors.ObData;
import java.util.Vector;

/**
 *
 * @author ERA-mobile
 */
public class CheckQTY {

    public Vector<ObData> checkIntNum = new Vector<ObData>();
    public int posInRootVector = -1;

    public int getSize() {
        return checkIntNum.size();
    }

    boolean isEmpty() {
        return checkIntNum.isEmpty();
    }

    public void setPosInRootVector(int i) {
        posInRootVector = i;
    }

    public boolean contains1() {
        for (int i = 0; i < checkIntNum.size(); i++) {
            if (Double.parseDouble(checkIntNum.elementAt(i).get_result_str()) == 1.00) {
                return true;
            }

        }
        return false;
    }

    public void add1(int i) {
        ObData o = new ObData();
        o.set_result_str("1");
        checkIntNum.add(o);
        posInRootVector = i;
    }
}
