/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Check;

import Constructors.ObData;
import java.util.Vector;

/**
 *
 * @author ERA-mobile
 */
public class CheckSingleMam {

    public double price = -1;
    public double percent = -1;

    public CheckSingleMam(double price, double percent) {
        this.price = price;
        this.percent = percent;

    }

    public double getPrice() {
        return price;
    }

    public double getPercent() {
        return percent;
    }

    public void getPrice(double _price) {
        price = _price;
    }

    public void getPercent(double _percent) {
        percent = _percent;
    }

    public static boolean iSMam(Vector<String> mam_percents, double Total_mam, double Total_befure_mam, double Total_Due, double DeviationForBudget) {
        if (Math.abs(Total_Due - (Total_befure_mam + Total_mam)) <= DeviationForBudget) {
            for (int k = 0; k < mam_percents.size(); k++) {
                try {
                    double _mam = Double.valueOf(mam_percents.get(k)) * 0.01;
                    if (Math.abs(Total_mam - (Total_befure_mam * _mam)) <= DeviationForBudget) {
                        return true;
                    }

                } catch (Exception e) {
                }
            }
        }
        return false;
    }

    public static double findRateInLine(Vector<ObData> line_n, Vector<ObData> ExchangeRatePrices, int start) {
        double rate = 1;
        for (ObData o : line_n) {
            if (o.get_name_by_inv().equalsIgnoreCase("rate")) {
                if (rate != 1) {
                    return 1;
                }
                rate = Double.valueOf(o.get_result_str());
            }
        }

        if (rate == 1
                && ExchangeRatePrices.size() > 0
                && line_n != null && line_n.size() > 0
                && (ExchangeRatePrices.size()==1 || start < ExchangeRatePrices.get(0).get_line_num())) {
            try {
                return Double.valueOf(ExchangeRatePrices.get(0).get_result_str());
            } catch (Exception e) {
            }
        }

        return rate;
    }
}
