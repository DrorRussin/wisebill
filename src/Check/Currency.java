/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Check;

import Constructors.ObData;

/**
 *
 * @author zvika
 */
public class Currency {

    private static String[] ILS = {"ILS", "NIS", "ש\"ח", "ש''ח", "שח", "חש", "₪", "שקל"};
    private static String[] USD = {"USD", "$", "dollar"};

    public static String getCurrency(ObData o) {
        try {
            if (o != null && o.get_result_str() != null && !o.get_result_str().isEmpty()) {
                String obdata = o.get_result_str().replaceAll(" ", "").replaceAll("״", "\"");

                if (sameCurrency(obdata, ILS)) {
                    return ILS[0];
                }

                if (sameCurrency(obdata, USD)) {
                    return USD[0];
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String clearCurrency(ObData o) {
        try {
            String obdata = o.get_result_str().replaceAll(" ", "").replaceAll("״", "\"");
            if (containsCurrency(obdata, ILS) != null && containsCurrency(obdata, ILS).length() > 2) {
                o.set_result_str(containsCurrency(obdata, ILS));
                return containsCurrency(obdata, ILS);
            }

            if (containsCurrency(obdata, USD) != null && containsCurrency(obdata, USD).length() > 2) {
                o.set_result_str(containsCurrency(obdata, ILS));
                return containsCurrency(obdata, USD);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static boolean sameCurrency(String obdata, String[] Currency) {
        for (String currency : Currency) {
            if (currency.equalsIgnoreCase(obdata)) {
                return true;
            }
        }
        return false;
    }

    private static String containsCurrency(String obdata, String[] Currency) {
        for (String currency : Currency) {
            if (obdata.contains(currency)) {
                return obdata.replaceAll(currency, "").trim();
            }
        }
        return null;
    }
}
