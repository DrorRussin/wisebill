/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Check;

import java.util.Vector;

/**
 *
 * @author ERA-mobile
 */
public class CheckVM {
    public Vector<Double> checkDNum;
    public int posInRootVector=-1;
    
    public CheckVM(Vector<Double> checkDNum, int posInRootVector){
        this.checkDNum=checkDNum;
        this.posInRootVector=posInRootVector;
    }
    
    int getSize(){
        return checkDNum.size();
    }
    boolean isEmpty(){
        return checkDNum.isEmpty();
    }
    
    void setPosInRootVector(int i){
        posInRootVector=i;
    }
  
  
}
