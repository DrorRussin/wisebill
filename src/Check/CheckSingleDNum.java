/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Check;

import java.util.Vector;

/**
 *
 * @author ERA-mobile
 */
public class CheckSingleDNum {

    public double checkNum = -1;
    public int posInRootVector = -1;
    public int posInSubVector = -1;

    public CheckSingleDNum(double checkNum, int posInRootVector, int posInSubVector) {
        this.checkNum = checkNum;
        this.posInRootVector = posInRootVector;
        this.posInSubVector = posInSubVector;
    }

    public int getPosInRootVector() {
        return posInRootVector;
    }
}
