/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FT;

import CSV.ConfigFun;
import CSV.LFun;
import Install.Installation;
import Management.SConsole;
import WiseConfig.Config;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import javax.swing.JOptionPane;

/**
 *
 *
 */
public class FT {

    public static Config config;
    private static File f;
    private static FileChannel channel;
    private static FileLock lock;
    public static boolean only_one_run = false;
    
    //protected static Logger theLogger = Logger.getLogger("FT");

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(args.length==1){
            only_one_run = true;
        }
        config = new Config(args);
        CSV.MismatchForSuppliers.single = new CSV.MismatchForSuppliers(config.getMismatchForSuppliersPath());
        /*if (args.length > 0) {
            String pfoldpath = args[0];
            System.setProperty("user.dir", pfoldpath);
            System.out.println(System.getProperty("user.dir"));
        }*/
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SConsole.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SConsole.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SConsole.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SConsole.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    File f = new File("log");
                    if (!f.exists()) {
                        f.mkdir();
                    }
                } catch (SecurityException e) {
                }
                try {
                    File wf = new File("RingOnRequest.lock");
                    if (wf.exists()) {
                        wf.delete();
                    }
                    f = new File(ConfigFun.wiseBillFolderPath+"WBRingOnRequest.lock");
                    // Check if the lock exist
                    if (f.exists()) {
                        // if exist try to delete it
                        f.delete();
                    }
                    // Try to get the lock
                    channel = new RandomAccessFile(f, "rw").getChannel();
                    lock = channel.tryLock();
                    if (lock == null) {
                        channel.close();
                        //theLogger.info("Only 1 instance of programm can run.");
                        JOptionPane.showMessageDialog(null, "Only 1 instance of programm can run.");
                        
                        CSV.LFun.killWiseBill(); //System.exit(0);
                        throw new RuntimeException("Only 1 instance of programm can run.");

                    }
                    // Add shutdown hook to release lock when application shutdown
                    ShutdownHook shutdownHook = new ShutdownHook();
                    Runtime.getRuntime().addShutdownHook(shutdownHook);

                    //Your application tasks here..
                    System.out.println("Running");

                    Thread.sleep(1000); //10000
                } catch (InterruptedException e) {
                    //theLogger.info(e.getMessage());
                } catch (IOException e) {
                    //theLogger.info("Could not start process. " + e.getMessage());
                    throw new RuntimeException("Could not start process.", e);
                }

                LFun.copyFile("source\\HyperOCR\\wisecopy_09122018.exe", WiseConfig.Config.getWiseCopyExePath(), true);
                LFun.copyFile("source\\HyperOCR\\wisecopy_09122018.exe", WiseConfig.Config.getWiseCopyWBExePath(), true);
                LFun.copyFile("source\\HyperOCR\\WISEpage_04122018.exe", WiseConfig.Config.getWisePageExePath(), true);
                LFun.copyFile("C:\\HyperOCR\\WisePage.psw", "C:\\HyperOCR\\WisePageBill.psw", true);
                LFun.copyFile("source\\HyperOCR\\wiserun_16012017.exe", WiseConfig.Config.getWiseRunExePath(), true);

                /*
                 Check if is this first time run?
                 */
                if (config.checkIfConfigExists()) {
                    new SConsole().setVisible(true);

                } else {
                    new Installation().setVisible(true);

                }

            }
        });
    }

    public static void unlockFile() {
        // release and delete file lock
        try {
            if (lock != null) {
                lock.release();
            }
            channel.close();
            f.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class ShutdownHook extends Thread {

        public void run() {
            unlockFile();
        }
    }

}
