/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WiseConfig;

import CSV.CSV2Hash;
import CSV.LFun;
import static CSV.SaveData.CSV_FORMAT;
import CSV.SaveLastFields;
import Pathes.Path;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ERA (Work)
 */
public class Config {

    public String ServerINI = "", InputFolder = "", OutputFolder = "", WiseBillFolder = "", format = "UTF-8", Path_Doctype_Settings = "", Document_Type = "",
            DirectoryINI = "", SuppliersPath = "",  OpencvFolder = "", MismatchForSuppliersPath = "";
    public String OrderedItems = "";

    private static final String WiseRunExe_Path = "C:\\HyperOCR\\WiseRunBill.exe";
    private static final String WiseCopyExe_Path = "C:\\HyperOCR\\WiseCopyBill.exe";
    private static final String WiseCopyWBExe_Path = "C:\\HyperOCR\\WiseCopyBillWB.exe";
    private static final String WisePageExe_Path = "C:\\HyperOCR\\WisePageBill.exe";

    int DEVIATION = 0,
            INDEX_OF_PO_NUMBER = 0,
            INDEX_OF_ITEM_DATE_IN_PO = 0;
    Vector<Integer> INDEX_OF_PN = new Vector<Integer>(),
            INDEX_OF_PRICE = new Vector<Integer>(),
            INDEX_OF_DESCRIPTION = new Vector<Integer>(),
            INDEX_OF_QTY = new Vector<Integer>();
    double PRECENT_FOR_ITEMS_CSV = 0;
    //Vector<String> CSVFiles = new Vector<String>();
    public File WiseBillConfig;
    String DefaultCurrency;
    Vector<String> ForPayWords,
            QuantityWords,
            QuantityWordsTwoWords,
            UnitPriceWords,
            TaxWords,
            UnitPriceTwoWords,
            DescriptionWords,
            HazmanaWords,
            MishWords,
            ClientPNWords,
            ClientPNWords_TowWords,
            ManufacturerPNWords,
            SupplierPNWords,
            QuantityInPack,
            QuantityOfPacks,
            ExchangeRateWords;
    boolean CreateAnlFile;
    double DeviationForBudget,
            PercentageOfBudget;
    //String LastUpdateItemsIndexCsv;

    public static String Csv_ini = "C:\\HyperOCR\\ini\\Wrun_W_Bill_CSV.ini";
    public static String Server_ini = "C:\\HyperOCR\\ini\\Wrun_W_Bill_Server.ini";

    public Config(String[] args) {
        if (args.length == 1) {
            WiseBillConfig = new File(args[0]);
        } else {
            WiseBillConfig = new File(CSV.ConfigFun.wiseBillFolderPath + "\\Config\\WiseBillConfig.config");
        }
        read();
        Management.SConsole.config = this;
        Pathes.Path.config = this;
        try {
            CSV.CSV2Hash.config = this;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void createConfigFolder() {
        if (!WiseBillConfig.exists()) {
            try {
                WiseBillConfig.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean checkIfConfigExists() {
        return WiseBillConfig.exists();
    }

    public int getDeviation() {

        return DEVIATION;
    }

    public double getDeviationForBudget() {

        return DeviationForBudget;
    }

    public static String getWiseRunExePath() {

        return WiseRunExe_Path;
    }

    public static String getWiseCopyExePath() {

        return WiseCopyExe_Path;
    }

    public static String getWiseCopyWBExePath() {

        return WiseCopyWBExe_Path;
    }

    public static String getWisePageExePath() {

        return WisePageExe_Path;
    }

    public double getPercentageOfBudget() {

        return PercentageOfBudget;
    }

    public Vector<Integer> getIndexOfPrice() {

        return INDEX_OF_PRICE;
    }

    public Vector<Integer> getIndexOfPn() {

        return INDEX_OF_PN;
    }

    public Vector<Integer> getIndexOfDescription() {

        return INDEX_OF_DESCRIPTION;
    }

    public Vector<Integer> getIndexOfQty() {

        return INDEX_OF_QTY;
    }

    public int getIndexOfPoNumber() {

        return INDEX_OF_PO_NUMBER - 1;
    }

    public int getIndexOfItemDateInPo() {

        return INDEX_OF_ITEM_DATE_IN_PO - 1;
    }

    public double getPrecentForItemsCsv() {

        return PRECENT_FOR_ITEMS_CSV;
    }

    public String getWisePageINIFile() {

        return ServerINI;
    }

    /*public Vector<String> getWisePageCSV() {
        
        return CSVFiles;
    }*/
    public String getWisePageInput() {

        return InputFolder.trim();
    }

    public String getWisePageOutput() {

        return OutputFolder.trim();
    }

    public String getWiseBillFolder() {

        return WiseBillFolder;
    }

    public Vector<String> getForPayWords() {

        return ForPayWords;

    }

    public Vector<String> getQuantityWords() {

        return QuantityWords;

    }

    public Vector<String> getQuantityWordsTwoWords() {

        return QuantityWordsTwoWords;

    }

    public Vector<String> getUnitPriceWords() {

        return UnitPriceWords;

    }

    public Vector<String> getTaxWords() {

        return TaxWords;
    }

    public String getDefaultCurrency() {
        return DefaultCurrency;
    }

    public Vector<String> getUnitPriceTwoWords() {

        return UnitPriceTwoWords;

    }

    public Vector<String> getDescriptionWords() {

        return DescriptionWords;

    }

    public Vector<String> getExchangeRateWords() {

        return ExchangeRateWords;

    }

    public Vector<String> getHazmanaWords() {

        return HazmanaWords;

    }

    public Vector<String> getMishWords() {

        return MishWords;

    }

    public Vector<String> getClientPNWords() {

        return ClientPNWords;

    }

    public Vector<String> getClientPNWords_TowWords() {

        return ClientPNWords_TowWords;
    }

    public Vector<String> getManufacturerPNWords() {

        return ManufacturerPNWords;

    }

    public Vector<String> getSupplierPNWords() {

        return SupplierPNWords;

    }

    public Vector<String> getQuantityInPack() {

        return QuantityInPack;

    }

    public Vector<String> getQuantityOfPacks() {

        return QuantityOfPacks;

    }

    public boolean getCreateAnlFile() {

        return CreateAnlFile;
    }

    public String getPath_Doctype_Settings() {

        return Path_Doctype_Settings;
    }

    public String getDocument_Type() {

        return Document_Type;
    }

    public String getDirectoryINI() {

        return DirectoryINI;
    }

    public String getOrderedItems() {

        return OrderedItems;
    }

    public String getSuppliersPath() {
        return SuppliersPath;
    }

    public String getMismatchForSuppliersPath() {
        return MismatchForSuppliersPath;
    }

    public String getOpencvFolder() {

        return OpencvFolder;
    }

    public boolean setForPayWords(Vector<String> _ForPayWords) {
        read();
        ForPayWords = _ForPayWords;
        return write();
    }

    public boolean setWisePageINIFile(String _ServerINI) {
        read();
        ServerINI = _ServerINI;
        return write();
    }

    public boolean setWisePageInput(String _InputFolder) {
        read();
        InputFolder = _InputFolder;
        return write();
    }

    public boolean setWisePageOutput(String _OutputFolder) {
        read();
        OutputFolder = _OutputFolder;
        return write();
    }

    /*public boolean setWisePageCSVFiles(File files[]) {
        read();
        CSVFiles = new Vector<String>();
        for (File f : files) {
            CSVFiles.add(f.getName());
        }
        return write();
    }*/
    private String readFile() {
        //write();
        //Vector<String> strings = LFun.file2vector(WiseBillConfig.getPath(), format);
        String v = "";

        try {
            FileInputStream fr = new FileInputStream(WiseBillConfig);
            BufferedReader br = null;
            try {
                if (!format.equals("ANSI")) {
                    br = new BufferedReader(new InputStreamReader(fr, "Unicode"));
                } else {
                    br = new BufferedReader(new InputStreamReader(fr));
                }
            } catch (Exception e) {
            }
            String s = "";
            while ((s = br.readLine()) != null) {
                // System.out.println(s);
                v = v + s + "\n";
            }
        } catch (Exception r) {
        }
        return v;
    }

    private static String cleanTextContent(String text) {

        text = text.trim();
        // strips off all non-ASCII characters
        text = text.replaceAll("[^\\x00-\\x7F]", "");

        // erases all the ASCII control characters
        text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

        // removes non-printable characters from Unicode
        text = text.replaceAll("\\p{C}", "");

        return text.trim();
    }

    private void read() {
        //System.out.println("WiseConfig.Config.read()");
        String content = readFile();
        content = content;/*
         try {
         FileReader reader = new FileReader(WiseBillConfig);
         char[] chars = new char[(int) WiseBillConfig.length()];
         reader.read(chars);
         content = new String(chars);
         reader.close();
         } catch (Exception e) {
         }*/

        try {
            Vector<String> values = new Vector<String>();
            Vector<String> variable = new Vector<String>();

            StringTokenizer strToke = new StringTokenizer(content, "\n");
            while (strToke.hasMoreTokens()) {
                String string_val = strToke.nextToken();
                variable.add(string_val.substring(0, string_val.indexOf("=")).trim().toLowerCase());

                string_val = string_val.substring(string_val.indexOf("=") + 1, string_val.length()).replaceAll("\r", "");
                if (string_val.indexOf(";") != -1) {
                    string_val = string_val.substring(0, string_val.indexOf(";")).replaceAll("\r", "");
                }
                values.add(string_val);
            }

            for (int i = 0; i < variable.size(); i++) {
                if (variable.get(i).equalsIgnoreCase("DirectoryINI")) {
                    DirectoryINI = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("ServerINI")) {
                    ServerINI = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("SuppliersPath")) {
                    SuppliersPath = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("MismatchForSuppliersPath")) {
                    MismatchForSuppliersPath = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("OpencvFolder")) {
                    OpencvFolder = cleanTextContent(values.get(i));
                } else if (variable.get(i).equalsIgnoreCase("OrderedItems")) {
                    OrderedItems = cleanTextContent(values.get(i));

                } else if (variable.get(i).equalsIgnoreCase("InputFolder")) {
                    InputFolder = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("OutputFolder")) {
                    OutputFolder = values.get(i);
                    /* } else if (variable.get(i).equalsIgnoreCase("CSVFiles")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    CSVFiles = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        CSVFiles.add(string);
                    }*/
                } else if (variable.get(i).equalsIgnoreCase("WiseBillFolder")) {
                    WiseBillFolder = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("DEVIATION")) {
                    try {
                        DEVIATION = Integer.valueOf(values.get(i));
                    } catch (Exception e) {
                        DEVIATION = 50;
                    }
                } else if (variable.get(i).equalsIgnoreCase("ForPayWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    ForPayWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        ForPayWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("PRECENT_FROM_ITEMS_CSV")) {
                    try {
                        PRECENT_FOR_ITEMS_CSV = Double.valueOf(values.get(i));
                    } catch (Exception e) {
                        PRECENT_FOR_ITEMS_CSV = 0.05;
                    }
                } else if (variable.get(i).equalsIgnoreCase("INDEX_OF_PRICE")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    INDEX_OF_PRICE = new Vector<Integer>();
                    while (strToke.hasMoreTokens()) {
                        int index = Integer.valueOf(strToke.nextToken());
                        INDEX_OF_PRICE.add(index + 10);
                    }
                } else if (variable.get(i).equalsIgnoreCase("INDEX_OF_PN")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    INDEX_OF_PN = new Vector<Integer>();
                    while (strToke.hasMoreTokens()) {
                        int index = Integer.valueOf(strToke.nextToken());
                        INDEX_OF_PN.add(index + 10);
                    }
                } else if (variable.get(i).equalsIgnoreCase("INDEX_OF_DESCRIPTION")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    INDEX_OF_DESCRIPTION = new Vector<Integer>();
                    while (strToke.hasMoreTokens()) {
                        int index = Integer.valueOf(strToke.nextToken());
                        INDEX_OF_DESCRIPTION.add(index + 10);
                    }
                } else if (variable.get(i).equalsIgnoreCase("INDEX_OF_ITEM_QTY")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    INDEX_OF_QTY = new Vector<Integer>();
                    while (strToke.hasMoreTokens()) {
                        int index = Integer.valueOf(strToke.nextToken());
                        INDEX_OF_QTY.add(index + 10);
                    }
                } else if (variable.get(i).equalsIgnoreCase("INDEX_OF_PO_NUMBER")) {
                    try {
                        INDEX_OF_PO_NUMBER = Integer.valueOf(values.get(i));
                    } catch (Exception es) {

                    }
                } else if (variable.get(i).equalsIgnoreCase("INDEX_OF_ITEM_DATE_IN_PO")) {
                    try {
                        INDEX_OF_ITEM_DATE_IN_PO = Integer.valueOf(values.get(i));
                    } catch (Exception es) {

                    }
                } else if (variable.get(i).equalsIgnoreCase("QuantityWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    QuantityWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        QuantityWords.add(string);
                    }

                } else if (variable.get(i).equalsIgnoreCase("QuantityWordsTwoWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    QuantityWordsTwoWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        QuantityWordsTwoWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("TaxWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    TaxWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        TaxWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("DefaultCurrency")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    DefaultCurrency = values.get(i);
                } else if (variable.get(i).equalsIgnoreCase("UnitPriceWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    UnitPriceWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        UnitPriceWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("UnitPriceTwoWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    UnitPriceTwoWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        UnitPriceTwoWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("DescriptionWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    DescriptionWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        DescriptionWords.add(string);
                    }

                } else if (variable.get(i).equalsIgnoreCase("ExchangeRateWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    ExchangeRateWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        ExchangeRateWords.add(string);
                    }

                } else if (variable.get(i).equalsIgnoreCase("HazmanaWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    HazmanaWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        HazmanaWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("MishWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    MishWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        MishWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("ClientPNWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    ClientPNWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        ClientPNWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("ClientPNWords_TowWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    ClientPNWords_TowWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        ClientPNWords_TowWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("ManufacturerPNWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    ManufacturerPNWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        ManufacturerPNWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("SupplierPNWords")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    SupplierPNWords = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        SupplierPNWords.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("QuantityInPack")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    QuantityInPack = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        QuantityInPack.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("QuantityOfPack")) {
                    strToke = new StringTokenizer(values.get(i), ",");
                    QuantityOfPacks = new Vector<String>();
                    while (strToke.hasMoreTokens()) {
                        String string = strToke.nextToken();
                        QuantityOfPacks.add(string);
                    }
                } else if (variable.get(i).equalsIgnoreCase("CreateAnlFile")) {
                    try {
                        CreateAnlFile = Boolean.valueOf(values.get(i));
                    } catch (Exception es) {

                    }

                } else if (variable.get(i).equalsIgnoreCase("Path_Doctype_Settings")) {
                    try {
                        Path_Doctype_Settings = values.get(i);
                    } catch (Exception e) {

                    }
                } else if (variable.get(i).equalsIgnoreCase("Document_Type")) {
                    try {
                        Document_Type = values.get(i);
                    } catch (Exception e) {

                    }
                } else if (variable.get(i).equalsIgnoreCase("DeviationForBudget")) {
                    try {
                        DeviationForBudget = Double.valueOf(values.get(i));
                    } catch (Exception e) {

                    }

                } else if (variable.get(i).equalsIgnoreCase("PercentageOfBudget")) {
                    try {
                        PercentageOfBudget = Double.valueOf(values.get(i));
                    } catch (Exception e) {

                    }
                }

                /*else if(variable.get(i).equalsIgnoreCase("LastUpdateItemsIndexCsv")){
                    try {
                        LastUpdateItemsIndexCsv = values.get(i);
                    } catch (Exception e) {
LastUpdateItemsIndexCsv = "0";
                    }
                }*/
            }

            if (!new File(getOrderedItems()).exists()) {
                //JOptionPane.showMessageDialog(null, "Error! The File Ordered_Items.csv not exists in the path: " + getOrderedItems() + "'");
                //LFun.killWiseBill();
                System.err.println("Error! The File Ordered_Items.csv is not exists in the path: " + getOrderedItems() + "'"
                        + "\nCreate a new one.   ");
                File file = new File(getOrderedItems());
                file.getParentFile().mkdirs();
                file.createNewFile();

                Charset inputCharset = Charset.forName(CSV_FORMAT);
                OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(file), inputCharset);
                os.write("Supply_Certificate,ITEM_NUMBER_in_Supply_Certificate,ITEM_DATE_in_Supply_certificate,ITEM_QUANTITY_in_Supply_Certificate,VERIFIER_ID_of_the_Supply_Certificate,VERIFIER_PHONE_in_the_Supply_Certificate,VERIFIER_EMAIL_of_the_Supply_Certificate,PO_NUMBER,ITEM_NUMBER_in_PO,ITEM_DATE_in_PO,ITEM_QUANTITY_in_PO,PACKAGES_QUANTITY_PER_ITEM_in_PO,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PO,SUPPLIER_ID_IN_ERP,Customer_ITEM_CATNO,Supplier_ITEM_CATNO,Manufacturer_ITEM_CATNO,ITEM_DESCRIPTION_in_PO,ITEM_MODEL_NUMBER,ITEM_SERIAL_NUMBER,ITEM_CLASSIFICATION_NUMBER_in_ERP,ITEM_CLASSIFICATION_DESCRIPTION_in_ERP,CURRENCY_of_ITEM_in_PO,ITEM_UNIT_PRICE_in_PO,ITEM_TOTAL_PRICE_in_PO,VAT_PERCENT_PER_ITEM_in_PO,PROJECT_NAME_referring_to_ordered_item,PROJECT_NUMBER_referring_to_ordered_item,BUDGET_NUMBER_for_ordered_item,CURRENCY_for_total_sum_in_PO,TOTAL_VAT_EXEPMPT_SUM_in_PO,TOTAL_VAT_CHARGEABLE_SUM_in_PO,TOTAL_VAT_SUM_in_PO,TOTAL_SUM_EXCLUDING_VAT_in_PO,ORDERER_COMPANY_ID_IN_ERP,ORDERER_NAME,ORDERER_PHONE,ORDERER_EMAIL,PO_APPROVER_ID,PO_APPROVER_PHONE,PO_APPROVER_EMAIL,PO_PAYMENT_TERMS,PQ_NUMBER,ITEM_NUMBER_in_PQ,ITEM_DATE_in_PQ,ITEM_QUANTITY_in_PQ,PACKAGES_QUANTITY_PER_ITEM_in_PQ,ITEM_QUANTITY_IN_EACH_PACKAGE_in_PQ,CURRENCY_of_ITEM_in_PQ,ITEM_UNIT_PRICE_in_PQ,ITEM_TOTAL_PRICE_in_PQ,VAT_PERCENT_PER_ITEM_in_PQ,Bill_Of_Lading_NUMBER,ITEM_NUMBER_in_Bill_Of_Lading,ITEM_DATE_in_Bill_Of_Lading,ITEM_QUANTITY_in_Bill_Of_Lading,IMPORT_FILE_NUMBER,IMPORT_ENTRY_Number,ITEM_NUMBER_in_IMPORT_ENTRY,ITEM_DATE_in_IMPORT_ENTRY,ITEM_QUANTITY_in_IMPORT_ENTRY,IMPORT_AGENCY_Name,IMPORT_AGENCY_ID_IN_ERP,IMPORT_AGENCY_FILE_NUMBER,IMPORT_TAXES_PAID_EXCLUDING_VAT,IMPORT_VAT_PAID,CURRENCY_CONVERSION_RATIO_between_PO_and_INVOICE,AGREED_PRICE_UPLIFT_PERCENTAGE_in_INVOICE_vs_PO,Reported_INVOICE_NUMBER,Reported_INVOICE_DATE,Reported_ITEM_NUMBER_in_INVOICE,Reported_ITEM_CATNO_in_INVOICE,Reported_ITEM_DESCRIPTION_in_INVOICE,Reported_ITEM_INTERNAL_COUNTER_in_INVOICE,Reported_CURRENCY_of_item_in_INVOICE,Reported_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Reported_ITEM_QUANTITY_in_INVOICE,Reported_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Reported_ITEM_VAT_SUM_in_INVOICE,Verified_PROFORMA_INVOICE_NUMBER,Verified_PROFORMA_INVOICE_DATE,Verified_ITEM_NUMBER_in_PROFORMA_INVOICE,Verified_ITEM_CATNO_in_PROFORMA_INVOICE,Verified_ITEM_DESCRIPTION_in_PROFORMA_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_PROFORMA_INVOICE,Verified_CURRENCY_of_item_in_PROFORMA_INVOICE,Verified_ITEM_UNIT_PRICE_in_PROFORMA_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_PROFORMA_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_PROFORMA_INVOICE,Verified_ITEM_VAT_SUM_in_PROFORMA_INVOICE,Verified_PROFORMA_INVOICE_ID_IN_ERP,Verified_PROFORMA_INVOICE_FILENAME,Verified_INVOICE_NUMBER,Verified_INVOICE_DATE,Verified_ITEM_NUMBER_in_INVOICE,Verified_ITEM_CATNO_in_INVOICE,Verified_ITEM_DESCRIPTION_in_INVOICE,Verified_ITEM_INTERNAL_COUNTER_in_INVOICE,Verified_CURRENCY_of_item_in_INVOICE,Verified_ITEM_UNIT_PRICE_in_INVOICE_VAT_EXCLUDED,Verified_ITEM_QUANTITY_in_INVOICE,Verified_ITEM_TOTAL_PRICE_EXCLUDING_VAT_in_INVOICE,Verified_ITEM_VAT_SUM_in_INVOICE,Verified_INVOICE_ID_IN_ERP,Verified_INVOICE_FILENAME,ITEM_PAYMENT_APPROVER_NAME,ITEM_PAYMENT_APPROVER_PHONE,ITEM_PAYMENT_APPROVER_EMAIL,PREAPPROVED_PAYMENT_INSTRUCTION_NUMBER,PREAPPROVED_PAYMENT_INSTRUCTION_DATE,APPROVED_PAYMENT_INSTRUCTION_NUMBER,APPROVED_PAYMENT_INSTRUCTION_DATE,REMAIDER_BUDGET_EXCLUDING_VAT,REMAINDER_BUDGET_INCLUDING_VAT,BUDGET_CURRENCY,ACCOUNTING_TRANSACTION_TYPE,ACCOUNTING_COSTING_TYPE,ACCOUNTING_BATCH_NUMBER" + "\r\n");
                os.close();
            }
            if (!new File(getSuppliersPath()).exists()) {
                JOptionPane.showMessageDialog(null, "Error! The File Suppliers.csv is not exists in the path: '" + getSuppliersPath() + "'\nOr not configured in the file \"" + WiseBillConfig + "\" properly.");
                LFun.killWiseBill();
            }
            try {
                File file = new File(getMismatchForSuppliersPath());
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (Exception e) {
            }
            if (!new File(getMismatchForSuppliersPath()).exists()) {
                JOptionPane.showMessageDialog(null, "Error! The MismatchForSuppliersPath parameter not configured in the file \"" + WiseBillConfig + "\" properly.\nMismatchForSuppliersPath()='"+getMismatchForSuppliersPath()+" '");
                LFun.killWiseBill();
            } 

            //DirectoryINI = values.get(0);
            //ServerINI = values.get(1);
            //InputFolder = values.get(2);
            //OutputFolder = values.get(3);
            /*strToke = new StringTokenizer(values.get(4), ",");
            CSVFiles = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                CSVFiles.add(string);
            }*/
            //WiseBillFolder = values.get(5);
            /*try {
                DEVIATION = Integer.valueOf(values.get(6));
            } catch (Exception e) {
                DEVIATION = 50;
            }*/
 /*strToke = new StringTokenizer(values.get(7), ",");
            ForPayWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                ForPayWords.add(string);
            }*/
 /*try {
                PRECENT_FOR_ITEMS_CSV = Double.valueOf(values.get(8));
            } catch (Exception e) {
                PRECENT_FOR_ITEMS_CSV = 0.05;
            }*/
 /*strToke = new StringTokenizer(values.get(9), ",");
            INDEX_OF_PRICE = new Vector<Integer>();
            while (strToke.hasMoreTokens()) {
                int index = Integer.valueOf(strToke.nextToken());
                INDEX_OF_PRICE.add(index + 10);
            }*/

 /*strToke = new StringTokenizer(values.get(10), ",");
            INDEX_OF_PN = new Vector<Integer>();
            while (strToke.hasMoreTokens()) {
                int index = Integer.valueOf(strToke.nextToken());
                INDEX_OF_PN.add(index + 10);
            }*/

 /*strToke = new StringTokenizer(values.get(11), ",");
            INDEX_OF_DESCRIPTION = new Vector<Integer>();
            while (strToke.hasMoreTokens()) {
                int index = Integer.valueOf(strToke.nextToken());
                INDEX_OF_DESCRIPTION.add(index + 10);
            }*/

 /*strToke = new StringTokenizer(values.get(12), ",");
            INDEX_OF_QTY = new Vector<Integer>();
            while (strToke.hasMoreTokens()) {
                int index = Integer.valueOf(strToke.nextToken());
                INDEX_OF_QTY.add(index + 10);
            }*/
 /*try {
                INDEX_OF_PO_NUMBER = Integer.valueOf(values.get(13));
            } catch (Exception es) {

            }*/

 /*try {
                INDEX_OF_ITEM_DATE_IN_PO = Integer.valueOf(values.get(14));
            } catch (Exception es) {

            }*/

 /*strToke = new StringTokenizer(values.get(15), ",");
            QuantityWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                QuantityWords.add(string);
            }

            strToke = new StringTokenizer(values.get(16), ",");
            QuantityWordsTwoWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                QuantityWordsTwoWords.add(string);
            }

            strToke = new StringTokenizer(values.get(17), ",");
            UnitPriceWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                UnitPriceWords.add(string);
            }

            strToke = new StringTokenizer(values.get(18), ",");
            UnitPriceTwoWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                UnitPriceTwoWords.add(string);
            }
             */
 /*strToke = new StringTokenizer(values.get(19), ",");
            DescriptionWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                DescriptionWords.add(string);
            }*/

 /*strToke = new StringTokenizer(values.get(20), ",");
            HazmanaWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                HazmanaWords.add(string);
            }*/

 /*strToke = new StringTokenizer(values.get(21), ",");
            MishWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                MishWords.add(string);
            }*/

 /*strToke = new StringTokenizer(values.get(22), ",");
            ClientPNWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                ClientPNWords.add(string);
            }*/
 /*strToke = new StringTokenizer(values.get(23), ",");
            ClientPNWords_TowWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                ClientPNWords_TowWords.add(string);
            }

            strToke = new StringTokenizer(values.get(24), ",");
            ManufacturerPNWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                ManufacturerPNWords.add(string);
            }*/
 /*strToke = new StringTokenizer(values.get(25), ",");
            SupplierPNWords = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                SupplierPNWords.add(string);
            }

            strToke = new StringTokenizer(values.get(26), ",");
            QuantityInPack = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                QuantityInPack.add(string);
            }

            strToke = new StringTokenizer(values.get(27), ",");
            QuantityOfPacks = new Vector<String>();
            while (strToke.hasMoreTokens()) {
                String string = strToke.nextToken();
                QuantityOfPacks.add(string);
            }*/
 /*try {
                CreateAnlFile = Boolean.valueOf(values.get(28));
            } catch (Exception es) {

            }
            try {
                Path_Doctype_Settings = values.get(29);
            } catch (Exception e) {

            }
            try {
                Document_Type = values.get(30);
            } catch (Exception e) {

            }*/
 /*try {
                DeviationForBudget = Double.valueOf(values.get(31));
            } catch (Exception e) {

            }

            try {
                PercentageOfBudget = Double.valueOf(values.get(32));
            } catch (Exception e) {

            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean write() {
        ForPayWords = new Vector<String>();
        boolean ret = true;
        Writer writer = null;

        /*String csvString = "";
        for (String s : CSVFiles) {
            if (csvString.equals("")) {
                csvString = s;
            } else {
                csvString = csvString + "," + s;
            }
        }
         */
        String payString = "";
        for (String s : ForPayWords) {
            if (payString.equals("")) {
                payString = s;
            } else {
                payString = payString + "," + s;
            }
        }
        Vector<String> temp = new Vector<String>();
        temp.add("ServerINI=" + ServerINI);
        temp.add("InputFolder=" + InputFolder);
        temp.add("OutputFolder=" + OutputFolder);
        //temp.add("CSVFiles=" + csvString);
        temp.add("OrderedItems=" + OrderedItems);
        temp.add("SuppliersPath=" + SuppliersPath);
        temp.add("MismatchForSuppliersPath=" + MismatchForSuppliersPath);
        temp.add("OpencvFolder=" + OpencvFolder);
        temp.add("DEVIATION=" + DEVIATION);
        temp.add("ForPayWords=" + payString);

        Vector<String> lines = new Vector<String>();
        for (String s : temp) {
            lines.add(toUnicode(s));
        }

        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(WiseBillConfig), "Unicode"));
            for (String s : lines) {
                writer.write(s);
                writer.write(System.getProperty("line.separator"));
            }

        } catch (IOException ex) {
            ret = false;
            // report
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/

            }
        }
        return ret;
    }

    public static final String toUnicode(String src) {
        if (src == null) {
            return "";
        }
        char chars[] = src.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= 224 && chars[i] <= 250) {
                chars[i] += ('א' - 224);
            }
        }
        return new String(chars);
    }

}
