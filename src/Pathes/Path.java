/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pathes;

import CSV.CSV2Hash;
import WiseConfig.Config;
import java.io.File;

/**
 *
 * @author ERA (Work)
 */
public class Path {

    public static Config config = null;//new Config();
    private static String bill_in = "\\WB\\bill_in";
    private static String bill_in_a = "\\WB\\bill_in\\a";
    private static String bill_out = "\\WB\\bill_out";
    private static String bill_error = "\\WB\\error";

    private static String wise_in = "\\WB\\wise_in";
    private static String wise_out = "\\WB\\wise_out";
    private static String wise_out_a = "\\WB\\wise_out\\A";
    private static String backup_pdf = "\\WB\\backup_pdf";

    private static String jpg = "\\WB\\jpg";

    private static String csv_final = "\\WB\\csv_final";
    private static String csv = "\\WB\\csv";

    private static String AI = "C:\\HyperOCR\\AI\\WISEBILL";

    /*public static String bill_in = "C:\\WiseBill\\WB\\bill_in";
    public static String bill_in_a = "C:\\WiseBill\\WB\\bill_in\\a";
    public static String bill_out = "C:\\WiseBill\\WB\\bill_out";
    public static String bill_error = "C:\\WiseBill\\WB\\error";
    
    public static String wise_in = "C:\\WiseBill\\WB\\wise_in";
    public static String wise_out = "C:\\WiseBill\\WB\\wise_out";
    public static String wise_out_a = "C:\\WiseBill\\WB\\wise_out\\a";
    
    public static String jpg = "C:\\WiseBill\\WB\\jpg";
    
    public static String csv_final = "C:\\WiseBill\\WB\\csv_final";
    public static String csv = "C:\\WiseBill\\WB\\csv";
    
    public static String ocr_out = "C:\\WiseBill\\AC\\ocr_out"; */
    public static String getInputFolder() {
        return config.getWisePageInput();
    }
    
    public static String getOpencvFolder(){
        return config.getOpencvFolder();
    }

    public static String getOutputFolder() {
        return config.getWisePageOutput();
    }

    public static String getbill_in() {
        return config.getWiseBillFolder() + bill_in;
    }

    public static String getbill_in_a() {
        return config.getWiseBillFolder() + bill_in_a;
    }

    public static String getbill_out() {
        return config.getWiseBillFolder() + bill_out;
    }

    public static String getbill_error() {
        return config.getWiseBillFolder() + bill_error;
    }

    public static String getwise_in() {
        return config.getWiseBillFolder() + wise_in;
    }

    public static String getbackup_pdf() {
        try {
            new File(config.getWiseBillFolder() + backup_pdf).mkdirs();
        } catch (Exception e) {
        }
        return config.getWiseBillFolder() + backup_pdf;
    }

    public static String getwise_out() {
        return config.getWiseBillFolder() + wise_out;
    }

    public static String getwise_out_a() {
        return config.getWiseBillFolder() + wise_out_a;
    }

    public static String getjpg() {
        return config.getWiseBillFolder() + jpg;
    }

    public static String getcsv_final() {
        return config.getWiseBillFolder() + csv_final;
    }

    public static String get_ordered_Items_csv() {
        //return getcsv_final() + "\\ordered_Items.csv";
        return config.getOrderedItems();
    }

    public static String getcsv() {
        return config.getWiseBillFolder() + csv;
    }

    public static String getAI() {
        return AI;
    }

}
